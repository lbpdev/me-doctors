<?php namespace MEDoctors\Commands\APIs;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\APIs\TwitterProxy;

class FetchRSS extends Command implements SelfHandling {

    public function __construct()
    {
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $feed = \Feeds::make('http://feeds.bbci.co.uk/news/health/rss.xml');
        $data = array(
            'title'     => $feed->get_title(),
            'permalink' => $feed->get_permalink(),
            'items'     => $feed->get_items(),
        );

        print_r($data);
    }

}
