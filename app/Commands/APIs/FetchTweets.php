<?php namespace MEDoctors\Commands\APIs;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\APIs\TwitterProxy;

class FetchTweets extends Command implements SelfHandling {

    public function __construct()
    {
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        // Twitter OAuth Config options
        $oauth_access_token = '3306729955-2ATNKqqMTExwfneJNGYARo9FZ9dV1IdrMeLpDot';
        $oauth_access_token_secret = 'D7GbJbFsBpOiKE7pwUu3nYMJwTQWSJjgcLwbGMCgvloY0';
        $consumer_key = '63frGW7bnnyQ5sZBTqOgkIHmv';
        $consumer_secret = 'E0PfNDzzIrxZsALUdGVe9xvkIp3lcethnuGeln7fc2PksIzIMC';
        $user_id = '78884300';
        $screen_name = 'parallax';
        $count = 4;
        $keyword = "dubai";

//        $twitter_url = 'statuses/user_timeline.json';
//        $twitter_url .= '?user_id=' . $user_id;
//        $twitter_url .= '&screen_name=' . $screen_name;
//        $twitter_url .= '&count=' . $count;

        $twitter_url = 'search/tweets.json';
        $twitter_url .= '?q=%23'.$keyword;
        $twitter_url .= '&count=' . $count;

        // Create a Twitter Proxy object from our twitter_proxy.php class
        $twitter_proxy = new TwitterProxy(
            $oauth_access_token,			// 'Access token' on https://apps.twitter.com
            $oauth_access_token_secret,		// 'Access token secret' on https://apps.twitter.com
            $consumer_key,					// 'API key' on https://apps.twitter.com
            $consumer_secret,				// 'API secret' on https://apps.twitter.com
            $user_id,						// User id (http://gettwitterid.com/)
            $screen_name,					// Twitter handle
            $count							// The number of tweets to pull out
        );

        // Invoke the get method to retrieve results via a cURL request
        $tweets = $twitter_proxy->get($twitter_url);

        return $tweets;
    }

}
