<?php

namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserLogRepository;

class AddLogEntry extends Command implements SelfHandling {

    protected $username;
    protected $email;
    protected $log_type;

    public function __construct($username,$email,$log_type){
        $this->username = $username;
        $this->email = $email;
        $this->log_type = $log_type;

    }

    public function handle(UserLogRepository $userLogRepository){

        return $userLogRepository->storeEntry($this->username,$this->email,$this->log_type);
    }
}