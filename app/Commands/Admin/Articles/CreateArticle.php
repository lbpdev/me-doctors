<?php namespace MEDoctors\Commands\Admin\Articles;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\ArticleRepository;

class CreateArticle extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $therapies;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, array $therapies = [], $file)
    {
        $this->input = $input;
        $this->therapies = $therapies;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(ArticleRepository $articleRepository)
    {
        return $articleRepository->store($this->input, $this->therapies, $this->file);
    }

}
