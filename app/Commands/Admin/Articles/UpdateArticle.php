<?php namespace MEDoctors\Commands\Admin\Articles;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use MEDoctors\Models\Article;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\ArticleRepository;

class UpdateArticle extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $channelid;
    protected $therapies;
    protected $file;
    protected $article;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, array $therapies = [], $file , $article)
    {
        $this->input = $input;
        $this->therapies = $therapies;
        $this->file = $file;
        $this->article = $article;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(ArticleRepository $articleRepository)
    {
        return $articleRepository->update($this->input, $this->therapies, $this->file , $this->article);
    }

}
