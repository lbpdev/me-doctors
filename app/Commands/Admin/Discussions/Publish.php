<?php namespace MEDoctors\Commands\Admin\Discussions;

use MEDoctors\Models\Discussion;
use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

class Publish extends Command implements SelfHandling {

    /**
     * @var Object $item
     */
    protected $item;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(DiscussionRepository $discussionRepository)
    {
        return $discussionRepository->publish($this->item);
    }

}
