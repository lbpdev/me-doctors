<?php namespace MEDoctors\Commands\Admin\Discussions;

use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Commands\Command;

class StartPanelDiscussion extends Command {

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $therapyId;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile|array|null
     */
    public $attachment;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($userId, $title, $content, $therapyId, $attachment)
    {
        $this->userId = $userId;
        $this->title = $title;
        $this->content = $content;
        $this->therapyId = $therapyId;
        $this->attachment = $attachment;
    }

}
