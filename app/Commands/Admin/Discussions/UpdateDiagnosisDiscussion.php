<?php namespace MEDoctors\Commands\Admin\Discussions;

use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Commands\Command;
use MEDoctors\Models\Discussion;

class UpdateDiagnosisDiscussion extends Command {

    /**
     * @var discussion
     */
    public $discussion;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $therapyId;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile|array|null
     */
    public $attachment;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Discussion $discussion , $userId, $title, $content, $therapyId, $attachment, $remove_attachment)
    {
        $this->discussion = $discussion;
        $this->userId = $userId;
        $this->title = $title;
        $this->content = $content;
        $this->therapyId = $therapyId;
        $this->attachment = $attachment;
        $this->remove_attachment = $remove_attachment;
    }

}
