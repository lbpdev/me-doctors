<?php namespace MEDoctors\Commands\Admin\Events;

use MEDoctors\Models\Event;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\EventRepository;
class CreateEvent extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, $file)
    {
        $this->input = $input;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(EventRepository $eventRepository)
    {
        return $eventRepository->store($this->input, $this->file);
    }

}
