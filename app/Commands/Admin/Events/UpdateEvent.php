<?php namespace MEDoctors\Commands\Admin\Events;

use MEDoctors\Commands\Command;
use MEDoctors\Models\Event;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\EventRepository;
class UpdateEvent extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $file;
    protected $keep_thumb;
    protected $event;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, $file , $keep_thumb , Event $event)
    {
        $this->input = $input;
        $this->file = $file;
        $this->keep_thumb = $keep_thumb;
        $this->event = $event;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(EventRepository $eventRepository)
    {
        return $eventRepository->update($this->input, $this->file , $this->keep_thumb , $this->event);
    }

}
