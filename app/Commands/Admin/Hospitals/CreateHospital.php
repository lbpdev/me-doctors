<?php namespace MEDoctors\Commands\Admin\Hospitals;

use MEDoctors\Models\Event;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\HospitalRepository;

class CreateHospital extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $hospital;
    protected $contact;
    protected $facilities;
    protected $therapies;
    protected $location;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($hospital , $contact, $facilities, $therapies, $location , $file)
    {
        $this->hopital = $hospital;
        $this->contact = $contact;
        $this->facilities = $facilities;
        $this->therapies = $therapies;
        $this->location = $location;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(HospitalRepository $hospitalRepository)
    {
        return $hospitalRepository->store(
            $this->hopital,
            $this->contact,
            $this->facilities,
            $this->therapies,
            $this->location,
            $this->file
        );
    }

}
