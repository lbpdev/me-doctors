<?php namespace MEDoctors\Commands\Admin\Hospitals;

use MEDoctors\Models\Event;
use MEDoctors\Commands\Command;
use MEDoctors\Models\Hospital;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\HospitalRepository;

class UpdateHospital extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $hospital;
    protected $input;
    protected $location;
    protected $file;
    protected $keep_thumb;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Hospital $hospital ,$input , $contact, $facilities, $therapies, $location , $file, $keep_thumb)
    {
        $this->hopital = $hospital;
        $this->input = $input;
        $this->contact = $contact;
        $this->facilities = $facilities;
        $this->therapies = $therapies;
        $this->location = $location;
        $this->file = $file;
        $this->keep_thumb = $keep_thumb;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(HospitalRepository $hospitalRepository)
    {
        return $hospitalRepository->update(
            $this->hopital,
            $this->input ,
            $this->contact,
            $this->facilities,
            $this->therapies,
            $this->location,
            $this->file,
            $this->keep_thumb
        );
    }

}
