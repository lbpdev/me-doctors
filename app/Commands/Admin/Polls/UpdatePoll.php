<?php namespace MEDoctors\Commands\Admin\Polls;

use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\PollRepository;

class UpdatePoll extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $poll;
    protected $items;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($poll, array $items = [])
    {
        $this->poll = $poll;
        $this->items = $items;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(PollRepository $pollRepository)
    {
        return $pollRepository->update($this->poll, $this->items );
    }

}
