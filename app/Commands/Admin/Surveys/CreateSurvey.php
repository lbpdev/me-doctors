<?php namespace MEDoctors\Commands\Admin\Surveys;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\SurveyRepository;

class CreateSurvey extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $survey;
    protected $therapies;
    protected $items;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($survey, $therapies, array $items = [])
    {
        $this->survey = $survey;
        $this->therapies = $therapies;
        $this->items = $items;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(SurveyRepository $surveyRepository)
    {
        return $surveyRepository->store($this->survey, $this->therapies, $this->items );
    }

}
