<?php namespace MEDoctors\Commands\Admin\Videos;

use MEDoctors\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\VideoRepository;

class CreateVideo extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input,  $file)
    {
        $this->input = $input;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     */
    public function handle(VideoRepository $videoRepository)
    {
        return $videoRepository->store($this->input, $this->file);
    }
}
