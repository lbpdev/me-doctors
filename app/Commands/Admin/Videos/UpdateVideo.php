<?php namespace MEDoctors\Commands\Admin\Videos;

use MEDoctors\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

use MEDoctors\Repositories\Contracts\VideoRepository;

class UpdateVideo extends Command implements SelfHandling {


    /**
     * @var array
     */
    protected $input;
    protected $file;
    protected $keep_thumb;
    protected $video;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, $file , $keep_thumb, $video)
    {
        $this->input = $input;
        $this->file = $file;
        $this->keep_thumb = $keep_thumb;
        $this->video = $video;
    }

    /**
     * Execute the command.
     *
     * @return Video $video
     */
    public function handle(VideoRepository $videoRepository)
    {
        return $videoRepository->update($this->input, $this->file ,$this->keep_thumb , $this->video);
    }

}
