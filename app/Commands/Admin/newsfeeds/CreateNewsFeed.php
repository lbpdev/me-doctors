<?php namespace MEDoctors\Commands\Admin\newsfeeds;

use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;

use MEDoctors\Repositories\Contracts\NewsFeedRepository;

class CreateNewsFeed extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input,  $file)
    {
        $this->input = $input;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     */
    public function handle(NewsFeedRepository $newsFeedRepository)
    {
        return $newsFeedRepository->store($this->input, $this->file);
    }

}
