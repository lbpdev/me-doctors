<?php namespace MEDoctors\Commands\Admin\newsfeeds;

use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;

use MEDoctors\Repositories\Contracts\NewsFeedRepository;

class UpdateNewsFeed extends Command implements SelfHandling {

    /**
     * @var array
     */
    protected $input;
    protected $file;
    protected $news;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($input, $file , $news)
    {
        $this->input = $input;
        $this->file = $file;
        $this->news = $news;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(NewsFeedRepository $newsFeedRepository)
    {
        return $newsFeedRepository->update($this->input, $this->file , $this->news);
    }

}
