<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;
use MEDoctors\Models\User;

class PollAnswers extends Command {

    /**
     * @var User
     */
    public $user;

    /**
     * The survey id.
     *
     * @var int
     */
    public $pollId;

    /**
     * The answers of the user of the survey.
     *
     * @var array
     */
    public $pollItemId;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $pollId, $pollItemId)
    {
        $this->user = $user;
        $this->pollId = $pollId;
        $this->pollItemId = $pollItemId;
    }
}
