<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;

class PostComment extends Command {

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $discussionId;

    /**
     * @var string
     */
    public $content;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public $attachment;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($userId, $discussionId, $content, $attachment)
	{
		$this->userId = $userId;
        $this->discussionId = $discussionId;
        $this->content = $content;
        $this->attachment = $attachment;
	}

}
