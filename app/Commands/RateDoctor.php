<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class RateDoctor extends Command implements SelfHandling {

    /**
     * @var Int
     */
    protected $user_id;

    /**
     * @var array
     */
    protected $ratings;

    /**
     * @var int
     */
    protected $rater_id;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($user_id, $ratings, $rater_id)
    {
        $this->user = User::where('id', $user_id)->first();
        $this->ratings = $ratings;
        $this->rater_id = $rater_id;
    }

    /**
     * Execute the command.
     *
     * @return bool
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->rateDoctor($this->user, $this->ratings, $this->rater_id);
    }

}
