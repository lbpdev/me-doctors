<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\MailRepository;

class SendMail extends Command implements SelfHandling {

    protected $template;
    protected $sender_email;
    protected $sender_name;
    protected $recipient_email;
    protected $subject;
    protected $message;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($template, $sender_email, $sender_name, $recipient_email, $subject, $message)
	{
        $this->template = $template;
        $this->sender_email = $sender_email;
        $this->sender_name = $sender_name;
        $this->recipient_email = $recipient_email;
        $this->subject = $subject;
        $this->message = $message;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(MailRepository $mailRepository)
	{
        return $mailRepository->sendMail(
            $this->template,
            $this->sender_email,
            $this->sender_name,
            $this->recipient_email,
            $this->subject,
            $this->message
        );
	}

}
