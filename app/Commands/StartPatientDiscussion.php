<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

class StartPatientDiscussion extends Command {

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $therapyId;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile|array|null
     */
    public $attachment;


    /**
     * @var int
     */
    public $status;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($userId, $title, $content, $therapyId, $attachment , $status)
    {
        $this->userId = $userId;
        $this->title = $title;
        $this->content = $content;
        $this->therapyId = $therapyId;
        $this->attachment = $attachment;
        $this->status = $status;
    }

}
