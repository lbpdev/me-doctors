<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;

class SurveyAnswers extends Command {

    /**
     * @var User
     */
    public $user;

    /**
     * The survey id.
     *
     * @var int
     */
    public $surveyId;

    /**
     * The votes of the user of the survey.
     *
     * @var array
     */
    public $votes;

    /**
     * The answers of the user of the survey.
     *
     * @var array
     */
    public $answers;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
    public function __construct(User $user, $surveyId, array $answers)
    {
        $this->user = $user;
        $this->surveyId = $surveyId;
        $this->answers = $answers;
    }

}
