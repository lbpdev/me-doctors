<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;

class UpdateComment extends Command {

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $commentId;

    /**
     * @var string
     */
    public $content;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public $attachment;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($userId, $commentId, $content, $attachment , $remove_attachment)
    {
        $this->userId = $userId;
        $this->commentId = $commentId;
        $this->content = $content;
        $this->attachment = $attachment;
        $this->remove_attachment = $remove_attachment;
    }

}
