<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUser extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $account;

    /**
     * @var array
     */
    protected $therapies;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $account, $therapies)
    {
        $this->user = $user;
        $this->account = $account;
        $this->therapies = $therapies;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateUser($this->user, $this->account, $this->therapies);
    }

}
