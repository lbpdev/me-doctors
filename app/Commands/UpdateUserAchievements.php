<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserAchievements extends Command implements SelfHandling {

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var array
	 */
	protected $achievements;

	/**
	 * @var boolean
	 */
	protected $die;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(User $user, $achievements, $die)
	{
		$this->user = $user;
		$this->achievements = $achievements;
		$this->die = $die;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(UserRepository $userRepository)
	{
		return $userRepository->updateAchievements($this->user, $this->achievements, $this->die);
	}

}
