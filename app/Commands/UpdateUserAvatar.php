<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserAvatar extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $file;

    /**
     * @var boolean
     */
    protected $remove;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $file, $remove)
    {
        $this->user = $user;
        $this->file = $file;
        $this->remove = $remove;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        if($this->remove)
            return $userRepository->removeAvatar($this->user);

        return $userRepository->updateAvatar($this->user, $this->file);
    }

}
