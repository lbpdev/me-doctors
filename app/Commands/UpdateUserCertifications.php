<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserCertifications extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $certifications;

    /**
     * @var boolean
     */
    protected $die;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $certifications, $die)
    {
        $this->user = $user;
        $this->certifications = $certifications;
        $this->die = $die;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateCertifications($this->user, $this->certifications, $this->die);
    }

}
