<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserContacts extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $contacts)
    {
        $this->user = $user;
        $this->contacts = $contacts;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateContacts($this->user, $this->contacts);
    }

}
