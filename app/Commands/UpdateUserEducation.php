<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserEducation extends Command implements SelfHandling {

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var array
	 */
	protected $educations;

	/**
	 * @var boolean
	 */
	protected  $die;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(User $user, $educations , $die)
	{
		$this->user = $user;
		$this->educations = $educations;
		$this->die = $die;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(UserRepository $userRepository)
	{
		return $userRepository->updateEducation($this->user, $this->educations , $this->die);
	}

}
