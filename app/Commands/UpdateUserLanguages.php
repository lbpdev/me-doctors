<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserLanguages extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $languages;

    /**
     * @var boolean
     */
    protected $die;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $languages, $die)
    {
        $this->user = $user;
        $this->languages = $languages;
        $this->die = $die;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateLanguages($this->user, $this->languages, $this->die);
    }

}
