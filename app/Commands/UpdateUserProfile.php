<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserProfile extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $profile;
    protected $file;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $profile , $file)
    {
        $this->user = $user;
        $this->profile = $profile;
        $this->file = $file;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateProfile($this->user, $this->profile, $this->file);
    }

}
