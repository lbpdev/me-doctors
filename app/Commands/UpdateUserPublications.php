<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserPublications extends Command implements SelfHandling {

	/**
	 * @var User
	 */
	protected $user;

	/**
	 * @var array
	 */
	protected $publications;

    /**
     * @var boolean
     */
    protected $die;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(User $user, $publications, $die)
	{
		$this->user = $user;
		$this->publications = $publications;
        $this->die = $die;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle(UserRepository $userRepository)
	{
		return $userRepository->updatePublications($this->user, $this->publications, $this->die);
	}

}
