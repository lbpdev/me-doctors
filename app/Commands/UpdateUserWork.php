<?php namespace MEDoctors\Commands;

use MEDoctors\Models\User;
use MEDoctors\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use MEDoctors\Repositories\Contracts\UserRepository;

class UpdateUserWork extends Command implements SelfHandling {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $work;

    /**
     * @var boolean
     */
    protected $die;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user, $work, $die)
    {
        $this->user = $user;
        $this->work = $work;
        $this->die = $die;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->updateWork($this->user, $this->work, $this->die);
    }

}
