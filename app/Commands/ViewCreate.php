<?php namespace MEDoctors\Commands;

use MEDoctors\Commands\Command;

class ViewCreate extends Command {

    /**
     * @var int
     */
    public $viewable_id;

    /**
     * @var string
     */
    public $user_ip;

    /**
     * @var int
     */
    public $user_id;


    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct($viewable_id, $user_ip, $user_id)
    {
        $this->viewable_id = $viewable_id;
        $this->user_ip = $user_ip;
        $this->user_id = $user_id;
    }

}
