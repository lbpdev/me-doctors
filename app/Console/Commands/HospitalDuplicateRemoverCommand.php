<?php namespace MEDoctors\Console\Commands;

use Illuminate\Console\Command;
use MEDoctors\Services\Hospitals\HospitalDuplicateRemover;
use Symfony\Component\Console\Input\InputArgument;

class HospitalDuplicateRemoverCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'med:clear-duplicate-hospitals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove duplicate hospitals based on name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire(HospitalDuplicateRemover $duplicateRemover)
    {
        $duplicates = $duplicateRemover->clearDuplicates();
        $this->info($duplicates.' duplicate(s) removed.');
    }

}
