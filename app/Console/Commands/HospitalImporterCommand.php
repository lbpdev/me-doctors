<?php namespace MEDoctors\Console\Commands;

use Illuminate\Console\Command;
use MEDoctors\Services\Hospitals\HospitalImporter;
use Symfony\Component\Console\Input\InputArgument;

class HospitalImporterCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'med:import-hospitals';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import hospitals for the Healthcare Directory.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(HospitalImporter $hospitalImporter)
	{
		$hospitalImporter->importFromPath($this->argument('path'));

		$this->info('Hospitals have been imported.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['path', InputArgument::REQUIRED, 'Path to csv file.'],
		];
	}


}
