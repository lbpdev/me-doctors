<?php namespace MEDoctors\Console\Commands\NewsFetchers;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use MEDoctors\Services\Hospitals\HospitalDuplicateRemover;
use Symfony\Component\Console\Input\InputArgument;

use MEDoctors\Services\Feeds\KimonoNewsFetcher;

class MedicalNewsTodayFetcherCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'news-fetcher:mnt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news from medical news today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire(KimonoNewsFetcher $fetcher)
    {
        $fetcher->fetchAPI();
    }

}
