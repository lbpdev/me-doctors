<?php namespace MEDoctors\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use MEDoctors\Models\ServiceHospitalUser;
use MEDoctors\Models\ServiceUser;
use MEDoctors\Models\ServiceUserExp;
use MEDoctors\Services\Hospitals\HospitalDuplicateRemover;
use Symfony\Component\Console\Input\InputArgument;

class PremiumCheckerCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'check-services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news from medical news today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('');
        $this->info('Starting ( Current date: ' . date('Y-m-d') . ' )');

        $this->info('');
        $this->info('Checking Users');

        $expired = ServiceUser::
                    where('expires_on', '<' , date("Y-m-d H:i:s"))->
                    select('user_id', 'premium_service_id','expires_on','created_at')->get();

        if(count($expired)){
            foreach($expired as $exp){
                $this->info($exp->id.' '.$exp->expires_on.' Deactivating');
                $data = $exp->toArray();
                $data['started_on'] = strtotime($exp->created_at);
                ServiceUserExp::create($data);
                $this->info('Deactivated');
            }
        }

        ServiceUser::where('expires_on', '<' , date("Y-m-d H:i:s"))->delete();

        $this->info('Total: '.count($expired));

        $this->info('');
        $this->info('Checking Hospitals');

        $expired = ServiceHospitalUser::
        where('expires_on', '<' , date("Y-m-d H:i:s"))->get();

        if(count($expired)){
            foreach($expired as $exp){
                $this->info($exp->id.' '.$exp->expires_on.' Deactivating');
                $exp->active = 0;
                $exp->save();
                $this->info($exp->id.' Deactivated ');
            }
        }

        $this->info('');
        $this->info('Total: '.count($expired));

        $this->info('');
        $this->info('Check complete');
    }

}
