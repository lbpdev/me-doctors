<?php namespace MEDoctors\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'MEDoctors\Console\Commands\HospitalImporterCommand',
		'MEDoctors\Console\Commands\HospitalDuplicateRemoverCommand',
		'MEDoctors\Console\Commands\NewsFetchers\MedicalNewsTodayFetcherCommand',
		'MEDoctors\Console\Commands\PremiumCheckerCommand',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
        $schedule->command('news-fetcher:mnt')->everyFiveMinutes();
	}

}
