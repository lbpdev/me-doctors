<?php namespace MEDoctors\Events;

use MEDoctors\Events\Event;

use Illuminate\Queue\SerializesModels;
use MEDoctors\Models\Article;

class ArticleViewed extends Event {

	use SerializesModels;


    /**
     * @var Article
     */
    public $article;


    /**
     * @var string
     */
    public $userIp;

    /**
     * @var int
     */
    public $userId;


    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Article $article, $userIp, $userId)
    {
        $this->article = $article;
        $this->userIp = $userIp;
        $this->userId = $userId;
    }
}
