<?php namespace MEDoctors\Events;

use MEDoctors\Events\Event;

use Illuminate\Queue\SerializesModels;
use MEDoctors\Models\Hospital;

class HospitalViewed extends Event {

    use SerializesModels;


    /**
     * @var Hospital
     */
    public $hospital;


    /**
     * @var string
     */
    public $userIp;

    /**
     * @var int
     */
    public $userId;


    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Hospital $hospital, $userIp, $userId)
    {
        $this->hospital = $hospital;
        $this->userIp = $userIp;
        $this->userId = $userId;
    }
}
