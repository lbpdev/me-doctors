<?php namespace MEDoctors\Events;

use Illuminate\Contracts\Database\ModelIdentifier;

trait SerializesModelsWithTrashed {

    /**
     * Get the restored property value after deserialization.
     *
     * @param  mixed  $value
     * @return mixed
     */
    protected function getRestoredPropertyValue($value)
    {
        return $value instanceof ModelIdentifier
            ? (new $value->class)->withTrashed()->findOrFail($value->id) : $value;
    }
    
}