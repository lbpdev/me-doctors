<?php namespace MEDoctors\Events;

use MEDoctors\Models\User;
use MEDoctors\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserHasBeenRemoved extends Event {

	use SerializesModelsWithTrashed, SerializesModels  {
		SerializesModelsWithTrashed::getRestoredPropertyValue insteadof SerializesModels;
	}

	/**
	 * @var User
	 */
	public $user;

	/**
	 * Create a new event instance.
	 *
	 * @param User $user
	 * 
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

}
