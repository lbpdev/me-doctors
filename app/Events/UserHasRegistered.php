<?php namespace MEDoctors\Events;

use MEDoctors\Models\User;
use MEDoctors\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserHasRegistered extends Event {

	use SerializesModels;

	/**
	 * @var User
	 */
	public $user;

	/**
	 * Create a new event instance.
	 *
	 * @param User $user
	 * 
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

}
