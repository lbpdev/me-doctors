<?php namespace MEDoctors\Events;

use MEDoctors\Events\Event;

use Illuminate\Queue\SerializesModels;
use MEDoctors\Models\User;

class UserViewed extends Event {

	use SerializesModels;


    /**
     * @var User
     */
    public $user;


    /**
     * @var string
     */
    public $userIp;

    /**
     * @var int
     */
    public $userId;


    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(User $user, $userIp, $userId)
    {
        $this->user = $user;
        $this->userIp = $userIp;
        $this->userId = $userId;
    }
}
