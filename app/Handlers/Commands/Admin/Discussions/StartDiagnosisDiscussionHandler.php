<?php namespace MEDoctors\Handlers\Commands\Admin\Discussions;

use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Commands\Admin\Discussions\StartDiagnosisDiscussion;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Services\Uploaders\DiscussionAttachmentUploader;

class StartDiagnosisDiscussionHandler {

	/**
	 * @var DiscussionRepository
	 */
	protected $discussionRepository;

	/**
	 * @var DiscussionAttachmentUploader
	 */
	protected $uploader;

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct(DiscussionRepository $discussionRepository,
								DiscussionAttachmentUploader $uploader)
	{
		$this->discussionRepository = $discussionRepository;
		$this->uploader = $uploader;
	}

	/**
	 * Handle the command.
	 *
	 * @param  StartDiagnosisDiscussion  $command
	 * @return void
	 */
	public function handle(StartDiagnosisDiscussion $command)
	{
		if ($attachments = $command->attachment)
		{
			$attachments = $this->uploader->uploadMultiple($attachments);
		}

		$discussion = $this->discussionRepository->createDoctorDiscussion(
            $this->getDiscussionPayload($command), $attachments
		);

        return $discussion;
	}

    /**
     * Create a discussion payload.
     *
     * @param StartDiagnosisDiscussion $command
     *
     * @return array
     */
    private function getDiscussionPayload(StartDiagnosisDiscussion $command)
    {
        $author_id  = $command->userId;
        $title      = $command->title;
        $slug       = str_slug($title);
        $content    = $command->content;
        $therapy_id = $command->therapyId;
        $status     = $command->status;

        return compact('author_id', 'title', 'content', 'slug', 'therapy_id' , 'status');
    }

}
