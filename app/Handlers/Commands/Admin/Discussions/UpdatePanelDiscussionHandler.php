<?php namespace MEDoctors\Handlers\Commands\Admin\Discussions;

use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Commands\Admin\Discussions\StartPanelDiscussion;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Services\Uploaders\DiscussionAttachmentUploader;

class UpdatePanelDiscussionHandler {

    /**
     * @var DiscussionRepository
     */
    protected $discussionRepository;

    /**
     * @var DiscussionAttachmentUploader
     */
    protected $uploader;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(DiscussionRepository $discussionRepository,
                                DiscussionAttachmentUploader $uploader)
    {
        $this->discussionRepository = $discussionRepository;
        $this->uploader = $uploader;
    }

    /**
     * Handle the command.
     *
     * @param  StartPanelDiscussion  $command
     * @return void
     */
    public function handle($command)
    {
        if ($attachments = $command->attachment)
        {
            $attachments = $this->uploader->uploadMultiple($attachments);
        }

        $discussion = $this->discussionRepository->updatePanelDiscussion(
            $command->discussion , $this->getDiscussionPayload($command), $attachments, $command->remove_attachment
        );

    }

    /**
     * Create a discussion payload.
     *
     * @param StartDiagnosisDiscussion $command
     *
     * @return array
     */
    private function getDiscussionPayload($command)
    {
        $author_id  = $command->userId;
        $title      = $command->title;
        $slug       = str_slug($title);
        $content    = $command->content;
        $therapy_id = $command->therapyId;

        return compact('author_id', 'title', 'content', 'slug', 'therapy_id');
    }

}
