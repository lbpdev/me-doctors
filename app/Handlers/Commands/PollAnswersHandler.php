<?php namespace MEDoctors\Handlers\Commands;

use MEDoctors\Repositories\Contracts\PollRepository;
use MEDoctors\Commands\PollAnswers;

use Illuminate\Queue\InteractsWithQueue;

class PollAnswersHandler {

    /**
     * @var SurveyRepository
     */
    protected $pollRepository;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(PollRepository $pollRepository)
    {
        $this->pollRepository = $pollRepository;
    }

    /**
     * Handle the command.
     *
     * @param  SurveyAnswers  $command
     * @return void
     */
    public function handle(PollAnswers $command)
    {
        $this->pollRepository->vote(
            $command->user,
            $command->pollId,
            $command->pollItemId
        );
    }

}
