<?php namespace MEDoctors\Handlers\Commands;

use MEDoctors\Commands\PostComment;
use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Repositories\Contracts\UploadRepository;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Services\Uploaders\DiscussionAttachmentUploader;

class PostCommentHandler {

	/**
	 * @var DiscussionRepository
	 */
	protected $discussionRepository;

	/**
	 * @var DiscussionAttachmentUploader
	 */
	protected $uploader;

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct(DiscussionRepository $discussionRepository, 
	                            DiscussionAttachmentUploader $uploader)
	{
		$this->discussionRepository = $discussionRepository;
		$this->uploader = $uploader;
	}

	/**
	 * Handle the command.
	 *
	 * @param  PostComment  $command
	 * @return void
	 */
	public function handle(PostComment $command)
	{
        $attachments = $this->uploader->uploadMultiple($command->attachment);

		$this->discussionRepository->postComment(
		    $command->userId,
		    $command->discussionId,
		    $command->content,
		    $attachments
		);
	}

}
