<?php namespace MEDoctors\Handlers\Commands;

use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Commands\StartPatientDiscussion;

use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Services\Uploaders\DiscussionAttachmentUploader;

class StartPatientDiscussionHandler {

    /**
     * @var SurveyRepository
     */
    protected $pollRepository;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(DiscussionRepository $discussionRepository, DiscussionAttachmentUploader $discussionAttachmentUploader)
    {
        $this->discussion = $discussionRepository;
        $this->uploader = $discussionAttachmentUploader;
    }

    /**
     * Handle the command.
     *
     * @param  StartPatientDiscussion  $command
     * @return void
     */
    public function handle(StartPatientDiscussion $command)

    {
        if ($attachments = $command->attachment)

        {
            $attachments = $this->uploader->uploadMultiple($attachments);
        }

        $discussion = $this->discussion->createPatientDiscussion(
            $this->getDiscussionPayload($command), $attachments
        );

        return $discussion;
    }

    /**
     * Create a discussion payload.
     *
     * @param StartDiagnosisDiscussion $command
     *
     * @return array
     */
    private function getDiscussionPayload(StartPatientDiscussion $command)
    {
        $author_id  = $command->userId;
        $title      = $command->title;
        $slug       = str_slug($title);
        $content    = $command->content;
        $therapy_id = $command->therapyId;
        $status     = $command->status;

        return compact('author_id', 'title', 'content', 'slug', 'therapy_id' , 'status');
    }
}
