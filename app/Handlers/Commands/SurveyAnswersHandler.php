<?php namespace MEDoctors\Handlers\Commands;

use MEDoctors\Commands\SurveyAnswers;
use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Models\Survey;
use MEDoctors\Models\SurveyEntries;
use MEDoctors\Repositories\Contracts\SurveyRepository;

class SurveyAnswersHandler {


    /**
     * @var SurveyRepository
     */
    protected $surveyRepository;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(SurveyRepository $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }

    /**
     * Handle the command.
     *
     * @param  SurveyAnswers  $command
     * @return void
     */
    public function handle(SurveyAnswers $command)
    {
        $survey = Survey::with('items')->where('id',$command->surveyId)->first();
        $items = $survey->items()->get();

        foreach($items as $item){
            foreach($item->choices as $choice){
                SurveyEntries::where('survey_item_choice_id', $choice->id)
                              ->where('user_id',$command->user->id)->delete();
            }
        }

        foreach ($command->answers as $itemId => $answers)
        {
            $this->surveyRepository->saveUserAnswers($command->user, $command->surveyId, $answers);
        }
    }

}
