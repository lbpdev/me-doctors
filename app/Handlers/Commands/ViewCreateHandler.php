<?php namespace MEDoctors\Handlers\Commands;

use MEDoctors\Commands\ViewCreate;
use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Repositories\Contracts\ViewRepository;

class ViewCreateHandler {

    /**
     * @var ViewRepository
     */
    protected $viewRepository;

    /**
     * Create the command handler.
     *
     * @return void
     */
    public function __construct(ViewRepository $viewRepository)
    {
        $this->viewRepository = $viewRepository;
    }

    /**
     * Handle the command.
     *
     * @param  PostComment  $command
     * @return void
     */

    public function handle(ViewCreate $command)
    {
        $this->viewRepository->viewCreate(
            $command->viewable_id,
            $command->viewable_type,
            $command->user_ip,
            $command->user_id
        );
    }

}
