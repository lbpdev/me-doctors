<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Events\ArticleViewed;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use MEDoctors\Repositories\Contracts\ViewRepository;
use MEDoctors\Repositories\Contracts\ArticleRepository;

class ArticleViewedHandler {

    protected $viewRepository;

    protected $articleRepository;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */


    public function __construct(ViewRepository $viewRepository , ArticleRepository $articleRepository)
    {
        $this->viewRepository = $viewRepository;
        $this->articleRepository = $articleRepository;
    }


	/**
	 * Handle the event.
	 *
	 * @param  ArticleViewed  $event
	 * @return void
	 */
    public function handle(ArticleViewed $event)
    {
        $view = $this->viewRepository->create(
            $event->userIp,
            $event->userId
        );

        if($view)
            $this->articleRepository->view($event->article, $view);
    }

}
