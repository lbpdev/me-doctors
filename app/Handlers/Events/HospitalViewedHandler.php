<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Events\HospitalViewed;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\ViewRepository;

class HospitalViewedHandler {

    protected $viewRepository;

    protected $hospitalRepository;

    /**
     * Create the event handler.
     *
     * @return void
     */


    public function __construct(ViewRepository $viewRepository , HospitalRepository $hospitalRepository)
    {
        $this->viewRepository = $viewRepository;
        $this->hospitalRepository = $hospitalRepository;
    }


    /**
     * Handle the event.
     *
     * @param  UserViewed  $event
     * @return void
     */
    public function handle(HospitalViewed $event)
    {
        $view = $this->viewRepository->create(
            $event->userIp,
            $event->userId
        );

        if($view)
        $this->hospitalRepository->view($event->hospital, $view);
    }

}
