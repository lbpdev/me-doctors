<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Mailers\UserMailer;
use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Events\UserHasRegistered;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendConfirmationEmail implements ShouldBeQueued {

	/**
	 * @var UserMailer
	 */
	protected $mailer;

	/**
	 * Create the event handler.
	 *
	 * @param UserMailer $mailer
	 * 
	 * @return void
	 */
	public function __construct(UserMailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserHasRegistered  $event
	 * @return void
	 */
	public function handle(UserHasRegistered $event)
	{
		$this->mailer->sendEmailConfirmationTo($event->user);
	}

}
