<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Events\DeniedDoctor;
use MEDoctors\Mailers\DoctorMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendDeniedDoctorEmail implements ShouldBeQueued {

	/**
	 * @var DoctorMailer
	 */
	protected $mailer;

	/**
	 * Create the event handler.
	 *
	 * @param DoctorMailer $mailer
	 * 
	 * @return void
	 */
	public function __construct(DoctorMailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  DeniedDoctor  $event
	 * @return void
	 */
	public function handle(DeniedDoctor $event)
	{
		$this->mailer->sendDeniedDoctorEmail($event->user);
	}

}
