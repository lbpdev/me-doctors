<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Mailers\UserMailer;
use Illuminate\Queue\InteractsWithQueue;
use MEDoctors\Events\UserHasBeenRemoved;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendRemovedAccountEmail implements ShouldBeQueued {

	/**
	 * @var UserMailer
	 */
	private $mailer;

	/**
	 * Create the event handler.
	 *
	 * @param UserMailer $mailer
	 */
	public function __construct(UserMailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  DoctorHasBeenRemoved  $event
	 * @return void
	 */
	public function handle(UserHasBeenRemoved $event)
	{
		$this->mailer->sendRemovedAccountEmail($event->user);
	}

}
