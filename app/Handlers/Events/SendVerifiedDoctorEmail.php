<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Mailers\DoctorMailer;
use MEDoctors\Events\VerifiedDoctor;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendVerifiedDoctorEmail implements ShouldBeQueued {

	/**
	 * @var DoctorMailer
	 */
	protected $mailer;

	/**
	 * Create the event handler.
	 *
	 * @param DoctorMailer $mailer
	 * 
	 * @return void
	 */
	public function __construct(DoctorMailer $mailer)
	{
		$this->mailer = $mailer;
	}

	/**
	 * Handle the event.
	 *
	 * @param  VerifiedDoctor  $event
	 * @return void
	 */
	public function handle(VerifiedDoctor $event)
	{
		$this->mailer->sendVerifiedDoctorEmail($event->user);
	}

}
