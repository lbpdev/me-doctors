<?php namespace MEDoctors\Handlers\Events;

use MEDoctors\Events\UserViewed;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Repositories\Contracts\ViewRepository;

class UserViewedHandler {

    protected $viewRepository;

    protected $userRepository;

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */


    public function __construct(ViewRepository $viewRepository , UserRepository $userRepository)
    {
        $this->viewRepository = $viewRepository;
        $this->userRepository = $userRepository;
    }


	/**
	 * Handle the event.
	 *
	 * @param  UserViewed  $event
	 * @return void
	 */
    public function handle(UserViewed $event)
    {
        $view = $this->viewRepository->create(
            $event->userIp,
            $event->userId
        );

        if($view)
            $this->userRepository->view($event->user, $view);
    }

}
