<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Services\StatsService;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\UserStatRepository;


class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Display admin dashboard.
     *
     * @param  StatsService $stats
     *
     * @return Response
     */
    public function home(StatsService $stats)
    {
        return view('admin.home', compact('stats'));
    }


}
