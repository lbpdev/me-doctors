<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Ad;
use MEDoctors\Models\AdPending;
use MEDoctors\Models\AdPendingItem;
use MEDoctors\Models\AdType;
use MEDoctors\Models\AdUser;
use MEDoctors\Models\Channel;

use MEDoctors\Repositories\Contracts\AdRepository;

class AdsController extends Controller {

    public function __construct(Ad $ad,AdType $adType, AdRepository $adRepository){
        $this->ad = $ad;
        $this->adType = $adType;
        $this->ads = $adRepository;

        parent::__construct();
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $ads = Ad::with('adType','adUser','image','channel')->get();
        return view('admin.ads.index',compact('ads'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function pending()
	{
        $ads = AdPendingItem::with('link','uploads','product','uploads','pendingAd.user')->get();
        return view('admin.ads.pending',compact('ads'));
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function approvePending($id)
    {
        $ad = AdPendingItem::where('id',$id)->first();
        $ad->status = 2;
        $ad->save();

        return redirect()->back();
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $ads = $this->ad->get();
        $adTypes = $this->adType->lists('name','id');
        $channels = Channel::where('id', '!=', 1)->select('name','id')->lists('name','id');

        return view('admin.ads.create',compact('ads','adTypes','channels'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->ads->store($request);

        return redirect(route('admin.ads.index'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $ad = Ad::with('adUser')->where('id',$id)->first();
        $channels = Channel::where('id', '!=', 1)->select('name','id')->lists('name','id');
        $ads = $this->ad->get();
        $adTypes = $this->adType->lists('name','id');

        return view('admin.ads.edit',compact('ad','ads','adTypes','channels'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
        $this->ads->update($request);

        Session::flash('message','Updated Successfully');

        return redirect(route('admin.ads.index'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $ad = Ad::where('id',$id)->delete();

        return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function toggle($id)
	{
		$ad = Ad::where('id',$id)->first();

        if($ad->active)
            $ad->active = 0;
        else
            $ad->active = 1;

        $ad->save();

        return redirect(route('admin.ads.index'));
	}

}
