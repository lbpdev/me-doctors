<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\ArticleRepository;
use MEDoctors\Repositories\Contracts\ViewRepository;

use MEDoctors\Models\View;
use MEDoctors\Models\Article;
use MEDoctors\Models\Therapy;

use MEDoctors\Events\ArticleViewed;

use MEDoctors\Events\Event;
use Illuminate\Support\Str;
use MEDoctors\Models\Channel;

class ArticlesController extends Controller {

    /**
     * @var ArticleRepository
     */
    protected $articles;

    public function __construct(ArticleRepository $articles)
    {
        $this->middleware('auth');
        $this->articles = $articles;
        parent::__construct();
    }

    /**
     * Display the articles.
     *
     * @return Response
     */
    public function index($channel_id)
    {
        $posts = Article::with('therapies')->where('channel_id', $channel_id)->get();
        return view('admin.articles.show' , compact('posts','forApproval'));
    }


    public function create(){
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $channels = Channel::orderBy('name')->where('id', '!=', 1)->select('name','id')->get()->toArray();
        return view('admin.articles.create', compact('therapies','channels'));
    }

    public function store(Request $request){

        $input = [
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'subtitle' => $request->get('subtitle'),
            'channel_id' => $request->get('channel'),
            'date_posted' => strtotime($request->get('date')),
        ];

        $therapies = $request->get('therapies');
        $file = $request->file('thumbnail');

        $article = $this->dispatchFrom('MEDoctors\Commands\Admin\Articles\CreateArticle', $request, [
            'input'    => $input,
            'therapies'    => $therapies,
            'file' => $file
        ]);

        return redirect(route('adm_articles_single',$article->slug));
    }


    public function show($slug){
        $article = Article::with('uploads')->where('slug',$slug)->first();
        return view('admin.articles.single' , compact('article'));
    }


    public function edit($article_id){
        $article = Article::with('uploads')->where('id',$article_id)->first();
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $channels = Channel::where('id', '!=', 1)->select('name','id')->lists('name','id');

        $article->therapies = $article->therapies->toArray();
        return view('admin.articles.edit' ,  compact('article' , 'therapies' , 'channels'));
    }

    public function update(Request $request){

        $file = $request->file('thumbnail');
        $keep_thumb = true;

        if($request->input('remove_thumb'))
            $keep_thumb = false;

        $input = [
            'title'=>$request->get('title'),
            'subtitle' => $request->get('subtitle'),
            'content' => $request->get('content') ,
            'keep_thumb' => $keep_thumb ,
            'channel_id' => $request->get('channel_id') ,
            'date_posted' => strtotime($request->get('date')),
        ];

        $therapies = $request->get('therapies');
        $article = Article::where('id',$request->input('id'))->first();

        $article = $this->dispatchFrom('MEDoctors\Commands\Admin\Articles\UpdateArticle', $request, [
            'article'    => $article,
            'input'    => $input,
            'therapies'    => $therapies ,
            'file' => $file
        ]);

        return redirect(route('adm_articles_single', $article->slug));
    }

    public function destroy($id){
        Article::where('id',$id)->delete();
        return redirect(route('adm_articles',2));
    }

}














