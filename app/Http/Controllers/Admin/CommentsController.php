<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use MEDoctors\Models\Comment;
use Carbon\Carbon;

use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Http\Controllers\UploadsController as UploadsController;

class CommentsController extends Controller {

    public function __construct(){
        $this->middleware('auth');

        parent::__construct();
    }

	public function index()
	{
		//
	}

	public function store(Request $request)
    {
        $data = $request->input();
        if (trim($data['content']) != "") {
            $comment = new Comment();
            $comment->user_id = $this->user->id;
            $comment->discussion_id = $data['discussion_id'];
            $comment->content = $data['content'];
            $comment->save();
            $comment_id = $comment->id;

            $file = $request->file('attachment');

            if($file){
                $attachment = ($file != null ? UploadsController::uploadFile($file , 'attachments') : '');

                $uploadMeta = new UploadsMeta();
                $uploadMeta->upload_id = $attachment->id;
                $uploadMeta->meta_for = "comments";
                $uploadMeta->meta_id = $comment_id;
                $uploadMeta->save();
            }
        }

        if(isset($data['admin']))
            return redirect('admin/'.$data['type'].'/'.$data['discussion_id']);
        else
            return redirect('discussions/'.$data['discussion_slug']);

	}

    public static function getDiscussionComments($discussion_id){

        $comments = Comment::with('attachments')->where('discussion_id' , $discussion_id)->orderBy('created_at', 'desc')->get();

        foreach($comments as $index=>$comment){
            $comments[$index]->time_added = $comment->created_at->toTimeString();
            $comments[$index]->date_added = $comment->created_at->toFormattedDateString();
            $comments[$index]->author = BaseController::getUserBasicData($comment->user_id);

            $attachment = UploadsMeta::where('meta_id',$comment->id)
                ->where('meta_for', 'comments')
                ->leftJoin('uploads', 'uploads.id', '=', 'uploads_meta.upload_id')
                ->first();

            if($attachment){
                $attachment->file_type = substr($attachment->mime_type, 0 ,strpos($attachment->mime_type, '/'));
                $comments[$index]->attachment = $attachment;
            }
        }

        return $comments;
    }

    public static function countDiscussionComments($discussion_id){
        $count = Comment::where('discussion_id' , $discussion_id)->count();
        return $count;
    }

    public static function getLastUpdate($discussion_id){
        $latest_comment = Comment::where('discussion_id' , $discussion_id)->orderby('created_at','desc')->first();
        $last_update['time'] = Carbon::now()->diffForHumans($latest_comment->created_at, true);
        $user = BaseController::getUserBasicData($latest_comment->user_id);

        if($user){
            $last_update['user'] = $user->fname . " " . $user->lname;
        } else {
          Comment::where('user_id' , $latest_comment->user_id )->where('discussion_id' , $discussion_id)->delete();
          self::getLastUpdate($discussion_id);
        }

        $last_update['username'] = $user->username;
        return $last_update;
    }

    public static function getComment($comment_id){
        $comment = Comment::with('attachments')->where('id' , $comment_id)->first();
        dd($comment);
        $data['attachment'] = $attachment;
        $data['comment'] = $comment;

        return $data;
    }

	public static function update($request)
	{
		$input = $request->input();
        $comment = Comment::where('id',$input['comment_id'])->first();

        $file = $request->file('attachment');

        if($file){
            $attachment = ($file != null ? UploadsController::uploadFile($file , 'attachments') : '');
            $uploadMeta = new UploadsMeta();
            $uploadMeta->upload_id = $attachment->id;
            $uploadMeta->meta_for = "comments";
            $uploadMeta->meta_id = $input['comment_id'];
            $uploadMeta->save();
        }

        if($comment) {
            $comment['content'] = $input['content'];
            $comment->save();
        }

        if(isset($input['remove_attachment'])){
            UploadsController::destroyUpload($input['attachment_id']);
        }

        $data['discussion_id'] = $comment['discussion_id'];
        $data['type'] = $input['type'];

        return $data;

	}

	public static function destroy($id , $request)
	{
        $comment = Comment::where('id',$id)->delete();
        return $request->input();
	}

}
