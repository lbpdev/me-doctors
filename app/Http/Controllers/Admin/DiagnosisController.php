<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Commands\Admin\Discussions\StartDiagnosisDiscussion;

use MEDoctors\Models\Discussion;
use MEDoctors\Models\Therapy;
use MEDoctors\Repositories\Contracts\TherapyRepository;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

use MEDoctors\Commands\Admin\Discussions\UpdateDiagnosisDiscussion;
use MEDoctors\Commands\Admin\Discussions\Publish;
use MEDoctors\Commands\Admin\Discussions\Unpublish;

class DiagnosisController extends Controller {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

    /**
     * @var TherapyRepository
     */
    protected $therapy;

    public function __construct(DiscussionRepository $discussions, TherapyRepository $therapy)
    {
        $this->discussions = $discussions;
        $this->therapy = $therapy;
        $this->middleware('auth');

        parent::__construct();
    }

	/**
     * Display the latest doctor discussions.
     *
     * @param Request $request
     * 
     * @return Response
     */
    public function index()
    {
        $discussions = Discussion::where('channel_id',2)->orderBy('created_at', 'DESC')->get();
        $forApproval = $this->discussions->unpublishedDoctorDiscussions();
        return view('admin.discussions.diagnosis.show' , compact('discussions','forApproval'));
    }


    /**
     * Display the form for posting a new discussion.
     *
     * @return Response
     */
    public function create()
    {
        $therapies = $this->therapy->forSelect();
        return view('admin.discussions.diagnosis.create', compact('therapies'));
    }

    public function store(Request $request)
    {
        $this->dispatch(new StartDiagnosisDiscussion(
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            1 // Status ( Auto Approve since Admin )
        ));

        return redirect(route('adm_diag'));
    }

    public function edit($id){
        $data = Discussion::with('author','comments','attachments')->where('id',$id)->first();
        $therapies = Therapy::lists('name','id');
        return view('admin.discussions.diagnosis.edit' , compact('therapies','data'));
    }


    public function update(Request $request)
    {
        $discussion  = Discussion::where('id',$request->id)->first();
        $remove_attachment = false;

        if($request->get('remove_attachment'))
            $remove_attachment = true;

        $this->dispatch(new UpdateDiagnosisDiscussion(
            $discussion,
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            $remove_attachment
        // $this->getFileFromRequest($request, 'attachment')
        ));

        return redirect(route('adm_diag_single',$discussion->id));
    }

     /**
     * Display the discussion based on the id provided.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $data = Discussion::where('id',$id)->first();
        return view('admin.discussions.diagnosis.single' , compact('data'));
    }


    /**
     * Publish diagnosis.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function publish($id)
    {
        $discussion = Discussion::where('id',$id)->first();
        $this->dispatch(new Publish(
            $discussion
        ));

        return redirect()->back();
    }

    /**
     * Unpublish diagnosis.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function unpublish($id)
    {
        $discussion = Discussion::where('id',$id)->first();
        $this->dispatch(new Unpublish(
            $discussion
        ));

        return redirect()->back();
    }
}
