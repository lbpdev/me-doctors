<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use MEDoctors\Commands\Admin\Discussions\Unpublish;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\PostComment;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Models\Comment;
use MEDoctors\Commands\UpdateComment;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

use MEDoctors\Commands\Admin\Discussions\Publish;

use Illuminate\Support\Facades\Mail;

class DiscussionsCommentsController extends Controller {

    /**
     * Create the controller instance.
     */
    public function __construct(DiscussionRepository $discussionRepository)
    {
        $this->middleware('auth');
        $this->discussions = $discussionRepository;

        parent::__construct();
    }

    /**
     * Post a new comment for the discussion.
     *
     * @param  int $discussionId
     *
     * @return Response
     */

    public function store($discussionId, Request $request)
    {
        $this->dispatch(new PostComment(
            $this->user->id,
            $discussionId,
            $request->get('content'),
            $request->file('attachment')
        ));

        return redirect()->back();
    }

    /**
     * Post a new comment for the discussion.
     *
     * @param  int $discussionId
     *
     * @return Response
     */

    public function index()
    {
        $comments = Comment::with('author')->orderBy('created_at', 'DESC')->get();
        $forApproval = $this->discussions->unpublishedComments();

        return view('admin.discussions.comments.index', compact('comments','forApproval'));
    }



    /**
     * Edit a comment for the discussion.
     *
     * @param  int $commentid
     *
     * @return Response
     */


    public function edit($commentid , Request $request){
        $input = $request->input();
        $data = Comment::with('attachments','discussion')->where('id', $commentid)->first();
        $data->url = $input;

        return view('admin.discussions.edit-comment' , compact('data'));
    }

    /**
     * Update a comment for the discussion.
     *
     * @param  int $discussionId
     *
     * @return Response
     */

    public function update($commentId, Request $request)
    {
        $remove_attachment = false;

        if($request->get('remove_attachment'))
            $remove_attachment = true;

        $this->dispatch(new UpdateComment(
            $this->user->id,
            $commentId,
            $request->get('content'),
            $request->file('attachment'),
            $remove_attachment

        ));

        $type = $request->get('type')==1 ? 'panel' : 'diagnosis';
        $url = 'admin/'. $type . '/'. $request->get('discussion_id');
        return redirect($url);
    }

    /**
     * Publish comment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function publish($id)
    {
        $comment = Comment::with('author','discussion.comments.author')->where('id',$id)->first();

        $this->dispatch(new Publish(
            $comment
        ));

        $this->sendApproveNotification($comment);
        $this->sendCommentNotification($comment);

        return redirect()->back();
    }


    /**
     * UnPublish comment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function unpublish($id)
    {
        $comment = Comment::where('id',$id)->first();
        $this->dispatch(new Unpublish(
            $comment
        ));

        return redirect()->back();
    }


    public function destroy($comment_id){
        Comment::where('id',$comment_id)->delete();
        return redirect()->back();
    }

    public function sendApproveNotification($comment){
        $data['author'] = $comment->author->fullName;
        $data['title'] = $comment->discussion->title;

        if($comment->discussion->channel_id==1)
            $data['link'] = route('doctor.discussions.show', $comment->discussion->slug);
        else
            $data['link'] = route('doctor.diagnosis.show', $comment->discussion->slug);

        $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.notifications.comment-approved',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $comment->author->email,
            'subject'  => 'Discussion Comment Approved',
            'message'  => $data
        ]);
    }

    public function sendCommentNotification($comment){

        $comments = $comment->discussion->comments;
        $discussion_author = $comment->discussion->author->email;
        $comment_author = $comment->author;

        $data['link'] = route('doctor.diagnosis.show', $comment->discussion->slug);
        $data['title'] = $comment->discussion->title;

        if($comment->discussion->channel_id==1)
            $data['link'] = route('doctor.discussions.show', $comment->discussion->slug);

        $data['author'] = $comment->discussion->author->fullName;

        /**
         * Send notification to discussion author
         */
        $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.notifications.new-comment-on-post',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $comment->discussion->author->email,
            'subject'  => 'New Comment on Your Post',
            'message'  => $data
        ]);

        /**
         * Send notification to all discussion users
         */

        $sent[] = $comment_author->email;
        $sent[] = $discussion_author;

        foreach($comments as $comment){
            $author = $comment->author;
            if(!in_array($author->email,$sent)){
                $data['author'] = $author->fullName;

                $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
                    'template' => 'emails.notifications.new-comment',
                    'sender_email' => 'mailman@middleeastdoctor.com',
                    'sender_name'  => 'Middle East Doctor',
                    'recipient_email'  => $author->email,
                    'subject'  => 'New Comment on Post',
                    'message'  => $data
                ]);

                $sent[] = $author->email;
            }
        }
    }
}
