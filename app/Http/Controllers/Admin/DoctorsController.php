<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Bus\Dispatcher;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\Language;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\User;
use MEDoctors\Services\DoctorService;
use MEDoctors\Services\Registration\DoctorRegistrar;
use MEDoctors\Exceptions\InvalidDoctorEmailException;

class DoctorsController extends Controller {

    public function __construct(
        DoctorRegistrar $registrar,
        UserRepository $userRepository,
        DoctorService $doctorService,
        HospitalRepository $hospitalRepository,
        Dispatcher $dispatcher
    ){
        $this->users    = $userRepository;
        $this->doctors = $doctorService;
        $this->hospitals = $hospitalRepository;
        $this->registrar = $registrar;
        $this->dispatcher = $dispatcher;

        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display unverified users listing.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $doctors = $this->users->doctorsOnly()->get();
        return view('admin.users.doctors.index', compact('doctors'));
    }

    /**
     * Show the doctor profile.
     *
     * @param string $username
     *
     * @return Response
     */
    public function show($username , Request $request)
    {
        $user = $this->users->getByUsername($username);

        $user->load('specialties',
            'hospitals.location',
            'hospitals.schedules',
            'educations',
            'languages',
            'achievements',
            'publications',
            'certifications',
            'contacts',
            'services');

        $languages = Language::orderBy('name')->select('name', 'id')->get()->toArray();
        $hospitals = $this->hospitals->generateAutoLoadData(Hospital::with('location')->get());
        $this->hospitals->generateHospitalSchedules($user);
        $user->languages = $user->languages->toArray();
        $agent = \BrowserDetect::detect()->browserFamily;

        return view('admin.users.doctors.show', compact('user','languages','agent','hospitals'));
    }

    /**
     * Show the doctor create.
     *
     */
    public function create()
    {
        $countries = Country::gccCountries()->lists('name', 'code');
        $therapies = Therapy::lists('name', 'id');

        return view('admin.users.doctors.create',compact('countries','therapies'));
    }


    /**
     * Show the doctor create.
     *
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $education = $request->only('education');
        $certifications = $request->only('certifications');
        $phone_numbers = $request->only('phone_numbers');
        $file = $request->file('photo');

        try
        {
            $validator = $this->registrar->validatorLight($request->all());

            if ($validator->fails())
            {
                $this->throwValidationException($request, $validator);
            }

            $data['password'] = $this->generatePassword($data);

            $user = $this->registrar->createBypass($data);
            $user->profile()->create(array());
            $this->users->updateAvatar($user,$file);

            $pass['user'] = $user->fullName;
            $pass['password'] = $data['password'];
            $pass['username'] = $user->username;

            // Notify User
            $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
                'template' => 'emails.doctor.created',
                'sender_email' => 'mailman@middleeastdoctor.com',
                'sender_name'  => 'Middle East Doctor',
                'recipient_email'  => $user->email,
                'subject'  => 'Account verification on Middle East Doctor',
                'message'  => $pass
            ]);

            session()->flash('registration.success', 'Doctor has been registered and an e-mail was sent to '.$user->email.'.<br> Click <a href="'.route('admin.users.doctors.show',$user->username).'">here</a> to view profile.');

            return redirect()->back();

        } catch (InvalidDoctorEmailException $e)
        {
            session()->flash('registration', [
                'error' => $e->getMessage(),
                'invalid_doctor_email' => true
            ]);

            session(['temp-registration-user' => $data]);

            return redirect()->back()->withInput();
        }
    }

    /**
     * Show the doctor profile.
     *
     * @param string $username
     *
     * @return Response
     */
    public function destroy($username)
    {
        $this->users->getByUsername($username)->forceDelete();

        return redirect(route('admin.users.doctors'));
    }


    public function edit($username)
    {
        $user = $this->users->getByUsername($username);
        $therapies = Therapy::orderBy('name')->select('name', 'id')->get()->toArray();
        $user->load('profile','contacts');

        return view('admin.users.doctors.profile.edit', compact('user','therapies'));
    }


    public function update(Request $request)
    {
        $input = $request->all();

        foreach($input['therapies'] as $therapy)
            $therapies[] = array('therapy_id'=>$therapy);

        $file = $request->file('photo');
        $account = $request->only('fname','lname','email','password','designation','username');

        $user = User::where('id',$input['user_id'])->first();

        $this->dispatchFrom('MEDoctors\Commands\UpdateUser', $request, [
            'user'      => $user ,
            'account'   => $account,
            'therapies' => $therapies
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserProfile', $request, [
            'user'    => $user ,
            'profile' => $input,
            'file'    => $file,
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserContacts', $request, [
            'user'    => $user,
            'contacts' => $input['phone_number'],
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserAvatar', $request, [
            'user'   => $user ,
            'file'   => $file,
            'remove' => isset($input['remove_thumb']) ? true : false
        ]);

        Session::flash('message', 'Profile has been updated successfully.');
        Session::flash('alert-type', 'success');
        return redirect(route('admin.users.doctors.edit',$user->username));
    }

    public function availService($user_id,$service_name){
        $this->users->availService($user_id,$service_name);

        return redirect()->back();
    }

    public function toggleService($user_id){
        $this->users->toggleService($user_id);

        return redirect()->back();
    }

    public function generatePassword($data){

        if($data['password'])
            return $data['password'];

        $lname = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $data['lname']));
        $token = rand(1,99999);

        return $lname.$token;

    }
}
