<?php namespace MEDoctors\Http\Controllers\Admin\Email;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use Illuminate\Support\Facades\DB;

use yajra\Datatables\Facades\Datatables;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\Emails\EmailDomainsBlacklist;

class DomainsBlacklistController extends Controller {

	/**
     * @var EmailDomainsBlacklist
     */
    protected $emailDomainsBlacklist;
    
    /**
     * Create DomainsBlacklistController instance.
     *
     * @param EmailDomainsBlacklist $emailDomainsBlacklist
     */
    public function __construct(EmailDomainsBlacklist $emailDomainsBlacklist)
    {
        $this->emailDomainsBlacklist = $emailDomainsBlacklist;
        
        parent::__construct();
    }

    /**
     * Display a paginated blacklist of email domains.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $domains = DB::table('email_domains_blacklist')->orderBy('id','DESC')->paginate(20);
        return view('admin.emails.domains.blacklist',compact('domains'));
    }

    /**
     * Store a new domain to the white list.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            ['domain' => 'required|active_url|unique:email_domains_blacklist,name'], 
            ['domain.unique' => trans('emails.domains.blacklist.exists')]
        );

        $this->emailDomainsBlacklist->addDomain($request->get('domain'));

        return back()->with('flash_message', trans('emails.domains.blacklist.store'));
    }

    /**
     * Remove an email domain from the black list.
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->emailDomainsBlacklist->deleteById($id);
        
        session()->flash('flash_message', trans('emails.domains.blacklist.delete'));

        return back();
    }

}
