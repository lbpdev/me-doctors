<?php namespace MEDoctors\Http\Controllers\Admin\Email;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use yajra\Datatables\Facades\Datatables;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use MEDoctors\Repositories\Contracts\Emails\EmailDomainsWhitelist;

class DomainsWhitelistController extends Controller {

    /**
     * @var EmailDomainsWhitelist
     */
    protected $emailDomainsWhitelist;

    /**
     * Create DomainsWhitelistController instance.
     *
     * @param EmailDomainsWhitelist $emailDomainsWhitelist
     */
    public function __construct(EmailDomainsWhitelist $emailDomainsWhitelist)
    {
        $this->emailDomainsWhitelist = $emailDomainsWhitelist;
        
        parent::__construct();
    }

    /**
     * Display a paginated whitelist of email domains.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $domains = DB::table('email_domains_whitelist')->orderBy('id','DESC')->paginate(20);
        return view('admin.emails.domains.whitelist',compact('domains'));
    }

    /**
     * Store a new domain to the white list.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            ['domain' => 'required|active_url|unique:email_domains_whitelist,name'], 
            ['domain.unique' => trans('emails.domains.whitelist.exists')]
        );

        $this->emailDomainsWhitelist->addDomain($request->get('domain'));

        return back()->with('flash_message', trans('emails.domains.whitelist.store'));
    }

    /**
     * Remove an email domain from the white list.
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->emailDomainsWhitelist->deleteById($id);

        session()->flash('flash_message', trans('emails.domains.whitelist.delete'));

        return back();
    }

}
