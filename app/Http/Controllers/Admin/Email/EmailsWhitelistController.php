<?php namespace MEDoctors\Http\Controllers\Admin\Email;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use yajra\Datatables\Facades\Datatables;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\Emails\EmailsWhitelist;

class EmailsWhitelistController extends Controller {

    /**
     * @var EmailsWhitelist
     */
    protected $emailsWhitelist;
    
    /**
     * Create EmailsWhitelistController instance.
     *
     * @param EmailsWhitelist       $emailsWhitelist
     */
    public function __construct(EmailsWhitelist $emailsWhitelist)
    {
        $this->emailsWhitelist = $emailsWhitelist;

        parent::__construct();
    }

    /**
     * Display a paginated whitelist of emails.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $emails = $this->emailsWhitelist->forDataTable(['id', 'email'])->orderBy('id','DESC')->paginate(20);

        return view('admin.emails.whitelist', compact('emails'));
    }

    /**
     * Store a new email to the white list.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            ['email' => 'required|email|unique:doctor_emails_whitelist'], 
            ['email.unique' => trans('emails.whitelist.exists')]
        );

        $this->emailsWhitelist->addEmail($request->get('email'));

        return back()->with('flash_message', trans('emails.whitelist.store'));
    }

    /**
     * Remove an email from the white list.
     *
     * @param int $id
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->emailsWhitelist->deleteById($id);

        session()->flash('flash_message', trans('emails.whitelist.delete'));

        return back();
    }
}
