<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Carbon\Carbon;
use MEDoctors\Models\Event;
use MEDoctors\Models\Upload;

use MEDoctors\Commands\Admin\Events\CreateEvent;

class EventsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index(){
        $events = Event::get();
        return view('admin.events.show' , compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $input = [
            'title'=> $request->get('title'),
            'content' => $request->get('content'),
            'event_date' => strtotime($request->get('date_start')),
            'event_date_end' => strtotime($request->get('date_end')),
            'location' => $request->get('location'),
            'city' => $request->get('city'),
            'state' => $request->get('state'),
            'country' => $request->get('country'),
        ];

        $file = $request->file('thumbnail');

        $event = $this->dispatchFrom('MEDoctors\Commands\Admin\Events\CreateEvent', $request, [
            'input'    => $input,
            'file' => $file
        ]);

        return redirect(route('adm_event_single',$event->id));
    }


    public function edit($event_id){
        $event = Event::with('uploads')->where('id', $event_id)->first();
        return view('admin.events.edit' ,  compact('event'));
    }


    public function update(Request $request)
    {
        $keep_thumb = true;
        if($request->input('remove_thumb'))
            $keep_thumb = false;


        $input = [
            'id'=>$request->get('id'),
            'title'=>$request->get('title'),
            'content' => $request->get('content'),
            'event_date' => strtotime($request->get('date_start')),
            'event_date_end' => strtotime($request->get('date_end')),
            'location' => $request->get('location'),
            'city' => $request->get('city'),
            'state' => $request->get('state'),
            'country' => $request->get('country'),
        ];

        $file = $request->file('thumbnail');

        $event = $this->dispatchFrom('MEDoctors\Commands\Admin\Events\UpdateEvent', $request, [
            'input'            => $input,
            'file'             => $file,
            'keep_thumb'       => $keep_thumb,
            'event'            => Event::where('id',$request->get('id'))->first()
        ]);

        return redirect(route('adm_event_single',$event->id));
    }

    public function destroy($event_id){
        Event::where('id',$event_id)->delete();
        return redirect(route('adm_events'));
    }

    public function show($event_id){
        $event = Event::where('id',$event_id)->first();
        return view('admin.events.single' , compact('event'));
    }

}
