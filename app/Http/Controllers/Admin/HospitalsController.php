<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\HospitalClaim;
use MEDoctors\Models\HospitalFacility;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\Country;
use MEDoctors\Models\City;

use Illuminate\Bus\Dispatcher;

class HospitalsController extends Controller {

    public function __construct(HospitalRepository $hospitalRepository,Dispatcher $dispatcher ){
        $this->hospital = $hospitalRepository;
        $this->dispatcher = $dispatcher;
        $this->middleware('auth');

        parent::__construct();
    }
    /**
     * Display a listing of hospitals.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $hospitals = Hospital::with('location')->get();
        return view('admin.hospitals.index', compact('hospitals','therapies','countries','cities'));
    }

    /**
     * Display a listing of hospitals.
     *
     * @return Response
     */
    public function claims(Request $request)
    {
        $claims = HospitalClaim::with('hospital','claimer.roles')->get();
        return view('admin.hospitals.claims', compact('claims'));
    }

    /**
     * Grant Claim to user.
     *
     * @return Response
     */
    public function grantClaims($claim_id)
    {
        $claim = HospitalClaim::with('claimer.roles','hospital')->where('id',$claim_id)->first();
        $claim->granted = 1;
        $claim->save();

        $data['user'] = $claim->claimer->fullName;
        $data['user_type'] = $claim->claimer->roles[0]->id;
        $data['hospital'] = $claim->hospital->name;
        $data['token'] = $claim->token;

        // Notify User
        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.hospitals.claim',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $claim->email,
            'subject'  => 'Hospital Listing Claim Approved',
            'message'  => $data
        ]);

        Session::flash('message','E-mail verification has been sent to '.$claim->email);
        return redirect()->back();
    }


    /**
     * Grant Claim to user.
     *
     * @return Response
     */
    public function resendVerification($claim_id)
    {
        $claim = HospitalClaim::with('claimer.roles','hospital')->where('id',$claim_id)->first();

        $data['user'] = $claim->claimer->fullName;
        $data['user_type'] = $claim->claimer->roles[0]->id;
        $data['hospital'] = $claim->hospital->name;
        $data['token'] = $claim->token;

        // Notify User
        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.hospitals.claim',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $claim->email,
            'subject'  => 'Hospital Listing Claim Approved',
            'message'  => $data
        ]);

        Session::flash('message','E-mail verification has been sent to '.$claim->email);
        return redirect()->back();
    }

    /**
     * Deny Claim for user.
     *
     * @return Response
     */
    public function denyClaims($claim_id)
    {
        HospitalClaim::where('id',$claim_id)->delete();
        Session::flash('message','Claim Denied');
        return redirect()->back();
    }

    /**
     * Display a hospital.
     *
     * @return Response
     */
    public function show($hospital_slug)
    {
        $hospital = $this->hospital->withData(['location','avatar'])->findBySlug($hospital_slug);
        return view('admin.hospitals.show', compact('hospital','therapies','countries','cities'));
    }

    /**
     * Display unverified hospitals.
     *
     * @return Response
     */
    public function unverified()
    {
        $hospitals = Hospital::with('location')->where('verified',0)->get();
        return view('admin.hospitals.unverified', compact('hospitals','therapies','countries','cities'));
    }


    /**
     * Display unverified hospitals.
     *
     * @return Response
     */
    public function verify($id)
    {
        $hospital = Hospital::with('location','avatar')->where('id',$id)->first();
        $hospital->verified = 1;
        $hospital->save();

        return redirect()->back();
    }

    /**
     * Create a hospital.
     *
     * @return Response
     */
    public function create()
    {
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $facilities = HospitalFacility::orderBy('name')->select('name','id')->get()->toArray();
        return view('admin.hospitals.create', compact('therapies','facilities'));
    }


    /**
     * Create a hospital.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $hospital = $request->only('hospital');
        $contact = $request->get('contacts');
        $facilities = $request->get('facilities');
        $therapies = $request->get('therapies');
        $location = $request->get('location');
        $file     = $request->file('thumbnail');

        $newHospital = $this->dispatchFrom('MEDoctors\Commands\Admin\Hospitals\CreateHospital', $request, [
            'hospital'  => $hospital['hospital'],
            'contact'  => $contact,
            'facilities'  => $facilities,
            'therapies'  => $therapies,
            'location'  => $location,
            'file'      => $file,
        ]);

        return redirect(route('admin.hospitals.show' , $newHospital->slug));
    }

    /**
     * Edit hospital.
     *
     * @return Response
     */
    public function edit($hospital_slug)
    {
        $hospital = $this->hospital->withData(['location','avatar'])->findBySlug($hospital_slug);
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $facilities = HospitalFacility::orderBy('name')->select('name','id')->get()->toArray();
        return view('admin.hospitals.edit', compact('hospital','therapies','facilities'));
    }


    /**
     * Update a hospital.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $keep_thumb = true;

        $input = $request->only('hospital');
        $hospital = $this->hospital->findBySlug($request->get('slug'));

        $contact = $request->get('contacts');
        $facilities = $request->get('facilities');
        $therapies = $request->get('therapies');
        $location = $request->get('location');
        $file     = $request->file('thumbnail');

        if($request->input('remove_thumb'))
            $keep_thumb = false;

        $newHospital = $this->dispatchFrom('MEDoctors\Commands\Admin\Hospitals\UpdateHospital', $request, [
            'hospital'   => $hospital,
            'input'      => $input['hospital'],
            'contact'    => $contact,
            'facilities' => $facilities,
            'therapies'  => $therapies,
            'location'   => $location,
            'file'       => $file,
            'keep_thumb' => $keep_thumb,
        ]);

        return redirect(route('admin.hospitals.show' , $newHospital->slug));
    }

    public static function destroy($id)
    {
        Hospital::where('id',$id)->delete();
        return redirect(route('admin.hospitals.index'));
    }

    public static function destroyUnverified($id)
    {
        Hospital::where('id',$id)->delete();
        return redirect(route('admin.hospitals.unverified'));
    }


    public function availService($hospital_id,$service_name){
        $this->hospital->availService($hospital_id,$service_name);

        return redirect()->back();
    }

    public function toggleService($hospital_id){
        $this->hospital->toggleService($hospital_id);

        return redirect()->back();
    }
}
