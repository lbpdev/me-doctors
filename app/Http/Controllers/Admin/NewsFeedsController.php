<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\Channel;


class NewsFeedsController extends Controller {

    public function __construct(NewsFeed $newsFeed){
        $this->model = $newsFeed;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index(){
        $news = NewsFeed::get();
        return view('admin.news.index' , compact('news'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function channel($channel_id){
        $news = $this->model->where('channel_id',$channel_id)->get();
        return view('admin.news.index' , compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $channels = Channel::orderBy('name')->where('id', '!=', 1)->select('name','id')->get()->toArray();
        return view('admin.news.create', compact('channels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $input = [
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'subtitle' => $request->get('subtitle'),
            'channel_id' => $request->get('channel'),
            'date_posted' => strtotime($request->get('date')),
        ];

        $file = $request->file('thumbnail');

        $news = $this->dispatchFrom('MEDoctors\Commands\Admin\newsfeeds\CreateNewsFeed', $request, [
            'input'    => $input,
            'file' => $file
        ]);

        return redirect(route('admin.news.show',$news->slug));
    }


    public function edit($news_id){
        $news = NewsFeed::with('uploads')->where('id', $news_id)->first();
        $channels = Channel::orderBy('name')->where('id', '!=', 1)->select('name','id')->get()->toArray();
        return view('admin.news.edit' ,  compact('news','channels'));
    }


    public function update(Request $request)
    {
        $keep_thumb = true;
        if($request->input('remove_thumb'))
            $keep_thumb = false;

        $input = [
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'subtitle' => $request->get('subtitle'),
            'channel_id' => $request->get('channel_id'),
            'keep_thumb' => $keep_thumb ,
            'date_posted' => strtotime($request->get('date')),
        ];

        $file = $request->file('thumbnail');

        $news = $this->dispatchFrom('MEDoctors\Commands\Admin\newsfeeds\UpdateNewsFeed', $request, [
            'input'            => $input,
            'file'             => $file,
            'keep_thumb'       => $keep_thumb,
            'news'            => NewsFeed::where('id',$request->get('id'))->first()
        ]);

        return redirect(route('admin.news.show',$news->slug));
    }

    public function destroy($news_id){
        NewsFeed::where('id',$news_id)->delete();
        return redirect(route('admin.news.index'));
    }

    public function show($slug){
        $news = NewsFeed::where('slug',$slug)->first();
        return view('admin.news.show' , compact('news'));
    }

}
