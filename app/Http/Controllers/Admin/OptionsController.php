<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Article;
use MEDoctors\Models\Option;
use MEDoctors\Models\Poll;
use MEDoctors\Repositories\Contracts\ArticleRepository;
use MEDoctors\Services\HomePageService;

class OptionsController extends Controller {

    public function __construct(ArticleRepository $articleRepository){
        $this->articleRepository = $articleRepository;
        parent::__construct();
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $options = Option::get();
        $patientArticles = Article::where('channel_id', 3)->get()->lists('title','id');
        $doctorArticles = Article::where('channel_id', 2)->get()->lists('title','id');

        $polls = Poll::get()->lists('title','id');

        return view('admin.settings.index' , compact('options','patientArticles' , 'doctorArticles' , 'polls'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request)
	{
		foreach($request->input('option') as $option){
            Option::where('id',$option['id'])->update($option);
        }

        return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
