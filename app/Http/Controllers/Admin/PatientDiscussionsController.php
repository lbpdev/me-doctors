<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use MEDoctors\Commands\StartPatientDiscussion;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Models\Discussion;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

use MEDoctors\Commands\Admin\Discussions\StartPanelDiscussion;
use MEDoctors\Commands\Admin\Discussions\UpdatePanelDiscussion;
use MEDoctors\Commands\Admin\Discussions\Publish;
use MEDoctors\Commands\Admin\Discussions\Unpublish;
use MEDoctors\Models\Therapy;
use MEDoctors\Repositories\Contracts\TherapyRepository;

class PatientDiscussionsController extends Controller {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

    public function __construct(DiscussionRepository $discussions, TherapyRepository $therapyRepository)
    {
        $this->discussions = $discussions;
        $this->therapy = $therapyRepository;
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Display the latest panel discussions.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index()
    {
        $discussions = Discussion::with('therapy')->where('channel_id',3)->orderBy('created_at', 'DESC')->get();
        $forApproval = $this->discussions->unpublishedPatientDiscussions();
        return view('admin.discussions.patients.show' , compact('discussions','forApproval'));

    }


    /**
     * Display the discussion based on the id provided.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show($id){
        $data = Discussion::with('author','comments.attachments','attachments')->where('id',$id)->first();
        return view('admin.discussions.patients.single' , compact('data'));
    }


    public function create()
    {
        $therapies = $this->therapy->forSelect();
        return view('admin.discussions.patients.create', compact('therapies'));
    }


    public function edit($id){
        $data = Discussion::with('author','comments','attachments')->where('id',$id)->first();
        $therapies = $this->therapy->forSelect();
        return view('admin.discussions.patients.edit' , compact('data','therapies'));
    }


    public function store(Request $request)
    {
        $discussion = $this->dispatch(new StartPatientDiscussion(
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            1
        ));

        return redirect(route('patient_discussions'));
    }



    public function update(Request $request)
    {
        $discussion  = Discussion::where('id',$request->id)->first();
        $remove_attachment = false;

        if($request->get('remove_attachment'))
            $remove_attachment = true;

        $this->dispatch(new UpdatePanelDiscussion(
            $discussion,
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            $remove_attachment
        // $this->getFileFromRequest($request, 'attachment')
        ));

        return redirect(route('adm_single_panel',$discussion->id));
    }

    public static function destroy($id)
    {
        Discussion::where('id',$id)->delete();
        return redirect(route('adm_diag'));
    }

    /**
     * Publish diagnosis.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function publish($id)
    {
        $discussion = Discussion::where('id',$id)->first();
        $this->dispatch(new Publish(
            $discussion
        ));

        return redirect()->back();
    }

    /**
     * Unpublish diagnosis.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function unpublish($id)
    {
        $discussion = Discussion::where('id',$id)->first();
        $this->dispatch(new Unpublish(
            $discussion
        ));

        return redirect()->back();
    }
}
