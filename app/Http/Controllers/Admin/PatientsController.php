<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\User;
use Illuminate\Support\Facades\Session;

class PatientsController extends Controller {

    public function __construct(UserRepository $userRepository){
        $this->users    = $userRepository;
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Display unverified users listing.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $patients = $this->users->patientsOnly()->get();
        return view('admin.users.patients.index', compact('patients'));
    }

    /**
     * Show the doctor profile.
     *
     * @param string $username
     *
     * @return Response
     */
    public function show($username , Request $request)
    {
        $user = $this->users->getByUsername($username);

        $user->load('contacts');

        return view('admin.users.patients.show', compact('user'));
    }

    /**
     * Show the doctor profile.
     *
     * @param string $username
     *
     * @return Response
     */
    public function destroy($username)
    {
        $this->users->getByUsername($username)->forceDelete();

        return redirect(route('admin.users.patients'));
    }


    public function edit($username)
    {
        $user = $this->users->getByUsername($username);
        $user->load('profile','contacts');

        return view('admin.users.patients.profile.edit', compact('user'));
    }


    public function update(Request $request)
    {
        $input = $request->all();

        $file = $request->file('photo');
        $account = $request->only('fname','lname','email','password','designation','username');

        $user = User::where('id',$input['user_id'])->first();

        $this->dispatchFrom('MEDoctors\Commands\UpdateUser', $request, [
            'user'      => $user ,
            'account'   => $account,
            'therapies' => []
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserProfile', $request, [
            'user'    => $user ,
            'profile' => $input,
            'file'    => $file,
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserContacts', $request, [
            'user'    => $user,
            'contacts' => $input['phone_number'],
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserAvatar', $request, [
            'user'   => $user ,
            'file'   => $file,
            'remove' => isset($input['remove_thumb']) ? true : false
        ]);

        Session::flash('message', 'Profile has been updated successfully.');
        Session::flash('alert-type', 'success');
        return redirect(route('admin.users.patients.edit',$user->username));
    }


}
