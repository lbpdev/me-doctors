<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Poll;
use MEDoctors\Models\PollItem;
use MEDoctors\Models\PollVote;

use MEDoctors\Commands\PollAnswers;
use MEDoctors\Commands\Admin\Polls\CreatePoll;
use MEDoctors\Commands\Admin\Polls\UpdatePoll;
use MEDoctors\Repositories\Contracts\PollRepository;

class PollsController extends Controller {

    protected $poll;

    public function __construct(PollRepository $poll)
    {
        $this->poll = $poll;
        $this->middleware('auth');

        parent::__construct();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    public function index(){
        $polls= Poll::get();
        return view('admin.polls.show' , compact('polls'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('admin.polls.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $input = $request->input();

        foreach($request->get('items') as $index=>$item){
            $items[$index]['body'] = $item;
        }

        $new_poll = $this->dispatch(new CreatePoll($input['title'], $items));

        return redirect(route('adm_polls_single',$new_poll->id));
	}

    /**
     * Show Poll
     *
     */

    public function show($poll_id){
        $total_entries = 0;
        $poll = Poll::with('items.votes')->whereId($poll_id)->first();

        foreach($poll->items as $item){
            $item->total_entries = count($item->votes);
            $total_entries += count($item->votes);

        }

        $poll->total_entries = $total_entries;

        return view('admin.polls.single' , compact('poll'));
    }



    /**
     * Edit Poll.
     *
     */

    public function edit($poll_id){
        $poll = Poll::with('items')->where('id',$poll_id)->first();
        return view('admin.polls.edit' ,  compact('poll'));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request)
	{
        $input = $request->input();

        $poll = $request->only('title','id');

        foreach($input['items'] as $index=>$item){
            if(isset($input['item_id'][$index]))
                $items[$index]['id'] = $input['item_id'][$index];

            $items[$index]['body'] = $item;
        }

        $new_poll = $this->dispatch(new UpdatePoll($poll, $items));

        return redirect(route('adm_polls_single',$new_poll->id));
	}


    public  function vote(Request $request){

        extract($request->only('poll_id', 'poll_item'));
        $this->dispatch(new PollAnswers($this->user, $poll_id, $poll_item[0]));

        return redirect(route('adm_polls_single', $poll_id));
    }


    public static function destroy($poll_id){
        Poll::where('id' , $poll_id)->delete();
        return redirect(route('adm_polls'));
    }

}
