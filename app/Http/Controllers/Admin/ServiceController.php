<?php namespace MEDoctors\Http\Controllers;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\AdPending;
use MEDoctors\Models\AdPendingItem;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\PremiumService;
use MEDoctors\Models\ServiceTransaction;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Services\TC;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutSale;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutError;

use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Services\Uploaders\AdBannerUploader;

class ServiceController extends Controller {

    public function __construct(
            UserRepository $userRepository,
            HospitalRepository $hospitalRepository,
            AdBannerUploader $adBannerUploader
    ){
        $this->users    = $userRepository;
        $this->hospitals    = $hospitalRepository;
        $this->uploader    = $adBannerUploader;
        $this->middleware('auth');
        parent::__construct();
    }


    /**
     * The landing page.
     *
     *
     * @return Response
     */
    public function advertise()
    {
        $products = PremiumService::where('slug','!=','platinum-account')->get();
        $pending_purchases = AdPending::with('items.product','items.uploads')->where('user_id',$this->user->id)->get();

        return view('doctor.pages.advertise',compact('products','pending_purchases'));
    }


	public function storeQueue(Request $request){
        $input = $request->input('product');

        $pAd = AdPending::create(array('user_id'=>$this->user->id));

        foreach($input as $product){
            if(isset($product['premium_service_id'])){

                $qty = $product['quantity'];
                unset($product['quantity']);

                for($x=0;$x<$qty;$x++){
                    $pAd->items()->create($product);
                }
            }
        }

        return redirect()->back();
    }


	public function updateQueue(Request $request){
        $input = $request->input('orders');
        $files = $request->file('orders');
        $data = null;

        foreach($files as $oIndex => $orders)
            foreach($orders['items'] as $iIndex => $item){
                foreach($item as $fIndex => $file){
                    if($file){
                        $data = AdPendingItem::where('id',$input[$oIndex]['items'][$iIndex]['item_id'])->first();

                        $photo = ($file != null ? $this->uploader->upload($file) : false);

                        if($photo){
                            $data->uploads()->createMany($photo);
                            $data->status = 1;
                            $data->save();
                        }
                    }
                }
            }

        return redirect()->back();
    }

	public function doctorPaymentCallback(){

        $transaction = ServiceTransaction::create($_GET);

        $user = User::where('id',$_GET['user_id'])->select('username')->first();

        if($transaction){
//            TC::username('ellorin_gene');
//            TC::password('Washburn18');
//            TC::sandbox(true);  #Uncomment to use Sandbox
//            TC::verifySSL(false);
//
//            $args = array(
//                'sale_id' => $transaction->order_number
//            );
//
//            try {
//                $result = TwocheckoutSale::retrieve($args);
//                dd($result);
//            } catch (TwocheckoutError $e) {
//                $e->getMessage();
//                dd($result);
//            }

            $this->users->availService($user->username,'platinum-account');

            return redirect(route('doctor.profile',$user->username));
        }
    }

	public function hospitalPaymentCallback(){

        $data = $_GET;
        $data['product_id'] = $data['merchant_product_id'];

        $transaction = ServiceTransaction::create($data);


        $hospital = Hospital::where('id',$_GET['hospital_id'])->select('slug')->first();

        if($transaction){
//            TC::username('ellorin_gene');
//            TC::password('Washburn18');
//            TC::sandbox(true);  #Uncomment to use Sandbox
//            TC::verifySSL(false);
//
//            $args = array(
//                'sale_id' => $transaction->order_number
//            );
//
//            try {
//                $result = TwocheckoutSale::retrieve($args);
//                dd($result);
//            } catch (TwocheckoutError $e) {
//                $e->getMessage();
//                dd($result);
//            }

            $this->hospitals->availService($_GET['hospital_id'],'platinum-account');

            return redirect(route('doctor.hospitals.show',$hospital->slug));
        }
    }

}
