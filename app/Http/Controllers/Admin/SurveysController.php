<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\SurveyAnswers;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Models\Channel;
use MEDoctors\Models\Survey;
use MEDoctors\Models\SurveyEntries;
use MEDoctors\Models\Therapy;

use MEDoctors\Repositories\Contracts\SurveyRepository;
use MEDoctors\Commands\Admin\Surveys\CreateSurvey;
use MEDoctors\Commands\Admin\Surveys\UpdateSurvey;

class SurveysController extends Controller {


    /*
    * @var SurveyRepository
    */
    protected $survey;

    public function __construct(SurveyRepository $survey)
    {
        $this->survey = $survey;
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Display the surveys.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function index($channei_ld)
    {
        $surveys = Survey::where('channel_id', $channei_ld)->get();
        return view('admin.surveys.show' , compact('surveys'));
    }


    public function create(){
        $channels = Channel::where('id', '!=', 1)->lists('name','id');
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        return view('admin.surveys.create', compact('channels','therapies'));
    }


    /**
     * Save the survey answers.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $survey = $request->only('title', 'description' , 'channel_id' , 'disclaimer');
        $therapies = $request->get('therapies');
        $items = $request->only('survey');

        $new_survey = $this->dispatch(new CreateSurvey($survey, $therapies, $items));
        
        return redirect(route('adm_surveys_single',$new_survey->id));
    }

    /**
     * Update Survey.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $survey = $request->only('title', 'description' , 'survey_id' , 'channel_id' , 'disclaimer');
        $therapies = $request->get('therapies');
        $items = $request->only('survey');
        $items['survey']['items'] = array_values(array_filter($items['survey']['items']));

        $new_survey = $this->dispatch(new UpdateSurvey($survey,$therapies , array_filter($items)));

        return redirect(route('adm_surveys_single',$new_survey->id));
    }

    /**
     * Single view of a survey.
     *
     * @param  int $id
     *
     * @return Response
     */

    public function show($survey_id){
        $survey = Survey::with('items.choices')->where('id',$survey_id)->first();

        foreach($survey->items as $item){
            $total_entries = 0;
            foreach($item->choices as $choice){
                $entries = SurveyEntries::where('survey_item_choice_id', $choice->id)->get();
                $choice->entries = $entries;
                $choice->total_entries = count($entries);
                $total_entries += count($entries);
            }
            $item->total_entries = $total_entries;
        }

        return view('admin.surveys.single' , compact('survey'));
    }

    /**
     * Edit survey.
     *
     * @param  int $survey_id
     *
     * @return Response
     */

    public function edit($survey_id){
        $survey = Survey::with('items.choices','therapies')->where('id',$survey_id)->first();
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $channels = Channel::where('id', '!=', 1)->lists('name','id');
        return view('admin.surveys.edit' ,  compact('survey','channels','therapies'));
    }

    /**
     * Preview survey.
     *
     * @param  int $survey_id
     *
     * @return Response
     */


    public function preview($survey_id){
        $survey = Survey::with('items.choices')->where('id', $survey_id)->first();
        $entry_ids = $this->survey->userEntriesBySurvey($this->user, $survey);

        return view('admin.surveys.preview' ,  compact('survey','entry_ids'));
    }



    /**
     * Save the survey answers.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function vote(Request $request)
    {
        extract($request->only('survey_id', 'answers'));

        $this->dispatch(new SurveyAnswers($this->user, $survey_id, $answers));

        return redirect(route('adm_surveys_single',$survey_id));
    }



    /**
     * Destroy survey.
     *
     * @param  int id
     *
     * @return Response
     */

    public function destroy($id)
    {
        Survey::where('id',$id)->delete();
        return redirect(route('adm_surveys'));
    }

}
