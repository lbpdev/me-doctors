<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Upload;
use Carbon\Carbon;
use Image;

use MEDoctors\Models\UploadsMeta;
use MEDoctors\Models\Profile;
use MEDoctors\Services\Uploaders\ArticleThumbnailUploader;
use MEDoctors\Services\Uploaders\EventThumbnailUploader;
use MEDoctors\Services\Uploaders\HospitalAvatarUploader;
use MEDoctors\Services\Uploaders\ProfileAvatarUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadsController extends Controller {

    public function __construct(
        ArticleThumbnailUploader $articleThumbnailUploader,
        EventThumbnailUploader $eventThumbnailUploader,
        HospitalAvatarUploader $hospitalAvatarUploader,
        ProfileAvatarUploader $profileAvatarUploader
    ){
        $this->articleThumbnailUploader = $articleThumbnailUploader;
        $this->eventThumbnailUploader = $eventThumbnailUploader;
        $this->hospitalAvatarUploader = $hospitalAvatarUploader;
        $this->profileAvatarUploader = $profileAvatarUploader;

        parent::__construct();
    }

    public function index(){
        $files = Upload::orderBy('created_at','DESC')->get();
        return view('admin.files.index', compact('files'));
    }

    public function show($id){
        $file = Upload::where('id',$id)->first();
        return view('admin.files.show', compact('file'));
    }

    public function create(Request $request){
        $section = $request->get('section');
        $upload_types = [
            'article' => 'Article',
            'event' => 'Event',
            'hospital' => 'Hospital'
        ];

        return view('admin.files.create', compact('section','upload_types'));
    }

    public function store(Request $request){

        if($request->file() != null){
            $files = $request->file('files');

            if($request->get('upload_type') == 'article')
                $uploaded = ($files != null ? $this->articleThumbnailUploader->uploadMultiple($files) : false);
            elseif($request->get('upload_type') == 'event')
                $uploaded = ($files != null ? $this->eventThumbnailUploader->uploadMultiple($files) : false);
            elseif($request->get('upload_type') == 'hospital')
                $uploaded = ($files != null ? $this->hospitalAvatarUploader->uploadMultiple($files) : false);

            foreach($uploaded as $upload)
                Upload::create($upload);

        }

        return redirect(route('admin.files.index'));
    }

}
