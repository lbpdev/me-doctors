<?php namespace MEDoctors\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Events\DeniedDoctor;
use MEDoctors\Events\VerifiedDoctor;
use MEDoctors\Models\Referral;
use MEDoctors\Models\Role;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;

use MEDoctors\Services\Registration\DoctorRegistrar;
use yajra\Datatables\Facades\Datatables;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\UserRepository;

class UsersController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Create UsersController instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository, DoctorRegistrar $doctorRegistrar)
    {
        $this->userRepository = $userRepository;
        $this->doctorRegistrar = $doctorRegistrar;

        parent::__construct();
    }
    
    /**
     * Display unverified users listing.
     *
     * @param Request $request
     * 
     * @return Response
     */
    public function unverified(Request $request)
    {
        $unverifiedDoctors = $this->userRepository->unverifiedDoctors();

        if ($request->ajax())
        {
            return Datatables::of($this->userRepository->tableOfUnverifiedDoctors())->make(true);
        }

        return view('admin.users.unverified.index', compact('unverifiedDoctors'));
    }

    /**
     * Create a new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $roles = Role::lists('name','id');
        $therapies = Therapy::lists('name','id');

        return view('admin.users.create', compact('roles','therapies'));
    }

    /**
     * Create a new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        try
        {
            $user = User::create($data);
            $user->roles()->sync([$data['role']]);

            if($data['role']==2)
                $user->specialties()->sync($data['therapies']);

            Session::flash('message', 'User Successfully Registered');
            return redirect()->back();

        } catch (InvalidDoctorEmailException $e)
        {
            return redirect()->back();
        }
    }


    /**
     * Unverified doctor details view.
     *
     * @param  string $username
     *
     * @return Response
     */
    public function showUnverified($username)
    {
        $user = $this->userRepository->getByUsername($username);

        $user->load('credentials.attachments');

        return view('admin.users.unverified.show', compact('user'));
    }

    /**
     * Verify the doctor.
     *
     * @param  int $userId
     *
     * @return Response
     */
    public function verify($userId)
    {
        $user = $this->userRepository->verifyDoctor($userId);

        $referral = Referral::where('user_id',$user->id)->first();

        if($referral){
            $referral->registered = 1;
            $referral->save();
        }

        event(new VerifiedDoctor($user));

        session()->flash('flash_message', trans('registration.verified'));

        return redirect(route('admin.users.unverified'));
    }

    /**
     * Delete the doctor from the database.
     *
     * @param  int $userId
     *
     * @return Response
     */
    public function destroy($userId)
    {
        $user = $this->userRepository->deleteById($userId);

        event(new DeniedDoctor($user));

        session()->flash('flash_message', trans('registration.removed'));

        return redirect(route('admin.users.unverified'));
    }

}