<?php namespace MEDoctors\Http\Controllers\Admin;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Video;
use MEDoctors\Models\Channel;

class VideosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$videos = Video::get();
        return view('admin.videos.index', compact('videos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 */
	public function create()
	{
        $channels = Channel::orderBy('name')->where('id', '!=', 1)->lists('name','id');
        return view('admin.videos.create', compact('channels'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 */
	public function store(Request $request)
	{
        $input = $request->only(['title','description','source','channel_id']);

        $file = $request->file('thumbnail');

        $video = $this->dispatchFrom('MEDoctors\Commands\Admin\Videos\CreateVideo', $request, [
            'input'    => $input,
            'file' => $file
        ]);

        return redirect(route('admin.videos.show', $video->id));
	}

	/**
	 * Display the specified resource.
	 *
	 */
	public function show($id)
	{
        $video = Video::where('id',$id)->first();

        return view('admin.videos.show', compact('video'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 */
	public function edit($id)
	{
        $video = Video::where('id',$id)->first();
        $channels = Channel::orderBy('name')->where('id', '!=', 1)->lists('name','id');
        return view('admin.videos.edit', compact('video','channels'));
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(Request $request)
	{
        $keep_thumb = true;
        if($request->input('remove_thumb'))
            $keep_thumb = false;

        $input = $request->only(['title','description','source','channel_id']);

        $file = $request->file('thumbnail');

        $video = $this->dispatchFrom('MEDoctors\Commands\Admin\Videos\UpdateVideo', $request, [
            'input'            => $input,
            'file'             => $file,
            'keep_thumb'       => $keep_thumb,
            'video'            => Video::where('id',$request->get('id'))->first()
        ]);

        return redirect(route('admin.videos.show',$video->id));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 */
	public function destroy($id)
	{
		Video::where('id',$id)->delete();
        return redirect(route('admin.videos.index'));
	}

}
