<?php namespace MEDoctors\Http\Controllers;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Commands\APIs\FetchTweets;
use MEDoctors\Commands\APIs\FetchRSS;

class ApiController extends Controller {

	public function getTweets(){
        $tweets = $this->dispatch(new FetchTweets());
        echo $tweets;
    }

	public function getRSS(){
        $rss = $this->dispatch(new FetchRSS());
        return $rss;
    }

}
