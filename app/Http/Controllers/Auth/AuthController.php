<?php namespace MEDoctors\Http\Controllers\Auth;

use Illuminate\Bus\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Commands\AddLogEntry;
use MEDoctors\Events\UserHasRegistered;
use MEDoctors\Models\User;
use MEDoctors\Models\UserLog;

use Carbon\Carbon;

class AuthController extends Controller {
    /**
     * Default login url path.
     *
     * @var string
     */
    protected $loginPath = 'login';

    /**
     * Default redirect url path.
     *
     * @var string
     */
    protected $redirectPath = '/';

    /**
     * Create AuthController instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth,Dispatcher $dispatcher,User $user)
    {
        $this->auth = $auth;
        $this->model = $user;
        $this->dispatcher = $dispatcher;
        $this->middleware('guest', ['except' => 'logout']);

        parent::__construct();
    }

    /**
     * Authenticate a user.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {

        if($this->checkUserAttempts($request)){
            session()->flash('message', 'Too many log-in attempts. Please try again after 15 minutes.');
            return redirect($this->loginPath())->withInput($request->only('username_email', 'remember'));
        }

        $credentials = $this->getCredentials($request);

        $this->validate($request, [
            'username_email' => 'required', 'password' => 'required',
        ]);

        if(!$this->checkVerified($credentials,$request))
            return redirect($this->loginPath())->withInput($request->only('username_email', 'remember'));

        $lockTime = $this->checkUserAttempts($request);

        if($lockTime){
            session()->flash('message',  $this->getTooManyAttemptsMessage($lockTime));
            return redirect($this->loginPath())->withInput($request->only('username_email', 'remember'));
        }

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            $user = $this->auth->user()->load('profile','roles');

            $this->dispatcher->dispatchFromArray('MEDoctors\Commands\AddLogEntry',
                [
                    'username' =>  $user->username,
                    'email'    =>  $user->email,
                    'log_type' =>  'Login'
                ]
            );

            return redirect()->intended($this->redirectPath());

        }

        session()->flash('message', $this->getFailedLoginMessage());
        return $this->failedLogin($request);
    }


    /**
     * Check if user account is verified
     * @param $request
     * @return $this
     *
     */
    public function checkVerified($credentials, $request){

        if(isset($credentials['email']))
            $user = $this->model->where('email', $credentials['email'])->first();
        else
            $user = $this->model->where('username',$credentials['username'])->first();

        if($user && $user->verified == 0){
            session()->flash('message', $this->getUnverifiedMessage());
            return false;
        }

        return true;
    }


    /**
     * Get user log-in attempts
     *
     * @param Request $request
     *
     * @return array
     */
    public function checkUserAttempts($request)
    {
        $credentials = $request->get('username_email');

        if(isset($credentials['email']))
            $attempts = UserLog::where('email', $credentials)->where('log_type','Login Attempt')->orderBy('created_at','DESC')->get();
        else
            $attempts = UserLog::where('username', $credentials)->where('log_type','Login Attempt')->orderBy('created_at','DESC')->get();

        $now = Carbon::now();
        $liveAttempts = [];

        foreach($attempts as $attempt)
            if($attempt->created_at->diff($now)->i < 15)
                $liveAttempts[] = $attempt;

        if(count($liveAttempts)>4){
            $locktime = 15 - $attempts[0]->created_at->diff($now)->i ;
            if($locktime > 0)
                return $locktime;
        }
        return false;

    }

    /**
     * Fire failed login.
     * @param $request
     * @return $this
     *
     */
    public function failedLogin($request){

        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\AddLogEntry',
            [
                'username' =>  $request->get('username_email'),
                'email'    =>  $request->get('username_email'),
                'log_type' =>  'Login Attempt'
            ]
        );

        return redirect($this->loginPath())
            ->withInput($request->only('username_email', 'remember'));
    }
    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/';
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath'))
        {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    private function getFailedLoginMessage()
    {
        return 'These credentials do not match our records.';
    }

    /**
     * Get the too many attempts login message.
     *
     * @return string
     */
    public function getTooManyAttemptsMessage($lockTime)
    {
        return 'Too many log-in attempts. Please try again after '.$lockTime.' minute(s).';
    }

    /**
     * Get the unverified login message.
     *
     * @return string
     */
    private function getUnverifiedMessage()
    {
        return "You Account has not been verified. Please check you email or click <a href='".route('resend-verify.create')."'>HERE</a> to resend verification e-mail.";
    }

    /**
     * Get username and password from the request.
     *
     * @param Request $request
     *
     * @return array
     */
    private function getCredentials(Request $request)
    {
        $username = $request->get('username_email');

        $field = $this->getUsernameField($username);

        return [
            $field     => $username,
            'password' => $request->get('password'),
            'verified' => true
        ];
    }

//    /**
//     * Get user log-in attempts
//     *
//     * @param Request $request
//     *
//     * @return array
//     */
//    private function checkUserAttempts($request)
//    {
//        $ip = $request->ip();
//        if(count(UserLog::where('ip',$ip)->get())>2)
//            return true;
//
//        return false;
//    }

    /**
     * Determine if value of username field is a username or email.
     *
     * @param string $username
     *
     * @return string
     */
    private function getUsernameField($username)
    {
        return filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    }

    /**
     * Logs out a user.
     *
     * @return void
     */
    public function logout()
    {

        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\AddLogEntry',
            [
                'username' =>  $this->user->username,
                'email'    =>  $this->user->email,
                'log_type' =>  'Logout'
            ]
        );

        $this->auth->logout();

        return redirect($this->loginPath());
    }

    public function getResend()
    {
        return view('auth.resend');
    }

    public function postResend(Request $request)
    {
        $user = User::where('email',$request->get('email'))->first();

        if($user){

            event(new UserHasRegistered($user));

            Session::flash('status', 'A verification email has been resent to '.$user->email.'. Please check your inbox.');
        }

        else
            Session::flash('error', $this->getFailedLoginMessage());

        return redirect()->back();
    }

}
