<?php namespace MEDoctors\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    /**
     * The authenticated user.
     *
     * @var \MEDoctors\Models\User
     */
    protected $user;

    public function __construct()
    {
        $this->user = \Auth::user();
        view()->share('authUser', $this->user);
    }

}
