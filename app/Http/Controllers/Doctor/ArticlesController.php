<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\ArticleRepository;
use MEDoctors\Repositories\Contracts\SurveyRepository;
use MEDoctors\Repositories\Contracts\ViewRepository;

use MEDoctors\Models\View;
use MEDoctors\Models\Article;
use MEDoctors\Models\Therapy;

use MEDoctors\Events\ArticleViewed;

use MEDoctors\Events\Event;
use Illuminate\Support\Str;

use MEDoctors\Services\PopularDiscussions;
use MEDoctors\Services\HomePageService;

class ArticlesController extends Controller {

	/**
	 * @var ArticleRepository
	 */
	protected $articles;
	
	public function __construct(
        ArticleRepository $articles,
        SurveyRepository $surveyRepository ,
        PopularDiscussions $popular,
        HomePageService $home
    )
	{
	    $this->home = $home;
	    $this->articles = $articles;
	    $this->popular = $popular;
	    $this->surveys = $surveyRepository;
	    parent::__construct();
	}
	
	/**
	 * Display the articles.
	 *
	 * @return Response
	 */
	public function index()
	{
        $active = 0;
        $poll = [];
		$featured = $this->articles->featured('doctor-featured-article');
		$latest = $this->articles->withChannel(2)->noUpcoming()->latestPaginated(5);
		$latestArticles = $this->articles->withChannel(2)->latestPaginated(5);
		$popularArticles = $this->articles->popular(5,2);
		$archives = $this->articles->withChannel(2)->archived();
        $surveys = $this->surveys->latestPaginated(9);
        $videos = $this->home->videos(2,10);

        if ($this->user)
        {
            $poll = $this->home->activePoll($this->user->id,'doctor-active-poll');
            $discussions = $this->popular->panelDiscussions();
            $diagnosis = $this->popular->diagnosisDiscussions();
        }

        $therapiesWithArticles = Therapy::with('articles')->get();
        $therapies = [];

        foreach($therapiesWithArticles as $key=>$therapy){
            if(count($therapy->articles))
                $therapies[$therapy->slug] = $therapy->name;
        }

        array_unshift($therapies , 'Show All');
        $articlesTherapy = $this->articles->topPerTherapy(5,2);

		return view(
            'doctor.pages.articles.index',
            compact('poll',
                'discussions',
                'diagnosis',
                'featured',
                'latest',
                'latestArticles',
                'popularArticles',
                'archives' ,
                'therapies' ,
                'active' ,
                'surveys',
                'articlesTherapy',
                'videos'
            ));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * 
	 * @return Response
	 */
	public function show($slug , Request $request)
	{
		$article = $this->articles->findBySlug($slug);

        $popular = $this->articles->popular(3,2);
        $latest = $this->articles->noUpcoming()->latestPaginated(10);
        $surveys = $this->surveys->latestPaginated(9);
        $popularArticles = $this->articles->popular(5,2);

        if ($this->user)
        {
            $poll = $this->home->activePoll($this->user->id,'doctor-active-poll');
            $discussions = $this->popular->panelDiscussions();
            $diagnosis = $this->popular->diagnosisDiscussions();
        }

        if($article)
        {
        	$userId = $this->user ? $this->user->id : 0;
            event(new ArticleViewed($article, $request->ip() , $userId));
        }

        $articlesTherapy = $this->articles->topPerTherapy(5,2);
        return view('doctor.pages.articles.show', compact('popularArticles','articlesTherapy', 'article', 'popular', 'latest','surveys','poll','diagnosis','discussions'));
	}

    /**
     * Display filtered articles.
     *
     * @param  string  $name
     *
     * @return Response
     */

    public function filter(Request $request)
    {
        $slug = $request->input('therapy');

        if($slug==="0")
            return redirect(url('doctor/articles/category/All'));

        return redirect(route('articles_show_filtered',$slug));
    }

    public function show_filtered($slug)
    {
        $active = $slug;

        $latest = $this->articles->noUpcoming()->latestPaginated(10);

        if($slug==="All"){
            $active_title = 'All';
            $articles = Article::where('channel_id', '=', 2)->orderBy('created_at','DESC')->paginate(20);
        }
        else {
            $active_title = Therapy::where('slug', $active)->first()->name;
            $articles = Article::whereHas('therapies', function($q) use ($slug)
            {
                $q->where('slug', '=', $slug)->where('channel_id', '=', 2);
            })->paginate(20);
        }

        $surveys = $this->surveys->latestPaginated(9);
        $popularArticles = $this->articles->popular(5,2);

        if ($this->user)
        {
            $poll = $this->home->activePoll($this->user->id,'doctor-active-poll');
            $discussions = $this->popular->panelDiscussions();
            $diagnosis = $this->popular->diagnosisDiscussions();
        }

        $therapiesWithArticles = Therapy::with('articles')->get();
        $therapies = [];

        foreach($therapiesWithArticles as $key=>$therapy){
            if(count($therapy->articles))
                $therapies[$therapy->slug] = $therapy->name;
        }
        
        array_unshift($therapies , 'All');

        $articlesTherapy = $this->articles->topPerTherapy(5,2);
        return view('doctor.pages.articles.filtered', compact('popularArticles','articlesTherapy','discussions','diagnosis','poll','surveys','articles', 'therapies' , 'latest' , 'active' , 'active_title'));
    }

}
