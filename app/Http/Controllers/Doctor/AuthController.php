<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Intervention\Image\Response;
use MEDoctors\Models\Country;
use MEDoctors\Models\Therapy;
use MEDoctors\Http\Controllers\Auth\AuthController as BaseAuthController;

class AuthController extends BaseAuthController {

    /**
     * Default login url path.
     *
     * @var string
     */
    protected $loginPath = 'doctor/login';

    /**
     * Default redirect url path.
     *
     * @var string
     */
    protected $redirectPath = 'doctor';
    
    /**
     * Display the login page.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $countries = Country::gccCountries()->lists('name', 'code');
        $therapies = Therapy::lists('name', 'id');

        return view('doctor.auth.index', compact('therapies', 'countries'));
    }


}