<?php namespace MEDoctors\Http\Controllers\Doctor;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Http\Request;

use MEDoctors\Models\Conversation;
use Illuminate\Support\Facades\Auth;

use MEDoctors\Models\ConversationMember;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Models\Message;
use MEDoctors\Models\MessageRead;

use MEDoctors\Repositories\Contracts\UserRepository;
class ConversationsController extends Controller {

    public  function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
        $this->middleware('auth');

        parent::__construct();
    }

    public function create()
    {
        $users = $this->userRepository->getUsersForMessage(2);
        $therapies = Therapy::lists('name','id');
        array_push($therapies, 'All');

        return view('doctor.pages.messages.create' , compact('users','therapies'));
    }

    public function reply($id)
    {
        $conversation = Conversation::where('id', $id)
            ->with(['messages.sender','author','members'])->get()[0];
        $users = $this->userRepository->getUsersForMessage(2);

        return view('doctor.pages.messages.create' , compact('conversation','users'));
    }

    public function storeConversation(Request $request)
    {
        $input = $request->input();
        $author_id = $this->user['id'];
        $receiver = $input['receiver'];
        $out = "";

        /** Get email from "Full Name [ Username ]" **/
        preg_match_all("/\[([^\]]*)\]/", $receiver, $out);

        if(isset($out[1])) {
            foreach ($out[1] as $rec) {
                $recepient_username = trim($rec);
                $recepient = User::where('username', $recepient_username)->first();

                $oonversation = Conversation::create([
                    'subject' => $input['subject'],
                    'author_id' => $author_id
                ]);

                $conversation_id = $oonversation->id;

                $oonversation->members()->sync([
                    ['user_id' => $recepient->id, 'conversation_id' => $conversation_id],
                    ['user_id' => $this->user->id, 'conversation_id' => $conversation_id]
                ]);

                $message = Message::create(array(
                    'sender_id' => $author_id,
                    'conversation_id' => $conversation_id,
                    'message' => $input['message']
                ));

                $message->reads()->create(array(
                    'user_id' => $this->user->id,
                    'message_id' => $message->id
                ));

                $data['sender_name'] = $this->user->fname . ' ' . $this->user->lname;
                $data['conversation_id'] = $conversation_id;
                $data['link'] = 'messages_single';
                $data['subject'] = $input['subject'];

                $this->dispatchFrom('MEDoctors\Commands\SendMail', $request, [
                    'template' => 'emails.notifications.private-message',
                    'sender_email' => 'mailman@middleeastdoctor.com',
                    'sender_name' => 'Middle East Doctor',
                    'recipient_email' => $recepient->email,
                    'subject' => 'New Private Message Received: ' . $input['subject'],
                    'message' => $data
                ]);
            }
            return redirect(route('messages_show'));

        } else
            return false;

    }

    public function show()
    {
        $conversations = [];

        $members = ConversationMember::where('user_id', $this->user->id)
            ->with(['conversation.messages.reads', 'conversation.members', 'conversation.author'])
            ->orderBy('joined_at', 'DESC')->paginate(10);
        $conversations = self::getUnreads($members);

        return view('doctor.pages.messages.show', compact('conversations','members'));
    }

    public function single($id)
    {
        $conversation = Conversation::where('id', $id)->with(['author','members','messages.reads'])->first();
        self::addRead($conversation);
        return view('doctor.pages.messages.single' , compact('conversation'));
    }

    public function trash($conversation_id)
    {
        ConversationMember::where('user_id', $this->user->id)->where('conversation_id', $conversation_id)->delete();
        return redirect(route('messages_show'));
    }

    public function trashMultiple(Request $request)
    {

        $ids = $request->input('trash_ids');
        if($ids){
            foreach(json_decode($ids) as $id){
                ConversationMember::where('user_id', $this->user->id)->where('conversation_id', $id)->delete();
            }
        }
        return redirect(route('messages_show'));
    }

    public function trashed()
    {

        $members = ConversationMember::onlyTrashed()->where('user_id', $this->user->id)
            ->with(['conversation','conversation.members', 'conversation.author'])
            ->orderBy('joined_at', 'DESC')->paginate(10);

        $conversations = self::getUnreads($members);

        return view('doctor.pages.messages.trash' , compact('conversations','members'));
    }

    public function trashed_single($id)
    {
        $conversation = Conversation::where('id', $id)->with(['author','members','messages'])->first();

        return view('doctor.pages.messages.single_trash' , compact('conversation'));
    }

    public function restore($conversation_id)
    {
        ConversationMember::where('user_id', $this->user->id)->where('conversation_id', $conversation_id)->restore();
        return redirect(route('messages_show'));
    }

    public function restoreMultiple(Request $request)
    {
        $ids = $request->input('restore_ids');
        if($ids){
            foreach(json_decode($ids) as $id){
                ConversationMember::where('user_id', $this->user->id)->where('conversation_id', $id)->restore();
            }
        }
        return redirect(route('messages_show'));
    }

    public function archive(){
        return view('doctor.pages.messages.archive');
    }


    public function addRead($conversation)
    {
        foreach($conversation->messages as $message)
        {
            $read_data = array('user_id' => $this->user->id , 'message_id' => $message->id );

            if(count(
                    $message->reads()
                        ->where('user_id', $this->user->id)
                        ->where('message_id', $message->id)
                        ->get()
                ) < 1
            )

                $message->reads()->create($read_data);
        }
    }

    public function getUnreads($members)
    {
        $conversations = [];

        foreach ($members as $index=>$member)
        {
            foreach ($member->conversation->messages as $message)
            {
                $read = $message->reads()
                    ->where('user_id', $this->user->id)
                    ->where('message_id', $message->id)
                    ->get();

                if (count($read) < 1)
                    $conversations[$index]['unread_messages'][] = $message;
            }
            $conversations[$index]['conversation'] = $member->conversation;
        }

        return $conversations;
    }

    public function getUsersFromTherapy(Request $request){
        $users = $this->userRepository->getUsersForMessageTherapy(2,$request->input('therapy_id'));

        echo json_encode($users);
    }

}