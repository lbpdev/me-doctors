<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\PostComment;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

class DiscussionsCommentsController extends Controller {

    /**
     * Create the controller instance.
     */
    public function __construct(DiscussionRepository $discussionRepository)
    {
        $this->discussions = $discussionRepository;
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Post a new comment for the discussion.
     *
     * @param  int $discussionId
     *
     * @return Response
     */

    public function store($discussionId, Request $request)
    {
        $this->dispatch(new PostComment(
            $this->user->id,
            $discussionId,
            $request->get('content'),
            $request->file('attachment')
        ));

        Session::flash('message','Thank you for your feedback. Your comment has been queued and will be visible once approved by the moderators.');
        return redirect()->back();
    }


    /**
     * Get next set of records visa AJAX.
     *
     * @return Response
     */
    public function nextComments(Request $request)
    {
        $doctors = $this->discussions->nextComments($request->get('id'), $request->get('offset'), $request->get('limit'));

        echo json_encode($doctors);
    }

}
