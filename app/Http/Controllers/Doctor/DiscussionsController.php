<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Models\Discussion;
use MEDoctors\Models\Therapy;

use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Http\Requests\StartPanelDiscussionRequest;

class DiscussionsController extends Controller {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

	public function __construct(DiscussionRepository $discussions)
    {
        $this->discussions = $discussions;
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Display the latest panel discussions.
     *
     * @param Request $request
     * 
     * @return Response
     */
	public function index(Request $request)
    {
        $active = 0;
        $therapies = Therapy::lists('name','slug');
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->latestPanelPaginated($request->get('items', 20));
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();
        array_unshift($therapies , 'All');

        return view('doctor.pages.discussions.panel.index', compact('discussions','diagnosis','active','therapies','sort','sort_types'));
    }

    /**
     * Display the discussion based on the slug provided.
     *
     * @param  string $slug
     *
     * @return Response
     */
    public function show($slug, Request $request)
    {
        $discussion = $this->discussions->findBySlug($slug);
        $panels = $this->discussions->latestPanelPaginated($request->get('items', 6));
        $comments = $discussion->comments()->where('status',1)->orderBy('created_at','desc')->take(3)->get();

        return view('doctor.pages.discussions.panel.show', compact('discussion','panels','comments'));
    }

    public function create()
    {
        if(Session::get('role')==1) {
            $categories = $this->getCategoriesList('discussions')['therapies'];
            return view('doctor.pages.pages.discussions.add', compact('categories'));
        } else {
            return redirect('doctor.discussions.index');
        }
    }


    public function store(Request $request)
    {
        $this->dispatch(new StartPanelDiscussion(
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            0,
            $request->file('attachment')
        // $this->getFileFromRequest($request, 'attachment')
        ));

        return redirect(route('doctor.panel.index'));
    }

    public static function destroy($id , $request)
    {
        $discussion = Discussion::where('id',$id)->delete();
        return $request->input();
    }

//    public function show_popular()
//    {
//        $discussions = $this->discussions->popularPanelDiscussions(10);
//        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();
//        return view('doctor.pages.discussions.panel.index', compact('discussions','diagnosis'));
//    }
    public function filter(Request $request)
    {
        $id = $request->input('therapy');
        $sort = $request->input('sort');

        if(empty($id)&&$sort=='latest')
            return redirect(route('doctor.discussions.index'));
        elseif(empty($id)&&$sort=='popular')
            return redirect(route('discussions_popular'));

        if($sort=='latest')
            return redirect(route('discussions_show_filtered_latest',$id));

        elseif($sort=='popular')
            return redirect(route('discussions_show_filtered_popular',$id));
    }

    public function show_filtered_latest($id)
    {
        $therapy_id = Therapy::where('slug', $id)->first()->id;
        $active = $id;
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = Discussion::where('therapy_id', $therapy_id)->where('channel_id', 1)->orderBy('created_at','desc')->paginate(20);
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',2)->latest()->take(9)->get();
        return view('doctor.pages.discussions.panel.filtered', compact('panels','discussions','diagnosis', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_popular()
    {
        $active = 0;
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularPanelDiscussions(20);
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',2)->latest()->take(9)->get();
        return view('doctor.pages.discussions.panel.filtered', compact('panels','diagnosis', 'discussions', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_filtered_popular($id)
    {
        $active = Therapy::where('slug', $id)->first();
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularPanelDiscussionsWithFilter($active->id,20);
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',2)->latest()->take(9)->get();
        return view('doctor.pages.discussions.panel.filtered', compact('panels','discussions', 'diagnosis', 'therapies' , 'active' , 'sort','sort_types'));
    }

}
