<?php namespace MEDoctors\Http\Controllers\Doctor;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Event;
use MEDoctors\Models\Upload;
use MEDoctors\Repositories\Contracts\EventRepository;

class EventsController extends Controller {

    function __construct(EventRepository $eventRepository){
        $this->events = $eventRepository;

        parent::__construct();
    }

    public static function show($slug){
        $event = Event::where('slug',$slug)->first();
        return view('doctor.pages.events.show' , compact('event'));
    }


	/**
     * Display a listing of upcoming events.
     *
     * @return Response
     */
	public function upcoming()
    {
        $events = $this->events->paginatedUpcoming(10);
        return view('doctor.pages.events.index', compact('events'));
    }

	/**
     * Display a listing past events.
     *
     * @return Response
     */
	public function past()
    {
        $events = $this->events->paginatedPast(10);
        $past = true;
        return view('doctor.pages.events.index', compact('events','past'));
    }

}
