<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use MEDoctors\Models\Conversation;
use MEDoctors\Models\ConversationMember;
use MEDoctors\Models\Message;
use MEDoctors\Models\User;

use Illuminate\Http\Request;
use MEDoctors\Repositories\Contracts\MessagesRepository;

class MessagesController extends Controller {

    public function __construct(MessagesRepository $messagesRepository)
    {
        $this->messages = $messagesRepository;
        $this->middleware('auth');
        parent::__construct();
    }


    public function index(){
        $messages = Message::where('sender_id', Auth::id())
            ->with(['conversation.members','sender'])->orderBy('created_at' , 'DESC')->get();
        return view('doctor.pages.messages.sent', compact('messages'));
    }

    public function storeMessage(Request $request)
    {
        $this->messages->store($request,'messages_single');
        return redirect(route('messages_single',$request->input('conversation_id')));
    }

    public function single($id)
    {
        $message = Message::where('id', $id)
            ->with(['conversation','sender'])->first();

        return view('doctor.pages.messages.single_sent' , compact('message'));
    }

    public function trash($id)
    {
        Message::where('id', $id)->delete();
        return redirect('messages/sent');
    }

    public function trashMultiple(Request $request)
    {
        $ids = $request->input('trash_ids');
        if($ids){
            foreach(json_decode($ids) as $id){
                Message::where('id', $id)->delete();
            }
        }
        return redirect(route('messages_sent'));
    }

    public function search(Request $request)
    {

        $messages = Message::with('conversation')->whereHas('conversation.members', function($q)
        {
            $q->where('user_id', '=', $this->user->id);

        })->search($request->input('keyword'))->get();

        $conversations = Conversation::whereHas('members', function($q)
        {
            $q->where('user_id', '=', $this->user->id);

        })->search($request->input('keyword'))->get();

        return view('doctor.pages.messages.search_results', compact('messages','conversations'));
    }

}
