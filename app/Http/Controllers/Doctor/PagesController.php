<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use MEDoctors\Commands\FetchTweets;
use Illuminate\Contracts\Auth\Guard;
use MEDoctors\Models\AdPending;
use MEDoctors\Models\PremiumService;
use MEDoctors\Services\Feeds\NewsFeed;
use Illuminate\Support\Facades\Session;
use MEDoctors\Services\HomePageService;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Services\PopularDiscussions;

use MEDoctors\Models\Survey;

class PagesController extends Controller {

    /**
     * @var HomePageService
     */
    protected $home;

    /**
     * @var PopularDiscussions
     */
    protected $popular;

    /**
     * @var NewsFeed
     */
    protected $newsFeed;

    public function __construct(HomePageService $home, PopularDiscussions $popular, NewsFeed $newsFeed)
    {
        $this->home = $home;
        $this->popular = $popular;
        $this->newsFeed = $newsFeed;

        parent::__construct();
    }

    /**
     * The home page.
     *
     * @param  Guard $auth
     *
     * @return Response
     */
	public function index(Guard $auth)
	{
        $latestArticles = $this->home->latestArticles(2);
        $featuredArticle = $this->home->featuredArticle('doctor-featured-article');
        $popularArticles = $this->home->popularArticles(2,2);
        $latestSurveys = $this->home->latestSurveys(2,2);

        $events = $this->home->upcomingEvents();
        $poll = $this->home->activePoll($auth->id(),'doctor-active-poll');


        $news = $this->home->latestNews(5,2);
//        $news = $this->newsFeed->medical();
        $videos = $this->home->videos(2,4);
        $discussions = $this->popular->panelDiscussions();
        $diagnosis = $this->popular->diagnosisDiscussions();

        return view('doctor.pages.home' , compact(
            'latestSurveys',
            'featuredArticle',
            'latestArticles',
            'popularArticles',
            'events',
            'discussions',
            'diagnosis',
            'poll',
            'news',
            'videos'
        ));
    }

    public function directHome(){
        return Redirect::to('/doctor', 301);
    }

    /**
     * The landing page.
     *
     *
     * @return Response
     */
	public function soon()
	{
        return view('soon');
    }

    /**
     * The landing page.
     *
     *
     * @return Response
     */
	public function landing()
	{
        return view('landing');
    }

    /**
     * Show the pages requested by the user.
     *
     * @param  string $page
     *
     * @return Response
     */
    public function show($page)
    {
        if (method_exists($this, $page))
        {
            return $this->$page();
        }

        if ( ! view()->exists($view = "doctor.pages.{$page}")) {
            abort(404);
        }

        return view($view);
    }

}
