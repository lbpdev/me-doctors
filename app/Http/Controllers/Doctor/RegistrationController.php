<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Models\Country;
use MEDoctors\Models\Therapy;
use MEDoctors\Events\UserHasRegistered;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Services\Registration\DoctorRegistrar;
use MEDoctors\Exceptions\InvalidDoctorEmailException;
use MEDoctors\Repositories\Contracts\CountryRepository;

class RegistrationController extends Controller {

    /**
     * @var DoctorRegistrar
     */
    protected $registrar;
    
    /**
     * @var CountryRepository
     */
    protected $country;

    /**
     * Create RegistrationController instance.
     *
     * @param DoctorRegistrar $registrar
     * @param CountryRepository $country
     */
    public function __construct(DoctorRegistrar $registrar, CountryRepository $country)
    {
        $this->registrar = $registrar;
        $this->country = $country;

        parent::__construct();
    }
    
    /**
     * Show doctor credentials form.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function withCredentials(Request $request)
    {
        $user = session('temp-registration-user');
        $countries = Country::gccCountries()->lists('name', 'code');
        $therapies = Therapy::lists('name', 'id');

        // remove old user registration from the session.
        session()->forget('temp-registration-user');

        return view('doctor.registration.with_credentials', compact('user', 'countries', 'therapies'));
    }

    /**
     * Register a new user.
     *
     * @param  Request $request
     *
     * @return Response
     */
	public function register(Request $request)
    {
        // validate country if it belongs to GCC or not.
        if ( ! $this->country->isGcc($request->get('country')))
        {
            session()->flash('registration.error', trans('registration.invalid.country'));

            return redirect($this->loginPath())->withInput();
        }

        // validate the data for registration.
        $this->validateRequest($request);

        if ($this->requestHasCredentials($request))
        {
            return $this->registerWithCredentials($request);
        }

        return $this->registerWithoutCredentials($request);
    }

     /**
     * Register the doctor but send it to verification queue.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    private function registerWithCredentials(Request $request)
    {
        $user = $this->registrar->createWithCredentials(
            $request->all(), $this->credentialsFromRequest($request)
        );

//        event(new DoctorWithCredentialsHasRegistered($user));

        session()->flash('registration.success', trans('registration.with-credentials-success'));

        return redirect($this->loginPath());
    }

    /**
     * Register the doctor without credentials provided.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    private function registerWithoutCredentials(Request $request)
    {

        $data = $request->all();
        $education = $request->only('education');
        $certifications = $request->only('certifications');
        $phone_numbers = $request->only('phone_numbers');

        try
        {
            $user = $this->registrar->create($data);

            $data['user'] = $user;

            session()->flash('registration.success', 'Thank you for registering to MED. You may now log-in using your account.');

            return redirect($this->loginPath());

        } catch (InvalidDoctorEmailException $e)
        {
            session()->flash('registration', [
                'error' => $e->getMessage(), 
                'invalid_doctor_email' => true
            ]);

            session(['temp-registration-user' => $data]);

            return redirect($this->loginPath())->withInput();
        }
    }

    /**
     * Extract doctor credentials from request.
     *
     * @param  Request $request
     *
     * @return array
     */
    private function credentialsFromRequest(Request $request)
    {
        $credentials = $request->only('license_number', 'phone_number', 'place_of_employment');

        $credentials['other_files'] = $this->getFileFromRequest($request, 'other_files');
        $credentials['license_files'] = $this->getFileFromRequest($request, 'license_files');
        $credentials['photo_files'] = $this->getFileFromRequest($request, 'photo_files');

        return $credentials;
    }

    /**
     * Get the file or files from the request
     *
     * @param  Request $request
     * @param  string  $key
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile|array|null
     */
    private function getFileFromRequest($request, $key)
    {
        return $request->hasFile($key) ? $request->file($key) : null;
    }

    /**
     * Determine if the request has credentials passed.
     *
     * @param  Request $request
     *
     * @return boolean
     */
    private function requestHasCredentials(Request $request)
    {
        return $request->hasFile('license_files') ||
               $request->hasFile('photo_files') ||
               $request->get('phone_number') || 
               $request->get('place_of_employment');
    }

    /**
     * Confirm user registration from the email.
     *
     * @param  string $token
     *
     * @return Reponse
     */
    public function confirmEmail($token)
    {
        $this->registrar->confirmUserEmail($token);

        session()->flash('registration.success', trans('registration.confirmed'));

        return redirect($this->loginPath());
    }

    /**
     * Validate registration request.
     *
     * @param  Request  $request
     *
     * @return void
     */
    private function validateRequest(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
    }

    /**
     * The doctor's login url path.
     *
     * @return string
     */
    private function loginPath()
    {
        return route('doctor.login');
    }

}
