<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\SurveyAnswers;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Models\Survey;
use MEDoctors\Models\Therapy;
use MEDoctors\Repositories\Contracts\SurveyRepository;

class SurveysController extends Controller {


    /*
    * @var SurveyRepository
    */
    protected $survey;

    public function __construct(SurveyRepository $survey)
    {
        $this->survey = $survey;
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Display the surveys.
     *
     * @param  Request $request
     *
     * @return Response
     */

    public function index(Request $request)
    {
        $surveys = $this->survey->withChannel(2)->latestPaginated(10);
        $therapies = Therapy::lists('name','slug');

        $sort_types = [ 'DESC' => 'Newest' , 'ASC' => 'Oldest' ];
        array_unshift($therapies , 'Show All');
        return view('doctor.pages.surveys.index', compact('surveys','therapies','sort_types'));
    }

    /**
     * Save the survey answers.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        extract($request->only('survey_id', 'answers'));

        $this->dispatch(new SurveyAnswers($this->user, $survey_id, $answers));

        Session::flash('message','Thank you for your feedback. You can come back and change your answers anytime.');
        return redirect()->back();
    }

    /**
     * Single view of a survey.
     *
     * @param  string $slug
     * @param  Request $request
     *
     * @return Response
     */

    public function show($slug, Request $request)
    {
        $selected = Survey::with('items.choices','therapies')->where('slug',$slug)->first();

        if(count($selected->therapies))
            $surveys = Survey::where('channel_id',2)->whereHas('therapies', function ($q) use ($selected) {
                $q->where('slug', $selected->therapies[0]->slug);
            })->paginate(10);
        else
            $surveys = Survey::where('channel_id',2)->paginate(10);

        $entry_ids = $this->survey->userEntriesBySurvey($this->user, $selected);

        $therapies = Therapy::lists('name','slug');
        $sort_types = [ 'DESC' => 'Newest' , 'ASC' => 'Oldest' ];
        array_unshift($therapies , 'Show All');

        return view('doctor.pages.surveys.show', compact(
            'therapies' ,
            'sort_types',
            'sort_types',
            'surveys',
            'selected',
            'entry_ids'));
    }

    /**
     * Filter and list surveys.
     *
     * @return Response
     */
    public function filter(Request $request)
    {
        $surveys = $this->survey
            ->withChannel(2)
            ->filterTherapy($request->get('therapy'))
            ->sortDate($request->get('sort'))
            ->latestPaginated(10);

        $therapies = Therapy::lists('name','slug');
        $sort_types = [ 'DESC' => 'Newest' , 'ASC' => 'Oldest' ];
        array_unshift($therapies , 'Show All');

        return view('doctor.pages.surveys.index', compact('sort_types','therapies','surveys'));
    }
}
