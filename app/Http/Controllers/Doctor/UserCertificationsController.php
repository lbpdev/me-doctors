<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\UpdateUserCertifications;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\User;

class UserCertificationsController extends Controller {

    /**
     * Create UserEducationController instance.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function update(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);
        $certifications= $this->userRepository->filterCertifications($formFields['certifications']);

        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

        $this->dispatch(new UpdateUserCertifications($user, $certifications , true));

        return redirect()->back();
    }


    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function updateIE(Request $request)
    {
        $certifications= $this->userRepository->filterCertifications($request->get('certifications'));

        $this->dispatch(new UpdateUserCertifications($this->user, $certifications , false));

        return redirect()->back();
    }



}
