<?php namespace MEDoctors\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\UpdateUserLanguages;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Models\User;

class UserLanguagesController extends Controller {

    /**
     * Create UserEducationController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');

        parent::__construct();
    }

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function update(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);
        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

        $this->dispatch(new UpdateUserLanguages($user, $formFields['languages'] , true));

        return redirect()->back();
    }

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */

    public function updateIE(Request $request)
    {
        $this->dispatch(new UpdateUserLanguages($this->user, $request->get('languages') , false));

        return redirect()->back();
    }


}
