<?php namespace MEDoctors\Http\Controllers\Doctor;

use hisorange\BrowserDetect\Provider\BrowserDetectService;
use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Models\Language;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Models\User;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\Therapy;

use MEDoctors\Services\TC;
use Illuminate\Support\Facades\Session;

use MEDoctors\Services\Twocheckout\Api\TwocheckoutAccount;
use MEDoctors\Services\Twocheckout\Api\Twocheckout_Payment;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutApi;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutSale;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutProduct;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutCoupon;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutOption;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutUtil;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutError;
use MEDoctors\Services\Twocheckout\TwocheckoutReturn;
use MEDoctors\Services\Twocheckout\TwocheckoutNotification;
use MEDoctors\Services\Twocheckout\TwocheckoutCharge;
use MEDoctors\Services\Twocheckout\TwocheckoutMessage;

use MEDoctors\Events\UserViewed;

class UsersController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;
    
    /**
     * Create UsersController instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository , HospitalRepository $hospitalRepository)
    {
        $this->userRepository = $userRepository;
        $this->hospitalRepository = $hospitalRepository;
        $this->user = \Auth::user();
        $this->middleware('auth');

        parent::__construct();
    }
        
    /**
     * Show the user profile.
     *
     * @param string $username
     * 
     * @return Response
     */
    public function show($username , Request $request)
    {

        $user = $this->userRepository->withTraits()->getByUsername($username);

        $user->load('specialties',
            'hospitals.location',
            'hospitals.schedules',
            'educations',
            'languages',
            'achievements',
            'publications',
            'certifications',
            'kol',
            'services',
            'contacts');
        $languages = Language::orderBy('name')->select('name', 'id')->get()->toArray();
        $hospitals = $this->hospitalRepository->generateAutoLoadData(Hospital::with('location')->get());
        $this->hospitalRepository->generateHospitalSchedules($user);
        $user->languages = $user->languages->toArray();
        $agent = \BrowserDetect::detect()->browserFamily;

        if($user)
        {
            $viewerId = $this->user ? $this->user->id : 0;
            event(new UserViewed($user, $request->ip() , $viewerId));
        }

        $waiting_times = $this->generateWaitingTimes();

        $current_user_ratings = $this->userRepository->getCurrentUserRating($user);

        return view('doctor.pages.users.show', compact('user','languages','agent','hospitals','current_user_ratings','waiting_times'));
    }

    public function generateWaitingTimes(){
        $data = array(
            '5' => '5 Minutes',
            '10' => '10 Minutes',
            '30' => '30 Minutes',
            '60' => '1 hour',
            '120' => '2 hours',
            '180' => '3 hours',
            '360' => '6 hours',
            '720' => '12 hours',
        );

        return $data;
    }

    /**
     * Update the profile of the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $this->dispatchFrom('MEDoctors\Commands\DoctorProfileUpdate', $request, [
            'user' => $this->user
        ]);
    }

    public function edit_profile($username)
    {
        $user = $this->userRepository->getByUsername($username);
        $therapies = Therapy::orderBy('name')->select('name', 'id')->get()->toArray();
        $user->load('profile','contacts');

        return view('doctor.pages.users.profile.edit', compact('user','therapies'));
    }

    public function kolize()
    {
        $this->user->kol()->create([]);

        return redirect()->back();
    }


    public function update_profile(Request $request)
    {
        $input = $request->all();

        foreach($input['therapies'] as $therapy)
            $therapies[] = array('therapy_id'=>$therapy);

        $file = $request->file('photo');
        $account = $request->only('fname','lname','email','password','designation','username');


        $user = $this->user;
        if(isset($input['user_id']))
            $user = User::where('id',$input['user_id'])->first();


        $this->dispatchFrom('MEDoctors\Commands\UpdateUser', $request, [
            'user'      => $user ,
            'account'   => $account,
            'therapies' => $therapies
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserProfile', $request, [
            'user'    => $this->user ,
            'profile' => $input,
            'file'    => $file,
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserContacts', $request, [
            'user'    => $user,
            'contacts' => $input['phone_number'],
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserAvatar', $request, [
            'user'   => $user ,
            'file'   => $file,
            'remove' => isset($input['remove_thumb']) ? true : false
        ]);

        Session::flash('message', 'Profile has been updated successfully.');
        return redirect(route('edit_personal_profile',$this->user->username));
    }

    public function purchasePremium(Request $request)
    {

        TC::privateKey('49E5F06F-EE89-4396-ABB3-01E29B413CAF');
        TC::sellerId('901301595');
        TC::sandbox(true);  #Uncomment to use Sandbox
        TC::verifySSL(false);

//      sellerId: "901301595",
//      publishableKey: "C1A0F6B1-5CAF-4175-A012-B07962549E83",
        dd($request->input('token'));
        try {
            $charge = TwocheckoutCharge::auth(array(
                "merchantOrderId" => "123",
                "token" => $request->input('token'),
                "currency" => 'USD',
                "lineItems" => array(
                    [
                        "name" => 'Product Nae',
                        "recurrence" => '1 Month',
                        "price" => '10',
                        "duration" => '1 Year',
                        "quantity" => '3',
                        "tangible" => 'N',
                        "type" => 'product'
                    ]
                ), 'array'));

            if ($charge['response']['responseCode'] == 'APPROVED') {
                dd($charge);
                echo "Thanks for your Order!";
            }
        } catch (TwocheckoutError $e) {
            $e->getMessage();
            dd($e);
        }

//        return redirect()->back();
    }
}
