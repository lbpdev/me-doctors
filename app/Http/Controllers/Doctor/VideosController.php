<?php namespace MEDoctors\Http\Controllers\Doctor;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Video;
use MEDoctors\Models\Channel;

class VideosController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$videos = Video::orderBy('created_at','DESC')->paginate(10);
        return view('doctor.pages.videos.index', compact('videos'));
	}

}
