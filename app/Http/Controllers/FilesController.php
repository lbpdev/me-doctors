<?php namespace MEDoctors\Http\Controllers;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Models\Upload;
use MEDoctors\Http\Controllers\Controller;

class FilesController extends Controller {

    /**
     * Download a file.
     *
     * @param  int $uploadId
     *
     * @return Response
     */
	public function download($uploadId)
    {
        $file = Upload::where('id' , $uploadId)->firstOrFail();

        return response()->download(public_path($file->url));
    }

}
