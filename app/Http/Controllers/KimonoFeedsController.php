<?php namespace MEDoctors\Http\Controllers;

use Carbon\Carbon;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Repositories\Contracts\DummyNewsFeedRepository;

use MEDoctors\Repositories\Contracts\NewsFeedRepository;
use MEDoctors\Services\Feeds\KimonoNewsFetcher;

use MEDoctors\Models\DummyNewsFeed;

class KimonoFeedsController extends Controller {

    public function  __construct(
        DummyNewsFeedRepository $newsFeedRepository,
        KimonoNewsFetcher $kimonoNewsFetcher
    )
    {
        $this->newsFetcher = $kimonoNewsFetcher;
        $this->news = $newsFeedRepository;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $requests)
	{
        $this->newsFetcher->fetchPost($requests->input());
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function get()
	{
        $request = "https://www.kimonolabs.com/api/cd22cjuu?apikey=z5k1LKk9H3noOY1vtknWftQDGKOJ4POj&kimbypage=1";
        $response = file_get_contents($request);
        $results = json_decode($response, TRUE);

        echo '<pre>';

        $data = [];

//        print_r($results);

        $index = 0;
        foreach($results['results'] as $page){

            if(isset($page['body'])){

                isset($page['url']) ? $data[$index]['url'] = $page['url'] : null;

                isset($page['head'][0]['title']) ? $data[$index]['title'] = $page['head'][0]['title'] : null;

                isset($page['head'][0]['sub-title']) ? $data[$index]['subtitle'] = $page['head'][0]['sub-title'] : null;

                isset($page['head'][0]['image']) ? $data[$index]['photo'] = $page['head'][0]['image']['src'] : null;

                if(isset($page['head'][0]['date_posted'])) {
                    $date = "";
                    $dateStringComponents = explode(' ',$page['head'][0]['date_posted']);
                    foreach($dateStringComponents as $component){
                        if(strtotime($component))
                            $date .= " ".$component;

                    }
                    $data[$index]['date_posted'] = strtotime($date) ;

                }

                $data[$index]['content'] = '<p>';
                foreach($page['body'] as $lines=>$body){

                    if($lines+1==count($page['body']))
                        $data[$index]['content'] .= '</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>';
                    elseif($lines%5==1)
                        $data[$index]['content'] .= '</p><p>&nbsp;</p><p>';

                    $data[$index]['content'] .= $body['contents'];
                }


                $data[$index]['content'] .= '<p>&nbsp;</p>'.'<b>Copyright Owners</b>: <a href="'.$page['url'].'">'.$page['url'].'</a>';

                $index++;
            }

        }

//        print_r($data);


        $newData = 0;
        $existingData = 0;
        $skipped = 0;
        foreach($data as $news){
            $news_exists = NewsFeed::where('url',$news['url'])->first();

            if(!$news_exists && $newData < 5){
                $newData++;
                $news['channel_id'] = 2;
                print_r($news);
//                $return_data[] = $this->news->store($news,null);
            } elseif($news_exists) {
                $existingData++;
            } else {
                $skipped++;
            }
        }


        return $newData . ' Added. ' . $existingData . ' already exists. '.$skipped.' skipped';


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
