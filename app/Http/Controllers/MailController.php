<?php namespace MEDoctors\Http\Controllers;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\MailRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

class MailController extends Controller {

    public function __construct(MailRepository $mailRepository,UserRepository $userRepository){
        $this->mail = $mailRepository;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    public function send_mail(Request $request){

        $data = $request->input();

        if(
            strlen($data['subject']) != mb_strlen($data['subject'], 'utf-8') ||
            preg_match('/[bcdfghjklmnpqrstvwxz]{5}/i', $data['subject'])==1 ||
            preg_match('/[aeiou]{5}/i', $data['subject'])==1 ||
            preg_match('/[bcdfghjklmnpqrstvwxz]{5}/i', $data['content'])==1 ||
            preg_match('/[aeiou]{5}/i', $data['email'])==1 ||
            preg_match('/[bcdfghjklmnpqrstvwxz]{5}/i', $data['email'])==1 ||
            strlen($data['content'])<10 ||
            preg_match('/[aeiou]{5}/i', $data['content'])==1 ||
            count(explode(' ', $data['content'])) < 5
        )
            return redirect()->back();

        $content = explode(' ',$data['content']);


        foreach($content as $word)
            if(preg_match_all('/[aeiou]/i',$word,$matches)>10 || preg_match_all('/[bcdfghjklmnpqrstvwxz]/i',$word,$matches)>15)
                return redirect()->back();

        if(isset($data['url']) && $data['url']==''){
            $this->dispatchFrom('MEDoctors\Commands\SendMail', $request, [
                'template' => 'emails.contact.inquiry',
                'sender_email' => 'gene@leadingbrands.me',
                'sender_name'  => 'Middle East Doctor',
                'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
                'subject'  => 'MED - Contact Form Submission',
                'message'  => $data

            ]);
        }

        Session::flash('message', 'Thank you for the feedback. We will contact you shortly regarding your inquiry.');
        return redirect()->back();
    }

    public function message_notification(Request $request){

        $data = $request->input();

        $this->dispatchFrom('MEDoctors\Commands\SendMail', $request, [
            'template' => 'emails.notifications.private-message',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $data['recipient_email'],
            'subject'  => 'New Private Message Received',
            'message'  => $data['message']
        ]);

        return redirect()->back();
    }

    public function sendReferral(Request $request){
        $data = $request->input();

        $data['sender_email'] = $this->user->email;
        $data['sender_name'] = $this->user->fullName;

        $this->dispatchFrom('MEDoctors\Commands\SendMail', $request, [
            'template' => 'emails.notifications.referral',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $data['email'],
            'subject'  => 'Middle East Doctor Membership Invitation',
            'message'  => $data
        ]);

        $this->userRepository->createReferralEmail($this->user->id,$data['email']);

        echo $data['email'];
    }

}
