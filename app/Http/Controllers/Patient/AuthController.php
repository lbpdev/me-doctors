<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use Intervention\Image\Response;
use Illuminate\Support\Facades\Session;

use MEDoctors\Models\Country;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;

use MEDoctors\Events\UserHasRegistered;

use MEDoctors\Http\Controllers\Auth\AuthController as BaseAuthController;

class AuthController extends BaseAuthController {

    /**
     * Default login url path.
     *
     * @var string
     */
    protected $loginPath = 'patient/login';

    /**
     * Default redirect url path.
     *
     * @var string
     */
    protected $redirectPath = 'patient';

    /**
     * Display the login page.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('patient.auth.index');
    }


    /**
     * Authenticate a user.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {

        $credentials = $this->getCredentials($request);

        $this->validate($request, [
            'username_email' => 'required', 'password' => 'required',
        ]);

        $lockTime = parent::checkUserAttempts($request);

        if($lockTime){
            session()->flash('message',  parent::getTooManyAttemptsMessage($lockTime));
            return redirect($this->loginPath())->withInput($request->only('username_email', 'remember'));
        }

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            $user = $this->auth->user()->load('profile','roles');

            $this->dispatcher->dispatchFromArray('MEDoctors\Commands\AddLogEntry',
                [
                    'username' =>  $user->username,
                    'email'    =>  $user->email,
                    'log_type' =>  'Login'
                ]
            );

            return redirect(route('patient.home'));
        }

        session()->flash('message', $this->getFailedLoginMessage());

        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\AddLogEntry',
            [
                'username' =>  $request->get('username_email'),
                'email'    =>  $request->get('username_email'),
                'log_type' =>  'Login Attempt'
            ]
        );

        return redirect($this->loginPath())
            ->withInput($request->only('username_email', 'remember'));
    }


    /**
     * Get username and password from the request.
     *
     * @param Request $request
     *
     * @return array
     */
    private function getCredentials(Request $request)
    {
        $username = $request->get('username_email');

        $field = $this->getUsernameField($username);

        return [
            $field     => $username,
            'password' => $request->get('password'),
        ];
    }


    /**
     * Determine if value of username field is a username or email.
     *
     * @param string $username
     *
     * @return string
     */
    private function getUsernameField($username)
    {
        return filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    }


    /**
     * Get the failed login message.
     *
     * @return string
     */
    private function getFailedLoginMessage()
    {
        return 'These credentials do not match our records.';
    }


    public function getResend()
    {
        return view('patient.auth.resend');
    }

    public function postResend(Request $request)
    {
        $user = User::where('email',$request->get('email'))->
                      where('id',$request->get('user_id'))->first();

        $result = [];

        if($user){

            $user->token = str_random(30);
            $user->save();

            event(new UserHasRegistered($user));

            $result['result'] = 1;
            $result['text'] = 'A verification email has been resent to '.$user->email.'. <br>Please check your inbox.';
        }
        else{
            $result['result'] = 0;
            $result['text'] = 'This is not the email address you used to register.';
        }

        return response()->json($result);
    }
}