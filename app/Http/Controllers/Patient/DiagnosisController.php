<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Commands\Admin\Discussions\StartDiagnosisDiscussion;
use MEDoctors\Commands\Admin\Discussions\UpdateDiagnosisDiscussion;

use MEDoctors\Models\Discussion;
use MEDoctors\Models\Therapy;
use MEDoctors\Repositories\Contracts\TherapyRepository;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Http\Requests\StartDiagnosisDiscussionRequest;


class DiagnosisController extends Controller {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

    /**
     * @var TherapyRepository
     */
    protected $therapy;

    public function __construct(DiscussionRepository $discussions, TherapyRepository $therapy)
    {
        $this->discussions = $discussions;
        $this->therapy = $therapy;
        $this->middleware('authp');

        parent::__construct();
    }

	/**
     * Display the latest doctor discussions.
     *
     * @param Request $request
     * 
     * @return Response
     */
    public function index(Request $request)
    {
        $discussions = $this->discussions->latestDoctorPaginated($request->get('items', 6));
        $forApproval = $this->discussions->unpublishedPanelDiscussions();

        $active = 0;
        $therapies = Therapy::lists('name','slug');
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $panels = Discussion::where('channel_id',1)->latest()->take(9)->get();
        array_unshift($therapies , 'All');

        return view('patient.pages.discussions.diagnosis.index', compact('forApproval','panels','discussions','therapies','active','sort','sort_types'));
    }

    /**
     * Display the form for posting a new discussion.
     *
     * @return Response
     */
    public function create()
    {
        $therapies = $this->therapy->forSelect();

        return view('patient.pages.discussions.diagnosis.create', compact('therapies'));
    }

    /**
     * Start a new discussion.
     *
     * @param  StartDiagnosisDiscussionRequest $request
     *
     * @return Response
     */
    public function store(StartDiagnosisDiscussionRequest $request)
    {
        $discussion = $this->dispatch(new StartDiagnosisDiscussion(
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            0 // Status ( 0 Since not admin )
        ));

        // Notify Admin

        $mailData['title'] = $discussion->title;
        $mailData['id'] = $discussion->id;
        $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.admin.pending-diagnosis',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
            'subject'  => 'New Diagnosis for Approval',
            'message'  => $mailData
        ]);

        Session::flash('message','Thank you for posting. Your post has been queued for approval and will become visible once approved by the moderators.');
        return redirect(route('doctor.diagnosis.create'));
    }

     /**
     * Display the discussion based on the slug provided.
     *
     * @param  string $slug
     *
     * @return Response
     */
    public function show($slug)
    {
        $discussion = $this->discussions->findBySlug($slug);
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();
        $comments = $discussion->comments()->where('status',1)->orderBy('created_at','desc')->take(3)->get();

        return view('patient.pages.discussions.diagnosis.show', compact('discussion','diagnosis','comments'));
    }

    public function edit($slug){
        $data = Discussion::with('author','comments','attachments')->where('slug',$slug)->first();
        $therapies = Therapy::lists('name','id');
        return view('patient.pages.discussions.diagnosis.edit' , compact('therapies','data'));
    }


    public function update(Request $request)
    {
        $discussion  = Discussion::where('id',$request->get('discussion_id'))->first();
        $remove_attachment = false;

        if($request->get('remove_attachment'))
            $remove_attachment = true;

        $this->dispatch(new UpdateDiagnosisDiscussion(
            $discussion,
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            $remove_attachment
        ));

        return redirect(route('doctor.diagnosis.show',$discussion->slug));
    }


    public function filter(Request $request)
    {
        $id = $request->input('therapy');
        $sort = $request->input('sort');

        if(empty($id)&&$sort=='latest')
            return redirect(route('doctor.diagnosis.index'));
        elseif(empty($id)&&$sort=='popular')
            return redirect(route('diagnosis_popular'));

        if($sort=='latest')
            return redirect(route('show_filtered_latest',$id));

        elseif($sort=='popular')
            return redirect(route('show_filtered_popular',$id));
    }

    public function show_filtered_latest($id)
    {
        $therapy_id = Therapy::where('slug', $id)->first()->id;
        $active = $id;
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = Discussion::where('therapy_id', $therapy_id)->where('channel_id', 2)->orderBy('created_at','desc')->paginate(10);

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',1)->latest()->take(9)->get();
        return view('patient.pages.discussions.diagnosis.filtered', compact('panels','discussions', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_popular()
    {
        $active = 0;
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularDoctorDiscussions(10);

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',1)->latest()->take(9)->get();
        return view('patient.pages.discussions.diagnosis.filtered', compact('panels','discussions', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_filtered_popular($id)
    {
        $active = Therapy::where('slug', $id)->first();
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularDoctorDiscussionsWithFilter($active->id,10);

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',1)->latest()->take(9)->get();
        $active = $id;
        return view('patient.pages.discussions.diagnosis.filtered', compact('panels','discussions', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public static function destroy($slug)
    {
        Discussion::where('slug',$slug)->delete();
        return redirect(route('doctor.diagnosis.index'));
    }
}
