<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Models\Discussion;
use MEDoctors\Models\Therapy;

use Illuminate\Support\Facades\Session;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Commands\StartPatientDiscussion;
use MEDoctors\Repositories\Contracts\TherapyRepository;

class DiscussionsController extends Controller {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

	public function __construct(DiscussionRepository $discussions, TherapyRepository $therapyRepository)
    {
        $this->discussions = $discussions;
        $this->therapy = $therapyRepository;
        $this->middleware('authp');

        parent::__construct();
    }

    /**
     * Display the latest panel discussions.
     *
     * @param Request $request
     * 
     * @return Response
     */
	public function index(Request $request)
    {
        $active = 0;
        $therapies = Therapy::lists('name','slug');
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];


        $discussions = $this->discussions->latestPatientPaginated($request->get('items', 6));
        array_unshift($therapies , 'All');

        return view('patient.pages.discussions.index', compact('discussions','active','therapies','sort','sort_types'));
    }

    /**
     * Display the discussion based on the slug provided.
     *
     * @param  string $slug
     *
     * @return Response
     */
    public function show($slug, Request $request)
    {
        $discussion = $this->discussions->findBySlug($slug);
        $panels = $this->discussions->latestPatientPaginated($request->get('items', 6));
        $comments = $discussion->comments()->where('status',1)->orderBy('created_at','desc')->take(3)->get();

        return view('patient.pages.discussions.show', compact('discussion','panels','comments'));
    }

    /**
     * Display the form for posting a new discussion.
     *
     * @return Response
     */
    public function create()
    {
        $therapies = $this->therapy->forSelect();

        return view('patient.pages.discussions.create', compact('therapies'));
    }

    /**
     * Start a new discussion.
     *
     * @param  StartDiagnosisDiscussionRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $discussion = $this->dispatch(new StartPatientDiscussion(
            $this->user->id,
            $request->get('title'),
            $request->get('content'),
            $request->get('therapy_id'),
            $request->file('attachment'),
            0 // Status ( 0 Since not admin )
        ));

        // Notify Admin

        $mailData['title'] = $discussion->title;
        $mailData['id'] = $discussion->id;
        $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.admin.pending-diagnosis',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
            'subject'  => 'New Diagnosis for Approval',
            'message'  => $mailData
        ]);

        Session::flash('message','Thank you for posting. Your post has been queued for approval and will become visible once approved by the moderators.');
        return redirect(route('patient.discussions.create'));
    }

    public static function destroy($id , $request)
    {
        $discussion = Discussion::where('id',$id)->delete();
        return $request->input();
    }

//    public function show_popular()
//    {
//        $discussions = $this->discussions->popularPanelDiscussions(10);
//        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();
//        return view('patient.pages.discussions.panel.index', compact('discussions','diagnosis'));
//    }
    public function filter(Request $request)
    {
        $id = $request->input('therapy');
        $sort = $request->input('sort');

        if(empty($id)&&$sort=='latest')
            return redirect(route('patient.discussions.index'));
        elseif(empty($id)&&$sort=='popular')
            return redirect(route('patient.discussions_popular'));

        if($sort=='latest')
            return redirect(route('patient.discussions_show_filtered_latest',$id));

        elseif($sort=='popular')
            return redirect(route('patient.discussions_show_filtered_popular',$id));
    }

    public function show_filtered_latest($slug)
    {
        $therapy_id = Therapy::where('slug', $slug)->first()->id;
        $active = $slug;
        $sort = 'latest';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = Discussion::where('therapy_id', $therapy_id)->where('channel_id', 3)->orderBy('created_at','desc')->paginate(10);
        $diagnosis = Discussion::where('channel_id',3)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',3)->latest()->take(9)->get();
        return view('patient.pages.discussions.filtered', compact('panels','discussions','diagnosis', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_popular()
    {
        $active = 0;
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularPatientDiscussions(10);
        $diagnosis = Discussion::where('channel_id',3)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',3)->latest()->take(9)->get();
        return view('patient.pages.discussions.filtered', compact('panels','diagnosis', 'discussions', 'therapies' , 'active' , 'sort','sort_types'));
    }

    public function show_filtered_popular($id)
    {
        $active = Therapy::where('slug', $id)->first();
        $sort = 'popular';
        $sort_types = [ 'latest' => 'Latest' , 'popular' => 'Popular' ];

        $discussions = $this->discussions->popularPatientDiscussionsWithFilter($active->id,10);
        $diagnosis = Discussion::where('channel_id',2)->latest()->take(9)->get();

        $therapies = Therapy::lists('name','slug');
        array_unshift($therapies , 'All');

        $panels = Discussion::where('channel_id',2)->latest()->take(9)->get();
        return view('patient.pages.discussions.filtered', compact('panels','discussions', 'diagnosis', 'therapies' , 'active' , 'sort','sort_types'));
    }

}
