<?php namespace MEDoctors\Http\Controllers\Patient;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Repositories\Contracts\HospitalRepository;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\Location;
use MEDoctors\Models\Language;
use MEDoctors\Models\Hospital;
use MEDoctors\Events\UserViewed;

use MEDoctors\Services\DoctorService;
use MEDoctors\Repositories\Eloquent\DbUserRepository;
use Illuminate\Bus\Dispatcher;
use Illuminate\Support\Facades\Session;

class DoctorsController extends Controller {

    protected $userRepository;

    public function __construct(UserRepository $userRepository, DoctorService $doctorService, HospitalRepository $hospitalRepository){
        $this->users   = $userRepository;
        $this->doctors = $doctorService;
        $this->hospitals = $hospitalRepository;

        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $toLoad = 10;
        $valid_countries = [ 'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY' ];

        // All Doctors
        $docs = $this->users
            ->withTraits()
            ->withHospitals()
            ->filterSpecialty('all')
            ->filterCountry('all')
            ->filterCity('all')
            ->doctorsOnly()
            ->orderBy('created_at', 'DESC')
            ->take($toLoad)
            ->get();

        // Featured Doctors
        $doctors = $this->users
            ->withTraits()
            ->withHospitals()
            ->filterSpecialty('all')
            ->filterCountry('all')
            ->filterCity('all')
            ->featuredFirst()
            ->doctorsOnly()
            ->orderBy('created_at', 'DESC')
            ->take($toLoad)->get();

        $featuredCount = count($doctors);

        if($featuredCount<$toLoad){

            // Append Featured To new array then Non-Featured
            foreach($docs as $doctor){

                if($doctor->services)
                    continue;

                if(count($doctors)>=$toLoad)
                    break;

                $doctors[] = $doctor;
            }
        }

        $total_doctors = $this->users
            ->doctorsOnly()
            ->count();

        // Featured Doctors
        $totalFeatured = $this->users
            ->filterSpecialty('all')
            ->filterCountry('all')
            ->filterCity('all')
            ->featuredFirst()
            ->doctorsOnly()
            ->count();

        $therapies = array('all' => 'All Therapies') + Therapy::orderBy('name', 'ASC')->lists('name','slug');

        $countries = Country::with('cities')->whereIn('code', $valid_countries)->orderBy('name')->get();

        $cities_with_doctors = [];

        $allDocs = User::with('hospitals.location')->whereHas('hospitals.location',function($q){})->get();

        foreach($allDocs as $doctor){
            foreach($doctor->hospitals as $hospital){
                if($hospital->location){
                    if(!in_array($hospital->location->city , $cities_with_doctors))
                        array_push($cities_with_doctors,strtoupper($this->clean($hospital->location->city)));
                }
            }
        }

        $cities_with_doctors = (array_filter($cities_with_doctors));
        foreach($countries as $index=>$country){
            $cities[$country->name] = [];
            if(count($country->cities)>0){
                foreach($country->cities as $city){
                    if(array_search(strtoupper(trim($city->name)), $cities_with_doctors))
                        $cities[$country->name][] = $city->name;
                }
            }
        }

        return view('patient.pages.doctors.index', compact('totalFeatured','doctors','therapies','countries','cities','total_doctors','featuredCount'));
    }

    public function clean($string) {
        $string = str_replace('-', ' ', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function filter(Request $request)
    {
        $toLoad = 10;
        $valid_countries = [ 'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY' ];

        // All Doctors
        $docs = $this->users
            ->withTraits()
            ->withHospitals()
            ->filterSpecialty($request->get('therapy'))
            ->filterCountry($request->get('country'))
            ->filterCity($request->get('city'))
            ->doctorsOnly()
            ->take($toLoad)
            ->get();

        $total_doctors = $this->users
            ->doctorsOnly()
            ->count();

        // Featured Doctors
        $doctors = $this->users->getFeaturedWithFilters(
            $request->get('therapy'),
            $request->get('country'),
            $request->get('city'),
            $request->get('sort'),
            $request->get('featuredCount'),
            $toLoad
        );

        $featuredCount = count($doctors);

        if($featuredCount<$toLoad){

            // Append Featured To new array then Non-Featured
            foreach($docs as $doctor){

                if($doctor->services)
                    continue;

                if(count($doctors)>=$toLoad)
                    break;

                $doctors[] = $doctor;
            }
        }

        // Featured Doctors
        $totalFeatured = $this->users->countFeatured($request->get('therapy'),$request->get('country'),$request->get('city'));

        $therapies = array('all' => 'All Therapies') + Therapy::orderBy('name', 'ASC')->lists('name','slug');

        $countries = Country::with('cities')->whereIn('code', $valid_countries)->orderBy('name')->get();

        $cities_with_doctors = [];

        $allDocs = User::with('hospitals.location')->whereHas('hospitals.location',function($q){})->get();

        foreach($allDocs as $doctor){
            foreach($doctor->hospitals as $hospital){
                if($hospital->location){
                    if(!in_array($hospital->location->city , $cities_with_doctors))
                        array_push($cities_with_doctors,strtoupper($this->clean($hospital->location->city)));
                }
            }
        }

        $cities_with_doctors = (array_filter($cities_with_doctors));
        foreach($countries as $index=>$country){
            $cities[$country->name] = [];
            if(count($country->cities)>0){
                foreach($country->cities as $city){
                    if(array_search(strtoupper(trim($city->name)), $cities_with_doctors))
                        $cities[$country->name][] = $city->name;
                }
            }
            if($country->name==$request->get('country'))
                $active_cities = $cities[$country->name];
        }

        return view('patient.pages.doctors.index', compact('active_cities','totalFeatured','doctors','therapies','countries','cities','total_doctors','featuredCount'));
    }

    public function rate(Request $request, Dispatcher $dispatcher){

        $this->middleware('authp');
        $results = $dispatcher->dispatchFromArray('MEDoctors\Commands\RateDoctor', [
            'user_id'  => $request->get('user_id'),
            'ratings'  => $request->get('ratings'),
            'rater_id' => $this->user->id
        ]);

        Session::flash('message', 'Ratings Updated Successfully');

        return redirect()->back();
    }

    /**
     * Get next set of records visa AJAX.
     *
     * @return Response
     */
    public function getNext(Request $request)
    {
        // All Doctors
        $docs = $this->users
            ->withTraits()
            ->withHospitals()
            ->sortBy($request->get('sort'))
            ->filterSpecialty($request->get('therapy'))
            ->filterCountry($request->get('country'))
            ->filterCity($request->get('city'))
            ->skipAndTake($request->get('offset'),$request->get('take')*2)
            ->doctorsOnly()
            ->orderBy('created_at', 'DESC')
            ->get();

        $doctors= [];

        if($request->get('allFeaturedLoaded')){
            // Featured Doctors
            $doctors = $this->users
                ->withTraits()
                ->withHospitals()
                ->sortBy($request->get('sort'))
                ->filterSpecialty($request->get('therapy'))
                ->filterCountry($request->get('country'))
                ->filterCity($request->get('city'))
                ->noFeatured()
                ->skipAndTake($request->get('offset'),$request->get('take'))
                ->doctorsOnly()
                ->orderBy('created_at', 'DESC')
                ->get();
        }
        else {

            // Featured Doctors
            $doctors = $this->users->getFeaturedWithFilters(
                $request->get('therapy'),
                $request->get('country'),
                $request->get('city'),
                $request->get('sort'),
                $request->get('featuredCount'),
                $request->get('take')
            );

            if(!$request->get('featuredCount')%10==0){
                if(count($doctors)<10){
                    $featuredCount = $request->get('featuredCount') + count($doctors);
                    // Append Featured To new array then Non-Featured
                    foreach($docs as $doctor){
                        if($doctor->services)
                            continue;

                        if(count($doctors)>=10)
                            break;

                        $doctors[] = $doctor;
                    }
                }
            }
        }


        $ratings = $this->users->getTraitsArray($doctors);

        foreach($doctors as $index=>$doctor){
            $doctors[$index]['ratings'] = $ratings[$index];
            $name = $doctors[$index]['bio'];
            $name = strip_tags($name);
            $name = str_replace("\n", '', $name);
            $name = str_replace("\r", '', $name);
            $doctors[$index]['bio'] = str_replace("\r\n", '', $name); 
        }

        echo json_encode($doctors);
    }

    /**
     * Show the user profile.
     *
     * @param string $username
     * 
     * @return Response
     */
    public function show($username , Request $request)
    {

        $user = $this->users->withTraits()->getByUsername($username);

        $user->load('specialties',
            'hospitals.location',
            'hospitals.schedules',
            'educations',
            'languages',
            'achievements',
            'publications',
            'certifications',
            'kol',
            'services',
            'contacts');
        
        $languages = Language::orderBy('name')->select('name', 'id')->get()->toArray();
        $hospitals = $this->hospitals->generateAutoLoadData(Hospital::with('location')->get());
        $this->hospitals->generateHospitalSchedules($user);
        $user->languages = $user->languages->toArray();
        $agent = \BrowserDetect::detect()->browserFamily;

        if($user)
        {
            $viewerId = $this->user ? $this->user->id : 0;
            event(new UserViewed($user, $request->ip() , $viewerId));
        }

        $waiting_times = $this->generateWaitingTimes();

        $current_user_ratings = $this->users->getCurrentUserRating($user);

        return view('patient.pages.doctors.show', compact('user','languages','agent','hospitals','current_user_ratings','waiting_times'));

    }

    public function generateWaitingTimes(){
        $data = array(
            '5' => '5 Minutes',
            '10' => '10 Minutes',
            '30' => '30 Minutes',
            '60' => '1 hour',
            '120' => '2 hours',
            '180' => '3 hours',
            '360' => '6 hours',
            '720' => '12 hours',
        );

        return $data;
    }
}