<?php namespace MEDoctors\Http\Controllers\Patient;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Event;
use MEDoctors\Models\Upload;

class EventsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    public static function show($slug){
        $event = Event::where('slug',$slug)->first();
        return view('patient.pages.events.show' , compact('event'));
    }

}
