<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Bus\Dispatcher;
use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\EmailProvider;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\HospitalClaim;
use MEDoctors\Models\Location;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\HospitalFacility;

use MEDoctors\Events\HospitalViewed;

use MEDoctors\Repositories\Eloquent\CanAcceptCorporateEmailOnly;

class HospitalsController extends Controller {

    public function __construct(HospitalRepository $hospitalRepository, Dispatcher $dispatcher){
        $this->hospital = $hospitalRepository;

        parent::__construct();
    }
    /**
     * Display a listing of hospitals.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $toLoad = 10;
        $valid_countries = [ 'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY' ];

        $ahospitals = $this->hospital
            ->sortBy('latest')
            ->onlyVerified()
            ->getPaginated($toLoad);

        $hospitals = $this->hospital->getFeaturedWithFilters(
            'all',
            'all',
            'all',
            'latest',
            0,
            $toLoad
        );

        $featuredCount = count($hospitals);

        if($featuredCount<$toLoad){
            // Append Featured To new array then Non-Featured
            foreach($ahospitals as $hospital){

                if($hospital->services)
                    continue;

                if(count($hospitals)>=$toLoad)
                    break;

                $hospitals[] = $hospital;
            }
        }

        // Featured Doctors

        $totalFeatured = $this->hospital
            ->onlyVerified()
            ->featuredFirst()
            ->getAllCount();

        $total_hospitals = $this->hospital
            ->onlyVerified()
            ->getAllCount();

        $therapies = array('all' => 'All Therapies') + Therapy::orderBy('name', 'ASC')->lists('name','slug');

        $countries = Country::with('cities')->whereIn('code', $valid_countries)->orderBy('name')->get();

        foreach($countries as $index=>$country) {
            $cities[$country->name] = [];
            if(count($country->cities)>0) {
                foreach($country->cities as $city) {
                    if (
                        Location::where('city',$city->name)->
                        where('locationable_type','MEDoctors\Models\Hospital')->
                        count() > 0
                    )
                        $cities[$country->name][] = $city->name;
                }
            }
        }

        return view('patient.pages.hospitals.index', compact('totalFeatured','hospitals','therapies','countries','cities','total_hospitals','featuredCount'));
    }

    /**
     * Display a hospital.
     *
     * @return Response
     */
    public function show($hospital_slug, Request $request)
    {
        $hospital = $this->hospital->withData(['location','avatar','therapies','facilities'])->findBySlug($hospital_slug);

        if($this->user)
            $claim = HospitalClaim::where('hospital_id',$hospital->id)->where('user_id',$this->user->id)->pluck('email');

        if($hospital)
        {
            $viewerId = $this->user ? $this->user->id : 0;
            event(new HospitalViewed($hospital, $request->ip() , $viewerId));
        }

        return view('patient.pages.hospitals.show', compact('hospital','claim'));
    }


    /**
     * Edit hospital.
     *
     * @return Response
     */
    public function edit($hospital_slug)
    {
        $hospital = $this->hospital->withData(['location','avatar'])->findBySlug($hospital_slug);
        $therapies = Therapy::orderBy('name')->select('name','id')->get()->toArray();
        $facilities = HospitalFacility::orderBy('name')->select('name','id')->get()->toArray();
        return view('patient.pages.hospitals.edit', compact('hospital','therapies','facilities'));
    }


    /**
     * Update a hospital.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $keep_thumb = true;

        $input = $request->only('hospital');
        $hospital = $this->hospital->findBySlug($request->get('slug'));

        $contact = $request->get('contacts');
        $facilities = $request->get('facilities');
        $therapies = $request->get('therapies');
        $location = $request->get('location');
        $file     = $request->file('thumbnail');

        if($request->input('remove_thumb'))
            $keep_thumb = false;

        $newHospital = $this->dispatchFrom('MEDoctors\Commands\Admin\Hospitals\UpdateHospital', $request, [
            'hospital'   => $hospital,
            'input'      => $input['hospital'],
            'contact'    => $contact,
            'facilities' => $facilities,
            'therapies'  => $therapies,
            'location'   => $location,
            'file'       => $file,
            'keep_thumb' => $keep_thumb,
        ]);

        return redirect(route('patient.hospitals.show' , $newHospital->slug));
    }

    /**
     * Filter and list hospitals.
     *
     * @return Response
     */
    public function filter(Request $request)
    {
        $toLoad = 10;
        $valid_countries = [ 'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY' ];

        $ahospitals = $this->hospital
            ->onlyVerified()
            ->sortBy($request->get('sort'))
            ->filterSpecialty($request->get('therapy'))
            ->filterCountry($request->get('country'))
            ->filterCity($request->get('city'))
            ->getPaginated($toLoad);

        // Featured Hospitals
        $hospitals = $this->hospital->getFeaturedWithFilters(
            $request->get('therapy'),
            $request->get('country'),
            $request->get('city'),
            $request->get('sort'),
            0,
            $toLoad
        );

        $featuredCount = count($hospitals);

        if($featuredCount<$toLoad){
            // Append Featured To new array then Non-Featured
            foreach($ahospitals as $hospital){

                if($hospital->services)
                    if($hospital->services->active==1)
                        continue;

                if(count($hospitals)>=$toLoad)
                    break;

                $hospitals[] = $hospital;
            }
        }

        // Featured Hospitals
        $totalFeatured = $this->hospital->getFeaturedWithFiltersCount(
            $request->get('therapy'),
            $request->get('country'),
            $request->get('city')
        );

        $total_hospitals = $this->hospital
            ->sortBy($request->get('sort'))
            ->onlyVerified()
            ->filterSpecialty($request->get('therapy'))
            ->filterCountry($request->get('country'))
            ->filterCity($request->get('city'))
            ->getAllCount();

        //////////////

        $therapies = array('all' => 'All Therapies') + Therapy::orderBy('name', 'ASC')->lists('name','slug');

        $countries = Country::with('cities')->whereIn('code', $valid_countries)->orderBy('name')->get();

        foreach($countries as $index=>$country){
            $cities[$country->name] = [];
            if(count($country->cities)>0){
                foreach($country->cities as $city){
                    if(
                        Location::where('city',$city->name)->
                        where('locationable_type','MEDoctors\Models\Hospital')->
                        count() > 0
                    ){
                        $cities[$country->name][]= $city->name;

                        if($country->name == $_GET['country'])
                            $active_cities[$city->name] = $city->name;
                    }
                }
            }
        }

        return view('patient.pages.hospitals.index', compact('featuredCount','totalFeatured','hospitals','therapies','countries','cities','active_cities','total_hospitals'));
    }
    /**
     * Get next set of records visa AJAX.
     *
     * @return Response
     */
    public function getNext(Request $request)
    {
        $hosp = $this->hospital
            ->withData(['location','avatar','therapies','facilities','contacts','services'])
            ->sortBy($request->get('sort'))
            ->onlyVerified()
            ->filterSpecialty($request->get('therapy'))
            ->filterCountry($request->get('country'))
            ->filterCity($request->get('city'))
            ->skipAndTake($request->get('offset'),$request->get('take')*20);

        $hospitals= [];

        if($request->get('allFeaturedLoaded')){
            // Featured Doctors
            $hospitals = $this->hospital
                ->withData(['location','avatar','therapies','facilities','contacts','services'])
                ->sortBy($request->get('sort'))
                ->filterSpecialty($request->get('therapy'))
                ->filterCountry($request->get('country'))
                ->filterCity($request->get('city'))
                ->noFeatured()
                ->skipAndTake($request->get('offset'),$request->get('take'));
        }
        else {

            // Featured Hospitals
            $hospitals = $this->hospital->getFeaturedWithFilters(
                $request->get('therapy'),
                $request->get('country'),
                $request->get('city'),
                $request->get('sort'),
                $request->get('featuredCount'),
                $request->get('take')
            );


            if(!$request->get('featuredCount')%10==0){
                if(count($hospitals)<10){
                    $featuredCount = $request->get('featuredCount') + count($hospitals);
                    // Append Featured To new array then Non-Featured
                    foreach($hosp as $hospital){
                        if(isset($hospital['services']))
                            if(count($hospital['services']))
                                continue;

                        if(count($hospitals)>=10)
                            break;

                        $hospitals[] = $hospital;
                    }
                }
            }
        }


        for($x=0;$x<count($hospitals);$x++) {
            $name = $hospitals[$x]['description'];
            $name = strip_tags($name);
            $name = preg_replace("/&#?[a-z0-9]+;/i","",$name);
            $hospitals[$x]['description'] = str_replace("\r\n", '', $name); 
        }

        echo json_encode($hospitals);
    }

    public function checkProvider(Request $request){

        $domain = substr(strrchr($request->input('email'), "@"), 1);

        if(EmailProvider::where('name',$domain)->get()->count())
            return 0;

        return 1;
    }

    public function claim(Request $request){
        $user = $this->user;

        if(!$user)
            return redirect(route('patient.login'));

        $claim = HospitalClaim::create(array(
            'hospital_id' => $request->input('hospital_id'),
            'user_id' => $user->id,
            'email' => $request->input('email'),
            'token' => substr( md5(rand()), 0, 30)
        ));

        if($claim)
        {
            $data['user'] = $user->fullName;
            $data['hospital'] = $claim->hospital->name;
            $data['id'] = $claim->id;

            Session::flash('message', 'Your claim request has been sent successfully. If approved, a final verification email will be sent to '.$claim->email);

            // Notify User
            $this->dispatchFromArray('MEDoctors\Commands\SendMail', [
                'template' => 'emails.hospitals.claim-request',
                'sender_email' => 'mailman@middleeastdoctor.com',
                'sender_name'  => 'Middle East Doctor',
                'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
//                'recipient_email'  => 'gene@leadingbrands.me',
                'subject'  => 'MED Hospital Claim Request',
                'message'  => $data
            ]);
        }
        else
            Session::flash('message','Your claim request has been sent successfully. If approved, a final verification email will be sent to '.$claim->email);

        return redirect()->back();
    }


    public function verify($token){
        $claim = HospitalClaim::with('hospital')->where('token',$token)->first();

        if($claim){
            $claim->verified = 1;
            $claim->save();
        }

        return redirect(route('patient.hospitals.show',$claim->hospital->slug));

    }
}
