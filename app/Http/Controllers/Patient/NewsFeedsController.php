<?php namespace MEDoctors\Http\Controllers\Patient;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Repositories\Contracts\NewsFeedRepository;

class NewsFeedsController extends Controller {

    function __construct(NewsFeedRepository $newsFeedRepository)
    {
        $this->news = $newsFeedRepository;
        parent::__construct();
    }

    public static function show($slug)
    {
        $news = NewsFeed::where('slug',$slug)->first();
        return view('patient.pages.news.show' , compact('news'));
    }

    /**
     * Display a listing of upcoming news.
     *
     * @return Response
     */
    public function upcoming()
    {
        $news = NewsFeed::where('channel_id',3)->orderBy('created_at','DESC')->paginate(10);
        return view('patient.pages.news.index', compact('news'));
    }

}
