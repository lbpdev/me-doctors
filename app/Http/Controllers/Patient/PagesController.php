<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Commands\FetchTweets;
use Illuminate\Contracts\Auth\Guard;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Services\Feeds\NewsFeed;
use Illuminate\Support\Facades\Session;
use MEDoctors\Services\HomePageService;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Services\PopularDiscussions;

use MEDoctors\Models\Survey;

class PagesController extends Controller {

    /**
     * @var HomePageService
     */
    protected $home;

    /**
     * @var PopularDiscussions
     */
    protected $popular;

    /**
     * @var NewsFeed
     */
    protected $newsFeed;

    public function __construct(
        HomePageService $home,
        PopularDiscussions $popular,
        UserRepository $userRepository,
        HospitalRepository $hospitalRepository,
        DiscussionRepository $discussionRepository
    )
    {
        $this->home = $home;
        $this->popular = $popular;
        $this->users = $userRepository;
        $this->hospitals = $hospitalRepository;
        $this->discussions = $discussionRepository;

        parent::__construct();
    }

    /**
     * The home page.
     *
     * @param  Guard $auth
     *
     * @return Response
     */
	public function landing(Guard $auth)
	{
        $latestArticles = $this->home->latestArticles(3,5);
        $surveys = $this->home->latestSurveys(4,3);
        $poll = $this->home->activePoll($auth->id(),'patient-active-poll');
        $doctors = $this->users->doctorsOnly()->with('hospitals.location')->latest()->take(5)->get();
        $hospitals = $this->hospitals->sortBy('latest')->onlyVerified()->getOnly(5);
        $discussions = $this->discussions->latestPatient(5);
        $videos = $this->home->videos(3,10);
        $news = $this->home->latestNews(5,3);
        $landing = true;

        return view('patient.pages.home' , compact(
            'surveys',
            'latestArticles',
            'poll',
            'doctors',
            'hospitals',
            'discussions',
            'videos',
            'news',
            'landing'
        ));
    }

    /**
     * The home page.
     *
     * @param  Guard $auth
     *
     * @return Response
     */
	public function index(Guard $auth)
	{
        $latestArticles = $this->home->latestArticles(3,5);
        $surveys = $this->home->latestSurveys(4,3);
        $poll = $this->home->activePoll($auth->id(),'patient-active-poll');
        $doctors = $this->users->doctorsOnly()->with('hospitals.location')->latest()->take(5)->get();
        $hospitals = $this->hospitals->sortBy('latest')->onlyVerified()->getOnly(5);
        $discussions = $this->discussions->latestPatient(5);
        $videos = $this->home->videos(3,10);
        $news = $this->home->latestNews(5,3);

        return view('patient.pages.home' , compact(
            'surveys',
            'latestArticles',
            'poll',
            'doctors',
            'hospitals',
            'discussions',
            'videos',
            'news'
        ));
    }

    /**
     * The landing page.
     *
     *
     * @return Response
     */
	public function soon()
	{
        return view('soon');
    }

    /**
     * Show the pages requested by the user.
     *
     * @param  string $page
     *
     * @return Response
     */
    public function show($page)
    {
        if (method_exists($this, $page))
        {
            return $this->$page();
        }

        if ( ! view()->exists($view = "patient.pages.{$page}")) {
            abort(404);
        }

        return view($view);
    }

}
