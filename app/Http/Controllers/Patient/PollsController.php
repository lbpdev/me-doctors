<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Poll;
use MEDoctors\Models\PollItem;
use MEDoctors\Models\PollVote;

use MEDoctors\Commands\PollAnswers;
use MEDoctors\Repositories\Contracts\PollRepository;

class PollsController extends Controller {

    protected $poll;

    public function __construct(PollRepository $poll)
    {
        $this->poll = $poll;
        $this->middleware('auth');

        parent::__construct();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('templates.add_poll');
	}

    public  function vote(Request $request){

        extract($request->only('poll_id', 'poll_item'));
        if($poll_item)
            $this->dispatch(new PollAnswers($this->user, $poll_id, $poll_item[0]));

        return redirect()->back();
    }

}
