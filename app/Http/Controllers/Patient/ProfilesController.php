<?php namespace MEDoctors\Http\Controllers\Patient;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\DoctorQualification;
use MEDoctors\Models\Education;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\PhoneNumber;
use MEDoctors\Models\RoleUser;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Models\Profile;
use MEDoctors\Models\TherapyUser;
use MEDoctors\Models\FieldMeta;
use MEDoctors\Models\Award;
use MEDoctors\Models\Membership;
use MEDoctors\Models\Language;
use MEDoctors\Models\Certification;

use MEDoctors\Http\Controllers\ContactsController as ContactsController;
use MEDoctors\Http\Controllers\TherapiesController as TherapiesController;
use MEDoctors\Http\Controllers\UploadsController as UploadsController;
use MEDoctors\Http\Controllers\HospitalsController as HospitalsConroller;
use MEDoctors\Http\Controllers\DoctorQualificationsController as DoctorQualificationsController;
class ProfilesController extends Controller {


	public function index()
	{
		//
	}

    public function mock(){
        return view('pages.profiles.mock');
    }

    public function show($username)
    {
        $user = User::where('username',$username)
            ->with('profile','therapies','roles','educations','awards','certifications','languages','memberships')
            ->first();
        if($user)
            if( $user->roles[0]->id == 1 || $user->roles[0]->id == 2 ){
                return view('pages.profiles.show', compact('user','qualifications'));
            }
            else
                return view('patients.pages.profiles.show', compact('user'));

        return false;
    }

    public function edit_profile(User $user , $username)
    {
        $field_meta = FieldMeta::where('user_id', $user->id)
            ->where('meta_type', 'profile')
            ->where('value', 1)->first();

        $user = ProfilesController::getUserProfile($username);

        if($field_meta)
            $user->show_email = true;

        return view('pages.profiles.edit_profile', compact('user'));
    }

    public function edit_work($username)
    {
        $countries = Country::lists('name','code');
        $cities = City::lists('name','id');
        $therapies = Therapy::lists('name','id');

        $user = User::where('username',$username)->with('profile','educations')->first();
        return view('pages.profiles.edit_work', compact('user','cities','countries','therapies'));
    }

    public function update_profile(Request $request)
    {
        $username = self::do_update_profile($request);
        return redirect('profile/'.$username);
    }

    public static function do_update_profile($request){

        $file = $request->file('photo');
        $photo = ($file != null ? UploadsController::uploadFile($file , 'avatars') : false);

        $input = $request->all();

        if(isset($input['user_id']))
            $me = User::where('id',$input['user_id'])->first();
        else
            $me = Session::all();

        $existing_profile = Profile::where('user_id', $me['id'])->first();

        if (!$existing_profile){
            if($photo)
                $profile['avatar'] = $photo->id;

            $profile['user_id'] = $me['id'];
            $profile['bio'] = $input['bio'];
            Profile::create($profile);

        }
        else {
            $photo != null ? $existing_profile->avatar = $photo->id  : false;
            $existing_profile->bio = $input['bio'];
            $existing_profile->save();
        }

        if($photo){
            $photo_url = $photo->path.'/'.$photo->name;
            Session::set('avatar', $photo_url);
        }

        $field_meta_exists = FieldMeta::where('user_id', $me['id'])
            ->where('meta_type', 'profile')
            ->where('field', 'email')->first();

        if($field_meta_exists)
            $field_meta_exists->delete();

        if(isset($input['email_address'])){

            $field_meta = new FieldMeta();
            $field_meta['user_id'] = $me['id'];
            $field_meta['field'] = 'email';
            $field_meta['meta_type'] = 'profile';
            $field_meta['value'] = 1;
            $field_meta->save();
        }

        if(isset($input['role'])){
            $roleUser = RoleUser::where('user_id' , $me['id'])->first();

            if($roleUser==null){
                $roleUser = new RoleUser();
                $roleUser['user_id'] = $me['id'];
            }

            $roleUser['role_id'] = $input['role'];
            $roleUser->save();
        }

        return $me['username'];
    }

    public function update_work(Request $request)
    {
        $user = self::do_update_work($request);

        return redirect('profile/'.$user);
    }

    public static function do_update_work($request){

        $input_unfiltered = $request->all();
        $input = array_filter($input_unfiltered);

        if(isset($input['user_id']))
            $user = User::where('id',$input['user_id'])->first();
        else
            $user = \Auth::user();

        $user->therapies()->sync(array(1=>array('id' => $input['therapy'])));
        $data = null;

        ////////////////////////// Add New Educations and sync to user

        $educations = array_filter($input['educations']);
        if(count($educations)>0){
            foreach($educations as $index=>$education){
                $exists = Education::where('name', $education)->first();

                if(count($exists)<1){
                    $education_new = Education::create(array('name' => $education));
                    $education_id = $education_new->id;
                } else
                    $education_id = $exists->id;

                    $data[$index] = ['education_id' => $education_id , 'user_id' => $user->id];
            }
        }

        $user->educations()->sync($data);


        ///////////////////////// Add New Languages and sync to user

        $data = null; $languages = array_filter($input['languages']);
        if(count($languages)>0){
            foreach($languages as $index=>$language){
                $exists = Language::where('name', $language)->first();

                if(count($exists)<1){
                    $language_new = Language::create(array('name' => $language));
                    $language_id = $language_new->id;
                } else
                    $language_id = $exists->id;

                $data[$index] = ['language_id' => $language_id , 'user_id' => $user->id];
            }
        }

        $user->languages()->sync($data);


        ////////////////////////// Add New Memberships and sync to user

        $data = null; $memberships = array_filter($input['memberships']);
        if(count($memberships)>0){
            foreach($memberships as $index=>$membership){
                $exists = Membership::where('name', $membership)->first();

                if(count($exists)<1){
                    $membership_new = Membership::create(array('name' => $membership));
                    $membership_id = $membership_new->id;
                } else
                    $membership_id = $exists->id;

                $data[$index] = ['membership_id' => $membership_id , 'user_id' => $user->id];
            }
        }

        $user->memberships()->sync($data);


        ////////////////////////// Add New Awards and sync to user

        $data = null; $awards = array_filter($input['awards']);
        if(count($awards)>0){
            foreach($awards as $index=>$award){
                $exists = Award::where('name', $award)->first();

                if(count($exists)<1){
                    $award_new = Award::create(array('name' => $award));
                    $award_id = $award_new->id;
                } else
                    $award_id = $exists->id;

                $data[$index] = ['award_id' => $award_id , 'user_id' => $user->id];
            }
        }

        $user->awards()->sync($data);


        ////////////////////////// Add New certifications and sync to user

        $data = null; $certifications = array_filter($input['certifications']);
        if(count($certifications)>0){
            foreach($certifications as $index=>$certification){
                $exists = Certification::where('name', $certification)->first();

                if(count($exists)<1){
                    $certification_new = Certification::create(array('name' => $certification));
                    $certification_id = $certification_new->id;
                } else
                    $certification_id = $exists->id;

                $data[$index] = ['certification_id' => $certification_id , 'user_id' => $user->id];
            }
        }

        $user->certifications()->sync($data);

        $user->hospitals()->sync([
            'city_id' => $input['city_id']
        ]);


        HospitalsConroller::store($input , $user->id);
        ContactsController::store($input ,  $user->id);
        TherapiesController::store($input['therapy'] ,  $user->id);

        return $user['username'];
    }

    public function destroy($id)
    {
        //
    }

    public static function getUserProfile($username){

        $user = User::where('username', $username)->leftJoin('profiles', function($join){
            $join->on('profiles.user_id', '=' , 'users.id');
        })->leftJoin('hospitals', function($join){
            $join->on('hospitals.user_id', '=' , 'users.id');
        })->leftJoin('cities', function($join){
            $join->on('cities.id', '=' , 'hospitals.city_id');
        })->leftJoin('countries', function($join){
            $join->on('countries.code', '=' , 'hospitals.country_code');
        })->select(
            'users.*',
            'profiles.bio as bio',
            'profiles.avatar as avatar_id',
            'profiles.profile_type as profile_type',
            'hospitals.id as hospital_id',
            'hospitals.name as hospital_name',
            'cities.name as hospital_city',
            'countries.name as hospital_country',
            'hospitals.address as hospital_address'
        )->first();

        $contacts = PhoneNumber::where('phoneable_id', $user->id)->get()->toArray();

        foreach($contacts as $contact){
            if($contact['type']=='personal')
                $user['personal_number'] = $contact['number'];
            elseif($contact['type']=='office')
                $user['office_number'] = $contact['number'];
        }

        $user->avatar = $user->avatar_id != null ? UploadsController::getUploadURL($user->avatar_id) : '';
        $user->qualifications = DoctorQualificationsController::getQualifications($user->id);

        $field_meta = FieldMeta::where('user_id', $user->id)
            ->where('meta_type', 'profile')
            ->where('value', 1)->first();

        if($field_meta){
            $user->show_email = true;
        }

        return $user;
    }
}
