<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Models\Country;
use MEDoctors\Models\Therapy;
use MEDoctors\Events\UserHasRegistered;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Services\Registration\DoctorRegistrar;
use MEDoctors\Exceptions\InvalidDoctorEmailException;
use MEDoctors\Repositories\Contracts\CountryRepository;
use MEDoctors\Services\Registration\PatientRegistrar;

class RegistrationController extends Controller {

    /**
     * @var DoctorRegistrar
     */
    protected $registrar;

    /**
     * Create RegistrationController instance.
     *
     * @param PatientRegistrar $registrar
     */
    public function __construct(PatientRegistrar $registrar)
    {
        $this->registrar = $registrar;

        parent::__construct();
    }
    

    /**
     * Register a new user.
     *
     * @param  Request $request
     *
     * @return Response
     */
	public function register(Request $request)
    {
        $data = $request->all();

        self::validateRequest($request);

        try
        {
            $user = $this->registrar->create($data);

            event(new UserHasRegistered($user));

            session()->flash('registration.success', 'Thank you for registering with Middle East Doctor. You may now log in using your account details. In order to have full access to the site you must verify your account - please check your email for more details.');

            return redirect($this->loginPath());

        } catch (InvalidDoctorEmailException $e)
        {
            session()->flash('registration', [
                'error' => $e->getMessage(),
            ]);

            return redirect($this->loginPath())->withInput();
        }
    }

    /**
     * Confirm user registration from the email.
     *
     * @param  string $token
     *
     * @return Reponse
     */
    public function confirmEmail($token)
    {
        if($this->registrar->confirmUserEmail($token)){

            session()->flash('registration.success', trans('registration.confirmed'));

            if($this->user){
                session()->flash('registration.success', 'Your email address has been successfully verified and your account on Middle East Doctor is now active.');
                return redirect(route('patient.profile',$this->user->username));
            }
        }

        return redirect(route('patient.login'));
    }

    /**
     * Validate registration request.
     *
     * @param  Request  $request
     *
     * @return void
     */
    private function validateRequest(Request $request)
    {
        $validator = $this->registrar->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException($request, $validator);
        }
    }

    /**
     * The doctor's login url path.
     *
     * @return string
     */
    private function loginPath()
    {
        return route('patient.login');
    }

}
