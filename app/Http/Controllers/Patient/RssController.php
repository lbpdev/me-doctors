<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\ArticleRepository;

class RssController extends Controller {

    /**
     * @var ArticleRepository
     */
    protected $articleRepository;
    
    /**
     * Create RssController Instance.
     *
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }
    
    /**
     * Display RSS feeds articles list.
     *
     * @return Response
     */
	public function index()
    {
        $articles = $this->articleRepository->limit(10);

        return response()->view('rss', compact('articles'))
                         ->header('Content-Type', 'application/atom+xml; charset=UTF-8');
    }

}
