<?php namespace MEDoctors\Http\Controllers\Patient;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;

use MEDoctors\Models\Article;
use MEDoctors\Models\Discussion;
use MEDoctors\Models\Event;
use MEDoctors\Models\Survey;
use MEDoctors\Models\User;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\NewsFeed;

class SearchController extends Controller {

	public function search(Request $request){

        $articles = Article::where('channel_id', 3)->search($request->input('keyword'))->get();
        $discussions = Discussion::where('channel_id', 3)->search($request->input('keyword'))->get();
        $surveys = Survey::where('channel_id', 3)->search($request->input('keyword'))->get();        

        $users = User::with('profile','avatar')->whereHas('roles', function($q){
                $q->where('role_id' ,'=', 2);
            })->search($request->input('keyword'))->get();
        
        $hospitals = Hospital::with('location','avatar')->where('verified',1)->search($request->input('keyword'))->get();
        $news = NewsFeed::where('channel_id',2)->search($request->input('keyword'))->get();

        return view('patient.pages.search.show' , compact(
            'articles',
            'discussions',
            'surveys',
            'users',
            'hospitals',
            'news'
        ));
    }

}
