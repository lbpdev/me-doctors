<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Commands\UpdateUserAchievements;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\UserRepository;

class UserAchievementsController extends Controller {

	/**
     * Create UserEducationController instance.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;

        parent::__construct();
    }
    

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     * 
     * @return Response
     */
    public function update(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);
        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

        $achievements = $this->userRepository->filterAchievementWithName($formFields['achievements']);

        $this->dispatch(new UpdateUserAchievements($user, $achievements , true));

        return redirect()->back();
    }

    /**
     * Update the educations of the user without Ajax.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateIE(Request $request)
    {
        $achievements = $this->userRepository->filterAchievementWithName($request->get('achievements'));

        $this->dispatch(new UpdateUserAchievements($this->user, $achievements , false));

        return redirect()->back();
    }


}
