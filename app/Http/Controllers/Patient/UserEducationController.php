<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\UpdateUserEducation;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\User;

class UserEducationController extends Controller {

	/**
	 * Create UserEducationController instance.
	 */
	public function __construct(UserRepository $userRepository)
	{
	    $this->middleware('auth');
        $this->userRepository = $userRepository;

	    parent::__construct();
	}

	/**
	 * Update the educations of the user.
	 *
	 * @param Request $request
	 * 
	 * @return Response
	 */
	public function update(Request $request)
	{
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);
		$education = $this->userRepository->filterEducationWithSchool($formFields['education']);

        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

		$this->dispatch(new UpdateUserEducation($user, $education , true));

		return redirect()->back();
	}
    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateIE(Request $request)
    {
        $education = $this->userRepository->filterEducationWithSchool($request->get('education'));
        $this->dispatch(new UpdateUserEducation($this->user, $education , false));
        return redirect()->back();
    }

}
