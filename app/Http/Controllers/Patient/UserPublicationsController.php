<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Commands\UpdateUserPublications;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\User;

class UserPublicationsController extends Controller {

    /**
     * Create UserEducationController instance.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;

        parent::__construct();
    }

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     * 
     * @return Response
     */
    public function update(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);
        $publications = $this->userRepository->filterPublicationsWithName($formFields['publications']);

        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

        $this->dispatch(new UpdateUserPublications($user, $publications , true));

        return redirect()->back();
    }

    /**
     * Update the educations of the user without Ajax.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateIE(Request $request)
    {
        $publications = $this->userRepository->filterPublicationsWithName($request->get('publications'));

        $this->dispatch(new UpdateUserPublications($this->user, $publications , false));

        return redirect()->back();
    }


}
