<?php namespace MEDoctors\Http\Controllers\Patient;

use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Commands\UpdateUserWork;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Repositories\Contracts\UserRepository;

use MEDoctors\Models\User;

class UserWorkController extends Controller {

    /**
     * Create UserEducationController instance.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;

        parent::__construct();
    }

    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);

        $work = $this->userRepository->filterWorkWithLocation($formFields['work']);
        $user = $this->user;

        if(isset($formFields['user_id']))
            $user = User::where('id',$formFields['user_id'])->first();

        $this->dispatch(new UpdateUserWork($user, $work , true));

        return redirect()->back();
    }


    /**
     * Update the educations of the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function updateIE(Request $request)
    {
        $work = $this->userRepository->filterWorkWithLocation($request->get('work'));
        $this->dispatch(new UpdateUserWork($this->user, $work , false));

        return redirect()->back();
    }


}
