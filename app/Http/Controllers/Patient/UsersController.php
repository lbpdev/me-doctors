<?php namespace MEDoctors\Http\Controllers\Patient;

use hisorange\BrowserDetect\Provider\BrowserDetectService;
use Illuminate\Http\Request;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Models\Language;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Models\User;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\Therapy;

use Illuminate\Support\Facades\Session;
use MEDoctors\Events\UserViewed;

class UsersController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;
    
    /**
     * Create UsersController instance.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository , HospitalRepository $hospitalRepository)
    {
        $this->userRepository = $userRepository;
        $this->hospitalRepository = $hospitalRepository;
        $this->user = \Auth::user();
        $this->middleware('auth');

        parent::__construct();
    }
        
    /**
     * Show the user profile.
     *
     * @param string $username
     * 
     * @return Response
     */
    public function show($username , Request $request)
    {
        $user = $this->userRepository->getByUsername($username);

        $user->load('specialties',
            'hospitals.location',
            'hospitals.schedules',
            'educations',
            'languages',
            'achievements',
            'publications',
            'certifications',
            'kol',
            'contacts');

        $languages = Language::orderBy('name')->select('name', 'id')->get()->toArray();
        $hospitals = $this->hospitalRepository->generateAutoLoadData(Hospital::with('location')->get());
        $this->hospitalRepository->generateHospitalSchedules($user);
        $user->languages = $user->languages->toArray();
        $agent = \BrowserDetect::detect()->browserFamily;

        if($user)
        {
            $viewerId = $this->user ? $this->user->id : 0;
            event(new UserViewed($user, $request->ip() , $viewerId));
        }

        return view('patient.pages.users.show', compact('user','languages','agent','hospitals'));
    }

    /**
     * Update the profile of the user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $this->dispatchFrom('MEDoctors\Commands\DoctorProfileUpdate', $request, [
            'user' => $this->user
        ]);
    }

    public function edit_profile($username)
    {
        $user = $this->userRepository->getByUsername($username);
        $user->load('profile','contacts');

        return view('patient.pages.users.profile.edit', compact('user'));
    }

    public function kolize()
    {
        $this->user->kol()->create([]);

        return redirect()->back();
    }


    public function update_profile(Request $request)
    {
        $input = $request->all();

        $file = $request->file('photo');
        $account = $request->only('fname','lname','email','password','designation','username');


        $user = $this->user;

        if(isset($input['user_id']))
            $user = User::where('id',$input['user_id'])->first();

        $this->dispatchFrom('MEDoctors\Commands\UpdateUser', $request, [
            'user'      => $user ,
            'account'   => $account,
            'therapies' => []
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserProfile', $request, [
            'user'    => $this->user ,
            'profile' => $input,
            'file'    => $file,
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserContacts', $request, [
            'user'    => $user,
            'contacts' => $input['phone_number'],
        ]);

        $this->dispatchFrom('MEDoctors\Commands\UpdateUserAvatar', $request, [
            'user'   => $user ,
            'file'   => $file,
            'remove' => isset($input['remove_thumb']) ? true : false
        ]);

        Session::flash('message', 'Profile has been updated successfully.');
        return redirect(route('patient.edit_personal_profile',$this->user->username));
    }

}
