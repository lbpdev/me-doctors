<?php namespace MEDoctors\Http\Controllers;

use Queue;
use MEDoctors\Http\Requests;
use Illuminate\Http\Request;
use MEDoctors\Http\Controllers\Controller;


class QueuesController extends Controller {

    /**
     * Handle queues.
     *
     * @return mixed
     */
	public function receive()
    {
        return Queue::marshal();
    }

}
