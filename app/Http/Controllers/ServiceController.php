<?php namespace MEDoctors\Http\Controllers;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\AdPending;
use MEDoctors\Models\AdPendingItem;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\PremiumService;
use MEDoctors\Models\ServiceTransaction;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Services\TC;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutSale;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutError;

use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Services\Uploaders\AdBannerUploader;

class ServiceController extends Controller {

    public function __construct(
            UserRepository $userRepository,
            HospitalRepository $hospitalRepository,
            AdBannerUploader $adBannerUploader
    ){
        $this->users    = $userRepository;
        $this->hospitals    = $hospitalRepository;
        $this->uploader    = $adBannerUploader;
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * The landing page.
     *
     *
     * @return Response
     */
    public function advertise()
    {
        $products = PremiumService::where('slug','!=','platinum-account')->get();
        $pending_purchases = AdPending::with('items.product','items.uploads')->where('user_id',$this->user->id)->get();
        return view('doctor.pages.advertise',compact('products','pending_purchases'));
    }


	public function storeQueue(Request $request){
        $input = $request->input('product');

        $pAd = AdPending::create(array('user_id'=>$this->user->id));

        foreach($input as $product){
            if(isset($product['premium_service_id'])){

                $qty = $product['quantity'];
                unset($product['quantity']);

                for($x=0;$x<$qty;$x++){
                    $pAd->items()->create($product);
                }
            }
        }

        return redirect()->back();
    }


	public function updateQueue(Request $request){
        $input = $request->input('orders');
        $files = $request->file('orders');
        $data = null;

        foreach($files as $oIndex => $orders)
            foreach($orders['items'] as $iIndex => $item){
                foreach($item as $fIndex => $file){
                    $indexedItem = $input[$oIndex]['items'][$iIndex];
                    if($file){
                        $pItem = AdPendingItem::where('id',$indexedItem['item_id'])->first();

                        $photo = ($file != null ? $this->uploader->upload($file) : false);

                        if($photo){
                            $pItem->uploads()->createMany($photo);
                            $pItem->status = 1;
                            $pItem->save();

                            if(isset($indexedItem['url']))
                                $pItem->link()->create(array('url' => $indexedItem['url']));
                        }
                    }
                }
            }

        return redirect()->back();
    }

	public function doctorPaymentCallback(){

        $transaction = ServiceTransaction::create($_GET);

        $user = User::where('id',$_GET['user_id'])->select('username')->first();

        if($transaction){
//            TC::username('ellorin_gene');
//            TC::password('Washburn18');
//            TC::sandbox(true);  #Uncomment to use Sandbox
//            TC::verifySSL(false);
//
//            $args = array(
//                'sale_id' => $transaction->order_number
//            );
//
//            try {
//                $result = TwocheckoutSale::retrieve($args);
//                dd($result);
//            } catch (TwocheckoutError $e) {
//                $e->getMessage();
//                dd($result);
//            }

            $this->users->availService($user->username,'platinum-account');

            return redirect(route('doctor.profile',$user->username));
        }
    }

	public function hospitalPaymentCallback(){

        $data = $_GET;
        $data['product_id'] = $data['merchant_product_id'];

        $transaction = ServiceTransaction::create($data);


        $hospital = Hospital::where('id',$_GET['hospital_id'])->select('slug')->first();

        if($transaction){
//            TC::username('ellorin_gene');
//            TC::password('Washburn18');
//            TC::sandbox(true);  #Uncomment to use Sandbox
//            TC::verifySSL(false);
//
//            $args = array(
//                'sale_id' => $transaction->order_number
//            );
//
//            try {
//                $result = TwocheckoutSale::retrieve($args);
//                dd($result);
//            } catch (TwocheckoutError $e) {
//                $e->getMessage();
//                dd($result);
//            }

            $this->hospitals->availService($_GET['hospital_id'],'platinum-account');

            return redirect(route('doctor.hospitals.show',$hospital->slug));
        }
    }

	public function paymentCallback(){

        $run = 1;
        $index = 0;
        $data = [];
        $payments = [];

        $getData = $_GET;
        ksort($getData);

        while($run){
            if(isset($getData['li_'.$index.'_name'])){
                foreach($getData as $key=>$get){

                    if($key == "li_".$index."_name")
                        $data['product_description'] = $get;

                    elseif($key == "li_".$index."_price")
                        $data['total'] = $get;

                    elseif($key == "li_".$index."_quantity")
                        $data['quantity'] = $get;

                    elseif($key == "li_".$index."_product_id")
                        $data['product_id'] = $get;

                    elseif($key == "li_".$index."_description")
                        $data['product_description'] = $get;

                    elseif($key == "li_".$index."_tangible")
                        $data['tangible'] = $get;

                    elseif($key == "li_".$index."_type")
                        $data['type'] = $get;

                    elseif($key == "li_".$index."_transaction_id")
                        $payment = $get;

                    else
                        $data[$key] = $get;

                    if(!isset($getData['li_'.($index+1).'_name']))
                        $run = 0;
                }
            } else {
                $run = 0;
            }

            $transaction = ServiceTransaction::create($data);

            $pending = AdPendingItem::where('id',$payment)->first();
            $pending->status = 3;
            $pending->save();

            $pending->transaction()->attach($transaction->id);

            $index++;
        }

        foreach($payments as $payment){

        }

        return redirect(url('doctor/advertise'));
    }

}
