<?php namespace MEDoctors\Http\Controllers;

use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use Illuminate\Http\Request;
use MEDoctors\Models\Upload;
use Carbon\Carbon;
use Image;

use MEDoctors\Models\UploadsMeta;
use MEDoctors\Models\Profile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadsController extends Controller {


    public static function uploadFile(UploadedFile $file , $uploadType){

        if ($file!=null) {
            $extension = $file->getClientOriginalExtension();
            $entry = new Upload();
            $entry->mime_type = $file->getClientMimeType();
            $full_file_name = $file->getClientOriginalName();

            $file_name = pathinfo($full_file_name, PATHINFO_FILENAME);

//            if ($uploadType == 'avatars'){
//                $new_name = str_random(30);
//                $entry->name = $new_name . '.' . $extension;
//            }

            $file_name_exists = true;
            $endWhile = false;
            $time_stamp = Carbon::now();
            $time_stamp = str_replace(":","_",$time_stamp);
            $time_stamp = str_replace(" ","_",$time_stamp);


            while($file_name_exists==true || $endWhile==false) {
                $file_name_exists = Upload::where('name', $file_name . '.' . $extension )->exists();
                if ($file_name_exists){
                    $file_name = pathinfo($full_file_name, PATHINFO_FILENAME);
                    $file_name .= '_'.$time_stamp;

                    $file_name_exists = false;

                } else {
                    $endWhile = true;
                }
            }

            $entry->path = $uploadType;
            $entry->name = $file_name . '.' . $extension;

            $allowed = array("jpeg", "gif", "png" , "jpg");
            if(in_array($extension, $allowed)) {

                if($uploadType=="comments" || $uploadType=="attachments"){
                    if(getimagesize($file)[0]>960)
                        Image::make($file->getRealPath())->resize(960, 765)->save(public_path('uploads/' . $uploadType . '/' . $entry->name));
                    elseif(getimagesize($file)[0]<960 && getimagesize($file)[0]>320)
                        Image::make($file->getRealPath())->resize(640, 510)->save(public_path('uploads/' . $uploadType . '/' . $entry->name));
                    else
                        Image::make($file->getRealPath())->resize(320, 255)->save(public_path('uploads/' . $uploadType . '/' . $entry->name));
                }
                else{
                    if(getimagesize($file)[0]<960 && getimagesize($file)[0]>320)
                        Image::make($file->getRealPath())->resize(640, 510)->save(public_path('uploads/' . $uploadType . '/' . $entry->name));
                    else
                        Image::make($file->getRealPath())->resize(320, 255)->save(public_path('uploads/' . $uploadType . '/' . $entry->name));
                }
            } else {
                $file->move('uploads/' . $uploadType, $entry->name);
            }

            $entry->save();
        }


        return $entry;
    }

    public static function storeUploadMeta($file){
        dd();
    }

    public static function getUploadURL($id){
        $upload = Upload::where('id' , $id)->first();
        $url = $upload->path.'/'.$upload->name;

        return $url;
    }

    public static function getUserPhoto($user_id){
        $profile = Profile::where('user_id' , $user_id)->first();
        $url = null;
        if($profile) {
            $photo = Upload::where('id', $profile->avatar)->first();
            $url = ($photo != null ? $photo->path . '/' . $photo->name : '');
        }
        return $url;
    }


    public static function destroyUpload($upload_id){
        Upload::where('id' , $upload_id)->delete();
        UploadsMeta::where('upload_id' , $upload_id)->delete();

        return true;
    }
}
