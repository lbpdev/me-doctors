<?php namespace MEDoctors\Http\Controllers;

use Illuminate\Support\Facades\Session;
use MEDoctors\Http\Requests;
use MEDoctors\Http\Controllers\Controller;

use MEDoctors\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\User;
use MEDoctors\Models\Profile;
use MEDoctors\Models\RoleUser;
use MEDoctors\Models\UserVerification;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

use MEDoctors\Commands\UpdateUserEducation;
use MEDoctors\Commands\UpdateUserAchievements;
use MEDoctors\Commands\UpdateUserPublications;
use MEDoctors\Commands\UpdateUserLanguages;
use MEDoctors\Commands\UpdateUserCertifications;
use MEDoctors\Commands\UpdateUserWork;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Commands\CheckValidity;

use Illuminate\Cookie\CookieJar;

class UsersController extends Controller {


    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

	public function index()
	{
		//
	}


	public function create()
	{
		return view('pages.register');
	}

	public function store(RegisterRequest $request)
	{

        $input = $request->input();

        foreach($input['therapies'] as $therapy)
            $therapies[] = array('therapy_id'=>$therapy);

        $input['password']= Hash::make($request->get('password'));

        $user = User::create($input);
        $role = 2;

        if($request->is('patient/register'))
            $role = 3;

        $user->roles()->sync(
            array(1 => array('user_id' => $user->id , 'role_id' => $role))
        );

        $user->specialties()->sync($therapies);

        $input['confirmation_code'] = str_random(30);

        Session::reflash();
        Session::flash('Message' , "Registered Successfully. Please check you email for verification");


//        $verification = UserVerification::create(['confirmation_code' => $input['confirmation_code'], 'user_id' => $user->id]);
//
//        if ($verification){
//            Mail::send('emails.verification', $input, function ($message) {
//                $message->from('no-reply@middleeastdoctor.me', 'MED Website User Verification');
//
//                $message->to(Input::get('email'))->subject('Middle East Doctor User Verification');
//
//            });
//        }
//
        if($role==2)
            return redirect('login');

        return redirect('patient/login');
    }

	public function verify($confirmation_code)
	{
        $verified = UserVerification::where('confirmation_code', $confirmation_code)->first();

        if($verified){
            $user = User::where('id', $verified->user_id);
            $user->update(['status'=>1]);
            echo 'Your account has been verified. <a href="'.route('login').'">Back to login.</a>';
        }

	}


    /**
     * Check username and email availability.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkValidity(Request $request)
    {
        $inputData = $request->get('formData');
        parse_str($inputData, $formFields);

        echo $this->dispatch(new CheckValidity($formFields['email'],$formFields['username']));

    }


    /**
     * Check client's location.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function countryCheck(Request $request, CookieJar $cookieJar)
    {
        $country_code = $request->get('country');

        $valid_countries = [
            'AM', 'AZ', 'BH', 'GE', 'IR', 'IQ',
            'IL', 'JO', 'KW', 'LB', 'NT', 'OM',
            'QA', 'SA', 'SY', 'TR', 'TM', 'AE', 'YE'
        ];

        if(in_array($country_code, $valid_countries)){
            return redirect(route('login_verified'));
        }
        else {
            Session::flash('country_message','Sorry but membership is only available doctors in middle-east');
            return redirect()->back();
        }
    }
}
