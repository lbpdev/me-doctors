<?php namespace MEDoctors\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */

	protected $routeMiddleware = [
		'auth' => 'MEDoctors\Http\Middleware\Authenticate',
		'authp' => 'MEDoctors\Http\Middleware\AuthenticatePatient',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'MEDoctors\Http\Middleware\RedirectIfAuthenticated',
		'admin' => 'MEDoctors\Http\Middleware\MustBeAdmin',
		'doctor' => 'MEDoctors\Http\Middleware\MustBeDoctor',
		'addoc' => 'MEDoctors\Http\Middleware\MustBeAdminOrDoctor',
	];

	/**
	 * Bootstrap the application for HTTP requests.
	 * 
	 * @return void
	 */
	public function bootstrap()
    {
        parent::bootstrap();

        if ($this->app->environment() == 'local')
        {
            $this->pushMiddleware('Clockwork\Support\Laravel\ClockworkMiddleware');
        }
    }

}
