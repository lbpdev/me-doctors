<?php namespace MEDoctors\Http\Middleware;

use Closure;

class MustBeAdminOrDoctor {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if (!$user)
            return $next($request);

        if($user->hasRole('administrator') || $user->hasRole('doctor'))
        {
            return $next($request);
        }

        return view('errors.doctors-only', compact('user'));
    }

}
