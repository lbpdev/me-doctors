<?php namespace MEDoctors\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
    private $openRoutes = [
        'doctor.education.update'
    ];

	public function handle($request, Closure $next)
	{
		return parent::handle($request, $next);
	}

}
