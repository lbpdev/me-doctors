<?php namespace MEDoctors\Http\Requests;

use MEDoctors\Http\Requests\Request;

class RegisterRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'email' => 'required|unique:users,email',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'designation' => 'required',
            'education' => '',
            'achievements' => '',
            'publications' => '',
            'languages' => '',
            'languages' => '',
            'certifications' => '',
            'work' => '',
		];
	}

}
