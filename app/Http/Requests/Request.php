<?php namespace MEDoctors\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

    /**
     * @var \MEDoctors\Models\User
     */
    protected $user;
    
    public function __construct()
    {
        $this->user = Auth::user();
    }
    

}
