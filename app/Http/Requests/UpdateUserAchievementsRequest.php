<?php namespace MEDoctors\Http\Requests;

use MEDoctors\Http\Requests\Request;

class UpdateUserAchievementsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];

		foreach ($this->request->get('achievements') as $key => $achievement)
		{
		    $rules['achievements.' . $key . '.name'] = 'required';
		}

		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{
		$messages = [];

		foreach($this->request->get('achievements') as $key => $achievement)
		{
			$messages['achievements.'.$key.'.name.required'] = 'The "name '.$key.'" field is required.';
		}

		return $messages;
	}

}
