<?php namespace MEDoctors\Http\Requests;

use MEDoctors\Http\Requests\Request;

class UpdateUserEducationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];

		foreach ($this->request->get('education') as $key => $education)
		{
		    $rules['education.' . $key . '.school'] = 'required';
		}

		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{
		$messages = [];

		foreach($this->request->get('education') as $key => $education)
		{
			$messages['education.'.$key.'.school.required'] = 'The "school '.$key.'" field is required.';
		}

		return $messages;
	}

}
