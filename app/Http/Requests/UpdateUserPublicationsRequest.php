<?php namespace MEDoctors\Http\Requests;

use MEDoctors\Http\Requests\Request;

class UpdateUserPublicationsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];

		foreach ($this->request->get('publications') as $key => $publication)
		{
		    $rules['publications.' . $key . '.name'] = 'required';
		}

		return $rules;
	}

	/**
	 * Set custom messages for validator errors.
	 *
	 * @return array
	 */
	public function messages()
	{
		$messages = [];

		foreach($this->request->get('publications') as $key => $publication)
		{
			$messages['publications.'.$key.'.name.required'] = 'The "name '.$key.'" field is required.';
		}

		return $messages;
	}

}
