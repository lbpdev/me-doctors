<?php namespace MEDoctors\Http\ViewComposers;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use MEDoctors\Models\Ad;
use MEDoctors\Models\AdType;

class AdComposerPatient {

    /**
     * Create TodayComposer instance.
     *
     */
    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * Compose the view.
     *
     * @param  View   $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $ads['sidebar-sm'] = Ad::whereHas('adType',function($q){
            $q->where('slug','sidebar-sm');
        })->where('ad_start','<=',Carbon::now()->format('Y-m-d'))
          ->where('ad_end','>=',Carbon::now()->format('Y-m-d'))
          ->where('active',1)
          ->where('channel_id',3)
          ->orderByRaw("RAND()")->first();

        $ads['sidebar-long'] = Ad::whereHas('adType',function($q){
            $q->where('slug','sidebar-long');
        })->where('ad_start','<=',Carbon::now()->format('Y-m-d'))
          ->where('ad_end','>=',Carbon::now()->format('Y-m-d'))
          ->where('active',1)
          ->where('channel_id',3)
          ->orderByRaw("RAND()")->first();


        $ads['top-sm'] = Ad::whereHas('adType',function($q){
            $q->where('slug','top-sm');
        })->where('ad_start','<=',Carbon::now()->format('Y-m-d'))
          ->where('ad_end','>=',Carbon::now()->format('Y-m-d'))
          ->where('active',1)
          ->where('channel_id',3)
          ->orderByRaw("RAND()")->first();


        $ads['top-long'] = Ad::whereHas('adType',function($q){
            $q->where('slug','top-long');
        })->where('ad_start','<=',Carbon::now()->format('Y-m-d'))
          ->where('ad_end','>=',Carbon::now()->format('Y-m-d'))
          ->where('active',1)
          ->where('channel_id',3)
          ->orderByRaw("RAND()")->first();

        $view->with('ads', $ads );
    }


}