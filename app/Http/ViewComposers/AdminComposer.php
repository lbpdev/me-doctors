<?php namespace MEDoctors\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

class AdminComposer {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;
    
    /**
     * Create AdminComposer instance.
     *
     * @param DiscussionRepository $discussions [description]
     */
    public function __construct(
        DiscussionRepository $discussions,
        HospitalRepository $hospitalRepository,
        UserRepository $userRepository
    )
    {
        $this->users = $userRepository;
        $this->discussions = $discussions;
        $this->hospitals = $hospitalRepository;
    }

    /**
     * Compose the view.
     *
     * @param  View   $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $data['discussions'] = $this->discussions->countUnpublishedPanelDiscussions();
        $data['diagnosis'] = $this->discussions->countUnpublishedDoctorDiscussions();
        $data['comments'] = $this->discussions->countUnpublishedComments();
        $data['hospitals'] = $this->hospitals->countUnverifiedHospitals();
        $data['doctors'] = $this->users->countUnverifiedDoctors();

        $view->with('notifications', $data);
    }
}