<?php namespace MEDoctors\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use MEDoctors\Models\ConversationMember;

class MessagesComposer {

    /**
     * Create TodayComposer instance.
     *
     */
    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * Compose the view.
     *
     * @param  View   $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $unreads = [];

        if($this->user){
            $members = ConversationMember::where('user_id', $this->user->id)
                ->with(['conversation','conversation.members', 'conversation.author'])
                ->orderBy('created_at' , 'DESC')->get();

            foreach($members as $member){
                foreach($member->conversation->messages as $message){
                    $read = $message->reads()
                            ->where('user_id', $this->user->id)
                            ->where('message_id', $message->id)
                            ->get();

                    if(count($read)<1)
                        $unreads[] = $message;
                }
            }
        }
        $view->with('unread_messages', count($unreads) == 0 ? '' : count($unreads) );
    }


}