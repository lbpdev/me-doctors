<?php namespace MEDoctors\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Repositories\Contracts\DiscussionRepository;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\UserRepository;

class NewsTickerPatientComposer {

    /**
     * @var DiscussionRepository
     */
    protected $discussions;

    /**
     * Create AdminComposer instance.
     *
     * @param DiscussionRepository $discussions [description]
     */
    public function __construct()
    {
    }

    /**
     * Compose the view.
     *
     * @param  View   $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $data = NewsFeed::where('channel_id',3)->orderBy('created_at','DESC')->paginate(5);

        $view->with('news', $data);
    }
}