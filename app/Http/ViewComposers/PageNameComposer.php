<?php

namespace MEDoctors\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class PageNameComposer {

    public function __construct(Request $request){
        $this->request = $request;
    }

    public function compose(View $view){
        $pageName = "Official Website";

        $pageNames = [
            "login"      => 'Log-in',
            "articles"       => 'Articles',
            'discussions'    => 'Discussions',
            'diagnosis'    => 'Diagnosis',
            '/doctors'    => 'Doctors Directory',
            'hospitals'    => 'Hospitals Directory',
            'about'    => 'About MED',
            'contact'    => 'Contact Us',
        ];

        foreach($pageNames as $key=>$pname){
            if(strpos($this->request->url(), (string)$key))
                $pageName = $pname;
        }

        $view->with('pageName', $pageName);
    }

}