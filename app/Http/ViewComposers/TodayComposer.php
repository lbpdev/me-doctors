<?php namespace MEDoctors\Http\ViewComposers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Torann\GeoIP\GeoIP;
use Illuminate\Contracts\View\View;

class TodayComposer {

    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzle;

    /**
     * @var \Torann\GeoIP\GeoIP
     */
    protected $locator;
    
    /**
     * Create TodayComposer instance.
     *
     * @param Client $guzzle
     * @param GeoIP $locator
     */
    public function __construct(Client $guzzle, GeoIP $locator)
    {
        $this->guzzle = $guzzle;
        $this->locator = $locator;
    }
    
    /**
     * Compose the view.
     *
     * @param  View   $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $response = $this->guzzle->get($this->sunriseSunsetApiUrl())->json();

        $view->with('today', $this->buildTodayData($response));
    }

    /**
     * Build today data payload that the views can work with.
     *
     * @param  array $data
     *
     * @return array
     */
    private function buildTodayData($data = [])
    {
        $location = $this->getLocation();
        $timezone = $location['timezone'];
        $today = [
            'sunrise'  => '',
            'sunset'   => '',
            'city'     => $location['city'],
            'timezone' => $timezone
        ];

        if (isset($data['results']))
        {
            $sunriseSunset = $data['results'];
            $today['sunrise'] = $this->formatTime($sunriseSunset['sunrise'], $timezone);
            $today['sunset'] = $this->formatTime($sunriseSunset['sunset'], $timezone);
        }

        return $today;
    }

    /**
     * Format time to hour and minutes.
     *
     * @param  string $time
     * @param  string|null $timezone
     *
     * @return string
     */
    private function formatTime($time, $timezone = null)
    {
        $newTime = Carbon::createFromFormat('h:i:s A', $time, 'UTC');

        return $newTime->timezone($timezone)->format('H:i');
    }

    /**
     * Create url request api.
     *
     * @return string
     */
    private function sunriseSunsetApiUrl()
    {
        $location = $this->getLocation();
        $url = 'http://api.sunrise-sunset.org/json?';

        return $url . http_build_query([
           'lat' => $location['lat'],
           'lng' => $location['lon']
        ]);
    }

    /**
     * Get the location based on the user's ip.
     *
     * @return array
     */
    private function getLocation()
    {
        return $this->locator->getLocation();
    }
}