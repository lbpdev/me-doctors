<?php

Route::get('/', function(){
    return redirect('/patient');
});

Route::get('/landing', 'Doctor\PagesController@landing');
Route::post('/kimonofeeds', 'KimonoFeedsController@index');
Route::get('/kimonofeedsGet', 'KimonoFeedsController@get');
Route::get('/phpinfo', function(){
    phpinfo();
});

// Authentication Routes
Route::get('verify/{code}', ['as' => 'verify_user', 'uses' => 'UsersController@verify']);
Route::post('mail', ['as' => 'send_mail' , 'uses' => 'MailController@send_mail']);
Route::post('refer', ['as' => 'refer.send' , 'uses' => 'MailController@sendReferral']);

/**
 * Handle Queues
 */
Route::post('queue/receive', 'QueuesController@receive');


Route::get('forgot-password', [
    'as' => 'password.forgot',
    'uses' => 'Auth\PasswordController@getEmail'
]);

Route::get('patient/forgot-password', [
    'as' => 'patient.password.forgot',
    'uses' => 'Auth\PasswordController@getEmailPatient'
]);

Route::post('forgot-password', [
    'as' => 'password.forgot',
    'uses' => 'Auth\PasswordController@postEmail'
]);

Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\PasswordController@getReset'
]);

Route::post('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\PasswordController@postReset'
]);

Route::get('resend-verification', [
    'as' => 'resend-verify.create',
    'uses' => 'Auth\AuthController@getResend'
]);

Route::post('resend-verification', [
    'as' => 'resend-verify.execute',
    'uses' => 'Auth\AuthController@postResend'
]);

Route::get('doctor-premium-callback','ServiceController@doctorPaymentCallback');
Route::get('hospital-premium-callback','ServiceController@hospitalPaymentCallback');
Route::get('payment-callback','ServiceController@paymentCallback');

require_once(app_path().'/Routes/patient.php');
require_once(app_path().'/Routes/doctor.php');
require_once(app_path().'/Routes/admin.php');
