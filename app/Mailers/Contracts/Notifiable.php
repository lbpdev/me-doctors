<?php namespace MEDoctors\Mailers\Contracts;

interface Notifiable {

    /**
     * Get the email of the object to be notified.
     *
     * @return string
     */
    public function getEmail();
    
}