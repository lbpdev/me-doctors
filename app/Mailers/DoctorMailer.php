<?php namespace MEDoctors\Mailers;

use MEDoctors\Models\User;
use MEDoctors\Mailers\Mailer;

class DoctorMailer extends Mailer {
    
    /**
     * Notify doctor that his account has been verified.
     *
     * @param  User   $user
     *
     * @return void
     */
    public function sendVerifiedDoctorEmail(User $user)
     {
        $this->subject = 'Middle East Doctor - Account Verified';
        $this->view    = 'emails.doctor.verified';
        $this->data    = ['user' => $user];

        return $this->sendTo($user);
     }

    /**
     * Notify doctor that his account has been denied.
     *
     * @param  User   $user
     *
     * @return void
     */
    public function sendDeniedDoctorEmail(User $user)
    {
        $this->subject = 'Middle East Doctor - Account Denied';
        $this->view    = 'emails.doctor.denied';
        $this->data    = ['user' => $user];

        return $this->sendTo($user);
    }
}