<?php namespace MEDoctors\Mailers;

use Illuminate\Mail\Message;
use MEDoctors\Mailers\Contracts\Notifiable;
use Illuminate\Contracts\Mail\Mailer as MailerContract;

abstract class Mailer {

    /**
     * @var \Illuminate\Contracts\Mail\Mailer;
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $view;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var array
     */
    protected  $data = [];

    /**
     * @var array
     */
    protected  $attachments = [];

    /**
     * @param MailerContract $mailer
     */
    public function __construct(MailerContract $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Generic mailer sender.
     * 
     * @param $notifiable
     */
    public function sendTo(Notifiable $notifiable)
    {
        $subject = $this->subject;
        $attachments = $this->attachments;

        $this->mailer->queue($this->view, $this->data, function($message) use($notifiable, $subject, $attachments)
        {
            $message->to($notifiable->getEmail())->subject($subject);

            foreach ($attachments as $attachment)
            {
                $message->attach($attachment);
            }
        });
    }

}