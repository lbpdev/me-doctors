<?php namespace MEDoctors\Mailers;

use MEDoctors\Models\User;
use MEDoctors\Mailers\Mailer;

class UserMailer extends Mailer {
    
    /**
     * Send the confirmation email to the user.
     *
     * @param  User   $user
     *
     * @return void
     */
    public function sendEmailConfirmationTo(User $user)
    {
        $this->subject = 'Verify your email address';

        if($user->hasRole('patient'))
            $this->view    = 'emails.user.confirm-patient';
        else
            $this->view    = 'emails.user.confirm';

        $this->data    = ['user' => $user];

        return $this->sendTo($user);
     }

    /**
     * Notify user that his account has been deleted by the admin.
     *
     * @param  User   $user
     *
     * @return void
     */
    public function sendRemovedAccountEmail(User $user)
    {
        $this->subject = 'Middle East Doctor - Account Deleted';
        $this->view    = 'emails.user.removed';
        $this->data    = ['user' => $user];

        return $this->sendTo($user);
    }
}