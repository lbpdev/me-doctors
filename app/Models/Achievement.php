<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model {

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
