<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model {

    protected $fillable = ['title','description','link','ad_start','ad_end','image','channel_id','ad_type_id','ad_user_id','active'];
    protected $dates = ['ad_start','ad_end'];

    public function adType()
    {
        return $this->belongsTo('MEDoctors\Models\AdType');
    }

    public function adUser()
    {
        return $this->belongsTo('MEDoctors\Models\AdUser');
    }

    public function channel()
    {
        return $this->belongsTo('MEDoctors\Models\Channel');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * Gets the avatar for the user's profile
     *
     * @return string
     */
    public function getAdImageAttribute()
    {
        return $this->image ? url('/public').'/'.$this->image->url : 'http://placehold.it/240x240';
    }


    /**
     * Gets the avatar for the user's profile
     *
     * @return string
     */
    public function getChannelNameAttribute()
    {
        return $this->channel ? $this->channel->name : 'N/A';
    }


    /**
     * Gets the avatar for the user's profile
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->active ? 'Active' : 'Inactive';
    }

}
