<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdPending extends Model {

	protected $fillable = ['user_id','token'];

    public function items(){
        return $this->hasMany('MEDoctors\Models\AdPendingItem');
    }

    /**
     * An ad has an user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('MEDoctors\Models\User', 'id','user_id');
    }
}
