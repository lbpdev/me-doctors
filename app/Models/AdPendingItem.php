<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdPendingItem extends Model {

    protected $fillable = ['duration','status','premium_service_id'];

    public function product(){
        return $this->hasOne('MEDoctors\Models\PremiumService','id','premium_service_id');
    }

    /**
     * An ad has an image.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }


    /**
     * An ad item belongs to a pending ad.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pendingAd()
    {
        return $this->hasOne('MEDoctors\Models\AdPending', 'id','ad_pending_id');
    }


    /**
     * An ad item belongs to a pending ad.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaction()
    {
        return $this->belongsToMany('MEDoctors\Models\ServiceTransaction','ad_transactions','ad_pending_item_id','service_transaction_id');
    }


    /**
     * An ad item belongs to a pending ad.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function link()
    {
        return $this->hasOne('MEDoctors\Models\AdPendingItemLink','ad_pending_item_id');
    }

    /**
     * An ad item belongs to a pending ad.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getTransactionAttribute()
    {
        return $this->transaction()->first();
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'original')->first();
        return $thumb ? 'public/'.$thumb->url : 'http://placehold.it/320x240';
    }

}
