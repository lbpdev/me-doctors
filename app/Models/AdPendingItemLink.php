<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdPendingItemLink extends Model {

	protected $fillable = ['ad_pending_item_id','url'];

}
