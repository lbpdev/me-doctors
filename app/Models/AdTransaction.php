<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdTransaction extends Model {

	protected $fillable = ['service_transaction_id','ad_pending_item_id'];

    public function ad_pending_item()
    {
        return $this->hasOne('App\Models\AdPendingItem');
    }
}
