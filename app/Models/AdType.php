<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdType extends Model {

    protected $fillable = ['name','slug','location'];

	public $timestamps = false;

    public function ads(){
        return $this->hasMany('MEDoctors\Models\Ad');
    }

    public function getNameSizeAttribute(){
        return $this->name. ' ( ' . $this->sizeW . ' X ' . $this->sizeH . ' ) ';
    }
}
