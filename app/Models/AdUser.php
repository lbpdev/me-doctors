<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class AdUser extends Model {

    protected $fillable = ['email' , 'name'];

    /**
     * A user can have multiple phone numbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contacts()
    {
        return $this->morphMany('MEDoctors\Models\PhoneNumber', 'phoneable');
    }
    /**
     * A user can have multiple phone numbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function ads()
    {
        return $this->hasMany('MEDoctors\Models\AdUser');
    }


}
