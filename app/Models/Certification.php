<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model {

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
