<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['name', 'slug'];

    /**
     * A channel has many discussions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discussions()
    {
        return $this->hasMany('MEDoctors\Models\Discussion');
    }

    /**
     * A channel has many Articles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('MEDoctors\Models\Article');
    }


}