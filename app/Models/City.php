<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    public $table = 'cities';
    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country_code', 'district', 'population', 'name'];

    /**
     * Get the country associated with the city.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('MEDoctors\Models\Country', 'country_code', 'code');
    }

}
