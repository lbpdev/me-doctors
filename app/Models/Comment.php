<?php namespace MEDoctors\Models;

use MEDoctors\Models\Upload;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    use HasAttachments;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['discussion_id', 'user_id', 'content' , 'status'];

    /**
     * A comment belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('MEDoctors\Models\User', 'user_id');
    }

    /**
     * Create a new comment instance.
     *
     * @param  string $content
     * @param  int $userId
     *
     * @return mixed
     */
    public function post($content, $userId)
    {
        return new static([
            'content' => $content,
            'user_id' => $userId
        ]);
    }

    /**
     * A comment belongs to a discussion.
     * @return mixed
     */
    public function discussion()
    {
        return $this->belongsTo('MEDoctors\Models\Discussion', 'discussion_id');
    }
    
}
