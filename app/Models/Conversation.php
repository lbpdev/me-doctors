<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model {

    use SearchableTrait;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'subject' => 10,
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['subject' , 'author_id'];

    /**
     * A conversation has many messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function messages()
    {
        return $this->hasMany('MEDoctors\Models\Message')->orderBy('created_at','desc')->withTrashed();
    }

    /**
     * A conversation can have multiple members.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function members()
    {
        return $this->belongsToMany('MEDoctors\Models\User', 'conversation_members');
    }

    /**
     * A conversation belongs to an author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('MEDoctors\Models\User', 'author_id');
    }
}
