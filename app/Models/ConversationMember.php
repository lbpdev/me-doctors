<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class ConversationMember extends Model {

    use SearchableTrait;


    protected $dates = ['deleted_at','created_at','updated_at','left_at'];
    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'user_id' => 10,
        ]
    ];

    public $table = 'conversation_members';


    use SoftDeletes;


    /**
     * A member belongs to a conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function conversation()
    {
        return $this->belongsTo('MEDoctors\Models\Conversation', 'conversation_id');
    }

    /**
     * A member belongs to a conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function member()
    {
        return $this->belongsTo('MEDoctors\Models\User', 'user_id');
    }
}
