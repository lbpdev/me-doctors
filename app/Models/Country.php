<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Country codes for GCC countries.
     *
     * @var array
     */
    protected $gccCountryCodes = ['AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'region',
        'continent',
        'surface_area',
        'indep_year',
        'population',
        'life_expectancy',
        'gnp',
        'gnp_old',
        'local_name',
        'government_form',
        'head_of_state',
        'capital',
        'code2',
    ];

    /**
     * Get all the cities associated with the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('MEDoctors\Models\City', 'country_code', 'code');
    }

    /**
     * Get GCC countries only.
     *
     * @param  mixed $query
     *
     * @return mixed
     */
    public function scopeGccCountries($query)
    {
        return $query->whereIn('code', $this->getGccCountryCodes());
    }

    /**
     * Get GCC country codes.
     *
     * @return array
     */
    public function getGccCountryCodes()
    {
        return $this->gccCountryCodes;
    }

}
