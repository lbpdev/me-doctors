<?php namespace MEDoctors\Models;

trait CountsRelation {

    /**
     * Select query for counting relationships.
     *
     * @param $relation
     *
     * @return mixed
     */
    public function countRelation($relation)
    {
        return $relation->selectRaw($relation->getForeignKey() . ', count(*) AS count')
                        ->groupBy($relation->getForeignKey());
    }

    /**
     * Get the count from the query.
     *
     * @param $query
     *
     * @return int
     */
    public function getCount($query)
    {
        return $query->first() ? $query->first()->count : 0;
    }
}