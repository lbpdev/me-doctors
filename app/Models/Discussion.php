<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Discussion extends Model {

    use CountsRelation, HasAttachments ,SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [ 'title', 'content', 'therapy_id', 'slug', 'author_id' , 'status'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'content' => 10
        ]
    ];


    /**
     * The author of the discussion.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('MEDoctors\Models\User');
    }

    /**
     * A discussion has many comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('MEDoctors\Models\Comment');
    }

    /**
     * Comments count relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsCountRelation()
    {
        return $this->countRelation($this->comments()->where('status',1));
    }

    /**
     * Get comments count.
     *
     *
     * @return int
     */
    public function getCommentsCountAttribute()
    {
        return $this->getCount($this->commentsCountRelation);
    }

    /**
     * Get comments count.
     *
     * @return int
     */
    public function therapy()
    {
        return $this->belongsTo('MEDoctors\Models\Therapy');
    }

}
