<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorBedsideManner extends Model {

    protected $table = 'doctor_bedside_manners';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
