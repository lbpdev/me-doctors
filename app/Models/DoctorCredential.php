<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorCredential extends Model {

    use HasAttachments;

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'doctor_credentials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['phone_number', 'place_of_employment'];

    /**
     * Get the license files from the list of attachments.
     *
     * @return mixed
     */
    public function licenseFiles()
    {
        return $this->attachments->where('template', 'license');
    }

    /**
     * Get the photo identification files from the list of attachments.
     *
     * @return mixed
     */
    public function photoIdentifications()
    {
        return $this->attachments->where('template', 'photo_id');
    }

    /**
     * Get the photo identification files from the list of attachments.
     *
     * @return mixed
     */
    public function otherFiles()
    {
        return $this->attachments->where('template', '');
    }

}
