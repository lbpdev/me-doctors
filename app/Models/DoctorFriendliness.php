<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorFriendliness extends Model {

    protected $table = 'doctor_friendliness';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
