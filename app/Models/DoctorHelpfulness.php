<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorHelpfulness extends Model {

    protected $table = 'doctor_helpfulness';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
