<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorMedicalKnowledge extends Model {

    protected $table = 'doctor_medical_knowledge';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
