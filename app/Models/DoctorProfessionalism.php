<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorProfessionalism extends Model {

    protected $table = 'doctor_professionalism';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
