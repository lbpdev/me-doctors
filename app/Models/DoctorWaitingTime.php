<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorWaitingTime extends Model {

    protected $table = 'doctor_waiting_time';
    protected $fillable = [ 'user_id' , 'rater_id' , 'value' ];

}
