<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class DummyNewsFeed extends Model {


    protected $fillable = ['channel_id','title','subtitle','photo','url','content','slug','date_posted'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_posted'];


    /**
     * An article can have many views
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function views()
    {
        return $this->morphMany('MEDoctors\Models\View' , 'viewable');
    }

    /**
     * Views count relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewsCountRelation()
    {
        return $this->countRelation($this->views());
    }

    /**
     * Get the views count.
     *
     * @return int
     */
    public function getViewCountAttribute()
    {
        return $this->getCount($this->viewsCountRelation);
    }


    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? 'public/'.$thumb->url : 'http://placehold.it/320x240';
    }


    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getFeaturedImageAttribute()
    {
        $thumb = $this->uploads()->where('template', 'long')->first();

        return $thumb ? 'public/'.$thumb->url : 'http://placehold.it/480x280';
    }


    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getFullImageAttribute()
    {
        $thumb = $this->uploads()->where('template', 'full')->first();

        return $thumb ? 'public/'.$thumb->url : 'http://placehold.it/780x580';
    }

}
