<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'educations';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'school'];

    /**
     * A school belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('MEDoctors\Models\User');
    }

}
