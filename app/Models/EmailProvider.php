<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class EmailProvider extends Model {

	protected $fillable = ['name'];

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

}
