<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Nicolaslopezj\Searchable\SearchableTrait;

class Event extends Model {

    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
        'event_date',
        'event_date_end',
        'title',
        'content',
        'thumbnail',
        'city',
        'country',
        'state',
        'location',
        'slug'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['event_date','event_date_end'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'content' => 10,
            'city' => 10,
            'country' => 10,
            'state' => 10,
            'location' => 10,
        ]
    ];


    /**
     * Setup query for upcoming events.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeUpcoming($query)
    {
        return $query->where('event_date_end', '>=', Carbon::now()->format('Y/m/d'));
    }

    /**
     * Setup query for upcoming events.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePast($query)
    {
        return $query->where('event_date_end', '<', Carbon::now()->format('Y/m/d'));
    }

    /**
     * An event has a thumbnail.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? 'public/'.$thumb->url : asset('images/placeholders/event.jpg');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getlargeThumbAttribute()
    {
        $thumb = $this->uploads()->where('template', 'large')->first();

        return $thumb ? 'public/'.$thumb->url : asset('images/placeholders/event.jpg');
    }
}
