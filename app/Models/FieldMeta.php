<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class FieldMeta extends Model {

    protected $fillable = [
        'user_id' , 'field' , 'meta_type' , 'value'
    ];

}
