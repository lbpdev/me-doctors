<?php namespace MEDoctors\Models;

trait HasAttachments {

    /**
     * Get the attachments of the comment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * Get the images from the attachments.
     *
     * @return array
     */
    public function getImagesAttribute()
    {
        $images = [];
        $thumbnails = $this->attachments->where('template', 'thumb');

        foreach ($thumbnails as $thumbnail)
        {
            $images[] = [
                'thumb' => $thumbnail->url,
                'original' => $this->getOriginalUrl($thumbnail)
            ];
        }

        return $images;
    }

    /**
     * Get the url of the original image of the thumbnail.
     *
     * @param  \MEDoctors\Models\Upload $thumbnail
     *
     * @return string
     */
    private function getOriginalUrl(Upload $thumbnail)
    {
        $originalImage = $this->attachments->where('original_name', $thumbnail->original_name)->first();

        if ($originalImage)
        {
            return $originalImage->url;
        }

        return $thumbnail->url;
    }

    /**
     * Get the files from the attachments.
     *
     * @return array
     */
    public function getFilesAttribute()
    {
        return $this->attachments->where('template', '');
    }
}