<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Nicolaslopezj\Searchable\SearchableTrait;
use MEDoctors\Models\HospitalClaim;
use Illuminate\Support\Facades\Auth;

class Hospital extends Model {

    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug' , 'description' , 'website' , 'verified'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'name' => 10,
        ]
    ];

    /**
     * Get the location of the hospital.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function location()
    {
        return $this->morphOne('MEDoctors\Models\Location', 'locationable');
    }

    /**
     * a hospital has many views.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function views()
    {
        return $this->morphMany('MEDoctors\Models\View' , 'viewable');
    }

    /**
     * a hospital has many views.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function doctors()
    {
        return $this->belongsToMany('MEDoctors\Models\User' , 'hospital_doctors');
    }

    /**
     * Gets the avatar for the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->avatar()->first();

        return $thumb ? 'public/'.$thumb->url : asset('images/placeholders/hospital.png');
    }

    /**
     * Gets the primary phone number for the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getPhoneNumberAttribute()
    {
        $phone_numbers = $this->contacts()->get();
        return count($phone_numbers)>0 ? $phone_numbers[0]->number : 'N/A';
    }

    /**
     * Gets the primary phone number for the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getClaimedByCurrentUserAttribute()
    {
        $hospital = $this;
        $user = Auth::user();

        if($user){

            $claimed= HospitalClaim::where('hospital_id',$hospital->id)
                ->where('user_id',$user->id)
                ->where('verified',0)
                ->first();

            if($claimed)
                return true;
        }

        return false;
    }

    /**
     * Gets the primary phone number for the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getOwnedByCurrentUserAttribute()
    {
        $hospital = $this;
        $user = Auth::user();

        if($user){

            $claimed= HospitalClaim::where('hospital_id',$hospital->id)
                ->where('user_id',$user->id)
                ->where('verified',1)
                ->first();

            if($claimed)
                return true;
        }

        return false;
    }

    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getCurrentServiceAttribute()
    {
        $service = $this->services()->where('expires_on', '>' , date("Y-m-d H:i:s"))->first();

        return $service ? $service : false;
    }

    /**
     * Gets the primary phone number for the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getOwnedBySomeoneAttribute()
    {
        $hospital = $this;
        $user = Auth::user();

        if($user){

            $claimed= HospitalClaim::where('hospital_id',$hospital->id)
                ->where('verified',1)
                ->pluck('user_id');

            if($claimed)
                return true;
        }

        return false;
    }


    /**
     * Gets the City and Country of the hospital
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getBasicLocationAttribute()
    {
        $location = $this->location;

        if ( ! $location or ! $location->city or ! $location->country)
        {
            return 'N/A';
        }

        return $location->city . ', '. $location->country;
    }

    /**
     * a hospital has an avatar.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function avatar()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * A hospital user has many schedules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function schedules()
    {
        return $this->hasMany('MEDoctors\Models\HospitalSchedule');
    }

    /**
     * A hospital has therapies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function therapies()
    {
        return $this->belongsToMany('MEDoctors\Models\Therapy', 'hospital_therapies');
    }

    /**
     * A hospital has facilities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function facilities()
    {
        return $this->belongsToMany('MEDoctors\Models\HospitalFacility', 'hospital_facility_users');
    }

    /**
     * a hospital has many views.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function claims()
    {
        return $this->belongsToMany('MEDoctors\Models\HospitalClaim');
    }

    /**
     * A hospital can have multiple phone numbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contacts()
    {
        return $this->morphMany('MEDoctors\Models\PhoneNumber', 'phoneable');
    }


    /**
     * a user can be a KOL.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function services()
    {
        return $this->hasOne('MEDoctors\Models\ServiceHospitalUser','hospital_id');
    }

    /**
     * Combine the location attributes and return the whole address.
     *
     * @return string
     */
    public function getAddressAttribute()
    {
        $location = $this->location;
        $location = $location->street_number . ' '
                    . $location->street_route . ' '
                    . $location->state . ', '
                    . $location->city . ', '
                    . $location->country;

        return trim($location);
    }

    /**
     * Combine the location attributes and return the whole address.
     *
     * @return string
     */
    public function getDetails($request)
    {
        $data = [];
        foreach($request as $index=>$item){
            if($item['name']!=""){
                $data[$index] =  new static([
                    'name' => $item['name'],
                    'slug' => Str::slug($item['name']),
                    'verified' => 0
                ]);
            }
        }

        return $data;
    }

    /**
     * Combine the location attributes and return the whole address.
     *
     * @return Array
     */
    public function generateSchedule($user_id , $request)
    {
        foreach($request as $index=>$day){
            $data[$index] = [
                'user_id' => $user_id,
                'day'         => $day['day']
            ];
        }

        return $data;
    }

    /**
     * Generate Contact Proper format for create.
     *
     * @return Array
     */
    public function generateContactCreateData($contacts)
    {
        $data = null;
        if(count($contacts)>0){
            foreach($contacts as $index=>$contact){
                $data[$index] = [
                    'number' => $contact,
                    'type'   => $index<1 ? 'Primary' : 'Secondary'
                ];
            }
        }
        return $data;
    }

    /**
     * Generate Contact Proper format for create.
     *
     * @return Array
     */
    public function generateFacilitiesCreateData($facilities)
    {
        $data = null;
        if(count($facilities)>0){
            foreach($facilities as $index=>$facility){
                $data[$index] = [
                    'hospital_facility_id' => $facility,
                ];
            }
        }

        return $data;
    }

    /**
     * Generate Contact Proper format for create.
     *
     * @return Array
     */
    public function generateTherapiesCreateData($therapies)
    {
        $data = null;
        if(count($therapies)>0){
            foreach($therapies as $index=>$therapy){
                $data[$index] = [
                    'therapy_id' => $therapy,
                ];
            }
        }

        return $data;
    }

    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getIsPremiumAttribute()
    {
        if ($this->services()->where('active',1)->count())
            $flag = 1;
        elseif ($this->services()->where('active',0)->count())
            $flag = 2;
        else
            $flag = 0;

        return $flag;
    }

    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getPremiumAttribute()
    {
        if(isset($this->active))
            if($this->active)
                return true;

        return false;
    }
}
