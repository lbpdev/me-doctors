<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalClaim extends Model {

	protected $fillable = ['hospital_id','user_id','email','token'];

    public function hospital(){
        return $this->belongsTo('MEDoctors\Models\Hospital');
    }

    public function claimer(){
        return $this->belongsTo('MEDoctors\Models\User','user_id');
    }

}
