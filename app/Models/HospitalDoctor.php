<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalDoctor extends Model {

    protected $fillable = [
        'user_id' , 'hospital_id'
    ];

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;
}
