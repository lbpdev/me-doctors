<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalFacility extends Model {
    protected $table = 'hospital_facilities';

	protected $fillable = ['name','slug'];

    public $timestamps = false;
}
