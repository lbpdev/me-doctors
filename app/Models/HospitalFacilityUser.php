<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalFacilityUser extends Model {

    protected $fillable = [
        'hospital_id' , 'facility_id'
    ];

    public $timestamps = false;
}
