<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalSchedule extends Model {

    protected $table = 'hospital_schedules';

	protected $fillable = [
        'day' , 'hospital_id' , 'user_id'
    ];

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * A schedule belongs to a hospital.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hospital()
    {
        return $this->belongsTo('MEDoctors\Models\Hospital');
    }
}
