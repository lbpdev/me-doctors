<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalTherapy extends Model {

    protected $fillable = [
        'therapy_id' , 'hospital_id'
    ];

    public $timestamps = false;

}
