<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalUser extends Model {

    protected $table = 'hospital_user';

	protected $fillable = [
        'hospital_id' , 'user_id'
    ];

    public $timestamps = false;

    public function hospital(){
        return $this->belongsTo('MEDoctors\Models\Hospital');
    }
}
