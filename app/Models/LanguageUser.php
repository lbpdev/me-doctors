<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageUser extends Model {

    protected $table  = 'language_user';

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

}
