<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use MEDoctors\Models\Hospital;

class Location extends Model {

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','country', 'state', 'city', 'post_code', 'street_number', 'street_route'];

    /**
     * Get the model for the associated location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function locationable()
    {
        return $this->morphTo();
    }

    /**
     * Combine the location attributes and return the whole address.
     *
     * @return string
     */
    public function getAddressAttribute(Hospital $hospital ,$item)
    {
        $work =  new static([
            'name' => $item['location'],
            'street_number' => $item['street_number'],
            'street_route' => $item['street_route'],
            'city' => $item['locality'],
            'post_code' => $item['post_code'],
            'state' => $item['state'],
            'country' => $item['country']
        ]);

        return $work;
    }

}
