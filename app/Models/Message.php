<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Message extends Model {

    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'message' => 10,
        ]
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
	public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['conversation_id','sender_id','message'];


    /**
     * A message belongs to a conversation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conversation()
    {
        return $this->belongsTo('MEDoctors\Models\Conversation');
    }

    /**
     * The sender of the message.    
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function sender()
    {
        return $this->belongsTo('MEDoctors\Models\User', 'sender_id');
    }

    /**
     * The sender of the message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reads()
    {
        return $this->hasMany('MEDoctors\Models\MessageRead');
    }

}
