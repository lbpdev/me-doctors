<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class MessageRead extends Model {

    protected $table = 'message_read';

    protected $dates = ['read_at'];

    protected $fillable = ['user_id','message_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * A read object has a reader.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reader()
    {
        return $this->hasOne('MEDoctors\Models\User');
    }

    /**
     * A read object belongs to a message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('MEDoctors\Models\Message');
    }

}
