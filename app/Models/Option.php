<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model {

    protected $table = "options";

	protected $fillable = [
        'name' , 'value' , 'slug'
    ];

}
