<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['type', 'number'];

    /**
     * The model associated with the phone number.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function phoneable()
    {
        return $this->morphTo();
    }

    public function createInstance($request){
        $data[0] = new static([
            'type' => 'personal',
            'number' => $request[0]['personal']
        ]);

        $data[1] = new static([
            'type' => 'work',
            'number' => $request[0]['work']
        ]);
        return $data;
    }

    public function createInstanceSingle($type,$number){
        return new static([
            'type' => $type,
            'number' => $number
        ]);
    }
}
