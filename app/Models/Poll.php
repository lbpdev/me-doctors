<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['title', 'status'];

    /**
     * A poll can have many item choices.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('MEDoctors\Models\PollItem');
    }

    /**
     * Query for checking if the user has already voted an item from the poll.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  integer $userId
     *
     * @return static
     */
    public function scopeItemsWithUserVote($query, $userId)
    {
        return $query->with(['items' => function($query) use ($userId)
        {
            $query->leftJoin('poll_votes', function($join) use ($userId)
            {
                 $join->on('poll_votes.poll_item_id', '=', 'poll_items.id')
                            ->where('poll_votes.user_id', '=', $userId);
            })
            ->selectRaw('poll_items.id, poll_items.body, poll_items.poll_id, (poll_votes.id is not NULL) AS voted');
        }]);
    }

}
