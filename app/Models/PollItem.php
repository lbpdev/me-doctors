<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class PollItem extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['body'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'voted' => 'integer'
    ];


    /**
     * Get the item of the votes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votes()
    {
        return $this->hasMany('MEDoctors\Models\PollVote');
    }

}
