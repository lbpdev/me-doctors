<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class PollVote extends Model {

    protected $fillable = ['user_id', 'poll_id', 'poll_item_id'];

}