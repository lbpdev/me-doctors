<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class PremiumService extends Model {

	protected $fillable = ['name','description','slug','interval','price'];

    public $timestamps = false;

    public function users(){
        return $this->belongsToMany('MEDoctors\Models\ServiceUser');
    }
}
