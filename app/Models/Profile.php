<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id' , 'avatar_id' , 'bio'];
        
    /**
     * Gets the avatar for the profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function avatar()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    public function createInstance($request){
        return new static([
            'bio' => $request['bio']
        ]);
    }

}
