<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model {

    protected $fillable = ['receiver','user_id','referrer_id','registered'];

    public function referrer(){
        return $this->hasOne('MEDoctors\Models\User','referrer_id');
    }

    public function user(){
        return $this->hasOne('MEDoctors\Models\User','user_id');
    }


    public function getReferredByAttribute(){
        return User::where('id',$this->referrer_id)->first();
    }

    public function getReceivedByAttribute(){
        return User::where('email',$this->receiver)->first();
    }

    public function getRegisterStatusAttribute(){
        if($this->registered)
            return 'Registered';

        return 'Not Registered';
    }
}
