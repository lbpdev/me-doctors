<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the users of the role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('MEDoctors\Models\User');
    }

    /**
     * Get the users with role of doctor.
     *
     * @param  mixed
     *
     * @return mixed
     */
    public function scopeDoctors($query)
    {
        return $query->whereName('Doctor')->first()->users;
    }
    
}