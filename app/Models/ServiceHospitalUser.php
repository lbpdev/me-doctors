<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceHospitalUser extends Model {

    protected $fillable = ['hospital_id','premium_service_id','expires_on','active'];

    protected $dates = ['expires_on'];
}
