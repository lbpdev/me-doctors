<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceTransaction extends Model {

	protected $fillable = [
        'sid',
        'key',
        'order_number',
        'product_description',
        'lang',
        'currency_code',
        'invoice_id',
        'total',
        'credit_card_processed',
        'user_id',
        'user_id',
        'cart_weight',
        'fixed',
        'product_id',
        'product_id',
        'merchant_order_id',
        'country',
        'ip_country',
        'demo',
        'quantity',
        'pay_method',
        'cart_tangible',
        'merchant_product_id',
        'card_holder_name',
    ];

}
