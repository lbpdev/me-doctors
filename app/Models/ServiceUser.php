<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceUser extends Model {

    protected $fillable = ['user_id','premium_service_id','expires_on','active'];

    protected $dates = ['expires_on'];

}
