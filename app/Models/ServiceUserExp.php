<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceUserExp extends Model {

    protected $fillable = ['user_id','premium_service_id','expires_on','active','started_on'];

    protected $dates = ['expires_on','started_on'];
}
