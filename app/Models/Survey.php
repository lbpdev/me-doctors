<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Survey extends Model {

    use  SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['title' , 'description' , 'status' , 'slug' , 'channel_id' , 'disclaimer' ];


    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'description' => 10
        ]
    ];


    /**
     * A survey can have many questions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('MEDoctors\Models\SurveyItem');
    }

    /**
     * A Article can have multiple Therapies.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function therapies()
    {
        return $this->belongsToMany('MEDoctors\Models\Therapy', 'survey_therapies');
    }
}
