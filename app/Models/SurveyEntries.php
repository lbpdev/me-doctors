<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyEntries extends Model {

    protected $fillable = [
        'user_id' , 'survey_item_choice_id'
    ];


    /**
     * An entry may have other answers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function others()
    {
        return $this->hasOne('MEDoctors\Models\SurveyEntryOther');
    }
}
