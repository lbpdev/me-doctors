<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyItem extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['value' , 'survey_id' , 'allow_multiple'];

    /**
     * Get the choices of the surveys.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function choices()
    {
        return $this->hasMany('MEDoctors\Models\SurveyItemChoice');
    }

}

