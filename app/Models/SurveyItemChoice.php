<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyItemChoice extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['value'];

    /**
     * Get the entries of the choice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entries()
    {
        return $this->belongsToMany('MEDoctors\Models\SurveyEntries', 'survey_entries');
    }

}
