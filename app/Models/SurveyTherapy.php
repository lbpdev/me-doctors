<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyTherapy extends Model {


    protected $fillable = [
        'therapy_id' , 'survey_id'
    ];


    public $timestamps = false;

}
