<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Therapy extends Model {

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name','slug'];

    public function articles(){
        return $this->belongsToMany('MEDoctors\Models\Article','article_therapies');
    }
}
