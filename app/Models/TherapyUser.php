<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class TherapyUser extends Model {

    protected $table = 'therapy_user';

	protected $fillable = [
        'user_id' , 'therapy_id'
    ];

    /**
     * Disable timestamp field for the Model.
     *
     * @var bool
     */
    public $timestamps = false;
}
