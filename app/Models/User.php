<?php namespace MEDoctors\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use MEDoctors\Mailers\Contracts\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, Notifiable {

	use Authenticatable, CanResetPassword , SearchableTrait, SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['fname' , 'lname' , 'email', 'password', 'designation' , 'username' , 'status', 'token' , 'verified'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'fname' => 10,
            'lname' => 10,
            'username' => 10,
        ]
    ];

    /**
     * Get the name of the user.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    /**
     * Gets the avatar for the user's profile
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        $avatar = '';

        if ($profile = $this->profile)
        {
            $avatar = $profile->avatar;
        }

        return $avatar ? 'public/'.$avatar->url : 'images/placeholders/user.png';
    }

    /**
     * Gets the Primary Work Location for the user
     *
     * @return string
     */
    public function getPrimaryWorkLocationAttribute()
    {
        $work = '';

        if ($this->hospitals)
        {
            $work = count($this->hospitals) > 0 ? $this->hospitals[0]->location : false;
        }

        return $work ? $work->name : 'Not Specified';
    }

    /**
     * Gets the Primary Work Location for the user
     *
     * @return string
     */
    public function getPrimaryWorkAttribute()
    {
        $work = '';

        if ($this->hospitals)
        {
            $work = count($this->hospitals) > 0 ? ( $this->hospitals[0]->location ? $this->hospitals[0]->location : false ) : false;
        }

        return $work ? $work : false;
    }

    /**
     * Gets the Primary Hospital's Name for the user
     *
     * @return string
     */
    public function getPrimaryHospitalAttribute()
    {
        $work = '';

        if ($this->hospitals)
            $work = count($this->hospitals) > 0 ? $this->hospitals[0] : false;

        return $work ? $work->name : false;
    }

    /**
     * Gets the Primary Hospital's Name for the user
     *
     * @return string
     */
    public function getPrimarySpecialtyAttribute()
    {
        $specialty = '';

        if ($this->specialties)
            $specialty = count($this->specialties) > 0 ? $this->specialties[0] : false;

        return $specialty ? $specialty->name : false;
    }

    /**
     * Gets the Primary Hospital's Name for the user
     *
     * @return string
     */
    public function getPrimaryScheduleAttribute()
    {
        $work = '';

        if ($this->hospitals)
            $work = count($this->hospitals) > 0 ? $this->hospitals[0] : false;

        return $work ? $work->name : false;
    }

    /**
     * Gets the specialties of user in string format
     *
     * @return string
     */
    public function getAllSpecialtiesAttribute()
    {
        if ($this->specialties) 
        {
            return implode(', ', $this->specialties->lists('name'));
        }

        return 'Not Set';
    }

    /**
     * Get the credentials associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function credentials()
    {
        return $this->hasOne('MEDoctors\Models\DoctorCredential');
    }

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('MEDoctors\Models\Profile');
    }

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function avatar()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * A user can have multiple phone numbers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function contacts()
    {
        return $this->morphMany('MEDoctors\Models\PhoneNumber', 'phoneable');
    }


    /**
     * Get user's office number.
     *
     * @return bool
     */
    public function getWorkPhoneAttribute()
    {
        if($this->contacts())
            $contact = $this->contacts()->where('type','work')->first();
        return $contact ? $contact : false;
    }

    /**
     * Get user's personal number.
     *
     * @return bool
     */
    public function getPersonalPhoneAttribute()
    {
        if($this->contacts())
            $contact = $this->contacts()->where('type','personal')->first();

        return $contact ? $contact : false;
    }

    
    /**
     * A doctor can have many hospitals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hospitals()
    {
        return $this->belongsToMany('MEDoctors\Models\Hospital');
    }

    /**
     * A doctor can have many hospitals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function claims()
    {
        return $this->belongsToMany('MEDoctors\Models\HospitalClaim');
    }

    /**
     * A doctor can have many hospitals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function work()
    {
        return $this->hasMany('MEDoctors\Models\HospitalUser');
    }

    /**
     * A user can speak many languages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany('MEDoctors\Models\Language', 'language_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specialties()
    {
        return $this->belongsToMany('MEDoctors\Models\Therapy');
    }

    /**
     * A user can have many educations.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educations()
    {
        return $this->hasMany('MEDoctors\Models\Education');
    }

    /**
     * A user can have many certifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certifications()
    {
        return $this->hasMany('MEDoctors\Models\Certification');
    }

    /**
     * A user can have many publications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publications()
    {
        return $this->hasMany('MEDoctors\Models\Publication');
    }

    /**
     * A user can have many achievements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function achievements()
    {
        return $this->hasMany('MEDoctors\Models\Achievement');
    }

    /**
     * Gets user survey entries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function surveyEntries()
    {
        return $this->belongsToMany('MEDoctors\Models\SurveyItemChoice', 'survey_entries');
    }

    /**
     * Gets user survey entries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function surveyAnswers()
    {
        return $this->belongsToMany('MEDoctors\Models\SurveyAnswer');
    }

    /**
     * Gets user friendliness.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function friendliness()
    {
        return $this->hasMany('MEDoctors\Models\DoctorFriendliness','user_id');
    }

    /**
     * Gets user friendliness.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function bedsideManner()
    {
        return $this->hasMany('MEDoctors\Models\DoctorBedsideManner','user_id');
    }

    /**
     * Gets user helpfulness.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function helpfulness()
    {
        return $this->hasMany('MEDoctors\Models\DoctorHelpfulness','user_id');
    }

    /**
     * Gets user medical knowledge.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medicalKnowledge()
    {
        return $this->hasMany('MEDoctors\Models\DoctorMedicalKnowledge','user_id');
    }

    /**
     * Gets user professionalism.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function professionalism()
    {
        return $this->hasMany('MEDoctors\Models\DoctorProfessionalism','user_id');
    }

    /**
     * Gets user waiting time.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function waitingTime()
    {
        return $this->hasMany('MEDoctors\Models\DoctorWaitingTime','user_id');
    }


    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getFriendlinessRatingAttribute()
    {
        return $this->countRatingFromTrait($this->friendliness()->get());
    }


    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getFullNameAttribute()
    {
        return $this->fname.' '.$this->lname;
    }

    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getProfessionalismRatingAttribute()
    {
        return $this->countRatingFromTrait($this->professionalism()->get());
    }

    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getBedsideMannerRatingAttribute()
    {
        return $this->countRatingFromTrait($this->bedsideManner()->get());
    }

    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getMedicalKnowledgeRatingAttribute()
    {
        return $this->countRatingFromTrait($this->medicalKnowledge()->get());
    }

    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getHelpfulnessRatingAttribute()
    {
        return $this->countRatingFromTrait($this->helpfulness()->get());
    }

    /**
     * Gets user friendliness rating.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getWaitingTimeRatingAttribute()
    {
        return $this->averageRatingFromTrait($this->waitingTime()->get());
    }


    /**
     * Count rating value by trait
     *
     * @return string
     */
    public function countRatingFromTrait($ratings)
    {
        $data = 0;

        foreach ( $ratings as $rating )
            $data += $rating->value;

        return $data;
    }

    /**
     * Average rating value by trait
     *
     * @return string
     */
    public function averageRatingFromTrait($ratings)
    {
        $data = 0;
        foreach ( $ratings as $rating )
            $data += $rating->value;

        $data = $data ? $data / count($ratings) : 0;

        if($data>59)
            $data = round($data/60) > 1 ? round($data/60).'hrs' : round($data/60). 'hr';
        else
            $data .= ' minutes';

        return $data;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roles()
    {
        return $this->belongsToMany('MEDoctors\Models\Role');
    }

    /**
     * Determine if user has the role name provided.
     *
     * @param $name
     *
     * @return bool
     */
    public function hasRole($name)
    {
        return $this->roles->contains('name', ucfirst($name));
    }

    /**
     * Determine if user is verified.
     *
     * @return bool
     */
    public function getIsVerifiedAttribute()
    {
        return $this->verified ? true : false;
    }

    /**
     * a user can be a KOL.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function kol()
    {
        return $this->hasOne('MEDoctors\Models\UserKOL');
    }

    /**
     * a user can be a KOL.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function services()
    {
        return $this->hasOne('MEDoctors\Models\ServiceUser','user_id');
    }

    /**
     * Gets user survey entries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pollVotes()
    {
        return $this->belongsToMany('MEDoctors\Models\PollItem', 'poll_votes');
    }

    /**
     * Confirm a user's email address by token.
     *
     * @return void
     */
    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;

        $this->save();
    }

    /**
     * Update the user password in the database.
     *
     * @param $password
     */
    public function updatePassword($password)
    {
        $this->setPasswordAttribute($password);
        $this->save();
    }

    /**
     * A user can have many views
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function views()
    {
        return $this->morphMany('MEDoctors\Models\View' , 'viewable');
    }

    /**
     * Views count relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function viewsCountRelation()
    {
        return $this->countRelation($this->views());
    }

    /**
     * Get the views count.
     *
     * @return int
     */
    public function getViewCountAttribute()
    {
        return $this->getCount($this->viewsCountRelation);
    }


    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getIsPremiumAttribute()
    {
        if($this->services()->where('active',1)->where('expires_on', '>=' , date("Y-m-d H:i:s"))->count())
            $flag = 1;
        elseif ($this->services()->where('active',0)->where('expires_on', '<' , date("Y-m-d H:i:s"))->count())
            $flag = 2;
        else
            $flag = 0;

        return $flag;
    }

    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getCurrentServiceAttribute()
    {
        return $this->services()->where('expires_on', '>' , date("Y-m-d H:i:s"))->first();
    }

    /**
     * Check if user is premium.
     *
     * @return int
     */
    public function getPremiumAttribute()
    {
        if(isset($this->active))
            if($this->active)
                return true;

        return false;
    }

    /**
     * Hash the password when setting it.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Get the user's email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

}
