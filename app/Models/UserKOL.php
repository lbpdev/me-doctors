<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class UserKOL extends Model {


    protected $fillable = ['user_id'];
    protected $table    = 'user_kols';

    /**
     * a KOL is for a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
	public function user(){
        return $this->belongsTo('MEDoctors\Models\User');
    }

}
