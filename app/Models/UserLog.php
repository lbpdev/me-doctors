<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model {

	protected $fillable = ['username','email','ip','log_type'];

}
