<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model {

	protected $fillable =[
        'confirmation_code' , 'user_id'
    ];

}
