<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

	protected $fillable = ['source','title','description','slug','channel_id'];


    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        if($thumb)
            return asset('public/'.$thumb->url);

        if(strpos($this->source,'youtube.com')){
            return 'http://img.youtube.com/vi/'.self::getYoutubeIdAttribute($this->source).'/mqdefault.jpg';
        }

        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public/'.$thumb->url) : 'http://placehold.it/320x180';
    }

    /**
     * get Video Youtube code Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getYoutubeIdAttribute()
    {
        $source = $this->source;

        if(strpos($this->source,'www.youtube.com/embed')){
            $search = 'http://www.youtube.com/embed/';
            $trimmed = str_replace($search, '', $source);
            $search = 'https://www.youtube.com/embed/';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=1';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=0';
            $trimmed = str_replace($search, '', $trimmed);
        } elseif(strpos($this->source,'www.youtube.com/watch')){
            $search = 'https://www.youtube.com/watch?v=';
            $trimmed = str_replace($search, '', $source);
            $search = 'http://www.youtube.com/watch?v=';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=1';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=0';
            $trimmed = str_replace($search, '', $trimmed);
        } elseif(strpos($this->source,'www.youtube.com/v/')){
            $search = 'https://www.youtube.com/v/';
            $trimmed = str_replace($search, '', $source);
            $search = 'http://www.youtube.com/v/';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=1';
            $trimmed = str_replace($search, '', $trimmed);
            $search = '?autoplay=0';
            $trimmed = str_replace($search, '', $trimmed);
        }

        return isset($trimmed) ? $trimmed : $source;
    }

    /**
     * get Video Youtube code Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getUrlAttribute()
    {
        $source = $this->source;

        if(strpos($source,'www.youtube.com/embed'))
            return $source;
        elseif(strpos($source,'www.youtube.com/watch')){
            return 'https://www.youtube.com/embed/'.self::getYoutubeIdAttribute($source).'?autoplay=1';
        }
        else
        return $source;
    }
}
