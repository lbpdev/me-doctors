<?php namespace MEDoctors\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model {

	protected $fillable = [
        'viewable_id' , 'viewable_type' , 'user_ip' , 'user_id'
    ];


    public function post($viewable_id, $viewable_type, $user_ip, $user_id)
    {
        return [
            'user_ip' => $user_ip,
            'user_id' => $user_id,
            'viewable_id' => $viewable_id,
            'viewable_type' => $viewable_type,
        ];
    }
//
//    public function articles()
//    {
//        return $this->morphedMany('MEDoctors\Models\Article', 'viewable_id');
//    }

    public function viewable()
    {
        return $this->morphTo();
    }
}
