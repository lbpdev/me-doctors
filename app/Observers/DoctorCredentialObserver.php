<?php namespace MEDoctors\Observers;

class DoctorCredentialObserver {
    
    /**
     * Fired when doctor credentials are being deleted.
     *
     * @param  \MEDoctors\Models\DoctorCredential $credentials
     *
     * @return void
     */
    public function deleting($credentials)
    {
        foreach ($credentials->attachments as $attachment)
        {
            $attachment->delete();
        }
    }

}