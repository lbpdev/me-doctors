<?php namespace MEDoctors\Observers;

use Illuminate\Filesystem\Filesystem;

class UploadedFilesObserver {

    /**
     * @var Filesystem
     */
    protected $file;
    
    /**
     * Create UploadedFilesObserver instance.
     *
     * @param Filesystem $file
     */
    public function __construct(Filesystem $file)
    {
        $this->file = $file;
    }
    
     /**
     * Fired when an uploaded file is being deleted.
     *
     * @param  \MEDoctors\Models\Upload $file
     *
     * @return void
     */
    public function deleting($file)
    {
        $this->file->delete(public_path($file->url));
    }
}