<?php namespace MEDoctors\Observers;

class UserObserver {
    
    /**
     * Fired when a user is being deleted.
     *
     * @param  \MEDoctors\Models\User $user
     *
     * @return void
     */
    public function deleting($user)
    {
        $user->credentials->delete();
    }

}