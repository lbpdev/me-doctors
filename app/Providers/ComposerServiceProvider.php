<?php namespace MEDoctors\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer(['backend.*.sidebar'], 'MEDoctors\Http\ViewComposers\AdminComposer');
		View::composer('patient.header', 'MEDoctors\Http\ViewComposers\TodayComposer');
		View::composer('doctor.header', 'MEDoctors\Http\ViewComposers\TodayComposer');
        View::composer('doctor.*._news-ticker', 'MEDoctors\Http\ViewComposers\NewsTickerComposer');
        View::composer('patient.*._news-ticker', 'MEDoctors\Http\ViewComposers\NewsTickerPatientComposer');
		View::composer(['admin.*.sidebar'], 'MEDoctors\Http\ViewComposers\AdminComposer');
		View::composer(['doctor.*'], 'MEDoctors\Http\ViewComposers\MessagesComposer');
        View::composer(['doctor.*'], 'MEDoctors\Http\ViewComposers\AdComposer');
        View::composer(['patient.*'], 'MEDoctors\Http\ViewComposers\AdComposerPatient');
        View::composer(['patient.*'], 'MEDoctors\Http\ViewComposers\MessagesComposer');
		View::composer(['*'], 'MEDoctors\Http\ViewComposers\PageNameComposer');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
