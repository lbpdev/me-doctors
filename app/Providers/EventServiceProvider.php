<?php namespace MEDoctors\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
        'MEDoctors\Events\ArticleViewed' => [
            'MEDoctors\Handlers\Events\ArticleViewedHandler',
        ],
        'MEDoctors\Events\UserViewed' => [
            'MEDoctors\Handlers\Events\UserViewedHandler',
        ],
        'MEDoctors\Events\HospitalViewed' => [
            'MEDoctors\Handlers\Events\HospitalViewedHandler',
        ],
        'MEDoctors\Events\DoctorHasRegistered' => [
        	'MEDoctors\Handlers\Events\SendConfirmationEmail',
        ],

        'MEDoctors\Events\UserHasRegistered' => [
            'MEDoctors\Handlers\Events\SendConfirmationEmail',
        ],
        'MEDoctors\Events\UserHasBeenRemoved' => [
        	'MEDoctors\Handlers\Events\SendRemovedAccountEmail',
        ],

        'MEDoctors\Events\VerifiedDoctor' => [
            'MEDoctors\Handlers\Events\SendVerifiedDoctorEmail',
        ],

        'MEDoctors\Events\DeniedDoctor' => [
            'MEDoctors\Handlers\Events\SendDeniedDoctorEmail',
        ]
    ];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
