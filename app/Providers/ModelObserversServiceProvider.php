<?php namespace MEDoctors\Providers;

use Illuminate\Support\ServiceProvider;

class ModelObserversServiceProvider extends ServiceProvider {

	/**
	 * Model events to be observed.
	 *
	 * @var array
	 */
	protected $observers = [
		// 'MEDoctors\Models\User' => 'MEDoctors\Observers\UserObserver',
		// 'MEDoctors\Models\DoctorCredential' => 'MEDoctors\Observers\DoctorCredentialObserver',
		'MEDoctors\Models\Upload' => 'MEDoctors\Observers\UploadedFilesObserver',
	];

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		foreach ($this->observers as $model => $observer)
		{
		    if (class_exists($model))
		    {
		    	$instance = new $model;

		    	$instance::observe($observer);
		    }
		}
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
