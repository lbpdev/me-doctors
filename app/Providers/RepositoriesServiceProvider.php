<?php namespace MEDoctors\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider {

	protected $models = [
		'Article',
		'Channel',
		'Discussion',
		'Event',
		'Poll',
		'PollVote',
		'Upload',
		'Survey',
		'User',
        'View',
        'Therapy',
        'Message',
        'Hospital',
        'Country',
        'UserStat',
        'UserLog',
        'NewsFeed',
        'Video',
        'DummyNewsFeed',
        'Messages',
        'Ad',
	];

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		foreach ($this->models as $model)
		{
			$this->app->bind(
			    "MEDoctors\Repositories\Contracts\\{$model}Repository",
			    "MEDoctors\Repositories\Eloquent\Db{$model}Repository"
			);
		}

		$this->bindEmailRepositories();
		$this->bindMailRepositories();
	}

	private function bindEmailRepositories()
	{
	    $this->app->bind(
		    "MEDoctors\Repositories\Contracts\Emails\EmailDomainsWhitelist",
		    "MEDoctors\Repositories\Emails\DbEmailDomainsWhitelist"
		);

		$this->app->bind(
		    "MEDoctors\Repositories\Contracts\Emails\EmailDomainsBlacklist",
		    "MEDoctors\Repositories\Emails\DbEmailDomainsBlacklist"
		);

		$this->app->bind(
		    "MEDoctors\Repositories\Contracts\Emails\EmailsWhitelist",
		    "MEDoctors\Repositories\Emails\DbEmailsWhitelist"
		);
	}

	private function bindMailRepositories()
	{
	    $this->app->bind(
		    "MEDoctors\Repositories\Contracts\MailRepository",
		    "MEDoctors\Repositories\Eloquent\DbMailRepository"
		);
	}

}
