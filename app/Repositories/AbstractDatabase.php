<?php namespace MEDoctors\Repositories;

use Illuminate\Database\DatabaseManager;

abstract class AbstractDatabase {
    
    /**
     * Table name from the database.
     * 
     * @var string
     */
    protected $table;

    /**
     * The column of the table to check.
     *
     * @var string
     */
    protected $columnToCheck;

    /**
     * @var DatabaseManager
     */
    protected $db;

     /**
     * Create a new repository instance.
     *
     * @param DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->db = $db;
    }

    /**
     * Get the database manager.
     *
     * @return \Illuminate\Database\DatabaseManager
     */
    protected function getDatabaseManager()
    {
        return $this->db;
    }

    /**
     * Get database table to lookup to.
     *
     * @return string
     */
    protected function getTable()
    {
        return $this->table;
    }

    /**
     * Get the field to check.
     *
     * @return string
     */
    protected function getColumnToCheck()
    {
        return $this->columnToCheck;
    }

    /**
     * Paginate the records from the table.
     *
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15)
    {
        return $this->newQuery()->paginate($perPage);
    }

    /**
     * Create a query builder instance for data table.
     *
     * @param array $columns
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function forDataTable($columns = ['*'])
    {
        return $this->newQuery()->select($columns);
    }

    /**
     * Remove a record from the table by id.
     *
     * @param  int $id
     *
     * @return boolean
     */
    public function deleteById($id)
    {
        return $this->newQuery()->delete($id);
    }

    /**
     * Prepare a new query.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newQuery()
    {
        return $this->getDatabaseManager()->table($this->getTable());
    }

}