<?php
namespace MEDoctors\Repositories\Contracts;

interface AdRepository {

    public function store($request);

    public function update($request);

}