<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\Article;
use MEDoctors\Models\View;

interface ArticleRepository  {

    /**
     * Get the latest articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 15);

    /**
     * Get the top articles for each therapy.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function topPerTherapy($items = 5, $channel_id);

     /** Get a limited collection of articles.
     *
     * @param  int $limit
     *
     * @return mixed
     */
    public function limit($limit = 15);

    /**
     * Include only article in specific channel in the result.
     *
     * @return $this
     */
    public function withChannel($id);

    /**
     * Include only article who's date posted are past.
     *
     * @return $this
     */
    public function noUpcoming();

    /**
     * Gets the featured article or the first article.
     *
     * @return \MEDoctors\Models\Article|null
     */
    public function featured($slug);

    /**
     * Get the paginated popular articles.
     * 
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popular($perPage = 15 , $channel_id);

    /**
     * Get the archived articles.
     *
     * @return mixed
     */
    public function archived();

    /**
     * Find the article by its slug.
     *
     * @param  string $slug
     *
     * @return static|null
     **/

    public function findBySlug($slug);

    /**
     * Save the one who view the article.
     *
     * @param  Article $article
     * @param  View    $view
     *
     * @return array
     */
    public function view(Article $article , View $view);

    /**
     * Store new article.
     *
     * @param  Article $article
     *
     * @return array
     */
    public function store($request , $therapies, $file);

    /**
     * Update article.
     *
     * @param  Article $article
     *
     * @return array
     */
    public function update($request , $therapies, $file , Article $article);
}