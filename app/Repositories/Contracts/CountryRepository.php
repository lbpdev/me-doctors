<?php namespace MEDoctors\Repositories\Contracts;

interface CountryRepository {

    /**
     * Get GCC countries only.
     *
     * @param  mixed $query
     *
     * @return mixed
     */
    public function gccCounties();

    /**
     * Determine if the given country code belongs to GCC.
     *
     * @param  string  $country
     *
     * @return boolean
     */
    public function isGcc($country);
}