<?php namespace MEDoctors\Repositories\Contracts;

use MEDoctors\Models\Discussion;

interface DiscussionRepository {
    
    /**
     * Get the panel discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPanelDiscussions($perPage = 15);

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularDoctorDiscussions($perPage = 15);

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularDoctorDiscussionsWithFilter($therapy , $perPage = 15);

    /**
     * Get the panel discussions with filters.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPanelDiscussionsWithFilter($therapy , $perPage = 15);


    /**
     * Gets the latest paginated version of panel discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPanelPaginated($perPage = 15);

    /**
     * Gets the latest paginated version of doctor discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestDoctorPaginated($perPage = 15);

    /**
     * Gets the latest paginated version of patient discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPatientPaginated($perPage = 15);

    /**
     * Find a discussion by its slug.
     *
     * @param  string $slug
     *
     * @return static|null
     */
    public function findBySlug($slug);

    /**
     * Create a doctor discussion.
     *
     * @param  array  $attributes 
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createDoctorDiscussion(array $attributes, $attachments = []);

    /**
     * Create a patient discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createPatientDiscussion(array $attributes, $attachments = []);

    /**
     * Create a doctor discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createPanelDiscussion(array $attributes, $attachments = []);

    /**
     * Create a doctor discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function updateDoctorDiscussion(Discussion $discussion, array $attributes, $attachments = [], $remove_attachment);

    /**
     * Update a Panel discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function updatePanelDiscussion(Discussion $discussion, array $attributes, $attachments = [], $remove_attachment);


    /**
     * Find a discussion by its id.
     *
     * @param  int $id
     *
     * @return static|null
     */
    public function findById($id);

    /**
     * Post a comment for the discussion.
     *
     * @param  int $userId
     * @param  int $discussion
     * @param  string $comment
     * @param  array $attachments
     *
     * @return Comment
     */
    public function postComment($userId, $discussionId, $comment, $attachments = []);

    /**
     * Update a comment for the discussion.
     *
     * @param  int $userId
     * @param  int $comment
     * @param  string $comment
     * @param  array $attachments
     *
     * @return Comment
     */
    public function updateComment($userId, $commentId, $comment, $attachments = [] ,$remove_attachment);

    /**
     * Publish a discussion.
     *
     * @param  Object $item
     *
     * @return $item
     */
    public function publish($item);

    /**
     * Unpublish a discussion.
     *
     * @param  Object $item
     *
     * @return $item
     */
    public function unpublish($item);

     /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedDoctorDiscussions();

    /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedPanelDiscussions();

     /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedComments();

}