<?php namespace MEDoctors\Repositories\Contracts\Emails;

interface EmailDomainsBlacklist {

    /**
     * Determine if the email is in the blacklist.
     *
     * @param  string $email
     *
     * @return boolean
     */
    public function contains($email);

    /**
     * Create a query builder instance for data table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function forDataTable();

    /**
     * Add email domain to the black list.
     *
     * @param array|string $domain
     *
     * @return  void
     */
    public function addDomain($domain);

    /**
     * Remove an email domain from the white list.
     *
     * @param  int $id
     *
     * @return boolean
     */
    public function deleteById($id);
}