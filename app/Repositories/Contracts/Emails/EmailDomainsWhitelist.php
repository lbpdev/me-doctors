<?php namespace MEDoctors\Repositories\Contracts\Emails;

interface EmailDomainsWhitelist {

    /**
     * Determine if the email is in the whitelist.
     *
     * @param  string $email
     *
     * @return boolean
     */
    public function contains($email);

    /**
     * Create a query builder instance for data table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function forDataTable();

    /**
     * Add email domain to the white list.
     *
     * @param array|string $domain
     *
     * @return  void
     */
    public function addDomain($domain);

    /**
     * Remove an email domain from the white list.
     *
     * @param  int $id
     *
     * @return boolean
     */
    public function deleteById($id);

    /**
     * Extract domain from a website and add it to the whitelist.
     *
     * @param  array|string $website
     *
     * @return void
     */
    public function importWebsite($website);
    
}