<?php namespace MEDoctors\Repositories\Contracts\Emails;

interface EmailsWhitelist {

    /**
     * Determine if the email is in the white list.
     *
     * @param  string $email
     *
     * @return boolean
     */
    public function contains($email);

    /**
     * Create a query builder instance for data table.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function forDataTable();

    /**
     * Add email to the white list.
     *
     * @param string $email
     *
     * @return  void
     */
    public function addEmail($email);

    /**
     * Remove an email from the white list.
     *
     * @param  int $id
     *
     * @return boolean
     */
    public function deleteById($id);

}