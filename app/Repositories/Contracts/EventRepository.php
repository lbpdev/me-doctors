<?php namespace MEDoctors\Repositories\Contracts;

interface EventRepository {

    /**
     * Get the upcoming events.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginatedUpcoming($perPage = 15);

    /**
     * Get the past events.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginatedPast($perPage = 15);

    /**
     * Store new event.
     *
     * @param  Request $request
     *
     */
    public function store($request , $file);

    /**
     * Update event.
     *
     */
//    public function update($request , $file , $keep_thumb, Event $event);
}