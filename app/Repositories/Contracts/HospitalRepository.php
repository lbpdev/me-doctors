<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\View;

interface HospitalRepository  {

    /**
     * Create API readable format for hospitals and location.
     *
     * @param  array $hospital
     *
     * @return array
     */
    public function generateAutoLoadData($hospital);

    /**
     * Add Schedule to user's hospitals.
     *
     * @param  Object $user
     *
     * @return array
     */
    public function generateHospitalSchedules($user);

    /**
     * get hospital relative data.
     * @param  Array $array
     * @return array
     */
    public function withData($array);

    /**
     * store new hospital.
     *
     * @param  array $hospital
     * @param  array $contact
     * @param  array $facilities
     * @param  array $therapies
     * @param  array $location
     * @param  array|null $file
     *
     * @return array
     */
    public function store($hospital , $contact, $facilities, $therapies, $location , $file = null);

    /**
     * update hospital.
     *
     * @param  Hospital $hospital
     * @param  array $input
     * @param  array $location
     * @param  array $contact
     * @param  array $facilities
     * @param  array $therapies
     * @param  array $file
     * @param  array $keep_thumb
     *
     * @return array
     */
    public function update(Hospital $hospital, $input ,$contact, $facilities, $therapies, $location , $file, $keep_thumb );

    /**
     * Get next set of records.
     *
     * @param  int $offset
     * @param  Int $limit
     *
     * @return array
     */
    public function skipAndTake($offset, $limit);
}