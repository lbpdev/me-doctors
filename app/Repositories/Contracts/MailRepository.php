<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\View;

interface MailRepository  {


    /**
     * Send Mail
     *
     **/

    public function sendMail($template, $sender_email, $sender_name, $recipient_email, $subject, $message);
}