<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\Article;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\View;

interface MessagesRepository  {

   public function store($request,$callback);

}