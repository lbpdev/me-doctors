<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\Article;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\View;

interface NewsFeedRepository  {

    /**
     * Get the latest articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 15);


    /** Get a limited collection of articles.
     *
     * @param  int $limit
     *
     * @return mixed
     */
    public function limit($limit = 15);

    /**
     * Include only article in specific channel in the result.
     *
     * @return $this
     */
    public function withChannel($id);

    /**
     * Get the paginated popular articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popular($perPage = 15 , $channel_id);

    /**
     * Get the archived articles.
     *
     * @return mixed
     */
    public function archived();

    /**
     * Find the article by its slug.
     *
     * @param  string $slug
     *
     * @return static|null
     **/

    public function findBySlug($slug);

    /**
     * Save the one who view the article.
     *
     * @param  NewsFeed $newsFeed
     * @param  View    $view
     *
     * @return array
     */
    public function view(NewsFeed $newsFeed , View $view);

    /**
     * Store new newsFeed.
     *
     * @return array
     */
    public function store($request , $file);

    /**
     * Update newsFeed.
     *
     * @param  $request
     * @param  $file
     * @param  NewsFeed $newsFeed
     *
     * @return array
     */
    public function update($request ,$file , NewsFeed $newsFeed);
}