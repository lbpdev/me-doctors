<?php namespace MEDoctors\Repositories\Contracts;

use MEDoctors\Models\User;

interface PollRepository {

    /**
     * Get the latest poll with items.
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function latest();

    /**
     * Set the user id when getting the poll
     * to get the voted item of the user if it exists.
     *
     * @param int $userId
     *
     * @return $this
     */
    public function withVoteByUserId($userId);

    /**
     * Get the featured poll and determine if the user already voted.
     *
     * @param  int $userId
     * @param  int $slug
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function featured($userId , $slug);

    /**
     * Store user vote.
     *
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function vote(User $user, $pollId, $pollItemId);

}