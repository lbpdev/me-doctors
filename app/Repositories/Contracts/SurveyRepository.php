<?php namespace MEDoctors\Repositories\Contracts;

use MEDoctors\Models\User;
use MEDoctors\Models\Survey;

interface SurveyRepository {

    /**
     * Get the latest paginated surveys.
     *
     * @param  integer $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 15, $channel);
    
    /**
     * Get the first latest survey.
     *
     * @return \MEDoctors\Models\Survey|null
     */
    public function first();

    /**
     * Find a survey by its slug.
     *
     * @param  string $slug
     *
     * @return mixed
     */
    public function findBySlug($slug);

    /**
     * Save the survey votes of the user.
     *
     * @param  User   $user
     * @param  int $surveyItemId
     * @param  array  $votes
     *
     * @return mixed
     */
    public function saveUserVotes(User $user, $surveyItemId, array $votes = []);

    /**
     * Save the survey answers of the user.
     *
     * @param  User   $user
     * @param  int $surveyItemId
     * @param  array  $answers
     *
     * @return mixed
     */
    public function saveUserAnswers(User $user, $surveyItemId, array $answers = []);

    /**
     * Save the new survey.
     *
     * @param  User   $user
     * @param  array $survey
     * @param  array  $survey
     *
     * @return mixed
     */
    public function store($survey, $therapies, array $answers = []);

    /**
     * Update survey.
     *
     * @param  User   $user
     * @param  array $survey
     * @param  array  $survey
     *
     * @return mixed
     */
    public function update($survey, $therapies , array $answers = []);

    /**
     * Get user entries by survey.
     *
     * @param  User   $user 
     * @param  Survey $survey
     *
     * @return array
     */

    public function userEntriesBySurvey(User $user, Survey $survey);

    /**
     * Filter By Channel.
     *
     * @return $this
     */
    public function withChannel($id);

    /**
     * get surveys in specific therapy area.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterTherapy($therapies);

    /**
     * Sort data by date.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function sortDate($sort);
}