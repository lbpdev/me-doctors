<?php namespace MEDoctors\Repositories\Contracts;

interface TherapyRepository {

    /**
     * Get all the therapies as array.
     *
     * @return array
     */
    public function forSelect();

    /**
     * Find a therapy by its slug.
     *
     * @param  string $slug
     *
     * @return mixed|null
     */
    public function findBySlug($slug);

    /**
     * Find therapy from the database by name.
     *
     * @param  array|string $name
     *
     * @return mixed
     */
    public function findByName($name);

}