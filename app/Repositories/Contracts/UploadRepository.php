<?php namespace MEDoctors\Repositories\Contracts;

interface UploadRepository {

    /**
     * Save the attachments to the database.
     *
     * @param  array  $records
     *
     * @return array
     */
    public function save(array $records);
}