<?php namespace MEDoctors\Repositories\Contracts;

interface UserLogRepository {

    /**
     * Store Log Entry
     * @param $username
     * @param $email
     * @param $log_type
     * @return mixed
     */
    public function storeEntry($username,$email,$log_type);
}