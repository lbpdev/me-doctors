<?php namespace MEDoctors\Repositories\Contracts;

use MEDoctors\Models\User;
use MEDoctors\Models\View;

interface UserRepository  {

    /**
     * Get the user by its username.
     *
     * @param  string $username
     *
     * @return mixed
     */
    public function getByUsername($username);

    /**
     * Filter doctor users.
     *
     * @param  array $achievement
     *
     * @return array
     */
    public function doctorsOnly();

    /**
     * Sort Doctors by number of Views.
     *
     *
     */
    public function mostViewed();

    /**
     * get doctors in specific therapy area.
     *
     * @param  Array $array
     *
     * @return array
     */

    /**
     * Get doctors paginated.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginated($perPage = 10);


    public function filterSpecialty($specialty_id);
    /**
     * Create a doctor user.
     *
     * @param  array  $data
     *
     * @return User
     */
    public function createDoctor(array $data);

    /**
     * Create a doctor user.
     *
     * @param  array  $data
     *
     * @return User
     */
    public function createPatient(array $data);

    /**
     * Attach credentials for doctor.
     *
     * @param  User   $doctor     
     * @param  array  $credentials
     *
     * @return void
     */
    public function attachDoctorCredentials(User $doctor, array $credentials);

    /**
     * Confirm user's email address by token.
     *
     * @param  string $token
     *
     * @return mixed
     */
    public function confirmEmail($token);

    /**
     * Get all unverified doctors with their credentials.
     *
     * @return mixed
     */
    public function unverifiedDoctors();


    /**
     * Get all verified doctors with their credentials.
     *
     * @return mixed
     */
    public function verifiedDoctors();

    /**
     * Prepare unverified doctors data table query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function tableOfUnverifiedDoctors();

    /**
     * Verify the doctor by id.
     *
     * @param  int $id
     *
     * @return mixed
     */
    public function verifyDoctor($id);

    /**
     * rate a doctor.
     *
     * @param  User $user
     * @param  Array $ratings
     * @param  int $rater_id
     *
     * @return mixed
     */
    public function rateDoctor($user , $ratings , $rater_id);

    /**
     * Delete the user by id.
     *
     * @param  int $id
     *
     * @return mixed
     */
    public function deleteById($id);

    /**
     * Updates user's education.
     *
     * @param User   $user
     * @param array $educations
     *
     * @return  array|bool
     */
    public function updateEducation(User $user, array $educations = [], $die);

    /**
     * Updates user's achievements.
     *
     * @param User   $user
     * @param array $achievements
     *
     * @return  array|bool
     */
    public function updateAchievements(User $user, array $achievements = [], $die);

    /**
     * Updates user's publications.
     *
     * @param User   $user
     * @param array $publications
     *
     * @return  array|bool
     */
    public function updatePublications(User $user, array $publications = [], $die);

    /**
     * Updates user's certifications.
     *
     * @param User   $user
     * @param array $certifications
     *
     * @return  array|bool
     */
    public function updateCertifications(User $user, array $certifications = [], $die);

    /**
     * Updates user's languages.
     *
     * @param User   $languages
     * @param array $languages
     *
     * @return  array|bool
     */
    public function updateLanguages(User $user, array $languages = [], $die);

    /**
     * Updates user's work.
     *
     * @param User   $user
     * @param array $work
     *
     * @return  array|bool
     */
    public function updateWork(User $user, array $work = [], $die);


    /**
     * Updates user's profile.
     *
     * @param User   $user
     * @param array $profile
     *
     * @return  array|bool
     */
    public function updateProfile(User $user, array $profile = []);

    /**
     * Updates user's profile.
     *
     * @param User   $user
     * @param array $account
     * @param array $therapies
     *
     * @return  array|bool
     */
    public function updateUser(User $user, array $account = [], $therapies);


    /**
     * Updates user's contacts.
     *
     * @param User   $user
     * @param array $contacts
     *
     * @return  array|bool
     */
    public function updateContacts(User $user, array $contacts = []);

    /**
     * Updates user's avatar.
     *
     * @param User   $user
     * @param array $file
     *
     * @return  array|bool
     */
    public function updateAvatar(User $user, $file);

    /**
     * Remove user's avatar.
     *
     * @param User $user
     *
     * @return  array|bool
     */
    public function removeAvatar(User $user);

    /**
     * Get users for messages
     */
    public function getUsersForMessage($channel_id);

    /**
     * Filter publications that has a name.
     *
     * @param  array $publications
     *
     * @return array
     */
    public function filterPublicationsWithName($publications);

    /**
     * Filter education that has a school.
     *
     * @param  array $work
     *
     * @return array
     */
    public function filterWorkWithLocation($work);

    /**
     * Filter certifications that are blank.
     *
     * @param  array $certification
     *
     * @return array
     */
    public function filterCertifications($certification);

    /**
     * Filter education that has a school.
     *
     * @param  array $education
     *
     * @return array
     */
    public function filterEducationWithSchool($education);


    /**
     * Filter achievement that has a name.
     *
     * @param  array $achievement
     *
     * @return array
     */
    public  function filterAchievementWithName($achievement);

    /**
     * Save the one who view the article.
     *
     * @param  User $user
     * @param  View $view
     *
     * @return array
     */
    public function view(User $user, View $view);

    /**
     * Get next set of records.
     *
     * @param  int $offset
     * @param  Int $limit
     *
     * @return array
     */
    public function skipAndTake($offset, $limit);

    /**
     * user avails service.
     *
     * @return Array
     */
    public function availService($username,$service_id);

    public function getFeaturedWithFilters($specialty,$country,$city,$sort,$featuredCount,$take);
}
