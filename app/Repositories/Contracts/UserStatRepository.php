<?php namespace MEDoctors\Repositories\Contracts;

interface UserStatRepository {

    /**
     * Count the new users.
     *
     * @return int
     */
    public function newUsers();

    /**
     * Count the unverified doctors.
     *
     * @return int
     */
    public function unverifiedDoctors();

}