<?php namespace MEDoctors\Repositories\Contracts;

use Illuminate\Http\Request;
use MEDoctors\Models\Article;
use MEDoctors\Models\Video;
use MEDoctors\Models\View;

interface VideoRepository  {

    /**
     * Include only videos in specific channel in the result.
     *
     * @return $this
     */
    public function withChannel($id);


    /**
     * Get the latest videos.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 15);

    /**
     * Store new Video.
     *
     * @return array
     */
    public function store($request , $file);


    /**
     * Update an Video.
     * @param $request
     * @param $file
     * @param $keep_thumb
     * @param  $video
     * @return Video $video
     */
    public function update($request , $file , $keep_thumb, $video);
}