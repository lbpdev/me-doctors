<?php namespace MEDoctors\Repositories\Contracts;

interface ViewRepository {

    public function create($user_ip, $user_id);

}