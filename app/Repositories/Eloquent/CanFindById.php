<?php namespace MEDoctors\Repositories\Eloquent;

trait CanFindById {

    /**
     * Find an entity by its id.
     *
     * @param  int $id
     *
     * @return static|null
     */
    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }
}