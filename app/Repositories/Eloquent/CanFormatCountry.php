<?php namespace MEDoctors\Repositories\Eloquent;

use Illuminate\Support\Str;
trait CanFormatCountry {

    public function formatCountry($location){

        $formats = array(
            'UAE' => array('United Arab Emirates'),
            'Saudi Arabia' => array('Saudi')
        );

        foreach($formats as $key=>$format){
            if(in_array($location['country'] ,$format))
                $location['country'] = $key;
        }


        return $location;
    }
}