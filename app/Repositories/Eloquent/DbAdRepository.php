<?php
namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Ad;
use MEDoctors\Models\AdType;
use MEDoctors\Models\AdUser;
use MEDoctors\Repositories\Contracts\AdRepository;

use MEDoctors\Services\Uploaders\AdsImageUploader;

class DbAdRepository implements AdRepository {

    use CanCreateSlug;

    public function __construct(Ad $ad,AdType $adType,AdUser $adUser,AdsImageUploader $uploader){
        $this->uploader = $uploader;
        $this->ad = $ad;
        $this->adType = $adType;
        $this->adUser = $adUser;
    }

    public function store($request){

        $adData = $request->only('title','channel_id','ad_type_id','description','ad_start','ad_end','link');
        $adData['ad_start'] = strtotime($adData['ad_start']);
        $adData['ad_end'] = strtotime($adData['ad_end']);

        $adUserData = $request->only('ad_user');
//
        $adUser = $this->adUser->create($adUserData['ad_user']);

        $adData['ad_user_id'] = $adUser->id;

        $file = $request->file('image');

        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $ad = $this->ad->create($adData);

        if($photo)
            $ad->image()->createMany($photo);

        if($adUserData['ad_user']['contact'])
            $adUser->contacts()->create(array('type' => 'primary','number'=>$adUserData['ad_user']['contact']));
    }

    public function update($request){

        $adData = $request->only('title','channel_id','ad_type_id','description','ad_start','ad_end','link');
        $adData['ad_start'] = strtotime($adData['ad_start']);
        $adData['ad_end'] = strtotime($adData['ad_end']);

        $adUserData = $request->only('ad_user');
//
        $ad = Ad::with('adUser')->where('id',$request->input('id'))->first();

        if($ad->adUser())
            // $ad->adUser()->delete();
//
        $adUser = $this->adUser->create($adUserData['ad_user']);

        $adData['ad_user_id'] = $adUser->id;
        $ad = $ad->update($adData);

        $file = $request->file('image');

        if($file) {
            $photo = $this->uploader->upload($file);
            if ($photo)
                $ad->image()->createMany($photo);
        }

        if($adUserData['ad_user']['contact'])
            $adUser->contacts()->create(array('type' => 'primary','number'=>$adUserData['ad_user']['contact']));

        return true;
    }
}
