<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\ArticleTherapy;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\View;
use MEDoctors\Models\Article;
use Illuminate\Support\Str;
use MEDoctors\Models\Option;
use MEDoctors\Repositories\Eloquent\CanFindBySlug;
use MEDoctors\Repositories\Eloquent\CanCreateSlug;
use MEDoctors\Repositories\Contracts\ArticleRepository;
use MEDoctors\Services\Uploaders\ArticleThumbnailUploader;

use Carbon\Carbon;
class DbArticleRepository implements ArticleRepository {
    
    use CanFindBySlug, CanCreateSlug;
    
    /**
     * @var Article
     */
    protected $model;

    public function __construct(Article $model, ArticleThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    /**
     * Get the top five articles for each therapy.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function topPerTherapy($items = 5 , $channel_id)
    {
        $articles = $this->model->getModel()->with('therapies')
            ->selectRaw('articles.*, count(articles.id) as aggregate')
            ->where('channel_id',  '=', $channel_id)
            ->where('date_posted', '<=' , date("Y-m-d H:i:s"))
            ->leftJoin('views', 'views.viewable_id', '=', 'articles.id')
            ->groupBy('articles.id')
            ->orderBy('aggregate', 'DESC')
            ->latest('articles.created_at')->get();

        $therapies = Therapy::get();
        $data = [];

        foreach($therapies as $inx=>$therapy){
            $data[$inx]['id'] = $therapy->id;
            $data[$inx]['name'] = $therapy->name;
            foreach($articles as $article){
                foreach($article->therapies as $in=>$article_therapy){
                    if($article_therapy->id==$therapy['id'])
                        $data[$inx]['articles'][] = $article;
                }
            }
        }

        return $data;
    }

    /**
     * Get the latest articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 10)
    {
        return $this->model->with('therapies','uploads')->orderBy('date_posted','DESC')->paginate($perPage);
    }

    /**
     * Get a limited collection of articles.
     *
     * @param  int $limit
     *
     * @return mixed
     */
    public function limit($limit = 15)
    {
        return $this->model->latest()->limit($limit)->get();
    }

    /**
     * Filter By Channel.
     *
     * @return $this
     */
    public function withChannel($id)
    {
        $this->model = $this->model->where('channel_id', $id);

        return $this;
    }


    /**
     * Include only article who's date posted are past.
     *
     * @return $this
     */
    public function noUpcoming(){
        $this->model = $this->model->where('date_posted', '<=' , date("Y-m-d H:i:s"));
        return $this;
    }

    /**
     * Gets the featured article or the first article.
     *
     * @return Article|null
     */
    public function featured($slug)
    {
        $poll = Option::where('slug' , $slug)->first();
        return $this->model->where('id' , $poll->value)->first();
    }

    /**
     * Get the paginated popular articles.
     * 
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popular($perPage = 10, $channel_id)
    {
        if (date("d") < 16) {
            $cutoffStart = date("Y-m") . '-1';
            $cutoffEnd = date("Y-m") . '-15';
        } else {
            $cutoffStart = date("Y-m") . '-16';
            $cutoffEnd = date("Y-m") . '-30';
        }

        $articles = [];

        $possible_articles = Article::where('channel_id', $channel_id)->count();

        if($possible_articles){

            if($possible_articles < 5)
                $perPage = $possible_articles;

            while (count($articles) < $perPage){

                $articles = $this->model->with('therapies', 'uploads')->getModel()
                    ->selectRaw('articles.*, count(articles.id) as aggregate')
                    ->where('channel_id', '=', $channel_id)
                    ->where('date_posted', '>=', $cutoffStart)
                    ->where('date_posted', '<=', $cutoffEnd)
                    ->leftJoin('views', 'views.viewable_id', '=', 'articles.id')
                    ->groupBy('articles.id')
                    ->orderBy('aggregate', 'DESC')
                    ->latest('articles.created_at')
                    ->paginate($perPage);

                $cutoffStart = Carbon::parse($cutoffStart)->subDays(15);
            }
        }
        return $articles;
    }

    /**
     * Get the archived articles.
     *
     * @return mixed
     */
    public function archived()
    {
        return $this->model->with('therapies','uploads')->latest()->paginate(6);
    }

     /**
     * Save the one who view the article.
     *
     * @param  Article $article
     * @param  View    $view
     *
     * @return array
     */
    public function view(Article $article, View $view)
    {

        return $article->views()->save($view);
    }

     /**
     * Save new article.
     *
     * @param  Article $article
     *
     * @return array
     */

    public function store($request , $therapies , $file)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $request['slug'] = $this->generateSlug($request['title']);
        $article = $this->model->create($request);
        $article->therapies()->sync($therapies);

        if($photo)
            $article->uploads()->createMany($photo);

        return $article;
    }

    public function update($request , $therapies , $file , Article $article)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);

        if($article->title!=$request['title'])
            $request['slug'] = $this->generateSlug($request['title']);

        $article->date_posted = $request['date_posted'];
        $article->update($request);
        $article->therapies()->sync($therapies);

        if($photo){
            $article->uploads()->delete();
            $article->uploads()->createMany($photo);
        } elseif ($photo==false && $request['keep_thumb']==false) {
            $article->uploads()->delete();
        }

        return $article;
    }
}
