<?php  namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Channel;
use MEDoctors\Repositories\Contracts\ChannelRepository;

class DbChannelRepository implements ChannelRepository {

    /**
     * @var Channel
     */
    protected $model;

    public function __construct(Channel $model)
    {
        $this->model = $model;
    }

}