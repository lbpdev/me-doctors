<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Country;
use MEDoctors\Repositories\Contracts\CountryRepository;

class DbCountryRepository implements CountryRepository {
    
    /**
     * @var Country
     */
    protected $model;
    
     /**
     * Create a new repository instance.
     *
     * @param Country $model
     */
    public function __construct(Country $model)
    {
        $this->model = $model;
    }
    
    /**
     * Get GCC countries only.
     *
     * @param  mixed $query
     *
     * @return mixed
     */
    public function gccCounties()
    {
        return $this->model->gccCounties()->get();
    }

    /**
     * Determine if the given country code belongs to GCC.
     *
     * @param  string  $country
     *
     * @return boolean
     */
    public function isGcc($country)
    {
        $gccCodes = $this->model->getGccCountryCodes();

        return in_array($country, $gccCodes);
    }

}