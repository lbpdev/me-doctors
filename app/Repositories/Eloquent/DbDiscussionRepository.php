<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Channel;
use MEDoctors\Models\Comment;
use MEDoctors\Models\Discussion;
use MEDoctors\Repositories\Eloquent\CanFindById;
use MEDoctors\Repositories\Contracts\DiscussionRepository;

class DbDiscussionRepository implements DiscussionRepository {
    
    use CanFindById, CanCreateSlug;

    /**
     * @var Discussion
     */
    protected $model;

    /**
     * @var Channel
     */
    protected $channel;

    /**
     * @var Comment
     */
    protected $comment;

    public function __construct(Discussion $model, Channel $channel, Comment $comment)
    {
        $this->model = $model;
        $this->channel = $channel;
        $this->comment = $comment;
    }

     /**
     * Get the panel discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPanelDiscussions($perPage = 15)
    {
        return $this->panelChannel()->popularByComments()->paginate($perPage);
    }

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularDoctorDiscussions($perPage = 15)
    {
        return $this->doctorsChannel()->popularByComments()->paginate($perPage);
    }

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPatientDiscussions($perPage = 15)
    {
        return $this->patientChannel()->popularByComments()->paginate($perPage);
    }

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularDoctorDiscussionsWithFilter($therapy , $perPage = 15)
    {
        return $this->doctorsChannel()->popularByCommentsWithFilter($therapy)->paginate($perPage);
    }

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPanelDiscussionsWithFilter($therapy , $perPage = 15)
    {
        return $this->panelChannel()->popularByCommentsWithFilter($therapy)->paginate($perPage);
    }

    /**
     * Get the doctor(diagnosis) discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularPatientDiscussionsWithFilter($therapy , $perPage = 15)
    {
        return $this->patientChannel()->popularByCommentsWithFilter($therapy)->paginate($perPage);
    }


    /**
     * Gets the latest paginated version of panel discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPanelPaginated($perPage = 15)
    {
        return $this->panelChannel()
                    ->published()
                    ->model
                    ->with('commentsCountRelation')
                    ->latest()->paginate($perPage);
    }
    /**
     * Gets the latest paginated version of doctor discussions.
     *
     * @param  int $perPage
     * 
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestDoctorPaginated($perPage = 15)
    {
        return $this->doctorsChannel()
                    ->published()
                    ->model
                    ->with('commentsCountRelation')
                    ->latest()->paginate($perPage);
    }


    /**
     * Gets the latest paginated version of patient discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPatientPaginated($perPage = 15)
    {
        return $this->patientChannel()
            ->published()
            ->model
            ->with('commentsCountRelation')
            ->latest()->paginate($perPage);
    }

    /**
     * Gets the latest version of patient discussions.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPatient($perPage = 15)
    {
        return $this->patientChannel()
            ->published()
            ->model
            ->with('commentsCountRelation')
            ->take($perPage)->get();
    }

    /**
     * Find a discussion by its slug.
     *
     * @param  string $slug
     *
     * @return mixed
     */
    public function findBySlug($slug)
    {
        $model = $this->model
                ->with('attachments')
                ->with('author.profile')
                ->with('author.roles')
                ->with(['comments' => function($query)
                {
                    $query->with('author.profile');
                    $query->with('attachments');
                    $query->where('status',1);
                    $query->latest();
                }]);

        return $model->whereSlug($slug)->first();
    }

    /**
     * Create a doctor discussion.
     *
     * @param  array  $attributes 
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createDoctorDiscussion(array $attributes, $attachments = [])
    {
        $attributes['slug'] = $this->generateSlug($attributes['title']);
        $discussion = $this->doctorsChannel()->model->create($attributes);

        if ($attachments)
        {
            $discussion->attachments()->createMany($attachments);
        }

        return $discussion;
    }

    /**
     * Create a doctor discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createPatientDiscussion(array $attributes, $attachments = [])
    {
        $attributes['slug'] = $this->generateSlug($attributes['title']);
        $discussion = $this->patientChannel()->model->create($attributes);

        if ($attachments)
        {
            $discussion->attachments()->createMany($attachments);
        }

        return $discussion;
    }


    /**
     * Create a panel discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function createPanelDiscussion(array $attributes, $attachments = [])
    {
        $attributes['slug'] = $this->generateSlug($attributes['title']);
        $attributes['status'] = 1;
        $discussion = $this->panelChannel()->model->create($attributes);

        if ($attachments)
        {
            $discussion->attachments()->createMany($attachments);
        }

        return $discussion->slug;
    }

    /**
     * Update a panel discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function updatePanelDiscussion(Discussion $discussion, array $attributes, $attachments = [], $remove_attachment)
    {
        if($discussion->title != $attributes['title'])
            $attributes['slug'] = $this->generateSlug($attributes['title']);

        $discussion->update($attributes);

        if ($remove_attachment==true)
        {
            $discussion->attachments()->delete();
        }

        if ($attachments)
        {
            $discussion->attachments()->createMany($attachments);
        }

        return redirect(route('adm_edit_panel',$discussion));
    }

    /**
     * Update a panel discussion.
     *
     * @param  array  $attributes
     * @param  array  $attachments
     *
     * @return Discussion
     */
    public function updateDoctorDiscussion(Discussion $discussion, array $attributes, $attachments = [], $remove_attachment)
    {
        if($attributes['title']!=$discussion->title)
            $attributes['slug'] = $this->generateSlug($attributes['title']);

        $discussion->update($attributes);


        if ($remove_attachment==true)
        {
            $discussion->attachments()->delete();
        }

        if ($attachments)
        {
            $discussion->attachments()->createMany($attachments);
        }

        return $discussion;
    }

    /**
     * Post a comment for the discussion.
     *
     * @param  int $userId
     * @param  int $discussion
     * @param  string $content
     * @param  array $attachments
     *
     * @return Comment
     */
    public function postComment($userId, $discussionId, $content, $attachments = [])
    {
        $comment = $this->comment->post($content, $userId);

        $this->findById($discussionId)->comments()->save($comment);

        if ($attachments)
        {
            $comment->attachments()->createMany($attachments);
        }
        return $comment;
    }

    /**
     * Post a comment for the discussion.
     *
     * @param  int $userId
     * @param  int $comment
     * @param  string $content
     * @param  array $attachments
     *
     * @return Comment
     */
    public function updateComment($userId, $commentId, $content, $attachments = [], $remove_attachment)
    {
        $commentData = array('content' => $content);
        $comment = Comment::where('id', $commentId)->first();
        $comment->update($commentData);

        if ($remove_attachment==true)
        {
            $comment->attachments()->delete();
        }

        if ($attachments)
        {
            $comment->attachments()->createMany($attachments);
        }

        return $comment;
    }


    /**
     * Set the discussion channel to doctor.
     *
     * @return mixed
     */
    private function panelChannel()
    {
        return $this->channel('panel');
    }

    /**
     * Set the discussion channel to patient.
     *
     * @return mixed
     */
    private function patientChannel()
    {
        return $this->channel('patient');
    }

    /**
     * Set the discussion channel to doctor.
     *
     * @return mixed
     */
    private function doctorsChannel()
    {
        return $this->channel('doctor');
    }

    /**
     * Sets the channel for getting the discussions.
     *
     * @param  string $channel
     *
     * @return null|$this
     */
    private function channel($channel)
    {
        $channelRecord = $this->channel->whereSlug($channel)->first();

        if ($channelRecord)
        {
            $this->model = $channelRecord->discussions();
        }

        return $this;
    }

    /**
     * Get only published items.
     *
     * @return mixed
     */
    public function published()
    {
        $this->model = $this->model->where('status',1);
        return $this;
    }

    /**
     * Get the popular discussions based on comments count.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function popularByComments()
    {
        return $this->model->with('comments')
                    ->selectRaw('discussions.*, count(comments.id) as aggregate')
                    ->leftJoin('comments', 'comments.discussion_id', '=', 'discussions.id')
                    ->groupBy('discussions.id')
                    ->orderBy('aggregate', 'DESC')
                    ->latest();
    }

    private function popularByCommentsWithFilter($therapy)
    {
        return $this->model->with('comments')
                    ->where('therapy_id',$therapy)
                    ->selectRaw('discussions.*, count(comments.id) as aggregate')
                    ->leftJoin('comments', 'comments.discussion_id', '=', 'discussions.id')
                    ->groupBy('discussions.id')
                    ->orderBy('aggregate', 'DESC')
                    ->latest();
    }

    /**
     * Gets all discussions for approval.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function unpublishedDoctorDiscussions()
    {
        return $this->doctorsChannel()->model->where('status',0)
            ->latest()->get();
    }

    /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedDoctorDiscussions()
    {
        return $this->doctorsChannel()->model->where('status', 0)->count();
    }

    /**
     * Gets all discussions for approval.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function unpublishedPanelDiscussions()
    {
        return $this->panelChannel()->model->where('status',0)
            ->latest()->get();
    }

    /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedPatientDiscussions()
    {
        return $this->patientChannel()->model->where('status', 0)->count();
    }

    /**
     * Gets all discussions for approval.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function unpublishedPatientDiscussions()
    {
        return $this->patientChannel()->model->where('status',0)
            ->latest()->get();
    }

    /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedPanelDiscussions()
    {
        return $this->panelChannel()->model->where('status', 0)->count();
    }

    /**
     * Gets all discussions for approval.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function unpublishedComments()
    {
        return $this->doctorsChannel()->comment->where('status',0)
            ->latest()->get();
    }

    /**
     * Count all discussions for approval.
     *
     * @return int
     */
    public function countUnpublishedComments()
    {
        return $this->comment->where('status', 0)->count();
    }

    /**
     * Publish a discussion.
     *
     * @param  Discussion $discussion
     *
     * @return Discussion
     */

    public function publish($item){
        $item->update(['status' => 1]);
        return $item;
    }

    /**
     * Unpublish a discussion.
     *
     * @param  Discussion $discussion
     *
     * @return Discussion
     */

    public function unpublish($item){
        $item->update(['status' => 0]);
        return $item;
    }


    /**
     * Get next set of records.
     *
     * @param  int $discussion_id
     * @param  int $offset
     * @param  Int $limit
     *
     * @return array
     */
    public function nextComments($discussion_id ,$offset, $limit){
        $discussion = Discussion::with('comments')->where('id',$discussion_id)->first();
        return $discussion->comments()->orderBy('created_at','desc')->with('author.profile','author.profile.avatar')->skip($offset)->take($limit)->get()->toArray();
    }
}