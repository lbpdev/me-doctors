<?php  namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Event;
use MEDoctors\Repositories\Contracts\EventRepository;
use MEDoctors\Services\Uploaders\EventThumbnailUploader;

class DbEventRepository implements EventRepository {

    use CanCreateSlug;
    /**
     * @var Event
     */
    protected $model;

    public function __construct(Event $model , EventThumbnailUploader $eventThumbnailUploader)
    {
        $this->model = $model;
        $this->uploader = $eventThumbnailUploader;
    }

    /**
     * Get the upcoming events.
     *
     * @param  int $perPage
     * 
     * @return mixed
     */
    public function paginatedUpcoming($perPage = 15)
    {
        return $this->model->upcoming()->orderBy('event_date_end','asc')->paginate($perPage);
    }

    /**
     * Get the past events.
     *
     * @param  int $perPage
     *
     * @return mixed
     */
    public function paginatedPast($perPage = 15)
    {
        return $this->model->past()->orderBy('event_date_end','desc')->paginate($perPage);
    }

    /**
     * Save new article.
     *
     * @param  Article $article
     *
     * @return array
     */

    public function store($request , $file)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $request['slug'] = $this->generateSlug($request['title']);

        $event = $this->model->create($request);

        if($photo)
            $event->uploads()->createMany($photo);

        return $event;
    }

    /**
     * Update an article.
     *
     * @return array $article
     */
    public function update($request , $file , $keep_thumb, Event $event)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);

        if($event->title!=$request['title'])
            $request['slug'] = $this->generateSlug($request['title']);

        $event->update($request);

        if($photo){
            $event->uploads()->delete();
            $event->uploads()->createMany($photo);
        } elseif ($photo==false && $keep_thumb==false) {
            $event->uploads()->delete();
        }

        return $event;
    }
}