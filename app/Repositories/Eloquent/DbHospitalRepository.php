<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Hospital;
use MEDoctors\Models\PremiumService;
use MEDoctors\Models\ServiceHospitalUser;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Eloquent\CanFindBySlug;
use MEDoctors\Services\Uploaders\HospitalAvatarUploader;

use MEDoctors\Models\View;

use Carbon\Carbon;
class DbHospitalRepository implements HospitalRepository {

    use CanFindBySlug;
    use CanCreateSlug;
    use CanFormatCountry;

    /**
     * @var Hospital
     */
    protected $model;

    public function __construct(Hospital $model , HospitalAvatarUploader $hospitalAvatarUploader,PremiumService $service)
    {
        $this->model = $model;
        $this->services = $service;
        $this->uploader = $hospitalAvatarUploader;
    }


    /**
     * store new hospital.
     *
     * @param  array $hospital
     * @param  array $contact
     * @param  array $facilities
     * @param  array $therapies
     * @param  array $location
     * @param  array|null $file
     *
     * @return array
     */
    public function store($hospital , $contact, $facilities, $therapies, $location , $file = null)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $contact = $this->model->generateContactCreateData($contact);
        $facilities = $this->model->generateFacilitiesCreateData($facilities);
        $therapies = $this->model->generateTherapiesCreateData($therapies);
        $hospital['slug'] = $this->generateSlug($hospital['name']);
        $hospital['verified'] = 1;

        $newHospital = $this->model->create($hospital);

        if($photo)
            $newHospital->avatar()->createMany($photo);

        if($contact)
            $newHospital->contacts()->createMany($contact);

        if($location)
            $newHospital->location()->create($this->formatCountry($location));

        if($therapies)
            $newHospital->therapies()->sync($therapies);

        if($facilities)
            $newHospital->facilities()->sync($facilities);

        return $newHospital;
    }

    /**
     * update hospital.
     *
     * @param  Hospital $hospital
     * @param  array $input
     * @param  array $location
     * @param  array $contact
     * @param  array $facilities
     * @param  array $therapies
     * @param  array $file
     * @param  array $keep_thumb
     *
     * @return array
     */
    public function update(Hospital $hospital, $input ,$contact, $facilities, $therapies, $location , $file, $keep_thumb ){

        $photo = ($file != null ? $this->uploader->upload($file) : false);

        $contact = $this->model->generateContactCreateData($contact);
        $facilities = $this->model->generateFacilitiesCreateData($facilities);
        $therapies = $this->model->generateTherapiesCreateData($therapies);

        if($hospital->name != $input['name'])
            $hospital->slug  = $this->generateSlug($input['name']);

        $hospital->name = $input['name'];
        $hospital->description = $input['description'];
        $hospital->website = $input['website'];

        $hospital->save();

        if($photo){
            $hospital->avatar()->delete();
            $hospital->avatar()->createMany($photo);
        } elseif(!$keep_thumb) {
            $hospital->avatar()->delete();
        }

        $hospital->contacts()->delete();
        if($contact)
            $hospital->contacts()->createMany($contact);

        if($therapies)
            $hospital->therapies()->sync($therapies);
        else
            $hospital->therapies()->delete();

        if($facilities)
            $hospital->facilities()->sync($facilities);
        else
            $hospital->facilities()->delete();

        $hospital->location()->delete();
        $hospital->location()->create($location);

        return $hospital;
    }


    /**
     * Get the hospitals paginated.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginated($perPage = 10)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Get the hospitals in number of items.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getOnly($perPage = 10)
    {
        return $this->model->take($perPage)->get();
    }


    /**
     * Create API readable format for hospitals and location.
     *
     * @param  array $hospital
     *
     * @return array
     */
    public function generateAutoLoadData($hospitals){
        $data = [];
        foreach($hospitals as $index=>$hospital){
            $data[$index]['value']         = $hospital->name;
            if($hospital->location){
            $data[$index]['location']      = $hospital->location->name;
            $data[$index]['country']       = $hospital->location->country;
            $data[$index]['city']          = $hospital->location->city;
            $data[$index]['state']         = $hospital->location->state;
            $data[$index]['street_number'] = $hospital->location->street_number;
            $data[$index]['street_route']  = $hospital->location->street_route;
            $data[$index]['post_code']     = $hospital->location->post_code;
            }
        }

        return $data;
    }

    /**
     * Add Schedule to user's hospitals.
     *
     * @param  Object $user
     *
     * @return array
     */
    public function generateHospitalSchedules($user){
        foreach($user->hospitals as $index=>$hospital){
            $data = [];
            foreach($hospital->schedules as $indexc=>$schedule){
                if($schedule->hospital_id == $hospital->id && $schedule->user_id == $user->id)
                    $data[] = $schedule->day;
            }
            $user->hospitals[$index]['schedules'] = $data;
        }

        return $user;
    }

    /**
     * get hospital relative data.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function withData($array){
        $this->model = $this->model->with($array);

        return $this;
    }


    /**
     * get hospitals with therapy area.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterSpecialty($specialty){
        if($specialty!='all') {
            $this->model = $this->model->whereHas('therapies', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }
        return $this;
    }

    /**
     * get hospitals in country.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterCountry($country){
        if($country!='all'){
            $this->model = $this->model->whereHas('location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }
        return $this;
    }

    /**
     * get hospitals in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterCity($city){
        if($city!='all'){
            $this->model = $this->model->whereHas('location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }
        return $this;
    }


    /**
     * Get next set of records.
     *
     * @param  int $offset
     * @param  Int $limit
     *
     * @return array
     */
    public function skipAndTake($offset, $limit){
        return $this->model->skip($offset)->take($limit)
            ->with('location')->get()->toArray();
    }

    /**
     * sort Hospitals.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function sortBy($sortType){
        switch ($sortType){
            case 'views':
                $this->model = self::mostViewed();
                break;
            default:
                $this->model = $this->model->orderBy('created_at', 'desc');
        }
        return $this;
    }

    /**
     * get only verified Hospitals.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function onlyVerified(){
        $this->model = $this->model->where('verified', 1);

        return $this;
    }

    /**
     * Save the one who viewed the user.
     *
     * @param  User $user
     * @param  View $view
     *
     * @return array
     */
    public function view(Hospital $hospital, View $view)
    {
        return $hospital->views()->save($view);
    }

    /**
     * Sort Hospitals by number of Views.
     *
     *
     */
    public function mostViewed()
    {
        return $this->model->getModel()
            ->selectRaw('hospitals.*, count(hospitals.id) as aggregate')
            ->leftJoin('views', function($join)
            {
                $join->on('views.viewable_id', '=', 'hospitals.id');
                $join->where('views.viewable_type', '=', get_class($this->model));
            })
            ->groupBy('hospitals.id')
            ->orderBy('aggregate', 'DESC');
    }

    /**
     * Count unverified hospitals.
     *
     *
     */
    public function countUnverifiedHospitals()
    {
        return $this->model->where('verified',0)->get()->count();
    }

    /**
     * Count all possible results.
     *
     * @return Array
     */
    public function getAllCount()
    {
        return $this->model->count();
    }


    /**
     * user avails service.
     *
     * @return Array
     */
    public function availService($id,$service_name)
    {
        $hospital    = $this->model->where('id',$id)->first();
        $service     = $this->services->where('slug',$service_name)->first();

        if($service->interval == "yearly")
            $expiration = Carbon::now()->addMonths(12);
        else
            $expiration = Carbon::now()->addDays(30);

        $hospital->services()->create(array(
            'active'             => 1,
            'premium_service_id' => $service->id,
            'expires_on'         => strtotime($expiration)
        ));
    }

    /**
     * pause user service.
     *
     * @return Array
     */
    public function toggleService($hospital_id)
    {
        $hospital    = $this->model->where('id',$hospital_id)->first();
        $service = $hospital->services()->first();

        if($service->active)
            $service->active = 0;
        else
            $service->active = 1;

        $service->save();

        return $hospital;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function featuredFirst(){
        $this->model = $this->model->whereHas('services',function($q){
            $q->where('active',1);
        });
        return $this;
    }
    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function noFeatured(){
        $this->model = $this->model->has('services','=',0);
        return $this;
    }

    public function getFeaturedWithFilters($specialty,$country,$city,$sort,$featuredCount,$take){
        $query = Hospital::query();

        $query = $query->with('location','avatar','therapies','facilities','contacts','services');

        if($specialty!='all') {
            $query = $query->whereHas('therapies', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }

        if($country!='all'){
            $query = $query->whereHas('location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }

        if($city!='all'){
            $query = $query->whereHas('location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }

        $query = $query->whereHas('services',function($q){
            $q->where('active',1);
        });

        if($sort=='views'){
            $query = $query->selectRaw('hospitals.*, count(hospitals.id) as aggregate')
                ->leftJoin('views', function($join)
                {
                    $join->on('views.viewable_id', '=', 'hospitals.id');
                    $join->where('views.viewable_type', '=', 'MEDoctors\Models\Hospital' );
                })
                ->groupBy('hospitals.id')
                ->orderBy('aggregate', 'DESC');
        } else {
            $query = $query->orderBy('created_at','DESC');
        }

        $query = $query->skip($featuredCount)
            ->take($take)
            ->get();

        return $query;
    }

    public function getFeaturedWithFiltersCount($specialty,$country,$city){
        $query = Hospital::query();

        if($specialty!='all') {
            $query = $query->whereHas('therapies', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }

        if($country!='all'){
            $query = $query->whereHas('location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }

        if($city!='all'){
            $query = $query->whereHas('location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }

        $query = $query->whereHas('services',function($q){
            $q->where('active',1);
        });

        $query = $query->count();

        return $query;
    }
}