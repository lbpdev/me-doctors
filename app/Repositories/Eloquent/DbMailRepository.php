<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Repositories\Contracts\MailRepository;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use MEDoctors\Models\User;
class DbMailRepository implements MailRepository {

    /**
     * @var Poll
     */
    protected $model;
    protected $item;
    protected $vote;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /*
     *
     * Send Mail
     *
     */

    public function sendMail($template, $sender_email, $sender_name, $recipient_email, $subject, $message){

	try
	{
	        Mail::send($template, $message , function($m) use($message,$recipient_email,$subject,$sender_name,$sender_email)
	        {
	            $m->from($sender_email, $sender_name);

	            $m->to($recipient_email)->subject($subject);
	        });
	}
	catch (\Exception $e)
	{
	    dd($e->getMessage());
	}

    }
}