<?php namespace MEDoctors\Repositories\Eloquent;

use MaxMind\WebService\Http\Request;
use MEDoctors\Http\Controllers\Controller;
use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\View;
use Illuminate\Support\Str;
use MEDoctors\Models\Option;

use MEDoctors\Repositories\Contracts\MessagesRepository;

use MEDoctors\Services\Uploaders\NewsFeedThumbnailUploader;

use Illuminate\Bus\Dispatcher;

use MEDoctors\Models\Message;
use MEDoctors\Models\ConversationMember;

class DbMessagesRepository implements MessagesRepository {

    public function __construct(Dispatcher $dispatcher){
        $this->dispatcher = $dispatcher;
        $this->user = \Auth::user();
    }

    public function store($request,$callback){

        $input = $request->input();

        Message::create(array(
            'sender_id' => $this->user->id,
            'conversation_id' => $input['conversation_id'],
            'message' => $input['message']
        ));

        $members = ConversationMember::with('member')
            ->where('conversation_id',$input['conversation_id'])
            ->where('user_id','!=',$this->user->id)
            ->get();

        $data['conversation_id'] = $input['conversation_id'];
        $data['link'] = $callback;
        $data['subject'] = $input['subject'];

        foreach($members as $member){
            $data['sender_name'] = $this->user->fname . ' ' . $this->user->lname;
            $this->sendNotification($request,$member,$data);
        }

        return redirect(route('messages_single',$input['conversation_id']));
    }

    public function sendNotification($request,$member,$data){

        $this->dispatcher->dispatchFrom('MEDoctors\Commands\SendMail', $request, [
            'template' => 'emails.notifications.private-message',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $member->member->email,
            'subject'  => 'New Private Message Received: '.$data['subject'],
            'message'  => $data
        ]);

    }
}
