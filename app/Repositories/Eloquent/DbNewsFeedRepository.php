<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\View;
use Illuminate\Support\Str;
use MEDoctors\Models\Option;
use MEDoctors\Repositories\Contracts\NewsFeedRepository;
use MEDoctors\Repositories\Eloquent\CanFindBySlug;
use MEDoctors\Repositories\Eloquent\CanCreateSlug;

use MEDoctors\Services\Uploaders\NewsFeedThumbnailUploader;

class DbNewsFeedRepository implements NewsFeedRepository {

    use CanFindBySlug, CanCreateSlug;

    /**
     * @var Article
     */
    protected $model;

    public function __construct(NewsFeed $model, NewsFeedThumbnailUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    /**
     * Get the latest articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 10)
    {
        return $this->model->orderBy('date_posted','DESC')->paginate($perPage);
    }

    /**
     * Get a limited collection of articles.
     *
     * @param  int $limit
     *
     * @return mixed
     */
    public function limit($limit = 15)
    {
        return $this->model->latest()->limit($limit)->get();
    }

    /**
     * Filter By Channel.
     *
     * @return $this
     */
    public function withChannel($id)
    {
        $this->model = $this->model->where('channel_id', $id);

        return $this;
    }


    /**
     * Get the paginated popular articles.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popular($perPage = 10, $channel_id)
    {
        return $this->model->with('therapies','uploads')->getModel()
            ->selectRaw('articles.*, count(articles.id) as aggregate')
            ->where('channel_id',  '=', $channel_id)
            ->leftJoin('views', 'views.viewable_id', '=', 'articles.id')
            ->groupBy('articles.id')
            ->orderBy('aggregate', 'DESC')
            ->latest('articles.created_at')
            ->paginate($perPage);
    }

    /**
     * Get the archived articles.
     *
     * @return mixed
     */
    public function archived()
    {
        return $this->model->with('uploads')->latest()->paginate(6);
    }

    /**
     * Save the one who view the article.
     *
     * @param  NewsFeed $newsFeed
     * @param  View    $view
     *
     * @return array
     */
    public function view(NewsFeed $newsFeed, View $view)
    {
        return $newsFeed->views()->save($view);
    }

    /**
     * Save new article.
     *
     * @param  Article $article
     *
     * @return array
     */

    public function store($request , $file)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $request['slug'] = $this->generateSlug($request['title']);
        $article = $this->model->create($request);

        if($photo)
            $article->uploads()->createMany($photo);

        return $article;
    }

    public function update($request , $file , NewsFeed $newsFeed)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);

        if($newsFeed->title!=$request['title'])
            $request['slug'] = $this->generateSlug($request['title']);

        $newsFeed->date_posted = $request['date_posted'];

        $newsFeed->update($request);

        if($photo){
            $newsFeed->uploads()->delete();
            $newsFeed->uploads()->createMany($photo);
        } elseif ($photo==false && $request['keep_thumb']==false) {
            $newsFeed->uploads()->delete();
        }

        return $newsFeed;
    }
}
