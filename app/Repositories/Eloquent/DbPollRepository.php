<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Option;
use MEDoctors\Models\Poll;
use MEDoctors\Models\PollItem;
use MEDoctors\Models\PollVote;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\PollRepository;
use MEDoctors\Repositories\Eloquent\CanCreateSlug;

class DbPollRepository implements PollRepository {

    use CanCreateSlug;
    /**
     * @var Poll
     */
    protected $model;
    protected $item;
    protected $vote;

    public function __construct(Poll $model , PollItem $item , PollVote $vote)
    {
        $this->model = $model;
        $this->item = $item;
        $this->vote = $vote;
    }
    
    /**
     * Get the latest poll with items.
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function latest()
    {
        return $this->model->latest()->first();
    }

    /**
     * Store New Poll
     *
     * @param string $poll
     * @param array $items
     *
     * @return array $poll
     */

    public function store($poll , $items)
    {
        $poll = $this->model->create(array('title'=>$poll,'status'=>1));
        $poll->items()->createMany($items);

        return $poll;
    }

    /**
     * Update survey .
     *
     * @param  User   $user
     * @param  array    $items
     *
     * @return mixed
     */
    public function update($pollData , array $items = [])
    {
        $poll = Poll::where('id',$pollData['id'])->first();

        $poll->update($pollData);

        foreach($items as $index=>$item)
        {

            if(isset($item['id']))
            {
                $new_item = PollItem::where('id',$item['id'])->first();
                $new_item->update(array('body'=>$item['body']));
                $item_ids[] = $item['id'];
            }
            else{
                $new_poll = $poll->items()->create(array('body'=>$item['body']));
                $item_ids[] = $new_poll->id;
            }
        }

        PollItem::where('poll_id', $pollData['id'])->whereNotIn('id', $item_ids)->delete();

        return $poll;
    }


    /**
     * Set the user id when getting the poll
     * to get the voted item of the user if it exists.
     *
     * @param int $userId
     *
     * @return $this
     */
    public function withVoteByUserId($userId)
    {
        $this->model = $this->model->itemsWithUserVote($userId);

        return $this;
    }

    /**
     * Set the user id when getting the poll
     * to get the voted item of the user if it exists.
     *
     * @param int $userId
     * @param int $slug
     *
     * @return $this
     */
    public function featured($userId , $slug)
    {
        $poll = Option::where('slug' , $slug)->first();
        return $this->model->where('id' , $poll->value)->first();
    }

    public function vote(User $user, $pollId, $pollItemId)
    {
        return $user->pollVotes()->sync([$pollItemId]);
    }

}