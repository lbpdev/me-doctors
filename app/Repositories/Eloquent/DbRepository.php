<?php namespace MEDoctors\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;

abstract class DbRepository {
    
    /**
     * @var Model
     */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Alias to set the "limit" value of the query.
     *
     * @param  int  $value
     * @return $this
     */
    public function take($limit = 100)
    {
       $this->model = $this->model->take($limit);

       return $this;
    }

}