<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\SurveyEntries;
use MEDoctors\Models\SurveyItem;
use MEDoctors\Models\SurveyItemChoice;
use MEDoctors\Models\User;
use MEDoctors\Models\Survey;
use MEDoctors\Repositories\Contracts\SurveyRepository;

class DbSurveyRepository implements SurveyRepository {

    use CanCreateSlug;
    /**
     * @var Survey
     */
    protected $model;

    public function __construct(Survey $model)
    {
        $this->model = $model;
    }


    /**
     * Filter By Channel.
     *
     * @return $this
     */
    public function withChannel($id)
    {
        $this->model = $this->model->with('therapies')->where('channel_id', $id);

        return $this;
    }

    /**
     * Get the latest paginated surveys.
     *
     * @param  integer $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 15, $channel = 2)
    {
        return $this->withChannel($channel)->model->latest()->paginate($perPage);
    }

    /**
     * Get the first latest survey.
     *
     * @return Survey|null
     */
    public function first()
    {
        return $this->model->latest()->first();
    }

    /**
     * Find a survey by its slug.
     *
     * @param  string $slug
     *
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->model->with('items.choices')->where('slug',$slug)->first();
    }

    /**
     * Save the new survey .
     *
     * @param  User   $user         
     * @param  array    $items
     *
     * @return mixed
     */
    public function store($survey , $therapies , array $items = [])
    {
        $survey['slug'] = $this->generateSlug($survey['title']);
        $new_survey = $this->model->create($survey);

        if($therapies)
            $new_survey->therapies()->sync($therapies);

        foreach($items['survey']['items'] as $item){

            if(isset($item['choices'])){ // SELECT TYPE ITEM
                $new_item = $new_survey->items()->create(array('value'=>$item['value'],'allow_multiple' => $item['allow_multiple']));

                foreach($item['choices'] as $choice) {
                    $new_item->choices()->create($choice);
                }
            } else { // TEXT TYPE QUESTION
                $new_survey->items()->create(array('value'=>$item['value']));
            }
        }

        return $new_survey;
    }


    /**
     * Update survey .
     *
     * @param  User   $user
     * @param  array    $items
     *
     * @return mixed
     */
    public function update($surveyData , $therapies , array $items = [])
    {
        $survey = Survey::where('id',$surveyData['survey_id'])->first();

        if( $surveyData['title'] != $survey->title)
            $surveyData['slug'] = $this->generateSlug($survey['title']);

        $survey->update($surveyData);

        $survey->therapies()->sync($therapies ? $therapies : array());

        foreach($items['survey']['items'] as $index=>$item)
        {

            if(isset($item['id']))
            {
                $new_item = SurveyItem::where('id',$item['id'])->first();
                $new_item->update(array('value'=>$item['value'],'allow_multiple' => $item['allow_multiple']));
                $item_ids[] = $item['id']; $choice_ids[$index]['item'] =  $item['id'];
            }
            else{
                $new_item = $survey->items()->create(array('value'=>$item['value'],'allow_multiple' => $item['allow_multiple']));
                $item_ids[] = $new_item->id; $choice_ids[$index]['item'] =  $new_item->id;

            }

            foreach($item['choices'] as $choice)
            {
                if(isset($choice['id']))
                {
                    $old_item = SurveyItemChoice::where('id',$choice['id'])->first();
                    $old_item->update(array('value' => $choice['value']));
                    $choice_ids[$index][] = $choice['id'];
                }
                else
                {
                    $x = $new_item->choices()->create($choice);
                    $choice_ids[$index][] = $x->id;
                }
            }

        }

        SurveyItem::where('survey_id', $surveyData['survey_id'])->whereNotIn('id', $item_ids)->delete();
        foreach($item_ids as $index=>$item_id)
            SurveyItemChoice::where('survey_item_id', $item_id)->whereNotIn('id', $choice_ids[$index])->delete();

        return $survey;
    }

    /**
     * Save the survey answers of the user.
     *
     * @param  User   $user
     * @param  int $surveyItemId
     * @param  array  $votes
     *
     * @return mixed
     */
    public function saveUserVotes(User $user, $surveyId, array $votes = [])
    {
        $survey = Survey::with('items.choices')->where('id',$surveyId)->first();

        foreach($survey->items as $item){
            foreach($item->choices as $choice){
                SurveyEntries::where('survey_item_choice_id', $choice->id)
                             ->where('user_id', $user->id)->delete();
            }
        }

        $user->surveyEntries()->sync($votes, false);
    }


    /**
     * Save the survey answers of the user.
     *
     * @param  User   $user
     * @param  int $surveyItemId
     * @param  array  $answers
     *
     * @return mixed
     */
    public function saveUserAnswers(User $user, $surveyItemId, array $answers = [])
    {
        return $user->surveyEntries()->sync($answers, false);
    }

    /**
     * Get user entries by survey.
     *
     * @param  User   $user 
     * @param  Survey $survey
     *
     * @return array
     */
    public function userEntriesBySurvey(User $user, Survey $survey)
    {
        $choices = $survey->items->lists('choices');
        $choiceIds = [];

        foreach ($choices as $choice)
        {
            $choiceIds[] = $choice->lists('id');
        }

      return $user->surveyEntries()
                   ->whereIn('survey_item_choice_id', array_collapse($choiceIds))
                   ->lists('survey_entries.survey_item_choice_id');
    }

    /**
     * get surveys in specific therapy area.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterTherapy($therapies){
        if($therapies) {
            $this->model = $this->model->whereHas('therapies', function ($q) use ($therapies) {
                $q->where('slug', $therapies);
            });
        }

        return $this;
    }

    /**
     * Sort data by date.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function sortDate($sort){
        $this->model = $this->model->orderBy('created_at',$sort);
        return $this;
    }
}