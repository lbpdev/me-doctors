<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Therapy;
use MEDoctors\Repositories\Contracts\TherapyRepository;
use MEDoctors\Repositories\Eloquent\CanFindBySlug;

class DbTherapyRepository implements TherapyRepository {

    use CanFindBySlug;
    
    /**
     * @var Therapy
     */
    protected $model;

    public function __construct(Therapy $model)
    {
        $this->model = $model;
    }
    
    /**
     * Get all the therapies as array.
     *
     * @return array
     */
    public function forSelect()
    {
        return $this->model->with('therapy')->lists('name', 'id');
    }

     /**
     * Find therapy from the database by name.
     *
     * @param  array|string $name
     *
     * @return mixed
     */
    public function findByName($name)
    {
        return $this->model->whereIn('name', (array) $name)->get();
    }

}