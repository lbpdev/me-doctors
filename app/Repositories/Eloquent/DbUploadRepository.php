<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Upload;
use MEDoctors\Repositories\Contracts\UploadRepository;

class DbUploadRepository implements UploadRepository {
    
    /**
     * @var Upload
     */
    protected $model;

    public function __construct(Upload $model)
    {
        $this->model = $model;
    }

    /**
     * Save the attachments to the database.
     *
     * @param  array  $records
     *
     * @return array
     */
    public function save(array $records)
    {
        // stubs..
    }
    
}