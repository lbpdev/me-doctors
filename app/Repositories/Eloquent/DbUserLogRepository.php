<?php namespace MEDoctors\Repositories\Eloquent;

use Illuminate\Http\Request;
use MEDoctors\Models\User;
use MEDoctors\Models\UserLog;
use MEDoctors\Repositories\Contracts\UserLogRepository;

class DbUserLogRepository implements UserLogRepository {

    public function __construct(UserLog $userLog, Request $request){
        $this->model = $userLog;
        $this->request = $request;
    }

    /**
     * Store Log Entry
     * @param $username
     * @param $email
     * @param $log_type
     * @return mixed
     */
    public function storeEntry($username,$email,$log_type){
        $this->model->create(array(
            'username' => $username,
            'email' => $email,
            'ip' => $this->request->ip(),
            'log_type' => $log_type
        ));

        return true;
    }

}
