<?php namespace MEDoctors\Repositories\Eloquent;

use Carbon\Carbon;
use Illuminate\Bus\Dispatcher;
use Illuminate\Support\Facades\Auth;
use MEDoctors\Models\PremiumService;
use MEDoctors\Models\Referral;
use MEDoctors\Models\Role;
use MEDoctors\Models\User;
use MEDoctors\HospitalUser;
use MEDoctors\Models\View;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\Profile;

use Illuminate\Support\Facades\Session;
use MEDoctors\Models\Upload;
use MEDoctors\Models\Language;
use MEDoctors\Models\Location;
use MEDoctors\Models\PhoneNumber;
use MEDoctors\Models\Therapy;
use Illuminate\Support\Facades\Hash;
use MEDoctors\Models\HospitalSchedule;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Repositories\Eloquent\User\Filters;
use MEDoctors\Repositories\Eloquent\User\Traits;
use MEDoctors\Services\Uploaders\ProfileAvatarUploader;

use MEDoctors\Repositories\Eloquent\CanFormatCountry;

class DbUserRepository implements UserRepository {

    use CanFormatCountry,Filters,Traits;

   /**
    * @var User
    */
   protected $model;
   protected $hospital;
   protected $location;
   protected $uploader;

   /**
    * Create instance of DbUserRepository
    *
    * @param User $model
    */
   public function __construct(
       Profile $profile ,
       User $model ,
       Hospital $hospital ,
       Location $location ,
       ProfileAvatarUploader $uploader ,
       PremiumService $services ,
       Upload $upload,
       PhoneNumber $phoneNumber,
       Dispatcher $dispatcher
   )
   {
       $this->model = $model;
       $this->dispatcher = $dispatcher;
       $this->hospital = $hospital;
       $this->location = $location;
       $this->upload = $upload;
       $this->uploader = $uploader;
       $this->profile = $profile;
       $this->phone_number = $phoneNumber;
       $this->services = $services;
       $this->user = \Auth::user();
   }



    /**
     * Create a doctor user.
     *
     * @param  array  $data
     *
     * @return User
     */
    public function createDoctor(array $data)
    {
        $user = $this->model->create($data);

        $role = Role::where('name', 'Doctor')->first();

        $user->roles()->sync([$role->id]);

            $primaryContact = $this->phone_number->createInstanceSingle('personal',$data['phone_numbers_personal']);
            $user->contacts()->save($primaryContact);

        if(trim($data['phone_numbers_work'])){
            $workContact = $this->phone_number->createInstanceSingle('work',$data['phone_numbers_work']);
            $user->contacts()->save($workContact);
        }

        if($data['education'])
            $user->educations()->create(array('school'=>$data['education']));
        if($data['certifications'])
            $user->certifications()->create(array('name'=>$data['certifications']));

        if (isset($data['therapies']))
        {
            $user->specialties()->sync($data['therapies']);
        }

        return $user;
    }

    /**
     * Create a doctor user.
     *
     * @param  array  $data
     *
     * @return User
     */
    public function createPatient(array $data)
    {
        $user = $this->model->create($data);

        $role = Role::where('name', 'Patient')->first();

        $user->roles()->sync([$role->id]);

        return $user;
    }

    /**
     * Attach credentials for doctor.
     *
     * @param  User   $doctor     
     * @param  array  $credentials
     *
     * @return void
     */
    public function attachDoctorCredentials(User $doctor, array $credentials)
    {
        $doctorCredentials = $doctor->credentials()->create($credentials);

        if ($attachments = $credentials['attachments'])
        {
            $doctorCredentials->attachments()->createMany($attachments);
        }
    }

    /**
     * Confirm user's email address by token.
     *
     * @param  string $token
     *
     * @return mixed
     */
    public function confirmEmail($token)
    {
        $user = $this->model->where('token',$token)->first();

        if($user)
        {
            $user->confirmEmail();

            $referral = Referral::where('user_id',$user->id)->first();

            if($referral){
                $referral->registered = 1;
                $referral->save();
            }

            return true;
        }

        return false;
    }

    /**
     * Count unverified hospitals.
     *
     *
     */
    public function countUnverifiedDoctors()
    {
        return $this->unverifiedDoctorsQuery()->get()->count();
    }

    /**
     * Verify the doctor by id.
     *
     * @param  int $id
     *
     * @return mixed
     */
    public function verifyDoctor($id)
    {
        $user = $this->model->findOrFail($id);

        $user->token = str_random(30);

        $user->save();

        return $user;
    }

    /**
     * rate a doctor.
     *
     * @param  User $user
     * @param  Array $ratings
     * @param  int $rater_id
     *
     * @return mixed
     */
    public function rateDoctor($user , $ratings , $rater_id)
    {
        self::deleteRatingsByUser($user , $rater_id);

        if($ratings['friendliness'])
            $user->friendliness()->create(array('rater_id' => $rater_id , 'value' => $ratings['friendliness']));

        if($ratings['helpfullness'])
        $user->helpfulness()->create(array('rater_id' => $rater_id , 'value' => $ratings['helpfullness']));

        if($ratings['bedside'])
        $user->bedsideManner()->create(array('rater_id' => $rater_id , 'value' => $ratings['bedside']));

        if($ratings['medical'])
        $user->medicalKnowledge()->create(array('rater_id' => $rater_id , 'value' => $ratings['medical']));

        if($ratings['professionalism'])
        $user->professionalism()->create(array('rater_id' => $rater_id , 'value' => $ratings['professionalism']));

        if($ratings['waiting'])
        $user->waitingTime()->create(array('rater_id' => $rater_id , 'value' => $ratings['waiting']));

        return true;
    }

    /**
     * delete doctor ratings by user.
     *
     * @param  User $user
     * @param  Array $ratings
     * @param  int $rater_id
     *
     * @return bool
     */
    public function deleteRatingsByUser($user , $rater_id)
    {
        $user->friendliness()->where('rater_id', $rater_id)->delete();
        $user->helpfulness()->where('rater_id', $rater_id)->delete();
        $user->bedsideManner()->where('rater_id', $rater_id)->delete();
        $user->medicalKnowledge()->where('rater_id', $rater_id)->delete();
        $user->professionalism()->where('rater_id', $rater_id)->delete();
        $user->waitingTime()->where('rater_id', $rater_id)->delete();

        return true;
    }

    /**
     * Delete the user by id.
     *
     * @param  int $id
     *
     * @return mixed
     */
    public function deleteById($id)
    {
        $user = $this->model->findOrFail($id);

        $user->forceDelete();
        
        return $user;
    }

     /**
     * Updates user's education.
     *
     * @param User   $user
     * @param array $educations
     *
     * @return  array|bool
     */
    public function updateEducation(User $user, array $educations = [], $die)
    {
        $data = [];
        $user->educations()->delete();

        if ($educations)
        {
            $user->educations()->createMany($educations);
            foreach($user->educations as $education)
                $data[] = $education->school;
        }

        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }

    /**
     * Updates user's achievements.
     *
     * @param User   $user
     * @param array $achievements
     *
     * @return  array|bool
     */
    public function updateAchievements(User $user, array $achievements = [], $die)
    {
        $data = [];
        $user->achievements()->delete();

        if ($achievements)
        {
            $user->achievements()->createMany($achievements);

            foreach($user->achievements as $achievement)
                $data[] = $achievement->name;

        }

        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }

    /**
     * Updates user's publications.
     *
     * @param User   $user
     * @param array $publications
     *
     * @return  array|bool
     */
    public function updatePublications(User $user, array $publications = [], $die)
    {
        $data = [];
        $user->publications()->delete();

        if ($publications)
        {
            $user->publications()->createMany($publications);

            foreach($user->publications as $publication)
                $data[] = $publication->name;

        }

        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }

    /**
     * Updates user's languages.
     *
     * @param User   $user
     * @param array $languages
     *
     * @return  array|bool
     */
    public function updateLanguages(User $user, array $languages = [], $die)
    {
        $data = [];
        if ($languages)
        {
            $user->languages()->sync($languages);
            foreach($user->languages as $language)
                $data[] = $language->name;

        }
        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }

    /**
     * Updates user's certifications.
     *
     * @param User   $user
     * @param array $certifications
     *
     * @return  array|bool
     */
    public function updateCertifications(User $user, array $certifications = [], $die)
    {
        $data = [];
        $user->certifications()->delete();

        if ($certifications)
        {
            $user->certifications()->createMany($certifications);

            foreach($user->certifications as $certification)
                $data[] = $certification->name;

        }

        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }


    /**
     * Updates user's work.
     *
     * @param User   $user
     * @param array $work
     *
     * @return  array|bool
     */
    public function updateWork(User $user, array $work = null, $die)
    {
        $data = [];
        $user->work()->delete();
        HospitalSchedule::where('user_id' , $user->id)->delete();

        if ($work) {
            $hospitals = $this->hospital->getDetails($work);

            foreach ($hospitals as $index => $hospital) {

                $query_hospital = Hospital::where('name', $hospital->name)->first();

                if (count($query_hospital) == 0){


                    if(Auth::user()->hasRole('Administrator'))
                        $hospital['verified'] = 1;

                    $query_hospital = $user->hospitals()->save($hospital);
                    $mail_data['hospital_name'] = $query_hospital->name;
                    $mail_data['hospital_slug'] = $query_hospital->slug;

                    if(!Auth::user()->hasRole('Administrator')){
                        // Notify Admin of new Hospital
                        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
                            'template' => 'emails.admin.pending-hospital',
                            'sender_email' => 'mailman@middleeastdoctor.com',
                            'sender_name'  => 'Middle East Doctor',
                            'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
                            'subject'  => 'New Hospital Verification',
                            'message'  => $mail_data
                        ]);
                    }
                }
                else
                    $user->work()->create(['hospital_id' => $query_hospital->id]);

                if (isset($work[$index]['days'])) {
                    $schedules = $this->hospital->generateSchedule($user->id, $work[$index]['days']);
                    $query_hospital->schedules()->createMany($schedules);
                }

                $locations = $this->location->getAddressAttribute($query_hospital, $work[$index]);
                self::updateLocation($query_hospital, $locations);
            }

            if ($user->work){
                foreach ($user->work as $index=>$work) {
                    $hospital = Hospital::with('location','schedules')->where('id', $work->hospital_id)->first();
                    $data[$index]['name'] = $hospital->name;
                    $data[$index]['location'] = $hospital->location->name;

                    foreach($hospital->schedules as $schedule){
                        if($user->id == $schedule->user_id)
                        $data[$index]['schedules'][] = $schedule->day;
                    }
                }
            }
        }

        if(\BrowserDetect::detect()->browserFamily!="Internet Explorer"){
            echo json_encode($data);
            if($die)
                die();
        }
    }

    /**
     * Updates hospital location.
     *
     * @param User   $hospital
     * @param array $locations
     *
     * @return  array|bool
     */

    public function updateLocation(Hospital $hospital, $location)
    {
        $hospital->location()->delete();
        $hospital->location()->save($this->formatCountry($location));

        return true;
    }


    public function updateProfile(User $user, array $profile = []){

        /*
         * Update Profile
         */
        $user_profile = $user->profile()->first();

        if($user_profile){
            $user_profile->update($profile);
        }
        else {
            $profile = $this->profile->createInstance($profile);
            $user->profile()->save($profile);
        }

    }

    public function updateUser(User $user, array $account = [], $therapies){
        /*
         * Update User Details
         */

        if(!$account['password'])
            unset($account['password']);

        if($account['email'] == $user->email)
            unset($account['email']);
        else{
            if(User::where('email',$account['email'])->first()){
                Session::flash('error','Email "'.$account['email'].'" is already registered on MED.');
                return redirect()->back();
            }
        }
        if($account['username'] == $user->username)
            unset($account['username']);

        else{
            if(User::where('username',$account['username'])->first()){
                Session::flash('error','Username "'.$account['username'].'" is already registered on MED.');
                return redirect()->back();
            }
        }

        $user->update($account);
        $user->specialties()->sync($therapies);
    }


    public function updateContacts(User $user, array $contacts = []){

        $contacts = $this->phone_number->createInstance($contacts);

        if($user->contacts())
            $user->contacts()->delete();

        $user->contacts()->saveMany($contacts);

    }


    public function updateAvatar(User $user, $file){
        $photo = ($file != null ? $this->uploader->upload($file) : false);

        if($photo){
            $photo = $this->upload->createInstance($photo);

            if($user->profile->avatar())
                $user->profile->avatar()->delete();

            $user->profile->avatar()->saveMany($photo);
        }

    }

    /**
     * Remove user's avatar.
     *
     * @param User $user
     *
     * @return  array|bool
     */
    public function removeAvatar(User $user){
        $user->profile->avatar()->delete();
    }

    public function getUsersForMessage($channel_id)
    {
        $users = User::whereHas('roles',function($q) use($channel_id){
            $q->where('role_id',$channel_id);
        })->get();

        $data = [];
        $loop = 0;
        foreach($users as $user){
            if($user->id != $this->user->id){
                $data[$loop] = $user->fname . ' ' . $user->lname . ' [ ' . $user->username .' ]';
                $loop++;
            }
        }
        return $data;
    }

    public function getUsersForMessageTherapy($channel_id,$therapy_id)
    {

        $data = '';     

        if($therapy_id<=Therapy::count()){

            $users = User::whereHas('roles',function($q) use($channel_id){
            $q->where('role_id',$channel_id);
            })->whereHas('specialties',function($q) use($therapy_id){
                $q->where('therapy_id',$therapy_id);
            })->get();

        } else {
            $users = User::whereHas('roles',function($q) use($channel_id){
                $q->where('role_id',$channel_id);
            })->get();
        }   

        foreach($users as $user)
            if($user->id != $this->user->id)
                $data .= $user->fname . ' ' . $user->lname . ' [ ' . $user->username .' ] ';

        return $data;
    }

    /**
     * sort doctors.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function sortBy($sortType){
        switch ($sortType) {
            case 'views':
                $this->model = self::mostViewed();
                break;
            default:
                $this->model = $this->model->orderBy('created_at', 'desc');
        }
        return $this;
    }

    /**
     * Save the one who viewed the user.
     *
     * @param  User $user
     * @param  View $view
     *
     * @return array
     */
    public function view(User $user, View $view)
    {
        return $user->views()->save($view);
    }

    /**
     * Get next set of records.
     *
     * @param  int $offset
     * @param  Int $limit
     *
     * @return array
     */
    public function skipAndTake($offset, $limit){
        $this->model = $this->model->skip($offset)->take($limit);
        return $this;
    }

    /**
     * Count all possible results.
     *
     * @return Array
     */
    public function getAllCount()
    {
        return $this->model->count();
    }

    /**
     * user avails service.
     *
     * @return Array
     */
    public function availService($username,$service_name)
    {
        $user    = $this->user->where('username',$username)->first();
        $service = $this->services->where('slug',$service_name)->first();

        if($service->interval == "yearly")
            $expiration = Carbon::now()->addMonths(12);
        else
            $expiration = Carbon::now()->addDays(30);

        $user->services()->create(array(
            'active'             => 1,
            'premium_service_id' => $service->id,
            'expires_on'         => strtotime($expiration)
        ));
    }

    /**
     * pause user service.
     *
     * @return Array
     */
    public function toggleService($username)
    {
        $user    = $this->user->where('username',$username)->first();
        $service = $user->services()->first();

        if($service->active)
            $service->active = 0;
        else
            $service->active = 1;

        $service->save();

        return $user;
    }

    public function countFeatured($specialty,$country,$city){
        $query = User::query();

        if($specialty!='all') {
            $query = $query->whereHas('specialties', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }

        if($country!='all'){
            $query = $query->whereHas('hospitals.location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }

        if($city!='all'){
            $query = $query->whereHas('hospitals.location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }

        $query = $query
            ->whereHas('roles', function($q){
                $q->where('role_id' ,'=', 2);
            })->whereHas('services',function($q){
                $q->where('active',1);
            })
            ->count();

        return $query;
    }

    public function getFeaturedWithFilters($specialty,$country,$city,$sort,$featuredCount,$take){
        $query = User::query();

        $query = $query->with(
            'friendliness',
            'bedsideManner',
            'helpfulness',
            'medicalKnowledge',
            'professionalism',
            'waitingTime',
            'specialties',
            'services',
            'contacts',
            'hospitals.location',
            'profile.avatar'
        );

        if($specialty!='all') {
            $query = $query->whereHas('specialties', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }

        if($country!='all'){
            $query = $query->whereHas('hospitals.location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }

        if($city!='all'){
            $query = $query->whereHas('hospitals.location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }

        $query = $query
            ->whereHas('roles', function($q){
                $q->where('role_id' ,'=', 2);
            })->whereHas('services',function($q){
                $q->where('active',1);
            });

        if($sort=='views'){

            $query = $query->selectRaw('users.*, count(users.id) as aggregate')
                ->leftJoin('views', function($join)
                {
                    $join->on('views.viewable_id', '=', 'users.id');
                    $join->where('views.viewable_type', '=', 'MEDoctors\Models\User' );
                })
                ->groupBy('users.id')
                ->orderBy('aggregate', 'DESC');
        } else {
            $query = $query->orderBy('created_at','DESC');
        }

        $query = $query->skip($featuredCount)
            ->take($take)
            ->get();

        return $query;
    }

    public function createReferral($data,$user,$registered){
        $referrer = User::where('email',$data['referred_by'])->first();

        if($referrer){
            $referral = Referral::create(array(
                'user_id' => $user->id,
                'referrer_id' => $referrer->id,
                'registered' => $registered,
                'receiver' => $user->email
            ));

            return $referral;
        }

        return false;
    }

    public function createReferralEmail($referrer_id,$receiver){

        $referral = Referral::create(array(
            'user_id' => null,
            'referrer_id' => $referrer_id,
            'registered' => 0,
            'receiver' => $receiver
        ));

        return $referral;

    }
}
