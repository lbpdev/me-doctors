<?php namespace MEDoctors\Repositories\Eloquent;

use Carbon\Carbon;
use MEDoctors\Models\User;
use MEDoctors\Repositories\Contracts\UserStatRepository;

class DbUserStatRepository implements UserStatRepository {
    
    /**
     * @var User
     */
    protected $model;
    
     /**
     * Create a new repository instance.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Count the new users.
     *
     * @return int
     */
    public function newUsers()
    {
        return $this->model
                        ->whereVerified(true)
                        ->where('created_at', '>=', Carbon::now()->subMonth())
                        ->count();
    }

    /**
     * Count the unverified doctors.
     *
     * @return int
     */
    public function unverifiedDoctors()
    {
        return $this->model->whereVerified(false)->whereNull('token')->count();
    }
    
    
}