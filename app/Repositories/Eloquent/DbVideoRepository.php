<?php  namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\Event;
use MEDoctors\Repositories\Contracts\VideoRepository;
use MEDoctors\Services\Uploaders\VideoThumbnailUploader;
use MEDoctors\Models\Video;

class DbVideoRepository implements VideoRepository {

    use CanCreateSlug;
    /**
     * @var Event
     */
    protected $model;

    public function __construct(Video $model , VideoThumbnailUploader $videoThumbnailUploader)
    {
        $this->model = $model;
        $this->uploader = $videoThumbnailUploader;
    }

    /**
     * Save new article.
     *
     * @param  $request
     * @param  $file
     *
     * @return Video $video
     */

    public function store($request , $file)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);
        $request['slug'] = $this->generateSlug($request['title']);

        $video = $this->model->create($request);

        if($photo)
            $video->uploads()->createMany($photo);

        return $video;
    }

    /**
     * Get the latest videos.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestPaginated($perPage = 10)
    {
        return $this->model->orderBy('created_at','DESC')->paginate($perPage);
    }


    /**
     * Filter By Channel.
     *
     * @return $this
     */
    public function withChannel($id)
    {
        $this->model = $this->model->where('channel_id', $id);

        return $this;
    }

    /**
     * Update an Video.
     * @param $request
     * @param $file
     * @param $keep_thumb
     * @param  $video
     * @return Video $video
     */
    public function update($request , $file , $keep_thumb, $video)
    {
        $photo = ($file != null ? $this->uploader->upload($file) : false);

        if($video->title!=$request['title'])
            $request['slug'] = $this->generateSlug($request['title']);

        $video->update($request);

        if($photo){
            $video->uploads()->delete();
            $video->uploads()->createMany($photo);
        } elseif ($photo==false && $keep_thumb==false) {
            $video->uploads()->delete();
        }

        return $video;
    }
}