<?php namespace MEDoctors\Repositories\Eloquent;

use MEDoctors\Models\View;
use MEDoctors\Repositories\Contracts\ViewRepository;

use Carbon\Carbon;

class DbViewRepository implements ViewRepository {

    use CanFindById;

    /**
     * @var Discussion
     */
    protected $model;

    public function __construct(View $model)
    {
        $this->model = $model;
    }

    public function create($user_ip, $user_id)
    {
//        $now = Carbon::now();
//
//        $latestView = $this->model
//                        ->where('user_id',$user_id)
//                        ->where('user_ip',$user_ip)
//                        ->orderBy('created_at','DESC')
//                        ->select('created_at')
//                        ->first();
//
//        dd($this->model->fill(compact('user_ip', 'user_id')));
//
//        if($latestView->created_at->diffInMinutes($now)>10)
        return $this->model->fill(compact('user_ip', 'user_id'));

//        return false;
    }


}