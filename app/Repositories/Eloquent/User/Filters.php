<?php
namespace MEDoctors\Repositories\Eloquent\User;

trait Filters {
    /**
     * Filter publications that has a name.
     *
     * @param  array $publications
     *
     * @return array
     */
    public function filterPublicationsWithName($publications)
    {
        return array_filter($publications, function($item)
        {
            return ! empty($item['name']);
        });
    }

    /**
     * Filter education that has a school.
     *
     * @param  array $education
     *
     * @return array
     */
    public function filterWorkWithLocation($work)
    {
        return array_filter($work, function($item)
        {
            return ! empty($item['location']);
        });
    }
    /**
     * Filter certifications that are blank.
     *
     * @param  array $certification
     *
     * @return array
     */
    public function filterCertifications($certification)
    {
        return array_filter($certification, function($item)
        {
            return ! empty($item['name']);
        });
    }

    /**
     * Filter education that has a school.
     *
     * @param  array $education
     *
     * @return array
     */
    public function filterEducationWithSchool($education)
    {
        return array_filter($education, function($item)
        {
            return ! empty($item['school']);
        });
    }


    /**
     * Filter achievement that has a name.
     *
     * @param  array $achievement
     *
     * @return array
     */
    public  function filterAchievementWithName($achievement)
    {
        return array_filter($achievement, function($item)
        {
            return ! empty($item['name']);
        });
    }



    /**
     * get doctors in specific therapy area.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterSpecialty($specialty){
        if($specialty!='all') {
            $this->model = $this->model->whereHas('specialties', function ($q) use ($specialty) {
                $q->where('slug', $specialty);
            });
        }
        return $this;
    }

    /**
     * get doctors in country.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterCountry($country){
        if($country!='all'){
            $this->model = $this->model->whereHas('hospitals.location' , function($q) use($country) {
                $q->where('country', $country);
            });
        }
        return $this;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function filterCity($city){
        if($city!='all'){
            $this->model = $this->model->whereHas('hospitals.location' , function($q) use($city) {
                $q->where('city', $city);
            });
        }
        return $this;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function featuredOnly($city){
        $this->model = $this->model->whereHas('hospitals.location' , function($q) use($city) {
            $q->where('city', $city);
        });
        return $this;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function featuredFirst(){
        $this->model = $this->model->whereHas('services',function($q){
            $q->where('active',1);
        });
        return $this;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function newQuery(){
        $this->model = $this->model->newQuery();
        return $this;
    }


    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function noFeatured(){
        $this->model = $this->model->has('services','=',0);
        return $this;
    }

    /**
     * get doctors in city.
     *
     * @param  Array $array
     *
     * @return array
     */
    public function joinService(){
        $this->model = $this->model->with('services')->leftJoin('service_users','users.id','=','service_users.user_id');
        return $this;
    }

    /**
     * Filter doctor users.
     *
     * @return array
     */
    public function doctorsOnly()
    {
        return $this->model
            ->with('profile','contacts','profile.avatar','credentials')
            ->whereHas('roles', function($q){
                $q->where('role_id' ,'=', 2);
            });
    }


    /**
     * Filter Patient users.
     *
     * @return array
     */
    public function patientsOnly()
    {
        return $this->model
            ->with('profile','contacts','profile.avatar')
            ->whereHas('roles', function($q){
                $q->where('role_id' ,'=', 3);
            });
    }

    /**
     * Get the user by its username.
     *
     * @param  string $username
     *
     * @return mixed
     */


    /**
     * Get doctors paginated.
     *
     * @param  int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginated($perPage = 10)
    {
        return $this->model->paginate($perPage);
    }


    public function getByUsername($username)
    {
        return $this->model->whereUsername($username)->firstOrFail();
    }


    /**
     * Get all unverified doctors with their credentials.
     *
     * @return mixed
     */
    public function unverifiedDoctors()
    {
        return $this->unverifiedDoctorsQuery()->paginate(10);
    }

    /**
     * Prepare unverified doctors data table query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function tableOfUnverifiedDoctors()
    {
        return $this->unverifiedDoctorsQuery()->select([
            'users.id',
            'users.fname',
            'users.lname',
            'users.username',
            'users.email'
        ]);
    }

    /**
     * Prepare unverified doctors query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function unverifiedDoctorsQuery()
    {
        return $this->model->with('credentials')->has('credentials')
            ->whereNull('token')->whereVerified(false);
    }


    /**
     * Get all unverified doctors with their credentials.
     *
     * @return mixed
     */
    public function verifiedDoctors()
    {
        return $this->verifiedDoctorsQuery()->paginate(20);
    }

    /**
     * Prepare unverified doctors query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function verifiedDoctorsQuery()
    {
        return $this->model->with('credentials')->has('credentials')
            ->whereNull('token')->whereVerified(true);
    }


    /**
     * Prepare unverified doctors data table query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function tableOfVerifiedDoctors()
    {
        return $this->verifiedDoctorsQuery()->select([
            'users.id',
            'users.fname',
            'users.lname',
            'users.username',
            'users.email'
        ]);
    }



    /**
     * Sort Doctors by number of Views.
     *
     *
     */
    public function mostViewed()
    {
        return $this->model->getModel()
            ->selectRaw('users.*, count(users.id) as aggregate')
            ->leftJoin('views', function($join)
            {
                $join->on('views.viewable_id', '=', 'users.id');
                $join->where('views.viewable_type', '=', get_class($this->model) );
            })
            ->groupBy('users.id')
            ->orderBy('aggregate', 'DESC');
    }

}