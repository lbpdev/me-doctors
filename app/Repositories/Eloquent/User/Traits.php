<?php
namespace MEDoctors\Repositories\Eloquent\User;

trait Traits {

    /**
     * rate a doctor.
     *
     * @param  User $user
     * @param  Array $ratings
     * @param  int $rater_id
     *
     * @return mixed
     */
    public function getCurrentUserRating($doctor)
    {
        $ratings = [];
        if($this->user){

            $ratings['friendliness']     = $doctor->friendliness()->where('rater_id', $this->user->id)->first() ? true : false;
            $ratings['helpfulness']      = $doctor->helpfulness()->where('rater_id', $this->user->id)->first() ? true : false;
            $ratings['bedsideManner']    = $doctor->bedsideManner()->where('rater_id', $this->user->id)->first() ? true : false;
            $ratings['medicalKnowledge'] = $doctor->medicalKnowledge()->where('rater_id', $this->user->id)->first() ? true : false;
            $ratings['professionalism']  = $doctor->professionalism()->where('rater_id', $this->user->id)->first() ? true : false;

            $waitingTime = $doctor->waitingTime()->where('rater_id', $this->user->id)->first();
            $ratings['waitingTime']      = $waitingTime ? $waitingTime->value : false;
        }

        return $ratings;
    }


    /**
     *
     * Get with doctor Traits
     */
    public function withTraits(){
        $this->model = $this->model->with(
            'friendliness',
            'bedsideManner',
            'helpfulness',
            'medicalKnowledge',
            'professionalism',
            'waitingTime',
            'specialties',
            'services'
        );

        return $this;
    }

    /*
     * Get ratings in an array
     */
    public function getTraitsArray($doctors){
        $ratings = [];

        foreach($doctors as $index=>$doctor){
            $ratings[$index]['friendliness'] = $doctor->friendlinessRating;
            $ratings[$index]['professionalism'] = $doctor->professionalismRating;
            $ratings[$index]['bedside_manner'] = $doctor->bedsideMannerRating;
            $ratings[$index]['medical_knowledge'] = $doctor->medicalKnowledgeRating;
            $ratings[$index]['helpfulness'] = $doctor->HelpfulnessRating;
            $ratings[$index]['waiting_time'] = $doctor->waitingTimeRating;
        }

        return $ratings;
    }

    /**
     *
     * Get with doctor Traits
     */
    public function withProfile(){
        $this->model = $this->model->with(
            'profile',
            'contacts',
            'profile.avatar',
            'specialties'
        );

        return $this;
    }


    /**
     *
     * Get with doctor hospitals
     */
    public function withHospitals(){
        $this->model = $this->model->with('hospitals.location');
        return $this;
    }



    /**
     *
     * Get with doctor hospitals
     */
    public function withServices(){
        $this->model = $this->model->with('services');
        return $this;
    }

}