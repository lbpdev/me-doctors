<?php namespace MEDoctors\Repositories\Emails;

use MEDoctors\Repositories\AbstractDatabase;

class DbEmailDomainRepository extends AbstractDatabase {

     /**
     * Determine if the email is in the blacklist.
     *
     * @param  string $email
     *
     * @return boolean
     */
    public function contains($email)
    {
        $query = $this->newQuery()->where(
                    $this->getColumnToCheck(),
                    $this->extractEmailDomain($email)
                );

        return $query->count() > 0;
    }

     /**
     * Add email domain to the list.
     *
     * @param array|string $domain
     *
     * @return  void
     */
    public function addDomain($domain)
    {
        $this->newQuery()->insert($this->prepareInsertData((array) $domain));
    }

    /**
     * Prepare the insert data.
     *
     * @param  array  $domains
     *
     * @return array
     */
    private function prepareInsertData(array $domains)
    {
        $insertData = [];

        foreach ($domains as $domain)
        {
           $insertData[][$this->getColumnToCheck()] = $domain;
        }

        return $insertData;
    }

    /**
     * Extract the domain from the email string.
     *
     * @param  string $email
     *
     * @return string
     */
    private function extractEmailDomain($email)
    {
        return substr(strrchr($email, "@"), 1);
    }

}