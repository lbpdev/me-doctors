<?php namespace MEDoctors\Repositories\Emails;

use MEDoctors\Repositories\Contracts\Emails\EmailDomainsBlacklist;

class DbEmailDomainsBlacklist extends DbEmailDomainRepository implements EmailDomainsBlacklist {
    
    /**
     * Table name from the database.
     * 
     * @var string
     */
    protected $table = 'email_domains_blacklist';

    /**
     * The column of the table to check.
     *
     * @var string
     */
    protected $columnToCheck = 'name';

}