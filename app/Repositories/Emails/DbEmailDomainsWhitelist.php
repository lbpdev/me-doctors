<?php namespace MEDoctors\Repositories\Emails;

use MEDoctors\Repositories\Contracts\Emails\EmailDomainsWhitelist;

class DbEmailDomainsWhitelist extends DbEmailDomainRepository implements EmailDomainsWhitelist {
    
    /**
     * Table name from the database.
     * 
     * @var string
     */
    protected $table = 'email_domains_whitelist';

    /**
     * The column of the table to check.
     *
     * @var string
     */
    protected $columnToCheck = 'name';

    /**
     * Extract domain from a website and add it to the whitelist.
     *
     * @param  array|string $website
     *
     * @return void
     */
    public function importWebsite($website)
    {
        $uniqueDomains = $this->extractUniqueDomains(
            $this->prepareDomains((array) $website)
        );

        if ($uniqueDomains)
        {
            $this->addDomain($uniqueDomains);
        }
    }

    /**
     * Extract unique domains based on the existing domains in the database
     * and the provided domains.
     *
     * @param  array $domains
     *
     * @return array
     */
    protected function extractUniqueDomains(array $domains)
    {
        $existingDomains = $this->getExistingDomains($domains);

        return array_where($domains, function($key, $value) use ($existingDomains)
        {
            return ! in_array($value, $existingDomains);
        });
    }

    /**
     * Get the existing domains from the database.
     *
     * @param  array  $domains
     *
     * @return array
     */
    protected function getExistingDomains(array $domains = [])
    {
        $column = $this->getColumnToCheck();

        return $this->newQuery()
                    ->selectRaw("LOWER({$column}) as {$column}")
                    ->whereIn($column, $domains)
                    ->lists($column);
    }

    /**
     * Remove extra url part like "http://" or "www".
     *
     * @param  array  $domains
     *
     * @return array
     */
    protected function prepareDomains(array $domains)
    {
        $domains = array_map(function($domain) {

            $domain = strtolower(preg_replace('/(?:https?:\/\/|www\.)/', '', $domain));

            return preg_replace('/\/.*/', '', $domain);
        }, $domains);

        return array_unique($domains);
    }

}