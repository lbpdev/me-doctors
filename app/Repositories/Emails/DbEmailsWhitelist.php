<?php namespace MEDoctors\Repositories\Emails;

use MEDoctors\Repositories\AbstractDatabase;
use MEDoctors\Repositories\Contracts\Emails\EmailsWhitelist;

class DbEmailsWhitelist extends AbstractDatabase implements EmailsWhitelist {
    
    /**
     * Table name from the database.
     * 
     * @var string
     */
    protected $table = 'doctor_emails_whitelist';

    /**
     * The column of the table to check.
     *
     * @var string
     */
    protected $columnToCheck = 'email';

    /**
     * Determine if the email is in the white list.
     *
     * @param  string $email
     *
     * @return boolean
     */
    public function contains($email)
    {
        $query = $this->db->table($this->getTable())->where($this->getColumnToCheck(), $email);

        return $query->first() ? true : false;
    }

    /**
     * Add email to the white list.
     *
     * @param string $email
     *
     * @return  void
     */
    public function addEmail($email)
    {
        $this->newQuery()->insert([ 'email' => $email ]);
    }

}