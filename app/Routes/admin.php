<?php

/**
* ADMIN ROUTES
*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['admin', 'auth']], function()
{
Route::get('/', ['as'=>'backend_home' , 'uses' => 'AdminController@home']);

/**
* Users
*/
Route::patch('users/{username}/verify', [
'as' => 'admin.users.verify',
'uses' => 'UsersController@verify'
]);

Route::get('users/unverified', [
'as' => 'admin.users.unverified',
'uses' => 'UsersController@unverified'
]);

Route::get('users/doctors/create', [
'as' => 'admin.users.doctors.create',
'uses' => 'DoctorsController@create'
]);

Route::get('users/doctors', [
'as' => 'admin.users.doctors',
'uses' => 'DoctorsController@index'
]);

Route::get('users/doctors/{username}', [
'as' => 'admin.users.doctors.show',
'uses' => 'DoctorsController@show'
]);

Route::post('users/doctors/register', [
'as' => 'admin.users.doctors.store',
'uses' => 'DoctorsController@store'
]);

Route::get('users/doctors/{username}/edit', [
'as' => 'admin.users.doctors.edit',
'uses' => 'DoctorsController@edit'
]);

Route::post('users/doctors/update', [
'as' => 'admin.users.doctors.update',
'uses' => 'DoctorsController@update'
]);

Route::get('users/doctors/{username}/delete', [
'as' => 'admin.users.doctors.destroy',
'uses' => 'DoctorsController@destroy'
]);

Route::get('users/doctors/{user_id}/availService/{service_slug}', [
'as' => 'admin.users.doctors.availService',
'uses' => 'DoctorsController@availService'
]);

Route::get('users/doctors/{user_id}/toggleService', [
'as' => 'admin.users.doctors.toggleService',
'uses' => 'DoctorsController@toggleService'
]);


// PATIENTS

Route::get('users/patients', [
    'as' => 'admin.users.patients',
    'uses' => 'PatientsController@index'
]);

Route::get('users/patients/{username}', [
    'as' => 'admin.users.patients.show',
    'uses' => 'PatientsController@show'
]);

Route::get('users/patients/{username}/edit', [
    'as' => 'admin.users.patients.edit',
    'uses' => 'PatientsController@edit'
]);

Route::post('users/patients/update', [
    'as' => 'admin.users.patients.update',
    'uses' => 'PatientsController@update'
]);

Route::get('users/patients/{username}/delete', [
    'as' => 'admin.users.patients.destroy',
    'uses' => 'PatientsController@destroy'
]);


    Route::get('users/unverified/{username}', [
'as' => 'admin.users.unverified.show',
'uses' => 'UsersController@showUnverified'
]);

Route::resource('users', 'UsersController');

/**
* Emails
*/
Route::group(['prefix' => 'emails', 'namespace' => 'Email'], function()
{
Route::get('whitelist', [
'as' => 'emails.whitelist',
'uses' => 'EmailsWhitelistController@index'
]);

Route::post('whitelist', [
'as' => 'emails.whitelist.store',
'uses' => 'EmailsWhitelistController@store'
]);

Route::delete('whitelist/{id}', [
'as' => 'emails.whitelist.delete',
'uses' => 'EmailsWhitelistController@delete'
]);

Route::get('domains/whitelist', [
'as' => 'emails.domains.whitelist',
'uses' => 'DomainsWhitelistController@index'
]);

Route::post('domains/whitelist', [
'as' => 'emails.domains.whitelist.store',
'uses' => 'DomainsWhitelistController@store'
]);

Route::delete('domains/whitelist/{id}', [
'as' => 'emails.domains.whitelist.delete',
'uses' => 'DomainsWhitelistController@delete'
]);

Route::get('domains/blacklist', [
'as' => 'emails.domains.blacklist',
'uses' => 'DomainsBlacklistController@index'
]);

Route::post('domains/blacklist', [
'as' => 'emails.domains.blacklist.store',
'uses' => 'DomainsBlacklistController@store'
]);

Route::delete('domains/blacklist/{id}', [
'as' => 'emails.domains.blacklist.delete',
'uses' => 'DomainsBlacklistController@delete'
]);

});

// Referrals

Route::get('referrals', ['as' => 'admin.referrals.index', 'uses' => 'ReferralController@index']);
Route::get('referrals/{id}/delete', ['as' => 'admin.referrals.delete', 'uses' => 'ReferralController@destroy']);

// ADS

Route::get('ads', ['as' => 'admin.ads.index', 'uses' => 'AdsController@index']);
Route::get('ads/pending', ['as' => 'admin.ads.pending', 'uses' => 'AdsController@pending']);
Route::get('ads/pending/{item_id}/approve', ['as' => 'admin.ads.approve' , 'uses' => 'AdsController@approvePending' ]);
Route::get('ads/create', ['as' => 'admin.ads.create', 'uses' => 'AdsController@create']);
Route::post('ads/create', 'AdsController@store');
Route::get('ads/{id}', ['as' => 'admin.ads.show', 'uses' => 'AdsController@show']);
Route::get('ads/{id}/edit', ['as' => 'admin.ads.edit', 'uses' => 'AdsController@edit']);
Route::post('ads/{id}/edit', 'AdsController@update');
Route::get('ads/{id}/destroy', ['as' => 'admin.ads.destroy', 'uses' => 'AdsController@destroy']);
Route::get('ads/{id}/toggle', ['as' => 'admin.ads.toggle', 'uses' => 'AdsController@toggle']);

// ARTICLES

Route::get('articles/channel/{channel_id}', ['as' => 'adm_articles', 'uses' => 'ArticlesController@index']);
Route::get('articles/create', ['as' => 'adm_articles_create', 'uses' => 'ArticlesController@create']);
Route::post('articles/create', 'ArticlesController@store');
Route::get('articles/{id}', ['as' => 'adm_articles_single', 'uses' => 'ArticlesController@show']);
Route::get('articles/{id}/edit', ['as' => 'adm_articles_edit', 'uses' => 'ArticlesController@edit']);
Route::post('articles/{id}/edit', 'ArticlesController@update');
Route::get('articles/{id}/destroy', ['as' => 'adm_articles_destroy', 'uses' => 'ArticlesController@destroy']);

// NEWS FEEDS
Route::get('news/channel/{channel_id}', ['as' => 'admin.news.channel', 'uses' => 'NewsFeedsController@channel']);
Route::get('news/{news_id}/destroy', ['as' => 'admin.news.delete', 'uses' => 'NewsFeedsController@destroy']);
Route::resource('news', 'NewsFeedsController');

// VIDEOS
Route::get('videos/{id}/delete', ['as' => 'admin.videos.delete' , 'uses' =>  'VideosController@destroy']);
Route::post('videos/{id}/edit', 'VideosController@update');
Route::resource('videos', 'VideosController');

// PANEL DISCUSSIONS

Route::get('panel', ['as' => 'adm_panel' , 'uses' => 'DiscussionsController@index']);
Route::get('panel/create', ['as' => 'adm_discussions_create_panel' , 'uses' => 'DiscussionsController@create']);
Route::post('panel/create', 'DiscussionsController@store');
Route::get('panel/{id}', ['as' => 'adm_single_panel' ,  'uses' => 'DiscussionsController@show']);
Route::post('panel/{id}', 'DiscussionsCommentsController@store');
Route::get('panel/{id}/edit', ['as' => 'adm_edit_panel' , 'uses' => 'DiscussionsController@edit']);
Route::post('panel/{id}/edit', 'DiscussionsController@update');


// DIAGNOSIS

Route::get('diagnosis', ['as' => 'adm_diag' , 'uses' => 'DiagnosisController@index']);
Route::get('diagnosis/create', ['as' => 'adm_discussions_create_diag' , 'uses' => 'DiagnosisController@create']);
Route::post('diagnosis/create', 'DiagnosisController@store');
Route::get('diagnosis/publish/{id}', ['as' => 'adm_publish_discussion' , 'uses' => 'DiagnosisController@publish']);
Route::get('diagnosis/unpublish/{id}', ['as' => 'adm_unpublish_discussion' , 'uses' => 'DiagnosisController@unpublish']);
Route::get('diagnosis/{id}', ['as' => 'adm_diag_single' , 'uses' => 'DiagnosisController@show']);
Route::post('diagnosis/{id}', 'DiscussionsCommentsController@store');
Route::get('diagnosis/{id}/edit', ['as' => 'adm_edit_diag' , 'uses' => 'DiagnosisController@edit']);
Route::post('diagnosis/{id}/edit', 'DiagnosisController@update');
Route::get('discussion/{id}/destroy', ['as' => 'adm_destroy_disc' , 'uses' => 'DiscussionsController@destroy']);

// PATIENT DISCUSSIONS

Route::get('patient_discussions', ['as' => 'patient_discussions' , 'uses' => 'PatientDiscussionsController@index']);
Route::get('patient_discussions/create', ['as' => 'patient_discussions_create_diag' , 'uses' => 'PatientDiscussionsController@create']);
Route::post('patient_discussions/create', 'PatientDiscussionsController@store');
Route::get('patient_discussions/publish/{id}', ['as' => 'patient_publish_discussion' , 'uses' => 'PatientDiscussionsController@publish']);
Route::get('patient_discussions/unpublish/{id}', ['as' => 'patient_unpublish_discussion' , 'uses' => 'PatientDiscussionsController@unpublish']);
Route::get('patient_discussions/{id}', ['as' => 'patient_diag_single' , 'uses' => 'PatientDiscussionsController@show']);
Route::post('patient_discussions/{id}', 'DiscussionsCommentsController@store');
Route::get('patient_discussions/{id}/edit', ['as' => 'patient_edit_diag' , 'uses' => 'PatientDiscussionsController@edit']);
Route::post('patient_discussions/{id}/edit', 'PatientDiscussionsController@update');
Route::get('patient_discussions/{id}/destroy', ['as' => 'patient_destroy_disc' , 'uses' => 'PatientDiscussionsController@destroy']);

// SURVEYS

Route::get('surveys/channel/{id}', ['as' => 'adm_surveys' , 'uses' => 'SurveysController@index']);
Route::get('surveys/create', ['as' => 'adm_surveys_create' , 'uses' => 'SurveysController@create']);
Route::post('surveys/create', 'SurveysController@store');
Route::get('surveys/{id}', ['as' => 'adm_surveys_single' , 'uses' => 'SurveysController@show']);
Route::get('surveys/{id}/preview', ['as' => 'adm_surveys_preview' , 'uses' => 'SurveysController@preview']);
Route::post('surveys/{id}/preview', ['as' => 'adm_surveys_preview' , 'uses' => 'SurveysController@vote']);
Route::get('surveys/{id}/edit', ['as' => 'adm_surveys_edit' , 'uses' => 'SurveysController@edit']);
Route::post('surveys/{id}/edit', 'SurveysController@update');
Route::get('surveys/{id}/destroy', ['as' => 'adm_surveys_destroy' , 'uses' => 'SurveysController@destroy']);


// POLLS

Route::get('polls/all', ['as' => 'adm_polls' , 'uses' => 'PollsController@index']);
Route::get('polls/create', ['as' => 'adm_polls_create' , 'uses' => 'PollsController@create']);
Route::post('polls/create', 'PollsController@store');
Route::get('polls/{id}', ['as' => 'adm_polls_single' , 'uses' => 'PollsController@show']);
Route::post('polls/{id}/vote', ['as' => 'adm_polls_vote' , 'uses' => 'PollsController@vote']);
Route::get('polls/{id}/edit', ['as' => 'adm_polls_edit' , 'uses' => 'PollsController@edit']);
Route::post('polls/{id}/edit', 'PollsController@update');
Route::get('polls/{id}/destroy', ['as' => 'adm_polls_destroy' , 'uses' => 'PollsController@destroy']);

// COMMENTS

Route::get('comments', ['as' => 'adm_comments' , 'uses' => 'DiscussionsCommentsController@index']);
Route::get('comment/{id}/edit', ['as' => 'adm_edit_comment' , 'uses' => 'DiscussionsCommentsController@edit']);
Route::get('comment/publish/{id}', ['as' => 'adm_publish_comment' , 'uses' => 'DiscussionsCommentsController@publish']);
Route::get('comment/unpublish/{id}', ['as' => 'adm_unpublish_comment' , 'uses' => 'DiscussionsCommentsController@unpublish']);
Route::post('comment/{id}/edit', 'DiscussionsCommentsController@update');
Route::get('comment/{id}/destroy', ['as' => 'adm_destroy_comment' , 'uses' => 'DiscussionsCommentsController@destroy']);

// Hospitals
Route::get('hospitals/unverified',['as' => 'admin.hospitals.unverified' , 'uses' => 'HospitalsController@unverified']);
Route::get('hospitals/claims',['as' => 'admin.hospitals.claims' , 'uses' => 'HospitalsController@claims']);
Route::get('hospitals/claims/{claim_id}/grant',['as' => 'admin.hospitals.claims.grant' , 'uses' => 'HospitalsController@grantClaims']);
Route::get('hospitals/claims/{claim_id}/deny',['as' => 'admin.hospitals.claims.deny' , 'uses' => 'HospitalsController@denyClaims']);
Route::get('hospitals/claims/{claim_id}/resend',['as' => 'admin.hospitals.claims.resend' , 'uses' => 'HospitalsController@resendVerification']);
Route::get('hospitals/{id}/verify',['as' => 'admin.hospitals.verify' , 'uses' => 'HospitalsController@verify']);
Route::get('hospitals/{id}/delete',['as' => 'admin.hospitals.delete' , 'uses' => 'HospitalsController@destroy']);
Route::get('hospitals/{id}/deleteUnverified',['as' => 'admin.hospitals.deleteUnverified' , 'uses' => 'HospitalsController@destroyUnverified']);
Route::resource('hospitals', 'HospitalsController');
Route::get('hospitals/{id}/toggleService', [
    'as' => 'admin.hospitals.toggleService',
    'uses' => 'HospitalsController@toggleService'
]);

Route::get('hospitals/{hospital_id}/availService/{service_slug}', [
    'as' => 'admin.hospitals.availService',
    'uses' => 'HospitalsController@availService'
]);

// Events

Route::get('events/all', ['as' => 'adm_events' , 'uses' => 'EventsController@index']);
Route::get('events/create', ['as' => 'adm_event_create' , 'uses' => 'EventsController@create']);
Route::post('events/create', 'EventsController@store');
Route::get('events/{id}', ['as' => 'adm_event_single' , 'uses' => 'EventsController@show']);
Route::get('events/{id}/edit', ['as' => 'adm_event_edit' , 'uses' => 'EventsController@edit']);
Route::post('events/{id}/edit', 'EventsController@update');
Route::get('events/{id}/destroy', ['as' => 'adm_event_destroy' , 'uses' => 'EventsController@destroy']);

// Files
Route::get('files/hospitals', ['as' => 'admin.files.hospitals' , 'uses' => 'UploadsController@hospitalFiles']);
Route::get('files/articles', ['as' => 'admin.files.articles' , 'uses' => 'UploadsController@articleFiles']);
Route::get('files/events', ['as' => 'admin.files.events' , 'uses' => 'UploadsController@eventFiles']);

Route::resource('files', 'UploadsController');

// Settings

Route::get('settings', ['as' => 'adm_settings' , 'uses' => 'OptionsController@index']);
Route::post('settings/update', ['as' => 'adm_settings_update' , 'uses' => 'OptionsController@update']);

// User Logs

Route::get('user-logs', ['as' => 'adm_user_logs' , 'uses' => 'UserLogsController@index']);

});

?>