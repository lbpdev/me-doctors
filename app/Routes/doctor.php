<?php

/**
 * Purchase ad or service.
 */
Route::get('doctor/advertise', 'ServiceController@advertise');
Route::post('doctor/advertise/purchase', ['as' => 'doctor.advertise.queue' , 'uses' => 'ServiceController@storeQueue' ]);
Route::post('doctor/advertise/update', ['as' => 'doctor.advertise.update' , 'uses' => 'ServiceController@updateQueue' ]);


/**
 * DOCTOR ROUTES
 */
Route::group(['prefix' => 'doctor', 'namespace' => 'Doctor' ,'middleware' => 'addoc' ], function()
{
    Route::get('search', ['as'=>'search' , 'uses'=>'SearchController@search']);

    Route::get('feed', 'RssController@index');

    /**
     * Authentication
     */
    Route::get('login', [
        'as' => 'doctor.login',
        'uses' => 'AuthController@index'
    ]);

    Route::post('login', [
        'as' => 'doctor.login',
        'uses' => 'AuthController@login'
    ]);


    Route::get('logout', [
        'as' => 'doctor.logout',
        'uses' => 'AuthController@logout'
    ]);

    Route::post('reset', [
        'as' => 'doctor.password.reset',
        'uses' => 'AuthController@reset'
    ]);

    Route::get('register', [
        'as' => 'doctor.register',
        'uses' => 'AuthController@index'
    ]);

    Route::post('register', [
        'as' => 'doctor.register',
        'uses' => 'RegistrationController@register'
    ]);

    Route::get('register/confirm/{token}', [
        'as' => 'doctor.register.confirm',
        'uses' => 'RegistrationController@confirmEmail'
    ]);

    Route::get('register/with-credentials', [
        'as' => 'doctor.register.with_credentials',
        'uses' => 'RegistrationController@withCredentials'
    ]);

    /**
     * Home
     */
    Route::get('/', [
        'as' => 'doctor.home',
        'uses' => 'PagesController@index'
    ]);

    /** Profile */
    Route::get('{username}/profile', [
        'as' => 'doctor.profile',
        'uses' => 'UsersController@show'
    ]);

    /** Profile */
    Route::get('become-kol', [
        'as' => 'doctor.kolize',
        'uses' => 'UsersController@kolize'
    ]);

    /** Education */
    Route::post('{username}/education', [
        'as' => 'doctor.education.update',
        'uses' => 'UserEducationController@update'
    ]);
    /** Education non-AJAX*/
    Route::put('{username}/educationIE', [
        'as' => 'doctor.education.updateIE',
        'uses' => 'UserEducationController@updateIE'
    ]);

    /** Achievements */
    Route::post('{username}/achievements', [
        'as' => 'doctor.achievements.update',
        'uses' => 'UserAchievementsController@update'
    ]);
    /** Achievements non-Ajax */
    Route::put('{username}/achievementsIE', [
        'as' => 'doctor.achievements.updateIE',
        'uses' => 'UserAchievementsController@updateIE'
    ]);

    /** Publications */
    Route::post('{username}/publications', [
        'as' => 'doctor.publications.update',
        'uses' => 'UserPublicationsController@update'
    ]);
    /** Publications non-Ajax  */
    Route::put('{username}/publicationsIE', [
        'as' => 'doctor.publications.updateIE',
        'uses' => 'UserPublicationsController@updateIE'
    ]);

    /** Certifications */
    Route::post('{username}/certifications', [
        'as' => 'doctor.certifications.update',
        'uses' => 'UserCertificationsController@update'
    ]);
    /** Certifications non-Ajax  */
    Route::put('{username}/certificationsIE', [
        'as' => 'doctor.certifications.updateIE',
        'uses' => 'UserCertificationsController@updateIE'
    ]);

    /** Languages */
    Route::post('{username}/languages', [
        'as' => 'doctor.languages.update',
        'uses' => 'UserLanguagesController@update'
    ]);

    /** Languages non-Ajax  */
    Route::put('{username}/languagesIE', [
        'as' => 'doctor.languages.updateIE',
        'uses' => 'UserLanguagesController@updateIE'
    ]);

    /** Work */
    Route::post('{username}/work', [
        'as' => 'doctor.work.update',
        'uses' => 'UserWorkController@update'
    ]);

    /** Work non-Ajax  */
    Route::put('{username}/workIE', [
        'as' => 'doctor.work.updateIE',
        'uses' => 'UserWorkController@updateIE'
    ]);

    Route::get('{username}/profile/edit_profile', [
        'as' => 'edit_personal_profile',
        'uses' => 'UsersController@edit_profile'
    ]);

    Route::post(
        '{username}/profile/edit_profile',
        'UsersController@update_profile'
    );

    Route::post('purchase-premium', ['as' => 'doctor.purchase-premium' , 'uses' => 'UsersController@purchasePremium']);

    Route::post('articles/category', ['as' => 'articles_filter' , 'uses' => 'ArticlesController@filter']);
    Route::get('articles/category/{slug}', ['as' => 'articles_show_filtered' , 'uses' => 'ArticlesController@show_filtered']);
    Route::resource('articles', 'ArticlesController', [
        'only' => ['index', 'show']
    ]);


// Panel Discussions

//    Route::get('discussions/popular', ['as' => 'discussions_popular' , 'uses' => 'DiscussionsController@show_popular']);
    Route::post('discussions/category', ['as' => 'discussions_filter' , 'uses' => 'DiscussionsController@filter']);
    Route::get('discussions/category/{slug}/latest', ['as' => 'discussions_show_filtered_latest' , 'uses' => 'DiscussionsController@show_filtered_latest']);
    Route::get('discussions/category/{slug}/popular', ['as' => 'discussions_show_filtered_popular' , 'uses' => 'DiscussionsController@show_filtered_popular']);
    Route::get('discussions/popular', ['as' => 'discussions_popular' , 'uses' => 'DiscussionsController@show_popular']);
    Route::post('discussions/{id}/next_comments', ['as' => 'doctor.discussions.nextComments' , 'uses' =>  'DiscussionsCommentsController@nextComments']);
    Route::resource('discussions', 'DiscussionsController');
    Route::resource('discussions.comments', 'DiscussionsCommentsController', [
        'only' => ['store']
    ]);

// Diagnosis

    Route::post('diagnosis/category', ['as' => 'diagnosis_filter' , 'uses' => 'DiagnosisController@filter']);
    Route::get('diagnosis/category/{slug}/latest', ['as' => 'show_filtered_latest' , 'uses' => 'DiagnosisController@show_filtered_latest']);
    Route::get('diagnosis/category/{slug}/popular', ['as' => 'show_filtered_popular' , 'uses' => 'DiagnosisController@show_filtered_popular']);
    Route::get('diagnosis/popular', ['as' => 'diagnosis_popular' , 'uses' => 'DiagnosisController@show_popular']);
    Route::get('diagnosis/create',['as' => 'diagnosis_add' , 'uses' => 'DiagnosisController@create']);
    Route::get('diagnosis/{slug}/destroy',['as' => 'diagnosis_destroy' , 'uses' => 'DiagnosisController@destroy']);
    Route::resource('diagnosis', 'DiagnosisController');


    Route::get('doctors/filter',['as' => 'doctor.doctors.filter' , 'uses' => 'DoctorsController@filter']);
    Route::post('doctors/next',['as' => 'doctor.doctors.next' , 'uses' => 'DoctorsController@getNext']);
    Route::resource('doctors', 'DoctorsController');

    Route::get('hospitals/filter', ['as' => 'doctor.hospitals.filter' , 'uses' => 'HospitalsController@filter']);
    Route::post('hospitals/update', ['as' => 'doctor.hospitals.update' , 'uses' => 'HospitalsController@update']);
    Route::post('hospitals/checkProvider',['as' => 'doctor.hospitals.checkProvider' , 'uses' => 'HospitalsController@checkProvider']);
    Route::post('hospitals/claim',['as' => 'doctor.hospitals.claim' , 'uses' => 'HospitalsController@claim']);
    Route::get('hospitals/claims/{claim_id}/verify',['as' => 'doctor.hospitals.claims.verify' , 'uses' => 'HospitalsController@verify']);
    Route::post('hospitals/next',['as' => 'doctor.hospitals.next' , 'uses' => 'HospitalsController@getNext']);
    Route::resource('hospitals', 'HospitalsController');

// Surveys

    Route::get('surveys/filter',['as' => 'doctor.surveys.filter' , 'uses' => 'SurveysController@filter']);
    Route::resource('surveys', 'SurveysController');

// VIDEOS
    Route::resource('videos', 'VideosController');

// EVENTS
    Route::get('events', ['as' => 'doctor.events.index', 'uses' => 'EventsController@upcoming']);
    Route::get('events/past', ['as' => 'doctor.events.past', 'uses' => 'EventsController@past']);
    Route::get('events/{slug}', ['as' => 'show_event', 'uses' => 'EventsController@show']);

// NEWS
    Route::get('news', ['as' => 'doctor.news.index', 'uses' => 'NewsFeedsController@upcoming']);
    Route::get('news/past', ['as' => 'doctor.news.past', 'uses' => 'NewsFeedsController@past']);
    Route::get('news/{slug}', ['as' => 'doctor.news.show', 'uses' => 'NewsFeedsController@show']);

// MESSAGES
    Route::group(['prefix' => 'messages'], function() {

        Route::get('search', ['as' => 'messages_search', 'uses' => 'MessagesController@search']);

        Route::get('/', ['as' => 'messages_show', 'uses' => 'ConversationsController@show']);
        Route::post('get-users-from-therapy', ['as' => 'get-users-from-therapy', 'uses' => 'ConversationsController@getUsersFromTherapy']);
        Route::get('inbox', ['as' => 'messages_show', 'uses' => 'ConversationsController@show']);
        Route::get('create', ['as' => 'messages_create', 'uses' => 'ConversationsController@create']);
        Route::post('create', 'ConversationsController@storeConversation');
        Route::get('archive', ['as' => 'messages_archive', 'uses' => 'ConversationsController@archive']);
        Route::get('trash', ['as' => 'messages_trashed', 'uses' => 'ConversationsController@trashed']);
        Route::post('trashMulti', ['as' => 'messages_trash_multi', 'uses' => 'ConversationsController@trashMultiple']);
        Route::get('{id}/trash', ['as' => 'messages_trash', 'uses' => 'ConversationsController@trash']);
        Route::get('restore/{id}', ['as' => 'messages_restore', 'uses' => 'ConversationsController@restore']);
        Route::post('restoreMulti', ['as' => 'messages_restore_multi', 'uses' => 'ConversationsController@restoreMultiple']);

        Route::get('sent', ['as' => 'messages_sent', 'uses' => 'MessagesController@index']);
        Route::get('sent/{id}', ['as' => 'messages_sent_single', 'uses' => 'MessagesController@single']);
        Route::get('sent/{id}/trash', ['as' => 'messages_sent_trash', 'uses' => 'MessagesController@trash']);
        Route::post('sent/trash', ['as' => 'messages_sent_trash_multi', 'uses' => 'MessagesController@trashMultiple']);

        Route::get('inbox/{id}', ['as' => 'messages_single', 'uses' => 'ConversationsController@single']);
        Route::post('inbox/{id}', 'ConversationsController@store');
        Route::get('inbox/{id}/reply', ['as' => 'messages_reply', 'uses' => 'ConversationsController@reply']);
        Route::post('inbox/{id}/reply', 'MessagesController@storeMessage');

    });

// POLLS

    Route::post('poll/{id}/vote', 'PollsController@vote');
    Route::get('create_poll', 'PollsController@create');
    Route::post('create_poll', 'PollsController@store');
    Route::post('poll_vote', ['as' => 'poll_vote' , 'uses' => 'PollsController@vote']);



    /**
     * Pages Routes.
     */
    Route::get('/{page}', 'PagesController@show');

});

/**
 * Downloads
 */
Route::get('download/{id}', 'FilesController@download');

Route::get('files/{id}/download', [
    'as' => 'files.download',
    'uses' => 'FilesController@download'
]);

?>