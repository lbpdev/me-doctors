<?php

/**
 * Patient Home
 */
Route::get('patient/', [
    'as' => 'patient.home',
    'uses' => 'PagesController@index'
]);


Route::get('patient/resend-verification', [
    'as' => 'patient.resend-verify.create',
    'uses' => 'Patient\AuthController@getResend'
]);

Route::post('resend-verification', [
    'as' => 'patient.resend-verify.execute',
    'uses' => 'Patient\AuthController@postResend'
]);


/**
 * patient ROUTES
 */
Route::group(['prefix' => 'patient', 'namespace' => 'Patient'], function()
{
    Route::get('search', ['as'=>'patient.search' , 'uses'=>'SearchController@search']);

    Route::get('feed', 'RssController@index');

    /**
     * Authentication
     */
    Route::get('login', [
        'as' => 'patient.login',
        'uses' => 'AuthController@index'
    ]);

    Route::post('login', [
        'as' => 'patient.login',
        'uses' => 'AuthController@login'
    ]);

    Route::get('logout', [
        'as' => 'patient.logout',
        'uses' => 'AuthController@logout'
    ]);

    Route::get('register', [
        'as' => 'patient.register',
        'uses' => 'AuthController@index'
    ]);

    Route::post('register', [
        'as' => 'patient.register',
        'uses' => 'RegistrationController@register'
    ]);

    Route::get('register/confirm/{token}', [
        'as' => 'patient.register.confirm',
        'uses' => 'RegistrationController@confirmEmail'
    ]);

    Route::get('register/confirm', function(){
        return 'Already Confirmed';
    });

    /**
     * Home
     */
    Route::get('/', [
        'as' => 'patient.home',
        'uses' => 'PagesController@index'
    ]);

    /** Profile */
    Route::get('{username}/profile', [
        'as' => 'patient.profile',
        'uses' => 'UsersController@show'
    ]);


    Route::get('{username}/profile/edit_profile', [
        'as' => 'patient.edit_personal_profile',
        'uses' => 'UsersController@edit_profile'
    ]);

    Route::post(
        '{username}/profile/edit_profile',
        'UsersController@update_profile'
    );

//

    Route::post('articles/category', ['as' => 'patient.articles_filter' , 'uses' => 'ArticlesController@filter']);
    Route::get('articles/category/{slug}', ['as' => 'patient.articles_show_filtered' , 'uses' => 'ArticlesController@show_filtered']);
    Route::resource('articles', 'ArticlesController', [
        'only' => ['index', 'show']
    ]);


// Panel Discussions

//    Route::get('discussions/popular', ['as' => 'discussions_popular' , 'uses' => 'DiscussionsController@show_popular']);
    Route::post('discussions/category', ['as' => 'patient.discussions_filter' , 'uses' => 'DiscussionsController@filter']);
    Route::get('discussions/category/{slug}/latest', ['as' => 'patient.discussions_show_filtered_latest' , 'uses' => 'DiscussionsController@show_filtered_latest']);
    Route::get('discussions/category/{slug}/popular', ['as' => 'patient.discussions_show_filtered_popular' , 'uses' => 'DiscussionsController@show_filtered_popular']);
    Route::get('discussions/popular', ['as' => 'patient.discussions_popular' , 'uses' => 'DiscussionsController@show_popular']);
    Route::post('discussions/{id}/next_comments', ['as' => 'patient.discussions.nextComments' , 'uses' =>  'DiscussionsCommentsController@nextComments']);
    Route::resource('discussions', 'DiscussionsController');
    Route::resource('discussions.comments', 'DiscussionsCommentsController', [
        'only' => ['store']
    ]);

    Route::get('discussions/create', ['as' => 'patient_disc_add' , 'uses' => 'DiscussionsController@create']);


    Route::get('doctors/filter',['as' => 'patient.doctors.filter' , 'uses' => 'DoctorsController@filter']);
    Route::post('doctors/next',['as' => 'patient.doctors.next' , 'uses' => 'DoctorsController@getNext']);
    Route::post('doctors/rate',['as' => 'patient.doctors.rate' , 'uses' => 'DoctorsController@rate']);
    Route::resource('doctors', 'DoctorsController');

    Route::get('hospitals/filter', ['as' => 'patient.hospitals.filter' , 'uses' => 'HospitalsController@filter']);
    Route::post('hospitals/next',['as' => 'patient.hospitals.next' , 'uses' => 'HospitalsController@getNext']);
    Route::post('hospitals/checkProvider',['as' => 'patient.hospitals.checkProvider' , 'uses' => 'HospitalsController@checkProvider']);
    Route::post('hospitals/claim',['as' => 'patient.hospitals.claim' , 'uses' => 'HospitalsController@claim']);
    Route::get('hospitals/claims/{claim_id}/verify',['as' => 'patient.hospitals.claims.verify' , 'uses' => 'HospitalsController@verify']);
    Route::resource('hospitals', 'HospitalsController');

// Surveys

    Route::get('surveys/filter',['as' => 'patient.surveys.filter' , 'uses' => 'SurveysController@filter']);
    Route::resource('surveys', 'SurveysController');

// EVENTS
    Route::get('events/{slug}', ['as' => 'show_event', 'uses' => 'EventsController@show']);

// NEWS
    Route::get('news', ['as' => 'patient.news.index', 'uses' => 'NewsFeedsController@upcoming']);
    Route::get('news/past', ['as' => 'patient.news.past', 'uses' => 'NewsFeedsController@past']);
    Route::get('news/{slug}', ['as' => 'patient.news.show', 'uses' => 'NewsFeedsController@show']);

// MESSAGES
    Route::group(['prefix' => 'messages'], function() {

        Route::get('search', ['as' => 'patient.messages_search', 'uses' => 'MessagesController@search']);

        Route::get('/', ['as' => 'patient.messages_show', 'uses' => 'ConversationsController@show']);
        Route::get('inbox', ['as' => 'patient.messages_show', 'uses' => 'ConversationsController@show']);
        Route::get('create', ['as' => 'patient.messages_create', 'uses' => 'ConversationsController@create']);
        Route::post('create', 'ConversationsController@storeConversation');
        Route::get('archive', ['as' => 'patient.messages_archive', 'uses' => 'ConversationsController@archive']);
        Route::get('trash', ['as' => 'patient.messages_trashed', 'uses' => 'ConversationsController@trashed']);
        Route::post('trashMulti', ['as' => 'patient.messages_trash_multi', 'uses' => 'ConversationsController@trashMultiple']);
        Route::get('{id}/trash', ['as' => 'patient.messages_trash', 'uses' => 'ConversationsController@trash']);
        Route::get('restore/{id}', ['as' => 'patient.messages_restore', 'uses' => 'ConversationsController@restore']);
        Route::post('restoreMulti', ['as' => 'patient.messages_restore_multi', 'uses' => 'ConversationsController@restoreMultiple']);

        Route::get('sent', ['as' => 'patient.messages_sent', 'uses' => 'MessagesController@index']);
        Route::get('sent/{id}', ['as' => 'patient.messages_sent_single', 'uses' => 'MessagesController@single']);
        Route::get('sent/{id}/trash', ['as' => 'patient.messages_sent_trash', 'uses' => 'MessagesController@trash']);
        Route::post('sent/trash', ['as' => 'patient.messages_sent_trash_multi', 'uses' => 'MessagesController@trashMultiple']);

        Route::get('inbox/{id}', ['as' => 'patient.messages_single', 'uses' => 'ConversationsController@single']);
        Route::post('inbox/{id}', 'ConversationsController@store');
        Route::get('inbox/{id}/reply', ['as' => 'patient.messages_reply', 'uses' => 'ConversationsController@reply']);
        Route::post('inbox/{id}/reply', 'MessagesController@storeMessage');

    });

// POLLS

    Route::post('poll_vote', ['as' => 'patient.poll_vote' , 'uses' => 'PollsController@vote']);

    /**
     * Pages Routes.
     */
    Route::get('/{page}', 'PagesController@show');

});

/**
 * Downloads
 */
Route::get('download/{id}', 'FilesController@download');

Route::get('files/{id}/download', [
    'as' => 'files.download',
    'uses' => 'FilesController@download'
]);

?>