<?php namespace MEDoctors\Services;

class CsvReader {

    /**
     * @var mixed
     */
    private $fp; 

    /**
     * Determine if the first row of the file is actually the header.
     *
     * @var boolean
     */
    private $firstRowHeader = true; 

    /**
     * @var string
     */
    private $delimiter = ',';

    /**
     * Length parameter for fgetcsv() function.
     *
     * @var integer
     */
    private $length = 8000;

    /**
     * Destroy class instance.
     */
    public function __destruct()
    {
        if ($this->fp)
        {
            fclose($this->fp);
        }
    }
    
    /**
     * Open the file with the provided path.
     *
     * @param  string $path
     *
     * @return $this
     */
    public function path($path)
    {
       $this->fp = fopen($path, 'r');

       return $this;
    }

    /**
     * Finally, get the data from the csv file.
     *
     * @return array|null
     */
    public function get()
    {
        if ( ! ($this->fp))
        {
            return null;
        }

        return $this->buildRows();
    }

    /**
     * Build the row data.
     *
     * @return array
     */
    private function buildRows()
    {
        $data = [];

        $header = null;

        while (($row = fgetcsv($this->fp, $this->length, $this->delimiter)) !== FALSE)
        {
            $row = $this->cleanRow($row);

            if( ! $header && $this->firstRowHeader) 
            {
                $header = $row;
            } 
            else 
            {
                $data[] = array_combine($header, $row);
            }
        }

        return $data;
    }

    /**
     * Clean the columns of the row.
     *
     * @param  array $row
     *
     * @return array
     */
    private function cleanRow($row)
    {
        foreach ($row as $k => $column)
        {
            $row[$k] = $this->clean($column);
        }

        return $row;
    }

    /**
     * Remove the extra whitespaces and line breaks and combine it to one line.
     *
     * @param  string $string
     *
     * @return string
     */
    private function clean($string)
    {
        $string = preg_replace('/\s\s+/', ' ', $string);

        return trim(preg_replace('/\n/', ' ', $string));
    }

}