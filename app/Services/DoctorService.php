<?php namespace MEDoctors\Services;

use MEDoctors\Repositories\Contracts\UserRepository;

class DoctorService {

    /**
     * @var $userRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->model = $userRepository;
    }


    /**
     * Get all doctor users.
     *
     * @param  array $achievement
     *
     * @return array
     */
    public function getDoctors()
    {
        return $this->model->with('specialties','work.hospital.location')->whereHas('roles', function($q){
            $q->where('role_id' ,'=', 2);
        });
    }


}