<?php namespace MEDoctors\Services\Feeds;

use SimplePie_Item;

abstract class FeedReader {
    
    /**
     * The terms to be excluded from the rss content.
     *
     * @var array
     */
    protected $termFilters = [
        'anal',
        'anus',
        'arse',
        'ass',
        'ballsack',
        'balls',
        'bastard',
        'bitch',
        'biatch',
        'bloody',
        'blowjob',
        'blow job',
        'bollock',
        'bollok',
        'boner',
        'boob',
        'bugger',
        'bum',
        'butt',
        'buttplug',
        'clitoris',
        'cock',
        'coon',
        'crap',
        'cunt',
        'damn',
        'dick',
        'dildo',
        'dyke',
        'fag',
        'feck',
        'fellate',
        'fellatio',
        'felching',
        'fuck',
        'f u c k',
        'fudgepacker',
        'fudge packer',
        'flange',
        'Goddamn',
        'God damn',
        'hell',
        'homo',
        'jerk',
        'jizz',
        'knobend',
        'knob end',
        'labia',
        'lmao',
        'lmfao',
        'muff',
        'nigger',
        'nigga',
        'omg',
        'penis',
        'piss',
        'poop',
        'prick',
        'pube',
        'pussy',
        'queer',
        'scrotum',
        'sex',
        'shit',
        's hit',
        'sh1t',
        'slut',
        'smegma',
        'spunk',
        'tit',
        'tosser',
        'turd',
        'twat',
        'vagina',
        'wank',
        'whore',
        'wtf',
        'marijuana'
    ];

    /**
     * Create rss feed payload.
     *
     * @param  array  $feeds
     *
     * @return array
     */
    protected function createPayload(array $feeds = [])
    {
        $data = [];
        $i = 0;

        foreach ($feeds as $ind=>$feed)
        {
            if ( ! $this->feedIsValid($feed)) continue;
            $data[$i]['title'] = $feed->get_title();
            $data[$i]['description'] = $feed->get_description();
            $data[$i]['url'] = $feed->get_link();

            $i++;
        }

        return $data;
    }

    /**
     * Check if the feed does not contain sensitive content.
     *
     * @param  SimplePie_Item $feed
     *
     * @return bool
     */
    protected function feedIsValid(SimplePie_Item $feed)
    {
        if(str_contains($feed->get_title(), $this->termFilters))
        {
            return false;
        }

        if(str_contains($feed->get_description(), $this->termFilters))
        {
            return false;
        }

        return true;
    }
}