<?php namespace MEDoctors\Services\Feeds;

use MEDoctors\Models\NewsFeed;
use MEDoctors\Repositories\Contracts\NewsFeedRepository;

use Illuminate\Support\Facades\Log;
class KimonoNewsFetcher {

    public function  __construct(NewsFeedRepository $newsFeedRepository){
        $this->news = $newsFeedRepository;
    }

    public  function fetchPost(Request $request)
    {
        $results = $request->input();
        $newData = $this->fetch($results);

        Log::info($newData.' Data fetched.');
        echo $newData.' Data fetched.';
    }

    public  function fetchAPI()
    {
        $request = "https://www.kimonolabs.com/api/cd22cjuu?apikey=z5k1LKk9H3noOY1vtknWftQDGKOJ4POj&kimbypage=1";
        $response = file_get_contents($request);
        $results = json_decode($response, TRUE);

        $report = $this->fetch($results);

        Log::info($report);
        echo $report;
    }

    public  function fetch($resource)
    {
        $data = [];

//        print_r($results);

        $index = 0;
        foreach($resource['results'] as $page){

            if(isset($page['body'])){

                isset($page['url']) ? $data[$index]['url'] = $page['url'] : null;

                isset($page['head'][0]['title']) ? $data[$index]['title'] = $page['head'][0]['title'] : null;

                isset($page['head'][0]['sub-title']) ? $data[$index]['subtitle'] = $page['head'][0]['sub-title'] : null;

                isset($page['head'][0]['image']) ? $data[$index]['photo'] = $page['head'][0]['image']['src'] : null;

                if(isset($page['head'][0]['date_posted'])) {
                    $date = "";
                    $dateStringComponents = explode(' ',$page['head'][0]['date_posted']);
                    foreach($dateStringComponents as $component){
                        if(strtotime($component))
                            $date .= " ".$component;

                    }
                    $data[$index]['date_posted'] = strtotime($date);
                }

                $data[$index]['content'] = '<p>';
                foreach($page['body'] as $lines=>$body){

                    if($lines+1==count($page['body']))
                        $data[$index]['content'] .= '</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>';
                    elseif($lines%2==1)
                        $data[$index]['content'] .= '</p><p>&nbsp;</p><p>';

                    $data[$index]['content'] .= $body['contents'];
                }


                $data[$index]['content'] .= '<p>&nbsp;</p>'.'Copyright Owner: <a href="'.$page['url'].'">'.$page['url'].'</a>';

                $index++;
            }

        }


        $newData = 0;
        $existingData = 0;
        $skipped = 0;
        foreach($data as $news){
            $news_exists = NewsFeed::where('url',$news['url'])->first();

            if(!$news_exists && $newData < 5){
                $newData++;
                $news['channel_id'] = 2;
                $return_data[] = $this->news->store($news,null);
            } elseif($news_exists) {
                $existingData++;
            } else {
                $skipped++;
            }
        }


        return $newData . ' Added. ' . $existingData . ' already exists. '.$skipped.' skipped';
    }
}