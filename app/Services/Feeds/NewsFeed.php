<?php namespace MEDoctors\Services\Feeds;

use Feeds;

class NewsFeed extends FeedReader {

    /**
     * Get medical rss feeds from google news.
     *
     * @return array
     */
    public function medical()
    {
        $url = 'http://news.google.com/news?cf=all&hl=en&pz=2&ned=us&q=medical&output=rss';
        
        $feed = Feeds::make($url);

        $news = $this->createPayload($feed->get_items());

        if(count($news)<5){
            $url = 'http://news.google.com/news?cf=all&hl=en&pz=2&ned=us&q=medical+middle+east&output=rss';
            $feed = Feeds::make($url);
            $feeds = $this->createPayload($feed->get_items());
            foreach($feeds as $feed_item)
                array_push($news, $feed_item);
        }

        return array_slice($news, 0, 5);
    }

}