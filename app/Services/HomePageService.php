<?php namespace MEDoctors\Services;

use MEDoctors\Models\NewsFeed;
use MEDoctors\Models\Poll;
use MEDoctors\Models\User;
use MEDoctors\Models\Survey;
use MEDoctors\Repositories\Contracts\PollRepository;
use MEDoctors\Repositories\Contracts\EventRepository;
use MEDoctors\Repositories\Contracts\ArticleRepository;
use MEDoctors\Repositories\Contracts\ChannelRepository;
use MEDoctors\Repositories\Contracts\VideoRepository;


class HomePageService {

    /**
     * @var ArticleRepository
     */
    protected $article;

    /**
     * @var EventRepository
     */
    protected $event;

    /**
     * @var PollRepository
     */
    protected $poll;

    /**
     * Create an instance of the service.
     *
     * @param ArticleRepository    $article
     * @param EventRepository      $event
     * @param VideoRepository      $video
     * @param PollRepository       $poll
     */
    public function __construct(ArticleRepository $article,
                                EventRepository $event,
                                VideoRepository $video,
                                PollRepository $poll)
    {
        $this->article = $article;
        $this->event = $event;
        $this->video = $video;
        $this->poll = $poll;
    }

    /**
     * Take seven most recent articles.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function latestArticles($slug, $items = 6)
    {
        return $this->article->noUpcoming()->withChannel($slug)->latestPaginated($items);
    }

    /**
     * Take seven most recent articles.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function videos($slug, $items = 6)
    {
        return $this->video->withChannel($slug)->latestPaginated($items);
    }

    /**
     * Take 6 most popular articles.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function popularArticles($slug, $channel_id)
    {
        return $this->article->withChannel($slug)->popular(5,$channel_id);
    }

    /**
     * Get the first eight upcoming events.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function upcomingEvents()
    {
        return $this->event->paginatedUpcoming(8);
    }

    /**
     * Get the first eight upcoming events.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function pastEvents()
    {
        return $this->event->paginatedPast(8);
    }

    /**
     * Get the latest poll and determine if the user already voted.
     *
     * @param  int $userId
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function latestPoll($userId)
    {
        return $this->poll->withVoteByUserId($userId)->latest();
    }

    /**
     * Get the latest poll and determine if the user already voted.
     *
     * @param  int $userId
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function latestSurveys($limit,$channel_id)
    {
        return Survey::where('channel_id', $channel_id)->with('therapies')->take($limit)->get();
    }

    /**
     * Get the News.
     *
     * @param  int $userId
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function latestNews($limit,$channel_id)
    {
        return NewsFeed::where('channel_id', $channel_id)->orderBy('created_at','DESC')->take($limit)->get();
    }

    /**
     * Get the featured poll and determine if the user already voted.
     *
     * @param  int $userId
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function activePoll($userId, $slug)
    {
        return $this->poll->withVoteByUserId($userId)->featured($userId, $slug);
    }


    /**
     * Get the latest poll and determine if the user already voted.
     *
     * @param  int $userId
     *
     * @return \MEDoctors\Models\Poll|null
     */
    public function featuredArticle($slug)
    {
        return $this->article->featured($slug);
    }

}
