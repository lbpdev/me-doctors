<?php namespace MEDoctors\Services\Hospitals;

use MEDoctors\Models\Hospital;
use MEDoctors\Services\CsvReader;
use MEDoctors\Repositories\Contracts\HospitalRepository;

class HospitalDuplicateRemover {

    protected $hospitalRepository;

    /**
     * Create HospitalImporter instance.
     *
     * @param HospitalRepository    $hospitalRepository
     */
    public function __construct(HospitalRepository $hospitalRepository)
    {
        $this->hospitalRepository = $hospitalRepository;
    }

    /**
     * Import hospitals from a csv file path.
     *
     * @param  string $path
     *
     * @return void
     */
    public function clearDuplicates()
    {
        $hospitals = Hospital::get();
        $allDups = [];

        foreach($hospitals as $hospital){
            $duplicates = Hospital::where('name', $hospital->name)->lists('id');
            foreach($duplicates as $index=>$duplicate){
                if($index>0){
                    Hospital::where('id',$duplicate)->delete();
                    $allDups[] = $duplicates[$index];
                }
            }

        }

        return count($allDups);
    }


}