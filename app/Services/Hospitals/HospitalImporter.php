<?php namespace MEDoctors\Services\Hospitals;

use MEDoctors\Services\CsvReader;
use MEDoctors\Services\Hospitals\TherapyFinder;
use MEDoctors\Repositories\Contracts\HospitalRepository;
use MEDoctors\Repositories\Contracts\Emails\EmailDomainsWhitelist;

class HospitalImporter {

    /**
     * @var CsvReader
     */
    protected $csvReader;

    /**
     * @var TherapyFinder
     */
    protected $therapyFinder;

    /**
     * @var HospitalRepository
     */
    protected $hospitalRepository;

    /**
     * Create HospitalImporter instance.
     *
     * @param CsvReader             $csvReader
     * @param TherapyFinder         $therapyFinder
     * @param HospitalRepository    $hospitalRepository
     * @param EmailDomainsWhitelist $emailDomainWhitelist
     */
    public function __construct(CsvReader $csvReader, 
                                TherapyFinder $therapyFinder, 
                                HospitalRepository $hospitalRepository,
                                EmailDomainsWhitelist $emailDomainWhitelist)
    {
        $this->csvReader = $csvReader;
        $this->therapyFinder = $therapyFinder;
        $this->hospitalRepository = $hospitalRepository;
        $this->emailDomainWhitelist = $emailDomainWhitelist;
    }
    
    /**
     * Import hospitals from a csv file path.
     *
     * @param  string $path
     *
     * @return void
     */
   public function importFromPath($path)
   {
       $data = $this->csvReader->path($path)->get();

       if ($data)
       {
            $hospitals = $this->buildPayload($data);

            $this->importToDatabase($hospitals);

            return $hospitals;
       }

       return null;
   }

   /**
    * Save the hospitals array to the database.
    *
    * @param  array $hospitals
    *
    * @return mixed
    */
   private function importToDatabase(array $hospitals)
   {
        $models = [];

        foreach ($hospitals as $hospital)
        {
            $models[] = $this->hospitalRepository->store(
                $hospital['hospital'],
                (array) $hospital['contact'],
                [], // facilities
                (array) $hospital['therapies'],
                $hospital['location']
            );
        }

        // Import the hospital websites to the email domains white list.
        $this->addWebsitesToWhitelist(
            array_filter(array_fetch($hospitals, 'hospital.website'))
        );

        return $models;
   }

   /**
    * Add the website to email domains whitelist.
    *
    * @param array $websites
    *
    * @return void
    */
   private function addWebsitesToWhitelist($websites = [])
   {
       if ($websites)
       {
            $this->emailDomainWhitelist->importWebsite($websites);
       }
   }

   /**
    * Build hospital payload for creation.
    *
    * @param  array  $data
    *
    * @return array
    */
   private function buildPayload(array $data)
   {
       $hospitals = $hospital = [];

        $data = $this->prepareHospitalData($data);

        foreach ($data as $k => $value)
        {
            if ( ! $value['name']) continue;

            $hospital['hospital'] = [
                'name'     =>  $value['name'],
                'verified' =>  1,
                'website'  => $value['website']
            ];

            $hospital['location'] = [
                'name'    => $value['location'],
                'city'    => $value['city'],
                'country' => $value['country']
            ];

            $hospital['contact'] = $value['phone'];

            $hospital['therapies'] = $this->extraTherapiesFromDatabase($value['specialties']);

            // $hospital['specialties'] = explode(',', $value['specialties']);

            $hospitals[] = $hospital;
        }

       return $hospitals;
   }

   /**
    * Prepare/Clean the hospital data.
    *
    * @param  array $data
    *
    * @return array
    */
   private function prepareHospitalData(array $data)
   {
        return array_map(function($item) {

            $item['specialties'] = trim(str_replace('.', '', preg_replace('/ ?, ?/', ',', $item['specialties'])), ',');
            $item['website'] = strtolower(trim($item['website'], '/'));

            return $item;
        }, $data);
   }

   /**
    * We will try to extract the therapies given and compare it to database.
    * All the specialties related from the database result will be returned.
    *
    * @param  string  $specialties
    *
    * @return array|null
    */
   private function extraTherapiesFromDatabase($specialties)
   {
        if ( ! $specialties) return null;

        $therapies = [];

        foreach (explode(',', $specialties) as $k => $specialty)
        {
            $therapy = $this->therapyFinder->findRelatedTo($specialty);

            if ( ! $therapy) continue;

            $therapies = array_merge($therapy, $therapies);
        }

        return $this->therapyFinder->getIdsByNames($therapies);
   }

}