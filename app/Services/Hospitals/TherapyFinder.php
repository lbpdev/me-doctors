<?php namespace MEDoctors\Services\Hospitals;

use MEDoctors\Repositories\Contracts\TherapyRepository;

class TherapyFinder {
    
    /**
     * @var TherapyRepository
     */
    protected $therapyRepository;

    /**
     * @var array
     */
    protected $therapies = [];

    /**
     * Create DatabaseTherapyFinder instance.
     *
     * @param TherapyRepository $therapyRepository
     */
    public function __construct(TherapyRepository $therapyRepository)
    {
        $this->therapyRepository = $therapyRepository;
        $this->therapies = config('middleeastdoctor.therapies');
    }

    /**
     * Find a related therapy model for the given string.
     *
     * @param  string $therapyToLook
     *
     * @return array|null
     */
    public function findRelatedTo($therapyToLook)
    {
        $therapyToLook = strtolower($therapyToLook);

        $matches = [];

        foreach($this->therapies as $therapy)
        {
            $therapyName = $therapy['name'];
            $existingTags = isset($therapy['tags']) ? $therapy['tags']: [];

            $tags = array_merge($existingTags, (array) strtolower($therapyName));

            foreach ($tags as $tag)
            {
                $tag = strtolower($tag);
                $hasText = strpos($tag, $therapyToLook);
                $hasTextReverse = strpos($therapyToLook, $tag);

                if ( ! in_array($therapyName, $matches)
                    && ($hasText !== false || $hasTextReverse !== false)
                )
                {
                    $matches[] = $therapyName;
                }
            }
        }

        return $matches;
    }

    /**
     * Find therapy models from the database by names.
     *
     * @param  array  $names
     *
     * @return array
     */
    public function getIdsByNames(array $names = [])
    {
        $therapies = $this->therapyRepository->findByName($names);

        return $therapies->lists('id');
    }
}