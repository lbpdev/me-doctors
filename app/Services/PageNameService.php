<?php

namespace MEDoctors\Services;

use Illuminate\Support\Facades\Route;
use MEDoctors\Http\Controllers\Controller;

class PageNameService {

    public function getPageName(){
        $pageName = "";

        $pageNames = [
            'login'       => 'Log-in',
            'articles'       => 'Articles',
            'discussions'    => 'Discussions',
            'diagnosis'    => 'Diagnosis',
            'doctors'    => 'Doctors Directory',
            'hospitals'    => 'Hospitals Directory',
            'about'    => 'About MED',
            'contact'    => 'Contact Us',
        ];

        foreach($pageNames as $pageName){
            if(strpos(Request::path(), $pageName))
                dd($pageName);
        }

        return $pageName;
    }
}