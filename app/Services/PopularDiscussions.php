<?php namespace MEDoctors\Services;

use MEDoctors\Repositories\Contracts\DiscussionRepository;

class PopularDiscussions {
    
    /**
     * @var DiscussionRepository
     */
    protected $discussion;

    public function __construct(DiscussionRepository $discussion)
    {
        $this->discussion = $discussion;
    }

    /**
     * Takes six recent panel discussions.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function panelDiscussions()
    {
        return $this->discussion->popularPanelDiscussions(5);
    }

    /**
     * Takes six recent doctor(diagnosis) discussions.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function diagnosisDiscussions()
    {
        return $this->discussion->popularDoctorDiscussions(5);
    }

    /**
     * Takes six recent doctor(diagnosis) discussions.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function patientDiscussions()
    {
        return $this->discussion->popularPatientDiscussions(5);
    }

}