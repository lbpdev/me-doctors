<?php namespace MEDoctors\Services\Registration;

use MEDoctors\Repositories\Contracts\Emails\EmailsWhitelist;
use MEDoctors\Repositories\Contracts\Emails\EmailDomainsBlacklist;
use MEDoctors\Repositories\Contracts\Emails\EmailDomainsWhitelist;

class DoctorEmailValidator {

    /**
     * The error message.
     *
     * @var string
     */
    protected $errorMessage;

    /**
     * @var EmailDomainsBlacklist
     */
    protected $emailDomainsBlacklist;

    /**
     * @var EmailDomainsWhitelist
     */
    protected $emailDomainsWhitelist;

    /**
     * @var EmailsWhitelist
     */
    protected $emailsWhitelist;
    
    /**
     * Create DoctorEmailValidator instance.
     *
     * @param EmailDomainsBlacklist $emailDomainsBlacklist
     * @param EmailDomainsWhitelist $emailDomainsWhitelist
     * @param EmailsWhitelist       $emailsWhitelist
     */
    public function __construct(EmailDomainsBlacklist $emailDomainsBlacklist,
                                EmailDomainsWhitelist $emailDomainsWhitelist,
                                EmailsWhitelist $emailsWhitelist)
    {
        $this->emailDomainsBlacklist = $emailDomainsBlacklist;
        $this->emailDomainsWhitelist = $emailDomainsWhitelist;
        $this->emailsWhitelist = $emailsWhitelist;
    }

    /**
     * Validate doctor's email for registration.
     *
     * @param  string  $email
     *
     * @return boolean
     */
    public function validForRegistration($email)
    {
        $this->setErrorMessage(trans('registration.invalid.email'));

        // or if email domain is white listed
        if ($this->emailsWhitelist->contains($email) or 
            $this->emailDomainsWhitelist->contains($email))
        {
            return true;
        }

        if ($this->emailDomainsBlacklist->contains($email))
        {
            return false;
        }

        return false;
    }

    /**
     * Error message getter.
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set the error message.
     *
     * @param string $message
     */
    public function setErrorMessage($message)
    {
        $this->errorMessage = $message;
    }

}