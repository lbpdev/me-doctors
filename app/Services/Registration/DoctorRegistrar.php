<?php namespace MEDoctors\Services\Registration;

use Illuminate\Bus\Dispatcher;
use MEDoctors\Repositories\Contracts\MailRepository;
use Validator;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Exceptions\InvalidDoctorEmailException;
use MEDoctors\Services\Registration\DoctorEmailValidator;
use MEDoctors\Services\Uploaders\DoctorCredentialsUploader;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class DoctorRegistrar implements RegistrarContract {

    /**
     * @var UserRepository
     */
    protected $userRepository;
    
    /**
     * @var DoctorEmailValidator
     */
    protected $emailValidator;

    /**
     * @var DoctorCredentialsUploader
     */
    protected $uploader;

    /**
     * Create DoctorRegistrar instance.
     *
     * @param UserRepository $userRepository
     * @param DoctorEmailValidator $emailValidator
     * @param DoctorCredentialsUploader $uploader
     * @param MailRepository $mailRepository
     */
    public function __construct(UserRepository $userRepository, 
                                DoctorEmailValidator $emailValidator,
                                DoctorCredentialsUploader $uploader,
                                MailRepository $mailRepository,
                                Dispatcher $dispatcher)
    {
        $this->userRepository = $userRepository;
        $this->emailValidator = $emailValidator;
        $this->uploader = $uploader;
        $this->mail = $mailRepository;
        $this->dispatcher = $dispatcher;
    }
    
	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
        $fileRules = 'multiple_mimes:jpeg,bmp,png,doc,docx,pdf,xls,xlsx,ppt,pptx,txt,zip,rar';
        
        $rules = [
            'country'             => 'required|max:255',
            'fname'               => 'required|max:255',
            'lname'               => 'required|max:255',
            'username'            => 'required|max:255|unique:users',
            'email'               => 'required|email|max:255|unique:users',
            'password'            => 'required|min:6',
            'designation'         => 'required|max:255',
            'therapies'           => 'required|max:255',
            'phone_number'        => 'sometimes|required',
            'education'           => 'sometimes',
            'phone_numbers'       => 'sometimes',
            'certifications'      => 'sometimes',
            'place_of_employment' => 'sometimes|required',
            'license_files'       => 'sometimes|multiple_required|' . $fileRules,
            'photo_files'         => 'sometimes|multiple_required|' . $fileRules,
            'other_files'         => $fileRules
        ];

		return Validator::make($data, $rules);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validatorLight(array $data)
	{
        $fileRules = 'multiple_mimes:jpeg,bmp,png,doc,docx,pdf,xls,xlsx,ppt,pptx,txt,zip,rar';

        $rules = [
            'username'            => 'required|max:255|unique:users',
            'email'               => 'required|email|max:255|unique:users',
            'photo'               => 'sometimes|' . $fileRules
        ];

		return Validator::make($data, $rules);
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * 
     * @return User
     * @throws InvalidDoctorEmailException
     */
	public function create(array $data)
    {
        if ( ! $this->emailValidator->validForRegistration($data['email']))
        {
            throw new InvalidDoctorEmailException($this->emailValidator->getErrorMessage());
        }

        $data['token'] = str_random(30);
        $data['verified'] = 1;

        $user = $this->userRepository->createDoctor($data);

        if(trim($data['referred_by']))
            $this->userRepository->createReferral($data,$user,1);

        return $user;
	}

    /**
     * Create a new user instance without validation.
     *
     * @param  array $data
     *
     * @return User
     * @throws InvalidDoctorEmailException
     */
	public function createBypass(array $data)
    {
        $data['token'] = str_random(30);
        $data['verified'] = 1;

        $user = $this->userRepository->createDoctor($data);

        return $user;
	}

    /**
     * Create a new doctor with their credentials.
     *
     * @param  array  $data
     * @param  array  $credentials
     *
     * @return User
     */
    public function createWithCredentials(array $data, array $credentials)
    {
        $user = $this->userRepository->createDoctor($data);

        if(trim($data['referred_by']))
            $referral = $this->userRepository->createReferral($data,$user,0);

        $credentials['attachments'] = $this->uploadCredentialFiles($credentials);

        $this->userRepository->attachDoctorCredentials($user, $credentials);

        $pass['user'] = $user->fname . ', ' . $user->lname;
        $pass['username'] = $user->username;


        // Notify User
        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.doctor.pending',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => $user->email,
            'subject'  => 'MED Account Verification',
            'message'  => $pass
        ]);

        // Notify Admin
        $this->dispatcher->dispatchFromArray('MEDoctors\Commands\SendMail', [
            'template' => 'emails.admin.pending-doctor',
            'sender_email' => 'mailman@middleeastdoctor.com',
            'sender_name'  => 'Middle East Doctor',
            'recipient_email'  => explode(',',env('ADMIN_EMAIL','gene@leadingbrands.me'),3),
            'subject'  => 'New Doctor Account Verification',
            'message'  => $pass
        ]);

        return true;
    }

    /**
     * Upload credential files.
     *
     * @param  array  $credentials
     *
     * @return array
     */
    private function uploadCredentialFiles(array $credentials)
    {
        $otherAttachments = [];

        if ($otherFiles = $credentials['other_files'])
        {
            $otherAttachments = $this->uploader->uploadMultiple($otherFiles);
        }

        $licenseFiles = $this->uploader->setFileTemplate('license')
                                       ->uploadMultiple($credentials['license_files']);

        $photoFiles = $this->uploader->setFileTemplate('photo_id')
                                       ->uploadMultiple($credentials['photo_files']);

        return array_merge($otherAttachments, $licenseFiles, $photoFiles);
    }

    /**
     * Confirm user's email address by token.
     *
     * @param  string $token
     *
     * @return mixed
     */
    public function confirmUserEmail($token)
    {
        return $this->userRepository->confirmEmail($token);
    }

}
