<?php namespace MEDoctors\Services\Registration;

use Illuminate\Bus\Dispatcher;
use MEDoctors\Repositories\Contracts\MailRepository;
use Validator;
use MEDoctors\Repositories\Contracts\UserRepository;
use MEDoctors\Exceptions\InvalidDoctorEmailException;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class PatientRegistrar implements RegistrarContract {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Create DoctorRegistrar instance.
     *
     * @param UserRepository $userRepository
     * @param MailRepository $mailRepository
     */
    public function __construct(UserRepository $userRepository,
                                MailRepository $mailRepository
                                )
    {
        $this->userRepository = $userRepository;
        $this->mail = $mailRepository;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        $rules = [
            'fname'               => 'required|max:255',
            'lname'               => 'required|max:255',
            'username'            => 'required|max:255|unique:users',
            'email'               => 'required|email|max:255|unique:users',
            'password'            => 'required|min:6',
            'designation'         => 'required|max:255',
        ];

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     * @throws InvalidDoctorEmailException
     */
    public function create(array $data)
    {
        $data['token'] = str_random(30);

        return $this->userRepository->createPatient($data);
    }


    /**
     * Confirm user's email address by token.
     *
     * @param  string $token
     *
     * @return mixed
     */
    public function confirmUserEmail($token)
    {
        return $this->userRepository->confirmEmail($token);
    }

}
