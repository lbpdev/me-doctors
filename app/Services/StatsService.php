<?php namespace MEDoctors\Services;

use MEDoctors\Repositories\Contracts\UserStatRepository;

class StatsService {
    
    /**
     * @var UserStatRepository
     */
    protected $userStat;

    /**
     * Create StatsService instance.
     *
     * @param UserStatRepository $userStat
     */
    public function __construct(UserStatRepository $userStat)
    {
        $this->userStat = $userStat;
    }

    /**
     * Get the count of new users within the month
     *
     * @return int
     */
    public function newUsers()
    {
        return $this->userStat->newUsers();
    }

    /**
     * Count the unverified doctors.
     *
     * @return int
     */
    public function unverifiedDoctors()
    {
        return $this->userStat->unverifiedDoctors();
    }
    
}