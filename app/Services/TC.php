<?php 
namespace MEDoctors\Services;

use MEDoctors\Services\Twocheckout\Api\TwocheckoutAccount;
use MEDoctors\Services\Twocheckout\Api\Twocheckout_Payment;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutApi;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutSale;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutProduct;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutCoupon;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutOption;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutUtil;
use MEDoctors\Services\Twocheckout\Api\TwocheckoutError;
use MEDoctors\Services\Twocheckout\TwocheckoutReturn;
use MEDoctors\Services\Twocheckout\TwocheckoutNotification;
use MEDoctors\Services\Twocheckout\TwocheckoutCharge;
use MEDoctors\Services\Twocheckout\TwocheckoutMessage;

class TC
{

    public static $sid;
    public static $privateKey;
    public static $username;
    public static $password;
    public static $sandbox;
    public static $verifySSL = true;
    public static $baseUrl = 'https://www.2checkout.com';
    public static $error;
    public static $format = 'array';
    const VERSION = '0.3.0';

    public static function sellerId($value = null) {
        self::$sid = $value;
    }

    public static function privateKey($value = null) {
        self::$privateKey = $value;
    }

    public static function username($value = null) {
        self::$username = $value;
    }

    public static function password($value = null) {
        self::$password = $value;
    }

    public static function sandbox($value = null) {
        if ($value == 1 || $value == true) {
            self::$sandbox = true;
            self::$baseUrl = 'https://sandbox.2checkout.com';
        } else {
            self::$sandbox = false;
            self::$baseUrl = 'https://www.2checkout.com';
        }
    }

    public static function verifySSL($value = null) {
        if ($value == 0 || $value == false) {
            self::$verifySSL = false;
        } else {
            self::$verifySSL = true;
        }
    }

    public static function format($value = null) {
        self::$format = $value;
    }
}
