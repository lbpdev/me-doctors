<?php
namespace MEDoctors\Services\Twocheckout\Api;

use MEDoctors\Services\TC;

class TwocheckoutCoupon extends TC
{

    public static function create($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/create_coupon';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

    public static function retrieve($params=array())
    {
        $request = new TwocheckoutApi();
        if(array_key_exists("coupon_code",$params)) {
            $urlSuffix = '/api/products/detail_coupon';
        } else {
            $urlSuffix = '/api/products/list_coupons';
        }
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }


    public static function update($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/update_coupon';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

    public static function delete($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/delete_coupon';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

}