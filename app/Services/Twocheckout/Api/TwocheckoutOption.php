<?php
namespace MEDoctors\Services\TwoCheckout\Api;

class TwocheckoutOption extends TC
{

    public static function create($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/create_option';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

    public static function retrieve($params=array())
    {
        $request = new TwocheckoutApi();
        if(array_key_exists("option_id",$params)) {
            $urlSuffix = '/api/products/detail_option';
        } else {
            $urlSuffix = '/api/products/list_options';
        }
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }


    public static function update($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/update_option';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

    public static function delete($params=array())
    {
        $request = new TwocheckoutApi();
        $urlSuffix = '/api/products/delete_option';
        $result = $request->doCall($urlSuffix, $params);
        return TwocheckoutUtil::returnResponse($result);
    }

}