<?php namespace MEDoctors\Validation;

use Illuminate\Validation\Validator;

class CustomValidator extends Validator {

    /**
     * If multiple files are allowed, validate the
     * first index of the array if it is not null.
     *
     * @param  string $attribute 
     * @param  string|array $value
     * @param  array $parameters
     *
     * @return boolean
     */
    public function validateMultipleRequired($attribute, $value, $parameters)
    {
        return ! is_null($value[0]);
    }

    /**
     * Validate multiple files if some of the files does not have a valid mime type.
     *
     * @param  string $attribute 
     * @param  string|array $value
     * @param  array $parameters
     *
     * @return boolean
     */
    public function validateMultipleMimes($attribute, $value, $parameters)
    {
        foreach ($value as $currentValue)
        {
            if (is_null($currentValue))
            {
                return true;
            }

            if ( ! $this->validateMimes($attribute, $currentValue, $parameters))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Replace all place-holders for the multiple mimes rule.
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array   $parameters
     * @return string
     */
    protected function replaceMultipleMimes($message, $attribute, $rule, $parameters)
    {
        return $this->replaceMimes($message, $attribute, $rule, $parameters);
    }

}