<?php 

/**
 * Set navigation link to active
 *
 * @param string|array $path
 * @param string $active
 *
 * @return string
 */
function set_active($path, $active = 'active')
{
    if (is_array($path))
    {
        foreach ($path as $urlPath)
        {
            if ($active = set_active($urlPath)) return $active;
        }

        return '';
    }

    $path = ltrim($path, '/');

    return Request::is($path) ? $active : '';
}

/**
 * Article tag uri generator for atom feed.
 *
 * @param  \MEDoctors\Models\Article $article
 *
 * @return string
 */
function article_tag_uri($article)
{
    $parsedUrl = parse_url(route('doctor.articles.show', $article->slug));

    $output[] = 'tag:';
    $output[] = $parsedUrl['host'] . ',';
    $output[] = $article->updated_at->format('Y-m-d') . ':';
    $output[] = $parsedUrl['path'];

    return implode('', $output);
}