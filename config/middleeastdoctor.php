<?php

return [

    'therapies' => [
        [ 
            'name' => 'Allergies and Immunology',
            'tags' => [
                'allergy',
                'allergies',
                'immunology',
                'immune'
            ]
        ],
        [
            'name' => 'Anaesthesiology',
            'tags' => [
                'anesthetic',
                'anaesthetic',
                'anaesthesia'
            ]
        ],
        [
            'name' => 'Cardiology',
            'tags' => [
                'cardio',
                'cardiovascular',
                'heart'
            ]
        ],
        [
            'name' => 'Cosmetic Surgery',
            'tags' => [
                'cosmetics',
                'cosmetic'
            ]
        ],
        [
            'name' => 'Dentistry',
            'tags' => [
                'dental'
            ]
        ],
        [
            'name' => 'Dermatology',
            'tags' => [
                'derma'
            ]
        ],
        [
            'name' => 'Endocrinology',
            'tags' => [
                'endocrine'
            ]
        ],
        [
            'name' => 'Family Medicine'
        ],
        [
            'name' => 'Gastroenterology'
        ],
        [
            'name' => 'General Practice',
            'tags' => [
                'general practitioner',
            ]
        ],
        [
            'name' => 'General Surgery',
            'tags' => [
                'general surgery'
            ]
        ],
        [
            'name' => 'Geriatrics'
        ],
        [
            'name' => 'Haematology',
            'tags' => [
                'hematology'
            ]
        ],
        [
            'name' => 'Hepatology'
        ],
        [
            'name' => 'Infectious Diseases'
        ],
        [
            'name' => 'Internal Medicine (Internists)',
            'tags' => [
                'internal medicine',
                'internists'
            ]
        ],
        [
            'name' => 'Laboratory Medicine'
        ],
        [
            'name' => 'Maxillo-facial Surgery',
            'tags' => [
                'maxillo',
                'facial'
            ]
        ],
        [
            'name' => 'Nephrology'
        ],
        [
            'name' => 'Neurology'
        ],
        [
            'name' => 'Neurosurgery'
        ],
        [
            'name' => 'Nuclear Medicine'
        ],
        [
            'name' => 'Nutrition and Diet',
            'tags' => [
                'nutrition',
                'diet'
            ]
        ],
        [
            'name' => 'Obstetrics and Gynaecology',
            'tags' => [
                'obstetrics',
                'gynaecology',
                'gynecology'
            ]
        ],
        [
            'name' => 'Occupational Medicine'
        ],
        [
            'name' => 'Oncology'
        ],
        [
            'name' => 'Ophthalmology'
        ],
        [
            'name' => 'Orthopaedics',
            'tags' => [
                'orthopaedics',
                'orthopedics'
            ]
        ],
        [
            'name' => 'Otorhinolaryngology (ENT)',
            'tags' => [
                'otorhinolaryngology'
            ]
        ],
        [
            'name' => 'Paediatrics and Neonatology',
            'tags' => [
                'paediatric',
                'pediatric',
                'neonatology',
                'pedia'
            ]
        ],
        [
            'name' => 'Pain Management',
            'tags' => [
                'pain'
            ]
        ],
        [
            'name' => 'Pathology'
        ],
        [
            'name' => 'Physical Therapy'
        ],
        [
            'name' => 'Podiatry'
        ],
        [
            'name' => 'Pulmonology',
            'tags' => [
                'pulmon'
            ]
        ],
        [
            'name' => 'Psychiatry and Psychology',
            'tags' => [
                'psychiatry',
                'psychology',
                'adolescent'
            ]
        ],
        [
            'name' => 'Radiology and Radiotherapy'
        ],
        [
            'name' => 'Rheumatology'
        ],
        [
            'name' => 'Trauma and Emergency',
            'tags' => [
                'emergency',
                'trauma',
                'accident'
            ]
        ],
        [
            'name' => 'Urology'
        ],
        [
            'name' => 'Vascular Surgery',
            'tags' => [
                'vascular'
            ]
        ],
    ]
];
