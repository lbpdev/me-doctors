<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table)
        {
            $table->increments('id');
            $table->char('code', 4)->unique()->default('');
            $table->string('name', 45)->default('');
            $table->char('currency_code', 3)->nullable()->default('');
            $table->string('continent_name', 15)->nullable()->default('');
            $table->double('area_sqkm')->nullable()->default(0);
            $table->integer('geoname_id')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }

}
