<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return voidS
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table)
        {
            $table->increments('id');
            $table->char('country_code', 2);
            $table->foreign('country_code')->references('code')->on('countries');
            $table->char('region', 24)->nullable()->default('');
            $table->string('name', 50)->nullable()->default('');
            $table->double('latitude')->nullable()->default(0);
            $table->double('longitude')->nullable()->default(0);

            $table->unique(['country_code', 'name', 'region'], 'country_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities');
    }

}
