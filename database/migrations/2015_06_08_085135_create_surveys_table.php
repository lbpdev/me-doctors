<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('surveys', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('channel_id')->index()->unsigned();
            $table->foreign('channel_id')->references('id')->on('channels');
            $table->integer('therapy_id')->index()->unsigned();
            $table->foreign('therapy_id')->references('id')->on('therapies');
            $table->string('title');
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);
            $table->string('slug')->unique();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('surveys');
	}

}
