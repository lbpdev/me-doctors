<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyItemChoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('survey_item_choices', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('value');
            $table->integer('survey_item_id')->unsigned()->index();
            $table->foreign('survey_item_id')->references('id')->on('survey_items')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('survey_item_choices');
	}

}
