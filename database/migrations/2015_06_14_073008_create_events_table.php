<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
            $table->date('event_date');
            $table->string('title' , 100);
            $table->integer('thumbnail')->index()->unsigned()->nullable();
            $table->foreign('thumbnail')->references('id')->on('uploads')->onDelete('cascade');
            $table->text('content');
            $table->string('city' , 25);
            $table->string('country' , 25);
            $table->string('state' , 25);
            $table->text('location');
            $table->string('slug')->unique();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
