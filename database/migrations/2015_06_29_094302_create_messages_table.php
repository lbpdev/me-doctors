i<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conversation_id')->unsigned()->index();
			$table->foreign('conversation_id')->references('id')->on('conversations')->onDelete('cascade');
			$table->integer('sender_id')->unsigned()->index();
			$table->foreign('sender_id')->references('id')->on('users')->onDelete('cascade');
			$table->text('message');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
