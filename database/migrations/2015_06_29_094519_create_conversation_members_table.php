<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conversation_members', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conversation_id')->index()->unsigned();
			$table->foreign('conversation_id')->references('id')->on('conversations')->onDelete('cascade');
			$table->integer('user_id')->index()->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamp('joined_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('left_at')->default('0000-00-00 00:00:00');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conversation_members');
	}

}
