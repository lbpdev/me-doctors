<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('locationable_id');
            $table->string('locationable_type');
            $table->string('name');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('post_code')->nullable();
            $table->string('street_number', 10)->nullable();
            $table->string('street_route')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
