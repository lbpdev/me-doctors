<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalFacilityUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hospital_facility_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('hospital_id')->index()->unsigned();
			$table->foreign('hospital_id')->references('id')->on('hospitals')->onDelete('cascade');
			$table->integer('hospital_facility_id')->index()->unsigned();
			$table->foreign('hospital_facility_id')->references('id')->on('hospital_facilities')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hospital_facility_users');
	}

}
