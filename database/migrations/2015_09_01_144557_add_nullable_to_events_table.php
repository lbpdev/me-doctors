<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('events', function(Blueprint $table)
		{
            $table->string('city' , 25)->nullable()->change();
            $table->string('country' , 25)->nullable()->change();
            $table->string('state' , 25)->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function(Blueprint $table)
		{
			$table->string('city' , 25)->change();
            $table->string('country' , 25)->change();
            $table->string('state' , 25)->change();
		});
	}

}
