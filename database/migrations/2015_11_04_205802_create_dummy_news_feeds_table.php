<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDummyNewsFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dummy_news_feeds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('channel_id')->index()->unsigned();
			$table->foreign('channel_id')->references('id')->on('channels');
			$table->string('title');
			$table->string('subtitle');
			$table->text('content');
			$table->string('slug')->unique();
			$table->date('date_posted');
			$table->string('photo')->nullable();
			$table->string('url')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dummy_news_feeds');
	}

}
