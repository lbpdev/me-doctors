<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',25);
			$table->string('slug',100);
			$table->string('location',10);
			$table->string('sizeW',10);
			$table->string('sizeH',10);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_types');
	}

}
