<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('active');
            $table->string('title',64);
            $table->string('description');
			$table->string('link',128);
			$table->date('ad_start');
			$table->date('ad_end');
            $table->integer('channel_id')->index()->unsigned();
            $table->foreign('channel_id')->references('id')->on('channels');
			$table->integer('ad_type_id')->indexed()->unsigned();
			$table->foreign('ad_type_id')->references('id')->on('ad_types')->onDelete('cascade');
			$table->integer('ad_user_id')->indexed()->unsigned();
			$table->foreign('ad_user_id')->references('id')->on('ad_users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads');
	}

}
