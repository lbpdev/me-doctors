<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceHospitalUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_hospital_users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('hospital_id')->indexed()->unsigned();
            $table->foreign('hospital_id')->references('id')->on('hospitals')->onDelete('cascade');
            $table->integer('premium_service_id')->indexed()->unsigned();
            $table->foreign('premium_service_id')->references('id')->on('premium_services')->onDelete('cascade');
            $table->tinyInteger('active');
            $table->date('expires_on');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_hospital_users');
	}

}
