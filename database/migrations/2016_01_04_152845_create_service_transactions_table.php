<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_transactions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('sid');
            $table->string('key');
            $table->string('order_number');
            $table->string('product_description');
            $table->string('lang',12);
            $table->string('currency_code');
            $table->string('invoice_id');
            $table->string('total',12);
            $table->string('credit_card_processed');
            $table->integer('user_id')->indexed()->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('cart_weight',12);
            $table->string('fixed',3);
            $table->integer('product_id')->indexed()->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('premium_services')->onDelete('cascade');
            $table->string('merchant_order_id');
            $table->string('country');
            $table->string('ip_country');
            $table->string('demo');
            $table->string('quantity');
            $table->string('pay_method',12);
            $table->string('cart_tangible',3);
            $table->string('merchant_product_id');
            $table->string('card_holder_name');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_transactions');
	}

}
