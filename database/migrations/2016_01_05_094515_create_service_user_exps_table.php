<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceUserExpsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_user_exps', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('user_id')->indexed()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('premium_service_id')->indexed()->unsigned();
            $table->foreign('premium_service_id')->references('id')->on('premium_services')->onDelete('cascade');
            $table->tinyInteger('active');
            $table->date('expires_on');
            $table->timestamp('started_on');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_user_exps');
	}

}
