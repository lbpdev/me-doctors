<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdPendingItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_pending_items', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('ad_id')->unsigned()->index()->nullable();
            $table->foreign('ad_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('premium_service_id')->unsigned()->index();
            $table->foreign('premium_service_id')->references('id')->on('premium_services')->onDelete('cascade');
            $table->integer('ad_pending_id')->unsigned()->index();
            $table->foreign('ad_pending_id')->references('id')->on('ad_pendings')->onDelete('cascade');
            $table->tinyInteger('status');
            $table->string('duration',12);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_pending_items');
	}

}
