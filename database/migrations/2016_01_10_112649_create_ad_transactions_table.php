<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_transactions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('service_transaction_id')->unsigned()->index();
            $table->foreign('service_transaction_id')->references('id')->on('service_transactions')->onDelete('cascade');
            $table->integer('ad_pending_item_id')->unsigned()->index();
            $table->foreign('ad_pending_item_id')->references('id')->on('ad_pending_items')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_transactions');
	}

}
