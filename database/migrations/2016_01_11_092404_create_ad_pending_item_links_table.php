<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdPendingItemLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_pending_item_links', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('ad_pending_item_id')->unsigned()->index();
            $table->foreign('ad_pending_item_id')->references('id')->on('ad_pending_items')->onDelete('cascade');
            $table->string('url',128);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_pending_item_links');
	}

}
