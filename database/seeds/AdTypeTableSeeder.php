<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\AdType;

class AdTypeTableSeeder extends Seeder {

    public function run()
    {

        AdType::create(array(
            'name'      => 'Sidebar Small ( 245x245 )',
            'slug'      => 'sidebar-sm',
            'location'  => 'sidebar',
            'sizeW'     => '',
            'sizeH'     => ''
        ));

        AdType::create(array(
            'name'      => 'Sidebar Long ( 245x*** )',
            'slug'      => 'sidebar-long',
            'location'  => 'sidebar',
            'sizeW'     => '',
            'sizeH'     => ''
        ));

        AdType::create(array(
            'name'      => 'Top Small ( 245x90 )',
            'slug'      => 'top-sm',
            'location'  => 'top',
            'sizeW'     => '',
            'sizeH'     => ''
        ));

        AdType::create(array(
            'name'      => 'Top Long ( 730x90 )',
            'slug'      => 'top-long',
            'location'  => 'top',
            'sizeW'     => '',
            'sizeH'     => ''
        ));
    }

}