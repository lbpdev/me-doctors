<?php

use MEDoctors\Models\Article;
use MEDoctors\Models\Channel;
use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
        \DB::table('articles')->delete();

		$faker = Faker\Factory::create();
        $channelIds = Channel::lists('id');

        for($x=0;$x<100;$x++)
        {
        	$title = $faker->sentence;
            $created_at = $faker->dateTimeThisYear;
            $date_posted = $faker->dateTimeThisYear;

			Article::forceCreate([
                'title'      => $title,
                'subtitle'    => $faker->sentence(),
                'channel_id' => $faker->randomElement($channelIds),
                'content'    => $faker->paragraph(12),
                'slug'       => str_slug($title),
                'created_at' => $created_at,
                'updated_at' => $created_at,
                'date_posted' => $date_posted
			]);
        }
	}

}
