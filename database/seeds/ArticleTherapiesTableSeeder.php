<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\Article;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\ArticleTherapy;

class ArticleTherapiesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('article_therapies')->delete();
        $faker = Faker\Factory::create();

        $articleIds = Article::lists('id');
        $therapyIds = Therapy::lists('id');

        foreach ($articleIds as $index)
        {
            ArticleTherapy::forceCreate([
                'article_id' => $index,
                'therapy_id' => $faker->randomElement($therapyIds)
            ]);
        }
	}

}
