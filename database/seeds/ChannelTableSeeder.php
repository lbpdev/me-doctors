<?php

use MEDoctors\Models\Channel;
use Illuminate\Database\Seeder;

class ChannelTableSeeder extends Seeder
{
    /**
     * Channels for the discussion.
     *
     * @var array
     */
    protected $channels = [
        ['name' => 'Panel', 'slug' => 'panel'],
        ['name' => 'Doctor', 'slug' => 'doctor'],
        ['name' => 'Patient', 'slug' => 'patient']
    ];

    public function run()
    {
        foreach ($this->channels as $channel)
        {
            Channel::create([
                'name' => $channel['name'],
                'slug' => $channel['slug']
            ]);
        }
    }
}
