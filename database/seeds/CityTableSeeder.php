<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory as TestDummy;

class CityTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('cities')->delete();
        DB::unprepared(file_get_contents(__DIR__ . '/cities.sql'));
    }

}