<?php

use MEDoctors\Models\User;
use MEDoctors\Models\Comment;
use Illuminate\Database\Seeder;
use MEDoctors\Models\Discussion;

class CommentTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		$discussionIds = Discussion::lists('id');
		$userIds = User::lists('id');
        $status = 1;

		foreach ($discussionIds as $discussionId)
		{
            foreach (range(1, 6) as $index){
                Comment::forceCreate([
                   'content' => $faker->paragraph,
                   'user_id' => $faker->randomElement($userIds),
                   'discussion_id' => $discussionId,
                    'status' => $status
                ]);
            }
		}
	}

}
