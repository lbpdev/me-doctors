<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->delete();
        DB::unprepared(file_get_contents(__DIR__ . '/countries.sql'));
    }
}
