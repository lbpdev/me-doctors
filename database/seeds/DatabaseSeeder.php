<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * The tables to seed.
	 * 
	 * @var array
	 */
	protected $tables = [
		'users',
         'cities',
         'countries',
         'languages',
		'roles',
		'role_user',
		'therapies',
		'articles',
        'channels',
        'discussions',
        'comments',
        'articles',
        'article_therapies',
        'events',
        'profiles',
        'field_metas',
        'surveys',
        'survey_therapies',
        'survey_items',
        'survey_item_choices',
        'survey_entries',
        'polls',
        'poll_items',
        'poll_votes',
        'options',
        'hospitals',
        'hospital_user',
        'hospital_facilities',
        'locations',
        'videos',
        'email_providers',
        'premium_services',
	];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->cleanDatabase();

		$this->call('UserTableSeeder');
        $this->call('CountryTableSeeder');
		$this->call('CityTableSeeder');
		$this->call('RoleTableSeeder');
        $this->call('RoleUserTableSeeder');
		$this->call('TherapyTableSeeder');
        $this->call('ChannelTableSeeder');
		$this->call('ProfileTableSeeder');
		$this->call('TherapyUserTableSeeder');
        $this->call('DiscussionTableSeeder');
		$this->call('PollTableSeeder');
		$this->call('PollItemTableSeeder');
        $this->call('SurveyTableSeeder');
        $this->call('SurveyTherapiesTableSeeder');
        $this->call('SurveyItemTableSeeder');
        $this->call('SurveyItemChoiceTableSeeder');
		$this->call('CommentTableSeeder');
		$this->call('EventTableSeeder');
		$this->call('ArticleTableSeeder');
        $this->call('ArticleTherapiesTableSeeder');
		$this->call('LanguagesTableSeeder');
		$this->call('OptionTableSeeder');
        $this->call('HospitalTableSeeder');
        $this->call('HospitalUserTableSeeder');
        $this->call('HospitalFacilitiesTableSeeder');
        $this->call('DoctorEmailsWhitelistTableSeeder');
        $this->call('EmailDomainsBlacklistTableSeeder');
//        $this->call('LocationTableSeeder');
        $this->call('EmailDomainsWhitelistTableSeeder');
        $this->call('VideosTableSeeder');
        $this->call('EmailProviderTableSeeder');
        $this->call('AdTypeTableSeeder');
        $this->call('PremiumServiceTableSeeder');
	}

	/**
	 * Empty the tables before running the seeder classes.
	 * 
	 * @return void
	 */
	private function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach ($this->tables as $tableName)
        {
            DB::table($tableName)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
