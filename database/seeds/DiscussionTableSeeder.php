<?php

use MEDoctors\Models\User;
use MEDoctors\Models\Channel;
use MEDoctors\Models\Therapy;
use Illuminate\Database\Seeder;
use MEDoctors\Models\Discussion;

class DiscussionTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();
		$userIds = User::lists('id');
		$therapyIds = Therapy::lists('id');
		$channelIds = Channel::lists('id');
		$status = 1;

		foreach (range(1, 50) as $index)
		{
			$title = $faker->sentence;

			Discussion::forceCreate([
			    'author_id' => $faker->randomElement($userIds),
			    'channel_id' => $faker->randomElement($channelIds),
			    'therapy_id' => $faker->randomElement($therapyIds),
			    'title' => $title,
			    'content' => $faker->paragraph,
			    'slug' => str_slug($title),
			    'status' => $status
			]);
		}
	}	

}
