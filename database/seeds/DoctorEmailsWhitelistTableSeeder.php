<?php

use Illuminate\Database\Seeder;

class DoctorEmailsWhitelistTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('doctor_emails_whitelist')->truncate();
        
        DB::unprepared(file_get_contents(__DIR__ . '/doctor_emails_whitelist.sql'));
    }
}
