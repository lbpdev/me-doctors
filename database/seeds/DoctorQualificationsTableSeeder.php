<?php

use Illuminate\Database\Seeder;

class DoctorQualificationsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('doctor_qualifications')->delete();
        
		\DB::table('doctor_qualifications')->insert(array (s));
	}

}
