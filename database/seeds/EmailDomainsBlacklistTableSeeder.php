<?php

use Illuminate\Database\Seeder;

class EmailDomainsBlacklistTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('email_domains_blacklist')->truncate();
        
        DB::unprepared(file_get_contents(__DIR__ . '/email_domains_blacklist.sql'));
    }
}
