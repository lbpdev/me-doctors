<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class EmailDomainsWhitelistTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('email_domains_whitelist')->truncate();
        
        DB::unprepared(file_get_contents(__DIR__ . '/email_domains_whitelist.sql'));
    }
}
