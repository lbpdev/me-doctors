<?php

use MEDoctors\Models\Event;
use MEDoctors\Models\Country;
use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();
        $countries = Country::all();

        foreach (range(1, 50) as $index)
        {
            $title = $faker->sentence;
            $country = $countries->random();
            $city = $country->cities->random();

            $countryName = $country->name;
            $cityName = ($city) ? $city->name : $faker->city;

            Event::create([
                'event_date' => $faker->dateTimeThisYear('2015-12-31'),
                'title'      => $title,
                'country'    => $countryName,
                'city'       => $cityName,
                'content'    => $faker->paragraph,
                'location'   => "{$countryName}, {$cityName}",
                'slug'       => str_slug($title)
            ]);
        }
	}

}
