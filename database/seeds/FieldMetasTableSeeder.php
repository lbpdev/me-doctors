<?php

use Illuminate\Database\Seeder;

class FieldMetasTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('field_metas')->delete();
        
		\DB::table('field_metas')->insert(array ());
	}

}
