<?php
use Illuminate\Database\Seeder;

class HospitalFacilitiesTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('hospital_facilities')->delete();

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Wifi',
                'slug' => 'wifi'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => '5 Star',
                'slug' => '5-star'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Cafe',
                'slug' => 'cafe'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Restaurant',
                'slug' => 'restaurant'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Florist',
                'slug' => 'florist'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Pharmacy',
                'slug' => 'pharmacy'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Concierge',
                'slug' => 'concierge'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'ER',
                'slug' => 'er'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Gift shop',
                'slug' => 'gift-shop'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Valet',
                'slug' => 'valet'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Parking',
                'slug' => 'parking'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'VIP',
                'slug' => 'vip'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Private',
                'slug' => 'private'
            )
        );

        DB::table('hospital_facilities')->insert(
            array(
                'name' => 'Government',
                'slug' => 'government'
            )
        );
    }
}
