<?php
use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\HospitalTherapy;

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class HospitalTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('hospitals')->delete();

        $hospitals = TestDummy::times(100)->create('MEDoctors\Models\Hospital');

        $faker = Faker\Factory::create();
        $therapyIds = Therapy::lists('id');
        $hospitalIds = Hospital::lists('id');

        foreach ($hospitals as $hospital)
        {
            foreach (range(1, 5) as $index)
            {
                HospitalTherapy::forceCreate([
                    'therapy_id' => $faker->randomElement($therapyIds),
                    'hospital_id' => $faker->randomElement($hospitalIds)
                ]);
            }
        }
    }
}
