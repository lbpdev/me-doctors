<?php

use MEDoctors\Models\Role;
use MEDoctors\Models\Hospital;
use MEDoctors\Models\HospitalUser;

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class HospitalUserTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $doctorIds = Role::doctors()->lists('id');
        $hospitalIds = Hospital::lists('id');

        foreach ($doctorIds as $doctorId)
        {
            HospitalUser::forceCreate([
                'user_id' => $doctorId,
                'hospital_id' => $faker->randomElement($hospitalIds)
            ]);
        }
    }

}
