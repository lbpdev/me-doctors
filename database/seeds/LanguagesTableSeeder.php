<?php

use Illuminate\Database\Seeder;


class LanguagesTableSeeder extends Seeder {

    public function run()
    {
        \DB::table('languages')->delete();

        DB::table('languages')->insert(array('name' => 'English', 'code' => 'en'));
        DB::table('languages')->insert(array('name' => 'German', 'code' => 'de'));
        DB::table('languages')->insert(array('name' => 'French', 'code' => 'fr'));
        DB::table('languages')->insert(array('name' => 'Dutch', 'code' => 'nl'));
        DB::table('languages')->insert(array('name' => 'Italian', 'code' => 'it'));
        DB::table('languages')->insert(array('name' => 'Spanish', 'code' => 'es'));
        DB::table('languages')->insert(array('name' => 'Polish', 'code' => 'pl'));
        DB::table('languages')->insert(array('name' => 'Russian', 'code' => 'ru'));
        DB::table('languages')->insert(array('name' => 'Japanese', 'code' => 'ja'));
        DB::table('languages')->insert(array('name' => 'Portuguese', 'code' => 'pt'));
        DB::table('languages')->insert(array('name' => 'Swedish', 'code' => 'sv'));
        DB::table('languages')->insert(array('name' => 'Chinese', 'code' => 'zh'));
        DB::table('languages')->insert(array('name' => 'Catalan', 'code' => 'ca'));
        DB::table('languages')->insert(array('name' => 'Ukrainian', 'code' => 'uk'));
        DB::table('languages')->insert(array('name' => 'Norwegian (Bokmål)', 'code' => 'no'));
        DB::table('languages')->insert(array('name' => 'Finnish', 'code' => 'fi'));
        DB::table('languages')->insert(array('name' => 'Vietnamese', 'code' => 'vi'));
        DB::table('languages')->insert(array('name' => 'Czech', 'code' => 'cs'));
        DB::table('languages')->insert(array('name' => 'Hungarian', 'code' => 'hu'));
        DB::table('languages')->insert(array('name' => 'Korean', 'code' => 'ko'));
        DB::table('languages')->insert(array('name' => 'Indonesian', 'code' => 'id'));
        DB::table('languages')->insert(array('name' => 'Turkish', 'code' => 'tr'));
        DB::table('languages')->insert(array('name' => 'Romanian', 'code' => 'ro'));
        DB::table('languages')->insert(array('name' => 'Persian', 'code' => 'fa'));
        DB::table('languages')->insert(array('name' => 'Arabic', 'code' => 'ar'));
        DB::table('languages')->insert(array('name' => 'Danish', 'code' => 'da'));
        DB::table('languages')->insert(array('name' => 'Esperanto', 'code' => 'eo'));
        DB::table('languages')->insert(array('name' => 'Serbian', 'code' => 'sr'));
        DB::table('languages')->insert(array('name' => 'Lithuanian', 'code' => 'lt'));
        DB::table('languages')->insert(array('name' => 'Slovak', 'code' => 'sk'));
        DB::table('languages')->insert(array('name' => 'Malay', 'code' => 'ms'));
        DB::table('languages')->insert(array('name' => 'Hebrew', 'code' => 'he'));
        DB::table('languages')->insert(array('name' => 'Bulgarian', 'code' => 'bg'));
        DB::table('languages')->insert(array('name' => 'Slovenian', 'code' => 'sl'));
        DB::table('languages')->insert(array('name' => 'Volapük', 'code' => 'vo'));
        DB::table('languages')->insert(array('name' => 'Kazakh', 'code' => 'kk'));
        DB::table('languages')->insert(array('name' => 'Waray-Waray', 'code' => 'war'));
        DB::table('languages')->insert(array('name' => 'Basque', 'code' => 'eu'));
        DB::table('languages')->insert(array('name' => 'Croatian', 'code' => 'hr'));
        DB::table('languages')->insert(array('name' => 'Hindi', 'code' => 'hi'));
        DB::table('languages')->insert(array('name' => 'Estonian', 'code' => 'et'));
        DB::table('languages')->insert(array('name' => 'Azerbaijani', 'code' => 'az'));
        DB::table('languages')->insert(array('name' => 'Galician', 'code' => 'gl'));
        DB::table('languages')->insert(array('name' => 'Simple English', 'code' => 'simple'));
        DB::table('languages')->insert(array('name' => 'Norwegian (Nynorsk)', 'code' => 'nn'));
        DB::table('languages')->insert(array('name' => 'Thai', 'code' => 'th'));
        DB::table('languages')->insert(array('name' => 'Newar / Nepal Bhasa', 'code' => 'new'));
        DB::table('languages')->insert(array('name' => 'Greek', 'code' => 'el'));
        DB::table('languages')->insert(array('name' => 'Aromanian', 'code' => 'roa-rup'));
        DB::table('languages')->insert(array('name' => 'Latin', 'code' => 'la'));
        DB::table('languages')->insert(array('name' => 'Occitan', 'code' => 'oc'));
        DB::table('languages')->insert(array('name' => 'Tagalog', 'code' => 'tl'));
        DB::table('languages')->insert(array('name' => 'Haitian', 'code' => 'ht'));
        DB::table('languages')->insert(array('name' => 'Macedonian', 'code' => 'mk'));
        DB::table('languages')->insert(array('name' => 'Georgian', 'code' => 'ka'));
        DB::table('languages')->insert(array('name' => 'Serbo-Croatian', 'code' => 'sh'));
        DB::table('languages')->insert(array('name' => 'Telugu', 'code' => 'te'));
        DB::table('languages')->insert(array('name' => 'Piedmontese', 'code' => 'pms'));
        DB::table('languages')->insert(array('name' => 'Cebuano', 'code' => 'ceb'));
        DB::table('languages')->insert(array('name' => 'Tamil', 'code' => 'ta'));
        DB::table('languages')->insert(array('name' => 'Belarusian (Taraškievica)', 'code' => 'be-x-old'));
        DB::table('languages')->insert(array('name' => 'Breton', 'code' => 'br'));
        DB::table('languages')->insert(array('name' => 'Latvian', 'code' => 'lv'));
        DB::table('languages')->insert(array('name' => 'Javanese', 'code' => 'jv'));
        DB::table('languages')->insert(array('name' => 'Albanian', 'code' => 'sq'));
        DB::table('languages')->insert(array('name' => 'Belarusian', 'code' => 'be'));
        DB::table('languages')->insert(array('name' => 'Marathi', 'code' => 'mr'));
        DB::table('languages')->insert(array('name' => 'Welsh', 'code' => 'cy'));
        DB::table('languages')->insert(array('name' => 'Luxembourgish', 'code' => 'lb'));
        DB::table('languages')->insert(array('name' => 'Icelandic', 'code' => 'is'));
        DB::table('languages')->insert(array('name' => 'Bosnian', 'code' => 'bs'));
        DB::table('languages')->insert(array('name' => 'Yoruba', 'code' => 'yo'));
        DB::table('languages')->insert(array('name' => 'Malagasy', 'code' => 'mg'));
        DB::table('languages')->insert(array('name' => 'Aragonese', 'code' => 'an'));
        DB::table('languages')->insert(array('name' => 'Bishnupriya Manipuri', 'code' => 'bpy'));
        DB::table('languages')->insert(array('name' => 'Lombard', 'code' => 'lmo'));
        DB::table('languages')->insert(array('name' => 'West Frisian', 'code' => 'fy'));
        DB::table('languages')->insert(array('name' => 'Bengali', 'code' => 'bn'));
        DB::table('languages')->insert(array('name' => 'Ido', 'code' => 'io'));
        DB::table('languages')->insert(array('name' => 'Swahili', 'code' => 'sw'));
        DB::table('languages')->insert(array('name' => 'Gujarati', 'code' => 'gu'));
        DB::table('languages')->insert(array('name' => 'Malayalam', 'code' => 'ml'));
        DB::table('languages')->insert(array('name' => 'Western Panjabi', 'code' => 'pnb'));
        DB::table('languages')->insert(array('name' => 'Afrikaans', 'code' => 'af'));
        DB::table('languages')->insert(array('name' => 'Low Saxon', 'code' => 'nds'));
        DB::table('languages')->insert(array('name' => 'Sicilian', 'code' => 'scn'));
        DB::table('languages')->insert(array('name' => 'Urdu', 'code' => 'ur'));
        DB::table('languages')->insert(array('name' => 'Kurdish', 'code' => 'ku'));
        DB::table('languages')->insert(array('name' => 'Cantonese', 'code' => 'zh-yue'));
        DB::table('languages')->insert(array('name' => 'Armenian', 'code' => 'hy'));
        DB::table('languages')->insert(array('name' => 'Quechua', 'code' => 'qu'));
        DB::table('languages')->insert(array('name' => 'Sundanese', 'code' => 'su'));
        DB::table('languages')->insert(array('name' => 'Nepali', 'code' => 'ne'));
        DB::table('languages')->insert(array('name' => 'Zazaki', 'code' => 'diq'));
        DB::table('languages')->insert(array('name' => 'Asturian', 'code' => 'ast'));
        DB::table('languages')->insert(array('name' => 'Tatar', 'code' => 'tt'));
        DB::table('languages')->insert(array('name' => 'Neapolitan', 'code' => 'nap'));
        DB::table('languages')->insert(array('name' => 'Irish', 'code' => 'ga'));
        DB::table('languages')->insert(array('name' => 'Chuvash', 'code' => 'cv'));
        DB::table('languages')->insert(array('name' => 'Samogitian', 'code' => 'bat-smg'));
        DB::table('languages')->insert(array('name' => 'Walloon', 'code' => 'wa'));
        DB::table('languages')->insert(array('name' => 'Amharic', 'code' => 'am'));
        DB::table('languages')->insert(array('name' => 'Kannada', 'code' => 'kn'));
        DB::table('languages')->insert(array('name' => 'Alemannic', 'code' => 'als'));
        DB::table('languages')->insert(array('name' => 'Buginese', 'code' => 'bug'));
        DB::table('languages')->insert(array('name' => 'Burmese', 'code' => 'my'));
        DB::table('languages')->insert(array('name' => 'Interlingua', 'code' => 'ia'));

    }

}
