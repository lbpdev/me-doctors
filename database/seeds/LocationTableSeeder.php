<?php
use MEDoctors\Models\Country;
use MEDoctors\Models\City;
use MEDoctors\Models\Hospital;

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class LocationTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('locations')->delete();

        $hospitals = Hospital::get();

        foreach ($hospitals as $hospital)
        {
            $location = TestDummy::build('MEDoctors\Models\Location');
            $chosen_country = Country::where('name',$location->country)->first();
            $random_city = City::where('country_code', $chosen_country->code)->get()->random(1);
            $location->city = $random_city->name;

            $hospital->location()->save($location);
        }
    }
}
