<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;
use MEDoctors\Models\User;

class MigoSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$user = User::create([
		    'fname'       => 'Migo',
		    'lname'       => 'Badelles',
		    'username'    => 'migo',
		    'email'       => 'migo@leadingbrands.me',
		    'password'    => 'LB@JLTjbc1502',
		    'status'      => 1,
		    'verified'    => 1,
		    'designation' => 'Admin'
		]);

		$user->roles()->sync(array('role_id'=>1));
	}

}

