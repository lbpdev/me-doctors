<?php
use MEDoctors\Models\User;

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class NewAdminSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(array(
            'username' => 'admin2',
            'email' => 'gene@leadingbrands.me',
            'password' => 'secret',
            'fname' => 'Gene',
            'lname' => 'Ellorin',
            'designation' => 'Admin',
            'status' => 0,
            'verified' => 1
        ));

        $user->roles()->sync([1]);
    }

}
