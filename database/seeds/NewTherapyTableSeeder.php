<?php

use MEDoctors\Models\Therapy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class NewTherapyTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
        // DB::statement('SET FOREIGN_KEY_CHECKS=0');

        // Therapy::truncate();


    $therapies = [
        [ 
            'name' => 'Allergies and Immunology'
        ],
        [
            'name' => 'Anaesthesiology'
        ],
        [
            'name' => 'Cardiology'
        ],
        [
            'name' => 'Cosmetic Surgery'
        ],
        [
            'name' => 'Dentistry'
        ],
        [
            'name' => 'Dermatology'
        ],
        [
            'name' => 'Endocrinology'
        ],
        [
            'name' => 'Family Medicine'
        ],
        [
            'name' => 'Gastroenterology'
        ],
        [
            'name' => 'General Practice'
        ],
        [
            'name' => 'General Surgery'
        ],
        [
            'name' => 'Geriatrics'
        ],
        [
            'name' => 'Haematology'
        ],
        [
            'name' => 'Hepatology'
        ],
        [
            'name' => 'Infectious Diseases'
        ],
        [
            'name' => 'Internal Medicine (Internists)'
        ],
        [
            'name' => 'Laboratory Medicine'
        ],
        [
            'name' => 'Maxillo-facial Surgery'
        ],
        [
            'name' => 'Nephrology'
        ],
        [
            'name' => 'Neurology'
        ],
        [
            'name' => 'Neurosurgery'
        ],
        [
            'name' => 'Nuclear Medicine'
        ],
        [
            'name' => 'Nutrition and Diet'
        ],
        [
            'name' => 'Obstetrics and Gynaecology'
        ],
        [
            'name' => 'Occupational Medicine'
        ],
        [
            'name' => 'Oncology'
        ],
        [
            'name' => 'Ophthalmology'
        ],
        [
            'name' => 'Orthopaedics'
        ],
        [
            'name' => 'Otorhinolaryngology (ENT)'
        ],
        [
            'name' => 'Paediatrics and Neonatology'
        ],
        [
            'name' => 'Pain Management'
        ],
        [
            'name' => 'Pathology'
        ],
        [
            'name' => 'Physical Therapy'
        ],
        [
            'name' => 'Podiatry'
        ],
        [
            'name' => 'Pulmonology'
        ],
        [
            'name' => 'Psychiatry and Psychology'
        ],
        [
            'name' => 'Radiology and Radiotherapy'
        ],
        [
            'name' => 'Rheumatology'
        ],
        [
            'name' => 'Trauma and Emergency'
        ],
        [
            'name' => 'Urology'
        ],
        [
            'name' => 'Vascular Surgery'
        ],
    ];

        foreach ($therapies as $therapy)
        {
            $therapySlugExists = Therapy::where('slug',str_slug($therapy['name']))->count();
            $therapyNameExists = Therapy::where('name',$therapy['name'])->count();

            if(!$therapySlugExists && !$therapyNameExists){
                Therapy::create([
                    'name' => $therapy['name'],
                    'slug' => str_slug($therapy['name'])
                ]);
            }
        }

        // DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}

}
