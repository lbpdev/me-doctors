<?php

use MEDoctors\Models\Article;
use MEDoctors\Models\Poll;
use MEDoctors\Models\Survey;
use MEDoctors\Models\Channel;
use MEDoctors\Models\Option;
use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $patientArticleIds = Article::where('channel_id',3)->lists('id');
        $doctorArticleIds = Article::where('channel_id',2)->lists('id');
        $pollIds = Poll::lists('id');

        /////////////////////////////////// PATIENTS ////////////////////////////////////

        Option::create([
            'name'       => "Doctor Featured Article",
            'value'      => $faker->randomElement($doctorArticleIds),
            'slug'       => str_slug("Doctor Featured Article")
        ]);

        Option::create([
            'name'       => "Doctor Active Poll",
            'value'      => $faker->randomElement($pollIds),
            'slug'       => str_slug("Doctor Active Poll")
        ]);

        /////////////////////////////////// DOCTORS ////////////////////////////////////

        Option::create([
            'name'       => "Patient Featured Article",
            'value'      => $faker->randomElement($patientArticleIds),
            'slug'       => str_slug("Patient Featured Article")
        ]);

        Option::create([
            'name'       => "Patient Active Poll",
            'value'      => $faker->randomElement($pollIds),
            'slug'       => str_slug('Patient Active Poll')
        ]);

    }

}
