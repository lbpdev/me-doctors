<?php

use Illuminate\Database\Seeder;

class PhoneNumbersTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('phone_numbers')->delete();
        
		\DB::table('phone_numbers')->insert(array ());
	}

}
