<?php

use MEDoctors\Models\Poll;
use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class PollItemTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();
		$polls = Poll::all();

		foreach ($polls as $poll)
		{
			$items = [];
			
			foreach (range(1, $faker->numberBetween(2, 5)) as $index)
			{
				$items[] = TestDummy::build('MEDoctors\Models\PollItem');
			}

		    $poll->items()->saveMany($items);
		}
	}

}
