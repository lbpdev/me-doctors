<?php

use Illuminate\Database\Seeder;

class PollVotesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('poll_votes')->delete();
        
		\DB::table('poll_votes')->insert(array ());
	}

}
