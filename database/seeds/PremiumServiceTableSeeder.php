<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\PremiumService;

class PremiumServiceTableSeeder extends Seeder {

    public function run()
    {

        DB::statement("SET foreign_key_checks = 0");
        PremiumService::truncate();
        DB::statement("SET foreign_key_checks = 1");

        PremiumService::create(array(
            'name'      => 'Platinum Account',
            'slug'      => 'platinum-account',
            'description'  => 'Doctor platinum account',
            'interval'     => 'yearly',
            'price'     => '50'
        ));

        PremiumService::create(array(
            'name'      => 'Top Banner - Long (730x90)',
            'slug'      => 'top-banner-long',
            'description'  => 'Top Banner - Long (730x90)',
            'interval'     => 'monthly',
            'price'     => '10'
        ));

        PremiumService::create(array(
            'name'      => 'Top Banner - Short (245x90)',
            'slug'      => 'top-banner-short',
            'description'  => 'Top Banner - Short (245x90)',
            'interval'     => 'monthly',
            'price'     => '7'
        ));

        PremiumService::create(array(
            'name'      => 'Side Banner - Long (230x500)',
            'slug'      => 'side-banner-long',
            'description'  => 'Side Banner - Long (230x500)',
            'interval'     => 'monthly',
            'price'     => '8'
        ));

        PremiumService::create(array(
            'name'      => 'Side Banner - Short (230x230)',
            'slug'      => 'side-banner-short',
            'description'  => 'Side Banner - Short (230x230)',
            'interval'     => 'monthly',
            'price'     => '6'
        ));

        PremiumService::create(array(
            'name'      => 'Video Ad',
            'slug'      => 'video-ad',
            'description'  => 'Video Ad',
            'interval'     => 'monthly',
            'price'     => '15'
        ));

        PremiumService::create(array(
            'name'      => 'Survey Sponsorship',
            'slug'      => 'survey-sponsorship',
            'description'  => 'Survey Sponsorship',
            'interval'     => 'monthly',
            'price'     => '6'
        ));

        PremiumService::create(array(
            'name'      => 'Article Sponsorship',
            'slug'      => 'article-sponsorship',
            'description'  => 'Article Sponsorship',
            'interval'     => 'monthly',
            'price'     => '6'
        ));

        PremiumService::create(array(
            'name'      => 'Discussion Sponsorship',
            'slug'      => 'discussion-sponsorship',
            'description'  => 'Discussion Sponsorship',
            'interval'     => 'monthly',
            'price'     => '6'
        ));
    }

}

