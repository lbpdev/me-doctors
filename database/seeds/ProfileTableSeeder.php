<?php

use MEDoctors\Models\User;
use MEDoctors\Models\Profile;
use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();
		$users = User::all();

		foreach ($users as $user)
		{
			$profile = new Profile;
			$profile->bio = $faker->paragraph;

		    $user->profile()->save($profile);
		}
	}

}
