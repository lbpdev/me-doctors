<?php

use MEDoctors\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {

	/**
	 * User roles.
	 *
	 * @var array
	 */
	protected $roles = [
		'Administrator',
		'Doctor',
		'Patient'
	];

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		foreach ($this->roles as $role)
		{
			Role::create(['name' => $role]);
		}
	}

}
