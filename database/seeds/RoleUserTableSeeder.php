<?php

use MEDoctors\Models\Role;
use MEDoctors\Models\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder {

	public function run()
    {
        $users = User::all();

        foreach ($users as $index=>$user)
        {
            $roleId = $this->generateRoleId($user);
            $user->roles()->attach([$roleId]);
        }
    }

    /**
     * @param $user
     *
     * @return inte
     */
    private function generateRoleId($user)
    {
        $faker = Faker\Factory::create();
        $roles = Role::all();

        switch ($user->username)
        {
            case 'admin':
                return $roles->where('name', 'Administrator')->first()->id;
                break;
            case 'patient':
                return $roles->where('name', 'Patient')->first()->id;
                break;
            case 'doctor':
                return $roles->where('name', 'Doctor')->first()->id;
                break;
            default:
//                return $faker->randomElement($roles->lists('id'));
                return $roles->where('name', 'Doctor')->first()->id;
                break;
        }
	}

}
