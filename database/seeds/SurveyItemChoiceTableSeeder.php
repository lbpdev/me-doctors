<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\SurveyItem;
use Laracasts\TestDummy\Factory as TestDummy;

class SurveyItemChoiceTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $surveyItems = SurveyItem::all();

        foreach ($surveyItems as $surveyItem)
        {
            $choices = [];
            
            foreach (range(1, $faker->numberBetween(2, 4)) as $index)
            {
                $choices[] = TestDummy::build('MEDoctors\Models\SurveyItemChoice');
            }

            $surveyItem->choices()->saveMany($choices);
        }
    }
}
