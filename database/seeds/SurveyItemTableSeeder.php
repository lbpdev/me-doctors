<?php

use MEDoctors\Models\Survey;
use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class SurveyItemTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $surveys = Survey::all();

        foreach ($surveys as $survey)
        {
            $items = [];
            
            foreach (range(1, $faker->numberBetween(2, 4)) as $index)
            {
                $items[] = TestDummy::build('MEDoctors\Models\SurveyItem');
            }

            $survey->items()->saveMany($items);
        }
    }
}
