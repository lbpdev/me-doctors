<?php

use MEDoctors\Models\Survey;
use MEDoctors\Models\Channel;

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SurveyTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('surveys')->delete();

        $faker = Faker\Factory::create();
        $channelIds = Channel::lists('id');

        foreach (range(1, 20) as $index)
        {
            $title = $faker->sentence;
            $created_at = $faker->dateTimeThisYear;

            Survey::forceCreate([
                'title'      => $title,
                'channel_id' => $faker->randomElement($channelIds),
                'description'    => $faker->paragraph,
                'slug'       => str_slug($title),
                'created_at' => $created_at,
                'updated_at' => $created_at
            ]);
        }
    }
}
