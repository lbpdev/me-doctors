<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\Survey;
use MEDoctors\Models\Therapy;
use MEDoctors\Models\SurveyTherapy;

class SurveyTherapiesTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('survey_therapies')->delete();
        $faker = Faker\Factory::create();

        $surveyIds = Survey::lists('id');
        $therapyIds = Therapy::lists('id');

        foreach ($surveyIds as $index)
        {
            SurveyTherapy::forceCreate([
                'survey_id' => $index,
                'therapy_id' => $faker->randomElement($therapyIds)
            ]);
        }
    }

}
