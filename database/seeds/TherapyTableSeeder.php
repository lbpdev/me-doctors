<?php

use MEDoctors\Models\Therapy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TherapyTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Therapy::truncate();

        foreach (config('middleeastdoctor.therapies') as $therapy)
        {
            Therapy::create([
                'name' => $therapy['name'],
                'slug' => str_slug($therapy['name'])
            ]);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}

}
