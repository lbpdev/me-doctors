<?php

use Illuminate\Database\Seeder;

use MEDoctors\Models\Therapy;
use MEDoctors\Models\User;
use MEDoctors\Models\TherapyUser;
use MEDoctors\Models\Role;

class TherapyUserTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('therapy_user')->delete();

        $faker = Faker\Factory::create();

        $doctorIds = Role::doctors()->lists('id');
        $therapyIds = Therapy::lists('id');

        foreach ($doctorIds as $index)
        {
            TherapyUser::forceCreate([
                'user_id' => $index,
                'therapy_id' => $faker->randomElement($therapyIds)
            ]);
        }
    }

}

