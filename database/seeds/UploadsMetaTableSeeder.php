<?php

use Illuminate\Database\Seeder;

class UploadsMetaTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('uploads_meta')->delete();
        
		\DB::table('uploads_meta')->insert(array (
			0 => 
			array (
				'id' => 1,
				'upload_id' => 45,
				'meta_for' => 'discussions',
				'meta_id' => 36,
				'created_at' => '2015-05-28 10:07:35',
				'updated_at' => '2015-05-28 10:07:35',
			),
			1 => 
			array (
				'id' => 2,
				'upload_id' => 46,
				'meta_for' => 'discussions',
				'meta_id' => 37,
				'created_at' => '2015-05-28 10:09:28',
				'updated_at' => '2015-05-28 10:09:28',
			),
			2 => 
			array (
				'id' => 3,
				'upload_id' => 47,
				'meta_for' => 'discussions',
				'meta_id' => 38,
				'created_at' => '2015-05-28 10:40:40',
				'updated_at' => '2015-05-28 10:40:40',
			),
			3 => 
			array (
				'id' => 4,
				'upload_id' => 48,
				'meta_for' => 'discussions',
				'meta_id' => 39,
				'created_at' => '2015-05-28 10:44:26',
				'updated_at' => '2015-05-28 10:44:26',
			),
			4 => 
			array (
				'id' => 5,
				'upload_id' => 50,
				'meta_for' => 'comments',
				'meta_id' => 5,
				'created_at' => '2015-05-28 11:01:04',
				'updated_at' => '2015-05-28 11:01:04',
			),
			5 => 
			array (
				'id' => 6,
				'upload_id' => 51,
				'meta_for' => 'comments',
				'meta_id' => 9,
				'created_at' => '2015-05-28 11:22:19',
				'updated_at' => '2015-05-28 11:22:19',
			),
			6 => 
			array (
				'id' => 7,
				'upload_id' => 52,
				'meta_for' => 'comments',
				'meta_id' => 10,
				'created_at' => '2015-05-28 11:25:24',
				'updated_at' => '2015-05-28 11:25:24',
			),
			7 => 
			array (
				'id' => 8,
				'upload_id' => 53,
				'meta_for' => 'comments',
				'meta_id' => 11,
				'created_at' => '2015-05-28 13:50:45',
				'updated_at' => '2015-05-28 13:50:45',
			),
			8 => 
			array (
				'id' => 9,
				'upload_id' => 54,
				'meta_for' => 'comments',
				'meta_id' => 12,
				'created_at' => '2015-05-28 13:50:53',
				'updated_at' => '2015-05-28 13:50:53',
			),
			9 => 
			array (
				'id' => 10,
				'upload_id' => 55,
				'meta_for' => 'comments',
				'meta_id' => 16,
				'created_at' => '2015-06-02 05:41:04',
				'updated_at' => '2015-06-02 05:41:04',
			),
			10 => 
			array (
				'id' => 11,
				'upload_id' => 56,
				'meta_for' => 'discussions',
				'meta_id' => 44,
				'created_at' => '2015-06-02 07:16:03',
				'updated_at' => '2015-06-02 07:16:03',
			),
			11 => 
			array (
				'id' => 12,
				'upload_id' => 58,
				'meta_for' => 'comments',
				'meta_id' => 21,
				'created_at' => '2015-06-04 10:54:24',
				'updated_at' => '2015-06-04 10:54:24',
			),
			12 => 
			array (
				'id' => 13,
				'upload_id' => 59,
				'meta_for' => 'comments',
				'meta_id' => 22,
				'created_at' => '2015-06-04 10:57:24',
				'updated_at' => '2015-06-04 10:57:24',
			),
			13 => 
			array (
				'id' => 14,
				'upload_id' => 60,
				'meta_for' => 'comments',
				'meta_id' => 23,
				'created_at' => '2015-06-04 10:58:43',
				'updated_at' => '2015-06-04 10:58:43',
			),
			14 => 
			array (
				'id' => 15,
				'upload_id' => 61,
				'meta_for' => 'comments',
				'meta_id' => 24,
				'created_at' => '2015-06-04 11:01:21',
				'updated_at' => '2015-06-04 11:01:21',
			),
			15 => 
			array (
				'id' => 19,
				'upload_id' => 65,
				'meta_for' => 'comments',
				'meta_id' => 26,
				'created_at' => '2015-06-04 12:49:29',
				'updated_at' => '2015-06-04 12:49:29',
			),
			16 => 
			array (
				'id' => 20,
				'upload_id' => 66,
				'meta_for' => 'comments',
				'meta_id' => 30,
				'created_at' => '2015-06-04 13:15:36',
				'updated_at' => '2015-06-04 13:15:36',
			),
			17 => 
			array (
				'id' => 21,
				'upload_id' => 67,
				'meta_for' => 'discussions',
				'meta_id' => 52,
				'created_at' => '2015-06-04 13:24:33',
				'updated_at' => '2015-06-04 13:24:33',
			),
			18 => 
			array (
				'id' => 24,
				'upload_id' => 70,
				'meta_for' => 'comments',
				'meta_id' => 31,
				'created_at' => '2015-06-04 13:55:46',
				'updated_at' => '2015-06-04 13:55:46',
			),
		));
	}

}
