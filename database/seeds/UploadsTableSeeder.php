<?php

use Illuminate\Database\Seeder;

class UploadsTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('uploads')->delete();
        
		\DB::table('uploads')->insert(array (
			0 => 
			array (
				'id' => 1,
				'path' => 'avatars',
				'name' => 'Koala.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-19 08:19:14',
				'updated_at' => '2015-05-19 08:19:14',
			),
			1 => 
			array (
				'id' => 2,
				'path' => 'avatars',
				'name' => 'Penguins.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-19 11:12:51',
				'updated_at' => '2015-05-19 11:12:51',
			),
			2 => 
			array (
				'id' => 3,
				'path' => 'avatars',
				'name' => 'Tulips.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 05:37:36',
				'updated_at' => '2015-05-25 05:37:36',
			),
			3 => 
			array (
				'id' => 4,
				'path' => 'avatars',
				'name' => 'Chrysanthemum.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 05:57:38',
				'updated_at' => '2015-05-25 05:57:38',
			),
			4 => 
			array (
				'id' => 5,
				'path' => 'avatars',
				'name' => 'Hydrangeas.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 06:14:20',
				'updated_at' => '2015-05-25 06:14:20',
			),
			5 => 
			array (
				'id' => 6,
				'path' => 'avatars',
				'name' => 'Desert.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 13:03:24',
				'updated_at' => '2015-05-25 13:03:24',
			),
			6 => 
			array (
				'id' => 7,
				'path' => 'avatars',
				'name' => '8gnhR9an23MXCxC73Dtg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 13:28:38',
				'updated_at' => '2015-05-25 13:28:38',
			),
			7 => 
			array (
				'id' => 8,
				'path' => 'avatars',
				'name' => 'yk0IlCd0xrTFLY1khlus.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 13:29:36',
				'updated_at' => '2015-05-25 13:29:36',
			),
			8 => 
			array (
				'id' => 9,
				'path' => 'avatars',
				'name' => 'VZ5hgA4lktDsNhUSoUsX.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 13:31:16',
				'updated_at' => '2015-05-25 13:31:16',
			),
			9 => 
			array (
				'id' => 10,
				'path' => 'avatars',
				'name' => '5NXWrIjL0AVEmPBmUBKY.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-25 13:32:18',
				'updated_at' => '2015-05-25 13:32:18',
			),
			10 => 
			array (
				'id' => 11,
				'path' => 'avatars',
				'name' => 'tKcO7ghTExE7CdDlKkwkfwYzPYF9Rh.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:17:32',
				'updated_at' => '2015-05-26 11:17:32',
			),
			11 => 
			array (
				'id' => 12,
				'path' => 'avatars',
				'name' => '7IaCQ7jRLQeqkKjbxxK4chF0k8ruYj.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:19:34',
				'updated_at' => '2015-05-26 11:19:34',
			),
			12 => 
			array (
				'id' => 13,
				'path' => 'avatars',
				'name' => 'By8Tn7vnS1ZpFU17rp5QlJc5Djp7Tp.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:20:53',
				'updated_at' => '2015-05-26 11:20:53',
			),
			13 => 
			array (
				'id' => 14,
				'path' => 'avatars',
				'name' => 'frwTA0o7h2VnN03PB5dBOvyJ07zBg4.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:22:50',
				'updated_at' => '2015-05-26 11:22:50',
			),
			14 => 
			array (
				'id' => 15,
				'path' => 'avatars',
				'name' => '3dGHkmIaIqfCdefzTlBsbOt8Um7N1m.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:23:41',
				'updated_at' => '2015-05-26 11:23:41',
			),
			15 => 
			array (
				'id' => 16,
				'path' => 'avatars',
				'name' => '5wCYknORkaQKd5le0Ebthnp6gXgFHT.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:35:59',
				'updated_at' => '2015-05-26 11:35:59',
			),
			16 => 
			array (
				'id' => 17,
				'path' => 'avatars',
				'name' => 'LPlXgAElWuKT1KG7hM38DEcUEt1zLi.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:37:29',
				'updated_at' => '2015-05-26 11:37:29',
			),
			17 => 
			array (
				'id' => 18,
				'path' => 'avatars',
				'name' => '1rFcKfAuJadJxGciF11vKKt28cTIBY.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:39:52',
				'updated_at' => '2015-05-26 11:39:52',
			),
			18 => 
			array (
				'id' => 19,
				'path' => 'avatars',
				'name' => 'V6H0XUlt5cd2Diye5JxDLlgNJM1qFE.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:40:49',
				'updated_at' => '2015-05-26 11:40:49',
			),
			19 => 
			array (
				'id' => 20,
				'path' => 'avatars',
				'name' => 'Zg8I8jcZjocODsLo5kXpFsn140NLW3.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-26 11:43:01',
				'updated_at' => '2015-05-26 11:43:01',
			),
			20 => 
			array (
				'id' => 21,
				'path' => 'avatars',
				'name' => 'uSb3ttOnTed0QHpX6cPIXXHHyFstLk.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-26 11:43:22',
				'updated_at' => '2015-05-26 11:43:22',
			),
			21 => 
			array (
				'id' => 22,
				'path' => 'avatars',
				'name' => 's0ZDtPZPiyone73why1I6glYh2vDES.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-26 11:43:50',
				'updated_at' => '2015-05-26 11:43:50',
			),
			22 => 
			array (
				'id' => 23,
				'path' => 'avatars',
				'name' => 'en6Gs51sT8lEhe3wa9BCI7ayHQq9tc.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:58:48',
				'updated_at' => '2015-05-26 11:58:48',
			),
			23 => 
			array (
				'id' => 24,
				'path' => 'avatars',
				'name' => '6YUOw60kzOWwMuRaoQByyOqbdFOXKj.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 11:59:00',
				'updated_at' => '2015-05-26 11:59:00',
			),
			24 => 
			array (
				'id' => 25,
				'path' => 'avatars',
				'name' => 'w3t2uCQ5DF3lrQzm98qfhaB3WHhQ4s.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 12:00:00',
				'updated_at' => '2015-05-26 12:00:00',
			),
			25 => 
			array (
				'id' => 26,
				'path' => 'avatars',
				'name' => 'sOvXBfbxeYNaJ1JAh9UgPXlCRVihgu.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-26 12:02:52',
				'updated_at' => '2015-05-26 12:02:52',
			),
			26 => 
			array (
				'id' => 27,
				'path' => '',
				'name' => 'logo.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:13:48',
				'updated_at' => '2015-05-28 07:13:48',
			),
			27 => 
			array (
				'id' => 28,
				'path' => 'avatars',
				'name' => 'logo.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:15:13',
				'updated_at' => '2015-05-28 07:15:13',
			),
			28 => 
			array (
				'id' => 29,
				'path' => 'avatars',
				'name' => 'logo.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:17:11',
				'updated_at' => '2015-05-28 07:17:11',
			),
			29 => 
			array (
				'id' => 30,
				'path' => 'avatars',
				'name' => 'logo.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:17:57',
				'updated_at' => '2015-05-28 07:17:57',
			),
			30 => 
			array (
				'id' => 31,
				'path' => 'avatars',
				'name' => '0',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:20:49',
				'updated_at' => '2015-05-28 07:20:49',
			),
			31 => 
			array (
				'id' => 32,
				'path' => 'avatars',
				'name' => 'png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:30:23',
				'updated_at' => '2015-05-28 07:30:23',
			),
			32 => 
			array (
				'id' => 33,
				'path' => 'avatars',
				'name' => 'logopng',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:34:08',
				'updated_at' => '2015-05-28 07:34:08',
			),
			33 => 
			array (
				'id' => 34,
				'path' => 'avatars',
				'name' => 'logo1.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:35:07',
				'updated_at' => '2015-05-28 07:35:07',
			),
			34 => 
			array (
				'id' => 35,
				'path' => 'avatars',
				'name' => 'logo1.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:35:24',
				'updated_at' => '2015-05-28 07:35:24',
			),
			35 => 
			array (
				'id' => 36,
				'path' => 'avatars',
				'name' => 'logo1.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:36:04',
				'updated_at' => '2015-05-28 07:36:04',
			),
			36 => 
			array (
				'id' => 37,
				'path' => 'avatars',
				'name' => 'logo1.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:36:27',
				'updated_at' => '2015-05-28 07:36:27',
			),
			37 => 
			array (
				'id' => 38,
				'path' => 'avatars',
				'name' => 'logo.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:37:58',
				'updated_at' => '2015-05-28 07:37:58',
			),
			38 => 
			array (
				'id' => 39,
				'path' => 'avatars',
				'name' => 'logo2.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 07:45:42',
				'updated_at' => '2015-05-28 07:45:42',
			),
			39 => 
			array (
				'id' => 40,
				'path' => 'avatars',
				'name' => 'logo_2015-05-28_08_04_12.png',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/png',
				'created_at' => '2015-05-28 08:04:12',
				'updated_at' => '2015-05-28 08:04:12',
			),
			40 => 
			array (
				'id' => 41,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:01:53',
				'updated_at' => '2015-05-28 10:01:53',
			),
			41 => 
			array (
				'id' => 42,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_10_06_10.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:06:10',
				'updated_at' => '2015-05-28 10:06:10',
			),
			42 => 
			array (
				'id' => 43,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_10_06_21.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:06:21',
				'updated_at' => '2015-05-28 10:06:21',
			),
			43 => 
			array (
				'id' => 44,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_10_06_35.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:06:35',
				'updated_at' => '2015-05-28 10:06:35',
			),
			44 => 
			array (
				'id' => 45,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_10_07_35.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:07:35',
				'updated_at' => '2015-05-28 10:07:35',
			),
			45 => 
			array (
				'id' => 46,
				'path' => 'attachments',
				'name' => '3 HTML 5 specifications.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 10:09:28',
				'updated_at' => '2015-05-28 10:09:28',
			),
			46 => 
			array (
				'id' => 47,
				'path' => 'attachments',
				'name' => 'Hydrangeas_2015-05-28_10_40_39.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-28 10:40:40',
				'updated_at' => '2015-05-28 10:40:40',
			),
			47 => 
			array (
				'id' => 48,
				'path' => 'attachments',
				'name' => 'Tulips_2015-05-28_10_44_26.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-28 10:44:26',
				'updated_at' => '2015-05-28 10:44:26',
			),
			48 => 
			array (
				'id' => 49,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_11_00_41.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 11:00:41',
				'updated_at' => '2015-05-28 11:00:41',
			),
			49 => 
			array (
				'id' => 50,
				'path' => 'attachments',
				'name' => 'CodingGuidelines-GeneEllorin_2015-05-28_11_01_04.pdf',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'application/pdf',
				'created_at' => '2015-05-28 11:01:04',
				'updated_at' => '2015-05-28 11:01:04',
			),
			50 => 
			array (
				'id' => 51,
				'path' => 'attachments',
				'name' => 'Penguins_2015-05-28_11_22_19.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-28 11:22:19',
				'updated_at' => '2015-05-28 11:22:19',
			),
			51 => 
			array (
				'id' => 52,
				'path' => 'attachments',
				'name' => 'TestV2.mp4',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'video/mp4',
				'created_at' => '2015-05-28 11:25:23',
				'updated_at' => '2015-05-28 11:25:23',
			),
			52 => 
			array (
				'id' => 53,
				'path' => 'attachments',
				'name' => 'Penguins_2015-05-28_13_50_44.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-28 13:50:45',
				'updated_at' => '2015-05-28 13:50:45',
			),
			53 => 
			array (
				'id' => 54,
				'path' => 'attachments',
				'name' => 'Hydrangeas_2015-05-28_13_50_53.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-05-28 13:50:53',
				'updated_at' => '2015-05-28 13:50:53',
			),
			54 => 
			array (
				'id' => 55,
				'path' => 'attachments',
				'name' => 'Penguins_2015-06-02_05_41_02.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-02 05:41:04',
				'updated_at' => '2015-06-02 05:41:04',
			),
			55 => 
			array (
				'id' => 56,
				'path' => 'attachments',
				'name' => 'Tulips_2015-06-02_07_16_01.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-02 07:16:03',
				'updated_at' => '2015-06-02 07:16:03',
			),
			56 => 
			array (
				'id' => 57,
				'path' => 'avatars',
				'name' => 'Jellyfish.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-02 07:17:34',
				'updated_at' => '2015-06-02 07:17:34',
			),
			57 => 
			array (
				'id' => 58,
				'path' => 'attachments',
				'name' => 'Penguins_2015-06-04_10_54_22.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-04 10:54:23',
				'updated_at' => '2015-06-04 10:54:23',
			),
			58 => 
			array (
				'id' => 59,
				'path' => 'attachments',
				'name' => 'empty_text_file.txt',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'text/plain',
				'created_at' => '2015-06-04 10:57:24',
				'updated_at' => '2015-06-04 10:57:24',
			),
			59 => 
			array (
				'id' => 60,
				'path' => 'attachments',
				'name' => 'empty_text_file_2015-06-04_10_58_43.txt',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'text/plain',
				'created_at' => '2015-06-04 10:58:43',
				'updated_at' => '2015-06-04 10:58:43',
			),
			60 => 
			array (
				'id' => 61,
				'path' => 'attachments',
				'name' => 'empty_text_file_2015-06-04_11_01_21.txt',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'text/plain',
				'created_at' => '2015-06-04 11:01:21',
				'updated_at' => '2015-06-04 11:01:21',
			),
			61 => 
			array (
				'id' => 65,
				'path' => 'attachments',
				'name' => 'Tulips_2015-06-04_12_49_29.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-04 12:49:29',
				'updated_at' => '2015-06-04 12:49:29',
			),
			62 => 
			array (
				'id' => 66,
				'path' => 'attachments',
				'name' => 'Tulips_2015-06-04_13_15_35.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-04 13:15:36',
				'updated_at' => '2015-06-04 13:15:36',
			),
			63 => 
			array (
				'id' => 67,
				'path' => 'attachments',
				'name' => 'Hydrangeas_2015-06-04_13_24_32.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-04 13:24:33',
				'updated_at' => '2015-06-04 13:24:33',
			),
			64 => 
			array (
				'id' => 70,
				'path' => 'attachments',
				'name' => 'Chrysanthemum_2015-06-04_13_55_45.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-04 13:55:46',
				'updated_at' => '2015-06-04 13:55:46',
			),
			65 => 
			array (
				'id' => 71,
				'path' => 'avatars',
				'name' => 'Penguins_2015-06-07_08_35_22.jpg',
				'fileable_id' => 0,
				'fileable_type' => '',
				'mime_type' => 'image/jpeg',
				'created_at' => '2015-06-07 08:35:23',
				'updated_at' => '2015-06-07 08:35:23',
			),
		));
	}

}
