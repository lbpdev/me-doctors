<?php

use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class UserTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
        \DB::table('users')->delete();

        TestDummy::create('admin');
        TestDummy::create('doctor');
        TestDummy::create('patient');
        TestDummy::create('LBP_Admin');
        TestDummy::times(128)->create('MEDoctors\Models\User');
	}

}
