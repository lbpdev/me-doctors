<?php

use Illuminate\Database\Seeder;
use MEDoctors\Models\Video;

class VideosTableSeeder extends Seeder {

    public function run()
    {
        $videos = array(
            [
                'source' => 'https://www.youtube.com/watch?v=2vejkD0Rl3o',
                'title' => 'TEDxMaastricht - Dave deBronkart - aka e-Patient Dave: "Let patients help!"',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=Z8HeFNJjuj0',
                'title' => 'Implants & Technology -- The Future of Healthcare? Kevin Warwick at TEDxWarwick',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=36m1o-tM05g',
                'title' => 'My philosophy for a happy life | Sam Berns | TEDxMidAtlantic',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=iUbfRzxNy20',
                'title' => 'Brian Goldman: Doctors make mistakes. Can we talk about that?',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=e4pqPs9MtTY',
                'title' => 'TEDMED 2014: Foteini Agrafioti',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=hRWCTYTUMv0',
                'title' => 'TEDMED 2014: Jared Heyman',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=e2qrU8Tzoec',
                'title' => 'TEDMED 2014: Sophie de Oliveira Barata',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=e2qrU8Tzoec',
                'title' => 'TEDMED 2014: Sophie de Oliveira Barata',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=aQO_oexCm5s',
                'title' => 'The #1 reason people die early, in each country',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=esugL07XANg',
                'title' => 'What healthcare will look like in 2020 | Stephen Klasko | TEDxPhiladelphia',
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=UMhLBPPtlrY',
                'title' => "Peter Attia: What if we're wrong about diabetes?",
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=cDDWvj_q-o8',
                'title' => "Empathy: The Human Connection to Patient Care",
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=cDDWvj_q-o8',
                'title' => "Empathy: The Human Connection to Patient Care",
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=9D0g5Bxo3g4',
                'title' => "TEDMED 2014: Nina Tandon",
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=t-CPXrdRFyk',
                'title' => "TEDMED 2014: Sonia Shah",
            ],
            [
                'source' => 'https://www.youtube.com/watch?v=z8BJVeU2_5Y',
                'title' => "TEDMED 2014: Rupal Patel",
            ],
        );

        //---------------------- Seed Admins -------------------------//

        foreach($videos as $video){
            $video['channel_id'] = 2;
            $videoExists = Video::where('title',$video['title'])->first();

            if($videoExists)
                $videoExists->update($video);
            else
                Video::create($video);

        }


    }

}

