# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 192.168.7.7 (MySQL 5.5.44-0ubuntu0.14.04.1)
# Database: me_doctors
# Generation Time: 2015-08-20 06:39:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table email_domains_blacklist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_domains_blacklist`;

CREATE TABLE `email_domains_blacklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `email_domains_blacklist` WRITE;
/*!40000 ALTER TABLE `email_domains_blacklist` DISABLE KEYS */;

INSERT INTO `email_domains_blacklist` (`id`, `name`)
VALUES
	(1,'novartis.com'),
	(2,'pfizer.com'),
	(3,'roche.com'),
	(4,'ferring.com'),
	(5,'maf.ae'),
	(6,'tameen.ae'),
	(7,'servier.com'),
	(8,'sanofi.com'),
	(9,'sanofi.us'),
	(10,'merck.com'),
	(11,'merck.de'),
	(12,'merckgroup.com'),
	(13,'merckserono.com'),
	(14,'alxn.com'),
	(15,'mundipharma.com'),
	(16,'futurehealthcare.ae'),
	(17,'merckserono.co.uk'),
	(18,'centurypharma.com'),
	(19,'its.jnj.com'),
	(20,'jnj.com'),
	(21,'janssen.com'),
	(22,'janssen.co.uk'),
	(23,'janssen-emea.com'),
	(24,'gsk.com'),
	(25,'astrazeneca.com'),
	(26,'astrazeneca.co.uk'),
	(27,'astrazeneca-us.com'),
	(28,'gilead.com'),
	(29,'amgen.com'),
	(30,'celgene.com'),
	(31,'biogen.com'),
	(32,'abbvie.com'),
	(33,'abbott.com'),
	(34,'bms.com'),
	(35,'b-ms.co.uk'),
	(36,'bmsfrance.fr'),
	(37,'takeda.com'),
	(38,'astellas.com'),
	(39,'daiichisankyo.com'),
	(40,'eisai.com'),
	(41,'lilly.com'),
	(42,'bayer.com'),
	(43,'tevapharm.com'),
	(44,'tevausa.com'),
	(45,'teva.com'),
	(46,'tevagenerics.com'),
	(47,'julphar.com'),
	(48,'julphar.net'),
	(49,'sunpharma.com'),
	(50,'cipla.com'),
	(51,'lupin.com'),
	(52,'torrentpharma.com'),
	(53,'glenmarkpharma.com'),
	(54,'wipro.com'),
	(55,'biocon.com'),
	(56,'aurobindo.com'),
	(57,'ajantapharma.com'),
	(58,'mylan.com'),
	(59,'pg.com'),
	(60,'novonordisk.com'),
	(61,'novonordisk-us.com'),
	(62,'boehringer-ingelheim.com'),
	(63,'nutricia.co.uk'),
	(64,'nutricia.com'),
	(65,'danone.com'),
	(66,'nestle.com'),
	(67,'actavis.com'),
	(68,'otsuka.com'),
	(69,'baxter.com'),
	(70,'cardinalhealth.com'),
	(71,'fresenius.com'),
	(72,'medtronic.com'),
	(73,'rb.com'),
	(74,'bd.com'),
	(75,'edwards.com'),
	(76,'colgatepalmolive.com'),
	(77,'colgate.com'),
	(78,'pepsico.com'),
	(79,'unilever.com'),
	(80,'clorox.com'),
	(81,'kelloggs.com'),
	(82,'henkel.com'),
	(83,'generalmills.com'),
	(84,'beiersdorf.com');

/*!40000 ALTER TABLE `email_domains_blacklist` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
