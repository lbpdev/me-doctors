# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 192.168.7.7 (MySQL 5.5.44-0ubuntu0.14.04.1)
# Database: me_doctors
# Generation Time: 2015-08-26 13:23:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table email_domains_whitelist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email_domains_whitelist`;

CREATE TABLE `email_domains_whitelist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_domains_whitelist_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `email_domains_whitelist` WRITE;
/*!40000 ALTER TABLE `email_domains_whitelist` DISABLE KEYS */;

INSERT INTO `email_domains_whitelist` (`id`, `name`)
VALUES
	(2,'accessclinic.com'),
	(59,'advancedcareuae.com'),
	(11,'albayangroupuae.com'),
	(138,'alborglaboratories.com'),
	(13,'alimedical.org'),
	(5,'alkhailmedicalceter.com'),
	(97,'almariyapolyclinic.com'),
	(15,'alnoorrashidiya.com'),
	(136,'alzahra.com'),
	(139,'americancenteruae.com'),
	(146,'amritamedicalcentre.com'),
	(148,'amsa.ae'),
	(149,'amsarenalcare.com'),
	(86,'asterdmhealthcare.com'),
	(85,'asterhospital.com'),
	(3,'astermedicalcentre.com'),
	(87,'atlasera.com'),
	(102,'atlasstar.co'),
	(96,'belhoulspeciality.com'),
	(147,'biosytechworld.com'),
	(54,'cedars-jaih.com'),
	(57,'cpslabs.com'),
	(144,'crescentmc.ae'),
	(6,'dmhealthcare.com'),
	(52,'docibgroup.com'),
	(55,'drismail.greensmedia.com'),
	(58,'drsanjaymedical.com'),
	(93,'drshankarpolyclinic.ae'),
	(89,'fathimamedicalgroup.com'),
	(84,'gmcclinics.com'),
	(8,'gulf-healthcare.com'),
	(90,'hcmena.com'),
	(105,'icare-clinics.com'),
	(83,'ihd.ae'),
	(104,'jebelalihospital.com'),
	(145,'jimc.ae'),
	(4,'karamamedicalcentre.com'),
	(95,'kidsheartdubai.com'),
	(98,'kimsdubai.com'),
	(91,'lakeshoremedicalclinics.com'),
	(133,'lifeline.ae'),
	(101,'lotusmedicalcenter.greensmedia.com'),
	(53,'Mashaher.com'),
	(103,'medhealth.ae'),
	(100,'nmc.ae'),
	(137,'phd-laboratories.com'),
	(9,'phdiagnostics.ae'),
	(143,'physioartuae.com'),
	(140,'primehealth.ae'),
	(141,'Primehealthcaregroup.com'),
	(94,'rajanortho.com'),
	(134,'rcmc.ae'),
	(88,'rpcuae.com'),
	(142,'samaamedical.com'),
	(56,'shamsmoopendentalclinic.ae'),
	(99,'thumbayhospital.com'),
	(150,'unicaredubai.com'),
	(51,'unitymedcenter.com'),
	(135,'ydl-me.com'),
	(92,'zulekhahospitals.com');

/*!40000 ALTER TABLE `email_domains_whitelist` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
