$(document).ready(function(){
    //Up Coming Events slider
    var events = $("#latest-articles-car");
    events.owlCarousel({
        itemsCustom  : [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 1],
            [1000, 1],
            [1200, 1],
            [1400, 1],
            [1600, 1]
        ],
        pagination : true,
        autoPlay : true
    });

    //
    //// Custom Navigation Events
    //$(".latest-articles-car .next").on('click', function(){
    //    events.trigger('owl.next');
    //});
    //
    //$(".latest-articles-car .prev").on('click', function(){
    //    events.trigger('owl.prev');
    //});

});
