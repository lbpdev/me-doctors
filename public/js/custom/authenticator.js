(function() {

    var $auth = $('#whoAmI');
    var $login = $('#loginModal');

    $('.who-item').on('click', function() {
        var el = $(this),
            login = el.data('login'),
            loginUrl = el.data('login-url'),
            registerUrl = el.data('register-url');

        if (login && loginUrl && registerUrl) {
            var image = $(login.image).attr('src');

            $auth.modal('hide');

            $login.find('img').attr('src', image);
            $login.find('.modal-title').text(login.text);
            $login.find('form').attr('action', loginUrl);
            $login.find('#registerLink').attr('href', registerUrl);
            $login.modal('show');
        }
    });

})();