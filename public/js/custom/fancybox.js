$(document).ready(function(){
    /* This is basic - uses default settings */
    $("a#single_image").fancybox();

    /* Using custom settings */

    $("a#inline").fancybox({
        'hideOnContentClick': true
    });

    /* Apply fancybox to multiple items */

    $("a.group").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'overlayShow'	:	false
    });

    $(".fancybox").on("click", function(){
        $.fancybox({
            href: this.href,
            type: $(this).data("type"),
            helpers: {
                overlay: {
                    locked: false
                }
            }
        }); // fancybox
        return false
    }); // on

});