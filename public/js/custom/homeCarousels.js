$(document).ready(function(){


    // Videos slider
    var videos = $("#video_slider");
    videos.owlCarousel({
        itemsCustom  : [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 3],
            [1000, 3],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        navigation : false,
        autoPlay : true
    });

    // Custom Navigation Events
    $(".videos_slider .next").on('click', function(){
        videos.trigger('owl.next');
    });

    $(".videos_slider .prev").on('click', function(){
        videos.trigger('owl.prev');
    });

    //Up Coming Events slider
    var events = $("#up_coming_events_slider");
    events.owlCarousel({
        itemsCustom  : [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 3],
            [1200, 3],
            [1400, 3],
            [1600, 4]
        ],
        navigation : false,
        autoPlay : true
    });


    // Custom Navigation Events
    $(".upcoming-events .next").on('click', function(){
        events.trigger('owl.next');
    });

    $(".upcoming-events .prev").on('click', function(){
        events.trigger('owl.prev');
    });


});
