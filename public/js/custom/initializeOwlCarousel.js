$(document).ready(function(){

    // Latest Articles
    var ocarousel = $("#owlcarousel");
    ocarousel.owlCarousel({
        itemsCustom  : [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 3],
            [1200, 3],
            [1400, 3],
            [1600, 3]
        ],
        navigation : false,
        pagination : false,
        autoPlay : true
    });

    // Custom Navigation Events
    $(".customNavigation .next").on('click', function(){
        ocarousel.trigger('owl.next');
    });

    $(".customNavigation .prev").on('click', function(){
        ocarousel.trigger('owl.prev');
    });
});