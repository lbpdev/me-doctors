// Gene Custom JS

$(document).ready(function(){

    // User Options Function
    $('.user-options-bt').on('click', function(event) {
        $( "#user-options" ).animate({
            top: 10
        }, 500 , function(){
            $("#user-options").mouseleave(function() {
                $("#user-options").delay('100').animate({
                    top: '-100%'
                });
            })
        });
    });

    // Add additional fields function.
    if($('.add_field_bt').length > 0){ addFieldEvent(); }

    function addFieldEvent() {
        var clone_field;
        $('.add_field_bt').unbind("click");
        $('.add_field_bt').on('click', function (event) {
            event.preventDefault();
            if ($(event.target).closest('.fields').find('input').length > 0)
                clone_field = ($(event.target).closest('.fields').find('input.form-control')).eq(0).clone();
            else if ($(event.target).closest('.fields').find('select').length > 0)
                clone_field = ($(event.target).closest('.fields').find('select')).eq(0).clone();
            else if ($(event.target).closest('.fields').find('textarea').length > 0)
                clone_field = ($(event.target).closest('.fields').find('textarea')).eq(0).clone();

            $(event.target).before(clone_field.val(''));

            var questionCount = $(event.target).parent().find('.questioncount').length;
            if( questionCount > 0){
                var questionFieldsCount = $(event.target).closest('.fields').find('input.form-control').length;
                $(event.target).parent().find('.questioncount').val(parseInt(questionFieldsCount));
            }
        });
        removeFieldEvent(1);
        return true;
    }


    function removeFieldEvent(){
        $('.remove_field_bt').unbind("click");
        $('.remove_field_bt').on('click', function (event) {
            event.preventDefault();
            var maxFields = parseInt($(event.target).attr('rel'));

            console.log(maxFields);
            if ($(event.target).parent().find('input.form-control').length > maxFields){
                ($(event.target).parent().find('input.form-control')).last().remove();
                ($(event.target).parent().find('input.choice_id')).last().remove();
            }
            else if ($(event.target).closest('.fields').find('select').length > maxFields)
                ($(event.target).closest('.fields').find('select')).last().remove();
            else if ($(event.target).closest('.fields').find('textarea').length > maxFields)
                ($(event.target).closest('.fields').find('textarea')).last().remove();
            else {
                ($(event.target).closest('.fields').find('input.form-control')).last().val("");
                ($(event.target).closest('.fields').find('textarea')).last().val("");
            }

            if( $(event.target).parent().find('.questioncount').length > 0){
                var questionFieldsCount = $(event.target).closest('.fields').find('input.form-control').length;
                $(event.target).parent().find('.questioncount').val(parseInt(questionFieldsCount));
            }
        });
        return true;
    }


    // Add additional field groups function for Survey Items.
    if($('.add_field_group').length > 0){
        var clone_field;
        $('.add_field_group').on('click', function(event) {
            event.preventDefault();
            if($(event.target).parent().find('.form-group').length > 0)
                clone_field = ($(event.target).parent().find('.form-group')).eq(0).clone();

            clone_field.find('input.form-control').val('').removeAttr('checked');
            clone_field.find('.choice_id').remove();
            clone_field.find('input.item_id').val('0');
            clone_field.prepend('<a href="#" rel="2" class="pull-left remove_field_group_bt glyphicon glyphicon-minus-sign"></a>');

            $(event.target).parent().find('.form-group').last().after(clone_field);
            addFieldEvent();
            removeFieldEvent();
            removeFieldGroupEvent()
            checkBack();
        });
    }


    if($('.remove_field_group_bt').length > 0){ removeFieldGroupEvent(); }

    function removeFieldGroupEvent(){

        $('.remove_field_group_bt').unbind("click");
        $('.remove_field_group_bt').on('click', function (event) {

            event.preventDefault();
            var maxFields = parseInt($(event.target).attr('rel'));

            //if ($(event.target).parent().find('.form-group').length > maxFields)

                $(event.target).parent().remove();
        });
        return true;
    }




    // RADIO BUTTON SURVEY ANSWERING FUNCTION

    if($('.choice-radio').length > 0){ radioChoicesEvent(); }

    function radioChoicesEvent(){

        $('.choice-radio').unbind("click");
        $('.choice-radio').on('click', function (event) {

            $(event.target).parent().find('.choice-input').val($(event.target).val());

        });
        return true;
    }


    // CHECKBOX ADD POLL FUNCTION
    if($('#add_poll').length > 0){
        var poll_flag = false;

        $('#add_poll').on('click', function(event) {
            poll_flag = event.target.checked;

            if(poll_flag){
                $('#poll_form').show();
                $('#add_poll_item').show();

                pollTitle = document.createElement("input");
                pollTitle.setAttribute('type', 'text');
                pollTitle.setAttribute('placeholder', 'Poll Title');
                pollTitle.setAttribute('name', 'poll_title');
                pollTitle.setAttribute('class', 'form-control poll-item');
                pollTitle.required = true;
                $('#poll_form .form-group').append('Poll Title: ', pollTitle);

                var textField = [];
                var wrapper;
                    for(var fields=0; fields<2; fields++){
                        textField[fields] = document.createElement("input");
                        textField[fields].setAttribute('type', 'text');
                        textField[fields].setAttribute('placeholder', 'Poll Item Name');
                        textField[fields].setAttribute('name', 'poll_item[]');
                        textField[fields].setAttribute('class', 'form-control poll-item');
                        textField[fields].required = true;

                        wrapper = "<div class='poll_item_group pi_group"+[fields+1]+"'>Poll Item "+[fields+1]+":</div>";

                        $('#poll_form .form-group').append(wrapper);
                        $('.pi_group'+[fields+1]).append(textField[fields]);
                    }
            } else {
                $('#poll_form .form-group').empty();
                $('#poll_form').show();
                $('#add_poll_item').hide();
            }
        });

    }

    if($('.check-back').length > 0){ checkBack(); }

    function checkBack(){
        $('.check-back').unbind('click');
        $('.check-back').on('click', function(event){
            if(event.target.checked)
                $(event.target).parent().find('.allow-holder').val(1);
            else
                $(event.target).parent().find('.allow-holder').val(0);
        });
    }

    $('#add_poll_item').on('click', function(event){
        event.preventDefault();
        var textField;
        textField = document.createElement("input");

        textField.setAttribute('type', 'text');
        textField.setAttribute('placeholder', 'Poll Item Name');
        textField.setAttribute('name', 'items[]');
        textField.setAttribute('class', 'form-control poll-item');
        textField.required = true;

        var item_number = parseInt($('.poll-item').length);
        var wrapper = "<div class='poll_item_group pi_group"+item_number+"'>Item "+item_number+":<i class='remove_poll_item fa fa-minus-circle'></i></div>";

        $('.poll-form .form-group').append(wrapper);
        $('.pi_group'+item_number).append(textField);

        removePollItemEvent();
    });

    if($('.remove_poll_item').length > 0){ removePollItemEvent(); }
    function removePollItemEvent(){
        $('.remove_poll_item').on('click', function(event){
            $(event.target).parent().remove();
        });
    }
});