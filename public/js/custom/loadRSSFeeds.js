if($('#feed').length>0){
    google.load("feeds", "1");

function OnLoad() {
    // Query for president feeds on cnn.com
    var query = 'site:feeds.bbci health';
    google.feeds.findFeeds(query, findDone);

}

function findDone(result , max) {
    // Make sure we didn't get an error.
    if (!result.error) {
        // Get content div
        var content = document.getElementById('feed');
        var html = '';

        // Loop through the results and print out the title of the feed and link to

        // the url.
        for (var i = 0; i < result.entries.length; i++) {
            if(i<4){
                var entry = result.entries[i];
                html += '<li class="margin-b-10 clearfix"><div class="media-body col-md-12 padding-tb-10">'+
                '<p class="media-heading"><a target="_blank" href="' + entry.link + '">' + entry.title + '</a></p>';
            }
        }
        content.innerHTML = html;
    }
}

google.setOnLoadCallback(OnLoad);
}