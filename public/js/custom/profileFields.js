$(document).ready(function(){

    /*
     Toggle Add Form
     */
    $('.add-toggle').on('click' , function(event){
        $(this).toggle();
        $(this).closest('.section').find('.add-form').toggle();

        $('.location').each(function(e){
            initMap(e);
        });

        event.preventDefault();
    });

    /*
     Cancel Adding
     */
    $('.add-cancel').on('click' , function(event){
        event.preventDefault();
        $(this).closest('.section').find('.add-toggle').toggle();
        $(this).parent().parent().toggle();
    });


    function addFieldEvent() {
        var clonedField, totalFields, nameAttr;
        $('.add_field_bt').unbind("click");
        $('.add_field_bt').on('click', function (event) {
            totalFields = ($(this).parent().find('.form-control')).length;
            clonedField = ($(this).parent().find('.form-control')).eq(0).clone();

            nameAttr = getNameAttribute(clonedField, totalFields);

            //                        
            $(clonedField).attr('name' , nameAttr).val('');

            $(this).parent().find('.fields').append(clonedField);

            event.preventDefault();
        });
        removeFieldEvent(1);
        return true;
    }

    // Add additional fields function.
    addFieldEvent();

    function getNameAttribute(field, totalFields) {
        var fieldName = $(field).attr('name'),
            nameRegex = /[^\[]+/,
            keysRegex = /[^[\]]+(?=])/g,
            startName,
            endName = '', // the last set of the field name if it is an array
            nameKeys, // the array of keys inside the brackets if the field name is an array
            i, len, key;

        startName = fieldName.match(nameRegex)[0];
        nameKeys = fieldName.match(keysRegex);

        if (nameKeys)
        {
            for (i = 0, len = nameKeys.length; i < len; i++)
            {
                key = nameKeys[i];
                // if key is a number increment the array key
                if ( ! isNaN(key)) {
                    key = totalFields;
                }
                endName += '[' + key + ']';
            }
            return startName + endName;
        }
        return startName;
    }


    removeFieldEvent();
    function removeFieldEvent(){
        $('.remove_field_bt').unbind("click");
        $('.remove_field_bt').on('click', function (event) {
            event.preventDefault();
            var maxFields = parseInt($(this).attr('rel'));

            if ($(this).parent().find('input.form-control').length > maxFields){
                ($(this).parent().find('input.form-control')).last().remove();
            }
            else {
                ($(this).parent().find('input.form-control')).last().val("");
            }
        });
        return true;
    }

    // Add additional fields function.
    addFieldGroupEvent();
    function addFieldGroupEvent() {
        var clonedField, totalGroups, activeField, nameAttr, i, gen;

        $('.add_field_group_bt').unbind("click");
        $('.add_field_group_bt').on('click', function (event) {
            totalGroups = $('input.location').length;
            gen = totalGroups + Math.floor((Math.random() * 100) + 50);

            clonedField = ($(this).parent().find('.fields')).eq(0).clone();

            var fieldCount = $(clonedField).find('input').length;

            for (i = 0; i < fieldCount; i++)
            {
                activeField = $(clonedField).find('input')[i];
                nameAttr = getNameAttribute(activeField, gen);

                $(activeField).attr('name' , nameAttr).val('');
            }

            for (i = 0; i < 7; i++)
            {
                itemid = Math.random().toString(36).substring(7);
                var activeCheckbox = $(clonedField).find('input[type=checkbox]')[i];
                $(activeCheckbox).attr('name' , 'work['+gen+'][days]['+i+'][day]').val('');
                $(activeCheckbox).attr('id' , itemid).val('');
                $(activeCheckbox).parent().find('label').attr('for' , itemid);
                $(activeCheckbox).attr('value' , i);
            }

            $(this).parent().find('.fields-group').append(clonedField);
            initializeAPI(totalGroups,clonedField);
            autoCompleteBind();
            removeSpecificFieldGroupEvent();
            event.preventDefault();
        });

        removeFieldEvent(1);
        return true;
    }


    removeFieldGroupEvent();
    function removeFieldGroupEvent(){
        $('.remove_field_group_bt').unbind("click");
        $('.remove_field_group_bt').on('click', function (event) {
            event.preventDefault();
            var maxFields = parseInt($(this).attr('rel'));

            if ($(this).parent().find('.fields').length > maxFields){
                ($(this).parent().find('.fields')).last().remove();
            }
        });
        return true;
    }

    removeSpecificFieldGroupEvent();
    function removeSpecificFieldGroupEvent(){
        $('.fa-close').unbind("click");
        $('.fa-close').on('click', function (event) {
            if ($('.fields-group .fields').length > 1)
                $(this).parent().remove();
            else
                $(this).parent().find('input').val('');

            event.preventDefault();
        });
        return true;
    }

    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    var placeSearch, autocomplete = [];
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    autocomplete = [];
    function initializeAPI(indx,target) {
        // Create the autocomplete object, restricting the search
        // to geographical location types.
        autocomplete[indx] = new google.maps.places.Autocomplete(
            /** @type {HTMLInputElement} */($('input.location')[indx]),
            { types: ['geocode'] });
        // When the user selects an address from the dropdown,
        // populate the address fields in the form.

        google.maps.event.addListener(autocomplete[indx], 'place_changed', function(e) {
            fillInAddress(autocomplete[indx],target);
            // Show location on Map
        });

    }

    function fillInAddress(autocompleteData,target) {
        console.log('Target: '+target);
        // Get the place details from the autocomplete object.
        var place = autocompleteData.getPlace();


        $(target).find('table input').val('');
        var targetIndex = $('.fields-group .fields').index(target);
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                var input = $('.fields-group .fields')[targetIndex];
                $(input).find('.'+addressType).val('').val(val);
            }
        }

        initMap(targetIndex);
    }


    var field_groups = $('input.location').length;
    $('.fields-group .fields').each(function(index){
        initializeAPI(index,$(this));
    });
});

