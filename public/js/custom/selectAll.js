$('.select-all-wrapper').append(
    '<a href="#" class="select-all">Select All</a>'+
    '<a href="#" class="unselect-all" style="display: none">Unselect All</a>'
);


$('.select-all').on('click', function(e){

    $(this).parent().find('select option').each(function(index,item){
        $(item).attr('selected','selected');
    });

    $(this).parent().find('select').trigger('change');
    $(this).hide();
    $('.unselect-all').show();
    e.preventDefault();
});

$('.unselect-all').on('click', function(e){
    $('select option').each(function(index,item){
        $(item).removeAttr('selected');
    });

    $(this).parent().find('select').trigger('change');
    $(this).hide();
    $('.select-all').show();
    e.preventDefault();
});