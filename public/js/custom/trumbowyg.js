if($('.textarea').length>0){
    $('.textarea').trumbowyg({
        removeformatPasted: true,
        btns: ['viewHTML',
            '|', 'formatting',
            '|', 'btnGrp-design',
            '|', 'link',
            '|', 'btnGrp-justify',
            '|', 'btnGrp-lists',
            '|', 'horizontalRule']
    });
}