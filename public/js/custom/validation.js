(function() {

    var messages = {
        required: 'Please fill in this field.',
        email: 'Please enter a valid email.',
    };

    // $('form[data-toggle="validator"]').validator({
    //     errors: {
    //         native: "Please fill in this field."
    //     }
    // });

    $('input, select').on('invalid change', function(e) {
        var $el = $(this);

        if ($el.val() !== '') {
            return this.setCustomValidity('');
        }

        if ($el.prop('required')) {
            this.setCustomValidity(messages.required);
        }
    });
})();