$(document).ready(function(){

    // Videos slider
    var videos = $("#video_slider");
    videos.owlCarousel({
        itemsCustom  : [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 3],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        pagination : false,
        navigation : false,
        autoPlay : false
    });

    // Custom Navigation Events
    $(".videos_slider .next").on('click', function(){
        videos.trigger('owl.next');
    });

    $(".videos_slider .prev").on('click', function(){
        videos.trigger('owl.prev');
    });

});
