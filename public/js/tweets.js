$(function(){
	$.ajax({
		url: $('#tweets').attr('rel'),
		type: 'GET',
		success: function(response) {
            console.log(response);
			if (typeof response.errors === 'undefined' || response.errors.length < 1) {

				var $tweets = $('<ul class="no-padding"></ul>');
				$.each(response.statuses, function(i, obj) {
					$tweets.append(
                    '<li class="clearfix">'+
                        '<div class="col-md-12">'+
                            '<div class="row">'+
                                '<div class="col-md-2 col-sm-2 col-xs-2">'+
                                    '<div class="row">'+
                                        '<img src="'+ obj.user.profile_image_url +'" width="100%">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-10 col-sm-10 col-xs-10 padding-r-0">'+
                                    '<a href="https://twitter.com/'+ obj.user.screen_name +'" target="_blank">'+
                                        '<h5 class="t-black">'+ obj.user.name +'</h5>'+
                                        '<span class="twitterName">'+ obj.user.screen_name +'</span><br>'+
                                    '</a>'+
                                    '<p>'+ obj.text +'</p>'+
                                    '<span class="font-10 pull-right">'+ obj.created_at+'</span><br>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</li>'
                    );
				});

				$('#tweets').html($tweets);
			} else {
				$('.tweets-container p:first').text('Response error');
			}
		},
		error: function(errors) {
            console.log(errors);
			$('.tweets-container p:first').text('Request error');
		}
	});
});
