[33mcommit deb1b2656b5ce005c47868ae0effb1adf1b0a01b[m
Merge: 98d38e2 40b2b0e
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jul 5 10:00:34 2015 +0400

    Merge branch 'refactoring' of https://bitbucket.org/lbpdev/me-doctors into Patients

[33mcommit 98d38e2fe85e10c6a8625abb4c49f397ee7aad8e[m
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jul 5 10:00:12 2015 +0400

    Premerge

[33mcommit 40b2b0efd9f514ca902896d91588bf83120826d8[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jul 2 16:26:55 2015 +0400

    implement uploader classes

[33mcommit b885ea427659d23e2e6fef2e951803365ace5022[m
Author: elloringene <elloringene@gmail.com>
Date:   Thu Jul 2 16:06:28 2015 +0400

    Patient side initial setup

[33mcommit 31159b50db8e1839f8d0a987529f496d1b15cd23[m
Author: elloringene <elloringene@gmail.com>
Date:   Thu Jul 2 13:06:35 2015 +0400

    Mulitple Trash Added

[33mcommit d39f126ac97939ccb2f7befaf98f7ce6001b3d06[m
Merge: b469304 3fd85ee
Author: elloringene <elloringene@gmail.com>
Date:   Thu Jul 2 10:32:59 2015 +0400

    Merge

[33mcommit 3fd85eeb56a419c932f774915f6e7077bd243b5e[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 16:14:50 2015 +0400

    fix exception when no poll variable is provided to the view

[33mcommit c6c5374b979340235acfecfc039f095953f2c943[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 16:14:20 2015 +0400

    implement posting of comment (no upload)

[33mcommit 47981813140ae65f0a2d43466e60a7ebe830a9fe[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 16:12:50 2015 +0400

    test for posting a comment

[33mcommit b46930447361c79065b9be86df20de145251a244[m
Author: elloringene <elloringene@gmail.com>
Date:   Wed Jul 1 15:54:50 2015 +0400

    Messages Updates and Trash Feature Added

[33mcommit 4eb06b45d276d489212d7b8dbd55e84a9f88a512[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:33:24 2015 +0400

    create login tests

[33mcommit 6e37df7f4b445ff7d2e1164129e6f14630639df3[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:32:01 2015 +0400

    setup testing database

[33mcommit c135e6c36048aae735f113d61659322484b57f23[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:31:39 2015 +0400

    remove upload metas table

[33mcommit beaa827f250732200c09be368e4a12116665bf5e[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:31:21 2015 +0400

    remove timestamps from therapy user pivot

[33mcommit edf0c5eec1323191cd4799d35b360f268191292f[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:29:35 2015 +0400

    setup codeception testing suites

[33mcommit 91b9dd451c0af239ef2bbe6b61b8257c7456c6c6[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Wed Jul 1 14:24:46 2015 +0400

    ignore env files

[33mcommit d722b145c7fa90fc23d6e8a100be0fcd85688ecf[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 15:54:32 2015 +0400

    include withFeatured article in getting the articles for home page

[33mcommit 12138d2c066ddcda9e189610d36fe97cac245d63[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 15:53:20 2015 +0400

    discussions refactoring

[33mcommit c34ecd2206507d2a74443d4a51402436f25fd379[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 15:46:22 2015 +0400

    Discussion and Comments author relationships

[33mcommit 0f0da618bca2e6d180160ed3b95d17c812295bdc[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 15:20:27 2015 +0400

    profiles and comments seeder

[33mcommit d13bca766b93c5d396300b1bcb3b1f8bceb1c6ce[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 15:20:18 2015 +0400

    remove unused fields

[33mcommit 286e40f5e7c16ddfe58fc25c8ba536431a2ebfe3[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 13:34:37 2015 +0400

    comments count relationship on discussion model

[33mcommit cb268b72e499776bf2f101197015ab5f06200300[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:43:26 2015 +0400

    can find by slug trait for the repositories

[33mcommit 8f32f3f364fed38d6d1b5526a42b4d118b2ffd0f[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:43:05 2015 +0400

    popular and archived articles method stubs

[33mcommit 879cc8f5579834a311e66b8c1df67ce95dce56c7[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:42:06 2015 +0400

    refactor articles view and routes

[33mcommit 19120b157b570af345be19c40c3d23fe8796543d[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:29:43 2015 +0400

    rename home method to index

[33mcommit 2eaec6ad6fd88d082f84a71a31867ea2fda0f3a8[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:29:28 2015 +0400

    fix missing parent construct

[33mcommit a77f81d2321f41085de2e6985de0f738f7f0411f[m
Merge: 170fd70 52150c1
Author: elloringene <elloringene@gmail.com>
Date:   Tue Jun 30 12:16:14 2015 +0400

    Merge branch 'refactoring' of https://bitbucket.org/lbpdev/me-doctors into NewFront

[33mcommit 52150c1a51f80ca7544223dcdfabb49dd29451d3[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 12:15:41 2015 +0400

    articles route resource

[33mcommit 170fd70afb9b49d8aa9544d3a45782c55fc6277a[m
Merge: 5e7fe6f 0f95af6
Author: elloringene <elloringene@gmail.com>
Date:   Tue Jun 30 12:02:18 2015 +0400

    Merge branch 'refactoring' of https://bitbucket.org/lbpdev/me-doctors into NewFront

[33mcommit 5e7fe6f87e347edcdcdbf7bb5007d913392b3668[m
Author: elloringene <elloringene@gmail.com>
Date:   Tue Jun 30 11:57:41 2015 +0400

    messages recepient autocomplete added.

[33mcommit 67e3c4c67d30bd0d20a8191f267a94b752990fe2[m
Merge: 741df84 e2df69c
Author: elloringene <elloringene@gmail.com>
Date:   Tue Jun 30 11:47:24 2015 +0400

    messages recepient autocomplete added.

[33mcommit 741df84ef99ad8bc14bc5d12d9a9e93a556ada9e[m
Author: elloringene <elloringene@gmail.com>
Date:   Tue Jun 30 11:41:51 2015 +0400

    messages recepient autocomplete added.

[33mcommit 0f95af64bc4f055d9b30825af881146d1dc0ad71[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 11:04:45 2015 +0400

    change article links route on home views

[33mcommit e2df69caa08903dbc9b3ab54a02b4dce8e152fc1[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 10:22:18 2015 +0400

    refactor home page variables and methods

[33mcommit 4497784d42c1eedec7ffc5486053c4a0ff693141[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 10:21:47 2015 +0400

    fix query n+1 issue on sidebar

[33mcommit 7023bddc950118ea86448534e10630f30de97a44[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 10:21:27 2015 +0400

    remove sessions on master page

[33mcommit 02404998d457436f91e1b9dbec01193b05e01229[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Tue Jun 30 10:19:09 2015 +0400

    extract popular discussions on its own class

[33mcommit 15e06b0df5384142d8090a94e70e29e91a5ba8e5[m
Merge: d564ebc 05e76c5
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 15:50:29 2015 +0400

    Merge

[33mcommit d564ebc055d36bb67357faabc6a67ae9dc257034[m
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 15:48:06 2015 +0400

    Messages Initial Development

[33mcommit 05e76c5ece3fc23394c4cc8809713a53e4a6f389[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:47:16 2015 +0400

    shared variables and auth user variable in views and controllers

[33mcommit 866c411787697e4fc16663ab8026d2b763e65f08[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:46:52 2015 +0400

    comments relationship in discussion model

[33mcommit e8ad9054f5bed5b52dbe53275df7cc02980e4082[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:46:28 2015 +0400

    change named routes to url links

[33mcommit f39bd95653d90e197cd4780d48af5d05df143071[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:45:26 2015 +0400

    refactor authentication

[33mcommit 374b2cfa3b2fe31922ff13e3044dc0583cb0422a[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:44:21 2015 +0400

    extract home page views

[33mcommit a2ccae1ded49973208b9af527d6a654bc716884c[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 15:01:14 2015 +0400

    user model profile and avatar relationships

[33mcommit 1f3151ddaf66c3a954ccc60c03b805f9850aec3d[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 14:58:22 2015 +0400

    move home sidebar folder location and extract partials

[33mcommit 8661f81d6e10e28fadd95a7914b5f9d9519a7bd2[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 14:56:47 2015 +0400

    ignore framework sessions

[33mcommit e15b4f45cc34e30b94180910d0c4054d4c6072a8[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 14:03:45 2015 +0400

    poll refactoring and repository bindings

[33mcommit e1ae7d022ac71ec0a62057bc3e2135a026bb18ea[m
Merge: da5c853 1ad956a
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 11:02:16 2015 +0400

    Merge branch 'refactoring' of https://bitbucket.org/lbpdev/me-doctors into NewFront

[33mcommit 1ad956ade87ddd4e74966d355271a43364922426[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 11:01:25 2015 +0400

    conversation message relationships

[33mcommit 4868d3b5c556393e65cd805d37cbadfcf7454c9b[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 11:00:29 2015 +0400

    conversation migrations

[33mcommit da5c85330ed12a6ab6b63af2ddc83ecfd1510e67[m
Merge: c2f506a 04195ff
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 09:50:32 2015 +0400

    Merge

[33mcommit c2f506a2f459ca6d8138ae66ef82cd1b139db64c[m
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 09:49:12 2015 +0400

    Merge

[33mcommit 65b693590c35ae72203941b51c74011867e0cc7b[m
Merge: 04e4b5d eacfc24
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 09:44:11 2015 +0400

    Merge

[33mcommit 04195ff1d7bf390fdcffc096f617ffe240007589[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:40:59 2015 +0400

    refactor home view

[33mcommit 0bdb96c11b820c5560d2de6a36e4d3213bb56309[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:40:44 2015 +0400

    scope with featured and casts in article model

[33mcommit e4864e3b4cd02adae40f167137bb023d79102497[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:39:44 2015 +0400

    get featured article method

[33mcommit 9159e9423822ac2bf77a8e30d0e5a4f0d1b2a371[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:39:26 2015 +0400

    add clockwork on local environment

[33mcommit ea7c2de6114d06debc9306c783c7dff37d06dc78[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:39:11 2015 +0400

    add LBP_Admin in user table seeding

[33mcommit 04e4b5de0cdd95c105a3e4e0edb58a98a1791b05[m
Author: elloringene <elloringene@gmail.com>
Date:   Mon Jun 29 09:38:50 2015 +0400

    Pre-merge

[33mcommit 1f7d7a0b48f6d04975c382a195f212ca0c1162be[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:38:04 2015 +0400

    force create an article to override created_at field

[33mcommit 36cd27a5791aa02c1b069f57b2ce91c7584639bd[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Mon Jun 29 09:37:43 2015 +0400

    clockwork storage

[33mcommit c775f1ea1c23ab4cd43319eab3488ce285ce8a14[m
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 15:52:00 2015 +0400

    Responsive Updates

[33mcommit 636bb050cd214c478bd8af73fc5f59d4c5e9acdb[m
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 15:45:14 2015 +0400

    Responsive Updates.

[33mcommit a8097cc437adb636dd45ee7fc2c3308fe0198777[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 12:51:26 2015 +0400

    add is_featured field in articles

[33mcommit fef74864415906d1615cc5485c4555411b6694b2[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 12:51:16 2015 +0400

    fix conflict

[33mcommit 7fa6ff025f474fa0d1940ec3e7ccf79b664ee49f[m
Merge: e0c4b62 1c22bfe
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 12:00:52 2015 +0400

    merge to master

[33mcommit 1c22bfe213cdfa12ab5abebac875752f6a931e1f[m
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 11:51:05 2015 +0400

    Pre-merge

[33mcommit 9405660b15e24d767536180da295f185355f7b14[m
Merge: 1a02afa 6661f62
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 11:50:07 2015 +0400

    Pre-merge

[33mcommit 1a02afa3a53cfb1eac0ab00fa9a206e7f6fbde83[m
Merge: fa2b735 53b93a0
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 11:43:42 2015 +0400

    Merge branch 'master' of https://bitbucket.org/lbpdev/me-doctors

[33mcommit 6661f629b4e6996c0e78f3720fc50494c09f9c2a[m
Author: elloringene <elloringene@gmail.com>
Date:   Sun Jun 28 11:41:39 2015 +0400

    Responsive Fix and Updates

[33mcommit 53b93a05f095ba63f7ee5abd19fa654814adc6b4[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 11:11:39 2015 +0400

    add necesarry gitignores

[33mcommit e0c4b62e797e7fe4287f931117867df8c6a549a7[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:58:08 2015 +0400

    rename diagnosis discussion to doctor discussion

[33mcommit 1c58f72cd2d963014f1e013b509d1a1766700a0f[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:57:53 2015 +0400

    paginated doctor discussions

[33mcommit 34ded5487535969b9e5828a933199a5595a6efc8[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:57:28 2015 +0400

    abstract db repository class

[33mcommit 66bdda7aa1ff054478a9acfe22df74c891f42fcb[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:48:28 2015 +0400

    Discussion table seeder

[33mcommit c84d977f3e0ad9e3c304fb2b5f40beb82df29f0a[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:48:15 2015 +0400

    set discussion fillable attributes

[33mcommit 0554486b6d74e941a92675c9b2e485215e037e09[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:47:26 2015 +0400

    channel-discussion relationship

[33mcommit 67bf6665f583a7e13f97612782f6bf1c2bac4a5d[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:46:57 2015 +0400

    replace limit query to paginated queries

[33mcommit 1008e2536ac1b968b4ee223b8dc513ff1c129e63[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:45:55 2015 +0400

    get paginated panel discussions

[33mcommit 6225ec8a16e221aaeb91d8b5e87bedfb703911f4[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Sun Jun 28 10:26:04 2015 +0400

    channel repository binding

[33mcommit eacfc24085cfb7483a0bff7983285204b5fba5c1[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:58:43 2015 +0400

    Channel model

[33mcommit 196d8ff6aa1725b5cd26abeb525c095e7db7c686[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:58:34 2015 +0400

    channel seeder

[33mcommit 6dc0bbda5ab49dfc6cbb55e63b4400253de2478b[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:58:14 2015 +0400

    fix events seeder

[33mcommit a4d70537842d25a45b4e851170839d54748a7da0[m
Author: elloringene <elloringene@gmail.com>
Date:   Thu Jun 25 15:42:04 2015 +0400

    Messages front added. Poll functionality added.

[33mcommit bca705e1736716bc0581bfefb498f797a76cd8ae[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:29:38 2015 +0400

    fix repository bindings

[33mcommit 6a91d5c61f12fac9fd6ecdaedadb951ca8ee3dfe[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:29:24 2015 +0400

    upcoming events

[33mcommit 6a82396677a70855cd8b2e979cff00ea7eb9ea9b[m
Author: Kenneth Sungcaya <ksungcaya@gmail.com>
Date:   Thu Jun 25 15:28:43 2015 +0400

    latest articles
