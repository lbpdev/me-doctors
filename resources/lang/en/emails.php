<?php

return [

    'whitelist' => [
        'delete' => 'Email has been removed from the white list.',
        'store' => 'Email has been added to the white list.',
        'exists' => 'This email address already exists on the white list.'
    ],

    'domains' => [
        'whitelist' => [
            'delete' => 'Email domain has been removed from the white list.',
            'store' => 'Domain has been added to the white list.',
            'exists' => 'This domain already exists on the white list.'
        ],
        'blacklist' => [
            'delete' => 'Email domain has been removed from the black list.',
            'store' => 'Domain has been added to the black list.',
            'exists' => 'This domain already exists on the black list.'
        ],
    ]

];