<?php

return [
    'invalid' => [
        'country' => 'Thank you for your interest in Middle East Doctor. This website is only available for healthcare professionals that are practising in the Middle East.',
        'email'   => 'We are unable to automatically verify your details.'
    ],

    'success' => 'Thank you for signing up! Please check your email and follow the instructions to activate your account.',
    'confirmed' => 'Your email address has been successfully verified and your account on Middle East Doctor is now active. Please log in using your username and password.',
    'with-credentials-success' => 'Thank you for submitting your credentials. Your account details have been sent to the administrator.',
    'verified' => 'User has been verified.',
    'removed' => 'User has been removed.'
];