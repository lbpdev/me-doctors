@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ads |
        <a href="{{ route('admin.ads.index') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Articles</a></li>--}}
        {{--<li class="active">Add New</li>--}}
      {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true , 'id' => 'dataForm']) !!}
            {!! Form::hidden('id' , $ad->id) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                        @if(Session::has('message'))
                            <div class="alert alert-success"> {{ Session::get('message') }} </div>
                        @endif
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Create a new Ad</h3></div>
                              <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-7">

                                            <b>Title</b> : <br>
                                            {!! Form::input('text','title' , $ad->title, [ 'class' => 'form-control' , 'required' ]) !!}

                                            <br><b>Ad Type</b> : <br>
                                            {!! Form::select('ad_type_id', $adTypes, $ad->ad_type_id , [ 'class' => 'margin-b-10 clearfix form-control' ]) !!}
                                            <br>

                                            <br><b>Channel</b> : <br>
                                            {!! Form::select('channel_id', $channels, $ad->channel_id , [ 'class' => 'form-control' ]) !!}
                                            <br>
                                            <b>Description</b> :
                                            {!! Form::textarea('description' , $ad->description, [ 'class' => 'form-control textarea','id' => 'editor' ]) !!}

                                            <br><b>Image</b>: <br>
                                            <img src="{{ $ad->adImage }}" height="100">
                                            {!! Form::input('file', 'image' , null , [ 'class' => 'form-control' ]) !!}<br>

                                            <b>Date Start :</b> <br>
                                            {!! Form::input('text','ad_start' , $ad->ad_start , [ 'class' => 'form-control datepicker' , 'required' ]) !!}<br>

                                            <b>Date End :</b> <br>
                                            {!! Form::input('text','ad_end' , $ad->ad_end , [ 'class' => 'form-control datepicker' , 'required' ]) !!}<br>

                                            <b>Link</b> : <br>
                                            {!! Form::input('text','link' , $ad->link, [ 'class' => 'form-control' , 'required' ]) !!}
                                             <br>
                                          </div>
                                          <div class="col-md-5">
                                            <b>Register to</b>: <br>
                                            {!! Form::input('text','ad_user[name]' , $ad->adUser ? $ad->adUser->name : '', [ 'class' => 'form-control' , 'required' , 'placeholder' => 'Full Name / Company Name']) !!}
                                            <br><b>Email</b>: <br>
                                            {!! Form::input('text','ad_user[email]' , $ad->adUser ?$ad->adUser->email : '', [ 'class' => 'form-control' , 'required' ]) !!}
                                            <br><b>Contact Number</b>: <br>
                                            {!! Form::input('text','ad_user[contact]' , $ad->adUser ? $ad->adUser->contacts[0]->number : '', [ 'class' => 'form-control' , 'required' ]) !!}

                                          </div>
                                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                      <br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/ckeditor/ckeditor.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/surveyFields.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/selectAll.js') }}"></script>

    <script>
      CKEDITOR.replace( 'editor' );

      CKEDITOR.config.format_address = { element: 'address', attributes: { 'class': 'referenceText' } };

      $('.datepicker').datepicker()
    </script>
@stop