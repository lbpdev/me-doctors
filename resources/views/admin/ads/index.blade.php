@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Ads
              </h1>
              {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Ads</a></li>--}}
              {{--</ol>--}}
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                        <a href="{{ route('admin.ads.create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Create an Ad</h3>
                        </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Starts</th>
                            <th>Ends</th>
                            <th>Ad Type</th>
                            <th>Channel</th>
                            <th>Created at</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($ads)
                            @foreach($ads as $ad)
                              <tr>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->id }}</a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}"><img src="{{ $ad->adImage }}" height="50"></a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->title }}</a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->ad_start->format('d M Y') }}</a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->ad_end->format('d M Y') }}</a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->adType->name }}</a></td>
                                <td><a href="#">{{ $ad->channelName }}</a></td>
                                <td><a href="{{ route('admin.ads.edit', $ad->id) }}">{{ $ad->created_at }}</a></td>
                                <td><a href="{{ route('admin.ads.toggle', $ad->id) }}">{{ $ad->status }}</a></td>
                                <td>
                                    <a href="{{ route('admin.ads.edit', $ad->id) }}">Edit</a> /
                                    <a onClick="if(confirm('Are you sure you want to delete this?'))
                                                 return true;
                                                 else return false;" href="{{ route('admin.ads.destroy', $ad->id) }}">Delete</a>
                                </td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                              <th>ID</th>
                              <th>Title</th>
                              <th>Starts</th>
                              <th>Ends</th>
                              <th>Create at</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 0, 'desc' ]
        });
    </script>
@stop