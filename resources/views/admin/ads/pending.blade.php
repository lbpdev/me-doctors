@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Ads
              </h1>
              {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Ads</a></li>--}}
              {{--</ol>--}}
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                        <a href="{{ route('admin.ads.create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Create an Ad</h3>
                        </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>User</th>
                            <th>Image</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($ads)
                            @foreach($ads as $item)
                              <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    {{ $item->product->name }} {{ $item->duration }}(s)<br>
                                    {!! $item->link ? 'Link: <a href='.$item->link->url.' target="_blank">'.$item->link->url.'</a>' : '' !!}
                                </td>
                                <td>
                                    <a href="{{ route($item->pendingAd->user->isDoctor ? 'doctor.profile' : 'patient.profile' , $item->pendingAd->user->username) }}">
                                        {{ $item->pendingAd->user->fullName }}
                                    </a>
                                </td>
                                <td>
                                    @if($item->uploads)
                                        <?php $fileType = ' '.$item->uploads->mime_type; ?>
                                    @endif

                                    @if(!$item->uploads)

                                    @elseif(strpos($fileType, 'image'))
                                        <a target="_blank" href="{{ asset($item->thumbnail) }}"><img src="{{ asset($item->thumbnail) }}" width="90"></a>
                                    @else
                                        <a target="_blank" href="{{ asset($item->uploads->url) }}">{{$item->uploads->original_name}}</a>
                                    @endif
                                </td>
                                <td>

                                @if($item->status==0)
                                    Waiting for Upload

                                @elseif($item->status==1)
                                    <a href="{{ route('admin.ads.approve', $item->id) }}">Approve</a> /
                                    <a onClick="if(confirm('Are you sure you want to delete this?'))
                                                 return true;
                                                 else return false;" href="{{ route('admin.ads.destroy', $item->id) }}">Delete</a>

                                @elseif($item->status==2)
                                    Waiting for Payment

                                @elseif($item->status==3)
                                    Payment Made<br>
                                    <a href="#">Create Ad</a>
                                @endif

                                </td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 0, 'desc' ]
        });
    </script>
@stop