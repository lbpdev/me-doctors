@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Articles |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Articles</a></li>--}}
        {{--<li class="active">Add New</li>--}}
      {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true , 'id' => 'dataForm']) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Add a new Article</h3></div>
                              <div class="col-md-12">
                                      <div class="row">
                                          <div class="col-md-10">
                                            <b>Title</b> : <br>
                                            {!! Form::input('text','title' , null, [ 'class' => 'form-control' , 'required' ]) !!}
                                            <br><b>Sub-title</b> : <br>
                                            {!! Form::input('text','subtitle' , null, [ 'class' => 'form-control' ]) !!}
                                            <br>
                                            <b>Channel</b> : <br>
                                            <select name="channel" class="margin-b-10 clearfix form-control">
                                                @foreach($channels as $channel)
                                                    <option value="{{ $channel['id'] }}">{{ $channel['name'] }}</option>
                                                @endforeach
                                            </select>
                                            <br>
                                            <br>

                                            <div class="select-all-wrapper">
                                                <b>Category</b> : <br>
                                                <select name="therapies[]" multiple="multiple" required="required" class="margin-b-5 form-control">
                                                    @foreach($therapies as $index=>$therapy)
                                                        @if($index==0)
                                                            <option selected value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                                        @endif
                                                        <option value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <br>
                                            <br>
                                            <b>Content</b> :
                                            {!! Form::textarea('content' , null, [ 'class' => 'form-control textarea','id' => 'editor' ]) !!}
                                            <b>Featured Image</b>: <br>
                                            {!! Form::input('file', 'thumbnail' , null , [ 'class' => 'form-control' ]) !!}<br>

                                            <b>Date Posted :</b> <br>
                                            {!! Form::input('text','date' , date('m/d/Y') , [ 'class' => 'form-control datepicker' , 'required' ]) !!}<br>


                                            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                          </div>
                                      <br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/ckeditor/ckeditor.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/surveyFields.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/selectAll.js') }}"></script>

    <script>
      CKEDITOR.replace( 'editor' );

      CKEDITOR.config.format_address = { element: 'address', attributes: { 'class': 'referenceText' } };

      $('select').select2();

      $('#dataForm').submit(function( event ) {
            if($('.select2-selection__choice').length > 0)
                console.log('yes');
            else {
                alert('Please select a category');
                event.preventDefault();
            }
      });


      $('.datepicker').datepicker()
    </script>
@stop