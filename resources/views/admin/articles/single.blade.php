@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="{{ route('adm_articles', $article->channel_id) }}"><i class="fa fa-angle-left"></i> Back</a>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Posts</a></li>--}}
      {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12">
                          <h3><i class="fa fa-check-square-o"></i> {{ $article->title }}</h3>
                          <a target="_blank" href="{{ route('doctor.articles.show', $article->slug) }}">View on site</a>
                          </div>
                              <div class="col-md-12">
                                  <br>

                                      <div class="row">
                                        <div class="col-md-10">
                                        @if($article->thumbnail)
                                        Featured Image: <br>
                                            <img src="{{ asset($article->thumbnail) }}" width="150"><br>
                                        @endif
                                        </div>
                                        <div class="col-md-10"><br>
                                        <b>Title :</b> <br>
                                        {{ $article->title }}<br><br>
                                        <b>Date Posted:</b> <br>
                                        {{ $article->date_posted->format('M D Y') }}<br><br>
                                        <b>Date Created:</b> <br>
                                        {{ $article->created_at->format('M D Y') }}<br><br>
                                        <b>Categories:</b> <br>
                                        @if($article->therapies)
                                            @foreach($article->therapies as $therapy)
                                                {{ $therapy->name }} ,
                                            @endforeach
                                        @endif<br><br>
                                        <b>Content :</b><br>
                                        {!! $article->content !!}<br><br>

                                        </div><br>
                                      </div>
                                  <div class="col-md-12">
                                      <div class="pull-right">
                                            <span class="pull-left"><a href="{{ route('adm_articles_edit', $article['id']) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                            <span class="pull-left">&nbsp;|&nbsp; </span>
                                            <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_articles_destroy', array($article['id']) ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

@stop