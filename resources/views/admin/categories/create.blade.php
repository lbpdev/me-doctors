@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Discussions
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Discussions</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New</h3>
            </div><!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <div class="hs_single_profile_detail">
                    <div class="hs_single_profile_detail">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::open() !!}
                                Name: <br>
                                    {!! Form::input('text','name' , null , [ 'required' , 'class' => 'form-control' ]) !!}
                            </div>
                            <div class="form-group">
                                @if(isset($_GET['type']))
                                    <?php $default = $_GET['type']; ?>
                                @else
                                    <?php $default = 1; ?>
                                @endif
                                    For: <br>
                                        {!! Form::select('for' , $fors , $default , [ 'class' => 'form-control' ]) !!}
                            </div>
                          </div>
                      </div>
                        <div class="voffset4 clearfix"></div>
                            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                        {!! Form::close() !!}
                    </div>
                </div><!-- /.box -->

        </div><!--/.col (right) -->
      </div>   <!-- /.row -->
      </div>   <!-- /.row -->
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop