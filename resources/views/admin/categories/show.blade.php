@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Categries
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Categories</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                        <a href="{{ route('adm_cats_create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add a Category</h3>
                        </a>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>For</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                              <tr>
                                <td>{{ $category->id }}</td>
                                <td><a href="{{ route('adm_cats_single' , $category->id) }}">{{ $category->name }}</a></td>
                                <td>{{ $category->for }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>For</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop