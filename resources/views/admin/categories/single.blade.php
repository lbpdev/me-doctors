@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Categories</a></li>
        <li class="active">{{ $category->name }}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
                <div class="box-body">
                    <div class="hs_single_profile_detail">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          {!! Form::open() !!}
                          {!! Form::hidden('id' , $category->id ) !!}
                            <div class="col-md-8">
                              <div class="row">
                                  <div class="col-md-12 poll-form">
                                    <div class="pull-right">
                                          <span class="pull-left"><a href="{{ route('adm_cats_destroy', array($category['id']) ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                                    </div>
                                    <div class="voffset4 clearfix"></div>
                                        <div class="form-group">
                                            Category Name:
                                            {!! Form::input('text','name' , $category->name , ['class'=>'form-control', 'required'] ) !!}
                                            For: <br>
                                            {!! Form::select('for' , $fors , $category->for , [ 'class' => 'form-control' ]) !!}
                                        </div>
                                  </div>
                              </div>
                            {!! Form::submit('Update', [ 'class' => 'form-control' ]) !!}
                            {!! Form::close() !!}
                            </div>
                        <div class="clearfix"></div>
                      </div>
                </div>
          </div>
        <div class="box-body">
            <div class="hs_single_profile_detail"></div>
        </div><!-- /.box -->

        </div><!--/.col (right) -->
      </div>   <!-- /.row -->
    </div>
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop