@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Comments
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">All Comments</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    @if($forApproval)
                    <h3>Awaiting Approval</h3>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Author</th>
                            <th>Content</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($forApproval as $comment)
                              <tr>
                                <td>{{ $comment->author->fname }} {{ $comment->author->lname }} </td>
                                <td><a href="{{ route('adm_edit_comment', $comment->id ) }}">{{ Str::words($comment->content,5) }}</a></td>
                                <td>
                                    <a href="{{ route('adm_publish_comment', $comment->id) }}">Publish</a> |
                                    <a href="{{ route('adm_edit_comment', $comment->id) }}">Edit</a> |
                                    <a href="{{ route('adm_destroy_comment', $comment->id) }}">Delete</a>
                                </td>
                                <td>{{ $comment->created_at }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <br><br>
                      <hr>
                    @endif

                    <h3>Published Comments</h3>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Author</th>
                            <th>Content</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($comments)>0)
                            @foreach($comments as $comment)
                              <tr>
                              @if($comment->author)
                                <td>{{ $comment->author->fname }} {{ $comment->author->lname }} </td>
                              @else
                                <td>UNKNOWN</td>
                              @endif
                              <td><a href="{{ route('adm_edit_comment',$comment->id) }}">{{ Str::words($comment->content,5) }}</a></td>
                              <td>
                                  <a href="{{ route('adm_unpublish_comment', $comment->id) }}">Unpublish</a> |
                                  <a href="{{ route('adm_edit_comment', $comment->id) }}">Edit</a> |
                                  <a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_destroy_comment', $comment->id) }}">Delete</a>
                              </td>
                              <td>{{ $comment->created_at }}</td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Author</th>
                            <th>Content</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "pageLength": 20,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 2, 'desc' ]
        });

        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "pageLength": 20,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 2, 'desc' ]
        });
    </script>
@stop