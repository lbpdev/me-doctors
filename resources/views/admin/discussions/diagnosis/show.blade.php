@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Diagnosis
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">All Diagnosis</h3><br><br>
                        <a href="{{ route('adm_discussions_create_diag') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add Diagnosis</h3>
                        </a>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    @if($forApproval)
                    <h3>Awaiting Approval</h3>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($forApproval as $discussion)
                              <tr>
                                <td><a href="{{ URL::to('admin/diagnosis/'.$discussion->id) }}">{{ $discussion->title }}</a></td>
                                <td>{{ $discussion->author->fname }} {{ $discussion->author->lname }} </td>
                                <td>
                                    <a href="{{ route('adm_publish_discussion', $discussion->id) }}">Publish</a> |
                                    <a href="{{ route('adm_edit_diag', $discussion->id) }}">Edit</a> |
                                    <a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_destroy_disc', $discussion->id) }}">Delete</a>
                                </td>
                                <td>{{ $discussion->created_at }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <br><br>
                      <hr>
                    @endif

                    <h3>Published Diagnosis</h3>
                      <table id="example2" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($discussions)
                            @foreach($discussions as $discussion)
                                @if($discussion->status==1)
                                  <tr>
                                    <td><a href="{{ URL::to('admin/diagnosis/'.$discussion->id) }}">{{ $discussion->title }}</a></td>
                                    <td>{{ $discussion->author->fname }} {{ $discussion->author->lname }} </td>
                                    <td>{{ $discussion->created_at }}</td>
                                  </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Date Created</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 2, 'desc' ]
        });

        $('#example2').dataTable({
          "pageLength": 20,
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 2, 'desc' ]
        });
    </script>
@stop