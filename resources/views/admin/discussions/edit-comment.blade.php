@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="{{ URL('admin/'. $data->discussion['channel_id'] == 1 ? 'panel' : 'diagnosis' . '/'. $data['discussion_id']) }}"> << Back to Discussion</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Discussions</a></li>
        <li class="active">Edit Discussion</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
            </div><!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="hs_single_profile_detail">
                 {!! Form::open(['files' => true]) !!}
                 {!! Form::hidden('comment_id', $data->id) !!}
                 {!! Form::hidden('type', $data->discussion['channel_id']) !!}
                 {!! Form::hidden('discussion_id', $data['discussion_id']) !!}
                    <div class="hs_single_profile_detail">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                                Content:
                                {!! Form::textarea('content', $data->content, [ 'required', 'class' => 'form-control textarea' ]) !!}
                            </div>
                            <div class="form-group">
                            </div>
                            <div class="form-group">
                            @if(isset($data))
                                @if(count($data->attachments)>0)
                                    Attachment:<br>
                                    {!! Form::hidden('attachment_id',$data->id) !!}
                                    <div class="hs_margin_40 clearfix"></div>
                                    <div class="well well-sm clearfix">
                                        <div class="col-md-12">
                                        @if(preg_match('/image/',$data->attachments[1]->mime_type))
                                        <?php $img_url = URL::to($data->attachments[1]->path.'/'.$data->attachments[1]->file_name); ?>
                                            <a id="single_image" href="{{$img_url}}"><img src="{{$img_url}}" width="100"></a>
                                        @else
                                            <a target="_blank" href="{{ URL::to('download/'.$data->attachments[1]->upload_id) }}">{{$data->attachments[1]->file_name}}</a>
                                        @endif
                                        </div>
                                        <div class="col-md-12">
                                            Remove Current Attachment: {!! Form::checkbox('remove_attachment', 1 , false ) !!}
                                        </div>
                                    </div>
                                @endif
                            @endif
                                Attachment: ( Optional )
                                {!! Form::input('file', 'attachment[]' , null , [ 'class' => 'form-control' ]) !!}
                            </div>
                          </div>
                      </div>
                    <div class="voffset4 clearfix"></div>
                    {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                  {!! Form::close() !!}
                </div>

          </div><!-- /.box -->

        </div><!--/.col (right) -->
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop