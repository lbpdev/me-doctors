@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Diagnosis
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Diagnosis</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
                  <h4 class="hs_heading">
                  <b>Title</b>: {{ $data->title }}
                  <div class="header-action pull-right">
                    <span class="pull-left">
                        @if($data->status==1)
                            <a href="{{ route('adm_unpublish_discussion', $data->id) }}"><i class="fa fa-check"></i> Unpublish </a>
                        @else
                            <a href="{{ route('adm_publish_discussion', $data->id) }}"><i class="fa fa-check"></i> Publish </a>
                        @endif
                    </span>
                    <span class="pull-left margin-rl-10">&nbsp; | &nbsp;</span>
                    <span class="pull-left"><a href="{{ route('adm_edit_diag', $data->id) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                    <span class="pull-left">&nbsp; | &nbsp;</span>
                    <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_destroy_disc', array($data->id ,'type=diagnosis') ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                  </div>
                  </h4>

            </div><!-- /.box-header -->
            <!-- form start -->

                <div class="col-lg-12 col-md-12 col-sm-12">
                  <h4 class="hs_heading"><b>Author Name</b>: {{ $data->author->fname }} {{ $data->author->lname }}</h4>
                  <h4 class="hs_heading"><b>Date Added</b>: {{ $data->createad_at }}</h4><br>
                  <h4 class="hs_heading"><b>Content</b>: <br> {!! $data->content !!}</h4>

                  <div class="hs_margin_30"></div>
                  <div class="hs_comment">
                    <div class="row">
                      <div class="col-lg-1 col-md-1 col-sm-2">
                      </div>
                      <div class="col-lg-11 col-md-11 col-sm-10">
                        <div class="post-content left-pad-20">


                            {{--@if(isset($poll))--}}
                                {{--@include('templates.poll')--}}
                            {{--@endif--}}

                            @if(count($data->attachments)>0)
                                <div class="hs_margin_40 clearfix"></div>
                                <div class="well well-sm">
                                    Attachment: <br>
                                    @if(preg_match('/image/',$data->attachments[0]->mime_type))
                                        <?php $img_url = URL::to($data->attachments[0]->path.'/'.$data->attachments[0]->file_name); ?>
                                        <a id="single_image" href="{{$img_url}}"><img src="{{$img_url}}" width="100"></a>
                                    @else
                                        <a target="_blank" href="{{ URL::to('download/'.$data->attachments[0]->id) }}">{{ $data->attachments[0]->original_name }}</a>
                                    @endif
                                </div>
                            @endif

                          </div>
                        </div>
                    </div>
                  </div><br><br>
                  <div class="hs_sub_comment_div">
                  <b>Comments</b> : <br><br>
                  @if(count($data->comments)>0)
                    @foreach($data->comments as $comment)
                        <div class="hs_sub_comment">
                            <div class="col-lg-11 col-md-11 col-sm-11">
                              <div class="hs_comment">
                                  <div class="{{ $comment->status==1 ? '' : 'alert alert-warning' }} col-lg-11 col-md-10 col-sm-10 z-index-1 well">
                                    <div class="hs_comment_date">

                                      <p class="hs_heading"><b>From</b>: {{ $comment->author->fname}} {{ $comment->author->lname}} , {{ $data->date_added }}</p>
                                      <p class="hs_heading"><b>Content</b>: <br> {!! $comment->content !!}</p>
                                      @if($comment->status==1)
                                          <span class="pull-left"><a href="{{ route('adm_unpublish_comment', $comment->id) }}">Unpublish</a> | </span>
                                      @else
                                          <span class="pull-left"><a href="{{ route('adm_publish_comment', $comment->id) }}">Publish</a> | </span>
                                      @endif
                                          <span class="pull-left"><a href="{{ route('adm_edit_comment', $comment->id ) }}"> Edit | </a></span>
                                          <span class="pull-left"><a href="{{ route('adm_destroy_comment', $comment->id) }}"> Delete</a></span>
                                    </div>

                                    @if(count($comment->attachments)>0)
                                        <div class="hs_margin_40 clearfix"></div>
                                        <div class="well well-sm">
                                            Attachment: <br>
                                            {{--@foreach($comment->attachments as $attachment)--}}
                                                @if(preg_match('/image/',$comment->attachments[0]->mime_type))
                                                <?php $img_url = URL::to($comment->attachments[0]->path.'/'.$comment->attachments[0]->file_name); ?>
                                                    <a id="single_image" href="{{$img_url}}"><img src="{{$img_url}}" width="100"></a>
                                                @else
                                                    <a target="_blank" href="{{ URL::to('download/'.$comment->attachments[0]->upload_id) }}">{{$comment->attachments[0]->file_name}}</a>
                                                @endif
                                            {{--@endforeach--}}
                                        </div>
                                    @endif
                                    </div>
                            </div>
                          </div>
                        </div>
                    @endforeach
                  @endif
                  </div>
                  <div class="hs_margin_40 clearfix"></div>
                  <div class="hs_comment_form">
                    <h4 class="hs_heading">Comment</h4>

                        {!! Form::open(['files' => true]) !!}

                        {!! Form::input('hidden','discussion_id' , $data->id , [ 'class' => 'form-control' , 'rows' => '8' ]) !!}
                        {!! Form::input('hidden','admin' , 1 ) !!}
                        {!! Form::input('hidden','type' , 'panel' ) !!}
                        {!! Form::textarea('content', null, [ 'class' => 'form-control' , 'rows' => '8' , 'required' ]) !!}

                        Attachment: ( Optional )
                        {!! Form::input('file', 'attachment[]' , null , [ 'class' => 'form-control' ]) !!}

                        <div class="hs_margin_40 clearfix"></div>
                        <div class="form-group">
                          <div class="col-lg-6 col-md-6 col-sm-12">
                            {!! Form::submit('Send', [ 'class' => 'btn btn-success pull-right' ]) !!}
                          </div>

                          {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            <div class="box-body">
                <div class="hs_single_profile_detail">
                </div>

          </div><!-- /.box -->

        </div><!--/.col (right) -->
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop