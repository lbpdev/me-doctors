@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Data Tables
                <small>advanced tables</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Table With Full Features</h3><br><br>

                      <div class="col-md-2">
                        <a href="{{ route('adm_discussions_create_panel') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add Panel Discussion</h3>
                        </a>
                      </div>
                      <div class="col-md-2">
                        <a href="{{ route('adm_discussions_create_diag') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add Diagnosis</h3>
                        </a>
                      </div>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Author Name</th>
                            <th>Author Username</th>
                            <th>Category</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($discussions as $discussion)
                              <tr>
                                <td>{{ $discussion->title }}</td>
                                <td>{{ $discussion->author_fname }} {{ $discussion->author_lname }} </td>
                                <td>{{ $discussion->author_username }} </td>
                                <td>{{ $discussion->category }}</td>
                                <td>{{ $discussion->created_at }}</td>
                              </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Title</th>
                            <th>Author Name</th>
                            <th>Author Username</th>
                            <th>Category</th>
                            <th>Date Created</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop