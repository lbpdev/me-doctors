{!! Form::open(['url' => $url, 'method' => 'DELETE']) !!}
    {!! Form::submit('Delete', ['class' => 'btn-link']) !!}
{!! Form::close() !!}