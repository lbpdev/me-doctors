@extends(' admin.master')

@section('content')
    <section class="content-header">
          <h1>Email Domains Blacklist</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li class="active">Email Domains Blacklist</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">

                        {!! Form::open([
                            'route' => 'emails.domains.blacklist.store', 
                            'class' => 'form-inline box-form ' . ($errors->first('domain') ? 'has-error' : '')
                        ]) !!}
                            <h4><b>Add new email domain to black list:</b></h4>
                            <div class="form-group">
                                {!! Form::text('domain', null, ['class' => 'form-control']) !!}
                                {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
                            </div>
                            {!! $errors->first('domain','<span class="help-block">:message</span>') !!}
                        {!! Form::close() !!}
                        
                        <table id="domainsWhitelist" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Domain</th>
                                        </tr>
                                    </thead>

                                    @foreach($domains as $domain)
                                        <tr>
                                            <td>{{ $domain->id }}</td>
                                            <td>{{ $domain->name }}</td>
                                        </tr>
                                    @endforeach
                                </table>

                    <?php echo $domains->render(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        (function($) {
            $("#domainsBlacklist").dataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ URL::current() }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    {
                        data: 'action',
                        searchable: false,
                        orderable: false
                    }
                ]
            });
        })(jQuery);
    </script>
@stop