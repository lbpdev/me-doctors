@extends(' admin.master')

@section('content')
    <section class="content-header">
          <h1>Emails Whitelist</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li class="active">Emails Whitelist</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">

                        {!! Form::open([
                            'route' => 'emails.whitelist.store', 
                            'class' => 'form-inline box-form ' . ($errors->first('email') ? 'has-error' : '')
                        ]) !!}
                            <h4><b>Add new email to white list:</b></h4>
                            <div class="form-group">
                                {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
                            </div>
                            {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                        {!! Form::close() !!}

                        <table id="emailsWhitelist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            @foreach($emails as $email)
                                <tr>
                                    <td>{{ $email->id }}</td>
                                    <td>{{ $email->email }}</td>
                                </tr>
                            @endforeach
                        </table>

                    <?php echo $emails->render(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
@stop