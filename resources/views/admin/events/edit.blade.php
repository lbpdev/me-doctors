@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Events |
        <a href="{{ route('adm_events') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Events</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            {!! Form::hidden('id', $event->id ) !!}

            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Edit Event</h3></div>
                              <div class="col-md-12">
                                  <br
                                      <div class="row">
                                        <div class="col-md-10">

                                        Title : <br>
                                        {!! Form::input('text','title' , $event->title, [ 'class' => 'form-control' , 'required' ]) !!}

                                        Date : <br>
                                        {!! Form::input('text','date' ,  date('m/d/Y', strtotime($event->event_date) ) , [ 'class' => 'form-control datepicker' , 'required' ]) !!}
                                        {!! Form::input('text','date_start' , $event->event_date->format('m/d/Y')  , [ 'id' => 'date_start', 'class' => 'hidden form-control' , 'required' ]) !!}
                                        {!! Form::input('text','date_end' , $event->event_date_end->format('m/d/Y') , [ 'id' => 'date_end', 'class' => 'hidden form-control' , 'required' ]) !!}

                                        Content : <br>
                                        {!! Form::textarea('content' , $event->content, [ 'class' => 'form-control textarea' ]) !!}

                                        Featured Image: <br>
                                        @if($event->thumbnail)
                                            <img src="{{ URL::to($event->thumbnail) }}" width="150"><br>
                                            Remove Thumbnail: {!! Form::checkbox('remove_thumb' ,1, false ) !!}
                                        @endif

                                        {!! Form::input('file', 'thumbnail' , null, [ 'class' => 'form-control' ]) !!}

                                        Location : <br>
                                        {!! Form::input('text','location' , $event->location, [ 'class' => 'form-control' , 'required' , 'id' => 'autocomplete' ]) !!}

                                        <table id="address" class="hidden"Catego>
                                              <tr>
                                                <td class="label">Street address</td>
                                                <td class="slimField"><input class="field" id="street_number"></input></td>
                                                <td class="wideField" colspan="2"><input class="field" id="route"></input></td>
                                              </tr>
                                              <tr>
                                                <td class="label">City</td>
                                                <td class="wideField" colspan="3"><input name="city" class="field" id="locality"
                                                      value="{{ $event->city }}"></input></td>
                                              </tr>
                                              <tr>
                                                <td class="label">State</td>
                                                <td class="slimField"><input name="state" class="field"
                                                      id="administrative_area_level_1" value="{{ $event->state }}"></input></td>
                                                <td class="label">Zip code</td>
                                                <td class="wideField"><input class="field" id="postal_code"></input></td>
                                              </tr>
                                              <tr>
                                                <td class="label">Country</td>
                                                <td class="wideField" colspan="3"><input name="country" class="field"
                                                      id="country" value="{{ $event->country }}"></input></td>
                                              </tr>
                                            </table>
                                        <br>
                                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                      </div><br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });

        $('.datepicker').daterangepicker(
        {
            startDate: '<?php echo $event->event_date->format('m/d/Y') ?>',
            endDate: '<?php echo $event->event_date_end->format('m/d/Y') ?>'
        });

        $('.datepicker').on('apply.daterangepicker', function(ev, picker) {
          //do something, like clearing an input
          $('#date_start').attr('value',picker.startDate.format('MM/DD/YY'));
          $('#date_end').attr('value',picker.endDate.format('MM/DD/YY'));
        });

        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };

        function initialize() {
          // Create the autocomplete object, restricting the search
          // to geographical location types.
          autocomplete = new google.maps.places.Autocomplete(
              /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
              { types: ['geocode'] });
          // When the user selects an address from the dropdown,
          // populate the address fields in the form.
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
          });
        }

        function fillInAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();

          for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
          }

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
            }
          }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = new google.maps.LatLng(
                  position.coords.latitude, position.coords.longitude);
              var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
              });
              autocomplete.setBounds(circle.getBounds());
            });
          }
        }

        initialize();
    </script>

@stop