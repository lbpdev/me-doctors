@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Events |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Events</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12">
                          <h3><i class="fa fa-check-square-o"></i>{{ $event->title }}
                          <div class="pull-right">
                                <span class="pull-left"><a href="{{ route('adm_event_edit', $event['id']) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                                <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_event_destroy', array($event['id']) ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div></h3>
                          </div>
                              <div class="col-md-12">
                                  <br>

                                      <div class="row">
                                        <div class="col-md-10">
                                        @if($event->thumbnail)
                                            <img src="{{ URL::to($event->thumbnail) }}" width="250">
                                        @endif
                                        </div>
                                        <div class="col-md-10"><br>
                                        <b>Title :</b> <br>
                                        {{ $event->title }}<br><br>

                                        <b>Date :</b> <br>
                                        Start: {{ date('m/d/Y', strtotime($event->event_date) )  }}<br>
                                        End: {{ date('m/d/Y', strtotime($event->event_date_end) )  }}<br><br>

                                        <b>Location :</b> <br>
                                        {{ $event->location }}<br><br>

                                        <b>Content :</b><br>
                                        {!! $event->content !!}<br><br>

                                      </div><br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

@stop