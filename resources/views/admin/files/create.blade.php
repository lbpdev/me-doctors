@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Articles |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Articles</a></li>--}}
        {{--<li class="active">Add New</li>--}}
      {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true , 'id' => 'dataForm' , 'route' => 'admin.files.store']) !!}
            <!-- left column -->
              <div class="col-md-12 col-sm-12">
                  <div class="box box-primary">
                    <div class="box-header with-border">
                    <div class="qualifications">
                      <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Add a new Article</h3></div>
                          <div class="col-md-12">
                              <div class="row">
                                  <div class="col-md-10">
                                    <b>Channel</b> : <br>
                                    {!! Form::select('upload_type', $upload_types , $section , [ 'class' => 'form-control' ]) !!}<br>
                                    <br><b>File(s)</b>: <br>
                                    {!! Form::input('file', 'files[]' , null , [ 'class' => 'form-control', 'multiple' ]) !!}<br>
                                    {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                  </div>
                              <br>
                              </div>
                          </div>
                    </div>
                        <br>
                    </div>
                  </div>
              </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop
