@extends('admin.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Files
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Files</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Files</h3><br><br>

                    <h3 class="box-title"><i class="fa fa-plus-circle"></i> Upload a File</h3>

                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>URL</th>
                            <th>Template</th>
                            <th>Type</th>
                            <th>Date Uploaded</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($files)
                            @foreach($files as $file)
                            <tr>
                                <td><a href="{{ route('admin.files.show', $file->id) }}">{{ $file->id }}</a></td>
                                <td>
                                    <a href="{{ route('admin.files.show', $file->id) }}">
                                        <img src="{{ asset('public/'.$file->url) }}" width="50">
                                        {{ $file->original_name }}
                                    </a>
                                </td>
                                <td><a href="{{ asset('public/'.$file->url) }}" target="_blank">
                                    <span style="font-size: 12px;">{{ asset('public/'.$file->url) }}</a></span>
                                </td>
                                <td><a href="{{ route('admin.files.show', $file->id) }}">{{ $file->template }}</a></td>
                                <td><a href="{{ route('admin.files.show', $file->id) }}">{{ $file->mime_type }}</a></td>
                                <td><a href="{{ route('admin.files.show', $file->id) }}">{{ $file->created_at->format('D M Y') }}</a></td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>URL</th>
                            <th>Template</th>
                            <th>Type</th>
                            <th>Date Uploaded</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop

@section('js')
<!-- DATA TABES SCRIPT -->

<script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $("#example1").dataTable();
    $('#example2').dataTable({
        "bPaginate": true,
          "pageLength": 20,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
</script>
@stop
