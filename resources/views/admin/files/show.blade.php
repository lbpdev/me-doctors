@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        File Details
        <a href="
            @if($file->uploadable_type == 'MEDoctors\Models\Hospital')
                {{ route('admin.files.hospitals') }}
            @elseif($file->uploadable_type == 'MEDoctors\Models\Article')
                {{ route('admin.files.articles') }}
            @elseif($file->uploadable_type == 'MEDoctors\Models\Event')
                {{ route('admin.files.events') }}
            @else
                {{ route('admin.files.index') }}
            @endif
        " class="pull-right">Back</a>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12">
                          <h3><i class="fa fa-hospital-o"></i> File</h3>
                          <div class="pull-right">
                                <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('admin.files.destroy', $file->id ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div></h3>
                          </div>
                              <div class="col-md-12 col-sm-12">
                                <div class="box box-primary">
                                  <div class="box-header with-border">
                                  <div class="qualifications">
                                        <div class="col-md-12">
                                            <div class="fields padding-b-5 border-bottom clearfix">
                                                <div class="col-md-6 padding-l-0">
                                                    <h3>{{ $file->original_name }}</h3>
                                                    <b>File name :</b> {{ $file->file_name }} <br>
                                                    <b>Path :</b> {{ $file->path }} <br>
                                                    <b>Mime Type :</b> {{ $file->mime_type }} <br>
                                                    <b>Upload Type :</b> {{ $file->uploadable_type }} <br>
                                                    <b>Uploaded on :</b> {{ $file->created_at }} <br>
                                                </div>

                                                <div class="col-md-6 padding-l-0 margin-b-20">
                                                      @if($file->url)
                                                          <img id="thumb" src="{{ asset($file->url) }}" width="100%">
                                                      @endif
                                                </div>


                                            </div>

                                        </div>
                                        <br>
                                            </div>
                                        </div>
                                      <br>
                                </div><!--/.col (right) -->
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
    </section>   <!-- /.row -->
@stop

@section('js')
@stop