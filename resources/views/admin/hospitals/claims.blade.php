@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Hospitals
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Hospitals</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">All Hospitals</h3><br><br>
                        <a href="{{ route('admin.hospitals.create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add a Hospital</h3>
                        </a>
                      </a>

                      @if(Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                      @endif
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Hospital</th>
                            <th>User</th>
                            <th>User Type</th>
                            <th>Email</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($claims)
                            @foreach($claims as $claim)
                                @if($claim->hospital)
                                  <tr>
                                    <td>{{ $claim->id }}</td>
                                    <td><a class="text-uppercase" href="{{ route('doctor.hospitals.show', $claim->hospital->slug) }}">{{ $claim->hospital->name }}</a></td>
                                    <td>
                                        <a class="" href="{{ $claim->claimer->roles[0]->id== 3 ? route('patient.profile', $claim->claimer->username) : route('doctor.profile', $claim->claimer->username) }}">
                                            {{ $claim->claimer->name }}
                                        </a>
                                    </td>
                                    <td>{{ $claim->claimer->roles[0]->name }}</td>
                                    <td>{{ $claim->email }}</td>
                                    <td>
                                    @if(!$claim->granted)
                                        <a href="{{ route('admin.hospitals.claims.grant', $claim->id) }}">Grant</a> /
                                        <a href="{{ route('admin.hospitals.claims.deny', $claim->id) }}">Deny</a>
                                    @else
                                        @if($claim->verified)
                                            <b>Claimed</b>
                                        @else
                                            <a href="{{ route('admin.hospitals.claims.resend', $claim->id) }}">Resend Verification</a>
                                        @endif
                                    @endif
                                    </td>
                                  </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Hospital</th>
                            <th>User</th>
                            <th>User Type</th>
                            <th>Email</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop