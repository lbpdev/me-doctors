@extends('admin.master')

@section('style')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/css/trumbowyg.css') }}" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hospital |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Hospital</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true , 'route' => 'admin.hospitals.store' , 'method' => 'POST']) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Add Hospital</h3></div>
                              <div class="col-md-12">
                                      <div class="fields padding-b-5 border-bottom clearfix">
                                          <div class="col-md-6 padding-l-0">
                                              <input type="text" value="" name="hospital[name]" placeholder="Hospital Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                                              <input type="text" value="" name="hospital[website]" placeholder="Website" autocomplete="false"  class="margin-b-5 margin-t-5 form-control">
                                              <input type="text" value="" name="contacts[]" placeholder="Contact Number" autocomplete="false"  class="margin-b-5 margin-t-5 form-control">
                                              <br>Therapy Areas : <br>
                                              <select name="therapies[]" multiple="multiple" class="margin-b-5 form-control">
                                                  @foreach($therapies as $index=>$therapy)
                                                      <option value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                                  @endforeach
                                              </select><br>
                                              <br>Facilities : <br>
                                              <select name="facilities[]" multiple="multiple" class="margin-b-5 form-control">
                                                  @foreach($facilities as $index=>$facility)
                                                      <option value="{{ $facility['id'] }}">{{ $facility['name'] }}</option>
                                                  @endforeach
                                              </select>
                                          </div>

                                          <div class="col-md-6 padding-l-0 margin-b-20">
                                              <img id="thumb" src="{{ asset('public/images/placeholders/hospital.png') }}" width="100%">
                                              {!! Form::input('file', 'thumbnail' , null , [ 'class' => 'form-control' ]) !!}
                                          </div>

                                          <hr>
                                          <h4>Description</h4>
                                          <div class="col-md-12 padding-b-20 padding-l-0">
                                            <textarea name="hospital[description]" placeholder="Hospital Description" autocomplete="false" style="height: 300px" class="textarea margin-b-5 margin-t-5 form-control"></textarea>
                                          </div>
                                      </div>

                                      <div class="col-md-12 padding-b-20">
                                      <h4>Location</h4>
                                      <div class="col-md-6 padding-l-0">
                                      {!! Form::input('text','location[name]' , null, [  'class' => 'form-control location' , 'id' => 'autocomplete' , 'placeholder' => 'Address']) !!}

                                          <table id="address" width="100%">
                                                <tr><td>Street</td><td><input value="" name="location[street_number]" class="field street_number form-control margin-tb-5" id="street_number"></td></tr>
                                                <tr><td>Route</td><td><input value="" name="location[street_route]" class="field route form-control margin-tb-5" id="route"></td></tr>
                                                <tr><td>City</td><td><input value="" name="location[city]" class="field locality form-control margin-tb-5" id="locality"></td></tr>
                                                <tr style="display:none;" ><td>State</td><td><input value="" name="location[state]" class="field administrative_area_level_1 form-control margin-tb-5" id="administrative_area_level_1"></td></tr>
                                                <tr><td>Post Code</td><td><input value="" name="location[post_code]" class="field postal_code form-control margin-tb-5" id="postal_code"></td></tr>
                                                <tr><td>Country</td><td><input value="" name="location[country]" class="field country form-control margin-tb-5" id="country"></td></tr>
                                          </table>
                                      </div>


                                      <div class="col-md-6 no-padding">
                                          <div class="map">
                                          </div>
                                      </div>

                                      {!! Form::submit('Update', [ 'class' => 'form-control margin-t-20 pull-left' ]) !!}
                                      </div>

                              </div>
                              <br>
                                  </div>
                              </div>
                            <br>
                      </div><!--/.col (right) -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script src="{{ asset('public/js/custom/updateImage.js') }}"></script>

    <script>
    $('select').select2().required = false;

    initMap();
        function initMap() {
            var map = new google.maps.Map($('.map')[0], {
                zoom: 12,
                disableDefaultUI: true,
//                draggable: false,
//                zoomControl: false,
//                scrollwheel: false,
//                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow;

            //Add listener
            google.maps.event.addListener(map, "click", function (event) {
                var latitude = event.latLng.lat();
                var longitude = event.latLng.lng();
                console.log( latitude + ', ' + longitude );

                radius = new google.maps.Circle({map: map,
                    radius: 100,
                    center: event.latLng,
                    fillColor: '#777',
                    fillOpacity: 0.1,
                    strokeColor: '#AA0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    draggable: true,    // Dragable
                    editable: true      // Resizable
                });

                // Center of map
                map.panTo(new google.maps.LatLng(latitude,longitude));

                geocodeLatLng(geocoder, map, infowindow, latitude, longitude);
            }); //end addListener

            geocodeAddress(geocoder, map);
          }

        function geocodeLatLng(geocoder, map, infowindow , latitude , longitude) {
          var latlng = {lat: latitude, lng: longitude};
          geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              if (results) {
                map.setZoom(11);


                var marker = new google.maps.Marker({
                  position: latlng,
                  map: map
                });

                $('.location')[0].value = results[1].formatted_address;
                google.maps.event.trigger(autocomplete, 'place_changed');

              } else {
                window.alert('No results found');
              }
            } else {
              window.alert('Geocoder failed due to: ' + status);
            }
          });
        }

      function geocodeAddress(geocoder, resultsMap) {
        var address = $('.location')[0].value;

        if(address=="")
            address = "Dubai - United Arab Emirates";

        geocoder.geocode({'address': address}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {}
        });
      }

        $('.datepicker').datepicker()

        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };

        function initialize() {
          // Create the autocomplete object, restricting the search
          // to geographical location types.
          autocomplete = new google.maps.places.Autocomplete(
              /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
              { types: ['geocode'] });
          // When the user selects an address from the dropdown,
          // populate the address fields in the form.
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
            initMap();
          });
        }

        function fillInAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();

          console.log(place);

          for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
          }

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
            }
          }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = new google.maps.LatLng(
                  position.coords.latitude, position.coords.longitude);
              var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
              });
              autocomplete.setBounds(circle.getBounds());
            });
          }
        }

        initialize();

        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

@stop