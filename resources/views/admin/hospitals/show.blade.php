@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hospitals |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">

                      @if($hospital)
                          <div class="col-md-12">
                          <h3><i class="fa fa-hospital-o"></i> Hospital</h3>
                          <div class="ool-md-12 padding-10">
                              @if($hospital->isPremium == 1)
                                  <h4>Premium User ( Active ) ( Expires on : {{ $hospital->services->expires_on->format('M d, Y') }} )</h4>
                                  <a href="{{ route('admin.hospitals.toggleService', [$hospital->id]) }}">
                                      <h4><i class="fa fa-remove"></i> Pause Premium Service</h4>
                                  </a>
                              @elseif($hospital->isPremium == 2)
                                  <h4>Service Paused ( Expires on : {{ $hospital->services->expires_on->format('M d, Y') }} )</h4>
                                  <a href="{{ route('admin.hospitals.toggleService', [$hospital->id]) }}">
                                      <h4><i class="fa fa-remove"></i> Resume Premium Service</h4>
                                  </a>
                              @else
                                  <a href="{{ route('admin.hospitals.availService', [$hospital->id,'platinum-account']) }}">
                                      <h4><i class="fa fa-check-circle-o"></i>Turn Premium</h4>
                                  </a>
                              @endif
                          </div>
                          <div class="pull-right">
                          @if($hospital->verified==0)
                                <span class="pull-left"><a href="{{ route('admin.hospitals.verify', $hospital->id) }}"><i class="fa fa-check"></i> Verify </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                          @endif
                                <span class="pull-left"><a href="{{ route('admin.hospitals.edit', $hospital->slug) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                                <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('admin.hospitals.delete', $hospital->id ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div></h3>
                          </div>
                              <div class="col-md-12 col-sm-12">
                                <div class="box box-primary">
                                  <div class="box-header with-border">
                                  <div class="qualifications">
                                        <div class="col-md-12">
                                            <div class="fields padding-b-5 border-bottom clearfix">
                                                <div class="col-md-6 padding-l-0">
                                                    <h3 class="text-uppercase" >{{ $hospital->name }} ( {{ $hospital->verified==1 ? 'Verified' : '' }} )</h3>

                                                    <b>Website :</b> <br>
                                                    <p>{{ $hospital->website ? $hospital->website : 'N/A' }}</p>

                                                    <b>Contact Numbers :</b>  <br>
                                                    @if (count($hospital->contacts) > 0)
                                                        {{ implode(', ', $hospital->contacts->lists('number'))}}
                                                    @else
                                                        N/A
                                                    @endif

                                                    <br><br><b>Therapy Areas :</b>  <br>
                                                    @if(count($hospital->therapies)>0)
                                                        {{ implode(', ', $hospital->therapies->lists('name')) }}
                                                    @else
                                                        N/A
                                                    @endif

                                                    <br><br><b>Facilities :</b>  <br>
                                                    @if (count($hospital->facilities) > 0)
                                                        {{ implode(', ', $hospital->facilities->lists('name'))}}
                                                    @else
                                                        N/A
                                                    @endif
                                                </div>

                                                <div class="col-md-6 padding-l-0 margin-b-20">

                                                      @if($hospital->thumbnail)
                                                          <img id="thumb" src="{{ asset($hospital->thumbnail) }}" width="100%">
                                                      @endif
                                                </div>

                                            <div class="col-md-12 padding-b-20 padding-l-0">
                                                <h4><b>Description</b></h4>
                                                {!! $hospital->description ? $hospital->description : '' !!}
                                            </div>

                                            <div class="col-md-12 padding-t-20">
                                            <h4><b>Location</b></h4>
                                            <div class="col-md-6 padding-l-0">
                                            {!! $hospital->location ? $hospital->location->name : '' !!}

                                                <table id="address" width="100%">
                                                      <tr><td>Street</td><td>{{ $hospital->location ? $hospital->location->street_number : ''  }}</td></tr>
                                                      <tr><td>Route</td><td>{{ $hospital->location ? $hospital->location->street_route : ''  }}</td></tr>
                                                      <tr><td>City</td><td>{{ $hospital->location ? $hospital->location->city : ''  }}</td></tr>
                                                      <tr style="display:none;"><td>State</td><td>{{ $hospital->location ? $hospital->location->state : ''  }}</td></tr>
                                                      <tr><td>Post Code</td><td>{{ $hospital->location ? $hospital->location->post_code : ''  }}</td></tr>
                                                      <tr><td>Country</td><td>{{ $hospital->location ? $hospital->location->country : ''  }}</td></tr>
                                                </table>
                                            </div>

                                            <div class="col-md-6 no-padding">
                                                <div class="map">
                                                </div>
                                            </div>

                                            </div>

                                        </div>
                                        <br>
                                            </div>
                                        </div>
                                      <br>
                                </div><!--/.col (right) -->
                          </div>
                            <br>
                          </div>
                      @else
                       <p>Hospital no longer exists.</p>
                      @endif
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script>
    initMap();
        function initMap() {
            var map = new google.maps.Map($('.map')[0], {
                zoom: 12,
                disableDefaultUI: true,
//                draggable: false,
//                zoomControl: false,
//                scrollwheel: false,
//                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map);
          }

          function geocodeAddress(geocoder, resultsMap) {
            var address = "<?php echo $hospital->location ? $hospital->location->name : ''; ?>";
            if(address=="")
                address = "Dubai - United Arab Emirates";

            geocoder.geocode({'address': address}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
              } else {}
            });
          }
    </script>
@stop