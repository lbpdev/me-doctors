@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Hospitals
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Hospitals</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Unverified Hospitals</h3><br><br>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($hospitals)
                            @foreach($hospitals as $hospital)
                              <tr>
                                <td><a href="{{ route('admin.hospitals.show', $hospital->slug) }}">{{ $hospital->id }}</a></td>
                                <td><a class="text-uppercase" href="{{ route('admin.hospitals.show', $hospital->slug) }}">{{ $hospital->name }}</a></td>
                                <td>
                                    <a href="{{ route('admin.hospitals.verify', $hospital->id) }}">Publish</a> |
                                    <a href="{{ route('admin.hospitals.edit', $hospital->slug) }}">Edit</a> |
                                    <a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('admin.hospitals.deleteUnverified', $hospital->id ) }}">Delete</a>
                                </td>
                                <td>{{ $hospital->created_at }}</td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                            <th>Date Created</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop