@include('admin.templates.header')
@include('admin.templates.sidebar')

@yield('style')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('partials.flash_message')

        @yield('content')
    </div>

@include('admin.templates.footer')

@yield('js')

