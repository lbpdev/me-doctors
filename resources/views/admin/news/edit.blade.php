@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        News Feed |
        <a href="{{ route('admin.news.channel', $news->channel_id) }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      {{--<ol class="breadcrumb">--}}
        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
        {{--<li><a href="#">Posts</a></li>--}}
        {{--<li class="active">Add New</li>--}}
      {{--</ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true , 'id' => 'dataForm' , 'method' => 'put', 'route' => 'admin.news.update']) !!}
            {!! Form::hidden('id', $news->id ) !!}

            <!-- left column -->
      <div class="col-md-12 col-sm-12">
          <div class="box box-primary">
            <div class="box-header with-border">
            <div class="qualifications">
              <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Edit News Feed</h3></div>
                  <div class="col-md-12">
                      <br>
                      <div class="row">
                        <div class="col-md-10">
                            Title : <br>
                            {!! Form::input('text','title' , $news->title, [ 'class' => 'form-control' , 'required' ]) !!}

                            <br><b>Sub-title</b> : <br>
                            {!! Form::input('text','subtitle' , $news->subtitle, [ 'class' => 'form-control']) !!}

                             <br><b>Channel</b> : <br>
                            <select name="channel_id" class="margin-b-10 clearfix form-control">
                                @foreach($channels as $channel)
                                    <option {{ $channel['id'] == $news->channel_id  ? 'selected' : '' }} value="{{ $channel['id'] }}">{{ $channel['name'] }}</option>
                                @endforeach
                            </select>


                            <br><br>Content : <br>
                            {!! Form::textarea('content' , $news->content, [ 'class' => 'form-control textarea' , 'id' => 'editor' ]) !!}

                            Featured Imagse: <br>
                            @if($news->thumbnail)
                                <img src="{{ asset($news->thumbnail) }}" width="150"><br>
                                Remove Thumbnail: {!! Form::checkbox('remove_thumb' ,1, false ) !!}
                            @endif
                            {!! Form::input('file', 'thumbnail' , null, [ 'class' => 'form-control' ]) !!}
                            <br>

                            <b>Date Posted :</b> <br>
                            {!! Form::input('text','date' , $news->date_posted->format('m/d/Y') , [ 'class' => 'form-control datepicker style-default' , 'required', 'readonly' ]) !!}<br>

                            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                        </div><br>
                      </div>
                  </div>
              </div>
            <br>
            </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop


@section('js')

    <script src="{{ asset('public/js/dist/ckeditor/ckeditor.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/selectAll.js') }}"></script>
    <script>

    CKEDITOR.replace( 'editor' );

    CKEDITOR.config.format_address = { element: 'address', attributes: { 'class': 'referenceText' } };

    $('.datepicker').datepicker()
    </script>
@stop