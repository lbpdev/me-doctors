@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
               @if(Request::is('admin/articles/channel/3'))
                    Patient
               @elseif(Request::is('admin/articles/channel/2'))
                    Doctor
               @endif
                News Feed
              </h1>
              {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Posts</a></li>--}}
              {{--</ol>--}}
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                        <a href="{{ route('admin.news.create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add News Feed</h3>
                        </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Edit</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($news)
                            @foreach($news as $newsfeed)
                              <tr>
                                <td><a href="{{ route('admin.news.show', $newsfeed->slug) }}">{{ $newsfeed->id }}</a></td>
                                <td><a href="{{ route('admin.news.show', $newsfeed->slug) }}">{{ $newsfeed->title }}</a></td>
                                <td>{{ $newsfeed->created_at }}</td>
                                <td>
                                <a href="{{ route('admin.news.edit', $newsfeed->id) }}"> Edit </a> /
                                <a onClick="if(confirm('Are you sure you want to delete this?'))
                                   return true;
                                   else return false;" href="{{ route('admin.news.delete', $newsfeed->id) }}"> Delete </a></td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Edit</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 0, 'desc' ]
        });
    </script>
@stop