@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Polls |
        <a href="{{ route('adm_polls') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Polls</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open() !!}
            {!! Form::hidden('id' , $poll->id ) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Create a Poll</h3></div>
                              {!! Form::open(['files' => true]) !!}
                                <div class="col-md-8">
                                  <div class="row">
                                      <div class="col-md-12 poll-form">
                                        <div class="voffset4 clearfix"></div>
                                            <div class="form-group">
                                                Poll Title:
                                                {!! Form::input('text','title' , $poll->title , ['class'=>'form-control poll-item', 'required'] ) !!}

                                                @foreach($poll['items'] as $index=>$item)
                                                <div class="poll_item_group pi_group1">
                                                    @if($index>1)
                                                        <i class="remove_poll_item fa fa-minus-circle"></i>
                                                    @endif
                                                    Item {{ $index+1 }}:
                                                    {!! Form::hidden('item_id[]' , $item['id'] ) !!}
                                                    {!! Form::input('text','items[]' , $item->body , ['class'=>'form-control poll-item', 'required'] ) !!}
                                                </div>
                                                @endforeach
                                            </div>
                                        <a href="#" class="pull-right" id="add_poll_item">Add item <i class="fa fa-plus-circle"></i> </a>
                                      </div>
                                  </div>
                                <div class="voffset4 clearfix"></div>
                                {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                {!! Form::close() !!}
                                </div>
                            <div class="clearfix"></div>
                          </div>

                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/custom/lbp.js') }}"></script>
@stop