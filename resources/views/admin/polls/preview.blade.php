@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surveys |
        <a href="{{ route('adm_surveys_single', $survey['id']) }}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Surveys</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open() !!}
            {!! Form::hidden('survey_id',$survey['id']) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <div class="col-md-12">
                          <h3 class="inner-title"><i class="fa fa-check-square-o"></i>{{ $survey['title'] }}
                          <div class="pull-right">
                                <span class="pull-left"><a href="{{ route('adm_surveys_edit', $survey['id']) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                                <span class="pull-left"><a href="{{ route('adm_surveys_destroy', array($survey['id'],'type=panel') ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div>
                          </h3></div>
                              <div class="col-md-12">
                                  <br>
                                  <div class="row">
                                      <div class="col-md-6">
                                        <b>Description</b>: <br>
                                        {{ $survey['description'] }}
                                      </div>
                                  </div>
                                  <div class="clearfix voffset2"></div>
                                  <h3>Survey Items:</h3>
                                  @foreach($survey['items'] as $index=>$item)
                                  <?php $multiple = false; ?>
                                      <div class="well clearfix form-group">
                                          <div class="col-md-1">{{ $index+1 }}.</div>
                                          <div class="col-md-11">
                                              <div class="fields">
                                              @if($item['allow_multiple'])
                                                <?php $multiple = true; ?>
                                              @endif
                                              <b>{{ $item['value'] }}</b>
                                              <br>
                                              </div>
                                          </div>
                                          <div class="col-md-11 col-md-offset-1">
                                              <div class="fields">
                                              @if(!$multiple)
                                                    {!! Form::hidden('choice[]' , 0 , [ 'class' => 'choice-input' ] ); !!}
                                              @endif

                                              @foreach($item['choices'] as $indexC=>$choice)
                                                @if($multiple)
                                                    {!! Form::checkbox('choice[]' , $choice['id'] ); !!}
                                                @else
                                                    {!! Form::radio('choice'.$index, $choice['id']  , null , ['class' => 'choice-radio']) !!}
                                                @endif
                                                {{ $choice['value'] }}<br>
                                              @endforeach

                                              </div>
                                          </div>
                                      </div>
                                  @endforeach
                                  {!! Form::submit('Submit' ,  ['class' => 'form-control']) !!}
                              </div>
                            <br>
                          </div>

                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/lbp.js') }}"></script>
@stop