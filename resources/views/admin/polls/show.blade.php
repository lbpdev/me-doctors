@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Polls
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Polls</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">All Polls</h3><br><br>
                        <a href="{{ route('adm_polls_create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add a new Poll</h3>
                        </a>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Last Updated</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($polls)
                            @foreach($polls as $poll)
                              <tr>
                                <td><a href="{{ route('adm_polls_single', $poll->id) }}">{{ $poll->id }}</a></td>
                                <td><a href="{{ route('adm_polls_single', $poll->id) }}">{{ $poll->title }}</a></td>
                                <td>{{ $poll->created_at }}</td>
                                <td>{{ $poll->updated_at }}</td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Last Updated</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop