@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Polls |
        <a href="{{ route('adm_polls') }}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Surveys</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
          <div class="col-md-12">

            <div class="box box-primary">
               <div class="box-header with-border">
               <div class="col-md-12">
                   <div class="pull-left">
                        <span class="pull-left"><a href="{{ route('adm_polls_edit', $poll->id) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                        <span class="pull-left">&nbsp;|&nbsp; </span>
                        <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_polls_destroy', array($poll->id) ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                  </div>
              </div>
                   <div class="col-md-6">
                       <h3>Preview:</h3>
                        {!! Form::open(['url' => route('adm_polls_vote' , $poll->id) ]) !!}
                        @include('admin.templates.poll')
                   </div>
                    <div class="col-md-4 survey-results">
                       <h3>Votes:</h3>
                      @foreach($poll->items as $indexC=>$item)
                          <!-- Progress bars -->
                            <div class="clearfix">
                              <span class="pull-left">{{ $item->value }}</span>

                              @if(count($item->votes)>0)
                                    <small class="pull-right">
                                        ( {{ $item->total_entries }} votes )
                                        @if($item->total_entries)
                                          {{ ($item->total_entries/$poll->total_entries)*100 }}%
                                        @else
                                          0%
                                        @endif
                                    </small>
                                @else
                                    <small class="pull-right">0%</small>
                                @endif
                            </div>

                              @if(count($item->votes)>0)
                                <div class="progress xs">
                                  <div class="progress-bar progress-bar-green" style="width: {{ ($item->total_entries/$poll->total_entries)*100 }}% "></div>
                                </div>
                              @else
                                <div class="progress xs">
                                  <div class="progress-bar progress-bar-green" style="width:0 "></div>
                                </div>
                              @endif
                        @endforeach
                      </div>
                </div>   <!-- /.row -->
            </div>
            </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/lbp.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
@stop