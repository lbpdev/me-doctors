@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Referrals
              </h1>
              {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
                {{--<li><a href="#">Ads</a></li>--}}
              {{--</ol>--}}
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>By</th>
                            <th>Receiver</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($referrals)
                            @foreach($referrals as $referral)
                              <tr>
                                <td>{{ $referral->id }}</td>
                                <td><a target="_blank" href="{{ route('admin.users.doctors.show', $referral->referredBy->username) }}">{{ isset($referral->referredBy) ? $referral->referredBy->fullName : 'Missing' }}</a></td>
                                <td>
                                @if($referral->receivedBy)
                                    <a target="_blank" href="{{ route('admin.users.doctors.show', $referral->referredBy->username) }}">
                                        {{ $referral->receivedBy ? $referral->receivedBy->email : 'Missing' }}
                                    </a>
                                @else
                                    {{ $referral->receiver }}
                                @endif
                                </td>
                                <td>{{ $referral->registerStatus }}</td>
                                <td>{{ $referral->created_at->format('d M Y') }}</td>
                                <td>
                                    <a onClick="if(confirm('Are you sure you want to delete this?'))
                                     return true;
                                     else return false;" href="{{ route('admin.referrals.delete', $referral->id) }}">Delete</a>
                                </td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>By</th>
                            <th>Receiver</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 0, 'desc' ]
        });
    </script>
@stop