@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Settings
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Polls</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Settings</h3><br><br>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        {!! Form::open(['route'=>'adm_settings_update']) !!}
                            @foreach($options as $index=>$option)
                                <b>{{ $option->name }} :</b>
                                {!! Form::text('option['.$index.'][id]' , $option->id , ['class'=>'hidden']) !!}
                                @if($option->slug == 'doctor-featured-article')
                                    {!! Form::select('option['.$index.'][value]', $doctorArticles , $option->value , [ 'class' => 'form-control' ] ) !!}
                                @elseif($option->slug == 'patient-active-poll' || $option->slug == 'doctor-active-poll')
                                    {!! Form::select('option['.$index.'][value]', $polls , $option->value , [ 'class' => 'form-control' ] ) !!}
                                @elseif($option->slug == 'patient-featured-article')
                                    {!! Form::select('option['.$index.'][value]', $patientArticles , $option->value , [ 'class' => 'form-control' ] ) !!}
                                @endif
                                <hr>
                            @endforeach
                            {!! Form::submit('Save' , ['class'=>'form-control']) !!}
                        {!! Form::close() !!}
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
@stop