@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surveys |
        <a href="{{ route('adm_surveys',2) }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Surveys</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open() !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Add a new Survey</h3></div>
                              <div class="col-md-12">
                                  <br>
                                  <div class="row">
                                      <div class="col-md-12">
                                        Survey Title : <br>
                                        {!! Form::input('text','title' , null, [ 'class' => 'form-control' , 'required' ]) !!}
                                        <br>
                                        Description (Optional) : <br>
                                        {!! Form::textarea('description' , null, [ 'class' => 'textarea form-control' ]) !!}
                                        <br>
                                        <b>Channel</b> : <br>
                                        {!! Form::select('channel_id', $channels, 1 , [ 'class' => 'form-control' ]) !!}
                                        <br><br>
                                        <b>Therapy</b> : <br>

                                        <div class="select-all-wrapper">
                                            <select name="therapies[]" multiple="multiple" class="margin-b-5 form-control">
                                                @foreach($therapies as $index=>$therapy)
                                                    <option value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="clearfix voffset2"></div>
                                  <h3>Survey Items:</h3>
                                  <div class="well clearfix form-group">
                                      <div class="col-md-6">
                                          <div class="fields question">
                                          Question: <br>
                                          {!! Form::input('text','survey[items][0][value]' , null, [ 'class' => 'value form-control', 'required' ]) !!}
                                              <div class="multiple-answers">
                                                  Multiple Answers:

                                                  {!! Form::hidden('survey[items][0][allow_multiple]' , 0 , [ 'class' => 'allow-holder' ]) !!}
                                                  {!! Form::checkbox('survey[items][0][multiples_mock]' , 0 , null ,[ 'class' => 'check-back' ]) !!}
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="fields answers">
                                          Choices: <br>
                                              {!! Form::input('text','survey[items][0][choices][0][value]' , null, [ 'class' => 'form-control' , 'required' ]) !!}
                                              {!! Form::input('text','survey[items][0][choices][1][value]' , null, [ 'class' => 'form-control' , 'required' ]) !!}
                                              <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                              <a href="#" rel="2" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                                              {!! Form::hidden('survey[items][0][questioncount]' , 2 , [ 'class' => 'questioncount' ]) !!}
                                          </div>
                                      </div>
                                  </div>
                                  <a href="#" rel="2" class="pull-left remove_field_group_bt glyphicon glyphicon-minus-sign"></a>
                                  <a href="#" class="pull-right add_field_group">Add a question <i class="glyphicon glyphicon-plus-sign"></i> </a>
                                  {{--<a href="#" class="pull-right add_text_field_group">Add text question <i class="glyphicon glyphicon-plus-sign"></i> </a>--}}
                                  <br><br>
                              </div>
                          </div>
                            <br>
                            <div class="col-md-12">
                                <hr>
                                Disclaimer (Optional) : <br>
                                {!! Form::textarea('disclaimer' , null, [ 'class' => 'textarea form-control' ]) !!}
                                <br>

                                {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                            </div>
                          </div>

                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>
      $('select').select2();

        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/custom/selectAll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/surveyFieldsV2.js') }}"></script>
@stop