@extends('admin.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surveys |
        <a href="{{ route('adm_surveys_single', $survey['id']) }}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Surveys</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open() !!}
            {!! Form::hidden('survey_id' , $survey->id , [ 'class' => 'allow-holder' ]) !!}

            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <div class="col-md-12">
                          <h3 class="inner-title"><i class="fa fa-check-square-o"></i>{{ $survey->title }}
                          </h3></div>
                              <div class="col-md-12">
                                  <br>
                                  <div class="row">
                                      <div class="col-md-12">
                                        <b>Title</b>: <br>
                                        {!! Form::input('text','title' , $survey->title, [ 'class' => 'form-control' , 'required' ]) !!}
                                       <br><b>Channel</b> : <br>
                                       {!! Form::select('channel_id', $channels, $survey->channel_id , [ 'class' => 'form-control' ]) !!}
                                       <br><br>
                                       Therapies: <br>

                                   <div class="select-all-wrapper">
                                       <select name="therapies[]" multiple="multiple" class="margin-b-5 form-control">
                                           @foreach($therapies as $therapy)

                                               <?php $selected = false; ?>
                                               @foreach($survey->therapies as $survey_therapy)
                                                    @if($survey_therapy['id'] == $therapy['id'])
                                                       <?php $selected = true; ?>
                                                    @endif
                                               @endforeach

                                               <option {{ $selected ? 'selected' : '' }} value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                    <br><br>
                                        <b>Description</b>: <br>
                                        {!! Form::textarea('description' , $survey->description, [ 'class' => 'form-control textarea' ]) !!}
                                      </div>
                                  </div>
                                  <div class="clearfix voffset2"></div>
                                  <h3>Survey Items:</h3>
                                  @foreach($survey['items'] as $index=>$item)
                                      <div class="well clearfix form-group">
                                      @if($index>1)
                                        <a href="#" rel="2" class="pull-left remove_field_group_bt glyphicon glyphicon-minus-sign"></a>
                                      @endif
                                          <div class="col-md-6">
                                              <div class="fields question">
                                              <b>Question</b>: <br>
                                              {!! Form::hidden('survey[items]['.$index.'][id]' , $item->id, ['class' => 'item_id']) !!}
                                              {!! Form::input('text','survey[items]['.$index.'][value]' , $item->value, [ 'class' => 'value form-control', 'required' ]) !!}
                                              <br><br>
                                              <b>Multiple Answers</b>:

                                              @if($item['allow_multiple']==1)
                                                {!! Form::hidden('survey[items]['.$index.'][allow_multiple]' , 1 , [ 'class' => 'allow-holder' ]) !!}
                                                {!! Form::checkbox('survey[items]['.$index.'][multiples_mock]' , 0 , null ,[ 'checked' ,'class' => 'check-back' ]) !!}
                                              @else
                                                {!! Form::hidden('survey[items]['.$index.'][allow_multiple]' , 0 , [ 'class' => 'allow-holder' ]) !!}
                                                {!! Form::checkbox('survey[items]['.$index.'][multiples_mock]' , 0 , null ,[ 'class' => 'check-back' ]) !!}
                                              @endif
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="fields answers">
                                              <b>Choices</b>: <br>
                                              @foreach($item->choices as $indexC=>$choice)
                                                {!! Form::hidden('survey[items]['.$index.'][choices]['.$indexC.'][id]' , $choice->id, ['class' => 'choice_id'] ) !!}
                                                {!! Form::input('text','survey[items]['.$index.'][choices]['.$indexC.'][value]' , $choice->value , [ 'class' => 'form-control' , 'required' ]) !!}
                                              @endforeach
                                              <?php $indexC++ ?>
                                              <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                              <a href="#" rel="2" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                                              {!! Form::hidden('survey[items]['.$index.'][questioncount]' , $indexC , [ 'class' => 'questioncount' ]) !!}
                                            </div>
                                          </div>

                                        </div>
                                  @endforeach
                                  <a href="#" class="pull-left add_field_group glyphicon glyphicon-plus-sign"></a>
                                  <br><br>
                              </div>
                            <br>
                            <div class="col-md-12">
                                <hr>
                                Disclaimer (Optional) : <br>
                                {!! Form::textarea('disclaimer' , $survey->disclaimer , [ 'class' => 'textarea form-control' ]) !!}
                                <br>
                                {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                            </div>
                          </div>

                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>
      $('select').select2();

        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/custom/surveyFields.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/selectAll.js') }}"></script>
@stop