<div class="well clearfix form-group">
  {{--<div class="col-md-1">{{ $index+1 }}.</div>--}}
  <div class="col-md-5">
      <div class="fields">
      <b>Question</b>: <br>
      {{ $item->value }}
      <br><br>
      <b>Multiple Answers</b>:
      @if($item->allow_multiple==1)
        Yes
      @else
        No
      @endif
      </div>
  </div>
  <div class="col-md-3">
      <div class="fields">
      <b>Choices</b>: <br>
      <ul>
      @foreach($item->choices as $indexC=>$choice)
        <li>{{ $choice->value }}</li>
      @endforeach
      </ul>
    </div>
  </div>
  <div class="col-md-4 survey-results">
  <b>Results: </b>
  @foreach($item->choices as $indexS=>$choice)
      <!-- Progress bars -->
        <div class="clearfix">
          <span class="pull-left">{{ $choice->value }}</span>

          @if(count($choice->entries)>0)
              <small class="pull-right">
                  ( {{ $choice->total_entries }} votes )
                  @if($item->total_entries)
                    {{ ($choice->total_entries/$item->total_entries)*100 }}%
                  @else
                    0%
                  @endif
              </small>
          @else
              <small class="pull-right">0%</small>
          @endif
        </div>

          @if(count($choice->entries)>0)
            <div class="progress xs">
              <div class="progress-bar progress-bar-green" style="width: {{ ($choice->total_entries/$item->total_entries)*100  }}% "></div>
            </div>
          @else
            <div class="progress xs">
              <div class="progress-bar progress-bar-green" style="width:0 "></div>
            </div>
          @endif
    @endforeach
  </div>
</div>