@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
               @if(Request::is('admin/surveys/channel/3'))
                    Patient
               @elseif(Request::is('admin/surveys/channel/2'))
                    Doctor
               @endif
               Surveys
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                        <a href="{{ route('adm_surveys_create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add Survey</h3>
                        </a>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Last Updated</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($surveys)
                            @foreach($surveys as $survey)
                              <tr>
                                <td><a href="{{ route('adm_surveys_single', $survey->id) }}">{{ $survey->id }}</a></td>
                                <td><a href="{{ route('adm_surveys_single', $survey->id) }}">{{ $survey->title }}</a></td>
                                <td>{{ $survey->created_at }}</td>
                                <td>{{ $survey->updated_at }}</td>
                              </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Last Updated</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $('#example1').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 2, 'desc' ]
        });
    </script>
@stop