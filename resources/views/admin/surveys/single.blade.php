@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Surveys |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Surveys</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open() !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <div class="col-md-12">
                          <h3 class="inner-title"><i class="fa fa-check-square-o"></i>{{ $survey->title }}
                          <div class="pull-right">
                                {{--<span class="pull-left"><a href="{{ route('adm_surveys_preview', $survey->id) }}"><i class="fa fa-eye"></i> Preview </a></span>--}}
                                {{--<span class="pull-left">&nbsp;|&nbsp; </span>--}}
                                <span class="pull-left"><a href="{{ route('adm_surveys_edit', $survey->id) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                                <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('adm_surveys_destroy',$survey->id ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div>
                          </h3></div>
                              <div class="col-md-12">
                                  <br>
                                  <div class="row">
                                      <div class="col-md-6">
                                        <b>Description</b>: <br>
                                        {!! $survey->description !!}
                                      <br>
                                      <br>
                                      @if(count($survey->therapies))
                                        <b>Therapy tags</b>: <br>
                                        @foreach($survey->therapies as $index=>$therapy)
                                            {{ $therapy->name }} {{ isset($survey->therapies[$index+1]) ? ', ' : '' }}
                                        @endforeach
                                      @endif
                                      </div>
                                  </div>
                                  <div class="clearfix voffset2"></div>
                                  <h3>Survey Items:</h3>
                                  @foreach($survey->items as $index=>$item)
                                    @if(count($item->choices)>0)
                                        @include('admin.surveys.parts.select')
                                    @else
                                        @include('admin.surveys.parts.text')
                                    @endif
                                  @endforeach
                                  <br>
                                  @if($survey->disclaimer)
                                      <b>Disclaimer: </b> <br>
                                      <p>{{ $survey->disclaimer }}</p>
                                  @endif
                              </div>
                            <br>
                          </div>

                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>

    <script type="text/javascript" src="{{ asset('public/js/lbp.js') }}"></script>
    <script src="{{ asset('public/dist/js/pages/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
@stop