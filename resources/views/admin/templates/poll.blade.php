@if ($poll)
    <div class="panel panel-primary transparent-bg no-margin-bottom">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $poll->title }}</h3>
        </div>
        <div class="panel-body padding-5 transparent-bg">

            <ul class="list-group transparent-bg">
                @if ($voted = $poll->items->contains('voted', 1))
                    <li class="list-group-item transparent-bg">
                        <label><p>Thank you for voting. You voted for "{{ $poll->items->where('voted', 1)->first()->body }}"</p></label>
                    </li>
                @else
                    {!! Form::hidden('poll_id', $poll->id ); !!}
                    @foreach ($poll->items as $item)
                        <li class="list-group-item transparent-bg no-border">
                            <div class="checkbox">
                                <label>
                                @if(count($item->votes)>0)
                                    {!! Form::radio('poll_item[]', $item->id , true ) !!} {{ $item->body }}
                                @else
                                    {!! Form::radio('poll_item[]', $item->id , false ) !!} {{ $item->body }}
                                @endif
                                </label>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>

        @if ( ! $voted)
            <div class="panel-footer text-center transparent-bg">
                {!! Form::submit('Vote', ['class'=>'btn btn-primary btn-block btn-sm'] ) !!}
                {{--<a href="#" class="small">View Result</a>--}}
            </div>
        @endif
     </div>
@endif