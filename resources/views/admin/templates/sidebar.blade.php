<!-- Left side column. contains the logo and sidebar -->

      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            @if(Session::get('avatar'))
                <a href="{{ URL::to('profile/'.Session::get('username')) }}"><img class="img-circle" src="{{ URL::to('uploads/'.Session::get('avatar')) }}" width="60"></a>
            @else
                <img src="{{ asset('public/images/placeholders/user.png') }}" class="img-circle" width="60">
            @endif
            </div>
            <div class="pull-left info">
              <p>Admin</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
              {{--<input type="text" name="q" class="form-control" placeholder="Search..."/>--}}
              {{--<span class="input-group-btn">--}}
                {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
              {{--</span>--}}
            {{--</div>--}}
          {{--</form>--}}
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-home"></i> <span>Go to website</span></i>
              </a>
              <ul class="treeview-menu">
              <li>
                <a href="{{ URL::to('/patient') }}"><i class="fa fa-circle-o"></i> Patient Section</a>
              </li>
              <li>
                <a href="{{ URL::to('/doctor') }}"> <i class="fa fa-circle-o"></i> Doctor Section</a>
              </li>
            </ul>

            </li>
            <li class="{{ set_active(['admin/panel', 'admin/diagnosis', 'admin/comments'])  }} treeview">
              <a href="#">
                <i class="fa fa-comments-o"></i>
                    <span>Discussions </span>
                    @if($notifications['diagnosis'] || $notifications['discussions'] || $notifications['comments'] )
                        <span class="label label-warning">{{ $notifications['diagnosis'] + $notifications['discussions'] + $notifications['comments']  }}</span>
                    @endif
                    <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ set_active('admin/panel') }}"><a href="{{ route('adm_panel') }}">
                    <i class="fa fa-circle-o"></i>
                    Panel Discussions
                    @if($notifications['discussions'])
                        <span class="label label-warning">{{ $notifications['discussions']  }}</span>
                    @endif
                    </a>
                </li>
                <li class="{{ set_active('admin/patient_discussions') }}"><a href="{{ route('patient_discussions') }}">
                <i class="fa fa-circle-o"></i>
                    Patient Discussions
                    @if($notifications['diagnosis'])
                        <span class="label label-warning">{{ $notifications['diagnosis']  }}</span>
                    @endif
                </a>
                </li>
                <li class="{{ set_active('admin/diagnosis') }}"><a href="{{ route('adm_diag') }}">
                <i class="fa fa-circle-o"></i>
                    Diagnosis
                    @if($notifications['diagnosis'])
                        <span class="label label-warning">{{ $notifications['diagnosis']  }}</span>
                    @endif
                </a>
                </li>
                <li class="{{ set_active('admin/comments') }}">
                <a href="{{ route('adm_comments') }}">
                <i class="fa fa-circle-o"></i>
                    Comments
                    @if($notifications['comments'])
                        <span class="label label-warning">{{ $notifications['comments']  }}</span>
                    @endif
                </a>
                </li>
              </ul>
            </li>
            <li class="{{ set_active('admin/surveys*') }} treeview">
              <a href="#">
                <i class="fa fa-comment"></i> <span>Surveys</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ set_active('admin/surveys/channel/3') }}">
                    <a href="{{ route('adm_surveys',3) }}"><i class="fa fa-circle-o"></i>Patient Surveys</a>
                </li>
                <li class="{{ set_active('admin/surveys/channel/2') }}">
                    <a href="{{ route('adm_surveys',2) }}"><i class="fa fa-circle-o"></i>Doctor Surveys</a>
                </li>
                <li class="{{ set_active('admin/surveys/create') }}">
                    <a href="{{ route('adm_surveys_create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                </li>
              </ul>
            </li>
            <li  class="{{ set_active('admin/polls*') }} treeview">
              <a href="#">
                <i class="fa fa-bar-chart"></i> <span>Polls</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ set_active(route('adm_polls', [], false)) }}">
                    <a href="{{ route('adm_polls') }}"><i class="fa fa-circle-o"></i>View All</a>
                </li>
                <li class="{{ set_active(route('adm_polls_create', [], false)) }}">
                    <a href="{{ route('adm_polls_create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                </li>
              </ul>
            </li>
            <li class="{{ strpos(Request::path(), 'hospitals') ? 'active' : '' }} treeview">
              <a href="#">
                <i class="fa fa-hospital-o"></i>
                <span>
                    Hospitals
                    @if($notifications['hospitals'])
                        <span class="label label-warning">{{ $notifications['hospitals']  }}</span>
                    @endif
                </span>

                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ strpos(Request::path(), 'hospitals/all') ? 'active' : '' }}">
                    <a href="{{ route('admin.hospitals.index') }}"><i class="fa fa-circle-o"></i>View All</a>
                </li>
                <li>
                    <a href="{{ route('admin.hospitals.unverified') }}">
                        <i class="fa fa-circle-o"></i>Unverified Hospitals
                        @if($notifications['hospitals'])
                            <span class="label label-warning">{{ $notifications['hospitals']  }}</span>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.hospitals.claims') }}">
                        <i class="fa fa-circle-o"></i>Claims
                        @if($notifications['hospitals'])
                            <span class="label label-warning">{{ $notifications['hospitals']  }}</span>
                        @endif
                    </a>
                </li>
                <li class="{{ strpos(Request::path(), 'create') ? 'active' : '' }}">
                    <a href="{{ route('admin.hospitals.create') }}"><i class="fa fa-circle-o"></i> Add New</a>
                </li>
              </ul>
            </li>
            <li class="{{ strpos(Request::path(), 'events') ? 'active' : '' }} treeview">
              <a href="#">
                <i class="fa fa-ticket"></i> <span>Events</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ set_active(route('adm_events', [], false)) }}">
                    <a href="{{ route('adm_events') }}"><i class="fa fa-circle-o"></i>View All</a>
                </li>
                <li class="{{ set_active(route('adm_event_create', [], false)) }}"><a href="{{ route('adm_event_create') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
              </ul>
            </li>
            <li class="{{ set_active('admin/users*') }} treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>
                    Users
                    @if(count($notifications['doctors'])>0)
                        <span class="label label-warning">{{ $notifications['doctors']  }}</span>
                    @endif
                </span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.users.doctors') }}"><i class="fa fa-circle-o"></i> Doctors</a></li>
                <li><a href="{{ route('admin.users.doctors.create') }}"><i class="fa fa-circle-o"></i> Add a doctor</a></li>
                <li><a href="{{ route('admin.users.patients') }}"><i class="fa fa-circle-o"></i> Patients</a></li>
                <li><a href="{{ route('admin.users.unverified') }}">
                    <i class="fa fa-circle-o"></i>
                    Unverified Doctors
                    @if(count($notifications['doctors'])>0)
                        <span class="label label-warning">{{ $notifications['doctors'] }}</span>
                    @endif</a>
                </li>
                {{-- <li><a href="{{ route('admin.users.create') }}"><i class="fa fa-circle-o"></i>Add New</a></li> --}}
              </ul>
            </li>
            <li class="{{ set_active('admin/emails*') }} treeview">
              <a href="#">
                <i class="fa fa-envelope"></i> <span>Emails</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('emails.domains.whitelist') }}"><i class="fa fa-circle-o"></i>Domains Whitelist</a></li>
                <li><a href="{{ route('emails.whitelist') }}"><i class="fa fa-circle-o"></i>Emails Whitelist</a></li>
                <li><a href="{{ route('emails.domains.blacklist') }}"><i class="fa fa-circle-o"></i>Domains Blacklist</a></li>
              </ul>
            </li>

            <!-- ARTICLES -->

            <li class="{{ set_active('admin/articles*') }} treeview">
                    <a href="#">
                    <i class="fa fa-thumb-tack"></i> <span>Articles</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li class="{{ set_active(route('adm_articles', 3, false)) }}">
                        <a href="{{ route('adm_articles', 3) }}"><i class="fa fa-circle-o"></i>Patient Articles</a>
                    </li>
                    <li class="{{ set_active(route('adm_articles', 2)) }}">
                        <a href="{{ route('adm_articles',2) }}"><i class="fa fa-circle-o"></i>Doctor Articles</a>
                    </li>
                    <li class="{{ set_active(route('adm_articles_create', [], false)) }}">
                        <a href="{{ route('adm_articles_create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                    </li>
                    <li>
                  </ul>
            </li>

            <!-- NEWS FEEDS -->


            <li class="{{ set_active('admin/articles*') }} treeview">
                    <a href="#">
                    <i class="fa fa-newspaper-o"></i> <span>News Feeds</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li class="{{ set_active(route('admin.news.index', false)) }}">
                        <a href="{{ route('admin.news.index') }}"><i class="fa fa-circle-o"></i>View All</a>
                    </li>
                    <li>
                      <a href="{{ route('admin.news.channel', 3) }}"><i class="fa fa-circle-o"></i> Patient News</a>
                    </li>
                    <li>
                      <a href="{{ route('admin.news.channel', 2) }}"> <i class="fa fa-circle-o"></i> Doctor News</a>
                    </li>
                    <li class="{{ set_active(route('admin.news.create', [], false)) }}">
                        <a href="{{ route('admin.news.create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                    </li>
                    <li>
                  </ul>
            </li>

            <!-- VIDEOS -->


            <li class="{{ set_active('admin/videos*') }} treeview">
                    <a href="#">
                    <i class="fa fa-file-video-o"></i> <span>Videos</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li class="{{ set_active(route('admin.videos.index')) }}">
                        <a href="{{ route('admin.videos.index') }}"><i class="fa fa-circle-o"></i>View All</a>
                    </li>
                    <li class="{{ set_active(route('admin.videos.create')) }}">
                        <a href="{{ route('admin.videos.create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                    </li>
                    <li>
                  </ul>
            </li>

            <!-- ADS -->


            <li class="{{ set_active('admin/ads*') }} treeview">
                    <a href="#">
                    <i class="fa fa-image"></i> <span>Ads</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu">
                    <li class="{{ set_active(route('admin.ads.index')) }}">
                        <a href="{{ route('admin.ads.index') }}"><i class="fa fa-circle-o"></i>View All</a>
                    </li>
                    <li class="{{ set_active(route('admin.ads.pending')) }}">
                        <a href="{{ route('admin.ads.pending') }}"><i class="fa fa-circle-o"></i>Pending Requests</a>
                    </li>
                    <li class="{{ set_active(route('admin.ads.create')) }}">
                        <a href="{{ route('admin.ads.create') }}"><i class="fa fa-circle-o"></i>Add New</a>
                    </li>
                    <li>
                  </ul>
            </li>


            <!-- REFERRALS -->

            <li class="{{ set_active(route('admin.referrals.index')) }} treeview">
                    <a href="{{ route('admin.referrals.index') }}">
                    <i class="fa fa-thumbs-up"></i> <span>Referrals</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
            </li>

            <!-- FILES -->


            <li class="{{ set_active('admin/files*') }} treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>File Manager</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ route('admin.files.index') }}"><i class="fa fa-circle-o"></i>View Files</a></li>
                <li><a href="{{ route('admin.files.create') }}"><i class="fa fa-circle-o"></i>Upload a File</a></li>
              </ul>
            </li>
            <li class="{{ Request::is('admin/settings') ? 'active' : '' }} treeview">
              <a href="{{ route('adm_settings') }}">
                <i class="fa fa-wrench"></i> <span>Settings</span></i>
              </a>
            </li>
            <li class="{{ Request::is('admin/user-logs') ? 'active' : '' }} treeview">
              <a href="{{ route('adm_user_logs') }}">
                <i class="fa fa-wrench"></i> <span>User Logs</span></i>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ url('doctor/logout') }}">
                <i class="fa fa-sign-out"></i> <span>Logout</span></i>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>