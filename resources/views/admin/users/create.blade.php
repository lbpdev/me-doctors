@extends('admin.master')

@section('style')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users |
        {{--<a href="{{ route('admin.users.show') }}"><i class="fa fa-remove"></i> Cancel</a>--}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
                <div class="col-md-12">
                    <h3 class="hs_heading no-margin-top">Create</h3>
                    @if(Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message')  }}</div>
                    @endif
                </div>
                {!! Form::open([ 'route' => 'admin.users.store', 'data-toggle' => 'validator' , 'role' => 'form' , 'class' => 'clearfix']) !!}

                    <div class="form-group col-md-12">
                        <b>Role:</b>
                        {!! Form::select('role', $roles , 1 , ['class' => 'form-control']) !!}
                    </div>

                    @include('doctor.registration.form')
                    <div class="form-group col-md-12">
                        {!! Form::submit('Register', [ 'class' => 'submit button pull-left form-control']) !!}
                    </div>
                {!! Form::close() !!}
            </div>   <!-- /.row -->
          </div>   <!-- /.row -->
        </div>   <!-- /.row -->
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
    @include('doctor.auth.scripts')
@stop