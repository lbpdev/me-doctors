@extends('admin.master')

@section('styles')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
@stop

@section('content')
    <section class="content-header">
          <h1>Doctors</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li class="active">Doctors</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">

                @if(Session::has('registration.success'))
                    <div class="alert alert-success">
                       {!! Session::get('registration.success') !!}
                    </div>
                @endif

                    {!! Form::open([
                        'files'       => true,
                        'route'       => 'admin.users.doctors.store',
                        'data-toggle' => 'validator',
                        'role'        => 'form',
                        'class'       => 'clearfix'
                    ]) !!}

                    <div class="{{ session()->has('registration.error') || $errors->any() ? '' : 'register-form' }}" id="register-form">
                        @include('admin.users.doctors.registration.form')
                    </div>

                    {!! Form::close() !!}

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
    (function() {
        $('#register-therapies').select2({
            placeholder: "Select doctor's specialties"
        });
    })();
</script>
@stop