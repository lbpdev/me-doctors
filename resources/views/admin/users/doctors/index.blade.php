@extends('admin.master')

@section('content')
    <section class="content-header">
          <h1>Doctors</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li class="active">Doctors</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="usersTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Specialties</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($doctors as $doctor)
                                    <tr>
                                        <td><a href="{{ route('admin.users.doctors.show', $doctor->username) }}">{{ $doctor->id }}</a></td>
                                        <td>{{ $doctor->fname }}</td>
                                        <td>{{ $doctor->lname }}</td>
                                        <td>{{ $doctor->email }}</td>
                                        <td>{{ $doctor->primaryWorkLocation }}</td>
                                        <td>{{ $doctor->allSpecialties }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $('#usersTable').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
          "order": [ 0, 'desc' ]
        });
    </script>
@stop