<h4 class>Awards</h4>
<div class="section col-md-12 margin-b-7">
    <div class="col-md-12">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="awards" class="dataList">
        @if(isset($user->achievements) && count($user->achievements) > 0 )
            @foreach ($achievements = $user->achievements as $achievement)
                <span>{{ $achievement->name }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>

        <button class="add-toggle"><i class="fa fa-plus"></i> Update Awards</button>
    </div>
    <div class="add-form clearfix  well-sm col-md-12">
        <h5><i class="fa fa-plus-square-o"></i> Update Awards</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.achievements.update', $user->username]]) !!}
    @else
        {!! Form::open(['method' => 'PUT', 'route' => ['doctor.achievements.updateIE', $user->username]]) !!}
    @endif
        {!! Form::hidden('user_id', $user->id) !!}
            <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">
                @if(isset($achievements))
                    @foreach ($achievements as $index => $achievement)
                        <input type="text" name="achievements[{{$index}}][name]" placeholder="Award" class="margin-b-5 form-control" value="{{ $achievement->name }}">
                    @endforeach
                @endif
                <input type="text" name="achievements[{{ isset($achievements) ? $achievements->count() : 0 }}][name]" placeholder="Award" class="margin-b-5 form-control">
            </div>
            <button class=" pull-right add_field_bt margin-l-10  fa fa-plus"></button>
            <button rel="1" class=" pull-right remove_field_bt fa fa-minus"></button>

            <button class=" add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button type="submit" class=" add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
        </form>
    </div>
</div>