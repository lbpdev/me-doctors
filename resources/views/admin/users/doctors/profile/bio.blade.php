@if(isset($user->profile->bio))
    <div class="col-md-12 margin-t-10">
        <div class="row">
            <h1 class="t-blue margin-t-0">Professional Statement</h1>
            @if(isset($user->profile->bio))
                <p>{!! $user->profile->bio !!}</p>
            @else
                N/A
            @endif
        </div>
    </div>
@endif