<h4 class="clearfix margin-t-0">Education</h4>
<div class="section col-md-12 margin-b-7">
    <div class="col-md-12">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="educations" class="dataList">
        @if(isset($user->educations) && count($user->educations) > 0 )
            @foreach ($educations = $user->educations as $education)
                <span>{{ $education->school }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>
        <button class=" add-toggle"><i class="fa fa-plus"></i> Update Education</button>
    </div>
    <div class="add-form clearfix  well-sm col-md-12">
        {{--<img src="{{ asset('public/images/loader.gif') }}" width="60" class="loader" style="position: absolute;">--}}
        <h5><i class="fa fa-plus-square-o"></i> Update Education</h5>

    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.education.update', $user->username]]) !!}
    @else
        {!! Form::open(['method' => 'PUT', 'route' => ['doctor.education.updateIE', $user->username]]) !!}
    @endif
        {!! Form::hidden('user_id', $user->id) !!}
        <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">
                @if(isset($user->educations))
                    @foreach ($user->educations as $index => $education)
                        <input type="text" name="education[{{$index}}][school]" placeholder="Education" class="margin-b-5 form-control" value="{{ $education->school }}">
                    @endforeach
                @endif
                <input type="text" name="education[{{ isset($user->educations) ? $user->educations->count() : 0 }}][school]" placeholder="Education" class="margin-b-5 form-control">
            </div>
            <button class=" pull-right add_field_bt margin-l-10  fa fa-plus"></button>
            <button rel="1" class=" pull-right remove_field_bt fa fa-minus"></button>

            <button class=" add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button type="submit" class=" add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
    </div>
</div>