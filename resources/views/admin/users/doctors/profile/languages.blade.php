<h4 class="clearfix">Languages Spoken</h4>
<div class="section col-md-12 margin-b-7">
    <div class="col-md-12">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="languages" class="dataList">
        @if(isset($user->languages) && count($user->languages) > 0)
            @foreach ($user->languages as $language)
                <span>{{ $language['name'] }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>
        <button class=" add-toggle"><i class="fa fa-plus"></i> Update Languages</button>
    </div>
    <div class="add-form clearfix  well-sm col-md-12">
        <h5><i class="fa fa-plus-square-o"></i> Update Languages</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.languages.update', $user->username]]) !!}
    @else
        {!! Form::open(['method' => 'PUT', 'route' => ['doctor.languages.updateIE', $user->username]]) !!}
    @endif
        {!! Form::hidden('user_id', $user->id) !!}
            <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">
                <select name="languages[][language_id]" multiple="multiple" class="margin-b-5">
                    @foreach($languages as $language)
                        <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                    @endforeach


                    @foreach($user->languages as $user_language)
                        @foreach($languages as $language)
                            @if($user_language['id'] == $language['id'])
                                <option selected value="{{ $user_language['id'] }}">{{ $user_language['name'] }}</option>
                            @endif
                        @endforeach
                    @endforeach

                </select>
            </div>

            <button class=" add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button type="submit" class=" add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
    </div>
</div>