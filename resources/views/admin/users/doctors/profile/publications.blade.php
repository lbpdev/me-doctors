<h4 class="clearfix">Publications</h4>
<div class="section col-md-12 margin-b-7">
    <div class="col-md-12">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="publications" class="dataList">
        @if(isset($user->publications) && count($user->publications) > 0)
            @foreach ($publications = $user->publications as $publication)
                <span>{{ $publication->name }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>
        <button class=" add-toggle"><i class="fa fa-plus"></i> Update Publication</button>
    </div>
    <div class="add-form clearfix  well-sm col-md-12">
        <h5><i class="fa fa-plus-square-o"></i> Update Publication</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm',  'route' => ['doctor.publications.update', $user->username]]) !!}
    @else
        {!! Form::open(['method' => 'PUT', 'route' => ['doctor.publications.updateIE', $user->username]]) !!}
    @endif
        {!! Form::hidden('user_id', $user->id) !!}
            <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">

                @if(isset($publications))
                    @foreach ($publications as $index => $publication)
                        <input type="text" name="publications[{{$index}}][name]" placeholder="Publication" class="margin-b-5 form-control" value="{{ $publication->name }}">
                    @endforeach
                @endif

                <input type="text" name="publications[{{ isset($publications) ? $publications->count() : 0 }}][name]" placeholder="Publication" class="margin-b-5 form-control">
            </div>
            <button class=" pull-right add_field_bt margin-l-10  fa fa-plus"></button>
            <button rel="1" class=" pull-right remove_field_bt fa fa-minus"></button>

            <button class=" add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button type="submit" class=" add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
    </div>
</div>