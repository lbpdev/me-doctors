<h1 class="t-blue margin-t-20 margin-b-7 clearfix pull-left width-full">Location</h1>
<div class="section col-md-12 margin-b-7">
    <div class="row">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="works" class="dataList">
        @if(isset($user->hospitals) && count($user->hospitals) > 0)
            @foreach ($user->hospitals as $hospital)
            <?php $days = 0; ?>
                <span><b>{!! $hospital->name ? $hospital->name : '' !!}</b></span><br>
                <span>{!! $hospital->location ? $hospital->location->name : '' !!}</span><br>
                <span><b>Schedule :</b>
                    @for($x=0;$x<7;$x++)
                        @if( in_array( $x, $hospital->schedules ) )
                        <?php $days++; ?>
                            @if($x==0)     <i>Sun</i>
                            @elseif($x==1) <i>Mon</i>
                            @elseif($x==2) <i>Tue</i>
                            @elseif($x==3) <i>Wed</i>
                            @elseif($x==4) <i>Thu</i>
                            @elseif($x==5) <i>Fri</i>
                            @elseif($x==6) <i>Sat</i>
                            @endif
                         @if($days<count($hospital->schedules)) {{' , '}} @endif
                        @endif
                    @endfor
                </span>
                <hr>
            @endforeach
        @else
            N/A
        @endif
        </div>
    </div>

    <button class=" add-toggle"><i class="fa fa-plus"></i> Update Work</button>

    <div class="add-form clearfix  well-sm col-md-12  row">
        <h5><i class="fa fa-plus-square-o"></i> Update Work</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.work.update', $user->username]]) !!}
     @else
         {!! Form::open(['method' => 'PUT', 'route' => ['doctor.work.updateIE', $user->username]]) !!}
     @endif
        {!! Form::hidden('user_id', $user->id) !!}
     <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>

            <div class="fields-group">
                @foreach ($user->hospitals as $index=>$hospital)
                <div class="fields padding-b-5 border-bottom clearfix">
                    <span class="fa fa-close pull-right margin-b-10"></span>
                    <div class="col-md-6 padding-l-0">
                        <input type="text" value="{{ $hospital->name }}" name="work[{{ $index }}][name]" required placeholder="Company Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                        {!! Form::input('text','work['.$index.'][location]' , $hospital->location ? $hospital->location->name : '', [ 'class' => 'form-control location' , 'id' => 'location' , 'placeholder' => 'Address']) !!}
                        <br>
                        <table id="address" width="100%">
                            <tr><td>Street</td><td><input value="{{ $hospital->location ? $hospital->location->street_number : '' }}" name="work[{{ $index }}][street_number]" class=" form-control field street_number margin-tb-5" id="street_number"></td></tr>
                            <tr><td>Route</td><td><input value="{{ $hospital->location ? $hospital->location->street_route : '' }}" name="work[{{ $index }}][street_route]" class="form-control field route margin-tb-5" id="route"></td></tr>
                            <tr><td>City</td><td><input value="{{ $hospital->location ? $hospital->location->city : '' }}" name="work[{{ $index }}][locality]" class="form-control field locality margin-tb-5" id="locality"></td></tr>
                            <tr><td>State</td><td><input value="{{ $hospital->location ? $hospital->location->state : '' }}" name="work[{{ $index }}][state]" class="form-control field administrative_area_level_1 margin-tb-5" id="administrative_area_level_1"></td></tr>
                            <tr><td>Postal Code</td><td><input value="{{ $hospital->location ? $hospital->location->post_code : '' }}" name="work[{{ $index }}][post_code]" class="form-control field postal_code margin-tb-5" id="postal_code"></td></tr>
                            <tr><td>Country</td><td><input value="{{ $hospital->location ? $hospital->location->country : '' }}" name="work[{{ $index }}][country]" class="form-control field country margin-tb-5" id="country"></td></tr>
                        </table>
                    </div>

                    <div class="col-md-6 no-padding">
                        <div class="map">
                            <p>Location not found on Map</p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        Work days :
                        @for($x=0;$x<7;$x++)
                            <span class="day-check">
                                @if( in_array( $x, $hospital['schedules'] ) )
                                    <input class="square" checked name="work[{{$index}}][days][{{$x}}][day]" value="{{$x}}" type="checkbox" id="{{$index}}{{$x}}" />
                                @else
                                    <input class="square" name="work[{{$index}}][days][{{$x}}][day]" value="{{$x}}" type="checkbox" id="{{$index}}{{$x}}" />
                                @endif
                                @if($x==0) <label for="{{$index}}{{$x}}"><span></span>Sun</label>
                                @elseif($x==1) <label for="{{$index}}{{$x}}"><span></span>Mon</label>
                                @elseif($x==2) <label for="{{$index}}{{$x}}"><span></span>Tue</label>
                                @elseif($x==3) <label for="{{$index}}{{$x}}"><span></span>Wed</label>
                                @elseif($x==4) <label for="{{$index}}{{$x}}"><span></span>Thu</label>
                                @elseif($x==5) <label for="{{$index}}{{$x}}"><span></span>Fri</label>
                                @elseif($x==6) <label for="{{$index}}{{$x}}"><span></span>Sat</label>
                                @endif
                            </span>
                        @endfor
                    </div>
                </div>
                @endforeach

                <div class="fields padding-b-5 border-bottom clearfix">
                    <span class="fa fa-close pull-right margin-b-10"></span>
                    <div class="col-md-6 padding-l-0">
                    <input type="text" name="work[{{$user->hospitals->count()}}][name]" placeholder="Company Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                    {!! Form::input('text','work['.$user->hospitals->count().'][location]' , null, [  'class' => 'form-control location' , 'id' => 'location' , 'placeholder' => 'Address']) !!}
                    <br>
                        <table id="address" width="100%">
                            <tr><td>Street</td><td><input name="work[{{$user->hospitals->count()}}][street_number]" class="field street_number form-control margin-tb-5" id="street_number"></td></tr>
                            <tr><td>Route</td><td><input name="work[{{$user->hospitals->count()}}][street_route]" class="field route form-control margin-tb-5" id="route"></td></tr>
                            <tr><td>City</td><td><input name="work[{{$user->hospitals->count()}}][locality]" class="field locality form-control margin-tb-5" id="locality"></td></tr>
                            <tr><td>State</td><td><input name="work[{{$user->hospitals->count()}}][state]" class="field administrative_area_level_1 form-control margin-tb-5" id="administrative_area_level_1"></td></tr>
                            <tr><td>Post Code</td><td><input name="work[{{$user->hospitals->count()}}][post_code]" class="field postal_code form-control margin-tb-5" id="postal_code"></td></tr>
                            <tr><td>Country</td><td><input name="work[{{$user->hospitals->count()}}][country]" class="field country form-control margin-tb-5" id="country"></td></tr>
                        </table>
                    </div>

                    <div class="col-md-6 no-padding">
                        <div class="map">
                            <p>Location not found on Map</p>
                        </div>
                    </div>

                    <div class="col-md-12 margin-t-10">
                        <div class="row">
                            <span class="day-check">
                                <input class="square" name="work[{{$user->hospitals->count()}}][days][0][day]" value="0" type="checkbox" id="day1a" />
                                <label for="day1a"><span></span>Sun</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][1][day]" value="1" type="checkbox" id="day2a" />
                                <label for="day2a"><span></span>Mon</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][2][day]" value="2" type="checkbox" id="day3a" />
                                <label for="day3a"><span></span>Tue</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][3][day]" value="3" type="checkbox" id="day4a" />
                                <label for="day4a"><span></span>Wed</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][4][day]" value="4" type="checkbox" id="day5a" />
                                <label for="day5a"><span></span>Thu</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][5][day]" value="5" type="checkbox" id="day6a" />
                                <label for="day6a"><span></span>Fri</label>
                            </span>
                            <span class="day-check">
                                <input class="square"  name="work[{{$user->hospitals->count()}}][days][6][day]" value="6" type="checkbox" id="day7a" />
                                <label for="day7a"><span></span>Sat</label>
                            </span>
                        </div>
                    </div>

                </div>
            </div>


            <button class=" add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button class=" add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
            <button class="pull-right add_field_group_bt margin-l-10  fa fa-plus"></button>
            <button rel="1" class=" pull-right remove_field_group_bt fa fa-minus"></button>
    </div>
</div>
</div>
</div>
