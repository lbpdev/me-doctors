<h1 class="t-blue margin-t-20 margin-b-7 clearfix pull-left width-full">Location</h1>
<div class="section col-md-12 margin-b-7">
    <div class="row">
        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
        <div id="works" class="dataList">
        @if(isset($user->hospitals) && count($user->hospitals) > 0)
            @foreach ($user->hospitals as $index=>$hospital)
            <div class="fields padding-b-5 clearfix">
                    <div class="col-md-8 padding-l-0">
                        <span>{!! '<b>'.$hospital->name . '</b><br>' !!}</span>
                        <span><b>{!! $hospital->location ? $hospital->location->name : '' !!}</b></span><br>
                        <span><b>Schedule :</b>
                            @for($x=0;$x<7;$x++)
                                @if( in_array( $x, $hospital->schedules ) )
                                <?php $days++; ?>
                                    @if($x==0)     <i>Sun</i>
                                    @elseif($x==1) <i>Mon</i>
                                    @elseif($x==2) <i>Tue</i>
                                    @elseif($x==3) <i>Wed</i>
                                    @elseif($x==4) <i>Thu</i>
                                    @elseif($x==5) <i>Fri</i>
                                    @elseif($x==6) <i>Sat</i>
                                    @endif
                                 @if($days<count($hospital->schedules)) {{' , '}} @endif
                                @endif
                            @endfor
                        </span>
                    </div>

                    <div class="col-md-4 no-padding">
                        <div class="map" style="height: 200px">
                        </div>
                    </div>
            </div>
            <hr>
            @endforeach
        @else
            N/A
        @endif


        </div>
    </div>
    </div>
</div>
</div>
