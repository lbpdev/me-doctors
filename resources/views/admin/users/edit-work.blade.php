@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile |
        <a href="{{ route('adm_show_user' , $user->username) }}"><i class="fa fa-remove"></i> Cancel Changes</a> |
        <a href="{{ route('adm_edit_user' , $user->username) }}"><i class="fa fa-trash"></i> Edit Personal Details</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        {!! Form::open() !!}
        {!! Form::hidden('user_id', $user->id ) !!}
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-md-12">
                    <h2 class="hs_heading"><i class="fa fa-user"></i> Work Profile</h2>
                  </div>
                  <div class="row col-md-12">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <h4><i class="fa fa-phone"></i> Phone No: </h4>
                          Personal : <br>{!! Form::input('text', 'personal_number' , $user->personal_number  , [ 'class' => 'form-control' ]) !!}
                              <br>
                          Office : <br>{!! Form::input('text', 'office_number' , $user->office_number  , [ 'class' => 'form-control' ]) !!}
                              <br><br>
                          <i class="fa fa-medkit"></i> Therapy Area: <br><br>
                              Specialty : <br>{!! Form::select('therapy' , $therapies , 1 , [ 'class' => 'form-control' ]) !!}
                                  <br>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12">
                       <h4><i class="fa fa-stethoscope"></i> Where I work: </h4>
                              <div class="form-group">
                                  Country : <br>{!! Form::select('country_code' , $countries , $user->hospital_country_code,  [ 'class' => 'form-control' ]) !!}
                              </div>
                              <div class="form-group">
                                  City : <br>{!! Form::select('city_id' , $cities , $user->hospital_city , [ 'class' => 'form-control' ]) !!}
                              </div>
                              <div class="form-group">
                                  Address : <br>{!! Form::textarea('address' , $user->hospital_address, [ 'class' => 'form-control' ]) !!}
                              </div>
                              <div class="form-group">
                                  Company : <br>{!! Form::input('text','name' , $user->hospital_name, [ 'class' => 'form-control' ]) !!}
                              </div>
                      </div>
                  <div class="voffset8 clearfix hs_margin_30">&nbsp;</div>
                </div>
                </div>
              </div>
            </div>
          </div>

              <div class="col-md-6 col-sm-12">

                  <div class="box box-primary">
                    <div class="box-header with-border">
                    <div class="qualifications">
                      <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Qualifications</h3></div>
                          <div class="col-md-12">
                              <br><div class="form-group">
                              Education : <br>
                              <div class="fields">
                              @if($user->qualifications)
                                  @if($user->qualifications->education)
                                      @foreach($user->qualifications->education as $education)
                                          {!! Form::input('text','education[]' , $education, [ 'class' => 'form-control' ]) !!}
                                      @endforeach
                                  @else
                                      {!! Form::input('text','education[]' , null, [ 'class' => 'form-control' ]) !!}
                                  @endif
                              @else
                                  {!! Form::input('text','education[]' , null, [ 'class' => 'form-control' ]) !!}
                              @endif
                                  <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                  <a href="#" rel="1" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                              </div>
                          </div>
                          <br>
                          <div class="form-group">
                              Professional Memberships : <br>
                              <div class="fields">
                              @if($user->qualifications)
                                  @if($user->qualifications->affiliations)
                                      @foreach($user->qualifications->affiliations as $affiliation)
                                          {!! Form::input('text','affiliations[]' , $affiliation, [ 'class' => 'form-control' ]) !!}
                                      @endforeach
                                  @else
                                      {!! Form::input('text','affiliations[]' , null, [ 'class' => 'form-control' ]) !!}
                                  @endif
                              @else
                                  {!! Form::input('text','affiliations[]' , null, [ 'class' => 'form-control' ]) !!}
                              @endif
                                  <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                  <a href="#" rel="1" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                              </div>
                          </div>
                          <br>
                          <div class="form-group">
                              Awards and Publications : <br>
                              <div class="fields">
                              @if($user->qualifications)
                                  @if($user->qualifications->awards_and_publications)
                                      @foreach($user->qualifications->awards_and_publications as $award)
                                          {!! Form::input('text','awards[]' , $award, [ 'class' => 'form-control' ]) !!}
                                      @endforeach
                                  @else
                                      {!! Form::input('text','awards[]' , null, [ 'class' => 'form-control' ]) !!}
                                  @endif
                              @else
                                  {!! Form::input('text','awards[]' , null, [ 'class' => 'form-control' ]) !!}
                              @endif
                                  <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                  <a href="#" rel="1" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                              </div>
                          </div>
                          <br>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                          <div class="form-group">
                              Languages Spoken : <br>
                              <div class="fields">
                              @if($user->qualifications)
                                  @if($user->qualifications->languages)
                                      @foreach($user->qualifications->languages as $language)
                                          {!! Form::input('text','languages[]' , $language, [ 'class' => 'form-control' ]) !!}
                                      @endforeach
                                  @else
                                      {!! Form::input('text','languages[]' , null, [ 'class' => 'form-control' ]) !!}
                                  @endif
                              @else
                                  {!! Form::input('text','languages[]' , null, [ 'class' => 'form-control' ]) !!}
                              @endif
                                  <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                  <a href="#" rel="1" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                              </div>
                          </div>
                          <br>
                          <div class="form-group">
                              Board Certifications : <br>
                              <div class="fields">
                              @if($user->qualifications)
                                  @if($user->qualifications->insurances)
                                      @foreach($user->qualifications->insurances as $insurance)
                                          {!! Form::input('text','insurance[]' , $insurance, [ 'class' => 'form-control' ]) !!}
                                      @endforeach
                                  @else
                                      {!! Form::input('text','insurance[]' , null, [ 'class' => 'form-control' ]) !!}
                                  @endif
                              @else
                                  {!! Form::input('text','insurance[]' , null, [ 'class' => 'form-control' ]) !!}
                              @endif
                                  <a href="#" class="pull-right add_field_bt glyphicon glyphicon-plus-sign"></a>
                                  <a href="#" rel="1" class="pull-right remove_field_bt glyphicon glyphicon-minus-sign"></a>
                              </div>
                          </div>
                        <br>
                      </div>

                    </div><!--/.col (right) -->
                    </div>

              </div>   <!-- /.row -->
          </div>   <!-- /.row -->
        </div>   <!-- /.row -->

        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
        {!! Form::close() !!}
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/lbp.js') }}"></script>
@stop