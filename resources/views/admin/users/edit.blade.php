@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile |
        <a href="{{ route('adm_show_user' , $user->username) }}"><i class="fa fa-remove"></i> Cancel Changes</a> |
        <a href="{{ route('adm_edit_work_user' , $user->username) }}"><i class="fa fa-trash"></i> Edit Work Profile</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">

            {!! Form::open(['files' => true , 'route' => 'admin.users.doctors.update']) !!}
            {!! Form::hidden('user_id', $user->id ) !!}
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-md-12">
                    <h2 class="hs_heading"><i class="fa fa-user"></i> {{ $user->fname }} {{ $user->lname }}</h2>
                    <h4></h4>
                  </div>
              <div class="row">
                  <div class="col-md-4">
                    @if($user->avatar)
                        <img src="{{ asset('publicuploads/'.$user->avatar ) }}" alt="" width="200"/>
                    @else
                        <img src="http://placehold.it/100x100" alt="" width="200"/>
                    @endif

                    {!! Form::input('file', 'photo' , null , [ 'class' => 'form-control' ]) !!}
                  </div>
                  <div class="col-md-4 col-lg-offset-1">
                    <h4><i class="fa fa-user"></i> Email Address: </h4>
                    {!! Form::input('email', 'email_address' , $user->email , [ 'class' => 'form-control' , 'disabled' ]) !!}
                    Show e-mail address to public :

                    {!! Form::checkbox('email_address' , 1 , $user->show_email ) !!}
                    <br>
                    <h4><i class="fa fa-user"></i> Role: </h4>
                    {!! Form::select('role', $roles , $user->role , ['class' => 'form-control']) !!}
                  </div>
              </div>

              <div class="row">
                  <div class="col-md-12">
                    <h4><i class="fa fa-user"></i> Professional Summary</h4>
                    {!! Form::textarea('bio', $user->bio, [ 'class' => 'form-control textarea' ]) !!}

                    <p>Professional Summary (your professional medical summary will only be visible to other registered physicians on Middle East Doctor)</p>
                  </div>
                    {{--<div class="col-lg-6 col-md-6 col-sm-6"> Get connect with him:--}}
                      {{--<div class="hs_profile_social"><br>--}}
                        {{--<ul>--}}
                          {{--<li><a href=""><i class="fa fa-facebook"></i></a></li>--}}
                          {{--<li><a href=""><i class="fa fa-twitter"></i></a></li>--}}
                          {{--<li><a href=""><i class="fa fa-linkedin"></i></a></li>--}}
                          {{--<li><a href=""><i class="fa fa-skype"></i></a></li>--}}
                        {{--</ul>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  </div>
                </div><!--/.col (right) -->

            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
            {!! Form::close() !!}
            </div>   <!-- /.row -->
          </div>   <!-- /.row -->
        </div>   <!-- /.row -->
      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop