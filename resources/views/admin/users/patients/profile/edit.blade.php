@extends('admin.master')

@section('content')
        <section class="content-header">
              <h1>Doctors</h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
                <li class="active">Doctors</li>
              </ol>
        </section>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="hs_single_profile">
          {!! Form::open(['files' => true , 'route' => 'admin.users.patients.update']) !!}
            {!! Form::hidden('user_id', $user->id) !!}
            <div class="hs_single_profile_detail">
              <h3>
                {{ $user->fname.' '.$user->lname }} ( {{ $user->username }} )
                <span class="pull-right h-link"><a href="{{ route('admin.users.doctors', $user->username) }}"><< Back</a></span>
              </h3>

                @if(Session::has('error'))
                    <div class="alert alert-error">
                        {{ Session::get('error') }}
                    </div>
                @elseif(Session::has('message'))
                     <div class="col-md-12">
                       <div class="row">
                           <div class="alert alert-{{ Session::get('alert-type')  }}">
                               {{ Session::get('message') }}
                           </div>
                       </div>
                     </div>
                @endif

              <div class="row">
                  <div class="col-md-4">
                    <img src="{{ asset($user->avatar ) }}" alt="" width="100%"/>
                    {!! Form::input('file', 'photo' , null , [ 'class' => 'form-control' ]) !!}
                    @if($user->avatar)
                        Remove Avatar: {!! Form::checkbox('remove_thumb' ,1, false ) !!}
                    @endif
                  </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                          <h4><i class="fa fa-user"></i> Account Details:</h4>
                        <div class="col-md-6 no-padding">
                            First Name : <br><input type="text" name="fname" value="{{ $user->fname }}" class="form-control">
                                <br>
                        </div>
                        <div class="col-md-6 padding-r-0">
                            Last Name : <br><input type="text" name="lname" value="{{ $user->lname }}" class="form-control">
                                <br>
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="col-md-6 no-padding">
                            Email : <br><input type="text" name="email" value="{{ $user->email }}" class="form-control">

                        </div>
                        <div class="col-md-6 padding-r-0">
                            Username : <br><input type="text" name="username" value="{{ $user->username }}" class="form-control">
                        </div>
                        {{--Show e-mail address to public :--}}

                        {{--{!! Form::checkbox('email_address' , 1 , $user->show_email ) !!}--}}
                      </div>
                      <div class="col-md-8 margin-t-20">
                        <div class="col-md-6 no-padding">
                            Designation : <br><input type="text" name="designation" value="{{ $user->designation }}" class="form-control">
                        </div>
                        <div class="col-md-6 padding-r-0">
                            New Password <span style="font-size: 10px;">( Leave blank to keep old one )</span> :
                            <br>
                            {!! Form::password('password' , [ 'placeholder' => 'Password...', 'data-minlength' => '6', 'class' => 'form-control password-peeker' , 'id' => 'inputPassword']) !!}
                        </div>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 margin-t-20">
                      <hr>
                          <h4><i class="fa fa-phone"></i> Contact Number:</h4>
                          <div class="col-md-6 no-padding">
                                Personal : <br><input type="text" name="phone_number[0][personal]" value="<?php  if(isset($user->contacts[0])){ echo $user->contacts[0]->number; } ?>" class="form-control">

                          </div>
                          <div class="col-md-6 padding-r-0">
                                Contact : <br><input type="text" name="phone_number[0][work]" value="<?php  if(isset($user->contacts[1])){ echo $user->contacts[1]->number; } ?>" class="form-control">

                          </div>
                      </div>
              </div>

              <div class="row margin-t-20">
                  <div class="col-md-12">
                    <h4><i class="fa fa-user"></i> Professional Summary</h4>
                        <textarea name="bio" class="form-control textarea"> <?php  if(isset($user->profile->bio)){ echo $user->profile->bio; } ?> </textarea>

                    <p>Professional Summary (your professional medical summary will only be visible to other registered physicians on Middle East Doctor)</p>
                  </div>
                  </div>

            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
              </div>
            </div>

      {!! Form::close() !!}

      <!--Our Doctor Team end-->

    </div>
@stop

@section('js')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('public/js/dist/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('public/js/custom/trumbowyg.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/hideShowPassword.min.js') }}"></script>
    <script src="{{ asset('public/js/custom/password-peeker.js') }}"></script>
    <script type="text/javascript">
      $('select').select2();
    </script>
@stop