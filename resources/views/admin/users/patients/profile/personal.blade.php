<div class="hs_single_profile_detail padding-b-5 clearfix">

<a class="pull-right" onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('admin.users.doctors.destroy', $user->username) }}">
    <h4><i class="fa fa-trash"></i> Delete User</h4>
</a>

<hr class="margin-tb-5">
<div class="row">
    <div class="col-md-3 col-sm-3  col-xs-3 col-xxs-12 padding-b-0">
        <img src="{{ asset($user->avatar) }}" alt="{{ $user->name }}" width="100%"/>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-9 col-xxs-12 ">
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0margin-tb-0 padding-tb-0">
            <div class="row">
                <h3 class="margin-t-7 margin-tb-0">{{ $user->name }}</h3>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <h4 class="margin-tb-10">
              @foreach($user->specialties as $index=>$specialty)
                  {{ $specialty->name }} {{  ($index+1) < count($user->specialties) ? ', ' : '' }}
              @endforeach
              </h4>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 margin-tb-0 padding-tb-0">
                <div class="row line-height-22">
                    @forelse ($user->contacts as $contact)
                        <p class="text-capitalize">
                            <i class="fa fa-phone"></i> <b>{{ $contact->type }}: </b>
                            {{ $contact->number }}
                        </p>
                    @empty
                        <p>No phone number.</p>
                    @endforelse
                </div>
            </div>
        </div>

        <div class="ool-md-12 padding-10">
            <a href="{{ route('admin.users.doctors.edit', $user->username) }}">
                <button class=" t-black"><i class="fa fa-edit"></i> Edit Profile</button>
            </a>
        </div>
    </div>
</div>
