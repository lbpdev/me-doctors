@extends('admin.master')

@section('style')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <style>
        .alert {
            display: none;
            padding: 2px 10px;
        }
        .add-form {
            display: none;
        }
    </style>
@stop

@section('content')
    <div class="container profile page-container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 border-right">
                <div class="hs_single_profile">

                    @include ('admin.users.patients.profile.personal')
                    <hr class="margin-tb-5">

                        </div>
                    </div>
                </div>

        </div>
    </div>

@stop


@section('js')
@stop
