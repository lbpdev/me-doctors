@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile |
        <a href="{{ route('adm_edit_user' , $user->username) }}"><i class="fa fa-gear"></i> Edit Personal Details</a> |
        <a href="{{ route('adm_edit_work_user' , $user->username) }}"><i class="fa fa-suitcase"></i> Edit Work Profile</a> |
        <a href="{{ route('adm_destroy_user' , $user->id) }}"><i class="fa fa-trash"></i> Delete User</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Users</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">

                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-md-12">
                    <h2 class="hs_heading"><i class="fa fa-user"></i> {{ $user->fname }} {{ $user->lname }}</h2>
                    <h4></h4>
                  </div>
                  <div class="col-md-12">
                    @if($user->avatar)
                        <img src="{{ asset('publicuploads/'.$user->avatar ) }}" alt="" width="200" />
                    @else
                        <img src="http://placehold.it/100x100" alt="" />
                    @endif
                  </div>
                  <div class="col-md-6">
                    <h4 class="hs_heading"><b>Username</b>: {{ $user->username }}</h4>
                  </div>
                  <div class="col-md-6">
                     <h4 class="hs_heading"><b>E-mail</b>: {{ $user->email }}</h4><br>
                  </div>
                  <div class="col-md-6">
                    <h4 class="hs_heading"><b>First Name</b>: {{ $user->fname }}</h4>
                  </div>
                  <div class="col-md-6">
                     <h4 class="hs_heading"><b>Last Name</b>: {{ $user->lname }}</h4>
                  </div>
                  <div class="col-md-6">
                    <h4 class="hs_heading"><b>Designation</b>: {{ $user->designation }}</h4>
                  </div>
                  <div class="col-md-12">
                    <h4 class="hs_heading"><b>Professional Summary</b>: <br> {!! $user->bio !!}</h4>
                  </div>
                </div><!--/.col (right) -->
            </div>   <!-- /.row -->
          </div>   <!-- /.row -->
        </div>   <!-- /.row -->

        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">

                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-md-12">
                    <h2 class="hs_heading"><i class="fa fa-user"></i> Work Profile</h2>
                  </div>
                  <div class="row col-md-12">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <h4><i class="fa fa-phone"></i> Phone No: </h4>
                              <span class="text-capitalize">Personal: </span>
                              @if($user->personal_number)
                                  {{ $user->personal_number }}
                              @else
                                  N/A
                              @endif
                              <br>
                              <span class="text-capitalize">Office: </span>:
                              @if($user->office_number)
                                  {{ $user->office_number }}
                              @else
                                  N/A
                              @endif<br>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12">
                       <h4><i class="fa fa-stethoscope"></i> Where I work: </h4>
                              @if($user->hospital_name)
                                  {{ $user->hospital_name }} -
                              @else
                                   N/A
                              @endif
                              @if($user->hospital_address)
                                  {{ $user->hospital_address }} ,
                              @endif
                              @if($user->hospital_city)
                                  {{ $user->hospital_city }} -
                              @endif
                              @if($user->hospital_country)
                                  {{ $user->hospital_country }}
                              @endif
                      </div>
                  <div class="voffset8 clearfix hs_margin_30">&nbsp;</div>
                  <div class="qualifications">
                  <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Qualifications</h3></div>
                      <div class="col-md-12">
                          <h4><i class="fa fa-suitcase"></i> Education</h4>
                          @if($user->qualifications)
                              @foreach($user->qualifications->education as $education)
                                  {{ $education }}<br>
                              @endforeach
                          @else
                              N/A
                          @endif
                          <br>
                          <h4><i class="fa fa-hospital-o"></i> Professional Memberships</h4>
                          @if($user->qualifications)
                              @foreach($user->qualifications->affiliations as $affiliation)
                                  {{ $affiliation }}<br>
                              @endforeach
                          @else
                              N/A
                          @endif
                          <br>
                      </div>
                      <div class="col-md-12">
                          <h4><i class="fa fa-trophy"></i> Awards and Publications</h4>
                          @if($user->qualifications)
                              @foreach($user->qualifications->awards_and_publications as $awards)
                                  {{ $awards }}<br>
                              @endforeach
                          @else
                              N/A
                          @endif
                          <br>
                          <h4><i class="fa fa-heart"></i> Board Certifications</h4>
                          @if($user->qualifications)
                              @foreach($user->qualifications->insurances as $insurance)
                                  {{ $insurance }}<br>
                              @endforeach
                          @else
                              N/A
                          @endif
                          <br>
                      </div>
                      <div class="col-md-12">
                          <h4><i class="fa fa-comment-o"></i> Languages Spoken</h4>
                          @if($user->qualifications)
                              @foreach($user->qualifications->languages as $language)
                                  {{ $language }}<br>
                              @endforeach
                          @else
                              N/A
                          @endif
                          <br>
                      </div>
                  </div>

                </div><!--/.col (right) -->
            </div>   <!-- /.row -->
          </div>   <!-- /.row -->
        </div>   <!-- /.row -->

      </div>   <!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });
    </script>
@stop