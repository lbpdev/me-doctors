@extends('admin.master')

@section('content')
    <section class="content-header">
          <h1>Unverified Doctors</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li class="active">Unverified Doctors</li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="usersTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Place of Employment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($unverifiedDoctors as $doctor)
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.users.unverified.show', $doctor->username) }}">
                                            {{ $doctor->id }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.users.unverified.show', $doctor->username) }}">
                                                {{ $doctor->fname }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.users.unverified.show', $doctor->username) }}">
                                                {{ $doctor->lname }}
                                            </a>
                                        </td>
                                        <td>{{ $doctor->email }}</td>
                                        <td>{{ $doctor->credentials->phone_number }}</td>
                                        <td>{{ $doctor->credentials->place_of_employment }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Place of Employment</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop

@section('js')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        (function($) {
            var showUserLink = function(text, username) {
                var url = "{{ route('admin.users.unverified.show', ':username') }}";

                return '<a href="'+ url.replace(":username", username) +'">'+ text + '</a>';
            };

            $("#usersTable").dataTable({
                processing: true,
                "pageLength": 20
            });
        })(jQuery);
    </script>
@stop