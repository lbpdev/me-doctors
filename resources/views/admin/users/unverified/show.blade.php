@extends(' admin.master')

@section('content')
    <section class="content-header">
          <h1>Unverified Doctors</h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Admin</a></li>
            <li><a href="{{ route('admin.users.unverified') }}">Unverified Doctors</a></li>
          </ol>
    </section>

    <!-- Main content -->
    <section class="content user-details">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row user-details-header">
                            <div class="col-xs-6">
                                <h2>{{ $user->name }}</h2>
                            </div>
                            <div class="col-xs-6">
                                <div class="pull-right h3">
                                    {!! Form::open([
                                        'method' => 'PATCH',
                                        'route'  => ['admin.users.verify', $user->id], 
                                        'class'  => 'inline-block
                                    ']) !!}
                                        <button class="btn-link" type="submit"><i class="fa fa-check"></i> Verify</button>
                                    {!! Form::close() !!}
                                    <span>|</span>


                                    {!! Form::open([
                                        'method' => 'DELETE',
                                        'route'  => ['admin.users.destroy', $user->id], 
                                        'class'  => 'inline-block
                                    ']) !!}
                                        <button class="btn-link" type="submit"><i class="fa fa-trash-o"></i> Remove</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 user-details-body">

                                <p><b>Email: </b> {{ $user->email }}</p>
                                <p><b>Designation: </b> {{ $user->designation }}</p>
                                <p><b>Specialties: </b> {{ implode(', ', $user->specialties->lists('name')) }}</p>
                                <p><b>Phone Number: </b> {{ $user->credentials->phone_number }}</p>
                                <p><b>Place of Employment: </b> {{ $user->credentials->place_of_employment }}</p>

                                <hr />

                                <b>Medical License:</b> 
                                @include('partials.files.list', ['files' => $user->credentials->licenseFiles()])
                                
                                <b>Photo Identifications:</b> 
                                @include('partials.files.list', ['files' => $user->credentials->photoIdentifications()])

                                @if ($otherFiles = $user->credentials->otherFiles())
                                    <b>Extra Attachments:</b> 
                                    @include('partials.files.list', ['files' => $otherFiles])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop