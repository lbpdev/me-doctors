@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Videos |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Videos</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true, 'route' => 'admin.videos.store']) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Add a new Video</h3></div>
                              <div class="col-md-12">
                                  <br
                                      <div class="row">
                                          <div class="col-md-10">
                                        Title : <br>
                                        {!! Form::input('text','title' , null, [ 'class' => 'form-control' , 'required' ]) !!}

                                        Description : <br>
                                        {!! Form::textarea('description' , null, [ 'class' => 'form-control textarea' ]) !!}

                                        Source : <br>
                                        {!! Form::input('text','source' , null, [ 'class' => 'form-control' , 'required' , 'id' => 'autocomplete'  ]) !!}

                                        <br><br>Thumbnail ( Not required if source is Youtube): <br>
                                        {!! Form::input('file', 'thumbnail' , null , [ 'class' => 'form-control' ]) !!}

                                        <br><br><b>Channel</b> : <br>
                                        {!! Form::select('channel_id', $channels, 2 , [ 'class' => 'form-control' ]) !!}

                                        <br>
                                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                      </div><br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>

    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });

    </script>

@stop