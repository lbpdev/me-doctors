@extends('admin.master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Video |
        <a href="{{ route('admin.videos.index') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Video</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            {!! Form::hidden('id', $video->id ) !!}

            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Edit Video</h3></div>
                              <div class="col-md-12">
                                  <br
                                      <div class="row">
                                        <div class="col-md-10">

                                         Title : <br>
                                        {!! Form::input('text','title' , $video->title, [ 'class' => 'form-control' , 'required' ]) !!}

                                        Description : <br>
                                        {!! Form::textarea('description' , $video->description, [ 'class' => 'form-control textarea' ]) !!}

                                        Source : <br>
                                        {!! Form::input('text','source' , $video->source, [ 'class' => 'form-control' , 'required' , 'id' => 'autocomplete'  ]) !!}

<br><br>
                                        Thumbnail ( Not required if source is Youtube): <br>
                                        @if($video->thumbnail)
                                            <img src="{{ URL::to($video->thumbnail) }}" width="150"><br>
                                            Remove Thumbnail: {!! Form::checkbox('remove_thumb' ,1, false ) !!}
                                        @endif

                                        {!! Form::input('file', 'thumbnail' , null, [ 'class' => 'form-control' ]) !!}

                                        <br><br><b>Channel</b> : <br>
                                        {!! Form::select('channel_id', $channels, $video->channel_id , [ 'class' => 'form-control' ]) !!}

                                        <br>
                                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                                      </div><br>
                                  </div>
                              </div>
                          </div>
                            <br>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>


    <script>
        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });

    </script>

@stop