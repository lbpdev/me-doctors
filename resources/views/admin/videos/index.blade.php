@extends('admin.master')

@section('content')
    <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Videos
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Videos</a></li>
              </ol>
            </section>

            <!-- Main content -->
            <section class="content">
              <div class="row">
                <div class="col-xs-12">
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">All Videos</h3><br><br>
                        <a href="{{ route('admin.videos.create') }}">
                            <h3 class="box-title"><i class="fa fa-plus-circle"></i> Add an Video</h3>
                        </a>
                      </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Source</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if(count($videos)>0)
                            @foreach($videos as $video)
                              <tr>
                                <td><a href="{{ route('admin.videos.show', $video->id) }}">{{ $video->id }}</a></td>
                                <td><a href="{{ route('admin.videos.show', $video->id) }}">{{ $video->title }}</a></td>
                                <td>{!! Str::words($video->description,10) !!}</td>
                                <td>{{ $video->source }}</td>
                              </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>No Videos</td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                        @endif
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Source</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->
            </section><!-- /.content -->
@stop

@section('js')
    <!-- DATA TABES SCRIPT -->

    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    </script>
@stop