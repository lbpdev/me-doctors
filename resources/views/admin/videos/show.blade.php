@extends('admin.master')

@section('style')
    <link rel="stylesheet" href="{{ asset("css/jquery.fancybox.css") }}">
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Videos |
        <a href="{{ route('adm_surveys') }}"><i class="fa fa-remove"></i> Cancel</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Videos</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
          <div class="row">
            {!! Form::open(['files' => true]) !!}
            <!-- left column -->
                  <div class="col-md-12 col-sm-12">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                        <div class="qualifications">
                          <div class="col-md-12">
                          <h3><i class="fa fa-check-square-o"></i>{{ $video->title }}
                          <div class="pull-right">
                                <span class="pull-left"><a href="{{ route('admin.videos.edit', $video->id) }}"><i class="fa fa-pencil"></i> Edit </a></span>
                                <span class="pull-left">&nbsp;|&nbsp; </span>
                                <span class="pull-left"><a onclick="return confirm('Are you sure you want to delete this?')" href="{{ route('admin.videos.delete', $video->id ) }}"><i class="fa fa-trash-o"></i> Delete</a></span>
                          </div></h3>
                          </div>
                              <div class="col-md-12">
                                  <br>

                                      <div class="row">
                                        <div class="col-md-10">
                                        @if($video->thumbnail)
                                            <img src="{{ URL::to($video->thumbnail) }}" width="250">
                                        @endif
                                        </div>
                                        <div class="col-md-10"><br>
                                        <b>Title :</b> <br>
                                        {{ $video->title }}<br><br>

                                        <b>Description :</b> <br>
                                        {!! $video->description !!}<br><br>

                                        <iframe width="400" height="300" src="{{ strpos($video->YoutubeId, 'leadingbrands') ? $video->YoutubeId : 'http://www.youtube.com/embed/'.$video->YoutubeId }}?autoplay=1"></iframe>

                                      </div><br>
                                  </div>
                              </div>
                          </div>
                            <br>
                          </div>
                      </div><!--/.col (right) -->
                  </div>
          </div>   <!-- /.row -->
            {!! Form::close() !!}
    </section>   <!-- /.row -->
@stop

@section('js')
    <script src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/fancybox.js') }}"></script>
@stop