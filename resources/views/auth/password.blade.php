@extends('doctor.master')

@section('content')
<div class="container login-container">
    <div class="col-lg-8 col-md-8 col-sm-12 margin-t-20 register">
    <div class="row">
        <h4 class="margin-t-0 doctor-blue">Reset Password</h4>
        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @else

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <label class="col-md-12 text-left no-padding">E-Mail Address</label>
                    <div class="col-md-8 no-padding">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>

                    <div class="col-md-12 margin-t-10 no-padding">
                        <button type="submit" class="btn btn-primary pull-left blue-bg">
                            Send Password Reset Link
                        </button>
                    </div>
                </form>

            @endif
        </div>
    </div>
</div>

    @include('doctor.templates.login-sidebar')
	</div>
</div>
@endsection
