@extends('doctor.master')

@section('content')
<div class="container login-container">
    <div class="col-lg-8 col-md-8 col-sm-12 margin-t-20 register">
        <div class="row">
        <h4 class="margin-b-10">Reset Password</h4>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    @if(Session::has('message'))
                        <span class="alert alert-success">{{ Session::get('message') }}</span>
                    @endif

                    <form class="form-horizontal" role="form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">

                            <label class="col-md-12 no-padding">E-Mail Address</label>
                            <div class="col-md-8 no-padding">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>

                            <label class="col-md-12 no-padding">New Password</label>
                            <div class="col-md-8 no-padding">
                                <input type="password" class="form-control" name="password">
                            </div>

                            <label class="col-md-12 no-padding">Confirm New Password</label>
                            <div class="col-md-8 no-padding">
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>

                            <div class="col-md-12 no-padding margin-t-10">
                                <button type="submit" class="btn btn-primary blue-bg">
                                    Reset Password
                                </button>
                            </div>
                    </form>
                </div>
        </div>
    </div>

    @include('doctor.templates.login-sidebar')
	</div>
</div>
@endsection
