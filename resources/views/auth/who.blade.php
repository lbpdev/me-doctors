{{-- WHO AM I --}}
<div class="modal fade who-am-i" id="whoAmI" tabindex="-1" role="dialog" aria-labelledby="whoAmILabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title" id="whoAmILabel">WHO AM I?</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-offset-1 col-xs-5 who-item"
                         data-login='{"text":"DOCTOR LOGIN","image":"#doctorImage"}'
                         data-login-url="{{ url(route('doctor.login')) }}"
                         data-register-url="{{ url(route('doctor.register')) }}">
                        <img src="{{ asset('public/images/icon/who-am-i/doctor.png') }}" alt="Doctor" id="doctorImage">
                        <span>DOCTOR</span>
                    </div>
                    <div class="col-xs-5 who-item"
                             data-login='{"text":"PATIENT LOGIN","image":"#patientImage"}'
                             data-login-url="{{ url(route('patient.login')) }}"
                             data-register-url="{{ url(route('patient.register')) }}">
                            <img src="{{ asset('public/images/icon/who-am-i/patient.png') }}" alt="Patient" id="patientImage">
                            <span>PATIENT</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-offset-1 col-xs-5 who-item">
                    <img src="{{ asset('public/images/icon/who-am-i/facility.png') }}" alt="Healthcare Facility">
                    <span>HEALTHCARE FACILITY</span>
                </div>
                <div class="col-xs-5 who-item">
                    <img src="{{ asset('public/images/icon/who-am-i/other.png') }}" alt="Other">
                    <span>OTHER</span>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- LOGIN MODAL --}}
<div class="modal fade who-am-i" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        {!! Form::open(['route' => 'doctor.login', 'data-toggle' => 'validator']) !!}
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title" id="loginModalLabel">&nbsp;</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::input('text' , 'username_email' , null, ['required', 'class' => 'form-control', 'placeholder' => 'Username or Email...']) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        {!! Form::password('password' , [ 'required', 'class' => 'form-control', 'placeholder' => 'Password...' ]) !!}
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn-back pull-left" data-toggle="modal" data-target="#whoAmI" data-dismiss="modal"></button>
                    <div class="btn-group">
                        <a href="#" id="registerLink" class="modal-submit btn">Register</a>
                        {!! Form::submit('Login', ['class' => 'btn modal-submit']) !!}
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>