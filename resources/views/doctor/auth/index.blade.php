@extends('doctor.master')

@section('styles')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
@stop

@section('content')
    <div class="container login-container">
        <div class="row">

            {{-- REGISTRATION FORM --}}
            <div class="col-lg-8 col-md-8 col-sm-12 margin-t-5 register border-right-light border-lg">
                <h3 class="border-bottom-light width-full pull-left hs_heading margin-t-0  margin-b-4 clearfix">
                    <span class="page-icon doctor blue"></span>
                    <span class="page-title doctor-blue">Register</span>
                </h3>

                @if (session()->has('registration.success'))
                    <div class="alert alert-success">
                        {{ session('registration.success') }}
                    </div>
                @else
                    <p>You must be a licensed healthcare professional (HCP), practicing in the Middle East, to join Middle East Doctor.</p>

                    {!! Form::open([
                        'route'       => 'doctor.register',
                        'data-toggle' => 'validator',
                        'role'        => 'form' ,
                        'class'       => 'clearfix'
                    ]) !!}

                        @include('doctor.registration.countries')

                        <div class="row {{ session()->has('registration.error') || $errors->any() ? '' : 'register-form' }}" id="register-form">
                            @include('doctor.registration.form')

                            <div class="form-group">
                                {!! Form::submit('Register', [ 'class' => 'submit button pull-left']) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                @endif
            </div>
        
            {{-- LOGIN FORM --}}
            <div class="col-lg-4 col-md-4 col-sm-12 padding-l-10 login-panel">

                @include('doctor.auth.login')
                <hr />
                <a href="http://thepatient.me/" target="_blank">
                    <img src="{{ asset('public/images/ads/the-patient.png') }}" width="100%">
                </a>
            </div>
        </div> <!-- END OF ROW -->
    </div> <!-- END OF CONTAINER -->
@stop

@section('js')
    @include('doctor.auth.scripts')
    <script type="text/javascript" src="{{ asset('public/js/custom/profileFields.js') }}"></script>
    <script>
        $('#country').
        $('#invalid-country')
    </script>
@stop