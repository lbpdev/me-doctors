<div class="clearfix margin-t-5">

    <h3 class="border-bottom-light width-full pull-left hs_heading margin-t-0 margin-b-4 clearfix">
        <span class="page-icon doctor blue"></span>
        <span class="page-title doctor-blue">Login</span>
    </h3>
    @if(session()->has('message'))
        <div class="form-group">
            <div class="alert alert-danger" role="alert">
                {!! session('message') !!}
            </div>
        </div>
    @endif
        {!! Form::open([ 'route' => 'doctor.login', 'data-toggle' => 'validator' , 'role' => 'form', 'id' => 'login-form']) !!}
        <div class="form-group {{ $errors->first('username_email') ? 'has-error' : '' }}">
            {!! Form::label('Username' , 'Username or E-mail: ' ) !!}
            {!! Form::input('text' , 'username_email' , null, [ 'required', 'class' => 'form-control']) !!}
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group  {{ $errors->first('password') ? 'has-error' : '' }}">
            {!! Form::label('Password' , 'Password: ' ) !!}
            {!! Form::password('password' , [ 'required', 'data-minlength' => '6', 'class' => 'form-control' , 'id' => 'inputPassword']) !!}
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'form-control']) !!}
        </div>
        {!! Form::close() !!}
        <br>
        Forgot your password? <a href="{{ route('password.forgot') }}">Click here</a>.
</div> <!-- END OF PANEL -->
<div class="hs_margin_60"></div>