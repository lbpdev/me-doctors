<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('public/js/validator.js') }}"></script>
<script src="{{ asset('public/js/custom/validation.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('public/js/hideShowPassword.min.js') }}"></script>
<script src="{{ asset('public/js/custom/password-peeker.js') }}"></script>
<script src="{{ asset('public/js/custom/registration.js') }}" type="text/javascript"></script>