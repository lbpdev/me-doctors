@include('doctor.templates.modals.refer')

<section class="footer clearfix">
    <div class="container padding-r-0">
        <div class="col-md-8 margin-b-sm-5 ">
            <h2 class="hidden">Footer</h2>
            <div class="row">
                <ul class="nav-links">
                    <li><a href="{{ URL::to('doctor/about') }}">About Middle East Doctor</a></li>
                    <li><a href="{{ URL::to('doctor/terms') }}">Terms of Use</a></li>
                    <li><a href="{{ URL::to('doctor/contact') }}">Contact Us </a></li>
                    {{--<li><a href="#" data-toggle="modal" data-target="#refer">Refer to a friend </a></li>--}}
                </ul>
            </div>
        </div>
        <div class="col-md-4 copyright">
            <div class="row">
                <div class="col-md-8 text-right">
                    <img src="{{ asset('public/images/MED-Footer.png') }}" width="80" class="margin-r-7" alt="MED logo" title="MED logo">
                </div>
                <div class="col-md-4 text-left">
                  <div class="row">
                  Copyright <a href="#">2016</a><br>
                  Middle East Doctor LLC (Delaware, USA)
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div style="opacity:0">

    <!-- Facebook -->
    <a href="http://www.facebook.com/sharer.php?u=http://middleeastdoctor.com/" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
    </a>

    <!-- Google+ -->
    <a href="https://plus.google.com/share?url=http://middleeastdoctor.com/" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
    </a>

    <!-- LinkedIn -->
    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://middleeastdoctor.com/" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
    </a>

    <!-- Twitter -->
    <a href="https://twitter.com/share?url=http://middleeastdoctor.com/&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons" target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
    </a>

</div>

@include('auth.who')

<script type="text/javascript" src="{{ asset('public/js/jquery-1.11.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/custom/fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.powertip.min.js') }}"></script>
<script src="{{ asset('public/js/custom/authenticator.js') }}" type="text/javascript"></script>


{{--@if (Request::path()=='doctor')--}}
    {{--<script>--}}
        {{--[--}}
            {{--'{{ asset('public/js/plugins.js') }}',--}}
        {{--].forEach(function(src) {--}}
            {{--var script = document.createElement('script');--}}
            {{--script.src = src;--}}
            {{--script.async = false;--}}
            {{--document.head.appendChild(script);--}}
        {{--});--}}
    {{--</script>--}}
{{--@else--}}
    {{--<script type="text/javascript" src="{{ asset('public/js/plugins.js') }}"></script>--}}
{{--@endif--}}
<!--main js file end-->

@yield('sidebar-js')
@yield('js-inner')
@yield('js-alt')
@yield('js-alt-2')
@yield('js')

<script>
    $(document).ready(function(){
        $('.powerTip').powerTip({
            placement: 'sw' // south-west tooltip position
        });

        function tick(){
            $('#ticker li:first').slideUp( function () { $(this).appendTo($('#ticker')).slideDown(); });
        }
        setInterval(function(){ tick () }, 5000);


        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        $('#referEmail').keydown(function(event){
            if(event.keyCode === 13) {

                if(re.test($(this).val()))
                    $('#invalidReferEmail').hide();
                else
                {
                    event.preventDefault();
                    $('#invalidReferEmail').show();
                    return false;
                }
                $('#submitRefer').trigger('click');
                return false;
            }
        });

        $('#submitRefer').on('click',function(e){
            e.preventDefault();

            var url = '{{ route('refer.send') }}';

            if(re.test($('#referEmail').val())){
                $('#invalidReferEmail').hide();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: { email : $('#referEmail').val() },
                    context: document.body,
                    success: function(result){
                        $('#invitationSentTo').html('').append('Invitation sent to '+result);
                        $('#referralSent').show();
                        $('#invalidReferEmail').hide();
                        $('#referEmail').val('');
                    }
                }).done(function() {
                    $( this ).addClass( "done" );
                });

            } else {
                $('#invalidReferEmail').show();
            }

        });

    });

</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>


