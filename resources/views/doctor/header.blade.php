<head>
    <meta charset="utf-8">
    <meta name="description" content="MED provides an interactive platform where physicians across the region can connect with peers across multiple therapy areas and subspecialties." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="keywords" content="MED,Leadingbrand,Middle-east Doctors">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('publicappIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('publicappIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('publicappIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('publicappIcons/MED-Icon144x144.png') }}" />

    <!-- Dublin Core -->
    <meta name="DC.Title" content="Middle East Doctors - Official Website">
    <meta name="DC.Creator" content="Leading Brands JLT">
    <meta name="DC.Description" content="Leading Brands is a Dubai-based healthcare communications agency that creates award-winning patient education campaigns and marketing tools to engage with doctors.">
    <meta name="DC.Language" content="English">


    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="www.middleeastdoctor.com">
    <meta name="twitter:title" content="Middle East Doctors">
    <meta name="twitter:description" content="Middle East Doctor provides an interactive platform where physicians across the region can connect with peers across multiple therapy areas and subspecialties. ">

    <!-- Open Graph data -->
    <meta property="og:title" content="Middle East Doctors" />
    <meta property="og:type" content="webpage" />
    <meta property="og:url" content="http://www.middleeastdoctor.com/" />
    <meta property="og:image" content="http://middleeastdoctor.com/images/MED-Logo.png" />
    <meta property="og:description" content="Middle East Doctor provides an interactive platform where physicians across the region can connect with peers across multiple therapy areas and subspecialties. " />
    <meta property="og:site_name" content="www.middleeastdoctor.com" />
    <meta property="fb:admins" content="100000370643856" />

    @yield('metas')

    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="{{ asset('publicfavicon-32x32.png') }}" />
    <link rel="icon" type="image/ico" href="{{ asset('publicfavicon-32x32.png') }}" />

    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}" media="all"/>
    <link href="{{ asset('public/css/print.css') }}" rel="stylesheet" media="print">

    <title>Middle East Doctors {{ isset($pageName) ? ' - '.$pageName : '' }}</title>

    <script>
        [
            '{{ asset('public/js/plugins.js') }}',
        ].forEach(function(src) {
            var script = document.createElement('script');
            script.src = src;
            script.async = false;
            document.head.appendChild(script);
        });
    </script>

    <meta name="csrf_token" content="{{ csrf_token() }}" />
    @include('doctor.templates.analyticstracking')

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<header>
<section class="ad">
    <div class="width-full">
        <div class="container overflow-hidden padding-0-xs padding-r-0">
            <h2 class="hidden">Ads</h2>
            @include('templates.top-long')
            @include('templates.top-sm')
            {{--<a href="{{ URL::to('/') }}"><img src="http://placehold.it/480x100"  id="header-ad-mobile" width="100%"></a>--}}
        </div>
    </div>
</section>

<section class="banner">
    <div class="container padding-r-0 padding-r-10-xs">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 col-xss-12 logo-wrap padding-tb-8 ">

                <h2 class="hidden">Banner</h2>
                <div class="col-md-12 no-padding margin-b-4">
                    <span>{{ Carbon::now()->format('l') }}</span>
                    <span> {{ Carbon::now()->format('j') }}<sup>{{ Carbon::now()->format('S') }}</sup></span>
                    <span>{{ Carbon::now()->format('F') }}</span>
                    <span>{{ Carbon::now()->format('Y') }}</span>
                </div>

                <div class="col-md-12 no-padding margin-tb-4 margin-r-10 pull-left">
                    <span class="pull-left">{{ $today['city'] }}</span>
                </div>

                <div class="col-md-12  col-sm-12  no-padding text-right location">
                    <div class="col-md-12 pull-left margin-tb-4 margin-r-10 padding-r-10">
                        <div class="row">
                            <span class="weather-icon sun pull-left margin-r-5"></span>
                            <p class="pull-left" id="sunrise">{{ $today['sunrise'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 pull-left margin-tb-4 margin-r-10 padding-r-10">
                        <div class="row">
                            <span class="weather-icon margin-r-5     moon pull-left"></span>
                            <p class="pull-left" id="sunset">{{ $today['sunset'] }}</p>
                        </div>
                    </div>
               </div>

            </div>

            <div class="col-md-4 col-sm-4 col-xs-6 col-xss-6 padding-b-8 text-center text-left-xs padding-t-4">

                <div class="col-md-12 padding-b-4">
                    <a href="{{ url('doctor') }}" class="logo-link">Doctor Section</a> |
                    <a href="{{ url('patient') }}" class="logo-link">Patient Section</a>
                </div>
                <a href="{{ URL('/doctor') }}"><img src="{{ asset('public/images/MED-Logo.png') }}" class="head-logo padding-t-4 border-top-light" alt="MED logo" title="MED Logo"></a>

            </div>

            <div class="col-md-4 col-sm-4 col-xs-6 user-panel padding-tb-8">
                @if(isset($authUser))
                    <span class="link margin-b-4">
                        <a class="powerTip" title="Click to view profile." href="{{ route('doctor.profile', $authUser->username) }}">{{ $authUser->name }}</a>
                    </span>
                    <span class="link">
                        <a href="{{ route('messages_show') }}" class="icon inbox powerTip" title="Messages">
                            @if($unread_messages)
                                <span class="noti-count">{{ $unread_messages }}</span>
                            @endif
                        </a>
                    </span>
                    <span class="link"><a href="{{ route('doctor.logout') }}" class="icon logout powerTip" title="Logout"></a></span>
                    {{--<span class="link"><a href="{{ URL::to('doctor/about') }}" class="icon help"></a></span>--}}
                @else
                    <span class="link"><a href="{{ route('doctor.login') }}" class="icon login powerTip" title="Login"></a></span>
                    <span class="link"><a href="#" class="icon change powerTip" title="Change User" data-toggle="modal" data-target="#whoAmI"></a></span>
                    <span class="link"><a href="{{ URL::to('doctor/about') }}" title="Help" class="icon powerTip help"></a></span>
                @endif
            </div>
        </div>
    </div>
</section>

<section class="menu">
    <div class="container">
        <h2 class="hidden">Menu</h2>
        <nav class="navbar">
          <div class="container-fluid padding-r-0">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="mobile-search temp-hide show-sm">
                @include('doctor.partials._search')
              </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="{{ strpos(Request::path(), 'news')  ? 'active' : '' }}"><a title="News" href="{{ route('doctor.news.index') }}">News</a></li>
                <li class="{{ strpos(Request::path(), 'articles')  ? 'active' : '' }}"><a title="Articles" href="{{ route('doctor.articles.index') }}">Articles</a></li>
                <li class="{{ strpos(Request::path(), 'discussions')  ? 'active' : '' }}"><a title="Discussions" href="{{ route('doctor.discussions.index') }}">MED Talk</a></li>
                <li class="{{ strpos(Request::path(), 'diagnosis')  ? 'active' : '' }}"><a title="Diagnosis" href="{{ route('doctor.diagnosis.index') }}">MED Cases</a></li>
                <li class="{{ strpos(Request::path(), 'surveys')  ? 'active' : '' }}"><a title="Surveys" href="{{ route('doctor.surveys.index') }}">MED Voice</a></li>
                <li class="{{ strpos(Request::path(), 'doctors')  ? 'active' : '' }}"><a title="Doctors Directory" href="{{ route('doctor.doctors.index') }}">Doctors</a></li>
                <li class="{{ strpos(Request::path(), 'hospitals')  ? 'active' : '' }}"><a title="Hospital/Healthcare Directory" href="{{ route('doctor.hospitals.index') }}">Hospitals</a></li>
                @if(isset($authUser))
                    <li class="pull-right {{ strpos(Request::path(), 'hospitals')  ? 'active' : '' }}">
                        <a title="Advertise" href="{{ $authUser->id==1 ? URL::to('doctor/advertise') : URL::to('doctor/contact') }}">Advertise</a>
                    </li>
                @endif
              </ul>
            </div>
          </div>
        </nav>
    </div>
    @if(isset($authUser))
        @if($authUser->hasRole('Administrator'))
            <a href="{{ url('admin') }}" class="admin-link">Go to backend</a>
        @endif
    @endif
</section>
</header>

<div class="hidden">

    Middle East Doctor provides an interactive platform where physicians across the region can connect with peers across multiple therapy areas and subspecialties.

    Middle East Doctor is a website community to help connect doctors and HCPs across the region and provide them with a space where they can interact with peers across multiple therapy areas and subspecialties. HCPs can read the latest industry headlines, relevant medical articles, seek expert advice and opinions on treatments, and participate in discussions and surveys. Users have the option of creating a free or premium profile and building their online account.The website is carefully monitored for authentic, genuine users and ethical activity. Access to features such as surveys, discussions and diagnosis is done by logging in, and is available to HCPs only. Patients and the rest of the general public will not be given access to this part of the website.
</div>