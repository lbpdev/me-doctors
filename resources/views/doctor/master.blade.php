<!DOCTYPE html>
<html lang="en">

@include('doctor.header')

<div class="main-wrapper">
    @yield('content')
</div>

@include('doctor.footer')
