@extends('doctor.master')

@section('content')

<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-10">
                @include('doctor.templates.about-menu')

                <div class="col-md-9 clearfix row-sm padding-r-0 contents page about">
                    <h3 class="no-border hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix">
                        <span class="page-title doctor-blue">About MED</span>
                    </h3>


                        <h4 class="doctor-blue padding-b-5">For Healthcare Professionals (HCPs)</h4>
                        <p>Middle East Doctor is a website community to help connect doctors and HCPs across the region and provide them with a space where they can interact with peers across multiple therapy areas and subspecialties. HCPs can read the latest industry headlines, relevant medical articles, seek expert advice and opinions on treatments, and participate in discussions and surveys. Users have the option of creating a free or premium profile and building their online account.</p>

                        <p>The website is carefully monitored for authentic, genuine users and ethical activity. Access to features such as surveys, discussions and diagnosis is done by logging in, and is available to HCPs only. Patients and the rest of the general public will not be given access to this part of the website.</p>

                        <p>Please refer to the Terms of Use for a guide on how to use Middle East Doctor.</p>

                        <h4 class="doctor-blue padding-b-5">Etiquette</h4>
                        <p>Users must remain professional and courteous at all times and protect the identity of their patients, upholding all patient confidentiality. If your judgement is poor, Middle East Doctor may/will moderate your comments accordingly. Users who violate the Terms of Use or require regular moderation may have their accounts suspended or removed.</p>
                        <p>Abusive, threatening or offensive remarks will not be tolerated; profanity, name-calling, racial, religious, cultural, political, personal, professional attacks or otherwise negative behaviour are strictly prohibited. Any users engaging in this type of activity can be removed from the website.</p>

                        <p><b>When uploading images always ensure that the patient’s identity has not been revealed and the image is in no way culturally insensitive and that it is medically complaint.</b></p>



                </div>
        </div>
        @include('doctor.templates.sidebar-ads')
    </div>
</div>
</div>
@stop

@section('js')
    {{--<script src="{{ asset('public/js/tweets.js') }}"></script>--}}
@stop