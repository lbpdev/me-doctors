@extends('doctor.master')

@section('content')
<div class="container page-container advertise">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0">

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-b-4">
                    <h3 class="no-border doctor-blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
                        <span class="page-icon advertise"></span>
                        <span class="page-title">Advertise on MED</span>
                    </h3>
                <hr class="margin-tb-5">
                Liciisquam que odis et evelest iaessimus, cusaerit es doloriae cusda ditasit peres maximpos di dolorporem faceaque nonsequam vidipit es asinventis re consed ut remquo entur sa peles solupta ssuntur, te lit officim aut ped quoditiuntur ma necture iuntusandi beaquis cuscipsa persper ferovidi occum rem evel int.
                Venihiliquae ni ut a consed mil est poreiunt ime arum fugit parcian daerfero eicias nos susam fuga. Aditiam laboribus acernatur sit untota quibero odit, odis alitiae et lacea pel il iunt untotam etur si volleste

                @if(count($pending_purchases))
                    @include ('doctor.pages.advertise._doctor-pending')
                @endif


                    {{--@include ('doctor.pages.advertise._patient-banner')--}}
                    {{--@include ('doctor.pages.advertise._patient-content')--}}
                    {{--@include ('doctor.pages.advertise._doctor-content')--}}

                {!! Form::open(['route'=>'doctor.advertise.queue','method'=>'POST']) !!}
                    @include ('doctor.pages.advertise._doctor-banner')
                    <input type='submit' value='Make Request' class="font-18 pull-right pink button" />
                {!! Form::close() !!}
                </div>
              </div>
              </div>

          <div class="hs_margin_40"></div>
        </div>

        @include('doctor.templates.sidebar-ads')
        </div>
    </div>
</div>
@stop


@section('js')
<script>
    $(".ad-file-select").change(function () {
        $(this).parent().find('.add-link-txt').attr('required','required');
    });
</script>
@endsection