<hr class="margin-tb-10">
<h4 class="no-border doctor-blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix pull-left">
    <span class="small-icon patient"></span>
    <span class="font-21">Web Banners - Patient Section</span>
</h4>
<div class="col-md-12 advert-table">
        @foreach($products as $index=>$product)
        <div class="row">
            <div class="col-md-6 padding-l-0 padding-tb-10">

            {{--<input type='hidden' name='product[{{$index}}][type]' value='product' />--}}
            {{--<input type='hidden' name='product[{{$index}}][name]' value='{{ $product->name }}' />--}}
            {{--<input type='hidden' name='product[{{$index}}][price]' value='{{ $product->price }}' />--}}
            {{--<input type='hidden' name='product[{{$index}}][tangible]' value='N' />--}}
            {{--<input type='hidden' name='product[{{$index}}][recurrence]' value='1 Month' />--}}
            <table>
                <tr>
                    <td>
                      <input id="radioProduct_id{{ $product->id }}" class="square" type="checkbox" name="product[{{$index}}][premium_service_id]" value='{{ $product->id }}' >
                      <label for="radioProduct_id{{ $product->id }}"><span></span></label>
                    </td>
                    <td class="padding-l-10">
                        {{ $product->name }} - ${{ $product->price }} per month <br>
                        (maximum four adverts in rotation)
                    </td>
                </tr>
            </table>
            </div>
            <div class="col-md-2 padding-t-0">
                <div class="col-md-12 no-padding">
                Duration: <br>
                    <div class="small custom-select gray-bg">
                        <select name="product[{{$index}}][duration]" class="form-control pull-left">
                            <option value="1 Month">1 Month</option>
                            <option value="2 Month">2 Months</option>
                            <option value="4 Month">4 Months</option>
                            <option value="6 Month">6 Months</option>
                            <option value="1 Year">1 Year</option>
                        </select>
                    </div>
                 </div>
            </div>
            <div class="col-md-2 padding-t-0">
                <div class="col-md-12 no-padding">
                Quantity: <br>
                    <div class="small custom-select gray-bg">
                        <select name="product[{{$index}}][quantity]" class="form-control pull-left">
                            @for($dby=1;$dby<5;$dby++)
                                <option value="{{ $dby }}">{{ $dby }}</option>
                            @endfor
                        </select>
                     </div>
                 </div>
            </div>
            <div class="col-md-2 padding-tb-20">
                <a href="#"> See Sample</a>
            </div>
        </div>
    @endforeach
