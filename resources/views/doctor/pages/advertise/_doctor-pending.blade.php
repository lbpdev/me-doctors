<hr class="margin-tb-10">
<h4 class="no-border doctor-blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
    <span class="font-21">Pending Requests</span>
</h4>

{!! Form::open(['files' => true ,'route'=>'doctor.advertise.update','method'=>'POST','']) !!}

<?php $uploaded = 0; ?>
<?php $uploadable = 0; ?>
<?php $approved = 0; ?>

@foreach($pending_purchases as $index=>$orders)
    @foreach($orders->items as $Iindex=>$item)
    <div class="col-md-12 advert-table">
    <?php $uploadable++; ?>
        <input type="hidden" name="orders[{{$index}}][items][{{$Iindex}}][item_id]" value="{{ $item->id }}">
        <div class="row">
            <div class="col-md-4 padding-l-0 padding-tb-10">

                {{--<input type='hidden' name='product[{{$index}}][type]' value='product' />--}}
                {{--<input type='hidden' name='product[{{$index}}][name]' value='{{ $product->name }}' />--}}
                {{--<input type='hidden' name='product[{{$index}}][price]' value='{{ $product->price }}' />--}}
                {{--<input type='hidden' name='product[{{$index}}][tangible]' value='N' />--}}
                {{--<input type='hidden' name='product[{{$index}}][recurrence]' value='1 Month' />--}}

                {{--<form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>--}}
                 {{--<input type='hidden' name='sid' value='{{ env('2CHECKOUT_SID','901301595') }}' />--}}
                  {{--<input type='hidden' name='med_user_id' value='{{ $authUser->id }}' />--}}
                  {{--<input type='hidden' name='mode' value='2CO' />--}}
                {{--</form>--}}

                <table>
                    <tr>
                        <td class="padding-l-10">
                            <b>{{ $item->product->name }}</b><br>
                            {{ $item->duration }}(s) at ${{ $item->product->price }}/month
                        </td>
                    </tr>
                </table>
            </div>

            @if($item->status==2)
                <?php $approved++ ?>
                <div class="col-md-5 padding-tb-8">
                    <div class="col-md-12 no-padding">
                        Item approved. Waiting for Payment.
                    </div>
                </div>

                <div class="col-md-3 padding-tb-0">
                    {{--<a href='https://sandbox.2checkout.com/checkout/purchase?sid={{ env('2CHECKOUT_SID','901301595') }}&quantity=1&last_name={{ $authUser->lname }}&first_name={{ $authUser->fname }}&email={{ $authUser->email }}&product_id=1&user_id={{ $authUser->id }}'>Upgrade</a>--}}
                    <a href="#">Make Payment</a>
                </div>
            @else
                <div class="col-md-5 padding-tb-8">
                    <div class="col-md-12 no-padding">
                        @if(!$item->uploads)
                            @if($item->premium_service_id>1 && $item->premium_service_id<6)
                                <?php $file_types = '.jpg,.jpeg,.png,.gif'; ?>
                            @elseif($item->premium_service_id==6)
                                <?php $file_types = '.mp4,.avi,.mov'; ?>
                            @elseif($item->premium_service_id>6 && $item->premium_service_id<10)
                                <?php $file_types = '.doc,.docx,.txt,.pdf'; ?>
                            @else
                                <?php $file_types = '.doc,.docx,.txt,.pdf,.jpg,.jpeg,.png,.gif,.mp4,.avi,.mov'; ?>
                            @endif
                            <input type="file"  accept="{{$file_types}}" class="ad-file-select width-full" name="orders[{{$index}}][items][{{$Iindex}}][file]">
                            <p class="font-12">Accepted file types : {{ $file_types }}</p>
                            <br>
                            @if($item->premium_service_id>1 && $item->premium_service_id<6)
                                <p class="font-12">Ad link :</p>
                                <input type="text" placeholder="http://www.linktoadd.com" class="add-link-txt width-full" name="orders[{{$index}}][items][{{$Iindex}}][url]">
                            @endif

                        @elseif($item->status==3)
                            Ad is now in queue
                        @else
                            <?php $uploaded++; ?>
                            File uploaded.
                        @endif
                    </div>
                </div>
                <div class="col-md-3 padding-tb-0">
                    <b>Status:</b> <br>
                    @if($item->status==1)
                        Waiting for approval
                    @elseif($item->status==3)
                        Payment Made
                    @else
                        Waiting for upload
                    @endif
                </div>
            @endif
        </div>
    </div>
    @endforeach
@endforeach


@if($uploaded < $uploadable)
<div class="col-md-12 advert-table">
    <input type='submit' value='Upload Files' class="font-18 pull-right pink button"/><br>
</div>
@endif

{!! Form::close() !!}

{{--@foreach($pending_purchases as $index=>$orders)--}}
    {{--@foreach($orders->items as $Iindex=>$item)--}}
        {{--@if($item->status==2)--}}
             {{--<form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>--}}
              {{--<input type='hidden' name='sid' value='{{ env('2CHECKOUT_SID','901301595') }}' />--}}
              {{--<input type='hidden' name='mode' value='2CO' />--}}
              {{--<input type='hidden' name='med_user_id' value='{{ $authUser->id }}' />--}}
              {{--<input type='hidden' name='li_0_type' value='product' />--}}
              {{--<input type='hidden' name='li_0_product_id' value='{{ $item->product->id }}' />--}}
              {{--<input type='hidden' name='li_0_name' value='{{ $item->product->name }}' />--}}
              {{--<input type='hidden' name='li_0_price' value='{{ $item->product->price }}' />--}}
              {{--<input name='submit' type='submit' value='Checkout'/>--}}
            {{--</form>--}}
        {{--@endif--}}
    {{--@endforeach--}}
{{--@endforeach--}}

<?php $dx = 0; ?>
<?php $payable = 0; ?>
<form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>
<input type='hidden' name='sid' value='{{ env('2CHECKOUT_SID','901301595') }}' />
<input type='hidden' name='mode' value='2CO' />
<input type='hidden' name='med_user_id' value='{{ $authUser->id }}' />
@foreach($pending_purchases as $index=>$orders)
    @foreach($orders->items as $Iindex=>$item)
        @if($item->status==2)
          <input type='hidden' name='li_{{$dx}}_transaction_id' value='{{$item->id}}' />
          <input type='hidden' name='li_{{$dx}}_type' value='product' />
          <input type='hidden' name='li_{{$dx}}_product_id' value='{{ $item->product->id }}' />
          <input type='hidden' name='li_{{$dx}}_name' value='{{ $item->product->name }}' />
          <input type='hidden' name='li_{{$dx}}_price' value='{{ $item->product->price }}' />
          <?php $dx++; ?>
          <?php $payable++; ?>
      @endif
    @endforeach
@endforeach
@if($payable)
  <input name='submit' type='submit' value='Checkout' />
@endif
</form>