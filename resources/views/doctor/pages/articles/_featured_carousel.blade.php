<div class="col-lg-12 col-md-12 col-sm-12 border-top-blue ">
    <div class="row">
        <div class="media-list clearfix" id="latest-articles-car">
            @foreach($articles as $article)
            <div class="media featured clearfix">
                <div class="media-body col-md-4 col-sm-12 col-xs-6 ">
                    <a href="{{ route('doctor.articles.show', $article->slug) }}">
                        <h4 class="media-heading margin-t-0">
                                {{ Str::limit($article->title ,57 , '...') }}
                        </h4>
                    </a>

                    @if(strlen($article->title)<30)
                        <p>{!! Str::limit(strip_tags($article->content), 240, '...') !!}</p>
                    @else
                        <p>{!! Str::limit(strip_tags($article->content), 140, '...') !!}</p>
                    @endif

                    @include('doctor.pages.articles.tags')

                    <div class="owl-controls clickable"></div>
                </div>
                <div class="media-right col-md-8 col-sm-12 col-xs-6 no-padding pull-left">
                    <a href="{{ route('doctor.articles.show', $article->slug) }}">
                        <img class="media-object" src="{{ asset($article->FeaturedImage) }}"  width="100%" alt="">
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>