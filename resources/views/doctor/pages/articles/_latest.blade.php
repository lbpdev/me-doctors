<div class="sidebar-element clearfix">
    <h4>Lastest Articles</h4>
    <div class="col-md-12 padding-10 side-poll">
        <div class="tab-pane fade in active" id="tab1default">
            <ul class="discussions-list clearfix">
            @if ($latest)
                @foreach ($latest as $latestArticle)
                    <li>
                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                            <a href="{{ route('doctor.articles.show', $latestArticle['slug'] )}}">
                                {{ $latestArticle['title'] }}
                            </a>
                        </div>
                    </li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
</div>