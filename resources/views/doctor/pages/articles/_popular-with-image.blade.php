@foreach($articles as $article)
    <div class="col-md-6 col-sm-6 clearfix padding-l-10 article border-right-light">
        <div class="col-md-12 col-sm-12 no-padding col-xs-12">
            <div class="media-left col-md-12 col-sm-12 col-xs-6 col-xxs-12 no-padding">
                <a href="{{ route('doctor.articles.show', $article->slug) }}">
                    @if ($article->thumbnail)
                        <img class="media-object width-full" src="{{ asset($article->thumbnail) }}" alt="{{ $article->title }}" title="{{ $article->title }}">
                    @endif
                </a>
            </div>
            <div class="media-body col-md-12 col-sm-12 col-xs-6 col-xxs-12 padding-b-10">
                <h4 class="media-heading">
                    <a href="{{ route('doctor.articles.show', $article->slug) }}">
                      {{ Str::limit($article->title, 33, '...') }}
                      <!-- {{ substr($article->title , 0 , 19 ) }}{!! strlen($article->title)>19 ? '-<br>' : '' !!}
                      {{ substr($article->title , 19 , 17 ) }}{{ strlen($article->title)>36 ? '...' : '' }} -->
                    </a>
                </h4>

                @if(Str::length($article->title) > 23)
                    {!! Str::limit(strip_tags($article->content), 154, '...') !!}
                @else
                    {!! Str::limit(strip_tags($article->content), 194, '...') !!}
                @endif

                @include('doctor.pages.articles.tags')
            </div>
        </div>
    </div>
@endforeach