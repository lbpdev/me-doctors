@extends('doctor.master')

@section('content')
    <div class="container articles-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right ">
                @include ('doctor.partials._news-ticker')
                <hr class="no-margin">

                <h2 class="hs_heading pull-left width-full margin-b-4">{{ $active_title }} Articles</h2>

                @include('doctor.pages.articles._filter')
                <hr class="margin-t-0 margin-b-7  pull-left">
                @include ('doctor.pages.articles._filtered')

                </div>

                @include('doctor.pages.articles.sidebar')
            </div>
        </div>
    </div>
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/videosCarousel.js') }}"></script>
    <script>
        $('#therapy').on('change',function(e){
            $(this).closest('form').trigger('submit');
        });
    </script>
@stop