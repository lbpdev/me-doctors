<div class="col-md-12 articles">
    <div class="row">
        <h4 class="section-header">Newest </h4>
        @if ($latestArticles)
            <div class="col-md-8 no-padding">
                @include ('doctor.pages.articles._popular-with-image', [
                    'articles' => array_slice($latest->all(), 0, 2)
                ])
            </div>
            @include ('doctor.pages.articles._popular-rss', [
               'articles' => array_slice($latest->all(), 2)
            ])
        @else
            <p>No Popular Articles to Show.</p>
        @endif
    </div>
</div>