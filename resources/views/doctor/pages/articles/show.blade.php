@extends('doctor.master')

@section('metas')

    <!-- Place this data between the <head> tags of your website -->
    <meta name="description" content="{!! Str::limit(strip_tags($article->content), 150) !!}" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $article->title }}">
    <meta itemprop="description" content="{!! Str::limit(strip_tags($article->content), 150) !!}">
    <meta itemprop="image" content="{{ asset( $article->thumbnail) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="www.middleeastdoctor.com">
    <meta name="twitter:title" content="{{ $article->title }}">
    <meta name="twitter:description" content="{!! Str::limit(strip_tags($article->content), 150) !!}">
    <meta name="twitter:creator" content="Middle East Doctor">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset( $article->thumbnail) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $article->title }}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ route('doctor.articles.show', $article->slug )}}" />
    <meta property="og:image" content="{{ asset( $article->thumbnail) }}" />
    <meta property="og:description" content="{!! Str::limit(strip_tags($article->content), 150) !!}" />
    <meta property="og:site_name" content="www.middleeastdoctor.com" />
    <meta property="article:published_time" content="{{ $article->created_at }}" />
    <meta property="article:modified_time" content="{{ $article->updated_at }}" />
    <meta property="article:section" content="Article Section" />
    <meta property="article:tag" content="Article Tag" />
    <meta property="fb:admins" content="100000370643856" />

    <style>
        b, strong {
            line-height: 24px;
        }

        ul, ol {
            line-height: 24px;
            margin-top: 10px;
        }
    </style>
@stop

@section('content')
    <div class="container articles-page page-container single-article">
        <div class="row">
            <div class="col-md-9 col-sm-12 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="no-margin pull-left">
                {{--<h5><a href="{{ route('doctor.articles.index') }}"><< Back to Articles</a></h5>--}}

                <h2 class="hs_heading">{{ $article->title }}</h2>
                <h2 class="hs_subheading">{{ $article->subtitle }}</h2>
                <div class="hs_margin_30"></div>

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="post-content left-pad-20">

                        <div class="content padding-t-0">
                            @if(isset($article->FeaturedImage))
                                <img src="{{ asset( $article->FullImage) }}" width="100%" class="margin-b-10">
                            @endif

                            Published: <em>{{ $article->date_posted->format('F d, Y')  }}</em> <br><br>

                            <div class="fancify">
                            {!! $article->content !!}
                            </div>

                            <div class="col-md-12 margin-t-20">
                                <div class="row">
                                    <span class='st_facebook_large' displayText='Facebook'></span>
                                    <span class='st_twitter_large' displayText='Tweet'></span>
                                    <span class='st_googleplus_large' displayText='Google +'></span>
                                    <span class='st_linkedin_large' displayText='LinkedIn'></span>
                                    <span class='st_email_large' displayText='Email'></span>
                                </div>
                            </div>
                        </div>

                        <div class="voffset2 clearfix f-left"></div>
                        @include ('doctor.partials.articles.latest')

                      </div><!-- End post Content -->
                    </div><!-- /Col -->
                </div><!-- /Row -->
            </div>

            @include ('doctor.pages.articles.sidebar')
        </div>
    </div>
@stop

@section('js')

    <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>

    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "6eecfc05-f227-415e-b39c-b39cfcff45b5", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

    <script>
        $(document).ready(function(){
            var src = '';
            $('.fancify img').each(function(item){
                src = $(this).attr('src');
                $(this).wrap('<a href="'+src+'" class="fancybox"></a>');
            });

            $('.fancybox').fancybox();
        });
    </script>
@stop