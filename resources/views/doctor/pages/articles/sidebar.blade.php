<div class="clearfix col-md-3 col-sm-12 sidebar padding-r-0 ">

    <div class="panel with-nav-tabs panel-default clearfix latest-tabs">
        <div class="col-md-12 no-padding">

            @if ($authUser)
                  @include('doctor.partials._top-articles')
                  @include('doctor.partials._popular-discussions')
            @endif

            <div class="sidebar-element clearfix">
                <div class="col-md-12 no-padding side-ad">
                    @if(!$authUser)
                        <a href="{{ route('doctor.login') }}">
                            <img src="{{ asset('public/images/ads/register-side.png') }}" width="100%" alt="register med">
                        </a>
                    @endif
                </div>
            </div>

            @include('templates.box-event')
            @include('templates.sidebar-long')
        </div> 
    </div>
</div>