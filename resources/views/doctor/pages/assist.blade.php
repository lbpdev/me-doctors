@extends('doctor.master')

@section('content')
<div class="hs_page_title">
  <div class="container">
    <ul>
      <li><a href="{{ URL::to('/') }}">Home</a></li>
      <li>Assist</li>
    </ul>
  </div>
</div>

<div class="container">
  <!--who we are start-->
  <div class="col-md-8">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h2 class="hs_heading"><i class="fa fa-bullhorn"></i> Assist</h2>
      <div class="row hs_how_we_are">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Pose questions about particular patient cases and share information or your own experiences to help others.</h3>
            <br>
            <p>Get a second opinion on a patient with a condition you’ve never encountered before or ask a
            question that’s sending you round in circles. Using our Assist page, you can upload anonymous
            scans or results to ask for clarification from your fellow peers or ask about alternative treatment
            options for a patient where nearly all options are contraindicated.</p>
            <br>
            <p>Feel free to share your concerns or advice <a href="#">here</a>. </p>
        </div>
      </div>
    </div>
  </div>
  </div>

@include('......templates.sidebar-posts')
</div>


@stop
