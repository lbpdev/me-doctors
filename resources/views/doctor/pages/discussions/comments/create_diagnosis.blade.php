 <div class="hs_comment_form clearfix pull-left width-full margin-t-20 border-top green col-md-12">
    <h2 class="page-header-light green no-border margin-t-0">Add your comment</h2>

    {!! Form::open(['route' => ['doctor.discussions.comments.store', $discussion->id], 'files' => true]) !!}

    @include('doctor.pages.discussions.comments._form')
    
        <div class="hs_margin_40 clearfix"></div>
            <div class="form-group">
              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="row">
                {!! Form::submit('Post', [ 'class' => 'green pull-right button font-24 robotomedium padding-r-0' ]) !!}
              </div>
            </div>
        </div>

    {!! Form::close() !!}
</div>