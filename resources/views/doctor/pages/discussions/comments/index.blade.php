
@if(count($comments)>0)
<div id="comments-list">
    @foreach($comments as $index => $comment)
        <div class="pull-left discussion-item padding-tb-10 clearfix col-lg-12 col-md-12 margin-r-0 {{ $index % 2 == 0 ? '' : 'gray-bg' }}">
            <div class="col-lg-12 col-md-12 no-padding">
                <div class="post-content left-pad-20">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3 ">
                        <div class="row text-left avatar">
                            <a href="{{ $comment->author->id == 1 ? '#' : route('doctor.profile', $comment->author->username)}}">
                                <img src="{{ url($comment->author->avatar) }}" width="100%" class="avatar">
                            </a>
                            <a href="{{ $comment->author->id == 1 ? '#' : route('doctor.profile', $comment->author->username)}}" class="profile-link">View Profile</a>
                        </div>
                    </div>

                    <div class="col-lg-11 col-md-11 col-xs-3 ">
                        <a class="" href="{{ $comment->author->id == 1 ? '#' : route('doctor.profile', $comment->author->username)}}">
                            <h4 class="user-username margin-t-0 margin-b-0 black">{{ $comment->author->name }}</h4>
                        </a>
                    </div>

                    <div class="col-lg-11 col-md-11 col-xs-12 row-sm">
                        <div class="comment-contents content margin-tb-5">{!! $comment->content !!}</div>

                        @include('doctor.pages.discussions.comments._attachments', ['post' => $comment])

                        <hr class="border-top border-light  margin-t-4 margin-b-5">
                        <div class="">
                            <div class="row">
                                <div class="col-md-6 text-left font-14">Submitted {{ $comment->created_at->diffForHumans() }}</div>
                                {{--<div class="col-md-6 text-right">Votes</div>--}}
                            </div>
                        </div>
                    </div>
                </div><!-- End post Content -->
            </div><!-- End Col -->
        </div><!-- End Row -->
    @endforeach
</div>
    <button class="loadmore button pull-right margin-t-10">
        <span class="pink font-18 robotomedium pink">Load More Comments</span>
        <img src="{{ asset('public/images/icon/doc_loader_pink.GIF') }}" width="20" class="loader temp-hide">
    </button>
@else
    Be the first to comment.
@endif


@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>

    <script>
    $('.loadmore').on('click', function(e){
            var profile_url = "<?php echo URL::to('/'); ?>";
            var placeholder = "{{ asset('public/images/placeholders/user.png') }}";

            $(this).find('.loader').show();
            $.ajax({
              url: '<?php echo route('doctor.discussions.nextComments') ?>',
              type: "POST",
              data: {
                'id'    : <?php echo $discussion->id ?> ,
                'limit'    : 3 ,
                'offset'  : $('.discussion-item').length - 1
              },
              success: function(data){
                data = JSON.parse(data);
                if(data.length > 0){
                    $(data).each(function(key, item){
                        avatar = placeholder;
                        var template = $($('.discussion-item').last()).clone();

                        if(item.author.profile)
                            if(item.author.profile.avatar != null)
                                avatar = profile_url+'/'+item.profile.avatar.path+'/'+item.profile.avatar.file_name;

                        $(template).find('.avatar').attr('src',avatar);
                        $(template).find('.user-username').text(item.author.fname + ' '+ item.author.lname);
                        $(template).find('.profile-link').attr('href', profile_url + '/doctor/' + item.author.username + '/profile');
                        $(template).find('.comment-contents').text(item.content);

                        $('#comments-list').append(template);
                    });
                } else {
                      $('.loadmore span').text('No more results');
                }
              },
              error: function(error){
                console.log(error);
              },
              complete: function(e){
                $('.loader').hide();
              }

            });
        });
    </script>
@stop

