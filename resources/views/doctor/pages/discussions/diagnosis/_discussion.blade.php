 <li class="discussion clearfix height-auto">
     <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
     {{--@if( $discussion->status == 1 )--}}
        <div class="row green-border-b padding-b-10">
            <h4 class="media-heading border-light clearfix">
               <a href="{{ route('doctor.diagnosis.show', $discussion->slug) }}" class="pull-left green">
                    {{ $discussion->title }}
                </a>
                @if($discussion->therapy)
                    <span class="font-12 margin-l-5 pull-left vertical green">{{ $discussion->therapy ? '( '.$discussion->therapy->name.' )' : '' }}</span>
                @endif
            </h4>
            <!-- <span class="update">Started {{ $discussion->created_at->diffForHumans() }}</span><br> -->
            <span class="comments-count">{{ $discussion->commentsCount }} comments</span>
            <p>{!! Str::words(strip_tags($discussion->content) , 30, '...') !!}</p>
        </div>
     {{--@elseif( $discussion->status != 1 && $authUser->id == $discussion->author->id)--}}
         {{--<div class="row green-border-b padding-b-10 gray-bg">--}}
             {{--<h4 class="media-heading clearfix">--}}
                {{--<span class="pull-left green margin-l-10">--}}
                     {{--{{ $discussion->title }}--}}
                 {{--</span>--}}
             {{--</h4>--}}
            {{--<p class="pull-left margin-l-10">Waiting for moderator approval.</p>--}}
         {{--</div>--}}
     {{--@endif--}}
     </div>
 </li>