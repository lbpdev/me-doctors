 <div class="col-md-12 no-padding clearfix pull-left">
    <div class="col-md-12">
        <div class="row">
            <ul class="discussions-list">
                @if(count($discussions))
                    @foreach ($discussions as $discussion)

                        @include('doctor.pages.discussions.diagnosis._discussion')

                    @endforeach
                @else
                    <div class="col-md-12"> No Discussions for this section </div>
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>