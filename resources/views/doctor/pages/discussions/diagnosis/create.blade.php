@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-4">
                <h3 class="no-border hs_heading margin-b-0 padding-b-0 margin-t-0 green clearfix">
                    <span class="page-icon diagnosis"></span>
                    <span class="page-title">MED Cases</span>
                @if($authUser)
                    <div class="padding-t-5 green pull-right pull-left-xs col-md-6 col-sm-6 col-xxs-12 clearix row-sm padding-r-0 margin-t-0">
                        <a class="pull-right t-green font-15 action-title" href="{{ route('doctor.diagnosis.index') }}">Cancel</a>
                    </div>
                @endif
                </h3>
                <hr class="margin-t-5 margin-b-4">
                    @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif
                    <div class="hs_comment_form clearfix margin-t-7 border-top green clearfix col-md-12">
                        <h2 class="page-header-light green no-border margin-t-0">Create a Case</h2>
                        {!! Form::open(['files' => true , 'route' => 'doctor.diagnosis.store']) !!}
                            @include('doctor.pages.discussions._form')
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="clearfix"></div>
                @include ('doctor.partials.errors')
            </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop
