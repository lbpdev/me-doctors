@extends('doctor.master')

@section('content')
    <div class="hs_page_title">
      <div class="container">
        <div class="col-md-9">
        </div>
        <div class="col-md-3 search">
          <div class="hs_search_box" style="display: none;">
            <form class="form-inline" role="form">
                <div class="form-group has-success has-feedback">
                    <input type="text" class="form-control" id="inputSuccess4" placeholder="Search">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                @include ('doctor.partials._news-ticker')
                <hr>
                <h2 class="no-margin green">
                    <i class="fa fa-stethoscope"></i> MED Cases
                </h2>
                    <h4 class="hs_heading green">Start a new discussion</h4>
                    <div class="hs_margin_30 clearfix"></div>
                    @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif
                    {!! Form::open(['files' => true , 'route' => 'doctor.diagnosis.update' , 'method' => 'PUT']) !!}
                        {!! Form::hidden('discussion_id' , $data->id ) !!}
                        @include(' admin.templates.forms.create_diag')
                    {!! Form::close() !!}
                </div>
                <div class="clearfix"></div>
                @include ('doctor.partials.errors')
            </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop
