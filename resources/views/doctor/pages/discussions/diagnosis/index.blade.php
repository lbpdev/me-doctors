@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-b-4 margin-t-0">
                    <div class="diagnosis pull-left">

                        <h3 class="no-border hs_heading margin-b-0 padding-b-0 margin-t-0 green">
                            <span class="page-icon diagnosis"></span>
                            <span class="page-title">MED Cases</span>
                        @if($authUser)
                            <div class="padding-t-5 pull-right pull-left-xs col-md-6 col-sm-6 col-xxs-12 clearix row-sm padding-r-0 margin-t-0">
                                <a class="font-12 pull-right pull-left-xs t-black" href="{{ route('diagnosis_add') }}">
                                    <span class="action-icon plus"></span>
                                    <span class="action-title">Create MED Case</span>
                                </a>
                            </div>
                        @endif
                        </h3>
                        @include('doctor.pages.discussions.diagnosis._filter')
                        @include('doctor.pages.discussions.diagnosis._discussions')
                        <?php echo $discussions->render(); ?>

                        <div class="voffset2 clearfix f-left"></div>
                    </div>
                    @include ('doctor.partials.discussions.latest')
                </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop

