@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-md-9 col-sm-12 border-right">

                @include ('doctor.partials._news-ticker')
                <div class="col-md-12 padding-b-0 padding-t-0 clearfix margin-b-5 margin-t-0">
                    <div class="row">
                        <hr class="margin-b-4 margin-t-0">
                        <h3 class="no-border green hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-icon diagnosis"></span>
                            <span class="page-title">MED Cases</span>
                        </h3>
                        <a class="pull-right t-green font-15 action-title" href="{{ route('doctor.diagnosis.index') }}">Back</a>
                    </div>
                </div>
                <hr class="no-margin">
                @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif

                <div class="hs_margin_30"></div>

                @include('doctor.pages.discussions.diagnosis.single')

                <h2 class="section-header green border-top">Comments</h2>
                @include('doctor.pages.discussions.comments.index')

                <div class="hs_margin_40 clearfix"></div>

                @include('doctor.pages.discussions.comments.create_diagnosis')

                @include ('doctor.partials.diagnosis.latest')
            </div>

            @include('doctor.pages.discussions.sidebar')
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
@stop