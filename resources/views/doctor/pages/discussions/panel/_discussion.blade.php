 <li class="discussion clearfix height-auto">
     <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
        <div class="row pink-border-b padding-b-10">
            <h4 class="media-heading clearfix border-light">
               <a href="{{ route('doctor.discussions.show', $discussion->slug) }}" class="pull-left">
                    {{ $discussion->title }}
                </a>
                @if($discussion->therapy)
                    <span class="font-12 margin-l-5 pull-left vertical">({{ $discussion->therapy->name }})</span>
                @endif
            </h4>
            <!-- <span class="update">Started {{ $discussion->created_at->diffForHumans() }}</span><br> -->
            <span class="comments-count">{{ $discussion->commentsCount }} comments</span>
            <p>{!! Str::words(strip_tags($discussion->content) , 30, '...') !!}</p>
        </div>
     </div>
 </li>