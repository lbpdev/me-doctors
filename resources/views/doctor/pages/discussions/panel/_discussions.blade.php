 <div class="col-md-12 no-padding clearfix pull-left">
    <div class="col-md-12">
        <div class="row">
            <ul class="discussions-list">
                @if (count($discussions)>0)
                    @foreach ($discussions as $discussion)

                        @include('doctor.pages.discussions.panel._discussion')

                    @endforeach
                @else
                    There are currently no discussions for this section.
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>