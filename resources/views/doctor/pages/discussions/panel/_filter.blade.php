
{{--<div class="pull-right t-black title-action margin-tb-10">--}}
    {{--{!! Request::is('doctor/discussions')  ? 'Latest' : ' <a class="t-orange" href="'.route('doctor.discussions.index').'">Latest</a>' !!} |--}}
    {{--{!! Request::is('doctor/discussions/popular')  ? 'Popular' : ' <a class="t-orange" href="'.route('discussions_popular').'">Popular</a>' !!}--}}
{{--</div>--}}

<div class="pull-left width-full col-md-12 clearfix gray-bg margin-tb-5 disc-filter pull-left width-full">
        {!! Form::open(['route'=>'discussions_filter']) !!}
        <div class="col-md-7 no-padding clearfix input">
            Specialty: <br>
            <div class="custom-select white-bg">
                {!! Form::select('therapy' , $therapies , $active , [  'class' => 'form-control pull-left' ]) !!}
             </div>
        </div>
        <div class="col-md-4 padding-l-10 padding-0-xs padding-r-0 padding-0-sm clearfix input">
            Sort by: <br>
            <div class="custom-select white-bg ">
                {!! Form::select('sort' , $sort_types , $sort , [  'class' => 'form-control pull-left' ]) !!}
             </div>
        </div>
        <div class="col-md-1 padding-l-10 padding-r-0 row-xs clearfix input margin-t-20">
            <button type="submit" class="form-control magnify button search-icon margin-t-5"></button>
        </div>
        {!! Form::close() !!}
</div>