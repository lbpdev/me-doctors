@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-b-4  margin-t-0">
                    <div class="discussions">
                        <h3 class="no-border pink hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-icon discussion"></span>
                            <span class="page-title">Discussions</span>
                        </h3>
                        @include('doctor.pages.discussions.panel._filter')
                        @include('doctor.pages.discussions.panel._discussions')
                        <?php echo $discussions->render(); ?>

                        <div class="voffset2 clearfix f-left"></div>
                    </div>
                    @include ('doctor.partials.diagnosis.latest')
                </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop