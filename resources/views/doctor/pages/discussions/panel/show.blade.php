@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-md-9 col-sm-12 border-right">

                @include ('doctor.partials._news-ticker')
                <div class="col-md-12 padding-b-0 padding-t-0 clearfix margin-b-5 margin-t-0">
                    <div class="row">
                        <hr class="margin-b-4 margin-t-0">
                        <h3 class="no-border pink hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-icon discussion"></span>
                            <span class="page-title">MED Talk</span>
                        </h3>
                        <a class="pull-right t-pink font-15 action-title" href="{{ route('doctor.discussions.index') }}">Back</a>
                    </div>
                </div>
                <hr class="no-margin">
                @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif

                @include('doctor.pages.discussions.panel.single')
                <h2 class="section-header pink">Comments</h2>
                @include('doctor.pages.discussions.comments.index')
                
                @include('doctor.pages.discussions.comments.create_panel')

                @include ('doctor.partials.discussions.latest')
            </div>

            @include('doctor.pages.discussions.sidebar')
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
@stop