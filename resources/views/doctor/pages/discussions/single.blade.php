<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="post-content left-pad-20">
            <h3 class="padding-b-5 t-orange">{{ $discussion->title }}</h3>
            <div class="orange-border-b padding-b-5">
                <span class="comments small pull-right"><i class="fa fa-comments-o"></i> 
                    {{ $discussion->comments->count() }} 
                </span>
                <span class="update">Submitted {{ $discussion->created_at->diffForHumans() }}</span>
            </div>

            <div class="col-lg-2 col-md-2 col-xs-12 padding-t-10">
                <div class="row avatar">
                    <a href="{{ route('doctor.profile', $discussion->author->username)}}">
                        <img src="{{ url($discussion->author->avatar) }}" width="100%">
                    </a>
                </div>
            </div>

            <div class="col-lg-10 col-md-10 col-xs-12 row-sm">
                <a href="{{ route('doctor.profile', $discussion->author->username) }}">
                    <h4 class="margin-t-10 margin-b-0">{{ $discussion->author->name }}</h4>
                </a>
                
                <div class="content margin-tb-5">{!! $discussion->content !!}</div>

                @include('doctor.pages.discussions.comments._attachments', ['post' => $discussion])

                <div class="gray-border-t padding-t-10">
                    <div class="row">
                        <div class="col-md-6 text-left">{{ $discussion->created_at->diffForHumans() }}</div>
                        {{--<div class="col-md-6 text-right">Votes</div>--}}
                    </div>
                </div>
            </div>
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->