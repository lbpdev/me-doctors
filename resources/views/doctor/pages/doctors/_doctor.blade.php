<div class="doctor-item {{ $doctor->isPremium == 1 ? 'gray-bg' : 'non-featured'}} pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 col-xs-12 margin-r-0 border-bottom border-light padding-lb-10-xs">
{{--<div class="doctor-item pull-left item padding-tb-10 clearfix col-lg-12 col-md-12  col-xs-12  col-sm-12 col-xxs-12 margin-r-0 border-bottom border-light padding-r-0">--}}
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">
            @if($doctor->isPremium == 1)
                <span class="featured-ribbon pink"></span>
            @endif

            <div class="{{ $doctor->isPremium == 1 ? 'col-lg-2 col-md-2 ' : 'col-lg-2 col-md-2 '}} col-xs-3 padding-b-10-xs ">
            {{--<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 col-xxs-4 ">--}}
                <div class="row text-left avatar">
                    <a href="{{ route('doctor.profile', $doctor->username)}}">
                        <img src="{{ url($doctor->avatar) }}" width="{{ $doctor->isPremium == 1 ? '100%' : '100'}}" class="avatar">
                        {{--<img src="{{ url($doctor->avatar) }}" width="100%" class="avatar">--}}
                    </a>
                </div>
            </div>

            <div class="{{ $doctor->isPremium == 1 ? 'col-lg-7 col-md-7 ' : 'col-lg-10 col-md-10'}} col-xs-9 row-sm padding-r-0 padding-lb-10-xs">
            {{--<div class="col-lg-10 col-md-10 col-sm-10 padding-l-10 col-xs-9 col-xxs-8 padding-r-0">--}}
                <a class="doctor-username" href="{{ route('doctor.profile', $doctor->username)}}">
                    <h4 class="margin-t-0 padding-t-0 margin-b-0 black doctor-name">{{ $doctor->name }}</h4>
                </a>
                <div class="detail-line">
                    <i class="fa fa-stethoscope"></i>
                    <span class="doctor-specialties">
                        @if(count($doctor->specialties)>0)
                            @foreach($doctor->specialties as $specialty)
                                {{ $specialty->name }}
                                <?php break; ?>
                            @endforeach
                        @else
                            Not Specified
                        @endif
                        &nbsp;
                    </span>
                    <br class="xxs-show temp-hide">
                    <span class="small-icon hospital"></span> <span class="doctor-hospital">
                    {{ $doctor->primaryHospital ? $doctor->primaryHospital : 'N/A' }}
                    </span>
                </div>

                <div class="detail-line">
                    <span class="small-icon phone"></span> <span class="doctor-contact">
                        {{ $doctor->workPhone ? $doctor->workPhone->number  : 'N/A' }}
                    </span> &nbsp;
                    <br class="xxs-show temp-hide">
                    <span class="small-icon location"></span>
                    <span class="doctor-work">
                        {{ $doctor->primaryWorkLocation ? $doctor->primaryWorkLocation : 'N/A' }}
                    </span>
                </div>
                <hr class="margin-tb-5 xxs-hide">
                <div class="margin-t-5 xxs-hide">
                    <span class="doctor-ratings">
                        <i class="rate-icon ri-friendliness powerTip" title="Friendliness"></i> <span class="friendliness">{{ $doctor->friendlinessRating }}</span>
                        <i class="rate-icon ri-professionalism margin-l-10 powerTip" title="Professionalism"></i> <span class="professionalism">{{ $doctor->professionalismRating }}</span>
                        <i class="rate-icon ri-bed-manner margin-l-10 powerTip" title="Bed-side Manners"></i> <span class="bedside">{{ $doctor->bedsideMannerRating }}</span>
                        <i class="rate-icon ri-medical-knowledge powerTip" title="Medical Knowledge"></i> <span class="medical">{{ $doctor->medicalKnowledgeRating }}</span>
                        <i class="rate-icon ri-helpfulness margin-l-10 powerTip" title="Helpfulness"></i> <span class="helpfulness">{{ $doctor->helpfulnessRating }}</span>
                        <i class="rate-icon ri-waiting-time powerTip" title="Response Time"></i> <span class="waiting">{{ $doctor->waitingTimeRating }}</span>
                    </span>
                </div>
                <hr class="margin-tb-5 xxs-hide">
                <div class="margin-tb-5 xxs-hide">
                    <span class="doctor-details">
                        @if($doctor->profile)
                            {{ $doctor->profile->bio ? Str::words(strip_tags($doctor->profile->bio), 24) : 'No description'}}
                        @else
                            No description
                        @endif
                    </span>
                </div>
            </div>
            @if($doctor->isPremium == 1)
                <div class="col-lg-3 col-md-3 col-xs-12 row-sm padding-r-0">
                    <div class="map no-margin" style="height: 140px;" data-location="{{ isset($doctor->PrimaryWorkLocation) ? $doctor->PrimaryWorkLocation : 'N/A' }}"></div>
                </div>
            @endif
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->