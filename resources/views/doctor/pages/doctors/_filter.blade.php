
{{--<div class="pull-right t-black title-action margin-tb-10">--}}
    {{--{!! Request::is('doctor/discussions')  ? 'Latest' : ' <a class="t-orange" href="'.route('doctor.discussions.index').'">Latest</a>' !!} |--}}
    {{--{!! Request::is('doctor/discussions/popular')  ? 'Popular' : ' <a class="t-orange" href="'.route('discussions_popular').'">Popular</a>' !!}--}}
{{--</div>--}}

<div class="pull-left width-full col-md-12 padding-t-5 padding-b-3 clearfix gray-bg margin-b-0 doctor-filter">
        {!! Form::open(['route'=>'doctor.doctors.filter','method'=>'GET','id' => 'sortForm']) !!}
        <div class="col-md-2 no-padding clearfix input">
            <h3 class="filter-label pink">Search by:</h3>
        </div>
        <div class="col-md-3 no-padding clearfix margin-b-sm-5 input">
            <div class="custom-select white-bg">
                {!! Form::select('therapy' , $therapies , isset($_GET['therapy']) ? $_GET['therapy'] : 'null' , [  'class' => 'form-control pull-left' ]) !!}
            </div>
        </div>
        <div class="col-md-3  padding-l-10 padding-r-0 padding-l-sm-0 margin-b-sm-5 row-xs clearfix input">
            <div class="custom-select white-bg ">
                <select id="country" name="country" class="form-control pull-left" onchange="getCities()">
                    <option value="all">All Countries</option>
                    @foreach($countries as $index=>$country)
                        <option {{ ( isset($_GET['country']) && $_GET['country'] == $country->name ) ? 'selected' : '' }} value="{{ $country->name }}">{{ $country->name }}</option>
                    @endforeach
                </select>
             </div>
        </div>
        <div class="col-md-3  padding-l-10 padding-l-sm-0 padding-r-0 margin-b-sm-5 row-xs clearfix input">
            <div class="custom-select white-bg" id="cities-{{$country->code}}">
                <select id="city" name="city" class="form-control pull-left">
                    @if(isset($_GET['city']))
                        <option value="all">All Cities</option>

                        @if(isset($active_cities))
                            @foreach($active_cities as $city)
                                <option {{ ( $_GET['city'] == $city ) ? 'selected' : '' }} value="{{ $city }}">{{ $city }}</option>
                            @endforeach
                        @endif
                    @else
                        <option value="all">Select Region</option>
                    @endif
                </select>
             </div>
            {{--@foreach($countries as $country)--}}
                {{--<div class="custom-select white-bg temp-hide" data-country="{{$country->name}}">--}}
                    {{--<select name="city" class="form-control pull-left">--}}
                        {{--<option value="all">All Cities</option>--}}
                        {{--@foreach($country->cities as $city)--}}
                            {{--<option value="{{ $city->name }}">{{ $city->name }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                 {{--</div>--}}
            {{--@endforeach--}}
        </div>
        <div class="col-md-1 padding-l-10 padding-r-0 row-xs clearfix input">
            <button type="submit" class="magnify button search-icon top-sm-12"></button>
        </div>
</div>


<div class="col-md-12 border-bottom-very-light">
    <div class="row">
        <div class="custom-select left">
            <select id="sort" name="sort">
            @if(isset($_GET['sort']))
                <option  {{ $_GET['sort'] == 'latest' ? 'selected' : '' }} value="latest">Latest</option>
                <option  {{ $_GET['sort'] == 'views' ? 'selected' : '' }} value="views">Most Viewed</option>
            @else
                <option selected value="latest">Latest</option>
                <option value="views">Most Viewed</option>
            @endif
            </select>
        </div>
    </div>
</div>
{!! Form::close() !!}

@section('js-inner')
    <script>
    <?php
        echo "var cities = ". json_encode($cities) . ";\n";
    ?>;
        function getCities(e){
            var country = $('select[name=country]').val();
            var cts = cities[country];
            $('select[name=city]').empty();
            $('select[name=city]').append('<option value="all">All Cities</option>');

            if(country!='all'){
                cts.forEach(function(e,data){
                    $('select[name=city]').append('<option value="'+e+'">'+e+'</option>')
                });
            }
        }

        $('#sort').on('change',function(e){
            $('#sortForm').trigger('submit');
        });

//        getCities();
    </script>
@stop