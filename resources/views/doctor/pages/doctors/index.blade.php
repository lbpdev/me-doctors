@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-b-3 margin-t-0">
                    <div class="discussions">
                        <div class="discussions">
                            <h3 class="no-border pink hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
                                <span class="page-icon doctor"></span>
                                <span class="page-title">Doctor Directory</span>
                            </h3>
                            <hr class="margin-tb-5">
                            @include('doctor.pages.doctors._filter')
                            @include('doctor.pages.doctors._doctors')
                            <?php // echo $discussions->render(); ?>

                            @if(count($doctors)>0)
                                <button class="next button pull-right margin-t-10">
                                    <span class="pink font-18 robotomedium pink">
                                    {{ count($doctors) == $total_doctors ? 'No more results' : 'Load More'  }}
                                    </span>
                                    <img src="{{ asset('public/images/icon/doc_loader_pink.GIF') }}" width="20" class="loader temp-hide">
                                </button>
                            @endif
                            <div class="voffset2 clearfix f-left"></div>
                        </div>
                    {{--@include ('doctor.partials.diagnosis.latest')--}}
                    </div>
                </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script>

    var featuredLoaded = '<?php echo $featuredCount ?>';
    var totalFeatured = '<?php echo $totalFeatured ?>';
    var allFeaturedLoaded = false;

    function initMap() {
        $('.map').each(function(e , item){
            setTimeout(function() {
            if($(item).attr('rel')!="temp"){
                var map = new google.maps.Map(item, {
                    zoom: 12,
                    disableDefaultUI: true,
                    // draggable: false,zoomControl: false,scrollwheel: false,disableDoubleClickZoom: true
                });
                var geocoder = new google.maps.Geocoder();

                geocodeAddress(geocoder, map , item);
            }
            },1000);

        })
    }

    function initMapSingle(mapItem) {
        var map = new google.maps.Map(mapItem, {
            zoom: 12,
            disableDefaultUI: true,
            draggable: false,
            zoomControl: false,
            scrollwheel: false,
            disableDoubleClickZoom: true
        });
        var geocoder = new google.maps.Geocoder();

        geocodeAddress(geocoder, map , mapItem);

        setTimeout(function(){
            google.maps.event.trigger(map, "resize");
        },300);
    }

      function geocodeAddress(geocoder, resultsMap ,item) {
        var address = $(item).attr('data-location');

        if(address=="")
            address = "United Arab Emirates";
        if(address=="Not Specified")
            address = "United Arab Emirates";

        geocoder.geocode({'address': address}, function(results, status) {

          if (status === google.maps.GeocoderStatus.OK) {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            address = "Dubai - United Arab Emirates";
            geocoder.geocode({'address': address}, function(resultx, status) {
                if(resultx){
                    resultsMap.setCenter(resultx[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                }
            });
          }
        });
      }

    initMap();

    $('.next').on('click', function(e){

        var profile_url = "<?php echo URL::to('/'); ?>";
        var specialties,bio,avatar,location;
        var placeholder = "{{ asset('public/images/placeholders/user.png') }}";

        console.log(totalFeatured+'=='+featuredLoaded);
        if(totalFeatured==featuredLoaded)
            var allFeaturedLoaded = true;

        $(this).find('.loader').show();

        console.log(allFeaturedLoaded);

        $.ajax({
          url: '<?php echo route('doctor.doctors.next') ?>',
          type: "POST",
          data: {
            'take'    : 10 ,
            'offset'  : $('.doctor-item.non-featured').length,
            'sort'    : $('#sort').val() ,
            'therapy' : '<?php echo isset($_GET['therapy']) ? $_GET['therapy'] : 'all'; ?>' ,
            'country' : '<?php echo isset($_GET['country']) ? $_GET['country'] : 'all'; ?>' ,
            'city'    : '<?php echo isset($_GET['city']) ? $_GET['city'] : 'all'; ?>',
            'featuredCount': featuredLoaded,
            'allFeaturedLoaded': allFeaturedLoaded
          },
          success: function(data){
            data = JSON.parse(data);
            location = 'N/A';
            specialties = 'N/A';
            bio = 'N/A';
            hospital = 'N/A';

            if(data.length > 0){
                $(data).each(function(key, item){
                    bio = 'N/A';

                    if(item.services){
                        featuredLoaded++;
                        var template = $('#feat-template').clone().show();
                        $(template).find('.map').attr('rel','');
                    } else {
                        var template = $('#non-feat-template').clone().show();
                        $(template).addClass('non-featured');
                    }

                    avatar = placeholder;

                    if(item.profile)
                        if(item.profile.avatar)
                            avatar = profile_url+'/public/'+item.profile.avatar.path+'/'+item.profile.avatar.file_name;

                    console.log(item);

                        $(template).find('.avatar').attr('src',avatar);
                        $(template).find('.doctor-name').text(item.fname + ' '+ item.lname);
                        $(template).find('.doctor-username').attr('href', profile_url + '/doctor/' + item.username + '/profile');

                    if(item.profile){
                        if(item.profile.bio != null){
                            bio = JSON.stringify(item.profile.bio).substring(0,200);

                            bio = bio.replace(/<(?:.|\n)*?>/gm, '');
                            bio = bio.replace(/(\r\n|\n|\r)/gm,"");
                            bio = bio.length > 7 ? bio.substring(0,160)+'...' : '';
                            $(template).find('.doctor-details').text(bio);
                        }
                    }   

                    if(item.hospitals){
                        if(item.hospitals[0]){
                            hospital = item.hospitals[0].name;
                            if(item.hospitals[0].location)
                                location = ( item.hospitals[0].location.city ? item.hospitals[0].location.city  + ', ' : '' ) + item.hospitals[0].location.country;
                            else
                                location = 'Not Specified';
                        }
                    }

                    $(template).find('.doctor-hospital').text(hospital);
                    $(template).find('.doctor-work').text(location);

                    if(item.services && item.hospitals){
                        if(item.hospitals[0].location){
                            mapItem = $(template).find('.map');
                            mapItem.attr('data-location',item.hospitals[0].location.name);
                            initMapSingle(mapItem[0]);
                        }
                    }

                    if((item.contacts).length > 0)
                        $(template).find('.doctor-contact').text( item.contacts.length < 2 ? item.contacts[0].number : item.contacts[1].number);
                    else
                        $(template).find('.doctor-contact').text('N/A');

                    if(item.specialties)
                        if(item.specialties[0])
                            specialties = item.specialties[0].name;

                    $(template).find('.doctor-specialties').text(specialties);

                    $(template).find('.doctor-ratings .friendliness').text(item.ratings['friendliness']);
                    $(template).find('.doctor-ratings .professionalism').text(item.ratings['professionalism']);
                    $(template).find('.doctor-ratings .bedside').text(item.ratings['bedside_manner']);
                    $(template).find('.doctor-ratings .medical').text(item.ratings['medical_knowledge']);
                    $(template).find('.doctor-ratings .helpfulness').text(item.ratings['helpfulness']);
                    $(template).find('.doctor-ratings .waiting').text(item.ratings['waiting_time']);

                    $('.directory-list').append(template);
                });
            } else {
                  $('.next span').text('No more results');
            }

          },
          error: function(error){
            console.log(error);
          },
          complete: function(e){
            $('.loader').hide();
            console.log($('.doctor-item.non-featured').length);
          }

        });
    });

    </script>
@stop
