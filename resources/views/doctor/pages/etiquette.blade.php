@extends('doctor.master')

@section('content')
<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 row-sm border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-10">

                @include('doctor.templates.about-menu')
                <div class="col-md-9 clearfix row-sm padding-r-0 page about">

                      <h3 class="no-border yellow hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix doctor-blue">Etiquette</h3>

                      <h4 class="doctor-blue">Doctors</h4>

                      <p>Thank you for your being a part of Middle East Doctor.</p>

                      <p>We know that being a doctor is tough, and Middle East Doctor aims to create a
                      place where you can feel supported. You may, however, encounter healthcare professionals
                      (HCPs) on this site that have different opinions on treatments and how they deal with patients.</p>

                      <p>Please always conduct yourself professionally in such situations, and when responding, ask
                       yourself, “Is this is the way I would speak to someone in ‘real life’?”. If your judgement is poor,
                       our team are here to moderate your comments. Repeat offenders will, however, have their accounts
                        suspended.</p>

                      <p>Any abuse or threatening remarks will not be tolerated; profanity, name-calling and racial,
                       religious and ethic attacks are strictly prohibited.</p>

                      <p>Please remain courteous and professional at all times.</p>

                      <p>When posting in Diagnosis, uploading images may be very helpful. Please ensure that you
                      don’t reveal the patient’s identity and that images are fully compliant.</p>


                      <h4 class="doctor-blue">Patients</h4>

                      <p>When participating in patient discussions, please always conduct yourself with the utmost
                      esteem. People posting on the discussions may have different views. Respect their opinions, and
                      provide your views or opinions in a courteous manner to facilitate a meaningful conversation.</p>

                      <p>If your judgement is poor, our team are here to moderate your comments. Repeat offenders will,
                      however, have their accounts suspended.</p>

                      <p>Any abuse or threatening remarks will not be tolerated; profanity, name-calling and racial,
                      religious and ethic attacks are strictly prohibited.</p>

                      <p>Do not promote anything on this site: Middle East Doctor is not a platform to promote products,
                      services or websites. Violators will be suspended from using the site.</p>
                </div>
        </div>
        @include('doctor.templates.sidebar-ads')

        </div>
    </div>
</div>
@stop
