@if(count($events)>0)
    @foreach ($events as $event)
        <li class="media clearfix height-auto gray-bg">
           <div class="media-left col-md-4 col-sm-4 col-xs-12 no-padding">
                <a href="{{ route('show_event', $event->slug) }}">
                    @if($event->thumbnail)
                        <img class="media-object" src="{{ asset($event->thumbnail) }}" alt="" width="100%">
                    @endif
                </a>
            </div>
            <div class="media-body height-auto col-md-8 col-sm-8 col-xs-12 padding-tb-7">
                <h4 class="media-heading blue">
                   <a href="{{ route('show_event', $event->slug) }}">
                     {{ Str::words($event->title, 9, '...') }}
                    </a>
                </h4>

                <p>
                 <b>When: </b>
                 {{ $event->event_date->format('M d, Y')  }}
                 {{ $event->event_date_end && $event->event_date_end != $event->event_date ? ' - '.$event->event_date_end->format('M d, Y') : '' }}
                </p>
                <p><b>Where: </b>{{ $event->city }} {{ $event->city ? ', '.$event->country : $event->country }}</p>

                @if(Str::length($event->title) > 70)
                    {{ Str::words(strip_tags($event->content), 15, '...') }}
                @else
                    {{ Str::words(strip_tags($event->content), 30, '...') }}
                @endif
           </div>
        </li>
    @endforeach
@else
    No Articles for this section
@endif