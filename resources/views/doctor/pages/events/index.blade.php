@extends('doctor.master')

@section('content')
    <div class="container articles-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right ">
                @include ('doctor.partials._news-ticker')
                <hr class="no-margin">

                <h2 class="hs_heading pull-left width-full margin-b-4">
                     <a href="{{ isset($past) ? route('doctor.events.past') : route('doctor.events.index') }}">
                        {{  isset($past) ? 'Past Events' : 'Upcoming Events' }}
                    </a>

                     <a class="font-12 pull-right pull-left-xs t-black" href="{{ isset($past) ? route('doctor.events.index') : route('doctor.events.past') }}">
                         <span class="action-title">{{  isset($past) ? 'View upcoming events' : 'View past events' }}</span>
                     </a>
                </h2>

                <div class="col-md-12 no-padding clearfix">
                    <ul class="media-list">
                        @include('doctor.pages.events._event')
                    </ul>


                    <?php echo $events->render(); ?>
                </div>

                </div>

            @include('doctor.templates.sidebar-ads')
            </div>
        </div>
    </div>
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/videosCarousel.js') }}"></script>
    <script>
        $('#therapy').on('change',function(e){
            $(this).closest('form').trigger('submit');
        });
    </script>
@stop