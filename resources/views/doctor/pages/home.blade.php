@extends('doctor.master')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/js/style/addtohomescreen.css') }}">
@stop

@section('content')
    <section class="container">
        <div class="clearfix row">
            <div class="col-md-9 col-sm-8 border-right margin-t-5">

                @include ('doctor.partials._news-ticker')

                @include ('doctor.pages.home.articles.latest')

                @include ('doctor.pages.home.surveys.latest')

                @include ('doctor.pages.home.discussions.top')

                @include ('doctor.templates.videos.videos')

                @include ('doctor.pages.articles.popular')

                @include ('doctor.pages.home.events.upcoming')

            </div>

            @include ('doctor.pages.home.sidebar')
        </div>
    </section>
@stop

@section('js')
    {{--<script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>--}}
{{--    <script src="{{ asset('public/js/custom/eventsCarousel.js') }}"></script>--}}
{{--    <script src="{{ asset('public/js/custom/videosCarousel.js') }}"></script>--}}
{{--    <script src="{{ asset('public/js/custom/articlesCarousel.js') }}"></script>--}}
{{--    <script src="{{ asset('public/js/jquery.fancybox.js') }}"></script>--}}
{{--    <script src="{{ asset('public/js/addtohomescreen.min.js') }}"></script>--}}
    <script>
        [
            '{{ asset('public/js/home-plugins.js') }}',
        ].forEach(function(src) {
            var script = document.createElement('script');
            script.src = src;
            script.async = false;
            document.head.appendChild(script);
        });
    </script>
@stop