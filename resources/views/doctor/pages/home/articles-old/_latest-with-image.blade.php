@if ($articles)
    <div class="col-md-8 border-right-light padding-r-10 padding-l-0 margin-t-20">
        <ul class="media-list">
            @foreach ($articles as $article)
                <li class="media disc clearfix">
                    <div class="media-body col-md-5 col-sm-6 col-xs-6 padding-tb-20">
                        <h4 class="media-heading">
                            <a href="{{ route('doctor.articles.show', $article->slug) }}">
                                {{ Str::words($article->title , 10 , '...') }}
                            </a>
                        </h4>

                        <p>
                        @if(Str::length($article->title)>70)
                            {!! Str::words(strip_tags($article->content) , 8, '...') !!}
                        @else
                            {!! Str::words(strip_tags($article->content) , 16, '...') !!}
                        @endif
                        </p>

                        @include('doctor.pages.articles.tags')
                    </div>
                    <div class="media-right col-md-7 col-sm-6 col-xs-6 no-padding">
                        <a href="{{ route('doctor.articles.show', $article->slug) }}">
                            @if ($article->thumbnail)
                                <img class="media-object" src="{{ asset($article->thumbnail) }}"  width="100%" alt="">
                            @endif
                        </a>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endif