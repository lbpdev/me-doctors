@if ($latestSurveys)
    <div class="col-md-6 border-right-light padding-r-7 padding-l-0  margin-t-7 pull-left">
        <h4 class="section-header margin-t-0">SURVEYS</h4>
        <ul class="media-list surveys">
            @foreach ($latestSurveys as $survey)
            <li class="media clearfix height-auto">
                <div class="media-body col-md-12 col-sm-12 col-xs-12 padding-tb-20">
                    <h4 class="media-heading">
                       <a href="{{ route('doctor.surveys.show', $survey->slug) }}">
                         {{ Str::limit($survey->title ,24 , '...') }}
                        </a>
                    </h4>
                    <p>
                        {!! Str::limit(strip_tags($survey->description) , 115, '...') !!}
                    </p>
                    @include('doctor.pages.articles.tags', ['article' => $survey])
                </div>
            </li>
            @endforeach
        </ul>
    </div>
@endif
