<div class="col-md-12 discussions-rss width-full pull-left clearfix">
    <div class="row">
        @if ($latestArticles->count())
            @if($featuredArticle)
                @include ('doctor.pages.articles._featured_carousel', ['articles' => array_slice($latestArticles->all(), 0, 3)])
                @include ('doctor.pages.home.articles._surveys')
                @include ('doctor.pages.home.articles._latest-rss', ['articles' => array_slice($latestArticles->all(), 2)])
            @else
                @include ('doctor.pages.articles._featured', ['article' => $latestArticles->first()])
                @include ('doctor.pages.home.articles._latest-with-image', ['articles' => array_slice($latestArticles->all(), 1, 2)])
                @include ('doctor.pages.home.articles._latest-rss', ['articles' => array_slice($latestArticles->all(), 3)])
            @endif

        @else
            <p>No articles to show.</p>
        @endif
    </div>
</div>