<div class="up_coming_events_slider_item padding-r-0">
    <div class="hs_event_div">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="">
                    <a href="{{ route('show_event', $event->slug) }}">
                    <p class="event-date">{{ $event->event_date->format('d.m.y')  }}</p>
                    <p class="event-title">{{ Str::limit($event->title ,45 , '...') }}</p></a>
                </div>
            </div>
        </div>
    </div>
</div>