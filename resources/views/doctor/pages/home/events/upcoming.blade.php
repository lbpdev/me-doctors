<div class="col-md-12 upcoming-events pull-left width-full">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <h4 class="section-header">
                    EVENTS & CME
                    <a href="{{ route('doctor.events.index') }}">
                        <span class="action-title pull-right font-14 " style="line-height: 25px;">
                        <i class="fa fa-calendar"></i>
                        View All
                        </span>
                    </a>
                </h4>
                @if ($events->count())
                    <div class="up_coming_events ocarousel">
                        <div id="up_coming_events_slider" class="owl-carousel owl-theme">
                            @foreach ($events as $event)
                                @include ('doctor.pages.home.events._event')
                            @endforeach
                        </div>
                        {{--<div class="customNavigation text-right">--}}
                            {{--<a class="btn_prev prev"><i class="fa fa-chevron-left"></i></a>--}}
                            {{--<a class="btn_next next"><i class="fa fa-chevron-right"></i></a>--}}
                        {{--</div>--}}

                        <div class="customNavigation events text-right">
                            <a class="btn_prev white prev"><span class="arrow blue left"></span></a>
                            <a class="btn_next white next"><span class="arrow blue right"></span></a>
                        </div>
                    </div>
                @else
                    <p>No Upcoming Events</p>
                @endif
            </div>
        </div>
    </div>
</div>