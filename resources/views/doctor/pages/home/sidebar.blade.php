<div class="clearfix pull-left col-md-3 col-sm-4 sidebar margin-t-5 padding-r-0">
    @include('doctor.partials.news-feeds')

    @if (count($poll)>0)
        <div class="sidebar-element clearfix">
            <h4 class="poll-head blue-bg t-white padding-tb-7">Today's Poll</h4>
            <div class="col-md-12 side-poll">
                {!! Form::open(['route'=>'poll_vote']) !!}
                    @include('doctor.partials._poll')
                {!! Form::close() !!}
            </div>
        </div>
    @endif

    <div class="sidebar-element clearfix">
        @if(!$authUser)
        <a href="{{ route('doctor.login') }}">
            <img src="{{ asset('public/images/ads/register-side.png') }}" class="margin-b-7 width-full" alt="register med" title="register med">
        </a>
        @endif

        {{--<a href="{{ URL::to('doctor/contact') }}">--}}
            {{--<img src="{{ asset('images/ads/sponsor-survey.png') }}" width="100%" alt="register med">--}}
        {{--</a>--}}

        {{--<a class="margin-t-10 pull-left" target="_blank" href="https://itunes.apple.com/us/app/bodylite/id1031481153?ls=1&mt=8">--}}
            {{--<img src="{{ asset('images/ads/BodyLite-Ad.gif') }}" width="100%" alt="bodylite-ad">--}}
        {{--</a>--}}

    </div>

    @include('templates.box-event')
    @include('templates.sidebar-long')
</div>