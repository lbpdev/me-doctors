<div id="feat-template"  class="temp-hide doctor-item pull-left item padding-tb-10 gray-bg clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 margin-r-0 border-bottom border-light padding-r-0">
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">

            <span class="featured-ribbon yellow"></span>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            {{--<div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 col-xxs-12">--}}
                <div class="row text-left avatar">
                    <a class="hospital-slug" href="">
                        <img src="" width="100%" class="avatar">
                    </a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-xs-12 row-sm padding-r-0">
            {{--<div class="col-lg-9 col-md-9 col-sm-8 col-xs-9 col-xxs-12 padding-r-0">--}}
                <a class="hospital-slug"  href="">
                    <h4 class=" text-uppercase margin-t-0 padding-t-0 margin-b-0 black hospital-name"></h4>
                </a>

                <div class="detail-line">
                        <span><span class="small-icon yellow phone"></span>
                        <span class=" hospital-contact"></span> </span> &nbsp;

                        <span class="small-icon yellow location"></span>
                        <span class=" hospital-location"></span>
                </div>
                <hr class="margin-tb-5">
                <div class="margin-tb-5 hospital-details">

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 row-sm padding-r-0 col-sm-hide">
                <div class="map no-margin" style="height: 140px;" data-location=""></div>
            </div>
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->