<div class="hs_single_profile_detail">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::input('text','title' , isset($data->title) ? $data->title : '' , [ 'required' , 'class' => 'form-control' ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('therapy_id', 'Category:') !!}
            {!! Form::select('therapy_id', $therapies, isset($data->therapy_id) ? $data->therapy_id : '1' , [ 'class' => 'form-control' ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('content', 'Content:') !!}
            {!! Form::textarea('content', isset($data->content) ? $data->content : null , [ 'required', 'class' => 'form-control textarea' ]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('attachment', 'Attachment: (Optional)') !!}
            {!! Form::input('file', 'attachment[]' , null  , [ 'multiple' => true, 'class' => 'form-control' ]) !!}
        </div>
    </div>
</div>
<div class="voffset4 clearfix"></div>
{!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}