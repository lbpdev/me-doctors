<div class="doctor-item {{ $hospital->isPremium == 1 ? 'gray-bg' : 'non-featured' }} pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xxs-12 margin-r-0 border-bottom border-light padding-r-0">
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">
            @if($hospital->isPremium == 1)
                <span class="featured-ribbon yellow"></span>
            @endif

            <div class="{{ $hospital->isPremium == 1 ? 'col-lg-3 col-md-3 col-sm-12 col-xs-12' : 'col-lg-2 col-md-2 col-sm-12 col-xs-12'}} ">

                <div class="row text-left avatar">
                    <a class="hospital-slug" href="{{ route('doctor.hospitals.show' , $hospital->slug) }}">
                        <img src="{{ url($hospital->thumbnail) }}" width="100%" class="avatar">
                    </a>
                </div>
            </div>

            <div class="{{ $hospital->isPremium == 1 ? 'col-lg-6 col-md-6 ' : 'col-lg-10 col-md-10'}} col-xs-12 row-sm padding-r-0">

                <a class="hospital-slug"  href="{{ route('doctor.hospitals.show' , $hospital->slug) }}">
                    <h4 class=" text-uppercase margin-t-0 padding-t-0 margin-b-0 black hospital-name">{{ $hospital->name }}</h4>
                </a>

                <div class="detail-line">
                    <span>
                        <span class="small-icon yellow phone"></span>
                        <span class=" hospital-contact"> {{ $hospital->phoneNumber }}</span>
                    </span> &nbsp;

                    <span class="small-icon yellow location"></span>
                    <span class=" hospital-location">{{ $hospital->basicLocation }}</span>
                </div>

                <hr class="margin-tb-5">

                <div class="margin-tb-5 hospital-details">
                    {!! Str::limit(trim($hospital->description) == '' ? 'No description' : strip_tags($hospital->description),180) !!}
                </div>

            </div>

            @if($hospital->isPremium == 1)
                <div class="col-lg-3 col-md-3 col-xs-12 row-sm padding-r-0 col-sm-hide">
                    <div class="map no-margin" style="height: 140px;" data-location="{{ $hospital->location['name'] }}"></div>
                </div>
            @endif
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->