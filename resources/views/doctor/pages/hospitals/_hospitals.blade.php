 <div class="col-md-12 no-padding clearfix">
    <div class="col-md-12">
        <div class="row">
            <ul class="directory-list">
                @include('doctor.pages.hospitals._featured-template')
                @include('doctor.pages.hospitals._non-featured')
                @if (count($hospitals)>0)
                    @foreach ($hospitals as $index=>$hospital)
                        @include('doctor.pages.hospitals._hospital')
                    @endforeach
                @else
                    <p class="margin-t-20">There are currently no hospitals for this section.</p>
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>