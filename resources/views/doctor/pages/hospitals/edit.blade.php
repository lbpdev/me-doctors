@extends('doctor.master')

@section('styles')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <style>
        .select2-container { width: 100% !important; }
    </style>
@stop

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-9 profile col-md-9 col-sm-12 border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-b-3 margin-t-0">
                <h3 class="no-border yellow hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
                    <span class="page-icon hospitals"></span>
                    <span class="page-title">Hospital Directory</span>
                </h3>
                <hr class="margin-tb-5">
                <div class="hs_single_profile hospital">
                    {!! Form::open(['files' => true , 'route' => 'doctor.hospitals.update' , 'method' => 'PUT']) !!}
                        {!! Form::hidden('slug', $hospital->slug)!!}
                        <!-- left column -->
                          <div class="col-md-12 col-sm-12">
                              <div class="box box-primary">
                                <div class="box-header with-border">
                                <div class="qualifications">
                                  <div class="col-md-12"><h3><i class="fa fa-check-square-o"></i> Edit Hospital</h3></div>
                                      <div class="col-md-12">
                                          <div class="fields padding-b-5 border-bottom clearfix">
                                              <div class="col-md-6 padding-l-0">
                                                  <input type="text" value="{{ $hospital->name }}" name="hospital[name]" placeholder="Hospital Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                                                  <input type="text" value="{{ $hospital->website }}" name="hospital[website]" placeholder="Website" autocomplete="false"  class="margin-b-5 margin-t-5 form-control">

                                                  @if(count($hospital->contacts)>0)
                                                      @foreach($hospital->contacts as $contact)
                                                        <input type="text" value="{{ $contact->number }}" name="contacts[]" placeholder="Contact Number" autocomplete="false"  class="margin-b-5 margin-t-5 form-control">
                                                      @endforeach
                                                  @else
                                                        <input type="text" name="contacts[]" placeholder="Contact Number" autocomplete="false"  class="margin-b-5 margin-t-5 form-control">
                                                  @endif
                                                  <br>Therapy Areas : <br>
                                                  <select name="therapies[]" multiple="multiple" class="margin-b-5 form-control">
                                                      @foreach($therapies as $index=>$therapy)
                                                          <option value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                                      @endforeach


                                                        @foreach($hospital->therapies as $hospital_therapy)
                                                            @foreach($therapies as $therapy)
                                                                @if($hospital_therapy['id'] == $therapy['id'])
                                                                    <option selected value="{{ $hospital_therapy['id'] }}">{{ $hospital_therapy['name'] }}</option>
                                                                @endif
                                                            @endforeach
                                                        @endforeach

                                                  </select><br>

                                                  <br>Facilities : <br>
                                                  @if($hospital->isPremium==1)
                                                      <select name="facilities[]" multiple="multiple" class="margin-b-5 form-control">
                                                          @foreach($facilities as $index=>$facility)
                                                              <option value="{{ $facility['id'] }}">{{ $facility['name'] }}</option>
                                                          @endforeach

                                                            @foreach($hospital->facilities as $hospital_facility)
                                                                @foreach($facilities as $facility)
                                                                    @if($hospital_facility['id'] == $facility['id'])
                                                                        <option selected value="{{ $hospital_facility['id'] }}">{{ $hospital_facility['name'] }}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                      </select>
                                                  @else
                                                    <p class="margin-t-5">Adding of facilities is for premium users only.</p>
                                                  @endif
                                              </div>

                                              <div class="col-md-6 padding-l-0 margin-b-20">

                                                    @if($hospital->thumbnail)
                                                        Remove Thumbnail: {!! Form::checkbox('remove_thumb' ,1, false , ['id' => 'removeThumb','class' => 'square']) !!}
                                                        <label for="removeThumb"><span></span></label>
                                                        <img id="thumb" src="{{ asset($hospital->thumbnail) }}" width="100%">
                                                    @endif
                                                  {!! Form::input('file', 'thumbnail' , null , [ 'class' => 'form-control' ]) !!}
                                              </div>

                                              <hr class="margin-tb-5">
                                              <h4 class="pull-left">Description</h4>
                                              <div class="col-md-12 padding-b-20 padding-l-0">
                                                <textarea name="hospital[description]" placeholder="Hospital Description" autocomplete="false" style="height: 300px" class="textarea margin-b-5 margin-t-5 form-control">
                                                    {{ $hospital->description }}
                                                </textarea>
                                              </div>
                                          </div>

                                          <div class="col-md-12 padding-b-20">
                                          <h4>Location</h4>
                                          <div class="col-md-6 padding-l-0">
                                          {!! Form::input('text','location[name]' , $hospital->location ? $hospital->location->name : '' , [  'class' => 'form-control location' , 'id' => 'autocomplete' , 'placeholder' => 'Address']) !!}

                                              <table id="address" width="100%">
                                                    <tr><td>Street</td><td><input value="{{ $hospital->location ? $hospital->location->street_number : ''  }}" name="location[street_number]" class="field street_number form-control margin-tb-5" id="street_number"></td></tr>
                                                    <tr><td>Route</td><td><input value="{{ $hospital->location ? $hospital->location->street_route : ''  }}" name="location[street_route]" class="field route form-control margin-tb-5" id="route"></td></tr>
                                                    <tr><td>City</td><td><input value="{{ $hospital->location ? $hospital->location->city : ''  }}" name="location[city]" class="field locality form-control margin-tb-5" id="locality"></td></tr>
                                                    <tr style="display:none;"><td>State</td><td><input value="{{ $hospital->location ? $hospital->location->state : ''  }}" name="location[state]" class="field administrative_area_level_1 form-control margin-tb-5" id="administrative_area_level_1"></td></tr>
                                                    <tr><td>Post Code</td><td><input value="{{ $hospital->location ? $hospital->location->post_code : ''  }}" name="location[post_code]" class="field postal_code form-control margin-tb-5" id="postal_code"></td></tr>
                                                    <tr><td>Country</td><td><input value="{{ $hospital->location ? $hospital->location->country : ''  }}" name="location[country]" class="field country form-control margin-tb-5" id="country"></td></tr>
                                              </table>
                                          </div>


                                          <div class="col-md-6 no-padding">
                                              <div class="map">
                                              </div>
                                          </div>

                                        <br>Doctors : <br>
                                          @if($hospital->isPremium==1)
                                              <select name="doctors[]" multiple="multiple" class="margin-b-5 form-control">
                                                  @foreach($doctors as $index=>$doctor)
                                                      <option value="{{ $doctor['id'] }}">{{ $doctor['name'] }}</option>
                                                  @endforeach

                                                    @foreach($hospital->doctors as $doctor)
                                                        @foreach($doctors as $doc)
                                                            @if($doctor['id'] == $doc['id'])
                                                                <option selected value="{{ $doctor['id'] }}">{{ $doctor['name'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                              </select>
                                          @else
                                            <p class="margin-t-5">Adding of doctors is for premium users only.</p>
                                          @endif

                                          {!! Form::submit('Update', [ 'class' => 'form-control margin-t-20 pull-left' ]) !!}
                                          </div>

                                      </div>
                                      <br>
                                          </div>
                                          </div>
                                      </div>
                                    <br>
                              </div><!--/.col (right) -->
                        {!! Form::close() !!}
                </div>
                {{--<a href="#" data-toggle="modal" data-target="#claimModal" class="pull-right" href="http://localhost/me-doctors/patient/advertise">Claim</a>--}}
            </div>
            @include('patient.templates.sidebar-ads')
        </div>
    </div>

@stop

@section('js-inner')

    <script src="{{ asset('public/js/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('public/js/dist/langs/fr.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/upload/trumbowyg.upload.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/base64/trumbowyg.base64.js') }}"></script>
    <script src="{{ asset('public/js/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="{{ asset('public/js/custom/updateImage.js') }}"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script>
    $('select').select2();
    initMap();
        function initMap() {
            var map = new google.maps.Map($('.map')[0], {
                zoom: 12,
                disableDefaultUI: true,
//                draggable: false,
//                zoomControl: false,
//                scrollwheel: false,
//                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map);
          }

          function geocodeAddress(geocoder, resultsMap) {
            var address = $('#autocomplete')[0].value;
            if(address=="")
                address = "Dubai - United Arab Emirates";
            console.log(address);
            geocoder.geocode({'address': address}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
              } else {}
            });
          }

        $('.textarea').trumbowyg({
            removeformatPasted: true,
            btns: ['viewHTML',
                  '|', 'formatting',
                  '|', 'btnGrp-design',
                  '|', 'link',
                  '|', 'btnGrp-justify',
                  '|', 'btnGrp-lists',
                  '|', 'horizontalRule']
        });

        // This example displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        var placeSearch, autocomplete;
        var componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };

        function initialize() {
          // Create the autocomplete object, restricting the search
          // to geographical location types.
          autocomplete = new google.maps.places.Autocomplete(
              /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
              { types: ['geocode'] });
          // When the user selects an address from the dropdown,
          // populate the address fields in the form.
          google.maps.event.addListener(autocomplete, 'place_changed', function() {
            fillInAddress();
            initMap();
          });
        }

        function fillInAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();

          for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
          }

          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = place.address_components[i][componentForm[addressType]];
              document.getElementById(addressType).value = val;
            }
          }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var geolocation = new google.maps.LatLng(
                  position.coords.latitude, position.coords.longitude);
              var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
              });
              autocomplete.setBounds(circle.getBounds());
            });
          }
        }

        initialize();
    </script>

@stop























