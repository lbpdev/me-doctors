@if($hospital->doctors)
    @if(count($hospital->doctors))
    <div class="fields padding-b-7 clearfix">
    <h1 class="t-yellow margin-t-20 margin-b-7 clearfix pull-left width-full">Doctors</h1>
        <div class="col-md-12 padding-l-0">
            @foreach($hospital->doctors as $doctor)
                <div class="col-md-3">
                    <a href="{{ route('doctor.profile',$doctor->username) }}">
                        <div class="row">
                            <div class="col-md-4 no-padding margin-b-10">
                                <img src="{{ asset($doctor->avatar) }}" width="100%">
                            </div>
                            <div class="col-md-8 padding-r-0">
                                <h4 class="margin-t-4">{{ $doctor->fullName }}</h4>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    @endif
@endif