<div class="padding-b-7 clearfix">
@if(count($hospital->facilities)>0)
    <h1 class="t-yellow margin-t-20 margin-b-7 clearfix pull-left width-full">Facility Keys</h1>
    <div class="col-md-12 padding-l-0 facilities">
        @foreach($hospital->facilities as $facility)
            <span class="facility-icon {{ $facility->slug }}"></span> {{ $facility->name }}
        @endforeach
    </div>
@endif
</div>

