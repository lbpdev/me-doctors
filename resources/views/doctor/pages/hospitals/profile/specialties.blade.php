<div class="hs_single_profile_detail padding-b-10 clearfix">
    <h1 class="t-yellow margin-t-7 margin-b-4">Specialties</h1>
    @if($hospital->therapies && count($hospital->therapies) > 0)
        @foreach($hospital->therapies as $index=>$specialty)
          {{ $specialty->name }}{{  ($index+1) < count($hospital->therapies) ? ', ' : '' }}
        @endforeach
    @else
        Not set.
    @endif
</div>