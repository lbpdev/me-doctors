@extends('doctor.master')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
@stop

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-9 profile col-md-9 col-sm-12 border-right">

                @include ('doctor.partials._news-ticker')

                <hr class="margin-b-3 margin-t-0">

                <h3 class="no-border yellow hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
                    <span class="page-icon hospitals"></span>
                    <span class="page-title">Hospital Directory</span>
                </h3>

                <hr class="margin-tb-5">

                <div class="hs_single_profile hospital">

                    @include ('doctor.pages.hospitals.profile.main')
                    <hr class="no-margin">
                    @include ('doctor.pages.hospitals.profile.specialties')
                    <hr class="no-margin">
                    @include ('doctor.pages.hospitals.profile.location')
                    <hr class="no-margin">
                    @include ('doctor.pages.hospitals.profile.about')
                    {{--<hr class="no-margin">--}}
                    {{--@include ('doctor.pages.hospitals.profile.doctors')--}}
                    <hr class="no-margin">
                    @include ('doctor.pages.hospitals.profile.facilities')
                    <hr class="no-margin">
                    @include ('doctor.pages.hospitals.profile.doctors')

                </div>

            </div>

            @include('doctor.templates.sidebar-ads')
        </div>
    </div>

    <div class="hs_margin_40"></div>
        @include('doctor.templates.modals.hospital-claim')
    @if($authUser)
        @include('doctor.templates.modals.hospital-premium-form')
    @endif
@stop

@section('js')
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/profileFields.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script>
    function initMap() {
        var map = new google.maps.Map(document.getElementsByClassName('map')[0], {
            zoom: 12,
            disableDefaultUI: true,
            draggable: true,
            zoomControl: true,
            scrollwheel: true,
            disableDoubleClickZoom: true
        });
        var geocoder = new google.maps.Geocoder();

        geocodeAddress(geocoder, map , 0);
      }

      function geocodeAddress(geocoder, resultsMap ,index) {
        var address = "<?php echo preg_replace( "/\r|\n/", "", $hospital->location ? $hospital->location->name : ''); ?>";
        var map = $('.map')[index];

        geocoder.geocode({'address': address}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            $(map).find('.gm-style').show();
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            $(map).hide();
          }
        });
      }

      if(document.getElementsByClassName('map').length > 0)
        initMap();

    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    $('#email').keydown(function(event){
       if(event.keyCode === 13) {

        if(re.test($(this).val()))
            $('#invalidEmail').hide();
        else
        {
            event.preventDefault();
            $('#invalidEmail').show();
            return false;
        }
        $('#submitClaim').trigger('click');
        return false;
       }
     });

    $('#submitClaim').on('click',function(e){
        e.preventDefault();

        var url = '{{ route('doctor.hospitals.checkProvider') }}';
        var hospital_id = '{{ $hospital->id }}';

        console.log($('#email').val());
        if(re.test($('#email').val())){

        $.ajax({
            url: url,
            type: 'POST',
            data: { email : $('#email').val(), hospital_id : hospital_id },
            context: document.body,
            success: function(result){
              if(result==1){
                  $('#invalidEmail').hide();
                  $('#claimForm').trigger('submit');
              }
              else{
                  event.preventDefault();
                  $('#invalidEmail').show();
                  return false;
              }

            return false;
            }
          }).done(function() {
            $( this ).addClass( "done" );
          });
        } else {
          $('#invalidEmail').show();
        }

    });
    </script>
@stop

