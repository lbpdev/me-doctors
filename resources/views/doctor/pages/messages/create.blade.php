@extends('doctor.master')

@section('content')
<div class="container page-container messages">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 row-sm border-right">

                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-4">

                @include('doctor.templates.inbox-menu')
                <div class="col-md-9 col-sm-12 col-xs-12 clearfix margin-t-40 row-sm pull-left">
                {!! Form::open(['id'=>'msgForm' , 'onSubmit' => 'return checkEmail()']) !!}
                @if(isset($conversation))
                    {!! Form::hidden('conversation_id' , $conversation->id) !!}
                @endif
                    <div class="row">
                          <div class="col-md-12">

                            <div id="alert" class="alert alert-danger hidden" role="alert">Unidentified User. Please select one from the list.</div>
                            @if($authUser->hasRole('Administrator') && isset($therapies) )
                                <div class="form-group">
                                    Select Group : <br>
                                    <div class="custom-select gray">
                                        {!! Form::select('therapy_id',$therapies,null,['class' => 'form-control margin-t-4', 'id' => 'therapy_group']) !!}
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                To : <br>
                                @if(isset($conversation))
                                    {!! Form::input('text','receiver' , $conversation->author->username. ' ' . '[ ' .$conversation->author->email . ' ]' , [ 'required' , 'class' => 'form-control margin-t-4' , 'disabled']) !!}
                                 @else
                                    {!! Form::textarea('receiver' , null , [ 'rows' => '1' ,'required' , 'class' => 'form-control margin-t-4', 'id' => 'user_auto' ]) !!}
                                @endif
                            </div>
                            <div class="form-group">
                                Subject : <br>
                                @if(isset($conversation))
                                    {!! Form::input('text','subject' , 'RE: ' . $conversation->subject , [ 'required' , 'class' => 'form-control margin-t-4' ]) !!}
                                @else
                                    {!! Form::input('text','subject' , null , [ 'required' , 'class' => 'form-control margin-t-4' ]) !!}
                                @endif
                            </div>
                            <div class="form-group">
                                Content:
                                {!! Form::textarea('message', isset($conversation) ? $conversation->content : null , [ 'required', 'class' => 'form-control ' ,'id' => 'editor']) !!}
                            </div>

                          </div>
                    </div>
                    <div class="voffset1 clearfix"></div>
                    {!! Form::submit('Send', [ 'class' => 'form-control' ]) !!}
                {!! Form::close() !!}
                    <hr>
                       @if(isset($conversation))
                            <h4> Subject: {!! $conversation->subject !!}</h4>
                            From: {!! $conversation->author->email .'<br>' !!}

                            @foreach($conversation->messages as $message)
                                <div class="col-md-12 padding-tb-10">
                                        <div class="row">
                                        <span class="pull-left width-full margin-b-10"><b>{{ $message->sender->lname }} {{ $message->sender->fname }}:</b></span><br>
                                            {!! $message->message !!}<br>

                                        <span class="pull-right">{{ Carbon::now()->diffForHumans($message->created_at, true) }} ago</span>
                                        </div>
                                </div>
                            @endforeach

                            <hr>
                        @endif
                </div>
            </div>

            @include('doctor.templates.sidebar-ads')

        </div>
    </div>
</div>

@stop

@section('js')

    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/dist/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('public/js/dist/ckeditor/ckeditor.js') }}"></script>

    <script>
      CKEDITOR.replace( 'editor' );
      CKEDITOR.config.format_address = { element: 'address', attributes: { 'class': 'referenceText' } };
    </script>

        <script type="text/javascript">

            function auto_grow(element) {
                element.style.height = "5px";
                element.style.height = (element.scrollHeight)+"px";
            }

            $(document).ready(function(){
                // from a jQuery collection

                $('.textarea').trumbowyg('destroy');
                $('.textarea').trumbowyg({
                    removeformatPasted: false,
                    btns: [
//                        'viewHTML',
//                        '|',
                          'formatting',
                          '|', 'btnGrp-design',
                          '|', 'link',
                          '|', 'btnGrp-justify',
                          '|', 'btnGrp-lists'
//                        '|', 'horizontalRule'
                    ]
                });
            });

            $("#user_auto").autocomplete({
                source    : <?php echo json_encode($users); ?>,
                autoFocus : true
            });

//            $( "#user_auto" ).on( "focus", function( event ) {
//                $( "#user_auto" ).autocomplete( "search", " " );
//            });

            $( "#user_auto" ).on( "autocompleteselect", function( event, ui ) {
                var value = document.getElementById('user_auto').value;
                if(value.indexOf("@") > -1 && value.indexOf('[') > -1  && value.indexOf(']') > -1){
                    document.getElementById('alert').classList.add('hidden');
                }
            });

            $( "#therapy_group" ).on( "change", function( event, ui ) {

                $.ajax({
                    type: "POST",
                    url: '{{ route('get-users-from-therapy') }}',
                    data: { therapy_id : $(this).val() } ,
                    beforeSend: function (xhr) {
                     var token = $('meta[name="csrf_token"]').attr('content');

                     if (token) {
                           return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                     }
                    },
                    success: function(results){
                      var data = JSON.parse(results);
                      console.log(data);
                      $('#user_auto').val(data);
                    },
                    error: function(xhr, status, error) {
                      $(event.target).closest('.section').find('.alert-danger').show().delay(3000).fadeOut(3000);
                    }
                });
            });

            function checkEmail(){
                var value = document.getElementById('user_auto').value;

                if(value.indexOf("@") < 0 && value.indexOf('[') < 0  && value.indexOf(']') < 0 ){
                    document.getElementById('alert').classList.remove('hidden');
                    return false;
                } else {
                    return true;
                }
            }

        </script>

@stop







