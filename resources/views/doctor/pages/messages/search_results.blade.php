@extends('doctor.master')

@section('content')
<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents border-right padding-l-0 padding-r-0 row-sm">

                @include('doctor.templates.inbox-menu')

                <div class="col-md-9 clearfix row-sm pull-left">

                    <h4><i class="fa fa-comment"></i> Conversations</h4>

                    @if(count($conversations)>0)
                        @foreach($conversations as $conversation)
                            <div class="col-md-12 discussion-item  no-padding margin-b-10 clearfix border-bottom row-sm">
                              <div class="col-lg-12 col-md-12 no-padding">
                                <div class="post-content left-pad-20 clearfix">
                                  <div class="col-lg-11 col-md-11 col-xs-11 no-padding margin-t-4">
                                    <div class="col-md-11">
                                        <div class="row line-height-22">
                                            <a href="{{ route('messages_single',$conversation->id) }}">
                                                <span class="t-gray margin-t-0 margin-b-0 robotomedium">
                                                <strong>From :</strong>
                                                @if($conversation->author->id==Auth::id())
                                                    @foreach($conversation->members as $member)
                                                        @if($member->id != Auth::id())
                                                            {{ $member->fname }} {{ $member->lname }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                     {{ $conversation->author->lname }} {{ $conversation->author->fname }}
                                                @endif
                                                </span>
                                            </a><br>
                                            <a href="{{ route('messages_single',$conversation->id) }}">
                                                <span class="t-gray  margin-b-0 margin-t-5 robotolight">
                                                <strong>Subject :</strong>
                                                {{ $conversation->subject }}
                                                @if(isset($data['unread_messages']))
                                                    <span class="pink">( {{ count($data['unread_messages']) }} unread )</span>
                                                @endif
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="margin-t-5 col-md-12 padding-b-7">
                                        <div class="row">
                                         {{ strip_tags(Str::words($conversation->messages[0]->message ,33, '...')) }}
                                        </div>
                                    </div>
                                  </div>
                                  
                                </div><!-- End post Content -->
                              </div><!-- End Col -->
                            </div><!-- End Row -->
                        @endforeach
                    @else

                        @if(!isset($noCheck))
                            Inbox is empty
                        @else
                            No results.
                        @endif
                    @endif

                    <h4><i class="fa fa-envelope"></i> Messages</h4>

                   @if(isset($messages) && count($messages)>0)
                        @foreach($messages as $message)
                            <div class="col-md-12 padding-tb-10">
                                <a href="{{ route('messages_single',$message->conversation->id) }}">
                                        <div class="row">
                                        <span class="pull-left"><b>{{ $message->sender->lname }} {{ $message->sender->fname }}:</b></span><br>
                                            {!! $message->message !!}<br>
                                        <span class="pull-right">{{ Carbon::now()->diffForHumans($message->created_at , true) }} ago</span>
                                        </div>
                                </div>
                            </a>
                        @endforeach
                        <hr>
                    @else
                        No results.
                    @endif

                </div>

                <div class="clearfix"></div>

                <?php /** echo $discussions->render(); **/ ?>
            </div>

            @include('doctor.templates.sidebar-ads')
        </div>
    </div>
</div>

@stop

@section('js')
    <script type="text/javascript">

        function togglecheckboxes(master,group){
            var cbarray = document.getElementsByName(group);
            var ids = [];

            for(var i = 0; i < cbarray.length; i++){
                var cb = cbarray[i];

                if(master)
                    cb.checked = master.checked;

                if(cb.checked==true)
                    ids.push(cb.value);
            }
            updateIds(ids);
        }

        function updateIds(ids){
            document.getElementById('trash_ids').value = JSON.stringify(ids);
        }

        function trashSubmit(){
            document.forms["trash_form"].submit();
        }
    </script>
@stop
