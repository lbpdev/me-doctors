@extends('doctor.master')

@section('content')
<div class="container page-container messages">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents border-right padding-l-0 padding-r-0 row-sm">

                @include('doctor.templates.inbox-menu')

                <div class="col-md-9 clearfix row-sm">
                <div class="filters margin-t-20 border-bottom clearfix col-md-12 padding-10 row-sm">
                    <div class="col-lg-4 col-md-4  col-sm-4  col-md-4 col-xs-12 padding-l-0">
                        <span class="pull-left"><h3 class="no-margin t-regular">Sent </h3></span>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-11 no-padding actions">
                            {!! Form::open(['name'=>'trash_form', 'url'=>route('messages_sent_trash_multi')]) !!}
                                {!! Form::hidden('trash_ids' , null , ['id'=>'trash_ids']) !!}
                            {!! Form::close() !!}
                            <a class="pull-right" onclick="trashSubmit()" href="#"><i class="fa fa-trash"></i> Trash</a>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                        <span class=" margin-r-10 pull-right">
                            <input type="checkbox" onchange="togglecheckboxes(this,'checkToggle')">
                         </span>
                    </div>
                </div>

            @if(count($messages)>0)
                @foreach($messages as $message)
                    <div class="col-md-12 discussion-item margin-b-10 clearfix padding-10 border-bottom row-sm">
                      <div class="col-lg-12 col-md-12 no-padding">
                        <div class="post-content left-pad-20 clearfix">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
                            <div class="row">
                              <a href="{{ route('messages_sent_single',$message->id) }}">
                                <img src="http://placehold.it/200x255" width="100%">
                              </a>
                            </div>
                          </div>

                          <div class="col-lg-9 col-md-9 col-xs-9">
                            <div class="col-md-6">
                                <div class="row">
                                    <a href="{{ route('messages_sent_single',$message->id) }}">
                                        <h4 class="margin-t-0 margin-b-0">
                                        @foreach($message->conversation->members as $member)
                                            @if(Auth::id() != $member->id)
                                                {{ $member->fname }} {{ $member->lname }}
                                            @endif
                                        @endforeach
                                        </h4>
                                    </a>
                                    <a href="{{ route('messages_sent_single',$message->id) }}">
                                        <h5 class="margin-b-0 t-gray margin-t-5">{{ $message->subject }}</h5>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <span class="pull-right font-12">{{ Carbon::now()->diffForHumans($message->created_at, true) }} ago</span>
                                </div>
                            </div>
                            <div class="content margin-t-5 col-md-12">
                                <div class="row">
                                 {{ Str::words($message->message ,13, '...') }}
                                </div>
                            </div>
                          </div>
                          <div class="col-lg-1 col-md-1 col-xs-1 no-padding text-center">
                            <span class="pull-right text-center margin-r-10">
                                {!! Form::checkbox('checkToggle' , $message->id  , null ,  ['class' => 'text-middle no-margin' , 'onchange' => "togglecheckboxes(false,'checkToggle')"]) !!}
                            </span>
                          </div>
                        </div><!-- End post Content -->
                      </div><!-- End Col -->
                    </div><!-- End Row -->
                @endforeach
            @else
                No sent items
            @endif
            </div>

                <div class="clearfix"></div>

                <?php /** echo $discussions->render(); **/ ?>
            </div>
            @include('doctor.templates.sidebar-ads')

        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">

        function togglecheckboxes(master,group){
            var cbarray = document.getElementsByName(group);
            var ids = [];

            for(var i = 0; i < cbarray.length; i++){
                var cb = cbarray[i];

                if(master)
                    cb.checked = master.checked;

                if(cb.checked==true)
                    ids.push(cb.value);
            }
            updateIds(ids);
        }

        function updateIds(ids){
            document.getElementById('trash_ids').value = JSON.stringify(ids);
        }

        function trashSubmit(){
            document.forms["trash_form"].submit();
        }
    </script>
@stop
