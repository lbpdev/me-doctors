@extends('doctor.master')

@section('content')
<div class="container page-container messages">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 row-sm border-right">

                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-4">
                @include('doctor.templates.inbox-menu')

                <div class="col-md-9 col-sm-12  col-xs-12 clearfix pull-left">
                    <div class="filters clearfix col-md-12 padding-t-0 padding-l-0 padding-b-0">
                        <div class="col-lg-11 col-md-11 col-xs-11 padding-l-0 actions">
                            @if(isset($_GET['sent']))
                                <a class="pull-left margin-r-20" href="{{ route('messages_trash',$conversation->id ) }}"><span class="small-icon trash"></span> Trash</a>
                            @elseif(isset($_GET['trash']))
                                <a class="pull-left margin-r-20" href="{{ route('messages_restore',$conversation->id) }}"><i class="fa fa-plus-square-o"></i> Restore</a>
                            @else
                                <a class="pull-left margin-r-20" href="{{ route('messages_reply',$conversation->id) }}"><span class="small-icon reply"></span> Reply</a>
                                <a class="pull-left margin-r-20" href="{{ route('messages_trash',$conversation->id ) }}"><span class="small-icon trash"></span> Trash</a>
                            @endif
                        </div>
                    </div>
                    <hr class="margin-t-7 margin-b-0">
                    <div class="col-md-12 no-padding">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-xs-10 padding-tb-10">
                                <a href="{{ route('doctor.profile',$conversation->author->username) }}">
                                    <h4 class="margin-t-0 margin-b-0">
                                        {{ $conversation->subject }}
                                    </h4>
                                </a>
                                <a href="{{ route('messages_single',$conversation->id) }}">
                                    <h5 class="margin-b-0 t-gray margin-t-5">{{ $conversation->author->name }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr class="margin-tb-0">
                    <div class="pull-left date-divider">
                        <span class="date">{{ Carbon::now()->diffForHumans($conversation->created_at, true) }} ago</span>
                    </div>

                   @if(isset($conversation))
                        @foreach($conversation->messages as $message)
                            <div class="col-md-12 padding-b-10 padding-t-0">
                                <div class="row">
                                    <span class="pull-left width-full margin-b-10"><b>{{ $message->sender->lname }} {{ $message->sender->fname }}:</b></span><br>
                                        {!! $message->message !!}<br>
                                    <span class="pull-right">{{ Carbon::now()->diffForHumans($message->created_at , true) }} ago</span>
                                </div>
                            </div>
                        @endforeach

                        {!! Form::hidden('conversation_id' , $conversation->id) !!}
                        <hr>
                    @endif

                <div class="clearfix"></div>

                <?php /** echo $discussions->render(); **/ ?>
                </div>
            </div>
            @include('doctor.templates.sidebar-ads')
        </div>
    </div>
</div>

@stop