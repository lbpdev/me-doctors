@extends('doctor.master')

@section('content')
<div class="container page-container messages">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents border-right padding-l-0 padding-r-0 row-sm">

                @include('doctor.templates.inbox-menu')

                <div class="col-md-9 col-sm-12 col-xs-12 clearfix row-sm pull-left">
                    <div class="filters margin-t-0 border-bottom clearfix col-md-12 padding-tb-10 padding-l-0 padding-r-0 row-sm">

                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding actions">
                        {!! Form::open(['name'=>'restore_form', 'url'=>route('messages_restore_multi')]) !!}
                            {!! Form::hidden('restore_ids' , null , ['id'=>'restore_ids']) !!}
                        {!! Form::close() !!}
                        <a class="pull-right" onclick="restoreSubmit('checkToggle')" href="#"><i class="fa fa-plus-square-o"></i> Restore</a>

                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                            <span class="pull-right">
                                <input id="radioAll" class="square" type="checkbox" onchange="togglecheckboxes(this,'checkToggle')">
                                <label for="radioAll"><span></span></label>
                            </span>
                        </div>
                    </div>

                    @if(isset($conversations) && count(isset($conversations)) > 0)
                        @include('doctor.pages.messages._message')
                    @else
                        <p class="pull-left margin-t-10">Trash is empty.</p>
                    @endif

                <?php echo $members->render(); ?>
                </div>

                <div class="clearfix"></div>

            </div>

            @include('doctor.templates.sidebar-ads')
        </div>
    </div>
</div>

@stop

@section('js')
    <script type="text/javascript">

        function togglecheckboxes(master,group){
            var cbarray = document.getElementsByName(group);
            var ids = [];

            for(var i = 0; i < cbarray.length; i++){
                var cb = cbarray[i];

                if(master)
                    cb.checked = master.checked;

                if(cb.checked==true)
                    ids.push(cb.value);
            }
            updateIds(ids);
        }

        function updateIds(ids){
            document.getElementById('restore_ids').value = JSON.stringify(ids);
        }

        function restoreSubmit(){
            document.forms["restore_form"].submit();
        }
    </script>
@stop