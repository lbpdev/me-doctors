@if(count($news)>0)
    @foreach ($news as $feed)
        <li class="media clearfix height-auto gray-bg">
           <div class="media-left col-md-4 col-sm-4 col-xs-12 no-padding">
                <a href="{{ route('show_event', $feed->slug) }}">
                    @if($feed->thumbnail)
                        <img class="media-object" src="{{ asset($feed->thumbnail) }}" alt="" width="100%">
                    @endif
                </a>
            </div>
            <div class="media-body height-auto col-md-8 col-sm-8 col-xs-12 padding-tb-7">
                <h4 class="media-heading blue">
                   <a href="{{ route('doctor.news.show', $feed->slug) }}">
                     {{ Str::words($feed->title, 9, '...') }}
                    </a>
                </h4>

                <p>
                 <b>Date Posted: </b>
                 {{ $feed->date_posted->format('M d, Y')  }}
                </p>

                @if(Str::length($feed->title) > 70)
                    {{ Str::words(strip_tags($feed->content), 15, '...') }}
                @else
                    {{ Str::words(strip_tags($feed->content), 30, '...') }}
                @endif
           </div>
        </li>
    @endforeach
@else
    No Articles for this section
@endif