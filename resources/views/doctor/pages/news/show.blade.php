@extends('doctor.master')

@section('metas')

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $news->title }}">
    <meta itemprop="description" content="{!! Str::limit(strip_tags($news->content), 150) !!}">
    <meta itemprop="image" content="{{ asset( $news->thumbnail) }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="www.middleeastdoctor.com">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="{!! Str::limit(strip_tags($news->content), 150) !!}">
    <meta name="twitter:creator" content="Middle East Doctor">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset( $news->thumbnail) }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $news->title }}" />
    <meta property="og:type" content="news feed" />
    <meta property="og:url" content="{{ route('doctor.articles.show', $news->slug )}}" />
    <meta property="og:image" content="{{ asset( $news->thumbnail) }}" />
    <meta property="og:description" content="{!! Str::limit(strip_tags($news->content), 150) !!}" />
    <meta property="og:site_name" content="www.middleeastdoctor.com" />
    <meta property="article:published_time" content="{{ $news->created_at }}" />
    <meta property="article:modified_time" content="{{ $news->updated_at }}" />
    <meta property="article:section" content="Article Section" />
    <meta property="article:tag" content="Article Tag" />
    <meta property="fb:admins" content="100000370643856" />

    <style>
        b, strong {
            line-height: 24px;
        }

        ul, ol {
            line-height: 24px;
            margin-top: 10px;
        }
    </style>
@stop


@section('content')
    <div class="container articles-page page-container single-article">
            <div class="row">
                <div class="col-md-9 col-sm-12 border-right">
                    @include ('doctor.partials._news-ticker')
                    <hr class="no-margin pull-left">
                    {{--<h5><a href="{{ route('doctor.articles.index') }}"><< Back to Articles</a></h5>--}}

                    <h2 class="hs_heading">{{ $news->title }}</h2>
                    <h2 class="hs_subheading">{{ $news->subtitle }}</h2>
                    <div class="hs_margin_30"></div>

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="post-content left-pad-20">

                            <div class="content padding-t-0">
                                @if($news->FeaturedImage)
                                    <img src="{{ asset( $news->FeaturedImage) }}" width="100%" class="margin-b-10">
                                @endif

                                Published: <em>{{ $news->date_posted->format('F d, Y')  }}</em> <br><br>

                                <div class="fancify">
                                {!! $news->content !!}
                                </div>

                                <div class="col-md-12 margin-t-20">
                                    <div class="row">
                                        <span class='st_facebook_large' displayText='Facebook'></span>
                                        <span class='st_twitter_large' displayText='Tweet'></span>
                                        <span class='st_googleplus_large' displayText='Google +'></span>
                                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                                        <span class='st_email_large' displayText='Email'></span>
                                    </div>
                                </div>
                            </div>

                          </div><!-- End post Content -->
                        </div><!-- /Col -->
                    </div><!-- /Row -->
                </div>

                @include ('doctor.templates.sidebar-ads')
            </div>
        </div>
@stop

@section('js')
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({publisher: "6eecfc05-f227-415e-b39c-b39cfcff45b5", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
@stop