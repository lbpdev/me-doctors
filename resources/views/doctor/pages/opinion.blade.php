@extends('doctor.master')

@section('content')
<div class="hs_page_title">
  <div class="container">
    <ul>
      <li><a href="{{ URL::to('/') }}">Home</a></li>
      <li><a href="#">Opinion</a></li>
    </ul>
  </div>
</div>

<div class="container">
  <!--who we are start-->
  <div class="col-md-8">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h2 class="hs_heading"><i class="fa fa-bullhorn"></i> Opinion</h2>
      <div class="row hs_how_we_are">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Explore and discuss medical controversies, ethics, new guidelines, drugs, policies,
            pharmaceutical companies, practice management and various other subjects we believe you
            might have something to say about. </h3>
            <br>
            <p>Get other doctors’ perspectives on current hot topics, either in general medicine or
            your area of specialisation. Let us know your thoughts on topics relating to medicine,
             and let your voice be heard as we discuss some of today’s most important subjects. </p>

            <p>This section has been specifically designed so that you can join in discussions and
            conversations with your peers on general topics or discuss highly specialised subjects with fellow clinicians.</p>

            <p>Who knows? Maybe your discussions will influence real-world decision makers and lead to better patient
            and clinical outcomes. </p>
            <br>
            <p>Check out the latest debates <a href="{{ URL::to('discussions') }}">here</a>. </p>

        </div>
      </div>
    </div>
  </div>
  </div>

@include('......templates.sidebar-posts')
</div>


@stop
