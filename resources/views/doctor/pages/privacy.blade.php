@extends('doctor.master')

@section('content')
<div class="hs_page_title">
  <div class="container">
    <ul>
      <li><a href="{{ URL::to('/') }}">Home</a></li>
      <li>Privacy Policy</li>
    </ul>
  </div>
</div>

<div class="container">
  <!--who we are start-->
  <div class="col-md-8">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h2 class="hs_heading"><i class="fa fa-bullhorn"></i> Privacy Policy</h2>
      <div class="row hs_how_we_are">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <b>
                Middle East Doctor respects users' right to privacy and strives to
                be transparent at all times about its treatment of information available on the site. <br> <br>
                The "Privacy Policy” provided below discloses the privacy practices for Middle
                 East Doctor (“MED” or “Website") in relation to all “User(s)”.<br> <br>
                In the course of using the Website, MED collects information about Users and their
                 habits of using the Website. <br>
            </b>
            <br>
            <p>Middle East Doctor respects users' right to privacy and strives to be transparent at all times about its treatment of information available on the site.
               The "Privacy Policy” provided below discloses the privacy practices for Middle East Doctor (“MED” or “Website") in relation to all “User(s)”.
               In the course of using the Website, MED collects information about Users and their habits of using the Website.</p>

            <h3>How we use information</h3>
            <p>MED uses email addresses that have been shared to communicate with Users sharing
            information such as news, updates, sponsored messages and other information that is
            deemed important to the operation of Website.</p>
            <ul>
                <li>Merger or Acquisition</li>
            </ul>
            <br>
            <p>Information collected about Users may be transferred to a third party as a result
             of a sale or acquisition or merger involving MED or its owners, Leading Brands.</p>
            <ul>
                <li>Log Files</li>
             </ul>
            <br>
            <p>MED may use IP addresses and browser usage information to gather demographic information
            for aggregate use, which may be commercial in nature.</p>
            <ul>
                <li>Spam</li>
            </ul>
            <br>
            <p>MED has a strict no spam policy and will not sell email addresses to third parties.</p>

            <h3>Questions</h3>
            <p>If Users have any questions pertaining to MED’s Privacy Policy, please contact
             us by sending an email to info@leadingbrands.me.</p>
            <p>MED will attempt to respond to all questions and queries within a reasonable timeframe.</p>

        </div>
      </div>
    </div>
  </div>
  </div>

@include('......templates.sidebar-posts')
</div>


@stop
