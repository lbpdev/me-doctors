  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($articles as $article)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <h4 class="clearfix no-margin">
                                    <a href="{{ route('doctor.articles.show', $article->slug) }}" class="pull-left">
                                         {{ $article->title }}
                                     </a>
                                 </h4>
                                  <span class="update">Posted {{ $article->created_at->diffForHumans() }}</span>
                                 <p>{!! Str::words(strip_tags($article->content) , 14, '...') !!}</p>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
