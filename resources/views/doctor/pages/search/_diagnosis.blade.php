   <div class="col-md-12 no-padding clearfix">
      <div class="col-md-12">
          <div class="row">
              <ul class="search-list">
                  @foreach ($diagnosis as $diag)

                    <li class="discussion clearfix height-auto">
                         <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                            <div class="row">
                                <h4 class="clearfix no-margin">
                                   <a href="{{ route('doctor.diagnosis.show', $diag->slug) }}" class="pull-left">
                                        {{ $diag->title }}
                                    </a>
                                    <span class="comments pull-right"><i class="fa fa-comments-o"></i> {{ $diag->commentsCount }} </span>
                                </h4>
                                    <span class="update">Started {{ $diag->created_at->diffForHumans() }}</span>
                                <p>{!! Str::words(strip_tags($diag->content) , 14, '...') !!}</p>
                            </div>
                         </div>
                     </li>

                  @endforeach
              </ul>
          </div>
      </div>
  </div>

  <div class="clearfix"></div>
