   <div class="col-md-12 no-padding clearfix">
      <div class="col-md-12">
          <div class="row">
              <ul class="search-list">
                  @foreach ($discussions as $discussion)

                    <li class="discussion clearfix height-auto">
                         <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                            <div class="row">
                                <h4 class="clearfix no-margin">
                                   <a href="{{ route('doctor.discussions.show', $discussion->slug) }}" class="pull-left">
                                        {{ $discussion->title }}
                                    </a>
                                    <span class="comments pull-right"><i class="fa fa-comments-o"></i> {{ $discussion->commentsCount }} </span>
                                </h4>
                                    <span class="update">Started {{ $discussion->created_at->diffForHumans() }}</span>
                                <p>{!! Str::words(strip_tags($discussion->content) , 14, '...') !!}</p>
                            </div>
                         </div>
                     </li>

                  @endforeach
              </ul>
          </div>
      </div>
  </div>

  <div class="clearfix"></div>
