  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($news as $feed)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <h4 class="clearfix no-margin">
                                    <a href="{{ route('doctor.news.show', $feed->slug) }}" class="pull-left">
                                         {{ $feed->title }}
                                     </a>
                                 </h4>
                                  <span class="update">Posted {{ $feed->created_at->diffForHumans() }}</span>
                                 <p>{!! Str::words(strip_tags($feed->content) , 14, '...') !!}</p>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
