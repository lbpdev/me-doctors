@extends('doctor.master')

@section('content')
    <div class="container page-container search-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">
                <h3 class="no-border yellow hs_heading margin-b-20 padding-b-0 margin-t-0 clearfix">
                    <span class="page-title doctor-blue"> Search Results for : <?php echo $_GET['keyword']; ?></span>
                </h3>
                <div class="bs-example">
                    <div class="panel-group" id="accordion">
                    @if(count($articles)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseArticles" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Articles <span class="font-12 align-top">( {{count($articles)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseArticles" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._articles')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($news)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseArticles" class="collapsed">
                                    <i class="fa fa-caret-right"></i> News Feeds <span class="font-12 align-top">( {{count($news)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseArticles" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._news')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($hospitals)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseHospitals" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Hospitals <span class="font-12 align-top">( {{count($hospitals)}} )</span>
                                    </a>
                            </h4>
                            <div id="collapseHospitals" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._hospitals')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($discussions)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsePanel" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Panel Discussions <span class="font-12 align-top">( {{count($discussions)}} )</span>
                                </a>
                            </h4>
                            <div id="collapsePanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._discussions')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($diagnosis)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDiagnosis" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Diagnosis <span class="font-12 align-top">( {{count($diagnosis)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseDiagnosis" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._diagnosis')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($events)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEvents" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Events <span class="font-12 align-top">( {{count($events)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseEvents" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._events')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($surveys)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSurveys" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Surveys <span class="font-12 align-top">( {{count($surveys)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseSurveys" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._surveys')
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(count($users)>0)
                        <div class="panel panel-default">
                            <h4 class="no-margin doctor-blue gray-border-t padding-t-10" >
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUsers" class="collapsed">
                                    <i class="fa fa-caret-right"></i> Users <span class="font-12 align-top">( {{count($users)}} )</span>
                                </a>
                            </h4>
                            <div id="collapseUsers" class="panel-collapse collapse">
                                <div class="panel-body">
                                    @include('doctor.pages.search._users')
                                </div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>

                    @if(
                        count($articles)<1 &&
                        count($hospitals)<1 &&
                        count($discussions)<1 &&
                        count($diagnosis)<1 &&
                        count($events)<1 &&
                        count($surveys)<1 &&
                        count($news)<1 &&
                        count($users)<1
                    )
                        <p class="no-margin">Sorry, but your search has no results. Please amend your search words or phrase and try again.</p>
                    @endif
                    <?php /* echo $discussions->render(); */ ?>
                </div>

                @include('doctor.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop