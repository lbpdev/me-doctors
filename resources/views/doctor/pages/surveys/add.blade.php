@extends('doctor.master')

@section('content')
    <div class="hs_page_title">
      <div class="container">
        <div class="col-md-9">
            <h3>Discussions</h3>
        </div>
        {{--<ul>--}}
          {{--<li><a href="index.html">Home</a></li>--}}
          {{--<li><a href="blog-categories.html">blog Categories</a></li>--}}
        {{--</ul>--}}
        <div class="col-md-3 search">
            <a href="" id="hs_search">Search <i class="fa fa-search"></i></a>

              <div class="hs_search_box" style="display: none;">
                <form class="form-inline" role="form">
                  <div class="form-group has-success has-feedback">
                    <input type="text" class="form-control" id="inputSuccess4" placeholder="Search">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
                </form>
              </div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h4 class="hs_heading">Start a new discussion</h4>
                    <div class="hs_margin_30 clearfix"></div>
                      {!! Form::open(['files' => true]) !!}
                        <div class="hs_single_profile_detail">
                          <div class="row">
                              <div class="col-md-12">
                                Title: <br>
                                {!! Form::input('text','title' ,null, [ 'required' , 'class' => 'form-control' ]) !!}
                                <div class="voffset4 clearfix"></div>
                                Category: <br>
                                {!! Form::select('category_id' , $categories ,1, [ 'class' => 'form-control' ]) !!}
                                <div class="voffset4 clearfix"></div>
                                Content:
                                {!! Form::textarea('content', null, [ 'required', 'class' => 'form-control textarea' ]) !!}
                                <div class="voffset4 clearfix"></div>
                                Attachment: ( Optional )
                                {!! Form::input('file', 'attachment' , null , [ 'class' => 'form-control' ]) !!}
                                {{--Add poll:--}}
                                {{--{!! Form::checkbox('poll', 1 , null , [ 'id' => 'add_poll' , 'autocomplete' => 'off']) !!}--}}
                                <div class="voffset4 clearfix"></div>
                                <div id="poll_form">
                                    <div class="form-group">

                                    </div>
                                    <button class="pull-left" id="add_poll_item">Add a poll item +</button>
                                </div>
                              </div>
                                    {{--<div class="col-lg-6 col-md-6 col-sm-6"> Get connect with him:--}}
                                      {{--<div class="hs_profile_social"><br>--}}
                                        {{--<ul>--}}
                                          {{--<li><a href=""><i class="fa fa-facebook"></i></a></li>--}}
                                          {{--<li><a href=""><i class="fa fa-twitter"></i></a></li>--}}
                                          {{--<li><a href=""><i class="fa fa-linkedin"></i></a></li>--}}
                                          {{--<li><a href=""><i class="fa fa-skype"></i></a></li>--}}
                                        {{--</ul>--}}
                                      {{--</div>--}}
                                    {{--</div>--}}
                              </div>
                                    <div class="voffset4 clearfix"></div>
                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                        {!! Form::close() !!}
                        </div>
                    <div class="clearfix"></div>
                </div>

                @include('templates.sidebar')
            </div>
        </div>
    </div>
@stop
