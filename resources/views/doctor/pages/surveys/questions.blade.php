<div class="col-md-9 no-padding clearfix">
    {!! Form::open(['route' => 'doctor.surveys.store']) !!}
        {!! Form::hidden('survey_id', $survey->id) !!}
            <div class="col-md-12 survey-single">
                <h3 class="light-blue survey-title">{{ $survey->title }}</h3>

                @if(Session::get('message'))
                    <div class="clearfix alert alert-success">{{ Session::get('message') }}</div>
                @endif

                @if($survey->description)
                    {!! $survey->description !!} <br><br>
                @endif

                @if($survey->description)
                    {!! $survey->disclaimer !!} <br><br>
                @endif

                @foreach ($survey->items as $index=>$item)
                    <div class="well clearfix form-group col-md-12 padding-10">
                        <div class="fields">
                            {{ $item->value }}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="fields">
                            @if(count($item->choices) > 0)
                                @foreach($item->choices as $indexIn=>$choice)

                                    <?php $checked = in_array($choice->id, $entry_ids); ?>

                                    @if($item->allow_multiple)
                                        {!! Form::checkbox('answers['. $item->id .'][]' , $choice->id, $checked , ['id' => 'check'.$indexIn.$index , 'class' => '']) !!}
                                        <label for="check{{ $indexIn.$index }}"><span></span></label>
                                    @else
                                        {!! Form::radio('answers['. $item->id .'][]', $choice->id, $checked, ['id' => 'radio'.$indexIn.$index , 'class' => 'choice-radio']) !!}
                                        <label for="radio{{ $indexIn.$index }}"><span></span></label>
                                    @endif

                                    {{ $choice->value }}<br>
                                @endforeach
                            @else
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 voffset2 clearfix"></div>
                @endforeach
    {!! Form::submit('Save' ,  ['class' => 'button light-blue font-22']) !!}
    {!! Form::close() !!}
            </div>
</div>