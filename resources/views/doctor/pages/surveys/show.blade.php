@extends('doctor.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">

                @include ('doctor.partials._news-ticker')
                <div class="col-md-12 col-sm-12 col-xs-12 padding-b-0  padding-t-0 clearfix margin-b-4 margin-t-0">
                    <div class="row">
                        <hr class="margin-b-5 margin-t-0">
                        <h3 class="no-border light-blue hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-title light-blue margin-t-0">{{ count($selected->therapies) ? $selected->therapies[0]->name : '' }} Surveys</span>
                        </h3>

                        <a class="pull-right light-blue font-15 action-title" href="{{ route('doctor.surveys.index') }}">Back</a>
                    </div>
                </div>
                <hr class="margin-b-7 margin-t-4">

                    <div class="col-md-3 col-sm-12 col-xs-12 no-padding pull-left  clearfix">
                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left gray-bg margin-b-20 padding-tb-10">
                            <div class="row">
                                <ul class="surveys-list">
                                    @if ($surveys->count())
                                        @foreach ($surveys as $survey)
                                             <li class="discussion clearfix height-auto">
                                                <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                                                     <h4 class="{{ $survey->id == $selected->id ? 'active' : '' }} media-heading clearfix">
                                                         @if($survey->id != $selected->id)
                                                             <a href="{{ route('doctor.surveys.show', $survey->slug) }}" class="pull-left">
                                                               {{ $survey->title }}
                                                             </a>
                                                         @else
                                                            {{ $survey->title }}
                                                        @endif
                                                     </h4>
                                                </div>
                                             </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                
                    @include ('doctor.pages.surveys.questions', ['survey' => $selected])

                    <div class="clearfix"></div>
                </div>

                @include('doctor.templates.sidebar-ads')
            </div>
        </div>
    </div>
@stop