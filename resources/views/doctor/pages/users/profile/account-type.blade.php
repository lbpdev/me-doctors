<div class="col-md-12">
    <div class="row">
        <hr class="margin-tb-5">
        <h1 class="pink margin-t-7">Account Type</h1>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 no-padding">
                    <img src="{{ asset('public/images/icon/doctors/mac.png') }}">
                </div>
                <div class="col-md-6 padding-l-0">
                    <div class="col-md-12 no-padding">
                        <table>
                            <tr><td class="padding-10">
                                <input type="radio" name="account[]" value="1"  data-target="#premium" data-toggle="modal" class="gray" id="radioPrem">
                                <label for="radioPrem"><span></span></label>
                            </td><td class="gray-bg padding-10 width-full">
                                <h2 class="pink margin-tb-0">Premium</h2>
                                <h4 class="margin-t-4 margin-b-0">Benefits</h4>
                                <ul class="padding-l-20 benefits t-black">
                                    <li>Ediossit rerferero ma cus</li>
                                    <li>Ediossit rerferero ma cus</li>
                                    <li>Ediossit rerferero ma cus</li>
                                    <li>Ediossit rerferero ma cus</li>
                                </ul>
                            </td></tr>
                        </table>
                    </div>
                    <div class="col-md-12 no-padding margin-t-10">
                        <table>
                            <tr><td class="padding-10">
                                <input type="radio" checked name="account[]" value="1" class="gray" id="radioReg">
                                <label for="radioReg"><span></span></label>
                            </td><td class="padding-10 width-full">
                                <h2 class="margin-tb-0">Regular</h2>
                                <h4 class="margin-t-4 margin-b-0">Benefits</h4>
                                <ul class="padding-l-20 benefits t-black">
                                    <li>Ediossit rerferero ma cus</li>
                                    <li>Ediossit rerferero ma cus</li>
                                </ul>
                            </td></tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
