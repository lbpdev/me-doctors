@extends('doctor.master')

@section('style')
@stop

@section('content')
    <div class="hs_page_title">
      <div class="container profile">
        <h3><i class="fa fa-edit"></i> Edit Profile</h3>
      </div>
    </div>

    <div class="container profile">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="hs_single_profile">
          {!! Form::open(['files' => true]) !!}
            <div class="hs_single_profile_detail">
              <h3>
                {{ $user->fname.' '.$user->lname }} ( {{ $user->username }} )
                <span class="h-link">[ <a href="{{ route('doctor.profile', $user->username) }}">Cancel Changes</a> ]</span>
              </h3>
              <div class="row">
                  @if(Session::has('error'))
                       <div class="col-md-12">
                        <div class="alert alert-danger">
                          {{ Session::get('error') }}
                        </div>
                      </div>
                  @elseif(Session::has('message'))
                       <div class="col-md-12">
                         <div class="alert alert-success">
                             {{ Session::get('message') }}
                         </div>
                       </div>
                  @endif
                  <div class="col-md-4">
                    <img src="{{ asset($user->avatar ) }}" alt=""  id="blah" width="100%"/>
                    {!! Form::input('file', 'photo' , null , [ 'accept' => 'image/*', 'class' => 'form-control margin-b-10' , 'id' => 'imgInp']) !!}

                    @if($user->avatar)
                        Remove Avatar: {!! Form::checkbox('remove_thumb' ,1, false , [ 'class' => 'square' , 'id' => 'avatar_check']) !!}
                        <label for="avatar_check"><span></span></label>
                    @endif
                  </div>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                          <h4><i class="fa fa-user"></i> Account Details:</h4>
                        <div class="col-md-6 no-padding">
                            First Name : <br><input type="text" name="fname" value="{{ $user->fname }}" required class="form-control">
                                <br>
                        </div>
                        <div class="col-md-6 padding-r-0">
                            Last Name : <br><input type="text" name="lname" value="{{ $user->lname }}" required class="form-control">
                                <br>
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="col-md-6 no-padding">
                            Email : <br><input type="text" name="email" value="{{ $user->email }}" required class="form-control">

                        </div>

                        <div class="col-md-6 padding-r-0">
                            Username : <br><input type="text" name="username" value="{{ $user->username }}" class="form-control">
                        </div>
                        {{--Show e-mail address to public :--}}

                        {{--{!! Form::checkbox('email_address' , 1 , $user->show_email ) !!}--}}
                      </div>
                      <div class="col-md-8 margin-t-20">
                        <div class="col-md-6 no-padding">
                            Designation : <br><input type="text" name="designation" value="{{ $user->designation }}" required class="form-control">

                        </div>
                        <div class="col-md-6 padding-r-0">
                            Therapy Area(s) : <br>
                            <select name="therapies[]" multiple="multiple" class="form-control margin-b-5" required>
                                @foreach($therapies as $index=>$therapy)
                                    <option {{ (count($user->specialties)<1 && $index==17) ? 'selected' : '' }} value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                @endforeach

                                @foreach($user->specialties as $specialty)
                                    @foreach($therapies as $therapy)
                                        @if($specialty->id == $therapy['id'])
                                            <option selected value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                        @endif
                                    @endforeach
                                @endforeach

                            </select>
                            {!! $errors->first('therapies','<span class="help-block">:message</span>') !!}
                            <div class="help-block with-errors"></div>
                        </div>
                      </div>
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 margin-t-20">
                      <hr>
                          <h4><i class="fa fa-phone"></i> Contact Number:</h4>
                          <div class="col-md-6 no-padding">
                                Personal : <br><input type="text" name="phone_number[0][personal]" value="{{  $user->personalPhone ? $user->personalPhone->number : '' }}" class="form-control">
                          </div>
                          <div class="col-md-6 padding-r-0">
                                Office : <br><input type="text" name="phone_number[0][work]" value="{{  $user->personalPhone ? $user->personalPhone->number : '' }}" class="form-control">
                          </div>
                      </div>

                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 margin-t-20">
                        New Password <span class="font-10">( Leave blank to keep old one )</span> :
                        <br>
                        {!! Form::password('password' , [ 'placeholder' => 'Password...', 'data-minlength' => '6', 'class' => 'form-control password-peeker' , 'id' => 'inputPassword']) !!}
                      </div>
              </div>

              <div class="row margin-t-20">
                  <div class="col-md-12">
                    <h4><i class="fa fa-user"></i> Professional Summary</h4>
                        <textarea  id="word_count" name="bio" class="form-control textarea"> <?php  if(isset($user->profile->bio)){ echo $user->profile->bio; } ?> </textarea>

                        Total word Count : <span id="display_count">0</span> words. Words left : <span id="word_left">300</span><br>
                    <p>Professional Summary (your professional medical summary will only be visible to other registered physicians on Middle East Doctor)</p>
                  </div>
                  </div>
            <hr>

            {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
              </div>
            </div>

      {!! Form::close() !!}
          </div>
        </div>

      <!--Our Doctor Team end-->

      <div class="hs_margin_40"></div>
    </div>
@stop

@section('js')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('public/js/dist/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('public/js/custom/trumbowyg.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/hideShowPassword.min.js') }}"></script>
    <script src="{{ asset('public/js/custom/password-peeker.js') }}"></script>
    <script type="text/javascript">
      $('select').select2();

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah').attr('src', e.target.result);
              }

              reader.readAsDataURL(input.files[0]);
          }
      }

      $("#imgInp").change(function(){
          readURL(this);
      });

        $(".trumbowyg-editor").bind("paste", function(e){
              var words = $.trim($(this).text()).length ? $(this).text().match(/\S+/g).length : 0;
              if (words <= 300) {
                  $('#display_count').text(words);
                  $('#word_left').text(300-words);
              } else {
                  expString = $(this).text().split(/\s+/,300);
                  theNewString=expString.join(" ");

                  $(this).html(theNewString);

              }
          } );

        $(".trumbowyg-editor").on('keydown', function(e) {
            var words = $.trim($(this).text()).length ? $(this).text().match(/\S+/g).length : 0;
            if (words <= 300) {
                $('#display_count').text(words);
                $('#word_left').text(300-words);
            } else {
                if (e.which !== 8) e.preventDefault();
            }
        });

        $(".trumbowyg-editor").trigger('keydown');
    </script>
@stop