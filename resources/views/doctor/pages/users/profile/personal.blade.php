<div class="hs_single_profile_detail padding-b-5 clearfix">
<hr class="margin-b-3 margin-t-0">
    <h3 class="no-border blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
        <span class="page-icon doctor"></span>
        <span class="page-title pink">Doctor Directory</span>
    </h3>
<hr class="margin-tb-5">
<div class="row">
    <div class="col-md-3 col-sm-3  col-xs-3 col-xxs-12 padding-b-0">
        @if($user->isPremium == 1)
            <span class="featured-ribbon pink doctor"></span>
        @endif
        <img src="{{ asset($user->avatar) }}" alt="{{ $user->name }}" width="100%"/>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-9 col-xxs-12 ">
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0margin-tb-0 padding-tb-0">
            <div class="row">
                <h3 class="margin-t-7 margin-tb-0">
                    {{ $user->name }}
                    @if(isset($authUser) && $authUser->username == $user->username)
                        <a href="{{ route('edit_personal_profile', $user->username) }}">
                            <button class="button t-light-gray">[ <i class="fa fa-edit"></i> EDIT ]</button>
                        </a>
                    @endif
                </h3>
                @if($user->isPremium)
                    ( Premium service expires on : {{ $user->currentService->expires_on->format('m-d-Y') }} )
                @endif
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <h4 class="margin-tb-5">
                  @foreach($user->specialties as $index=>$specialty)
                      {{ $specialty->name }}{{  ($index+1) < count($user->specialties) ? ', ' : '' }}
                  @endforeach
              </h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <h5 class="no-margin">
              @foreach($user->hospitals as $index=>$hospital)
                  @if($index==0)
                    {{ $hospital->name }}
                  @endif
              @endforeach
              </h5>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <h5 class="no-margin">
              @foreach($user->hospitals as $index=>$hospital)
                  @if($index==0 && isset($hospital->location->name))
                    {{ isset($hospital->location) ? $hospital->location->name : 'N/A' }}
                  @endif
              @endforeach
              </h5>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row padding-b-4">
              @foreach($user->hospitals as $index=>$hospital)
                  @if($index==0)
                    <a href="http://www.{{ $hospital->website }}">{{ $hospital->website }}</a>
                  @endif
              @endforeach
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <p class="no-margin">
              @if(count($user->hospitals))
                  @foreach($user->hospitals as $index=>$hospital)
                  @if($hospital->schedules)
                  <?php $days = 0; ?>
                  Timings :
                      @for($x=0;$x<7;$x++)
                      @if( in_array( $x, $hospital->schedules ) )
                      <?php $days++; ?>

                          @if($x==0)     <i>Sun</i>
                          @elseif($x==1) <i>Mon</i>
                          @elseif($x==2) <i>Tue</i>
                          @elseif($x==3) <i>Wed</i>
                          @elseif($x==4) <i>Thu</i>
                          @elseif($x==5) <i>Fri</i>
                          @elseif($x==6) <i>Sat</i>
                          @endif
                       @if($days<count($hospital->schedules)) {{' , '}}
                       @endif
                      @endif
                  @endfor
                  @endif
                  @endforeach
               @endif
              </p>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 margin-tb-0 padding-tb-0">
                <div class="row line-height-22">
                    @if ($user->contacts)
                        @if($user->workPhone)
                            @if($user->workPhone->number)
                                <p class="text-capitalize">
                                    <span class="small-icon phone"></span>
                                    {{ $user->workPhone->number }} (Office)
                                </p>
                            @endif
                        @endif

                        @if($user->personalPhone )
                            @if($user->personalPhone->number)
                                <p class="text-capitalize">
                                    <span class="small-icon phone"></span>
                                    {{ $user->personalPhone->number }} ({{ $user->personalPhone->type }})
                                </p>
                            @endif
                        @endif
                    @else
                        <p>No phone number.</p>
                    @endif
                </div>
            </div>
        </div>


        <hr class="margin-tb-5">

        @if($authUser->username != $user->username)

        <a class="pink" href="#" data-toggle="modal" data-target="#ratingModal">Rate Doctor</a><br>
        <div class="clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
                <div class="row line-height-22">
                    <span class="doctor-ratings">
                        <i class="rate-icon ri-friendliness"></i> <span class="friendliness">{{ $user->friendlinessRating }}</span>
                        <i class="rate-icon ri-professionalism margin-l-10"></i> <span class="professionalism">{{ $user->professionalismRating }}</span>
                        <i class="rate-icon ri-bed-manner margin-l-10"></i> <span class="bedside">{{ $user->bedsideMannerRating }}</span>
                        <i class="rate-icon ri-medical-knowledge"></i> <span class="medical">{{ $user->medicalKnowledgeRating }}</span>
                        <i class="rate-icon ri-helpfulness margin-l-10"></i> <span class="helpfulness">{{ $user->helpfulnessRating }}</span>
                        <i class="rate-icon ri-waiting-time"></i> <span class="waiting">{{ $user->waitingTimeRating }}</span>
                        <a href="#" class="info-icon" data-toggle="modal" data-target="#ratingHelpModal"></a>
                    </span>
                </div>
            </div>
        </div>
        <hr class="margin-tb-5">
        @endif

        @if(Session::has('message'))
            <span class="alert alert-success pull-left margin-t-10">{{ Session::get('message') }}</span>
        @endif
    </div>
</div>
