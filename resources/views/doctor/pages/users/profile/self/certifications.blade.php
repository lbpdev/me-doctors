<div class="section col-md-12 margin-b-0 padding-l-0">
    <div class="col-md-12 padding-l-0">

    <h4 class="clearfix margin-t-0 margin-b-0">Board Certifications
        @if(isset($authUser) && $authUser->username == $user->username)
            <button class="button t-light-gray add-toggle">[ <i class="fa fa-edit"></i> EDIT ]</button>
        @endif
    </h4>

        <div id="certifications" class="dataList">
        @if(isset($user->certifications ) && count($user->certifications) > 0 )
            @foreach ($user->certifications as $certification)
                <span>{{ $certification->name }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>

        <div class="alert alert-success clearfix temp-hide"> Successfully Updated.</div>
    @if($authUser->username == $user->username)
    </div>
    <div class="add-form clearfix padding-l-0 well-sm col-md-12">
        <h5><i class="fa fa-plus-square-o"></i> Update Certifications</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.certifications.update', $user->username]]) !!}
    @else
        {!! Form::open(['method' => 'PUT', 'route' => ['doctor.certifications.updateIE', $user->username]]) !!}
    @endif
            <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">

                @if(isset($user->educations))
                    @foreach ($user->certifications as $index => $certification)
                        <input type="text" name="certifications[{{$index}}][name]" placeholder="Certification" class="margin-b-5 form-control" value="{{ $certification->name }}">
                    @endforeach
                @endif

                <input type="text" name="certifications[{{ isset($user->educations) ? count($user->educations) : 0 }}][name]" placeholder="Certification" class="margin-b-5 form-control">
            </div>

            <button href="#" class="button pull-right add_field_bt margin-l-10 fa fa-plus"></button>
            <button href="#" rel="1" class="button pull-right remove_field_bt fa fa-minus"></button>


            <button class="button add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button type="submit" class="button add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
    @endif
    </div>
</div>