<div class="section col-md-12 margin-b-7">
    <div class="row">

    <h4 class="clearfix margin-t-0 margin-b-0">Location
        @if(isset($authUser) && $authUser->username == $user->username)
            <button class="button t-light-gray add-toggle">[ <i class="fa fa-edit"></i> EDIT ]</button>
        @endif
    </h4>
        <div id="works" class="dataList">
        @if(isset($user->hospitals) && count($user->hospitals) > 0)
            @foreach ($user->hospitals as $hospital)
            <?php $days = 0; ?>
                <span><b>{!! $hospital->name ? $hospital->name : '' !!}</b></span><br>
                <span>{!! $hospital->location ? $hospital->location->name : '' !!}</span><br>
                <span><b>Schedule :</b>
                    @for($x=0;$x<7;$x++)
                        @if( in_array( $x, $hospital->schedules ) )
                        <?php $days++; ?>
                            @if($x==0)     <i>Sun</i>
                            @elseif($x==1) <i>Mon</i>
                            @elseif($x==2) <i>Tue</i>
                            @elseif($x==3) <i>Wed</i>
                            @elseif($x==4) <i>Thu</i>
                            @elseif($x==5) <i>Fri</i>
                            @elseif($x==6) <i>Sat</i>
                            @endif
                         @if($days<count($hospital->schedules)) {{' , '}} @endif
                        @endif
                    @endfor
                </span>
                <hr style="float:none;margin:4px 0">
            @endforeach
        @else
            N/A
        @endif
        </div>

        <div class="alert alert-success  temp-hide clearfix pull-left margin-t-0"> Successfully Updated.</div>
    </div>

    <div class="add-form clearfix  padding-l-0 well-sm col-md-12  row">
        <h5><i class="fa fa-plus-square-o"></i> Update Work</h5>
    @if($agent != "Internet Explorer")
        {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.work.update', $user->username]]) !!}
     @else
         {!! Form::open(['method' => 'PUT', 'route' => ['doctor.work.updateIE', $user->username]]) !!}
     @endif
     <div class="alert alert-danger clearfix width-full temp-hide"> An error has occurred. Please try again.</div>

            <div class="fields-group">
                @foreach ($user->hospitals as $index=>$hospital)
                <div class="fields padding-b-5 border-bottom clearfix">
                    <span class="fa fa-close pull-right margin-b-10"></span>
                    <div class="col-md-6 padding-l-0">
                        <input required type="text" value="{{ $hospital->name }}" name="work[{{ $index }}][name]" placeholder="Company Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                        {!! Form::input('text','work['.$index.'][location]' , $hospital->location ? $hospital->location->name : '', [ 'required', 'class' => 'form-control location' , 'id' => 'location' , 'placeholder' => 'Address']) !!}
                        <br>
                        <table id="address" width="100%">
                            <tr><td>Street</td><td><input value="{{ $hospital->location ? $hospital->location->street_number : '' }}" name="work[{{ $index }}][street_number]" class=" form-control field street_number margin-tb-5" id="street_number"></td></tr>
                            <tr><td>Route</td><td><input value="{{ $hospital->location ? $hospital->location->street_route : '' }}" name="work[{{ $index }}][street_route]" class="form-control field route margin-tb-5" id="route"></td></tr>
                            <tr><td>City</td><td><input value="{{ $hospital->location ? $hospital->location->city : '' }}" name="work[{{ $index }}][locality]" class="form-control field locality margin-tb-5" id="locality"></td></tr>
                            <tr><td>State</td><td><input value="{{ $hospital->location ? $hospital->location->state : '' }}" name="work[{{ $index }}][state]" class="form-control field administrative_area_level_1 margin-tb-5" id="administrative_area_level_1"></td></tr>
                            <tr><td>Postal Code</td><td><input value="{{ $hospital->location ? $hospital->location->post_code : '' }}" name="work[{{ $index }}][post_code]" class="form-control field postal_code margin-tb-5" id="postal_code"></td></tr>
                            <tr><td>Country</td><td><input value="{{ $hospital->location ? $hospital->location->country : '' }}" name="work[{{ $index }}][country]" class="form-control field country margin-tb-5" id="country"></td></tr>
                        </table>
                    </div>

                    <div class="col-md-6 no-padding">
                        <div class="map">
                            <p>Location not found on Map</p>
                        </div>
                    </div>

                    <div class="col-md-12 row margin-t-5">
                        <b>Work days: </b>
                        @for($x=0;$x<7;$x++)
                            <br class="xxs-show temp-hide">
                            <span class="day-check">
                                @if( in_array( $x, $hospital['schedules'] ) )
                                    <input class="square" checked name="work[{{$index}}][days][{{$x}}][day]" value="{{$x}}" type="checkbox" id="{{$index}}{{$x}}" />
                                @else
                                    <input class="square" name="work[{{$index}}][days][{{$x}}][day]" value="{{$x}}" type="checkbox" id="{{$index}}{{$x}}" />
                                @endif
                                @if($x==0) <label for="{{$index}}{{$x}}"><span></span>Sun</label>
                                @elseif($x==1) <label for="{{$index}}{{$x}}"><span></span>Mon</label>
                                @elseif($x==2) <label for="{{$index}}{{$x}}"><span></span>Tue</label>
                                @elseif($x==3) <label for="{{$index}}{{$x}}"><span></span>Wed</label>
                                @elseif($x==4) <label for="{{$index}}{{$x}}"><span></span>Thu</label>
                                @elseif($x==5) <label for="{{$index}}{{$x}}"><span></span>Fri</label>
                                @elseif($x==6) <label for="{{$index}}{{$x}}"><span></span>Sat</label>
                                @endif
                            </span>
                        @endfor
                    </div>
                </div>
                @endforeach

                @if(count($user->hospitals)==0)
                <div class="fields padding-b-5 border-bottom clearfix">
                    <span class="fa fa-close pull-right margin-b-10"></span>
                    <div class="col-md-6 padding-l-0">
                    <input type="text" required name="work[{{$user->hospitals->count()}}][name]" placeholder="Company Name" id="workname" autocomplete="false"  class="work-name margin-b-5 margin-t-5 form-control">
                    {!! Form::input('text','work['.$user->hospitals->count().'][location]' , null, [ 'required', 'class' => 'form-control location' , 'id' => 'location' , 'placeholder' => 'Address']) !!}
                    <br>
                        <table id="address" width="100%">
                            <tr><td>Street</td><td><input name="work[{{$user->hospitals->count()}}][street_number]" class="field street_number form-control margin-tb-5" id="street_number"></td></tr>
                            <tr><td>Route</td><td><input name="work[{{$user->hospitals->count()}}][street_route]" class="field route form-control margin-tb-5" id="route"></td></tr>
                            <tr><td>City</td><td><input name="work[{{$user->hospitals->count()}}][locality]" class="field locality form-control margin-tb-5" id="locality"></td></tr>
                            <tr><td>State</td><td><input name="work[{{$user->hospitals->count()}}][state]" class="field administrative_area_level_1 form-control margin-tb-5" id="administrative_area_level_1"></td></tr>
                            <tr><td>Post Code</td><td><input name="work[{{$user->hospitals->count()}}][post_code]" class="field postal_code form-control margin-tb-5" id="postal_code"></td></tr>
                            <tr><td>Country</td><td><input name="work[{{$user->hospitals->count()}}][country]" class="field country form-control margin-tb-5" id="country"></td></tr>
                        </table>
                    </div>

                    <div class="col-md-6 no-padding">
                        <div class="map">
                            <p>Location not found on Map</p>
                        </div>
                    </div>

                        <div class="col-md-12 margin-t-10">
                            <div class="row">
                                <span class="day-check">
                                    <input class="square" name="work[{{$user->hospitals->count()}}][days][0][day]" value="0" type="checkbox" id="day1a" />
                                    <label for="day1a"><span></span>Sun</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][1][day]" value="1" type="checkbox" id="day2a" />
                                    <label for="day2a"><span></span>Mon</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][2][day]" value="2" type="checkbox" id="day3a" />
                                    <label for="day3a"><span></span>Tue</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][3][day]" value="3" type="checkbox" id="day4a" />
                                    <label for="day4a"><span></span>Wed</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][4][day]" value="4" type="checkbox" id="day5a" />
                                    <label for="day5a"><span></span>Thu</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][5][day]" value="5" type="checkbox" id="day6a" />
                                    <label for="day6a"><span></span>Fri</label>
                                </span>
                                <br class="xxs-show temp-hide">
                                <span class="day-check">
                                    <input class="square"  name="work[{{$user->hospitals->count()}}][days][6][day]" value="6" type="checkbox" id="day7a" />
                                    <label for="day7a"><span></span>Sat</label>
                                </span>
                            </div>
                        </div>
                </div>
                @endif
            </div>


            <button class="button add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
            <button class="button add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
        {!! Form::close() !!}
        @if($user->isPremium == 1)
            <button class="button pull-right add_field_group_bt margin-l-10  margin-t-4  fa fa-plus"></button>
            <button rel="1" class="button pull-right remove_field_group_bt margin-t-4 fa fa-minus"></button>
        @endif
    </div>
</div>
</div>
</div>
@section('js')
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/profileFields.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script type="text/javascript">
      $('select').select2();
    </script>


    @if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== true)

        <script type="text/javascript">

                $( ".dataForm" ).submit(function( event ) {

                  var $form = $( this ),
                    data = $form.serialize(),
                    url = $form.attr( "action" );
                    $.ajax({
                      type: "POST",
                      url: url,
                      data: { formData : data },
                      beforeSend: function (xhr) {
                         var token = $('meta[name="csrf_token"]').attr('content');

                         if (token) {
                               return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                         }
                      },
                      success: function(results){
                          var data = JSON.parse(results);
                          if(data.length > 0){
                              if(data[0].location === undefined )
                                updateSingle($(event.target), data);
                              else
                                updateMulti($(event.target), data);
                          } else {
                            $(event.target).closest('.section').find('.dataList').empty();
                            toggleForms($(event.target));
                          }
                      },
                      error: function(xhr, status, error) {
                          $(event.target).closest('.section').find('.alert-danger').show().delay(3000).fadeOut(3000);
                      }
                    });
                  event.preventDefault();
                });
////////////////////////////////////////
                // UPDATE HTML WITH NEW DATA
                //////////////////////////////////////////

                function toggleForms(target){
                    target.closest('.add-form').toggle();
                    target.closest('.section').find('.add-toggle').toggle();
                }

                function updateSingle(target, data){
                  target.closest('.section').find('.alert-success').show().delay(3000).fadeOut(3000);
                    var holder = target.closest('.section').find('.dataList');
                    holder.empty();

                    if(data){
                        $.each(data, function(i, currProgram) {
                            console.log(data[i]);
                            holder.append('<span>'+data[i]+'</span><br>');
                        });
                    }
                    toggleForms(target);
                    event.preventDefault();
                }

                ////////////////////////////////////////
                            // UPDATE HTML WITH NEW DATA
                            //////////////////////////////////////////

                function updateMulti(target, data){

                  console.log(data);
                  var days = ['Sun' , 'Mon' , 'Tue' , 'Wed' , 'Thu' , 'Fri' , 'Sat'];
                  target.closest('.section').find('.alert-success').show().delay(3000).fadeOut(3000);
                    var holder = target.closest('.section').find('.dataList');
                    holder.empty();

                    if(data){
                        $.each(data, function(i, currProgram) {
                            holder.append('<b>'+data[i]['name']+'</b><br>');
                            holder.append('<span>'+data[i]['location']+'</span>');


                            if(data[i]['schedules']){
                                holder.append('<br><b>Schedule:</b> ');
                                var daysString = "";
                                data[i]['schedules'].forEach(function(entry) {
                                    daysString += days[entry]+' , ';
                                });

                                daysString = daysString.substring(0, daysString.length-2);

                                holder.append(daysString);
                            }
                            holder.append('<hr style="float:none;margin:4px 0">');
                        });
                    }

                    toggleForms(target);

                    $('html,body').animate({scrollTop: target.closest('.section').offset().top - 50});
                    event.preventDefault();
                }

          function initMap(index) {
            var map = new google.maps.Map(document.getElementsByClassName('map')[index], {
                zoom: 12,
                disableDefaultUI: true,
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map , index);
          }

          function geocodeAddress(geocoder, resultsMap ,index) {
            var address = $('input.location')[index].value;
            var map = $('.map')[index];
            if(address=="")
                address = "Dubai - United Arab Emirates";

            geocoder.geocode({'address': address}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                $(map).find('.gm-style').show();
                console.log('meron');
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
              } else {
                console.log('wala');
                $(map).hide();
              }
            });
          }

        ////////////////////////////////////////
                    // BIND THE AUTOCOMPLETE EVENT TO ALL FIELDS WITH CLASS ''
                    //////////////////////////////////////////

        function autoCompleteBind(){
            $(".work-name").autocomplete({
                source    : <?php echo json_encode($hospitals); ?>,
                autoFocus : true,
                select: function(event, ui){
                    $(event.target).parent().find('#address input').val();
                    $(event.target).parent().find('.location').val(ui.item.location);
                    $(event.target).parent().find('.street_number').val(ui.item.street_number);
                    $(event.target).parent().find('.route').val(ui.item.street_route);
                    $(event.target).parent().find('.postal_code').val(ui.item.post_code);
                    $(event.target).parent().find('.locality').val(ui.item.city);
                    $(event.target).parent().find('.administrative_area_level_1').val(ui.item.state);
                    $(event.target).parent().find('.country').val(ui.item.country);
                    var target = $(event.target).closest('.fields');

                    initMap($('.fields-group .fields').index(target));
                }
            });

            $(".work-name").on( "focus", function( event ) {
                $(event.target).autocomplete( "search", " " );
            });
        }
        autoCompleteBind();
        </script>
    @endif

@stop