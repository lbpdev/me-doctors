@if(isset($user->hospitals) && count($user->hospitals) > 0)
    @foreach ($user->hospitals as $index=>$hospital)
        @if(isset($hospital->location))
        <h1 class="pink {{ $index == 0 ? 'margin-t-20' : 'margin-t-0' }} margin-b-0 clearfix pull-left width-full">{{ $index == 0 ? 'Primary Location' : 'Other Locations' }}</h1>
           <div class="section col-md-12 margin-b-7">
               <div class="row">
                   <div id="works" class="dataList">
                        <div class="fields padding-b-5 clearfix">
                            <div class="col-md-12 no-padding">
                                <div data-location="{{ $hospital->location->name }}" class="map" style="height: 300px">
                                </div>
                            </div>
                        </div>
                        <hr class="margin-tb-5">
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@else
    N/A
@endif


</div>
</div>

@section('js')
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script type="text/javascript">

          function initMap() {
          $('.map').each(function(e , item){
              var map = new google.maps.Map(item, {
                  zoom: 6,
                  disableDefaultUI: true,
                  draggable: true,zoomControl: true,scrollwheel: true,disableDoubleClickZoom: true
              });
              var geocoder = new google.maps.Geocoder();

              geocodeAddress(geocoder, map , item);
          });
        }

        function geocodeAddress(geocoder, resultsMap ,item) {
          var address = $(item).attr('data-location');

          if(address=="")
              address = "Dubai - United Arab Emirates";

          geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              resultsMap.setCenter(results[0].geometry.location);
              var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
              });
            } else {
              $(item).hide();
            }
          });
        }
        initMap();

    </script>


@stop