@extends('doctor.master')


@section('content')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <div class="container profile page-container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 border-right">
                <div class="hs_single_profile">

                @include ('doctor.partials._news-ticker')

                    @include ('doctor.pages.users.profile.personal')

                    @if($user->id == 1)

                        @if($authUser->username == $user->username && $authUser->isPremium!=1)
                            @include ('doctor.pages.users.profile.account-type')
                            <hr class="margin-t-10 margin-b-5 pull-left">
                        @endif

                    @endif

                    <div class="col-md-12">
                        <div class="row">
                        @if(
                            count($user->educations) ||
                            count($user->awards) ||
                            count($user->publications) ||
                            count($user->languages) ||
                            count($user->certifications)
                        )
                            <hr class="margin-tb-5">
                            <h1 class="pink margin-t-7">Qualifications</h1>
                        @endif

                            <div class="col-md-12 qualifications">
                                <div class="row">

                                @if($authUser->username == $user->username)

                                <!---------- USER IS VIEWING OWN PROFILE ------------>

                                    @include ('doctor.pages.users.profile.self.education')

                                    @include ('doctor.pages.users.profile.self.awards')

                                    @include ('doctor.pages.users.profile.self.publications')

                                    @include ('doctor.pages.users.profile.self.languages')

                                    @include ('doctor.pages.users.profile.self.certifications')

                                    @include ('doctor.pages.users.profile.self.bio')
                                    <hr class="margin-t-10 margin-b-5 pull-left">

                                        @include ('doctor.pages.users.profile.self.work')

                                        <hr class="margin-t-10 margin-b-5 pull-left">-
                                        <h1 class="pink margin-t-20 margin-b-0 clearfix pull-left width-full">KOL</h1>
                                        <div class="section col-md-12 margin-b-7">
                                            <div class="row">
                                                @if(!$user->kol)
                                                    Would you like to be considered as a KOL and possibly contribute to a medical journal?
                                                    <a href="{{ route('doctor.kolize') }}"><b>Click here if yes.</b></a>
                                                @else
                                                    You have agreed to be considered as a KOL and have been added to our list of KOL users.
                                                @endif
                                            </div>
                                        </div>

                                @else

                                <!---------- USER IS VIEWING OTHER USER'S PROFILE ------------>

                                @if(count($user->educations) > 0 )
                                    @include ('doctor.pages.users.profile.education')
                                @endif

                                @if(count($user->awards) > 0 )
                                    @include ('doctor.pages.users.profile.awards')
                                @endif

                                @if(count($user->publications) > 0 )
                                    @include ('doctor.pages.users.profile.publications')
                                @endif

                                @if(count($user->languages) > 0 )
                                    @include ('doctor.pages.users.profile.languages')
                                @endif

                                @if(count($user->certifications) > 0 )
                                    @include ('doctor.pages.users.profile.certifications')
                                @endif

                                    @include ('doctor.pages.users.profile.bio')
                                    <hr class="margin-t-10 margin-b-5 pull-left">

                                    @include ('doctor.pages.users.profile.work')
                                @endif

                                </div>
                            </div>
                    {{--@endif--}}
                        </div>
                    </div>
                    </div>
                    @include('doctor.templates.sidebar-ads')
               </div>

        </div>
    </div>

    <div class="hs_margin_40"></div>

    @include('doctor.templates.modals.doctor-premium-form')
    @include('patient.templates.modals.rating-help')
    @include('patient.templates.modals.rating')
@stop

@section('js-alt')
    <script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
    <script>
        // Called when token created successfully.
        var successCallback = function(data) {
            var myForm = document.getElementById('myCCForm');
            console.log(data.response.token.token);
            // Set the token as the value for the token input
            myForm.token.value = data.response.token.token;

            // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
            myForm.submit();
        };

        // Called when token creation fails.
        var errorCallback = function(data) {
            // Retry the token request if ajax call fails
            if (data.errorCode === 200) {
                // This error code indicates that the ajax call failed. We recommend that you retry the token request.
            } else {
                alert(data.errorMsg);
            }
        };

        var tokenRequest = function() {
            // Setup token request arguments
            var args = {
                sellerId: "901301595",
                publishableKey: "C1A0F6B1-5CAF-4175-A012-B07962549E83",
                ccNo: $("#ccNo").val(),
                cvv: $("#cvv").val(),
                expMonth: $("#expMonth").val(),
                expYear: $("#expYear").val()
            };
            // Make the token request
            TCO.requestToken(successCallback, errorCallback, args);
        };

        $(function() {

            TCO.loadPubKey('sandbox');

            $("#submitCCForm").on('click',function(e) {
                // Call our token request functions
                tokenRequest();

                // Prevent form from submitting
                return false;
            });
        });

    </script>
@endsection

