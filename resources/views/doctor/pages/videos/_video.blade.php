<li class="media clearfix height-auto gray-bg">
   <div class="media-left col-md-4 col-sm-4 col-xs-12 no-padding">
        <a class="fancybox" data-type="iframe" href="{{ $video->url }}" title="youtube">
            @if($video->thumbnail)
                <img src="{{ $video->thumbnail }}" width="100%">
            @endif
        </a>
    </div>
    <div class="media-body height-auto col-md-8 col-sm-8 col-xs-12 padding-tb-7">
        <h4 class="media-heading blue">
            <a class="fancybox" data-type="iframe" href="{{ $video->url }}" title="youtube">
             {{ Str::words($video->title, 9, '...') }}
            </a>
        </h4>

        @if(Str::length($video->title) > 70)
            {{ Str::words(strip_tags($video->description), 15, '...') }}
        @else
            {{ Str::words(strip_tags($video->description), 30, '...') }}
        @endif
   </div>
</li>
