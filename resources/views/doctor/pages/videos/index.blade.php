@extends('doctor.master')

@section('content')
    <div class="container articles-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right ">
                @include ('doctor.partials._news-ticker')
                <hr class="no-margin">

                <h2 class="hs_heading pull-left width-full margin-b-4 t-blue">Videos</h2>

                <div class="col-md-12 no-padding clearfix">
                    <ul class="media-list">
                        @foreach($videos as $video)
                            @include('doctor.pages.videos._video',['video',$video])
                        @endforeach
                    </ul>


                    <?php echo $videos->render(); ?>
                </div>

                </div>

            @include('doctor.templates.sidebar-ads')
            </div>
        </div>
    </div>
@stop


@section('js')
@stop