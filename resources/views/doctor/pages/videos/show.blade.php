@extends('doctor.master')

@section('content')
    <div class="container articles-page page-container single-article">
            <div class="row">
                <div class="col-md-9 col-sm-12 border-right">
                    @include ('doctor.partials._news-ticker')
                    <hr class="no-margin pull-left">
                    {{--<h5><a href="{{ route('doctor.articles.index') }}"><< Back to Articles</a></h5>--}}

                    <h2 class="hs_heading">{{ $news->title }}</h2>
                    <h2 class="hs_subheading">{{ $news->subtitle }}</h2>
                    <div class="hs_margin_30"></div>

                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="post-content left-pad-20">

                            <div class="content padding-t-0">
                                @if(isset($news->FeaturedImage))
                                    <img src="{{ asset( $news->FullImage) }}" width="100%" class="margin-b-10">
                                @endif

                                Published: <em>{{ $news->date_posted->format('F d, Y')  }}</em> <br><br>

                                <div class="fancify">
                                {!! $news->content !!}
                                </div>

                                <div class="col-md-12 margin-t-20">
                                    <div class="row">
                                        <span class='st_facebook_large' displayText='Facebook'></span>
                                        <span class='st_twitter_large' displayText='Tweet'></span>
                                        <span class='st_googleplus_large' displayText='Google +'></span>
                                        <span class='st_linkedin_large' displayText='LinkedIn'></span>
                                        <span class='st_email_large' displayText='Email'></span>
                                    </div>
                                </div>
                            </div>

                            <div class="voffset2 clearfix f-left"></div>

                          </div><!-- End post Content -->
                        </div><!-- /Col -->
                    </div><!-- /Row -->
                </div>

                @include ('doctor.templates.sidebar-ads')
            </div>
        </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
@stop