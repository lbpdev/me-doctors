<div class="panel with-nav-tabs panel-default clearfix latest-tabs">
    <div class="col-md-12 col-sm-12 no-padding">

        @include('doctor.partials._tweets')

        <div class="sidebar-element clearfix">
            @if($authUser)
            @else
                <a href="{{ route('doctor.login') }}">
                    <img src="{{ asset('public/images/ads/register-side.png') }}" width="100%" alt="register med">
                </a>
            @endif
            {{--<a class="margin-t-10 pull-left" target="_blank" href="https://itunes.apple.com/us/app/bodylite/id1031481153?ls=1&mt=8">--}}
                {{--<img src="{{ asset('public/images/ads/BodyLite-Ad.gif') }}" width="100%" alt="bodylite-ad">--}}
            {{--</a>--}}
        </div>

        @include('templates.box-event')
        @include('templates.sidebar-long')  
    </div> <!---- COL-MD-12 ----->
</div>