    <div class="col-md-12 col-sm-12 news-ticker clearfix padding-t-0 padding-b-5 ">
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12 no-padding">
        <div class="pull-left padding-r-10">

        @if(count($news))
            Latest News:
        @else
            <p>No news feeds at the moment.</p>
        @endif

        </div>

        <ul class="pull-left padding-l-0" id="ticker">
        @if(count($news))
            @foreach($news as $feed)
                <li class="news-item">
                    <a href="{{ route('doctor.news.show', $feed['slug']) }}">
                        {{ Str::limit($feed['title'], 65) }}
                    </a>
                </li>
            @endforeach
        @endif
        </ul>

       </div>
        <div class="col-md-3 col-sm-3 col-xs-12 no-padding hide-sm">
            @include('doctor.partials._search')
       </div>
   </div>
</div>
