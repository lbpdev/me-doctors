@if ($poll)

    {!! Form::hidden('poll_id', $poll->id ) !!}
    <div class="panel panel-primary transparent-bg no-margin-bottom">
        <div class="panel-heading">
            <h3 class="panel-title robotolight blue">{{ $poll->title }}</h3>
        </div>
        <div class="panel-body padding-0 transparent-bg margin-t-10">

            <ul class="list-group transparent-bg margin-b-0 padding-r-20">
                @if ($voted = $poll->items->contains('voted', 1))
                    <li class="list-group-item transparent-bg">
                        <label><p>Thank you for voting. You voted for "{{ $poll->items->where('voted', 1)->first()->body }}"</p></label>
                    </li>
                @else
                    @foreach ($poll->items as $index=>$item)
                        <li class="list-group-item transparent-bg no-border">
                            <div class="row">
                                <div class="col-md-2 padding-r-0">
                                    <input type="radio" name="poll_item[]" value="{{ $item->id }}" id="radio{{ $index }}">
                                    <label for="radio{{ $index }}"><span></span></label>
                                </div>
                                <div class="col-md-10 padding-l-0">
                                    <label class="margin-l-5"> {{ $item->body }} </label>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>

        @if (!$voted)
            <div class="panel-footer padding-tb-0 text-center transparent-bg relative">
                @if($authUser)
                    {!! Form::submit('Vote', ['class'=>'btn btn-primary btn-block btn-sm'] ) !!}
                @else
                    <a href="{{ route('doctor.login') }}">Login to Vote.</a>
                @endif
                {{--<a href="#" class="small">View Result</a>--}}
            </div>
        @endif
     </div>
@endif