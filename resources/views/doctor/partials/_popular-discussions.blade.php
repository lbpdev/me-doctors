<div class="panel with-nav-tabs pull-left margin-b-0 panel-default clearfix latest-tabs">
        <h4 class="no-margin padding-5 blue-bg t-white robotolight text-center text-uppercase">Most Popular</h4>
            <div class="col-md-12 no-padding">
                <div class="panel with-nav-tabs panel-default margin-b-7">
                    <div class="">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">MED Talks</a></li>
                            <li><a href="#tab2default" class="text-center" data-toggle="tab">MED Cases</a></li>
                        </ul>
                    </div>
                <div class=" sidebar-element panel-body clearfix">
                    <div class="tab-content clearfix">
                        <div class="tab-pane fade in active" id="tab1default">
                            <ul class="discussions-list clearfix">
                                @if ($discussions->count())
                                    @foreach ($discussions as $index=>$discussion)
                                        <li>
                                            <div class="col-md-1 col-sm-1  col-xs-1 no-padding">
                                                <span class="counts">{{ $index+1 }}</span>
                                            </div>
                                            <div class="col-md-11 col-sm-11 col-xs-11 padding-r-10 padding-l-0">
                                                <a href="{{ route('doctor.discussions.show', $discussion->slug )}}">
                                                {{ Str::limit($discussion->title,53) }}
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                            <ul class="discussions-list clearfix">
                                @if ($diagnosis)
                                    @foreach($diagnosis as $index=>$diagnosisDiscussion)
                                        <li>
                                            <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                                                <span class="counts">{{  $index+1 }}</span>
                                            </div>
                                            <div class="col-md-11 col-sm-11 col-xs-11 padding-r-10 padding-l-0">
                                                <a href="{{ route('doctor.diagnosis.show', $diagnosisDiscussion->slug) }}">
                                                    {{ Str::limit($diagnosisDiscussion->title,53) }}
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>