{!! Form::open(['route'=>'search','method'=>'GET']) !!}
    <div class="search-field">
        {!! Form::input('text' , 'keyword' , null , ['class' => 'form-control']) !!}
        <button type="submit" class="magnify button search-icon"></button>
    </div>
{!! Form::close() !!}