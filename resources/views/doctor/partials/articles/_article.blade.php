<div class="padding-r-20 padding-l-20">
    <div class="hs_event_div">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 border-right gray padding-r-20">
                <div class="">
                    <a href="{{ route('doctor.articles.show', $article->slug) }}">
                    <h4>{!! Str::limit($article->title, 20, '...') !!}</h4>
                    </a>
                     <p>{!! Str::limit(strip_tags($article->content), 48, '...') !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>