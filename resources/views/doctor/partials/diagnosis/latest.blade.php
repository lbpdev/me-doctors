<div class="pull-left width-full latest-diagnosis">
    <div class="col-lg-12 col-md-12 col-sm-12 padding-r-0 padding-l-0">
        <h2 class="section-header">Latest MED Cases</h2>
        @if ($diagnosis->count())
            <div class="ocarousel">
                <div id="owlcarousel" class="owl-carousel owl-theme">
                    @foreach ($diagnosis as $index=>$discussion)
                        @include ('doctor.partials.diagnosis._diagnosis')
                    @endforeach
                </div>
                <div class="customNavigation text-right">
                    <a class="btn_prev white prev"><span class="arrow green left"></span></a>
                    <a class="btn_next white next"><span class="arrow green right"></span></a>
                </div>
            </div>
        @else
            <p>No Discussions</p>
        @endif
    </div>
</div>

@section('js-inner')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
@stop