<div class="padding-r-20 padding-l-20">
    <div class="hs_event_div">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 border-right gray">
                <div class="">
                <a href="{{ route('doctor.surveys.show', $survey->slug) }}">
                    <h4>{!! Str::limit($survey->title, 20, '...') !!}</h4>
                </a>
                     <p>{!! Str::limit(strip_tags($survey->description), 60, '...') !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>