<div class="row latest-surveys">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h2 class="section-header">Latest MED Voices</h2>
        @if ($surveys->count())
            <div class="ocarousel">
                <div id="owlcarousel" class="owl-carousel owl-theme">
                    @foreach ($surveys as $index=>$survey)
                        @include ('doctor.partials.surveys._survey')
                    @endforeach
                </div>
                <div class="customNavigation surveys text-right">
                    <a class="btn_prev white prev"><span class="arrow light-blue left"></span></a>
                    <a class="btn_next white next"><span class="arrow light-blue right"></span></a>
                </div>
            </div>
        @else
            <p>No Surveys</p>
        @endif
    </div>
</div>

