<div class="row">
    <div class="form-group col-md-12 margin-b-10">
        
        <h4 class="doctor-blue">In which country do you practice medicine?</h4>

        <div class="custom-select gray-bg margin-b-7">
            {!! Form::select('country', ['' => 'Choose country'] + $countries, null, ['class' => 'margin-b-5 form-control', 'id' => 'country']) !!}
        </div>

        <div class="alert alert-danger temp-hide" id="country-alert">
            Sorry but membership for this site is only available for doctors in the middle-east.
        </div>
        <hr class="margin-tb-5">
    </div>
</div>