@if (session('registration.invalid_doctor_email'))
    <div class="doctor-verification">
        <p>Based on the information that you provided to Middle East Doctor during the registration process, we could not automatically verify your professional credentials.</p>

        <p>If you have an alternative, official work email address please try registering with that instead. If Middle East Doctor still cannot verify your credentials, please complete the <a href="{{ route('doctor.register.with_credentials') }}"><u>following form</u></a> and we will verify your account manually. </p>
    </div>
@endif