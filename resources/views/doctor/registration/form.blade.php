<div class="form-group pull-left width-full">
    <div class="form-group margin-b-0 col-md-6 col-sm-6 col-xs-12 {{ $errors->first('fname') ? 'has-error' : '' }}">
        {!! Form::input('text' , 'fname' , null, [
            'required',
            'class' => 'form-control',
            'placeholder' => 'First Name...'
        ]) !!}
        {!! $errors->first('fname','<span class="help-block">:message</span>') !!}
        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group margin-b-0 col-md-6 col-sm-6 col-xs-12 padding-l-0 {{ $errors->first('lname') ? 'has-error' : '' }}">
        {!! Form::input('text' , 'lname', null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Last Name...']) !!}
        {!! $errors->first('lname','<span class="help-block">:message</span>') !!}
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('username') ? 'has-error' : '' }}">
    {!! Form::input('text' , 'username' , null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Username...']) !!}
    {!! $errors->first('username','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('email') ? 'has-error' : '' }}">
    {!! Form::email('email', null, [
        'required',
        'class'             => 'form-control',
        'type'              => 'email',
        'placeholder'       => 'E-mail...',
        'data-native-error' => 'Please enter a valid email address.'
    ]) !!}
    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('password') ? 'has-error' : '' }}">
    {!! Form::password('password', [
        'required',
        'placeholder'          => 'Password...',
        'data-minlength'       => '6',
        'data-minlength-error' => 'The password is not long enough – please enter a longer password.',
        'class'                => 'form-control password-peeker',
        'id'                   => 'inputPassword'
    ]) !!}
    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('designation') ? 'has-error' : '' }}">
    {!! Form::input('text' , 'designation' , null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Designation...']) !!}
    {!! $errors->first('designation','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('therapy') ? 'has-error' : '' }}">

    {!! Form::select('therapies[]', $therapies, null, ['multiple' => 'multiple', 'class' => 'select2 form-control margin-b-5', 'required' => 'required', 'id' => 'register-therapies']) !!}
    {!! $errors->first('therapies','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('therapy') ? 'has-error' : '' }}">
    <hr class="margin-tb-5">
    <h5>Profile</h5>

    <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
    <div class="col-md-6 no-padding">
        Personal Phone Number: <br>
        {!! Form::input('text' , 'phone_numbers_personal' , null ,[ 'required' => 'required', 'placeholder' => '+9710000000', 'class' => 'margin-b-5 form-control' ] ) !!}
        <div class="help-block with-errors"></div>
    </div>
    <div class="col-md-6 padding-r-0">
        Work Phone Number ( Optional ): <br>
        {!! Form::input('text' , 'phone_numbers_work' , null ,[ 'placeholder' => '+9710000000', 'class' => 'margin-b-5 form-control' ] ) !!}
    </div>
</div>

<div class="form-group pull-left width-full col-md-12 {{ $errors->first('therapy') ? 'has-error' : '' }}">

    <div class="add-form clearfix padding-l-0 well-sm col-md-12">
            {{--<img src="{{ asset('public/images/loader.gif') }}" width="60" class="loader" style="position: absolute;">--}}

            Education
            <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
            <div class="fields">
                {!! Form::input('text' , 'education' , null ,[ 'required' => 'required', 'placeholder' => 'Education', 'class' => 'margin-b-5 form-control' ] ) !!}
            </div>

        <div class="help-block with-errors"></div>
    </div>

    <div class="add-form clearfix padding-l-0 well-sm col-md-12">
            {{--<img src="{{ asset('public/images/loader.gif') }}" width="60" class="loader" style="position: absolute;">--}}

            Board Certifications
            <div class="fields">
                {!! Form::input('text' , 'certifications' , null ,[ 'required' => 'required', 'placeholder' => 'Certification', 'class' => 'margin-b-5 form-control' ] ) !!}
            </div>

        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group pull-left padding-l-0 width-full col-md-12 {{ $errors->first('therapy') ? 'has-error' : '' }}">
        Reffered By : ( Optional ) {!! Form::input('text' , 'referred_by' , isset($_GET['referred_by']) ? $_GET['referred_by'] : null, [ 'class' => 'form-control' , 'placeholder' => 'Referred By ( Email )']) !!}
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="col-md-12 temp-hide" id="invalid-country">

    <div class="alert alert-danger" role="alert">
        Thank you for your interest in Middle East Doctor. This website is only available for healthcare professionals that are practising in the Middle East.
    </div>

</div>

@if (session()->has('registration.error'))
    <div class="col-md-12">

        <div class="alert alert-danger" role="alert">
            {{ session('registration.error') }}
        </div>

        @include('doctor.registration.email_verification_error')
    </div>
@endif