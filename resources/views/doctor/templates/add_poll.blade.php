@extends('doctor.master')

@section('content')
    <div class="hs_page_title">
      <div class="container">
        <div class="col-md-9">
            <h3>Discussions</h3>
        </div>
        {{--<ul>--}}
          {{--<li><a href="index.html">Home</a></li>--}}
          {{--<li><a href="blog-categories.html">blog Categories</a></li>--}}
        {{--</ul>--}}
        <div class="col-md-3 search">
            <a href="" id="hs_search">Search <i class="fa fa-search"></i></a>

              <div class="hs_search_box" style="display: none;">
                <form class="form-inline" role="form">
                  <div class="form-group has-success has-feedback">
                    <input type="text" class="form-control" id="inputSuccess4" placeholder="Search">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
                </form>
              </div>
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h4 class="hs_heading">Create a Poll</h4>
                      {!! Form::open(['files' => true]) !!}
                        <div class="hs_single_profile_detail">
                          <div class="row">
                              <div class="col-md-12 poll-form">
                                <div class="voffset4 clearfix"></div>
                                    <div class="form-group">
                                        Poll Title: <input type="text" placeholder="Poll Title" name="poll_title" class="form-control poll-item" required="">
                                        <div class="poll_item_group pi_group1">
                                            Poll Item 1:<input type="text" placeholder="Poll Item Name" name="poll_item[]" class="form-control poll-item" required="">
                                        </div>
                                        <div class="poll_item_group pi_group2">Poll Item 2:
                                            <input type="text" placeholder="Poll Item Name" name="poll_item[]" class="form-control poll-item" required="">
                                        </div>
                                    </div>
                                <a href="#" class="pull-right" id="add_poll_item">Add a poll item +</a>
                              </div>
                          </div>
                        <div class="voffset4 clearfix"></div>
                        {!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}
                        {!! Form::close() !!}
                        </div>
                    <div class="clearfix"></div>
                </div>

                @include('templates.sidebar')
            </div>
        </div>
    </div>
@stop
