
<div class="hs_single_profile_detail">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
            {!! Form::hidden('type' , 'panel' ) !!}
            Title: <br>
            @if(isset($data))
                {!! Form::input('text','title' , $data['discussion']->title , [ 'required' , 'class' => 'form-control' ]) !!}
            @else
                {!! Form::input('text','title' , null , [ 'required' , 'class' => 'form-control' ]) !!}
            @endif
        </div>
        <div class="form-group">
            Content:
            @if(isset($data))
                {!! Form::textarea('content', $data['discussion']->content, [ 'required', 'class' => 'form-control textarea' ]) !!}
            @else
                {!! Form::textarea('content', null , [ 'required', 'class' => 'form-control textarea' ]) !!}
            @endif
        </div>
        <div class="form-group">
        </div>
        <div class="form-group">
            Attachment: ( Optional )
            @if(isset($data))
                @if($data['attachment'])
                    {!! Form::hidden('attachment_id',$data['attachment']->id) !!}
                    <div class="hs_margin_40 clearfix"></div>
                    <div class="well well-sm clearfix">
                        <div class="col-md-12">
                        @if($data['attachment']->file_type == "image")
                        <?php $img_url = URL::to('uploads/'.$data['attachment']->path.'/'.$data['attachment']->name); ?>
                            <a id="single_image" href="{{$img_url}}"><img src="{{$img_url}}" width="100"></a>
                        @else
                            <a target="_blank" href="{{ URL::to('download/'.$data['attachment']->upload_id) }}">{{$data['attachment']->name}}</a>
                        @endif
                        </div>
                        <div class="col-md-12">
                            Remove Current Attachment: {!! Form::checkbox('remove_attachment', 1 , false ) !!}
                        </div>
                    </div>
                @endif
            @endif
                {!! Form::input('file', 'attachment[]' , null , [ 'class' => 'form-control' ]) !!}
        </div>
        {{--Add poll:--}}
        {{--{!! Form::checkbox('poll', 1 , null , [ 'id' => 'add_poll' , 'autocomplete' => 'off']) !!}--}}
        {{--<div id="poll_form">--}}
            {{--<div class="form-group">--}}

            {{--</div>--}}
            {{--<button class="pull-left" id="add_poll_item">Add a poll item +</button>--}}
        {{--</div>--}}
      </div>
            {{--<div class="col-lg-6 col-md-6 col-sm-6"> Get connect with him:--}}
              {{--<div class="hs_profile_social"><br>--}}
                {{--<ul>--}}
                  {{--<li><a href=""><i class="fa fa-facebook"></i></a></li>--}}
                  {{--<li><a href=""><i class="fa fa-twitter"></i></a></li>--}}
                  {{--<li><a href=""><i class="fa fa-linkedin"></i></a></li>--}}
                  {{--<li><a href=""><i class="fa fa-skype"></i></a></li>--}}
                {{--</ul>--}}
              {{--</div>--}}
            {{--</div>--}}
      </div>
            <div class="voffset4 clearfix"></div>
{!! Form::submit('Submit', [ 'class' => 'form-control' ]) !!}