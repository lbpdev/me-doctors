<!-- Modal -->
<div class="modal fade" id="claim" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center pink">Claim Hospital Ownership</h4>
      </div>

      <div class="modal-body text-center">
           {!! Form::open(['id' => 'claimForm','route' => 'patient.hospitals.claim']) !!}
           <p class="margin-b-7">In order to claim this hospital, we need to verify you through email.<br> Please enter your company email below.</p>
           <div class="alert alert-danger temp-hide" id="invalidEmail">
            <p class="margin-b-7">Invalid e-mail. Please use a company e-mail.</p>
           </div>
           {!! Form::hidden('hospital_id', $hospital->id) !!}
           {!! Form::input('email', 'email', null , ['id' => 'email' ,'class' => 'form-control','required']) !!}
           {!! Form::close() !!}
           <div class="width-full text-center">
            {!! Form::button('Submit',['id' => 'submitClaim', 'class' => 'button pink font-20 margin-tb-5 padding-l-0']) !!}
           </div>
      </div>
    </div>

  </div>
</div>