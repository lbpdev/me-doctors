<div class="col-md-3 no-padding clearfix inbox-menu pull-left">
    <h3 class="no-border doctor-blue hs_heading pull-left width-full margin-b-0 padding-b-0 margin-t-0 clearfix">
        <span class="page-icon inbox"></span>
        <span class="page-title">Inbox</span>
    </h3>
    <div class="col-md-12 col-sm-12 col-xs-12 gray-bg pull-left padding-tb-10 margin-t-10">
        <div class="row">

           <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                 <h3 class="media-heading clearfix">
                     <a href="{{ route('messages_create') }}" class="{{ strpos(Request::path(), 'create')  ? 'active pink' : '' }} pull-left t-regular">New</a>
                 </h3>
           </div>
            <hr class="no-padding margin-t-0 margin-b-7">
            <ul class="no-padding inbox">
                 <li class="discussion clearfix height-auto">
                   <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                     <h3 class="media-heading clearfix">
                        <a href="{{ route('messages_show') }}" class="{{ strpos(Request::path(), 'inbox')  ? 'active pink' : '' }} pull-left t-regular">Messages
                        @if($unread_messages)
                            <span class="font-9">( {{ $unread_messages }} unread )</span>
                        @endif
                        </a>
                     </h3>
                   </div>
                 </li>
                 {{--<li class="discussion clearfix height-auto">--}}
                   {{--<div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">--}}
                         {{--<h3 class="media-heading clearfix">--}}
                             {{--<a href="{{ route('messages_sent') }}" class="{{ strpos(Request::path(), 'sent')  ? 'active pink' : '' }} pull-left t-regular">Sent</a>--}}
                         {{--</h3>--}}
                   {{--</div>--}}
                 {{--</li>--}}
                 {{--<li class="discussion clearfix height-auto">--}}
                   {{--<div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">--}}
                         {{--<h3 class="media-heading clearfix">--}}
                             {{--<a href="{{ route('messages_archive') }}" class="{{ Request::is('messages/archive')  ? '' : 't-black'  }} pull-left t-regular">Archive</a>--}}
                         {{--</h3>--}}
                   {{--</div>--}}
                 {{--</li>--}}
                 <li class="discussion clearfix height-auto">
                   <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                         <h3 class="media-heading clearfix">
                             <a href="{{ route('messages_trashed') }}" class="{{ strpos(Request::path(), 'trash')  ? 'active pink' : '' }} pull-left t-regular">Trash</a>
                         </h3>
                   </div>
                 </li>
            </ul>

            @include('doctor.pages.messages._search')
        </div>
    </div>
</div>