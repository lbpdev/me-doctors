<!-- Modal -->
<div class="modal fade" id="premium" role="dialog">
    <div class="modal-dialog" style="width: auto">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center pink">Premium Account</h4>
            </div>

            <div class="modal-body text-center">

                <a href='https://sandbox.2checkout.com/checkout/purchase?sid={{ env('2CHECKOUT_SID','901301595') }}&quantity=1&last_name={{ $authUser->lname }}&first_name={{ $authUser->fname }}&email={{ $authUser->email }}&product_id=2&hospital_id={{ $hospital->id }}&user_id={{ $authUser->id }}'>Upgrade</a>

                    {{--<form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>--}}
                      {{--<input type='hidden' name='sid' value='{{ env('2CHECKOUT_SID','901301595') }}' />--}}
                      {{--<input type='hidden' name='mode' value='2CO' />--}}
                      {{--<input type='hidden' name='product_id' value='1' />--}}
                      {{--<input type='hidden' name='med_user_id' value='{{ $authUser->id }}' />--}}
                      {{--<input type='hidden' name='li_0_name' value='Monthly Subscription' />--}}
                      {{--<input type='hidden' name='li_0_price' value='1.00' />--}}
                      {{--<input type='hidden' name='li_0_recurrence' value='1 Month' />--}}
                      {{--<input type='hidden' name='li_0_quantity' value='3' />--}}
                      {{--<input type='hidden' name='li_0_tangible' value='N' />--}}
                      {{--<input type='hidden' name='li_0_duration' value='1 Year' />--}}
                      {{--<input type='hidden' name='demo' value='Y' />--}}
                      {{--<input name='submit' type='submit' value='Checkout' />--}}
                    {{--</form>--}}

                    {{--{!! Form::open(['id' => 'myCCForm','route' => 'doctor.purchase-premium']) !!}--}}
                        {{--<input name="token" type="hidden" value="" />--}}
                        {{--<input name="_token" type="hidden" value="{{ csrf_token() }}" />--}}
                        {{--<div>--}}
                            {{--<label>--}}
                                {{--<span>Card Number</span>--}}
                                {{--<input id="ccNo" type="text" value="4000000000000002" autocomplete="off" required />--}}
                            {{--</label>--}}
                        {{--</div>--}}
                        {{--<div>--}}
                            {{--<label>--}}
                                {{--<span>Expiration Date (MM/YYYY) </span>--}}
                                {{--<input id="expMonth" type="text" size="2" required value="09"/>--}}
                            {{--</label>--}}
                            {{--<span> / </span>--}}
                            {{--<input id="expYear" type="text" size="4" required value="2017"/>--}}
                        {{--</div>--}}
                        {{--<div>--}}
                            {{--<label>--}}
                                {{--<span>CVC</span>--}}
                                {{--<input id="cvv" type="text" autocomplete="off" required value="1234" />--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--{!! Form::close() !!}--}}

                    {{--<div class="width-full text-center">--}}
                        {{--<input id="submitCCForm" type="submit" value="Submit Payment" class="button pink font-20 margin-tb-5 padding-l-0"/>--}}
                    {{--</div>--}}

            </div>
        </div>

    </div>
</div>