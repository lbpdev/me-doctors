<!-- Modal -->
<div class="modal fade" id="ratingModal" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center pink">RATE DOCTOR</h4>
      </div>

      <div class="modal-body">

      @if($authUser->verified)
        {!! Form::open(['route' => 'patient.doctors.rate']) !!}
        {!! Form::hidden('user_id', $user->id) !!}
           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-friendliness"></i> Friendliness</h4>
           <p class="margin-b-7">Was the doctor friendly and courteous<br>
           during your appointment?</p>
           <p>
           {!! Form::radio('ratings[friendliness]', 1, true, ['id' => 'radioFriendlinessT' , 'class' => 'choice-radio']) !!}
           <label for="radioFriendlinessT"><span></span></label>
           Yes
           {!! Form::radio('ratings[friendliness]', 0, $current_user_ratings['friendliness'] ? false : true, ['id' => 'radioFriendlinessF' , 'class' => 'gray-bg choice-radio']) !!}
           <label class="margin-l-10 " for="radioFriendlinessF"><span></span></label>
           No
           </p>


           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-professionalism"></i> Professionalism</h4>
           <p class="margin-b-7">Did the doctor conduct himself professionally<br>
           throughout the appointment?</p>
           <p>
           {!! Form::radio('ratings[professionalism]', 1, true, ['id' => 'radioProfessionalismT' , 'class' => 'choice-radio']) !!}
           <label for="radioProfessionalismT"><span></span></label>
           Yes
           {!! Form::radio('ratings[professionalism]', 0, $current_user_ratings['professionalism'] ? false : true, ['id' => 'radioProfessionalismF' , 'class' => 'gray-bg choice-radio']) !!}
           <label class="margin-l-10 " for="radioProfessionalismF"><span></span></label>
           No
           </p>


           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-bed-manner"></i> Bedside manner</h4>
           <p class="margin-b-7">Was the doctor’s approach and attitude<br>
           positive throughout the appointment?</p>
           <p>
           {!! Form::radio('ratings[bedside]', 1, true, ['id' => 'radioBedsideT' , 'class' => 'choice-radio']) !!}
           <label for="radioBedsideT"><span></span></label>
           Yes
           {!! Form::radio('ratings[bedside]', 0, $current_user_ratings['bedsideManner'] ? false : true , ['id' => 'radioBedsideF' , 'class' => 'gray-bg choice-radio']) !!}
           <label class="margin-l-10 " for="radioBedsideF"><span></span></label>
           No
           </p>


           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-medical-knowledge"></i> Medical knowledge</h4>
           <p class="margin-b-7">Did the doctor exhibit good medical<br>
           knowledge surrounding your condition?</p>
           <p>
           {!! Form::radio('ratings[medical]', 1, true, ['id' => 'radioMedicalT' , 'class' => 'choice-radio']) !!}
           <label for="radioMedicalT"><span></span></label>
           Yes
           {!! Form::radio('ratings[medical]', 0, $current_user_ratings['medicalKnowledge'] ? false : true , ['id' => 'radioMedicalF' , 'class' => 'gray-bg choice-radio']) !!}
           <label class="margin-l-10 " for="radioMedicalF"><span></span></label>
           No
           </p>


           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-helpfulness"></i> Helpfulness</h4>
           <p class="margin-b-7">Did the doctor successfully treat your<br>
           symptoms and provide a helpful diagnosis<br>
           and treatment course?</p>
           <p>
           {!! Form::radio('ratings[helpfullness]', 1, true, ['id' => 'radioHelpfulnessT' , 'class' => 'choice-radio']) !!}
           <label for="radioHelpfulnessT"><span></span></label>
           Yes
           {!! Form::radio('ratings[helpfullness]', 0, $current_user_ratings['helpfulness'] ? false : true , ['id' => 'radioHelpfulnessF' , 'class' => 'gray-bg choice-radio']) !!}
           <label class="margin-l-10 " for="radioHelpfulnessF"><span></span></label>
           No
           </p>


           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-waiting-time"></i> Waiting Time</h4>
           <p class="margin-b-7">Was the doctor punctual or were you<br>
           kept waiting for a long time?</p>
           <div class="custom-select gray-bg">
               {!! Form::select('ratings[waiting]', $waiting_times ,$current_user_ratings['waitingTime'] , ['class' => 'form-control pull-left']) !!}

            </div>
           <div class="width-full text-center">
                {!! Form::submit('Rate',['class' => 'button pink font-20 margin-tb-5 padding-l-0']) !!}
           </div>

        {!! Form::close() !!}
        @else
            <p class="">Only verified users may rate doctors. Please check your e-mail and verify your account first.</p>
        @endif
      </div>
    </div>

  </div>
</div>