<!-- Modal -->
<div class="modal fade" id="refer" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center pink">Recommend MED to a colleague</h4>
      </div>

      <div class="modal-body text-center">
           <div class="alert alert-success temp-hide" id="referralSent">
            <p class="margin-b-7" id="invitationSentTo"></p>
           </div>

           <p class="margin-b-7">Refer a healthcare professional to Middle East Doctor and get<br>  a one month free premium account when they register.<br></p>

           @if(isset($authUser))
              {!! Form::open(['id' => 'referForm','route' => 'patient.hospitals.claim']) !!}
               <div class="alert alert-danger temp-hide" id="invalidReferEmail">
                <p class="margin-b-7">Invalid e-mail. Please use a working e-mail address.</p>
               </div>
               {!! Form::hidden('user_id', $authUser->id) !!}
               {!! Form::input('email', 'email', null , ['id' => 'referEmail' ,'class' => 'text-center form-control','required' , 'placeholder' => "Colleague’s email address" ]) !!}
               {!! Form::close() !!}
               <div class="width-full text-center">
               {!! Form::button('Submit',['id' => 'submitRefer', 'class' => 'button pink font-20 margin-tb-5 padding-l-0' , 'value' => 'Send Invitation' ]) !!}
               </div>
           @else
            <p>Please log-in to proceed.</p>
           @endif
      </div>
    </div>

  </div>
</div>