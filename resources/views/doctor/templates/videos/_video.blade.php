<div class="videos_slider_item padding-5">
    <div class="hs_event_div">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <a class="fancybox" data-type="iframe" href="{{ $video->url }}" title="youtube">
                    <img class=" width-full" src="{{ $video->thumbnail }}" title="{{ $video->title }}" alt="{{ $video->title }}">
                    <p class="title">{{ Str::words($video->title, 12) }}</p>
                </a>
            </div>
        </div>
    </div>
</div>