<div class="col-md-12 clearfix pull-left width-full video_slider">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <h4 class="section-header">VIDEOS

                <a href="{{ route('doctor.videos.index') }}">
                    <span class="action-title pull-right font-14 " style="line-height: 25px;">
                    View All
                    </span>
                </a>
                </h4>
                {{--@if ($events->count())--}}
                    <div class="videos_slider ocarousel">
                        <div id="video_slider" class="owl-carousel owl-theme padding-10">

                            @if(count($videos)>0)
                                @foreach($videos as $video)
                                    @include ('doctor.templates.videos._video', [ 'video' => $video])
                                @endforeach
                            @else
                                <p>No Videos</p>
                            @endif

                        </div>

                        @if(count($videos)>0)
                            <div class="customNavigation video_slider_nav text-right">
                                <a class="btn_prev white prev"><span class="arrow blue left"></span></a>
                                <a class="btn_next white next"><span class="arrow blue right"></span></a>
                            </div>
                        @endif
                    </div>
                {{--@else--}}
                    {{--<p>No Videos</p>--}}
                {{--@endif--}}
            </div>
        </div>
    </div>
</div>