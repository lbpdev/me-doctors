@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            New Diagnosis requires approval.
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Title : {{ $title }}
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Click <a href="{{ route('adm_diag_single', $id) }}">here</a> check.
        </span>
    </p>
@stop