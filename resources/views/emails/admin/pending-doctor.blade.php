@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            User {{ $user }} requires verification.</span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Click <a href="{{ route('admin.users.unverified', $username) }}">here</a> to check.
        </span>
    </p>
@stop