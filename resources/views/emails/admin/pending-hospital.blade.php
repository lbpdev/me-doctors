@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Hospital <b>{{ $hospital_name }}</b> requires verification.</span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Click <a href="{{ route('admin.hospitals.show', $hospital_slug) }}">here</a> check.
        </span>
    </p>
@stop