@extends('emails.main')

@section('email_body')
    <h3>Inquiry from MED contact form.</h3>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Topic: {{ $topic }}
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Subject: {{ $subject }}
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            From: {{ $name }} ( {{ $email }} )
        </span>
    </p>
    <p>
        <span>
            Contact Number: {{ isset($contact) ? $contact : 'N/A' }}
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Message: {{ $content }}
        </span>
    </p>
@stop