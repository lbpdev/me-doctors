@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user }}</span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          We are pleased to inform you that your professional profile has been added to <a style="color: #337ab7;" href="{{ url('/') }}">www.middleeastdoctor.com</a>.
        </span>
    </p>

     <p>
        <span style="font-family: Calibri; font-size: 16px;">
             Please click <a style="color: #337ab7;" href="{{ url('doctor/login') }}">here</a> to verify your account using the following details:
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          <b>Username:</b> {{ $username }} <br>
          <b>Password:</b> {{ $password }}
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Welcome to the Middle East’s largest community for HCPs, and we look forward to seeing you on the site.
        </span>
    </p>
@stop