@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user['fname'] }} {{ $user['lname'] }}</span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Thank you for your interest in Middle East Doctor. 
        </span>
    </p>

     <p>
        <span style="font-family: Calibri; font-size: 16px;">
             It appears that you tried to create an account on <a href="http://middleeastdoctor.com" style="color: #337ab7;">www.middleeastdoctor.com</a>, but your credentials were not verified and your application to join the Middle East’s largest community of HCPs has not been approved. 
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          If you are a qualified medical doctor practicing in the Middle East, please reply to this email and attach your medical license and government-issued identity, and our team will reconsider your application.
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Thank you for your interest in Middle East Doctor.
        </span>
    </p>
@stop