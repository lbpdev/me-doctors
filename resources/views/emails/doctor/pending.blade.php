@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user }}</span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Thank you for registering. We will now evaluate your credentials and will notify you once they have been verified.
        </span>
    </p>
@stop