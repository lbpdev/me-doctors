@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user['fname'] }} {{ $user['lname'] }}</span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          We are pleased to inform you that your application to www.middleeastdoctor.com has been approved. In order to complete your registration please click on the verification link below:
        </span>
    </p>

     <p>
        <span style="font-family: Calibri; font-size: 16px;">
             <a style="color: #337ab7;" href="{{ route('doctor.register.confirm', $user['token']) }}">{{ route('doctor.register.confirm', $user['token']) }}</a>.
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Welcome to the Middle East’s largest community for HCPs and we look forward to seeing you on the site.
        </span>
    </p>
@stop