@extends('emails.main')

@section('email_body')

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          An ownership claim for {{ $hospital }} has been requested by {{ $user }}
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Click here to check : <a href="{{ route('admin.hospitals.claims') }}">{{ route('admin.hospitals.claims') }}</a>
        </span>
    </p>
@stop