@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user }} </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          We are pleased to inform you that your claim request for {{ $hospital }} has been approved. In order to complete the process please click on the verification link below:
        </span>
    </p>

     <p>
        <span style="font-family: Calibri; font-size: 16px;">
             <a style="color: #337ab7;" href="{{ $user_type == 2 ? route('doctor.hospitals.claims.verify', $token) : route('patient.hospitals.claims.verify', $token) }}">Click here to verify.</a>
        </span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
          Once verified, you will be able to edit and modify your listing.
        </span>
    </p>
@stop