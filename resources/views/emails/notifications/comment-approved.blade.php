@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            {{ $author }}, your comment on {{ $title }} has been approved.
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            <a href="{{ $link }}">Please click here to go to the discussion.</a>
        </span>
    </p>
@stop