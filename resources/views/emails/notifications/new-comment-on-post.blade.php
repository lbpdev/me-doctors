@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            {{ $author }} commented on you post <b>{{ $title }}</b>.
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            <a href="{{ $link }}">Click here to see the discussion.</a>
        </span>
    </p>
@stop