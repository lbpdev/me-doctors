@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            {{ $author }} also commented on {{ $title }}.
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            <a href="{{ $link }}">Click here to see the discussion.</a>
        </span>
    </p>
@stop