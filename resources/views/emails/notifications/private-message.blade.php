@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            You have received a new message from: {{ $sender_name }}
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            <a href="{{ route($link,$conversation_id) }}">To view your message, please click here</a>
        </span>
    </p>
@stop