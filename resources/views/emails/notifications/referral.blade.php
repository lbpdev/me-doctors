@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            You have received an invitation to join MED from: {{ $sender_name }}
        </span>
    </p>
    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            <a href="{{ route('doctor.register').'?referred_by='.$sender_email }}">Click here to join MED</a>
        </span>
    </p>
@stop