@extends('emails.main')

@section('email_body')
        <p>
            <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user['fname'] }} {{ $user['lname'] }}</span>
        </p>

        <p>
            <span style="font-family: Calibri; font-size: 16px;">
                Thank you registering with Middle East Doctor, and you are one step away from having full access to the site.
            </span>
        </p>

         <p>
            <span style="font-family: Calibri; font-size: 16px;">
                <a style="color: #337ab7;" href="{{ route('patient.register.confirm', $user['token']) }}">To verify your email address and confirm your account please click here.</a>
            </span>
        </p>
@stop