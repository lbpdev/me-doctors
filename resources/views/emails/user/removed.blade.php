@extends('emails.main')

@section('email_body')
    <p>
        <span style="font-family: Calibri; font-size: 16px;">Dear {{ $user['fname'] }} {{ $user['lname'] }}</span>
    </p>

    <p>
        <span style="font-family: Calibri; font-size: 16px;">
            Your account has been removed by the administrator.
        </span>
    </p>

     <p>
        <span style="font-family: Calibri; font-size: 16px;">
            For any concerns about your account, feel free to contact us <a href="http://middleeastdoctor.com/contact">here</a>.
        </span>
    </p>
@stop