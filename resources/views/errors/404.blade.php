<!DOCTYPE html>
<html lang="en">

@if( strpos(Request::url(), '/patient') )
    @include('patient.header')
@else
    @include('doctor.header')
@endif

<div class="main-wrapper">
    <div class="container">
        <div class="error-404 text-center">
            <br><br>
            <h1>THE PAGE YOU'RE TRYING TO ACCESS <BR> IS NOT AVAILABLE.</h1>
            <p> The page you are looking for does not exist. It may have been moved, or removed altogether.</p>
            <p>Perhaps you can return back to the site’s homepage and see if you can find what you are looking for.</p>
            <br><br>
            <a href="{{ strpos(Request::url(), '/patient') ? route('patient.home') : route('doctor.home') }}">&lt; Go Back</a>
        </div>
    </div>
</div>

@if( strpos(Request::url(), '/patient') )
    @include('patient.footer')
@else
    @include('doctor.footer')
@endif

</body>
</html>