<!DOCTYPE html>
<html lang="en">

@if( strpos(Request::url(), '/patient') )
    @include('patient.header')
@else
    @include('doctor.header')
@endif

<div class="main-wrapper">
    <div class="container">
        <div class="error-404 text-center">
            <br><br>
            <div class="title">Be right back.</div>
            <a href="{{ strpos(Request::url(), '/patient') ? route('patient.home') : route('doctor.home') }}">&lt; Go Back</a>
        </div>
    </div>
</div>

@if( strpos(Request::url(), '/patient') )
    @include('patient.footer')
@else
    @include('doctor.footer')
@endif

</body>
</html>