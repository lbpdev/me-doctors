<!DOCTYPE html>
<html lang="en">
@if( strpos(Request::url(), '/patient') )
    @include('patient.header')
@else
    @include('doctor.header')
@endif

<div class="main-wrapper">
    <div class="container">
        <div class="error-404 text-center">
            <br><br>
                <h1 class="font-30 fa fa-lock" style="font-size: 130px !important; margin: 30px;"></h1>
                <p>This section of Middle East Doctor is for doctors only. If you are a doctor or healthcare professional,<br> please log out as a patient, and login or register as a doctor.</p>
            <br><br>
            <a href="{{ $user->hasRole('patient') ? route('patient.home') : route('doctor.home') }}">&lt; Go Back</a>
        </div>
    </div>
</div>

@if( strpos(Request::url(), '/patient') )
    @include('patient.footer')
@else
    @include('doctor.footer')
@endif

</body>
</html>