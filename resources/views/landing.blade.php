
<html>
    <head>
    <meta name="google-site-verification" content="xKffKTkelGDIJrEtvba45GFP60f2mtM_rQdeAvXt-3k" />
    <title>Middle East Doctor - The Official Website</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />

    @yield('metas')

    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="{{ asset('publicfavicon-32x32.png') }}" />
    <link rel="icon" type="image/ico" href="{{ asset('publicfavicon-32x32.png') }}" />

    @section('styles')
    @show

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('public/css/style.css') }}" media="screen"/>
    <!-- Place this data between the <head> tags of your website -->

    <meta name="description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East." />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Middle East Doctor">
    <meta itemprop="description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East.">
    <meta itemprop="image" content="{{ asset('public/images/MED-Footer.png') }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="www.middleeastdoctor.com">
    <meta name="twitter:title" content="Middle East Doctor">
    <meta name="twitter:description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East.">
    <meta name="twitter:creator" content="Middle East Doctor">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('public/images/MED-Footer.png') }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="Middle East Doctor" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="http://middleeastdoctor.com/" />
    <meta property="og:image" content="{{ asset('public/images/MED-Footer.png') }}" />
    <meta property="og:description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East." />
    <meta property="og:site_name" content="www.middleeastdoctor.com" />
    <meta property="fb:admins" content="100000370643856" />
    </head>
<body style="padding:0;margin: 0; background-size: auto 100%;background-color:#000; background-image: url({{ asset('public/images/med-coming-soon.png') }}); background-position: center center; background-repeat: no-repeat">
    <div class="container text-center">
    <button data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#whoAmI" class="temp-hide" id="modalBT"></button>
<div class="modal fade who-am-i" id="whoAmI" tabindex="-1" role="dialog" aria-labelledby="whoAmILabel">
        {{-- WHO AM I --}}
            <div class="modal-dialog" role="document" style="margin-top: 11%;">
                <div class="modal-content"  style="border-radius: 30px;">
                    <div class="modal-header text-center">
                        <h4 class="modal-title" id="whoAmILabel">WHO AM I?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-offset-1 col-xs-5 who-item"
                                 data-login='{"text":"DOCTOR LOGIN","image":"#doctorImage"}'
                                 data-login-url="{{ url(route('doctor.login')) }}"
                                 data-register-url="{{ url(route('doctor.register')) }}">
                                 <a href="{{ URL('doctor') }}">
                                     <img src="{{ asset('public/images/icon/who-am-i/doctor.png') }}" alt="Doctor" id="doctorImage">
                                     <span>DOCTOR</span>
                                 </a>
                            </div>
                            <div class="col-xs-5 who-item"
                                     data-login='{"text":"PATIENT LOGIN","image":"#patientImage"}'
                                     data-login-url="{{ url(route('patient.login')) }}"
                                     data-register-url="{{ url(route('patient.register')) }}">
                                     <a href="{{ URL('patient') }}">
                                         <img src="{{ asset('public/images/icon/who-am-i/patient.png') }}" alt="Patient" id="patientImage">
                                         <span>PATIENT</span>
                                     </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-offset-1 col-xs-5 who-item">
                                <img src="{{ asset('public/images/icon/who-am-i/facility.png') }}" alt="Healthcare Facility">
                                <span>HEALTHCARE FACILITY</span>
                            </div>
                            <div class="col-xs-5 who-item">
                                <img src="{{ asset('public/images/icon/who-am-i/other.png') }}" alt="Other">
                                <span>OTHER</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>



<script type="text/javascript" src="{{ asset('public/js/jquery-1.11.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/smoothScroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/custom/fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.powertip.min.js') }}"></script>



<script>
   $(document).ready(function(){
        setInterval(function(){
            $('#modalBT').trigger('click');
        },1000)
   });

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1910012-8', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>