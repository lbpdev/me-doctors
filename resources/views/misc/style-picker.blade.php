
<!-- color picker start -->
<div id="style-switcher" class="hs_color_set">
  <div>
    <h3>color options</h3>
    <ul class="colors">
      <li>
        <p class='colorchange' id='color'></p>
      </li>
      <li>
        <p class='colorchange' id='color2'></p>
      </li>
      <li>
        <p class='colorchange' id='color3'></p>
      </li>
      <li>
        <p class='colorchange' id='color4'></p>
      </li>
      <li>
        <p class='colorchange' id='color5'></p>
      </li>
      <li>
        <p class='colorchange' id='style'></p>
      </li>
    </ul>
    <h3>Theme Option</h3>
    <select class="rock_themeoption" onchange="location = this.options[this.selectedIndex].value;">
      <option>Select Option</option>
      <option value="one-page.html">Single Page</option>
      <option value="index.html">Multi Pages</option>
    </select>
    <p> </p>
  </div>
  <div class="bottom"> <a href="" class="settings"><i class="fa fa-gear"></i></a> </div>
</div>
<!-- color picker end -->