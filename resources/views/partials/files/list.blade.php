<ul>
    @foreach ($files as $file)
        <li><a href="{{ route('files.download', $file->id) }}">{{ $file->original_name }}</a></li>
    @endforeach
</ul>