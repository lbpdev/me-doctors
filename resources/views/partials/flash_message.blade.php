@if ($message = session('flash_message'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
@endif