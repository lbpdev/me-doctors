@extends('patient.master')

@section('styles')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
@stop

@section('content')
    <div class="container login-container">
        <div class="row">

            {{-- REGISTRATION FORM --}}
            <div class="col-lg-8 col-md-8 col-sm-12 margin-t-20 register border-right-light">
                <h3 class="hs_heading no-margin-top pull-left width-full">
                    <span class="page-icon patient big"></span>
                    <span class="page-title"> Register</span>
                </h3>

                @if (session()->has('registration.success'))
                    <div class="alert alert-success">
                        {{ session('registration.success') }}
                    </div>
                @else
                    <p>Welcome to Middle East Doctor. Please complete the form below for full access to the site.</p>
                    <br>

                    {!! Form::open([
                        'route'       => 'patient.register',
                        'data-toggle' => 'validator',
                        'role'        => 'form' ,
                        'class'       => 'clearfix'
                    ]) !!}

                    <div class="row {{ session()->has('registration.error') || $errors->any() ? '' : 'register-form' }}" id="register-form">
                        @include('patient.registration.form')

                        <div class="form-group">
                            {!! Form::submit('Register', [ 'class' => 'submit button pull-left']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                @endif
            </div>

            {{-- LOGIN FORM --}}
            <div class="col-lg-4 col-md-4 col-sm-12 padding-l-10 login-panel">

                @include('patient.auth.login')
                <hr />
                <a href="http://thepatient.me/" target="_blank">
                    <img src="{{ asset('public/images/ads/the-patient.png') }}" width="100%">
                </a>
            </div>
        </div> <!-- END OF ROW -->
    </div> <!-- END OF CONTAINER -->
@stop

@section('js')
    @include('doctor.auth.scripts')
@stop