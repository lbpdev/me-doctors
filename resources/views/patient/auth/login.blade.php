<div class="login-panel clearfix margin-t-20 ">
    <h3 class="hs_heading no-margin-top pull-left width-full">
        <span class="page-icon patient big"></span>
        <span class="page-title"> Login</span>
    </h3>
    @if(session()->has('message'))
        <div class="form-group">
            <div class="alert alert-danger" role="alert">
                {{ session('message') }}
            </div>
        </div>
    @endif
        {!! Form::open([ 'data-toggle' => 'validator' , 'role' => 'form', 'id' => 'login-form' , 'route' => 'patient.login']) !!}

        <div class="form-group {{ $errors->first('username_email') ? 'has-error' : '' }}">
            {!! Form::label('Username' , 'Username or E-mail: ' ) !!}
            {!! Form::input('text' , 'username_email' , null, [ 'required', 'class' => 'form-control']) !!}
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group  {{ $errors->first('password') ? 'has-error' : '' }}">
            {!! Form::label('Password' , 'Password: ' ) !!}
            {!! Form::password('password' , [ 'required', 'data-minlength' => '6', 'class' => 'form-control' , 'id' => 'inputPassword']) !!}
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'form-control']) !!}
        </div>
        {!! Form::close() !!}
        <br>
        Forgot your password? <a href="{{ route('patient.password.forgot') }}">Click here</a>.

</div> <!-- END OF PANEL -->
<div class="hs_margin_60"></div>