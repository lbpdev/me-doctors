
<section class="footer clearfix">
    <div class="container padding-r-0">
        <div class="col-md-8 margin-b-sm-5 ">
            <div class="row">
                <ul class="nav-links">
                    <li><a href="{{ URL::to('patient/about') }}">About Middle East Doctor</a></li>
                    <li><a href="{{ URL::to('patient/terms') }}">Terms of Use</a></li>
                    <li><a href="{{ URL::to('patient/contact') }}">Contact Us </a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 copyright">
            <div class="row">
                <div class="col-md-8 text-right">
                    <img src="{{ asset('public/images/MED-Footer.png') }}" width="80" class="margin-r-7">
                </div>
                <div class="col-md-4 text-left">
                  <div class="row">
                  Copyright <a href="#">2016</a><br>
                  Middle East Doctor LLC (Delaware, USA)
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('auth.who')

<script type="text/javascript" src="{{ asset('public/js/jquery-1.11.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/custom/fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/jquery.powertip.min.js') }}"></script>

<!--main js file end-->

<script src="{{ asset('public/js/custom/authenticator.js') }}" type="text/javascript"></script>
@yield('sidebar-js')
@yield('js-inner')
@yield('js')

@include('patient.templates.analyticstracking')
</body>
</html>
