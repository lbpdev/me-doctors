<head>
    <meta name="keywords" content="Health Care, health" />
    <meta name="description" content="Health Care HTML Template" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/ppIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/appIcons/MED-Icon144x144.png') }}" />

    @yield('metas')
    <!-- favicon links -->
    <link rel="shortcut icon" type="image/ico" href="{{ asset('public/favicon-32x32.png') }}" />
    <link rel="icon" type="image/ico" href="{{ asset('public/favicon-32x32.png') }}" />

    @section('styles')
    @show

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-patient.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}" media="screen"/>
    <link rel="stylesheet" href="{{ asset('public/css/style-patient.css') }}" media="screen"/>

    <title>Middle East Doctors - Official Website</title>

    <meta name="csrf_token" content="{{ csrf_token() }}" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<header>

<section class="ad">
    <div class="container overflow-hidden padding-0-xs">
        @include('templates.top-long')
        @include('templates.top-sm')
        {{--<a href="{{ URL::to('/') }}"><img src="http://placehold.it/480x100"  id="header-ad  -mobile" width="100%"></a>--}}
    </div>
</section>


<section class="banner">
    <div class="container padding-r-0">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 col-xss-12 logo-wrap padding-tb-8 ">

                <div class="col-md-12 no-padding margin-b-4">
                    <span>{{ Carbon::now()->format('l') }}</span>
                    <span>{{ Carbon::now()->format('j') }}<sup>{{ Carbon::now()->format('S') }}</sup></span>
                    <span>{{ Carbon::now()->format('F') }}</span>
                    <span>{{ Carbon::now()->format('Y') }}</span>
                </div>

                <div class="col-md-12 no-padding margin-tb-4 margin-r-10 pull-left">
                    <span class="pull-left">{{ $today['city'] }}</span>
                </div>
                <div class="col-md-12  col-sm-12  no-padding text-right location">
                    <div class="col-md-12 pull-left margin-tb-4 margin-r-10 padding-r-10">
                        <div class="row">
                            <span class="weather-icon sun pull-left margin-r-5"></span>
                            <p class="pull-left" id="sunrise">{{ $today['sunrise'] }}</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 pull-left margin-tb-4 margin-r-10 padding-r-10">
                        <div class="row">
                            <span class="weather-icon margin-r-5     moon pull-left"></span>
                            <p class="pull-left" id="sunset">{{ $today['sunset'] }}</p>
                        </div>
                    </div>
               </div>

            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 col-xss-6 padding-b-8 text-center text-left-xs padding-t-4">
                <div class="col-md-12 padding-b-4">
                    <a href="{{ url('doctor') }}" class="logo-link">Doctor Section</a> |
                    <a href="{{ url('patient') }}" class="logo-link">Patient Section</a>
                </div>
                <a href="{{ URL('/patient') }}"><img src="{{ asset('public/images/MED-Logo.png') }}" class="head-logo padding-t-4 border-top-light"></a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 user-panel padding-tb-8">
                @if(isset($authUser))
                    <span class="link margin-b-4">
                        <a class="powerTip" title="Click to view profile." href="{{ $authUser->hasRole('Patient') ? route('patient.profile', $authUser->username) : route('doctor.profile', $authUser->username) }}">{{ $authUser->name }}</a>
                    </span>
                    <span class="link">
                        <a href="{{ $authUser->hasRole('Patient') ? route('patient.messages_show') : route('messages_show') }}" class="icon inbox powerTip" title="Messages">
                            @if($unread_messages)
                                <span class="noti-count">{{ $unread_messages }}</span>
                            @endif
                        </a>
                    </span>
                    <span class="link"><a href="{{ $authUser->hasRole('Patient') ? route('patient.logout') :  route('doctor.logout') }}" class="icon logout powerTip" title="Logout"></a></span>
                    {{--<span class="link"><a href="{{ URL::to('doctor/about') }}" class="icon help"></a></span>--}}
                @else
                    <span class="link"><a href="{{ route('patient.login') }}" class="icon login powerTip" title="Login"></a></span>
                    <span class="link"><a href="#" class="icon change powerTip" title="Change User" data-toggle="modal" data-target="#whoAmI"></a></span>
                @endif
            </div>
        </div>
    </div>
</section>

<section class="menu">
    <div class="container">
        <nav class="navbar">
          <div class="container-fluid padding-r-0">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <div class="mobile-search temp-hide show-sm">
                @include('doctor.partials._search')
              </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="{{ Request::is('doctor')  ? 'active' : '' }}"><a href="{{ route('patient.home') }}">Home</a></li>
                <li class="{{ strpos(Request::path(), 'articles')  ? 'active' : '' }}"><a href="{{ route('patient.articles.index') }}">KNOW MORE</a></li>
                <li class="{{ strpos(Request::path(), 'discussions')  ? 'active' : '' }}"><a href="{{ route('patient.discussions.index') }}">YOUR VOICE</a></li>
                <li class="{{ strpos(Request::path(), 'surveys')  ? 'active' : '' }}"><a href="{{ route('patient.surveys.index') }}">YOUR OPINION</a></li>
                <li class="{{ strpos(Request::path(), 'doctors')  ? 'active' : '' }}"><a href="{{ route('patient.doctors.index') }}">FIND A DOCTOR</a></li>
                <li class="{{ strpos(Request::path(), 'hospitals')  ? 'active' : '' }}"><a href="{{ route('patient.hospitals.index') }}">HOSPITALS/CLINICS</a></li>
                <li class="pull-right {{ strpos(Request::path(), 'hospitals')  ? 'active' : '' }}"><a href="{{ URL::to('patient/contact') }}">Advertise</a></li>
              </ul>
            </div>
          </div>
        </nav>
    </div>
    @if(isset($authUser))
        @if($authUser->hasRole('Administrator'))
            <a href="{{ url('admin') }}" class="admin-link">Go to backend</a>
        @endif
    @endif
</section>
</header>
