@extends('patient.master')

@section('content')

<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right">
                @include ('doctor.partials._news-ticker')
                <hr class="margin-t-0 margin-b-10">
                @include('patient.templates.about-menu')

                <div class="col-md-9 clearfix row-sm padding-r-0 contents page about">
                    <h3 class="no-border hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix">
                        <span class="page-title doctor-blue">About MED</span>
                    </h3>


                        <h4 class="doctor-blue padding-b-5">For Patients</h4>
                        <p>Patients and the general public can use Middle East Doctor to search the medical directory for doctors and hospitals throughout the region and find suitable healthcare facilities. </p>

                        <p>Users have the option of creating a free account and then gaining access to features such as surveys and discussions. The website is carefully monitored for genuine users, and ethical activity and respectable behaviour. Please refer to the Terms of Use for a guide on how to use Middle East Doctor. </p>

                        <h4 class="doctor-blue padding-b-5">Etiquette</h4>
                        <p>Users must remain courteous at all times and it is highly recommended that users protect their identity and personal information and data. Middle East Doctor does moderate users’ comments according to the Terms of Use. Users who violate the Terms of Use or require regular moderation may have their accounts suspended or removed.</p>
                        <p>Abusive, threatening or offensive remarks will not be tolerated; profanity, name-calling, racial, religious, cultural, political, personal, professional attacks or otherwise negative behaviour are strictly prohibited. Any users engaging in this type of activity can be removed from the website. </p>

                        <p>Middle East Doctor retains all rights to this website and its management. </p>



                </div>
        </div>
        @include('patient.templates.sidebar-ads')
    </div>
</div>
</div>
@stop

@section('js')
    {{--<script src="{{ asset('public/js/tweets.js') }}"></script>--}}
@stop