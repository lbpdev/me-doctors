<div class="row">
<hr class="margin-tb-10">
<h4 class="no-border doctor-blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
    <span class="small-icon patient"></span>
    <span class="font-21">Web Banners - Patient Section</span>
</h4>
<div class="col-md-12 advert-table">
    @for($db=0;$db<5;$db++)
        <div class="row">
            <div class="col-md-5 padding-l-0 padding-tb-10">
            <table>
                <tr>
                    <td>
                        <input id="radioDB{{$db}}" class="square" type="checkbox">
                        <label for="radioDB{{$db}}"><span></span></label>
                    </td>
                    <td class="padding-l-10">
                        728x90 banner - $1 per month <br>
                        (maximum four adverts in rotation)
                    </td>
                </tr>
            </table>
            </div>
            <div class="col-md-3 padding-t-0">
                <div class="col-md-12 no-padding">
                Duration: <br>
                    <div class="small custom-select gray-bg">
                        <select name="country" class="form-control pull-left">
                                <option value="">1 Month</option>
                                <option value="">2 Months</option>
                                <option value="">4 Months</option>
                                <option value="">6 Months</option>
                                <option value="">1 Year</option>
                                <option value="">Forever</option>
                        </select>
                     </div>
                 </div>
            </div>
            <div class="col-md-2 padding-t-0">
                <div class="col-md-12 no-padding">
                Quantity: <br>
                    <div class="small custom-select gray-bg">
                        <select name="country" class="form-control pull-left">
                            @for($dby=1;$dby<10;$dby++)
                                <option value="{{ $dby }}">{{ $dby }}</option>
                            @endfor
                        </select>
                     </div>
                 </div>
            </div>
            <div class="col-md-2 padding-tb-20">
                <a href="#"> See Sample</a>
            </div>
        </div>
    @endfor
    </div>
</div>