<div class="row">
<h4 class="no-border doctor-blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
    <span class="small-icon doctor"></span>
    <span class="font-21">Web Banners - Doctor Section</span>
</h4>
<div class="col-md-12 advert-table">
    @for($x=0;$x<3;$x++)
        <div class="row">
            <div class="col-md-5 padding-l-0 padding-tb-10">
            <table>
                <tr>
                    <td>
                        <input id="radioPC{{$x}}" class="square" type="checkbox">
                        <label for="radioPC{{$x}}"><span></span></label>
                    </td>
                    <td class="padding-l-10">
                        Sponsor Survey
                    </td>
                </tr>
            </table>
            </div>
            <div class="col-md-3 padding-t-0">
                <div class="col-md-12 no-padding">
                Specialty: <br>
                    <div class="small custom-select gray-bg">
                        <select name="country" class="form-control pull-left">
                                <option value="">1 Month</option>
                                <option value="">2 Months</option>
                                <option value="">4 Months</option>
                                <option value="">6 Months</option>
                                <option value="">1 Year</option>
                                <option value="">Forever</option>
                        </select>
                     </div>
                 </div>
            </div>
            <div class="col-md-2 padding-t-0">
                <div class="col-md-12 no-padding">
                Quantity: <br>
                    <div class="small custom-select gray-bg">
                        <select name="country" class="form-control pull-left">
                            @for($y=1;$y<10;$y++)
                                <option value="{{ $y }}">{{ $y }}</option>
                            @endfor
                        </select>
                     </div>
                 </div>
            </div>
            <div class="col-md-2 padding-tb-20">
                <a href="#"> See Sample</a>
            </div>
        </div>
@endfor
</div>
</div>
