@extends('patient.master')

@section('content')
<div class="hs_page_title">
  <div class="container">
    <ul>
      <li><a href="{{ URL::to('/') }}">Home</a></li>
      <li>Answer</li>
    </ul>
  </div>
</div>

<div class="container">
  <!--who we are start-->
  <div class="col-md-8">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h2 class="hs_heading"><i class="fa fa-bullhorn"></i> Answer</h2>
      <div class="row hs_how_we_are">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Let us know what you’re thinking. Answer regular polls and find out what the rest of the physician community think too.</h3>
            <br>
            <p>Very simple questions that you can answer quickly and can help paint a picture of the physician
             population in the Middle East. Have you seen the recent guidelines? Would you prescribe this drug?
             Do you enjoy this aspect of your role? What is your perception of a certain pharmaceutical company?
             Answers are quick and easy and take no more than a few seconds to submit.</p>
            <br>
            <p>Click <a href="#">here</a> for the latest polls and results. </p>

        </div>
      </div>
    </div>
  </div>
  </div>


@include('......templates.sidebar-posts')
</div>


@stop
