<div class="col-md-12 no-padding clearfix">
    <h2 class="section-header">Archives</h2>
    <ul class="media-list">
        @foreach ($archives as $archive)
            <li class="media clearfix height-auto">
               <div class="media-left col-md-4 col-sm-4 col-xs-12 no-padding">
                    <a href="{{ route('patient.articles.show', $archive->slug) }}">
                        @if($archive->thumbnail)
                            <img class="media-object" src="{{ asset($archive->thumbnail) }}" alt="" width="100%">
                        @endif
                    </a>
                </div>
                <div class="media-body height-auto col-md-8 col-sm-8 col-xs-12 padding-tb-20">
                   <a href="{{ route('patient.articles.show', $archive->slug) }}">
                        <h4 class="media-heading">
                             {{ Str::words($archive->title , 9 , '...') }}
                        </h4>
                    </a>
                    @if(Str::length($archive->title) > 70)
                        {{ Str::words(strip_tags($archive->content) , 20, '...') }}
                    @else
                        {{ Str::words(strip_tags($archive->content) , 30, '...') }}
                    @endif
                </div>
            </li>
        @endforeach
    </ul>

    <?php echo $archives->render(); ?>
</div>