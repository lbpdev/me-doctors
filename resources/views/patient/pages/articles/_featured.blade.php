<div class="media-list clearfix" id="latest-articles-car">
    <div class="media featured clearfix margin-b-5">
        <div class="media-body col-md-4 col-sm-6 col-xs-9 ">
            <a href="{{ route('patient.articles.show', $article->slug) }}">
                <h4 class="media-heading margin-t-0">
                        {{ Str::limit($article->title ,57 , '...') }}
                </h4>
            </a>

            @if(strlen($article->title)<30)
                <p>{!! Str::limit(strip_tags($article->content), 370, '...') !!}</p>
            @else
                <p>{!! Str::limit(strip_tags($article->content), 270, '...') !!}</p>
            @endif

            @include('patient.pages.articles.tags')

            <div class="owl-controls clickable"></div>
        </div>
        <div class="media-right col-md-8 col-sm-6 col-xs-3 no-padding">
            <a href="{{ route('patient.articles.show', $article->slug) }}">
                <img class="media-object width-full" src="{{ asset($article->FeaturedImage) }}" alt="">
            </a>
        </div>
    </div>
</div>