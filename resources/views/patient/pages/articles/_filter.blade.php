<div class="col-md-12 clearfix no-padding">
    {!! Form::open(['route'=>'patient.articles_filter']) !!}
    <div class="col-md-10 col-sm-10 no-padding clearfix gray">
        <div class="custom-select left">
            {!! Form::select('therapy' , $therapies , $active , [ 'id' => 'therapy' , 'class' => 'form-control pull-left' ]) !!}
        </div>
    </div>
    <div class="col-md-2 col-sm-2 padding-l-10 padding-r-0 row-xs">
        <a href="{{ url('patient/articles/category/All') }}">
            <span class="action-title pull-right font-18 yellow" style="line-height: 30px;">View All</span>
        </a>
        <button type="submit" class="form-control temp-hide">Filter</button>
    </div>
    {!! Form::close() !!}
</div>