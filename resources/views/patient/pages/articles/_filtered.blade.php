<div class="col-md-12 no-padding clearfix">
    <ul class="media-list">
    @if(count($articles)>0)
        @foreach ($articles as $article)
            <li class="media clearfix height-auto gray-bg">
               <div class="media-left col-md-4 col-sm-4 col-xs-12 no-padding">
                    <a href="{{ route('patient.articles.show', $article->slug) }}">
                        @if($article->thumbnail)
                            <img class="media-object" src="{{ asset($article->thumbnail) }}" alt="" width="100%">
                        @endif
                    </a>
                </div>
                <div class="media-body height-auto col-md-8 col-sm-8 col-xs-12 padding-tb-7">
                    <h4 class="media-heading">
                       <a href="{{ route('patient.articles.show', $article->slug) }}">
                         {{ Str::words($article->title, 9, '...') }}
                        </a>
                    </h4>

                    @if(Str::length($article->title) > 70)
                        {{ Str::words(strip_tags($article->content), 20, '...') }}
                    @else
                        {{ Str::words(strip_tags($article->content), 60, '...') }}
                    @endif
               </div>
            </li>
        @endforeach
    @else
        No Articles for this section
    @endif
    </ul>


    <?php echo $articles->render(); ?>
</div>