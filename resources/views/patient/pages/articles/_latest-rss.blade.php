@if ($articles)
    <div class="col-md-4 col-sm-12 rssfeeds padding-l-10 padding-r-0 row-sm">
        <ul class="media-list rss minimal">
            @foreach ($articles as $article)
                <li class="media">
                     <div class="media-body padding-10">
                        <h4 class="media-heading">
                           <a href="{{ route('patient.articles.show', $article->slug) }}">
                                {{ Str::words($article->title, 3, '...') }}
                            </a>
                        </h4>
                        {!! Str::words(strip_tags($article->content), 3, '...') !!}

                        @include('patient.pages.articles.tags')
                    </div>
                 </li>
            @endforeach
        </ul>
    </div>
@endif