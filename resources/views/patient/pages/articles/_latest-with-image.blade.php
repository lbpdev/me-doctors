@foreach($articles as $article)
    <div class="col-md-6 padding-l-10 clearfix article border-right-light">
        <div class="media-body col-md-12 col-sm-12 col-xs-12 no-padding margin-r-10">
            <div class="media-left col-md-12 col-sm-6 col-xs-12 no-padding">
                <a href="{{ route('patient.articles.show', $article->slug) }}">
                    @if ($article->thumbnail)
                        <img class="media-object width-full" src="{{ asset($article->thumbnail) }}" alt="">
                    @endif
                </a>
            </div>
            <div class="media-body col-md-12 col-sm-6 col-xs-12 col-xs-12 padding-b-10">
                <h4 class="media-heading">
                    <a href="{{ route('patient.articles.show', $article->slug) }}">
                      {{ Str::words($article->title , 6 , '...') }}
                    </a>
                </h4>

                @if(Str::length($article->title) > 70)
                    {!! Str::words(strip_tags($article->content), 10, '...') !!}
                @else
                    {!! Str::words(strip_tags($article->content), 15, '...') !!}
                @endif

                @include('patient.pages.articles.tags')
            </div>
        </div>
    </div>
@endforeach