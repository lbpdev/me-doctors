@if ($articles)
    <div class="col-md-4 col-sm-12 rssfeeds padding-l-10 padding-r-0 row-sm">
        <ul class="media-list rss minimal padding-l-10 border-left-light">
            @foreach ($articles as $article)
                <li class="media">
                     <div class="media-body">
                        <h4 class="media-heading">
                           <a href="{{ route('patient.articles.show', $article->slug) }}">
                                {{ Str::limit($article->title, 12, '...') }}
                            </a>
                        </h4>
                        {!! Str::limit(strip_tags($article->content), 105, '...') !!}

                        @include('patient.pages.articles.tags')
                    </div>
                 </li>
            @endforeach
        </ul>
    </div>
@endif