@extends('patient.master')

@section('content')
    <div class="container articles-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 border-right ">
                @include ('patient.partials._news-ticker')
                <hr class="no-margin pull-left">
                @include('patient.pages.articles._filter')

                <hr class="margin-t-0 margin-b-4 pull-left">
                @if(isset($_GET['page']))
                    @include ('patient.pages.articles._archives')
                @endif
                    <div class="discussions-rss clearfix">
                        @if ($featured)
                            @include ('patient.pages.articles._featured', ['article' => $featured])
                        @endif
                    </div>
                    
                    <div class="col-md-12 no-padding clearfix articles">
                        @include ('patient.pages.articles.popular')
                    </div>

                    {{--<div class="voffset4 clearfix f-left"></div>--}}
                    {{--@if(!isset($_GET['page']))--}}
                        {{--@include ('patient.pages.articles._archives')--}}
                    {{--@endif--}}

                    @if(!isset($_GET['page']) && $authUser)
                        @include ('patient.pages.articles.latest')
                    @endif


                    @if(!isset($_GET['page']) && $authUser)
                        @include ('patient.partials.surveys.latest')
                    @endif

                    <div class="col-md-12 no-padding clearfix blue">
                        @include ('patient.templates.videos.videos')
                    </div>

                </div>

                @include('patient.pages.articles.sidebar')
            </div>
        </div>
    </div>
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/videosCarousel.js') }}"></script>
    <script>
        $('#therapy').on('change',function(e){
            $(this).closest('form').trigger('submit');
        });
    </script>
@stop