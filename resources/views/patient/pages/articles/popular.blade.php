<div class="col-md-12 pull-left articles">
    <div class="row">
        <h4 class="section-header margin-t-0">{{ strpos(Request::path(), 'articles')  ? 'POPULAR' : 'KNOW MORE' }} </h4>
        @if (count($popularArticles)>0)
            <div class="col-md-8 no-padding">
                @include ('patient.pages.articles._popular-with-image', [
                    'articles' => array_slice($popularArticles->all(), 0, 2)
                ])
            </div>
            @include ('patient.pages.articles._popular-rss', [
               'articles' => array_slice($popularArticles->all(), 2)
            ])
        @else
            <p>No Popular Articles to Show.</p>
        @endif
    </div>
</div>