<div class="clearfix col-md-3 col-sm-12 sidebar padding-r-0 ">
   @include ('patient.partials._search')

    <div class="panel with-nav-tabs panel-default clearfix latest-tabs">
        <div class="col-md-12 no-padding">

            @if ($authUser)
                  @include('patient.partials._top-articles')
                  @include('patient.partials._popular-discussions')
            @endif

            <div class="sidebar-element clearfix">
                <div class="col-md-12 no-padding side-ad">
                    @if(!$authUser)
                        <a href="{{ route('patient.login') }}">
                            <img src="{{ asset('public/images/ads/register-side.png') }}" width="100%" alt="register med">
                        </a>
                    @endif
                    
                    @include('templates.sidebar-long')
                    @include('templates.box-event')
                </div>
            </div>
        </div> 
    </div>
</div>