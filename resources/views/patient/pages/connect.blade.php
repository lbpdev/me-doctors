@extends('patient.master')

@section('content')
<div class="hs_page_title">
  <div class="container">
    <ul>
      <li><a href="{{ URL::to('/') }}">Home</a></li>
      <li>Connect</li>
    </ul>
  </div>
</div>

<div class="container">
  <!--who we are start-->
  <div class="col-md-8">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-lg-12 col-md-12 col-sm-12">
      <h2 class="hs_heading"><i class="fa fa-bullhorn"></i> Connect</h2>
      <div class="row hs_how_we_are">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>Choose to allow patients to view your profile and get in contact to book appointments with you. </h3>
            <br>
            <p>Make your profile visible to the public so that patients looking for a specialist can easily find
            your contact details or book an appointment. You can include your workplace, educational and professional
             qualifications, accreditations and key specialist areas. There is also a section to upload a professional
              statement where you can appeal directly to the patient population and talk about your skills and experience.</p>
            <br>
            <p>Create your profile <a href="#">here</a>. </p>
            <p>Update your profile <a href="#">here</a>. </p>


        </div>
      </div>
    </div>
  </div>
  </div>
@include('......templates.sidebar-posts')
</div>


@stop
