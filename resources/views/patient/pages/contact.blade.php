@extends('patient.master')

@section('content')
<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                @include ('patient.partials._news-ticker')
                <hr class="margin-b-5 margin-t-0">
                    <h3 class="no-border yellow hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix">
                        <span class="page-icon contact"></span>
                        <span class="page-title doctor-blue">Contact</span>
                    </h3>
                    @if(Session::get('message'))
                        <p class="alert padding-20 margin-t-20 alert-success font-18">{{ Session::get('message') }}</p>
                    @else
                        {!! Form::open(['route'=>'send_mail','id' => 'contactForm']) !!}
                        <div class="col-md-3 no-padding gray-bg">
                            <div class="custom-select" style="height: 35px; background-position: 95% center;">
                                 <select class="form-control font-15" name="topic">
                                     <option value="Advertise">Advertise</option>
                                     <option value="Editorial">Editorial</option>
                                     <option value="Registration">Registration</option>
                                     <option value="General">General</option>
                                 </select>
                             </div>
                        </div>
                        <div class="col-md-9 padding-r-0">
                            {!! Form::input('text' , 'subject', '' , ['required', 'class' => 'form-control' , 'placeholder' => 'Subject']); !!}
                        </div>

                        <div class="col-md-12 no-padding gray-bg margin-t-10">
                            {!! Form::input('text' , 'name', '' , ['required', 'class' => 'form-control' , 'placeholder' => 'Full Name']); !!}
                        </div>

                        <div class="col-md-6 no-padding gray-bg margin-t-10">
                            {!! Form::input('text' , 'email', '' , ['required', 'class' => 'form-control' , 'placeholder' => 'E-mail']); !!}
                        </div>

                        <div class="col-md-6 padding-r-0 margin-t-10">
                            {!! Form::input('text' , 'contact', '' , ['class' => 'form-control' , 'placeholder' => 'Contact Number']); !!}
                        </div>

                        <div class="col-md-6 padding-r-0 margin-t-10">
                            {!! Form::hidden( 'url', '' ); !!}
                        </div>

                        <div class="col-md-12 margin-t-10">
                            <div class="row">
                                {!! Form::textarea('content' , ''  , ['required', 'class' => 'form-control padding-10' , 'placeholder' => 'Write message here...']) !!}
                            </div>
                        </div>

                        <div class="col-md-12 margin-t-10">
                            <div class="row">
                                <div class="col-md-10 padding-l-0">
                                    {!! Form::input('text' , 'code', '' , ['required', 'id' => 'code' ,'class' => 'form-control' , 'placeholder' => 'CAPTCHA']); !!}
                                </div>
                                <div class="col-md-2 padding-r-0">
                                    <img src="{{ asset('publicplugins/captchastylish/get_captcha.php') }}" alt="" id="captcha" />
                                </div>
                            </div>
                        </div>


                        {!! Form::close() !!}
                        {!! Form::button('Send', ['id' => 'submit' ,'style' => 'height: 35px;' , 'class' => 'pull-left pink button margin-t-0 t-orange font-22 ' , 'placeholder' => 'Subject']); !!}

                    @endif

                </div>
              </div>

          <div class="hs_margin_40"></div>
        </div>

        @include('patient.templates.sidebar-ads')
        </div>
    </div>
</div>
@stop


@section('js')
    <script>
    $(document).ready(function() {


     $('#submit').click(function(e) {
        // name validation

        $.post("{{ asset('publicplugins/captchastylish/post.php') }}?"+$("#contactForm").serialize(), {

        }, function(response){

        console.log(response.toString()+' == '+$('#code').val().toString());

        if(response.trim() == $('#code').val().toLowerCase().trim())
        {
            console.log('pasok');
            $("#contactForm").submit();
            return true;
        }
        else
        {
            console.log('mali');
            if($('#after_submit').length < 1)
                $("#submit").after('<label class="alert alert-danger" id="after_submit">Error ! Invalid captcha code .</label>');
            return false;
        }

        });
        e.preventDefault();
     });


     // refresh captcha
     $('img#refresh').click(function() {
    		change_captcha();
     });

     function change_captcha()
     {
    	document.getElementById('captcha').src="{{ asset('publicplugins/captchastylish/get_captcha.php') }}?rnd=" + Math.random();
     }

    });
    </script>
@stop