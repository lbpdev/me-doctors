<div class="hs_single_profile_detail">
  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
            {!! Form::input('text','title' , isset($data->title) ? $data->title : '' , [ 'placeholder' => 'Title...', 'required' , 'class' => 'padding-l-7  white-bg form-control' ]) !!}
        </div>
        <div class="form-group">
            {!! Form::select('therapy_id', $therapies, isset($data->therapy_id) ? $data->therapy_id : '1' , [ 'class' => 'padding-l-7  white-bg form-control' ]) !!}
        </div>
        <div class="form-group">
            {!! Form::textarea('content', isset($data->content) ? $data->content : null , [ 'placeholder' => 'About...', 'required', 'class' => 'padding-l-7 form-control  white-bg resize-v-only' ]) !!}
        </div>
        <div class="form-group temp-hide">
            {!! Form::label('attachment', 'Attachment: (Optional)') !!}
            {!! Form::input('file', 'attachment[]' , null  , [ 'multiple' => true, 'class' => 'form-control' ]) !!}
        </div>
    </div>
</div>
 <div class="form-group">
   <div class="col-lg-12 col-md-12 col-sm-12">
   <div class="row">
     {!! Form::submit('Post', [ 'class' => 'green pull-right margin-t-10 button font-24 robotomedium padding-r-0' ]) !!}
   </div>
 </div>
</div>