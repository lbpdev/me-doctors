@if ($post->attachments->count())
    <div class="well well-sm margin-t-10">
        Attachment: <br>
        @foreach ($post->images as $image)
            <a id="single_image" href="{{ url($image['original']) }}">
                <img src="{{ url($image['thumb']) }}" width="100">
            </a>
        @endforeach

        @foreach ($post->files as $file)
            <a target="_blank" href="{{ url('download/' . $file->id) }}">{{ $file->original_name }}</a>
        @endforeach
    </div>
@endif