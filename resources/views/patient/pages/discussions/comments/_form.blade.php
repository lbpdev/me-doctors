{!! Form::input('hidden','discussion_id' , $discussion->id , [ 'class' => 'form-control' , 'rows' => '8' ]) !!}
{!! Form::textarea('content', null, [ 'class' => 'form-control white-bg resize-v-only' , 'rows' => '8' , 'required', 'placeholder' => 'Write comment here...'  ]) !!}

{{--{!! Form::label('file', 'Attachment: ( Optional )') !!}--}}
{!! Form::input('file', 'attachment[]' , null , [ 'class' => 'form-control hidden', 'multiple' => true ]) !!}
