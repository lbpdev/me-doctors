 <div class="hs_comment_form clearfix pull-left width-full margin-t-7 border-top pink clearfix col-md-12">

    <h2 class="page-header-light pink no-border margin-t-0">Add your comment</h2>

    @if($authUser->verified)
    {!! Form::open(['route' => ['patient.discussions.comments.store', $discussion->id], 'files' => true]) !!}

    @include('patient.pages.discussions.comments._form')
    
        <div class="hs_margin_40 clearfix"></div>
            <div class="form-group">
              <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="row">
                {!! Form::submit('Post', [ 'class' => 'pink pull-right button font-24 robotomedium padding-r-0' ]) !!}
              </div>
            </div>
        </div>
    {!! Form::close() !!}
    @else
        <p class="t-green margin-b-10">Note: Only verified users may comment on discussions.
        Please check your email or click <a class="" href="#" data-toggle="modal" data-target="#reVerifyModal">Here</a>
        to resend verification.
        </p>
    @endif
</div>

@include('patient.templates.modals.reverify')