@extends('patient.master')

@section('content')
    <div class="container page-container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 border-right">
                @include ('patient.partials._news-ticker')
                <hr class="margin-t-0 margin-b-4">
                <h3 class="no-border hs_heading margin-b-0 padding-b-0 margin-t-0 pink clearfix">
                    <span class="page-icon discussion"></span>
                    <span class="page-title">Discussion</span>
                @if($authUser)
                    <div class="padding-t-5 pink pull-right pull-left-xs col-md-6 col-sm-6 col-xxs-12 clearix row-sm padding-r-0 margin-t-0">
                        <a class="pull-right t-pink font-15 action-title" href="{{ route('patient.discussions.index') }}">Cancel</a>
                    </div>
                @endif
                </h3>
                <hr class="margin-t-5 margin-b-4">
                    @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif
                    <div class="hs_comment_form clearfix margin-t-7 border-top pink clearfix col-md-12">
                        <h2 class="page-header-light pink no-border margin-t-0">Create a new Discussion</h2>
                        {!! Form::open(['files' => true , 'route' => 'patient.discussions.store']) !!}
                            @include('patient.pages.discussions._form')
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="clearfix"></div>
                @include ('patient.partials.errors')
            </div>

                @include('patient.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop
