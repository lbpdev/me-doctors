@extends('patient.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-md-9 col-sm-12 border-right">

                @include ('patient.partials._news-ticker')
                <div class="col-md-12 padding-b-0 padding-t-0 clearfix margin-b-5 margin-t-0">
                    <div class="row">
                        <hr class="margin-b-5 margin-t-0">
                        <h3 class="no-border pink hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-icon discussion"></span>
                            <span class="page-title">Discussions</span>
                        </h3>
                        <a class="pull-right t-green font-15 pink action-title" href="{{ route('doctor.diagnosis.index') }}">Back</a>
                    </div>
                </div>
                <hr class="no-margin">
                @if(Session::get('message'))<span class="alert alert-success margin-b-10 pull-left">{{ Session::get('message') }}</span>@endif

                @include('patient.pages.discussions.single')
                <h2 class="section-header pink">Comments</h2>
                @include('patient.pages.discussions.comments.index')

                @include('patient.pages.discussions.comments.create_panel')

                @include ('patient.partials.discussions.latest')
            </div>

            @include('patient.pages.discussions.sidebar')
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
@stop