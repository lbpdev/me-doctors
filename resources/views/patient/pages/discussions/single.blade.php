<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="left-pad-20 discussion-item">
            <h3 class="pink padding-b-0 margin-t-5 margin-b-0 clearfix">
               <a href="{{ route('doctor.diagnosis.show', $discussion->slug) }}" class="t-green pull-left">
                    {{ $discussion->title }}
                </a>

                @if($discussion->therapy)
                    <span class="font-12 margin-l-5 pull-left vertical">({{ $discussion->therapy->name }})</span>
                @endif
            </h3>
            <div class="padding-b-5 col-md-12">
                <div class="row">
                    <!-- <span class="update font-14">Submitted {{ $discussion->created_at->diffForHumans() }}</span><br> -->
                    <span class="comments-count">{{ $discussion->commentsCount }} comments</span>
                    <hr class="margin-t-4 margin-b-0 border-light">
                </div>
            </div>

            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3 margin-t-4">
                <div class="row avatar">
                    <a href="{{ $discussion->author->id == 1 ? '#' : route('doctor.profile', $discussion->author->username) }}">
                        <img src="{{ url($discussion->author->avatar) }}" width="100%">
                    </a>
                    <a href="{{ $discussion->author->id == 1 ? '#' : route('doctor.profile', $discussion->author->username)}}" class="profile-link big">View Profile</a>
                </div>
            </div>
            <div class="col-lg-11 col-md-11 col-xs-3 ">
                <a href="{{ $discussion->author->id == 1 ? '#' : route('doctor.profile', $discussion->author->username)}}">
                    <h4 class="margin-t-0 margin-b-0 black">{{ $discussion->author->name }}</h4>
                </a>
            </div>

            <div class="col-lg-11 col-md-11 col-xs-12 row-sm">
                <div class="discussion-content content margin-tb-5">{!! $discussion->content !!}</div>

                @include('doctor.pages.discussions.comments._attachments', ['post' => $discussion])

            </div>
        </div><!-- End post Content -->
    </div><!-- End Col -->

</div><!-- End Row -->