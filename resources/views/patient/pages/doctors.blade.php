@extends('patient.master')

@section('content')
    <div class="hs_page_title">
      <div class="container">
        <h3>Services Four Column</h3>
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="services.html">Services Four Column</a></li>
        </ul>
      </div>
    </div>
    <div class="container">
      <div class="col-md-12 align-c pull-down-62 hs_portfolio index_portfolio">
        <!-- /.title -->
        <h4 class="hs_heading">Services Available</h4>
        <ul class="push-down-62 portfolio-filter" data-scrollreveal="enter from the bottom over 1s but wait 0.5s">
          <li><a href="#" class="filter-item active" data-filter="all">All Services</a></li>
          <li><a href="#" class="filter-item" data-filter="Pediatric">Pediatric Clinic</a></li>
          <li><a href="#" class="filter-item" data-filter="Cancer">Cancer Care</a></li>
          <li><a href="#" class="filter-item" data-filter="General">General Surgery</a></li>
          <li><a href="#" class="filter-item" data-filter="Physiotherapy">Physiotherapy</a></li>
        </ul>
        <!-- /.push-down-62 portfolio-filter -->
      </div>
      <div class="clearfix"></div>
      <div class="row push-down-62 portfolio-grid index_portfolio_content" id="grid">
        <div class="col-md-3 col-sm-6 portfolio-item Pediatric Cancer mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Pediatric Clinic</h3>
          <div class="portfolio-details">
            <h4>Pediatric Clinic</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Cancer General mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Dental Clinic</h3>
          <div class="portfolio-details">
            <h4>Dental Clinic</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item General Physiotherapy printing mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>General Surgery</h3>
          <div class="portfolio-details">
            <h4>General Surgery</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Cancer printing Physiotherapy mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Physiotherapy</h3>
          <div class="portfolio-details">
            <h4>Physiotherapy</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Pediatric Cancer mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Emergency Services</h3>
          <div class="portfolio-details">
            <h4>Emergency Services</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Pediatric Cancer Physiotherapy mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Pregnancy Care</h3>
          <div class="portfolio-details">
            <h4>Pregnancy Care</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Cancer printing Physiotherapy mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Cancer Care</h3>
          <div class="portfolio-details">
            <h4>Cancer Care</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Pediatric Cancer mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Occupational Therapy</h3>
          <div class="portfolio-details">
            <h4>Occupational Therapy</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
        <div class="col-md-3 col-sm-6  portfolio-item Pediatric Cancer Physiotherapy mix"><img src="http://placehold.it/480x555" alt="" width="380" height="300"/>
          <h3>Ward</h3>
          <div class="portfolio-details">
            <h4>Ward</h4>
            <p>Fusce auctor magna aus feugiat ultricies. Donec aliquet dolor elit volutpat neque pulvinar eu. Nullam dapibus metus sed dui congue congue. </p>
            <a href="" class="btn btn-success">Read More</a> </div>
        </div>
        <!-- /.col-md-4 portfolio-item mix -->
      </div>
    </div>
@stop
