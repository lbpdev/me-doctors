 <div class="col-md-12 no-padding clearfix">
    <div class="col-md-12">
        <div class="row">
            <ul class="directory-list">
                @include('patient.pages.doctors._featured-template')
                @include('patient.pages.doctors._non-featured')
                @if (count($doctors)>0)
                    @foreach ($doctors as $index=>$doctor)
                        @include('patient.pages.doctors._doctor')
                    @endforeach
                @else
                    <p class="margin-t-20">There are currently no doctors for this section.</p>
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>