<div id="non-feat-template" class="temp-hide doctor-item pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 col-xs-12 margin-r-0 border-bottom border-light  padding-lb-10-xs">
{{--<div class="doctor-item pull-left item padding-tb-10 clearfix col-lg-12 col-md-12  col-xs-12  col-sm-12 col-xxs-12 margin-r-0 border-bottom border-light padding-r-0">--}}
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">

            <div class="col-lg-2 col-md-2 col-xs-3 padding-lb-10-xs padding-b-10-xs ">
            {{--<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 col-xxs-4 ">--}}
                <div class="row text-left avatar">
                    <a href="">
                        <img src="" width="100" class="avatar">
                        {{--<img src="{{ url($doctor->avatar) }}" width="100%" class="avatar">--}}
                    </a>
                </div>
            </div>

            <div class="col-lg-10 col-md-10 col-xs-9 row-sm padding-r-0 padding-lb-10-xs">
            {{--<div class="col-lg-10 col-md-10 col-sm-10 padding-l-10 col-xs-9 col-xxs-8 padding-r-0">--}}
                <a class="doctor-username" href="">
                    <h4 class="margin-t-0 padding-t-0 margin-b-0 black doctor-name"></h4>
                </a>
                <div class="detail-line">
                    <i class="fa fa-stethoscope"></i>
                    <span class="doctor-specialties">
                    </span>
                    <br class="xxs-show temp-hide">
                    <span class="small-icon hospital"></span> <span class="doctor-hospital">
                    </span>
                </div>

                <div class="detail-line">
                    <span class="small-icon phone"></span> <span class="doctor-contact">
                    </span> &nbsp;
                    <br class="xxs-show temp-hide">
                    <span class="small-icon location"></span>
                    <span class="doctor-work">
                    </span>
                </div>
                <hr class="margin-tb-5 xxs-hide">
                <div class="margin-t-5 xxs-hide">
                    <span class="doctor-ratings">
                        <i class="rate-icon ri-friendliness powerTip" title="Friendliness"></i> <span class="friendliness"></span>
                        <i class="rate-icon ri-professionalism margin-l-10 powerTip" title="Professionalism"></i> <span class="professionalism"></span>
                        <i class="rate-icon ri-bed-manner margin-l-10 powerTip" title="Bed-side Manners"></i> <span class="bedside"></span>
                        <i class="rate-icon ri-medical-knowledge powerTip" title="Medical Knowledge"></i> <span class="medical"></span>
                        <i class="rate-icon ri-helpfulness margin-l-10 powerTip" title="Helpfulness"></i> <span class="helpfulness"></span>
                        <i class="rate-icon ri-waiting-time powerTip" title="Response Time"></i> <span class="waiting"></span>
                    </span>
                </div>
                <hr class="margin-tb-5 xxs-hide">
                <div class="margin-tb-5 xxs-hide">
                    <span class="doctor-details">
                    </span>
                </div>
            </div>
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->