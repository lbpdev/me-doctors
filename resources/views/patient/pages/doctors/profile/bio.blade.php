@if(isset($user->profile))
    @if($user->profile->bio)
    <hr class="margin-t-10 margin-b-5 pull-left">
    <div class="col-md-12 margin-t-0">
        <div class="row">
            <h1 class="pink margin-t-0 margin-b-0">Professional Statement</h1>
            @if(isset($user->profile))
                <p>{!! $user->profile->bio ?  $user->profile->bio :  'N/A'; !!}</p>
            @else
                N/A
            @endif
        </div>
    </div>
    @endif
@endif