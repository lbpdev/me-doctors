<div class="section col-md-12 margin-b-7 padding-l-0">
    <div class="col-md-12 padding-l-0">

    <h4 class="clearfix margin-t-0 margin-b-0">Languages Spoken
    @if($authUser)
        @if(isset($authUser) && $authUser->username == $user->username)
            <button class="button t-light-gray add-toggle">[ <i class="fa fa-edit"></i> EDIT ]</button>
        @endif
    @endif
    </h4>
        <div id="languages" class="dataList">
        @if(isset($user->languages) && count($user->languages) > 0)
            @foreach ($user->languages as $language)
                <span>{{ $language['name'] }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>
        <div class="alert alert-success clearfix pull-left  temp-hide margin-t-0"> Successfully Updated.</div>

    @if($authUser)
        @if($authUser->username == $user->username)
        </div>
        <div class="add-form clearfix padding-l-0  well-sm col-md-12">
            <h5><i class="fa fa-plus-square-o"></i> Update Languages</h5>
        @if($agent != "Internet Explorer")
            {!! Form::open(['method' => 'POST', 'class' => 'dataForm', 'route' => ['doctor.languages.update', $user->username]]) !!}
        @else
            {!! Form::open(['method' => 'PUT', 'route' => ['doctor.languages.updateIE', $user->username]]) !!}
        @endif
                <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
                <div class="fields">
                    <select name="languages[][language_id]" multiple="multiple" class="margin-b-5">
                        @foreach($languages as $language)
                            <option value="{{ $language['id'] }}">{{ $language['name'] }}</option>
                        @endforeach


                        @foreach($user->languages as $user_language)
                            @foreach($languages as $language)
                                @if($user_language['id'] == $language['id'])
                                    <option selected value="{{ $user_language['id'] }}">{{ $user_language['name'] }}</option>
                                @endif
                            @endforeach
                        @endforeach

                    </select>
                </div>

                <button class="button add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                <button type="submit" class="button add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
            {!! Form::close() !!}
        @endif
    @endif
    </div>
</div>