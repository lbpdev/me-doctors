<div class="section col-md-12 margin-b-7 padding-l-0">
    <div class="col-md-12 padding-l-0">

    <h4 class="clearfix margin-t-0 margin-b-0">Publications
    @if($authUser)
        @if(isset($authUser) && $authUser->username == $user->username)
            <button class="button t-light-gray add-toggle">[ <i class="fa fa-edit"></i> EDIT ]</button>
        @endif
    @endif
    </h4>
        <div id="publications" class="dataList">
        @if(isset($user->publications) && count($user->publications) > 0)
            @foreach ($publications = $user->publications as $publication)
                <span>{{ $publication->name }}</span><br>
            @endforeach
        @else
            N/A
        @endif
        </div>

        <div class="alert alert-success clearfix temp-hide pull-left margin-t-0"> Successfully Updated.</div>

    @if($authUser)
        @if($authUser->username == $user->username)
        </div>
        <div class="add-form clearfix  padding-l-0 well-sm col-md-12">
            <h5><i class="fa fa-plus-square-o"></i> Update Publication</h5>
        @if($agent != "Internet Explorer")
            {!! Form::open(['method' => 'POST', 'class' => 'dataForm',  'route' => ['doctor.publications.update', $user->username]]) !!}
        @else
            {!! Form::open(['method' => 'PUT', 'route' => ['doctor.publications.updateIE', $user->username]]) !!}
        @endif
                <div class="alert alert-danger clearfix temp-hide"> An error has occurred. Please try again.</div>
                <div class="fields">

                    @if(isset($publications))
                        @foreach ($publications as $index => $publication)
                            <input type="text" name="publications[{{$index}}][name]" placeholder="Publication" class="margin-b-5 form-control" value="{{ $publication->name }}">
                        @endforeach
                    @endif

                    <input type="text" name="publications[{{ isset($publications) ? $publications->count() : 0 }}][name]" placeholder="Publication" class="margin-b-5 form-control">
                </div>
                <button class="button pull-right add_field_bt margin-l-10  fa fa-plus"></button>
                <button rel="1" class="button pull-right remove_field_bt fa fa-minus"></button>

                <button class="button add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                <button type="submit" class="button add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
            {!! Form::close() !!}
        @endif
    @endif
    </div>
</div>