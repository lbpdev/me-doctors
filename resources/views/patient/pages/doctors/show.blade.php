@extends('patient.master')

@section('content')
    <style>
        body {
            /*overflow-y: scroll !important;*/
        }
    </style>
    <div class="container profile page-container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 border-right">
                <div class="hs_single_profile">

                @include ('patient.partials._news-ticker')

                    @include ('patient.pages.doctors.profile.personal')
                    <hr class="margin-tb-5">

                    @if($user->id != 1)

                    {{--@if($authUser->username == $user->username)--}}
                        {{--@include ('doctor.pages.users.profile.account-type')--}}
                        {{--<hr class="margin-t-10 margin-b-5 pull-left">--}}
                    {{--@endif--}}

                    <div class="col-md-12">
                        <div class="row">
                            <h1 class="pink margin-t-7">Qualifications</h1>
                            <div class="col-md-12 qualifications">
                                <div class="row">
                                
                                    @include ('patient.pages.doctors.profile.education')

                                    @include ('patient.pages.doctors.profile.awards')

                                    @include ('patient.pages.doctors.profile.publications')

                                    @include ('patient.pages.doctors.profile.languages')

                                    @include ('patient.pages.doctors.profile.certifications')

                                    @include ('patient.pages.doctors.profile.bio')

                                    <hr class="margin-t-10 margin-b-5 pull-left">

                                    @include ('patient.pages.doctors.profile.work')

                                </div>
                            </div>
                    @endif
                        </div>
                    </div>
                    </div>
                    @include('patient.templates.sidebar-ads')
                </div>
        </div>
    </div>

    <div class="hs_margin_40"></div>

    @include('patient.templates.modals.rating-help')
    @include('patient.templates.modals.rating')

@stop
@section('js')
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

    <script type="text/javascript">

          function initMap() {
          $('.map').each(function(e , item){
              var map = new google.maps.Map(item, {
                  zoom: 6,
                  disableDefaultUI: true,
                  draggable: true,zoomControl: true,scrollwheel: true,disableDoubleClickZoom: true
              });
              var geocoder = new google.maps.Geocoder();

              geocodeAddress(geocoder, map , item);
          });
        }

        function geocodeAddress(geocoder, resultsMap ,item) {
          var address = $(item).attr('data-location');

          if(address=="")
              address = "Dubai - United Arab Emirates";

          geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
              resultsMap.setCenter(results[0].geometry.location);
              var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
              });
            } else {
              $(item).hide();
            }
          });
        }
        initMap();

    </script>

@stop
