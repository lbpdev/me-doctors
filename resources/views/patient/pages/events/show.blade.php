@extends('patient.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8 page-container border-right">

                <h4 class="hs_heading">{{ $event->title }}</h4>
                <div class="hs_margin_30"></div>

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="post-content left-pad-20">
                        @if(isset($event->largeThumb))
                            <img src="{{ asset( $event->largeThumb) }}" width="50%" class="article-image pull-right margin-b-10 margin-t-15 margin-l-10">
                        @endif

                        <div class="content padding-tb-10">
                            Where: {{ $event->location }} <br> <br>
                            When: {{ $event->event_date->format('Y-m-d') }} <br> <br>
                            {!! $event->content !!}
                        </div>

                        <div class="col-md-12 no-padding">
                            <div class="map" style="height: 300px;width:100%;">
                            </div>
                        </div>

                      </div><!-- End post Content -->
                    </div><!-- /Col -->
                </div><!-- /Row -->
            </div>

            @include ('patient.templates.sidebar-ads')
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/custom/addBlankOnLinks.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

        <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementsByClassName('map')[0], {
                zoom: 12,
                disableDefaultUI: true,
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map , 0);
          }

          function geocodeAddress(geocoder, resultsMap ,index) {
            var address = "<?php echo preg_replace( "/\r|\n/", "", $event->location ? $event->location : ''); ?>";
            var map = $('.map')[index];

            geocoder.geocode({'address': address}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                $(map).find('.gm-style').show();
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
              } else {
              $(map).find('.gm-style').hide();
              }
            });
          }

          if(document.getElementsByClassName('map').length > 0)
            initMap();
        </script>
@stop