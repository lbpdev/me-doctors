@extends('patient.master')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/js/style/addtohomescreen.css') }}">
    @if(isset($landing))
        <style>
            body.modal-open {
                overflow: hidden;
            }
            html { overflow: hidden; }
        </style>
    @endif
@stop

@section('content')
    <section class="container">
        <div class="clearfix row">
            <div class="col-md-9 col-sm-8 border-right margin-t-5">

                @include ('patient.partials._news-ticker')

                @include ('patient.pages.home.articles.latest')

                @include ('patient.pages.home.doctors.wrapper')

                @include ('patient.pages.home.hospitals.wrapper')

                @include ('patient.pages.home.surveys.wrapper')

                @include ('patient.pages.home.discussions.wrapper')

                @include ('patient.templates.videos.videos')

            </div>

            @include ('patient.pages.home.sidebar')
        </div>
    </section>

    @if(isset($landing))
        <button data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#whoAmI" class="temp-hide" id="modalBT"></button>
        <div class="modal fade who-am-i" id="whoAmI" tabindex="-1" role="dialog" aria-labelledby="whoAmILabel">
                {{-- WHO AM I --}}
                    <div class="modal-dialog" role="document" style="margin-top: 11%;">
                        <div class="modal-content"  style="border-radius: 30px;">
                            <div class="modal-header text-center">
                                <h4 class="modal-title" id="whoAmILabel">WHO AM I?</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-offset-1 col-xs-5 who-item" data-login='{"text":"DOCTOR LOGIN","image":"#doctorImage"}'>
                                         <a href="{{ URL('doctor') }}">
                                             <img src="{{ asset('public/images/icon/who-am-i/doctor.png') }}" alt="Doctor">
                                             <span>DOCTOR</span>
                                         </a>
                                    </div>
                                    <div class="col-xs-5 who-item" data-login='{"text":"PATIENT LOGIN","image":"#patientImage"}'>
                                         <a href="{{ URL('patient') }}">
                                             <img src="{{ asset('public/images/icon/who-am-i/patient.png') }}" alt="Patient">
                                             <span>PATIENT</span>
                                         </a>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-offset-1 col-xs-5 who-item">
                                     <a href="{{ URL('patient') }}">
                                        <img src="{{ asset('public/images/icon/who-am-i/facility.png') }}" alt="Healthcare Facility">
                                        <span>HEALTHCARE FACILITY</span>
                                     </a>
                                    </div>
                                    <div class="col-xs-5 who-item">
                                     <a href="{{ URL('patient') }}">
                                        <img src="{{ asset('public/images/icon/who-am-i/other.png') }}" alt="Other">
                                        <span>OTHER</span>
                                     </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    @endif
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/eventsCarousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/videosCarousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/articlesCarousel.js') }}"></script>
    <script src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('public/js/addtohomescreen.min.js') }}"></script>
    <script>

    @if(isset($landing))
       $(document).ready(function(){
            setInterval(function(){
                $('#modalBT').trigger('click');
            },1000)

            $('body').css('overflow','hidden');
       });
    @endif
    </script>
@stop