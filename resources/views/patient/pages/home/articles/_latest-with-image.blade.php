@foreach($articles as $article)
    <div class="col-md-6 col-sm-6 clearfix padding-l-10 article border-right-light">
        <div class="col-md-12 col-sm-12 no-padding col-xs-12">
            <div class="media-left col-md-12 col-sm-12 col-xs-6 col-xxs-12 no-padding">
                <a href="{{ route('patient.articles.show', $article->slug) }}">
                    @if ($article->thumbnail)
                        <img class="media-object width-full" src="{{ asset($article->thumbnail) }}" alt="">
                    @endif
                </a>
            </div>
            <div class="media-body col-md-12 col-sm-12 col-xs-6 col-xxs-12 padding-b-10">
                <h4 class="media-heading">
                    <a href="{{ route('patient.articles.show', $article->slug) }}">
                      {{ Str::limit($article->title , 27 , '...') }}
                    </a>
                </h4>
                {!! Str::limit(strip_tags($article->content), 154, '...') !!}

                @include('patient.pages.articles.tags')
            </div>
        </div>
    </div>
@endforeach