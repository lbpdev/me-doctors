<div class="col-md-12 pull-left articles">
    <div class="row">
        <h4 class="section-header margin-t-0">KNOW MORE</h4>
        @if (count($latestArticles)>0)
            <div class="col-md-8 no-padding">
                @include ('patient.pages.home.articles._latest-with-image', [
                    'articles' => array_slice($latestArticles->all(), 0, 2)
                ])
            </div>
            @include ('patient.pages.home.articles._latest-rss', [
               'articles' => array_slice($latestArticles->all(), 2)
            ])
        @else
            <p>No Articles to Show.</p>
        @endif
    </div>
</div>