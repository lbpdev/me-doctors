<div class="media-body col-md-12 col-sm-12 col-xs-12 padding-l-0 padding-r-0">
    <div class="col-md-2 no-padding">
       <h2 class="no-margin">{{ $index+1 }}</h2>
    </div>
    <div class="col-md-10 no-padding">
        <h3 class="line-height-14 font-12 no-margin">
           <a href="{{ route('patient.discussions.show', $discussion->slug) }}" class="t-black">
             {{ Str::limit($discussion->title ,56 , '...') }}
            </a>
        </h3>
    </div>
</div>
