 <div class="col-md-4 padding-l-0 padding-r-0 clearfix margin-t-10">
    <div class="col-md-12 padding-l-0 padding-r-0">
        <h4 class="section-header margin-t-0">YOUR VOICE</h4>
        <div class="home-discs">
        @if(count($discussions)>0)
            @foreach ($discussions as $index=>$discussion)
                <div class="padding-l-10 col-md-12 clearfix pull-left  padding-r-10 gray-bg">
                    @include('patient.pages.home.discussions.discussion')
                </div>
            @endforeach
        @else
            <p>No discussions available.</p>
        @endif
        </div>
    </div>
</div>
