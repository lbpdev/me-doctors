{{--<div class="doctor-item {{ $index < 3 ? 'gray-bg' : ''}} pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 margin-r-0 border-bottom border-light padding-r-0">--}}
<div class=" doctor-item pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 border-bottom-light gray-bg">
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">

            {{--@if($index < 3)--}}
                {{--<span class="featured-ribbon pink"></span>--}}
            {{--@endif--}}

            {{--<div class="{{ $index < 3 ? 'col-lg-2 col-md-2' : 'col-lg-1 col-md-1 '}} col-xs-3 ">--}}
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-xxs-12 ">
                <div class="row text-left avatar">
                    <a href="{{ route('patient.doctors.show', $doctor->username)}}">
                        {{--<img src="{{ url($doctor->avatar) }}" width="{{ $index < 3 ? '100%' : '100'}}" class="avatar">--}}
                        <img src="{{ url($doctor->avatar) }}" width="100%" class="avatar">
                    </a>
                </div>
            </div>

{{--            <div class="{{ $index < 3 ? 'col-lg-7 col-md-7 ' : 'col-lg-11 col-md-11 padding-l-55'}} col-xs-12 row-sm padding-r-0">--}}
            <div class="col-lg-10 col-md-10 col-sm-10 padding-l-10 col-xs-10  col-xxs-12 row-sm padding-r-0">
                <a class="doctor-username" href="{{ route('patient.doctors.show', $doctor->username)}}">
                    <h4 class="margin-t-0 padding-t-0 margin-b-0 black doctor-name robotoregular">{{ $doctor->name }}</h4>
                </a>
                <div class="detail-line">
                    <i class="fa fa-stethoscope"></i>
                    <span class="doctor-specialties">
                        @if(count($doctor->specialties)>0)
                            @foreach($doctor->specialties as $specialty)
                                {{ $specialty->name }}
                            @endforeach
                        @endif
                        &nbsp;
                    </span>
                    <span class="small-icon hospital"></span> <span class="doctor-hospital">{{ isset($doctor->primaryHospital) ? $doctor->primaryHospital : 'N/A' }}</span>
                </div>

                <div class="detail-line">
                    <span class="small-icon phone"></span> <span class="doctor-contact">{{ count($doctor->contacts)>0 ? $doctor->contacts[0]->number : 'N/A' }}</span> &nbsp;
                    <span class="small-icon location"></span>
                    <span class="doctor-work">
                        {{ $doctor->primaryWorkLocation ? $doctor->primaryWorkLocation : 'N/A' }}
                    </span>
                </div>

            </div>
            {{--@if($index < 3)--}}
                {{--<div class="col-lg-3 col-md-3 col-xs-12 row-sm padding-r-0">--}}
                    {{--<div class="map no-margin" style="height: 140px;" data-location="{{ isset($doctor->primaryHospital) ? $doctor->primaryHospital : 'N/A' }}"></div>--}}
                {{--</div>--}}
            {{--@endif--}}
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->