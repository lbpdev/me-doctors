 <div class="col-md-6 padding-l-0 padding-r-10 clearfix margin-t-10">
    <div class="col-md-12 padding-l-0  border-right-light padding-r-10">
        <h4 class="section-header margin-t-0">FIND A DOCTOR</h4>
        <ul class="directory-list padding-l-0 clearfix">
            @if (count($doctors)>0)
                @foreach ($doctors as $index=>$doctor)
                    @include('patient.pages.home.doctors._doctor')
                @endforeach
            @else
                <p class="margin-t-20">There are currently no doctors for this section.</p>
            @endif
        </ul>
    </div>
</div>
