<div class=" doctor-item pull-left item padding-tb-10 clearfix col-lg-12 col-md-12 border-bottom-light gray-bg">
    <div class="col-lg-12 col-md-12 no-padding">
        <div class="post-content left-pad-20">

            {{--@if($index < 3)--}}
                {{--<span class="featured-ribbon yellow"></span>--}}
            {{--@endif--}}

            {{--<div class="{{ $index < 3 ? 'col-lg-3 col-md-3 col-sm-12 col-xs-12' : 'col-lg-2 col-md-2 col-sm-12 col-xs-12'}} ">--}}
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="row text-left avatar">
                    <a class="hospital-slug" href="{{ route('patient.hospitals.show' , $hospital->slug) }}">
                        <img src="{{ url($hospital->thumbnail) }}" width="100%" class="avatar">
                    </a>
                </div>
            </div>

            {{--<div class="{{ $index < 3 ? 'col-lg-6 col-md-6 ' : 'col-lg-10 col-md-10'}} col-xs-12 row-sm padding-r-0">--}}
            <div class="col-lg-8 col-md-8 col-xs-12 row-sm padding-r-0">
                <a class="hospital-slug"  href="{{ route('patient.hospitals.show' , $hospital->slug) }}">
                    <h4 class="margin-t-0 padding-t-0 margin-b-0 black hospital-name robotoregular text-uppercase">{{ $hospital->name }}</h4>
                </a>

                <div class="detail-line margin-b-5">
                        <span><i class="fa fa-phone yellow"></i>
                        <span class=" hospital-contact"> {{ $hospital->phoneNumber }}</span> </span>
                        <br>
                        <i class="fa fa-location-arrow yellow"></i>
                        <span class=" hospital-location">{{ $hospital->website ? $hospital->website : 'N/A' }}</span>
                        <br>
                        <i class="fa fa-map-marker yellow"></i>
                        <span class="hospital-location">{{ $hospital->basicLocation }}</span>
                </div>
                <hr class="margin-tb-5">
                <div class="margin-tb-5 hospital-details">
                    {!! Str::limit(trim($hospital->description) == '' ? 'No description' : strip_tags($hospital->description),120) !!}
                </div>
            </div>
            {{--@if($index < 3)--}}
                {{--<div class="col-lg-3 col-md-3 col-xs-12 row-sm padding-r-0 col-sm-hide">--}}
                    {{--<div class="map no-margin" style="height: 140px;" data-location="{{ $hospital->location['name'] }}"></div>--}}
                {{--</div>--}}
            {{--@endif--}}
        </div><!-- End post Content -->
    </div><!-- End Col -->
</div><!-- End Row -->