 <div class="col-md-6 padding-l-0 padding-r-0 clearfix margin-t-10">
    <div class="col-md-12 padding-l-0 padding-r-0">
        <h4 class="section-header margin-t-0">FIND A HOSPITAL</h4>
        <ul class="directory-list padding-l-0 clearfix">
            @if (count($hospitals)>0)
                @foreach ($hospitals as $index=>$hospital)
                    @include('patient.pages.home.hospitals._hospital', ['hospital', $hospital])
                @endforeach
            @else
                <p class="margin-t-20">There are currently no hospitals for this section.</p>
            @endif
        </ul>
    </div>
</div>
