<div class="clearfix pull-left col-md-3 col-sm-4 sidebar margin-t-5 padding-r-0">
    @include('patient.partials.news-feeds')

        @if (count($poll)>0)
            <div class="sidebar-element clearfix">
                <h4 class="poll-head blue-bg t-white padding-tb-7">Today's Poll</h4>
                <div class="col-md-12 side-poll">
                    {!! Form::open(['route'=>'patient.poll_vote']) !!}
                        @include('patient.partials._poll')
                    {!! Form::close() !!}
                </div>
            </div>
        @endif

        <div class="sidebar-element clearfix">
            @if($authUser)
            @else
                <a href="{{ route('patient.login') }}">
                    <img src="{{ asset('public/images/ads/register-side.png') }}" class="margin-b-7" width="100%" alt="register med">
                </a>
            @endif
            
            @include('templates.box-event')
            @include('templates.sidebar-long')

        </div>
        </div><!-- COL-MD-12 -->
    </div>
</div>