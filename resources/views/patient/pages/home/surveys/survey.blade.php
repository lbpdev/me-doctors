<div class="media-body col-md-12 col-sm-12 col-xs-12 padding-tb-5">
    <h4 class="media-heading">
       <a href="{{ route('patient.surveys.show', $survey->slug) }}">
         {{ Str::limit($survey->title ,46 , '...') }}
        </a>
    </h4>
    <p>
        {{--{!! Str::limit(strip_tags($survey->description) , 20, '...') !!}--}}
    </p>
    @include('patient.pages.articles.tags', ['article' => $survey])
</div>
