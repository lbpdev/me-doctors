 <div class="col-md-8 padding-l-0 padding-r-10 clearfix margin-t-10">
    <div class="col-md-12 padding-l-0  border-right-light padding-r-10">
        <h4 class="section-header margin-t-0">YOUR OPINION</h4>
        <div class="home-surveys">
        @if(count($surveys)>0)
            @foreach ($surveys as $index=>$survey)
                <div class="padding-l-0 col-md-6 {{ $index%2==0 ? 'padding-r-10' : 'padding-r-0' }} padding-b-10 clearfix pull-left">
                    @include('patient.pages.home.surveys.survey')
                </div>
            @endforeach
        @else
            <p>No surveys available.</p>
        @endif
        </div>
    </div>
</div>
