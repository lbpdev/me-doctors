@extends('patient.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">
                @include ('patient.partials._news-ticker')
                <hr class="margin-b-3 margin-t-0">
                    <div class="discussions">
                        <h3 class="no-border yellow hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
                            <span class="page-icon hospitals"></span>
                            <span class="page-title">Hospital Directory</span>
                        </h3>
                        <hr class="margin-t-5 margin-b-7">
                        @include('patient.pages.hospitals._filter')
                        @include('patient.pages.hospitals._hospitals')

                        @if(count($hospitals)>0)
                            <button class="next button pull-right margin-t-10">
                                <span class="font-18 robotomedium yellow">{{ count($hospitals) == $total_hospitals ? 'No more results' : 'Load More'  }}</span>
                                <img src="{{ asset('public/images/icon/doc_loader_yellow.GIF') }}" width="20" class="loader temp-hide">
                            </button>
                        @endif

                        <div class="voffset2 clearfix f-left"></div>
                    </div>
                    {{--@include ('patient.partials.diagnosis.latest')--}}
                </div>

                @include('patient.pages.discussions.sidebar')
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script>

    var featuredLoaded = '<?php echo $featuredCount ?>';
    var totalFeatured = '<?php echo $totalFeatured ?>';
    var allFeaturedLoaded = false;

    function initMap() {
        $('.map').each(function(e , item){
            var map = new google.maps.Map(item, {
                zoom: 6,
                disableDefaultUI: true,
//                draggable: false,
//                zoomControl: false,
//                scrollwheel: false,
//                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map , item);
        })
      }

    function initMapSingle(mapItem) {
        var map = new google.maps.Map(mapItem, {
            zoom: 12,
            disableDefaultUI: true,
            draggable: false,
            zoomControl: false,
            scrollwheel: false,
            disableDoubleClickZoom: true
        });
        var geocoder = new google.maps.Geocoder();

        geocodeAddress(geocoder, map , mapItem);

        setTimeout(function(){
            google.maps.event.trigger(map, "resize");
        },300);
    }

      function geocodeAddress(geocoder, resultsMap ,item) {
        var address = $(item).attr('data-location');
        console.log(address)

        if(address=="")
            address = "United Arab Emirates";
        if(address=="Not Specified")
            address = "United Arab Emirates";


        geocoder.geocode({'address': address}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            address = "Dubai - United Arab Emirates";
            geocoder.geocode({'address': address}, function(results, status) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
            });
          }
        });
      }
      initMap();

      $('.next').on('click', function(e){
          var profile_url = "<?php echo route('patient.hospitals.index'); ?>";
          var base_url = "<?php echo url('/'); ?>";
          var specialties,bio,active_contact,avatar;

          var placeholder = "{{ asset('public/images/placeholders/hospital.png') }}";

        console.log(totalFeatured+'=='+featuredLoaded);
        if(totalFeatured==featuredLoaded)
            var allFeaturedLoaded = true;

          $(this).find('.loader').show();
          $.ajax({
            url: '<?php echo route('patient.hospitals.next') ?>',
            type: "POST",
            data: {
              'take'    : 10 ,
              'offset'  : $('.doctor-item.non-featured').length,
              'therapy' : '<?php echo isset($_GET['therapy']) ? $_GET['therapy'] : 'all'; ?>' ,
              'country' : '<?php echo isset($_GET['country']) ? $_GET['country'] : 'all'; ?>' ,
              'city'    : '<?php echo isset($_GET['city']) ? $_GET['city'] : 'all'; ?>',
              'sort'    : $('#sort').val(),
              'featuredCount': featuredLoaded,
              'allFeaturedLoaded': allFeaturedLoaded
            },
            success: function(data){
              data = JSON.parse(data);
              if(data.length > 0){
                  $(data).each(function(key, item){
                      active_contact = 'N/A';
                      avatar = placeholder;

                    if(item.services){
                        featuredLoaded++;
                        var template = $('#feat-template').clone().show();
                        $(template).find('.map').attr('rel','');
                    } else {
                        var template = $('#non-feat-template').clone().show();
                        $(template).addClass('non-featured');
                    }

                      $(template).find('.hospital-name').text(item.name);
                      $(template).find('.hospital-slug').attr('href', profile_url + '/' + item.slug);

                    if(item.avatar != null)
                        avatar = base_url+'/public/'+item.avatar.path+'/'+item.avatar.file_name;

                    $(template).find('.avatar').attr('src',avatar);

                      bio = JSON.stringify(item.description);
                      bio = bio.replace(/<(?:.|\n)*?>/gm, '');  
                      

                      bio = item.description;
                      bio = bio.replace(/<(?:.|\n)*?>/gm, '');  

                      if(bio.length != 92){
                            bio = bio.replace(/<(?:.|\n)*?>/gm, '');
                            bio = bio.substring(0,160)+'...';
                            $(template).find('.hospital-details').text(bio);
                      }
                      else
                        $(template).find('.hospital-details').text('No description');

                      if(item.contacts && (item.contacts).length > 0)
                        active_contact = item.contacts[0].number != '' ? item.contacts[0].number : 'N/A';

                      $(template).find('.hospital-contact').text(active_contact);

                      var work = 'Not Specified';
                      if(item.location)
                        work = item.location.country != '' ? item.location.city + ', ' + item.location.country : 'N/A';


                         if(item.services){
                            if(item.location){
                                mapItem = $(template).find('.map');
                                mapItem.attr('data-location',item.location.name);
                                console.log(mapItem[0]);
                                initMapSingle(mapItem[0]);
                            }
                        }

                      $(template).find('.hospital-location').text(work);
                      $('.directory-list').append(template);
                  });
              } else {
                  $('.next span').text('No more results');
              }
            },
            error: function(error){
              console.log(error.responseText);
            },
            complete: function(e){
              $('.loader').hide();
            }

          });
      });

    </script>
@stop
