<div class="fields padding-b-7 clearfix">
<h1 class="t-yellow margin-t-20 margin-b-4 clearfix pull-left width-full">About</h1>
    <div class="col-md-12 padding-l-0">
        <span>
            {!! trim($hospital->description) == '' ? 'No description' : $hospital->description !!}
        </span><br>
    </div>
</div>