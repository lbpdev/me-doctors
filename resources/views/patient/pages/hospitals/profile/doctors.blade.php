<div class="fields padding-b-7 clearfix">
<h1 class="t-yellow margin-t-20 margin-b-7 clearfix pull-left width-full">Doctors</h1>
    <div class="col-md-12 padding-l-0">
        @for($x=0;$x<8;$x++)
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-4 no-padding margin-b-10">
                        <img src="{{ asset('public/images/placeholders/user.png') }}" width="100%">
                    </div>
                    <div class="col-md-8 padding-r-0">
                        <h4 class="margin-t-4">Doctor</h4>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>