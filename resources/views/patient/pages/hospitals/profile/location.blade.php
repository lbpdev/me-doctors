<div class="fields padding-b-7 clearfix">
<h1 class="t-yellow margin-t-20 margin-b-4 clearfix pull-left width-full">Location</h1>
    <div class="col-md-4 padding-l-0 line-height-22">
        <span>{!! '<b>'. $hospital->name . '</b><br>'  !!}</span>
        <span><b>
            {{ $hospital->basicLocation }}
        </b><br></span>
        <span>
            {!! $hospital->location ? $hospital->location->name : ''  !!}
        <br></span>
    </div>

    <div class="col-md-8 no-padding">
        <div class="map" style="height: 300px">
        </div>
    </div>
</div>