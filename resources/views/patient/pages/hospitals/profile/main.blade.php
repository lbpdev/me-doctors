<div class="hs_single_profile_detail padding-b-20 clearfix">
    <div class="row">
        <div class="col-md-4 col-sm-4 padding-b-0">
            <img src="{{ asset($hospital->thumbnail) }}" alt="{{ $hospital->name }}" width="100%"/>
        </div>
        <div class="col-md-8 col-sm-8 ">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0margin-tb-0 padding-tb-0">
                <div class="row">
                    <h3 class="margin-t-7 margin-tb-0 text-uppercase">{{ $hospital->name }}</h3>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
                <div class="row">
                  <h4 class="margin-tb-10">
                  </h4>
                </div>
            </div>
            <div class="clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
                    <div class="row line-height-22">
                        <span class="small-icon yellow cursor"></span> {{ $hospital->website ? $hospital->website : 'No website' }} <br>
                        <span class="small-icon yellow phone"></span> {{ $hospital->phoneNumber }} <br>
                        <table>
                            <tr>
                                <td class="pull-left"><span class="small-icon yellow location"></span></td>
                                <td> {{ preg_replace( "/\r|\n/", "", $hospital->location ? $hospital->location->name : 'Not available' ) }}</td>
                            </tr>
                        </table>
                        @if(Session::has('message'))
                            <div class="alert alert-success margin-t-10 ">{!!  Session::get('message') !!}</div>
                        @elseif($hospital->ownedByCurrentUser)
                            <a href="{{ route('patient.hospitals.edit', $hospital->slug) }}">
                                <button class="button t-light-gray">[ <i class="fa fa-edit"></i> EDIT ]</button>
                            </a>
                        @elseif(!$hospital->ClaimedByCurrentUser)
                            <a class="margin-t-20 padding-l-5" href="#" data-toggle="modal" data-target="#claim">
                                <i class="fa fa-check-circle-o"></i> Claim this hospital
                            </a>
                        @else
                             @if(isset($claim))
                                <a href="#" class="margin-t-10 alert alert-success">Your claim request has been sent successfully. If approved, a final verification email will be sent to {{ $claim }}</a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
