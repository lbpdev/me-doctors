@extends('patient.master')

@section('content')
    <div class="container login-container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 margin-t-20 register">

                <h3 class="hs_heading no-margin-top"><i class="fa fa-user-plus"></i> Register</h3>

                {!! Form::open([
                        'url'         => 'register',
                        'data-toggle' => 'validator' ,
                        'role'        => 'form' ,
                        'class'       => 'clearfix'
                ]) !!}

                    @include('patient.templates.country_tmp')

                    @include('patient.templates.register_tmp')
                    {{--@include('patient.pages.auth.show')--}}
                {!! Form::close() !!}

            </div>

            <!--Login Start-->
            <div class="col-lg-4 col-md-4 col-sm-12 padding-l-20">
                @include('patient.templates.login_tmp')
                <hr>
                <a href="http://thepatient.me/" target="_blank"><img src="{{ asset('public/images/ads/the-patient.png') }}" width="100%"></a>
            </div>
        </div> <!-- END OF ROW -->
    </div> <!-- END OF CONTAINER -->
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('public/js/validator.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/hideShowPassword.min.js') }}"></script>
    <script src="{{ asset('public/js/custom/password-peeker.js') }}"></script>
    <script>
              function initMap(index) {
                var map = new google.maps.Map(document.getElementsByClassName('map')[index], {
                    zoom: 12,
                    disableDefaultUI: true,
                    draggable: false,
                    zoomControl: false,
                    scrollwheel: false,
                    disableDoubleClickZoom: true
                });
                var geocoder = new google.maps.Geocoder();

                geocodeAddress(geocoder, map , index);
              }

              function geocodeAddress(geocoder, resultsMap ,index) {
                var address = document.getElementsByClassName('location')[index].value;
                var map = $('.map')[index];
                if(address=="")
                    address = "Dubai - United Arab Emirates";

                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === google.maps.GeocoderStatus.OK) {
                    $(map).find('.gm-style').show();
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      position: results[0].geometry.location
                    });
                  } else {
                  $(map).find('.gm-style').hide();
                  }
                });
              }

        </script>

        <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
        <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
        <script type="text/javascript" src="{{ asset('public/js/custom/profileFields.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
        <script type="text/javascript">
            $('#register-therapies').select2({
                placeholder: "Select your specialties"
            });

<<<<<<< HEAD
=======
            var valid_countries = [
                'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' ,
                'AZ', 'GE', 'IR', 'IQ', 'IL', 'NT', 'SY', 'TR', 'TM', 'AM', 'YE'
            ];

>>>>>>> cbd12a04c1e418dfbaf7de23f24b7d7a22806bee
            $('#country').on('change',function(event,data){
                if(event.target.value!="OTHER"){
                    $('#register-form').animate({height: "380px"}, 500);
                    $('#country-alert').hide();
                } else {
                    $('#register-form').css('height','1px');
                    $('#country-alert').show(500);
                }
            });
        </script>


@stop