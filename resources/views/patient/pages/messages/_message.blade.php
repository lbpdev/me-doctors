@if(count($conversations)>0)
    @foreach($conversations as $data)
        <div class="col-md-12 discussion-item  no-padding margin-b-10 clearfix border-bottom row-sm">
          <div class="col-lg-12 col-md-12 no-padding">
            <div class="post-content left-pad-20 clearfix">
              <div class="col-lg-11 col-md-11 col-xs-11 no-padding margin-t-4">
                <div class="col-md-11">
                    <div class="row line-height-22">
                        @if(strpos(Request::path(), 'inbox'))
                            <a href="{{ route('patient.messages_single',$data['conversation']->id) }}">
                        @endif
                            <span class="t-gray margin-t-0 margin-b-0 robotomedium">
                            <strong>
                            @if($data['conversation']->author->id==Auth::id())
                                @foreach($data['conversation']->members as $member)
                                    @if($member->id != Auth::id())
                                        {{ $member->fname }} {{ $member->lname }}
                                    @endif
                                @endforeach
                            @else
                             {{ $data['conversation']->author->lname }} {{ $data['conversation']->author->fname }}
                            @endif
                            </strong>
                            </span>
                            </a><br>
                        @if(strpos(Request::path(), 'inbox'))
                            <a href="{{ route('patient.messages_single',$data['conversation']->id) }}">
                        @endif
                            <span class="t-gray  margin-b-0 margin-t-5 robotolight">
                            <strong>Subject :</strong>
                            {{ $data['conversation']->subject }}
                            @if(isset($data['unread_messages']))
                                <span class="pink">( {{ count($data['unread_messages']) }} unread )</span>
                            @endif
                            </span>
                        </a>
                    </div>
                </div>
                <div class="margin-t-5 col-md-12 padding-b-7">
                    <div class="row">
                    @if(count($data['conversation']->messages))
                        {{ strip_tags(Str::words($data['conversation']->messages[0]->message ,33, '...')) }}
                    @endif
                    </div>
                </div>
              </div>
              @if(!isset($noCheck))
                  <div class="col-lg-1 col-md-1 col-xs-1 no-padding text-center">
                    <span class="pull-right text-center bg-gray padding-t-5">
                        {!! Form::checkbox('checkToggle' , $data['conversation']->id , null ,  ['id'=> 'radio'.$data['conversation']->id ,'class' => 'square gray text-middle no-margin' , 'onchange' => "togglecheckboxes(false,'checkToggle')"]) !!}
                        <label for="radio{{ $data['conversation']->id }}"><span></span></label>
                    </span>
                  </div>
              @endif
            </div><!-- End post Content -->
          </div><!-- End Col -->
        </div><!-- End Row -->
    @endforeach
@else

    @if(!isset($noCheck))
        <p class="pull-left margin-t-10">Inbox is empty.</p>
    @else
        No results.
    @endif
@endif