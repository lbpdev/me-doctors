{!! Form::open(['route'=>'patient.messages_search','method'=>'GET']) !!}
    <div class="search-field margin-t-10">
        <div class="col-md-12"> {!! Form::input('text' , 'keyword' , null , ['class' => 'form-control white-bg']) !!} </div>
        <button type="submit" class="magnify button search-icon" style="top: 4px; right: 10px"></button>
    </div>
{!! Form::close() !!}