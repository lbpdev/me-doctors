@extends('patient.master')

@section('content')
<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 border-right col-xs-12 contents padding-l-0 padding-r-0 row-sm">

                @include('patient.templates.inbox-menu')
                <div class="col-md-9 clearfix row-sm">
                    <div class="filters margin-t-20 border-bottom clearfix col-md-12 padding-10 row-sm">
                        <div class="col-lg-4 col-md-4  col-sm-4  col-md-4 col-xs-12 padding-l-0">
                            <span class="pull-left"><h3 class="no-margin t-regular">Archive</h3></span>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-11 padding-l-0 actions">
                            <a class="pull-right margin-r-20" href="#"><i class="fa fa-trash"></i> Trash</a>
                            <a class="pull-right margin-r-20" href="#"><i class="fa fa-folder"></i> Archive</a>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                            <span class=" margin-r-10 pull-right">
                                {!! Form::checkbox('checkToggle' , '1' , null ,  ['class' => 'text-middle no-margin']) !!}
                             </span>
                        </div>
                    </div>
                    @for($x=0;$x<5;$x++)
                        <div class="col-md-12 discussion-item margin-b-10 clearfix padding-10 border-bottom row-sm">
                          <div class="col-lg-12 col-md-12 no-padding">
                            <div class="post-content left-pad-20 clearfix">
                              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
                                <div class="row">
                                  <a href="{{ URL::to('messages/1') }}"><img src="http://placehold.it/200x255" width="100%"></a>
                                </div>
                              </div>

                              <div class="col-lg-9 col-md-9 col-xs-9">
                                <div class="col-md-6">
                                    <div class="row">
                                        <a href="{{ URL::to('messages/1') }}"><h4 class="margin-t-0 margin-b-0">Sender</h4></a>
                                        <a href="{{ URL::to('messages/1') }}"><h5 class="margin-b-0 t-gray margin-t-5">Subject</h5></a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <span class="pull-right font-12">September 18 , 1991</span>
                                    </div>
                                </div>
                                <div class="content margin-t-5 col-md-12">
                                    <div class="row">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since...
                                    </div>
                                </div>
                              </div>
                              <div class="col-lg-1 col-md-1 col-xs-1 no-padding text-center">
                                <span class="pull-right text-center margin-r-10">
                                    {!! Form::checkbox('checkToggle' , '1' , null ,  ['class' => 'text-middle no-margin']) !!}
                                </span>
                              </div>
                            </div><!-- End post Content -->
                          </div><!-- End Col -->
                        </div><!-- End Row -->
                    @endfor
                </div>

                <div class="clearfix"></div>

                <?php /** echo $discussions->render(); **/ ?>
            </div>
            @include('patient.templates.sidebar-ads')

        </div>
    </div>
</div>

@stop