@extends('patient.master')

@section('content')
<div class="container page-container messages">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents border-right padding-l-0 padding-r-0 row-sm">

                @include ('patient.partials._news-ticker')
                <hr class="margin-t-0 margin-b-4">
                @include('patient.templates.inbox-menu')

                <div class="col-md-9 col-sm-12 col-xs-12 clearfix row-sm pull-left">

                    <div class="filters margin-t-0 border-bottom clearfix col-md-12 padding-b-10 padding-l-0 padding-r-0 row-sm">
                        <div class="col-lg-4 col-md-4  col-sm-4  col-md-4 col-xs-12 no-padding">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-11 no-padding actions">
                            {!! Form::open(['name'=>'trash_form', 'url'=>route('patient.messages_trash_multi')]) !!}
                                {!! Form::hidden('trash_ids' , null , ['id'=>'trash_ids']) !!}
                            {!! Form::close() !!}
                            <a class="pull-right" onclick="trashSubmit()" href="#"><span class="small-icon trash"></span>  Trash</a>
                            {{--<a class="pull-right margin-r-20" href="#"><i class="fa fa-folder"></i> Archive</a>--}}
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                            <span class="pull-right">
                                <input id="radioAll" class="square" type="checkbox" onchange="togglecheckboxes(this,'checkToggle')">
                                <label for="radioAll"><span></span></label>
                            </span>
                        </div>
                    </div>
                    <div class="scroll">
                        @if(isset($conversations))
                            @include('patient.pages.messages._message')
                        @else
                            <p class="pull-left margin-t-10">Inbox is empty.</p>
                        @endif
                    </div>

                <?php echo $members->render(); ?>
                </div>

                <div class="clearfix"></div>

            </div>

            @include('patient.templates.sidebar-ads')
        </div>
    </div>
</div>

@stop

@section('js')
    <script type="text/javascript">

        function togglecheckboxes(master,group){
            var cbarray = document.getElementsByName(group);
            var ids = [];

            for(var i = 0; i < cbarray.length; i++){
                var cb = cbarray[i];

                if(master)
                    cb.checked = master.checked;

                if(cb.checked==true)
                    ids.push(cb.value);
            }
            updateIds(ids);
        }

        function updateIds(ids){
            document.getElementById('trash_ids').value = JSON.stringify(ids);
        }

        function trashSubmit(){
            document.forms["trash_form"].submit();
        }
    </script>
@stop
