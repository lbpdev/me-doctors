@extends('patient.master')

@section('content')
    <div class="container articles-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 padding-l-0 row-sm border-right ">
                @include ('patient.partials._news-ticker')
                <hr class="no-margin">

                <h2 class="hs_heading pull-left width-full margin-b-4">News Feeds</h2>

                <div class="col-md-12 no-padding clearfix">
                    <ul class="media-list">
                        @include('patient.pages.news._event')
                    </ul>


                    <?php echo $news->render(); ?>
                </div>

                </div>

            @include('patient.templates.sidebar-ads')
            </div>
        </div>
    </div>
@stop


@section('js')
@stop