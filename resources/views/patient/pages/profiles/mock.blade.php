@extends('patient.master')

@section('content')

<div class="container profile">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="hs_single_profile">
                <div class="hs_single_profile_detail padding-b-20 gray-border-b clearfix">
                    <h3 class="t-black  padding-b-10 margin-b-20">
                    Your Profile
                    {{--@if(Auth::id()==$user->id)--}}
                        {{--<span class="h-link">[ <a href="{{ route('edit_personal_profile', $user->username) }}">Edit Personal Details</a> ]</span>--}}
                        {{--<span class="h-link">[ <a href="{{ route('edit_work_profile', $user->username) }}">Edit Work Details</a> ]</span>--}}
                    {{--@endif--}}
                    </h3>
                    <div class="row">
                        <div class="col-md-4 padding-b-20">
                            <img src="http://placehold.it/100x100" alt="" width="100%"/>
                        </div>
                        <div class="col-md-8">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3 class="margin-t-0"> Full Name</h3>
                            </div>
                            <div class="clearfix">
                                <div class="row col-md-6">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4><i class="fa fa-medkit"></i> Designation: </h4>
                                        Tester
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4><i class="fa fa-medkit"></i> Email: </h4>
                                        Sample@email.com
                                    </div>
                                </div>

                                <div class="row col-md-6">
                                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                                        <h4><i class="fa fa-phone"></i> Phone No: </h4>
                                            <span class="text-capitalize">Personal: </span>
                                            0564043349
                                            <br>
                                            <span class="text-capitalize">Office: </span>:
                                            05632315349
                                            <br>
                                    </div>
                                </div>
                            </div>
                            <div class="ool-md-12 padding-10">
                                <button class="add-toggle"><i class="fa fa-edit"></i> Edit Profile</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                <div class="col-md-12 row-sm"><h3 class="t-black"><i class="fa fa-check-square-o"></i> Qualifications</h3></div>
                    <div class="col-md-12 row-sm qualifications">

                        <h4><i class="fa fa-suitcase"></i> Education</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="educations">
                                    <span>Oxford University</span>
                                    <span>Harvard University</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Education</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Education</h5>
                                <form>
                                <div class="fields">
                                    <input type="text" value="Oxford Universit" name="education[0][name]" id="education" placeholder="Education" class="margin-b-5 form-control">
                                    <input type="text" value="Harvard University" name="education[1][name]" id="education" placeholder="Education" class="margin-b-5 form-control">
                                    <input type="text" name="education[3][name]" id="education" placeholder="Education" class="margin-b-5 form-control">
                                </div>
                                    <button class="pull-right add_field_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-hospital-o"></i> Professional Memberships</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="memberships">
                                    <span>Heart Hospital</span>
                                    <span>Mediclinic Welcare Hospital</span>
                                    <span>Saint Luke's Hospital</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Memberships</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Memberships</h5>
                                <form>
                                <div class="fields">
                                    <input type="text" value="Heart Hospital" name="membership[0][name]" id="membership" placeholder="Memberships" class="margin-b-5 form-control">
                                    <input type="text" value="Mediclinic Welcare Hospital" name="membership[1][name]" id="membership" placeholder="Memberships" class="margin-b-5 form-control">
                                    <input type="text" value="Saint Luke's Hospital" name="membership[2][name]" id="membership" placeholder="Memberships" class="margin-b-5 form-control">
                                    <input type="text" value="" name="membership[3][name]" id="membership" placeholder="Memberships" class="margin-b-5 form-control">
                                </div>
                                    <button class="pull-right add_field_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-trophy"></i> Awards</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="awards">
                                    <span>BAFTA Cymru Awards</span>
                                    <span>BAFTA Scotland Awards</span>
                                    <span>BAFTA TV Awards</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Awards</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Awards</h5>
                                <form>
                                <div class="fields">
                                    <input type="text" value="BAFTA Cymru Awards" name="award[0][name]" id="award" placeholder="Award" class="margin-b-5 form-control">
                                    <input type="text" value="BAFTA Scotland Awards" name="award[1][name]" id="award" placeholder="Award" class="margin-b-5 form-control">
                                    <input type="text" value="BAFTA TV Awards" name="award[2][name]" id="award" placeholder="Award" class="margin-b-5 form-control">
                                    <input type="text" name="award[3][name]" id="award" placeholder="Award" class="margin-b-5 form-control">
                                </div>
                                    <button class="pull-right add_field_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-trophy"></i> Publications</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="publications">
                                    <span>EHR Incentive Program: A Progress Report</span>
                                    <span>Keeping Medical Practice Staff on Task</span>
                                    <span>Healthcare Reform Influencing Physicians' Career Choices</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Publications</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Publications</h5>
                                <form>
                                <div class="fields">
                                    <input type="text" value="EHR Incentive Program: A Progress Report" name="publication[0][name]" id="publication" placeholder="Publication" class="margin-b-5 form-control">
                                    <input type="text" value="Keeping Medical Practice Staff on Task" name="publication[1][name]" id="publication" placeholder="Publication" class="margin-b-5 form-control">
                                    <input type="text" value="Healthcare Reform Influencing Physicians' Career Choices" name="publication[2][name]" id="publication" placeholder="Publication" class="margin-b-5 form-control">
                                    <input type="text" name="publication[3][name]" id="publication" placeholder="Publication" class="margin-b-5 form-control">
                                </div>
                                    <button class="pull-right add_field_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-comment-o"></i> Languages Spoken</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="languages">
                                    <span>English</span>
                                    <span>French</span>
                                    <span>Arabic</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Languages</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Languages</h5>
                                <form>
                                <div class="fields">
                                    <input type="text" value="English" name="language[0][name]" id="language" placeholder="Language" class="margin-b-5 form-control">
                                    <input type="text" value="French" name="language[1][name]" id="language" placeholder="Language" class="margin-b-5 form-control">
                                    <input type="text" value="Arabic" name="language[2][name]" id="language" placeholder="Language" class="margin-b-5 form-control">
                                    <input type="text" name="language[3][name]" id="language" placeholder="Language" class="margin-b-5 form-control">
                                </div>
                                    <button class="pull-right add_field_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-heart"></i> Board Certifications</h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="certifications">
                                    <span>American Board of Dermatology</span>
                                    <span>American Board of Emergency Medicine</span>
                                    <span>American Board of Colon and Rectal Surgery</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Update Certifications</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Update Certifications</h5>
                                <form>
                                    <div class="fields">
                                        <input type="text" name="certification[0][name]" value="American Board of Emergency Medicine" id="certification" placeholder="Certification" class="margin-b-5 form-control">
                                        <input type="text" name="certification[1][name]" value="American Board of Dermatology" id="certification" placeholder="Certification" class="margin-b-5 form-control">
                                        <input type="text" name="certification[2][name]" value="American Board of Colon and Rectal Surgery" id="certification" placeholder="Certification" class="margin-b-5 form-control">
                                        <input type="text" name="certification[3][name]" id="certification" placeholder="Certification" class="margin-b-5 form-control">
                                    </div>

                                    <button class="pull-right add_field_bt margin-l-10  margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5 margin-r-10"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                        <h4><i class="fa fa-stethoscope"></i> Where I work </h4>
                        <div class="section col-md-12 margin-b-20">
                            <div class="col-md-12">
                                <div id="works">
                                    <span>Mediclinic Welfare Hospital, GGICO , Dubai</span>
                                    <span>Mediclinic Burj Khalifa, Downtown , Dubai</span>
                                </div>
                                <button class="add-toggle"><i class=" margin-t-5 fa fa-edit"></i> Add Work</button>
                            </div>
                            <div class="add-form clearfix  well-sm col-md-8">
                                <h5><i class=" margin-t-5 fa fa-edit"></i> Add Work</h5>
                                <form>
                                    <div class="fields-group">
                                        <div class="fields padding-b-5 border-bottom">
                                            <input type="text" name="work[0][name]" placeholder="Company Name" id="name" class="margin-b-5 margin-t-5 form-control">
                                            <table id="address" class="hidden">
                                                  <tr>
                                                    <td class="label">Street address</td>
                                                    <td class="slimField"><input name="work[0][street_number]" class="field street_number" id="street_number"
                                                          disabled="true"></td>
                                                    <td class="wideField" colspan="2"><input name="work[0][route]" class="field route" id="route"
                                                          disabled="true"></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="label">City</td>
                                                    <td class="wideField" colspan="3"><input name="work[0][locality]" class="field locality" id="locality"
                                                          disabled="true"></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="label">State</td>
                                                    <td class="slimField"><input name="work[0][state]" class="field administrative_area_level_1"
                                                          id="administrative_area_level_1" disabled="true"></td>
                                                    <td class="label">Zip code</td>
                                                    <td class="wideField"><input name="work[0][postal_code]" class="field postal_code" id="postal_code"
                                                          disabled="true"></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="label">Country</td>
                                                    <td class="wideField" colspan="3"><input name="work[0][country]" class="field country"
                                                          id="country" disabled="true"></td>
                                                  </tr>
                                            </table>
                                            {!! Form::input('text','work[0][full]' , null, [ 'class' => 'form-control location' , 'required' , 'id' => 'location' , 'placeholder' => 'Address']) !!}
                                        </div>
                                    </div>
                                    <button class="pull-right add_field_group_bt margin-l-10   margin-t-5 fa fa-plus"></button>
                                    <button rel="1" class="pull-right remove_field_group_bt  margin-t-5 fa fa-minus"></button>

                                    <button class="add-cancel pull-left margin-t-5"><i class="fa fa-close"></i> Cancel</button>
                                    <button class="add-save pull-left margin-t-5 margin-l-10"><i class="fa fa-save"></i> Save</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="user-bio col-md-12 row-sm padding-t-20">
                <h3 class="t-black"><i class="fa fa-user"></i> Professional Statement</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
                <p>
                   It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                </p>
                </div>
            </div>
        </div>

        @include('.........templates.sidebar-ads')
    </div>

<div class="hs_margin_40"></div>
</div>
@stop

@section('js')
    <script src="{{ asset('public/js/custom/profileFields.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

@stop
