  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($events as $event)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <h4 class="clearfix no-margin">
                                    <a href="{{ route('show_event', $event->slug) }}" class="pull-left">
                                         {{ $event->title }}
                                     </a>
                                 </h4>
                                  <span class="update"><b>{{ $event->event_date->diffForHumans() }}</b> at <b>{{ $event->location }}</b></span>
                                 <p>{!! Str::words(strip_tags($event->content) , 14, '...') !!}</p>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
