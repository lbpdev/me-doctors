  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($hospitals as $hospital)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <div class="col-md-2">
                                    <div class="row">
                                     <img src="{{ asset($hospital->thumbnail) }}" width="100%">
                                    </div>
                                 </div>
                                 <div class="col-md-10 padding-r-10">
                                     <h4 class="pull-left width-full clearfix no-margin text-uppercase">
                                         <a href="{{ route('patient.hospitals.show', $hospital->slug) }}" class="pull-left">
                                            {{ $hospital->name }}
                                         </a>
                                     </h4>
                                     <br>
                                     <p class="margin-t-10 pull-left"><b>Location: </b>{{ $hospital->basicLocation }}</p>
                                 </div>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
