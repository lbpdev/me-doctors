  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($surveys as $survey)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <h4 class="clearfix no-margin">
                                    <a href="{{ route('patient.surveys.show', $survey->slug) }}" class="pull-left">
                                         {{ $survey->title }}
                                     </a>
                                  <span class="update pull-right"><b>{{ $survey->created_at->diffForHumans() }}</b></span>
                                 </h4>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
