  <div class="col-md-12 no-padding clearfix">
     <div class="col-md-12">
         <div class="row">
             <ul class="search-list">
                 @foreach ($users as $user)

                     <li class="discussion clearfix height-auto">
                          <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                             <div class="row">
                                 <h4 class="clearfix no-margin">
                                    <a href="{{ route('patient.doctors.show', $user->username) }}" class="pull-left">
                                    <img src="{{ asset($user->avatar) }}" width="50">
                                         {{ $user->fname }} {{ $user->lname }}
                                     </a>
                                 </h4>
                             </div>
                          </div>
                      </li>

                 @endforeach
             </ul>
         </div>
     </div>
 </div>

 <div class="clearfix"></div>
