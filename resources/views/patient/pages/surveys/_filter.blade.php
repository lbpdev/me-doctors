<div class="col-md-12 clearfix no-padding">
    {!! Form::open(['route'=>'patient.surveys.filter' , 'method' => 'GET']) !!}
    <div class="col-md-12 col-sm-12 no-padding clearfix gray">
        <div class="col-md-10 padding-l-0">
            <div class="custom-select left">
                {!! Form::select('therapy' , $therapies , isset($_GET['therapy']) ? $_GET['therapy'] : null , [ 'id' => 'therapy' , 'class' => 'form-control pull-left' ]) !!}
            </div>
        </div>
        <div class="col-md-2 padding-r-0 padding-0-sm">
            <div class="custom-select left">
                {!! Form::select('sort' , $sort_types , isset($_GET['sort']) ? $_GET['sort'] : null , [ 'id' => 'sort' , 'class' => 'form-control pull-left' ]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-2 col-sm-2 padding-l-10 padding-r-0 row-xs temp-hide">
        <button type="submit" class="form-control">Filter</button>
    </div>
    {!! Form::close() !!}
</div>