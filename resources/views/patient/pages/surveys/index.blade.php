@extends('patient.master')

@section('content')
    <div class="container page-container surveys-page">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">

                @include ('patient.partials._news-ticker')
                    <div class="col-md-12 padding-b-0 padding-t-0 clearfix margin-b-5 margin-t-0">
                        <div class="row">
                            <hr class="margin-b-5 margin-t-0">
                            <h3 class="no-border light-blue hs_heading margin-b-0 padding-b-0 margin-t-0">
                                <span class="page-icon surveys"></span>
                                <span class="page-title light-blue">Surveys</span>
                            </h3>
                        </div>
                    </div>
                <hr class="no-margin">
                    @include ('patient.pages.surveys._filter')
                <hr class="margin-t-0 margin-b-0">

                    <div class="col-md-12 padding-tb-10">
                        <div class="row">
                            <ul class="box-list surveys">
                                @if ($surveys->count())
                                    @foreach ($surveys as $index=>$survey)
                                        <li class="col-md-6 col-sm-6 col-xs-12 no-padding">
                                            <div class="item gray-bg {{ $index%2==0 ? 'margin-r-10' : '' }} margin-r-0-xs margin-b-7 padding-7">
                                              <a href="{{ route('patient.surveys.show', $survey->slug) }}" class="width-full pull-left">
                                                <h4 class="media-heading clearfix">
                                                   {{ strlen(strip_tags($survey->description)) > 0 ? Str::limit($survey->title, 45) : $survey->title  }}
                                                </h4>
                                             </a>
                                             <p class="pull-left">{{ Str::limit(strip_tags($survey->description), 120) }}</p>
                                             @if(count($survey->therapies))
                                                  <div class="col-md-12 gray-border-t margin-t-7 tags">
                                                      <div class="row">
                                                      {{ $survey->therapies[0]->name }}
                                                      @if(count($survey->therapies)>1)
                                                      ...
                                                        <span class="therapies temp-hide">
                                                            <ul>
                                                            @foreach($survey->therapies as $index=>$therapy)
                                                                <li>{{ $therapy->name }}</li>
                                                            @endforeach
                                                            </ul>
                                                        </span>
                                                        <button class="button view-all font-12" type="button" data-toggle="modal" data-target="#myModal">( View all )</button>
                                                      @endif
                                                      </div>
                                                  </div>

                                              @endif
                                             </div>
                                        </li>
                                    @endforeach
                                @else
                                    Sorry there are currently no surveys for this section.
                                @endif
                            </ul>
                        </div>

                    <?php echo $surveys->render(); ?>
                </div>
            </div>

                @include('patient.templates.sidebar-ads')
            </div>
        </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog" style="width: 310px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tagged Therapies</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop

@section('js')
    <script>
        $('.view-all').on('click',function(e){
            var therapies = $(this).parent().find('.therapies').clone();
            console.log(therapies);
            $('.modal-body').empty().append(therapies.show());
        });
        $('#therapy').on('change',function(e){
            $(this).closest('form').trigger('submit');
        });
        $('#sort').on('change',function(e){
            $(this).closest('form').trigger('submit');
        });
    </script>
@stop