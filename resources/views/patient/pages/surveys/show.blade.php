@extends('patient.master')

@section('content')
    <div class="container page-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 border-right">

                @include ('patient.partials._news-ticker')
                <div class="col-md-12 col-sm-12 padding-b-0 padding-t-0 clearfix margin-b-7 margin-t-0">
                    <div class="row">
                        <hr class="margin-b-5 margin-t-0">
                        <h3 class="no-border light-blue hs_heading margin-b-0 padding-b-0 margin-t-0">
                            <span class="page-title light-blue">{{ count($selected->therapies) ? $selected->therapies[0]->name : '' }} Surveys</span>
                        </h3>
                        <a class="pull-right light-blue font-15 action-title" href="{{ route('patient.surveys.index') }}">Back</a>
                    </div>
                </div>
                <hr class="margin-b-7 margin-t-4">

                    <div class="col-md-3 no-padding clearfix">
                        <div class="col-md-12 gray-bg padding-tb-10">
                            <div class="row">
                                <ul class="surveys-list">
                                    @if ($surveys->count())
                                        @foreach ($surveys as $survey)
                                             <li class="discussion clearfix height-auto">
                                                <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                                                     <h4 class="{{ $survey->id == $selected->id ? 'active' : '' }} media-heading clearfix">
                                                         @if($survey->id != $selected->id)
                                                             <a href="{{ route('patient.surveys.show', $survey->slug) }}" class="pull-left">
                                                               {{ $survey->title }}
                                                             </a>
                                                         @else
                                                            {{ $survey->title }}
                                                        @endif
                                                     </h4>
                                                </div>
                                             </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                    @include ('patient.pages.surveys.questions', ['survey' => $selected])

                    <div class="clearfix"></div>
                </div>

                @include('patient.templates.sidebar-ads')
            </div>
        </div>
    </div>
@stop