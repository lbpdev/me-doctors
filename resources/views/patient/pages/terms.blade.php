@extends('patient.master')

@section('content')
<div class="container page-container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 contents padding-l-0 row-sm border-right">
                @include ('patient.partials._news-ticker')
                <hr class="margin-t-0 margin-b-10">

                @include('patient.templates.about-menu')
                <div class="col-md-9 clearfix row-sm padding-r-0 page about">

                      <h3 class="no-border yellow hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix doctor-blue">Terms of Use</h3>

                          <div class="col-md-12 padding-l-0">
                              <h5>1.	Acceptance of terms</h5>
                               <p>The services that Middle East Doctor (“MED”) provides to User is subject to the following
                              Terms of Use ("Terms"). MED reserves the right to update the Terms at any time without notice
                              to User. The most current version of the Terms can be reviewed by clicking on the "Terms of Use"
                              hypertext link located at the bottom of our site.</p>
                              <ol type="A">
                                  <li>
                                      This Agreement, which incorporates by reference other provisions applicable to use of MED,
                                      including, but not limited to, supplemental terms and conditions set forth hereof
                                      ("Supplemental Terms") governing the use of certain specific material contained in MED,
                                      sets forth the terms and conditions that apply to use of MED by User. By using MED
                                      (other than to read this Agreement for the first time), User agrees to comply with
                                      all of the terms and conditions hereof. The right to use MED is personal to User
                                      and is not transferable to any other person or entity. User is responsible for all
                                      use of User's Account (under any screen name or password) and for ensuring that all
                                      use of User's Account complies fully with the provisions of this Agreement.
                                      User shall be responsible for protecting the confidentiality of User's password(s), if any.
                                  </li>
                                  <li>
                                      MED shall have the right at any time to change or discontinue any aspect or feature of MED,
                                       including, but not limited to, content, hours of availability, and equipment needed for access
                                       or use.
                                  </li>
                              </ol>
                              
          
                              <h5>2.	Changed Terms</h5>
                               <p>MED shall have the right at any time to change or modify the terms and conditions applicable to
                              User's use of MED, or any part thereof, or to impose new conditions, including, but not limited to,
                              adding fees and charges for use. Such changes, modifications, additions or deletions shall be effective
                              immediately upon notice thereof, which may be given by means including, but not limited to, posting
                              on MED, or by electronic or conventional mail, or by any other means by which User obtains notice
                              thereof. Any use of MED by User after such notice shall be deemed to constitute acceptance by User
                              of such changes, modifications or additions.</p>
                              
          
                              <h5>3.	Description of services</h5>
                              <p>Through its web property, MED provides User with access to a variety of resources, including,
                              but not limited to, communication and discussion areas, survey tools, doctor directories and
                              educational information (collectively "Services"). The Services, including any updates, enhancements,
                               new features, and/or the addition of any new Web properties, are subject to the Terms.</p>
                              
          
                              <h5>4.	Equipment </h5>
                              <p>User shall be responsible for obtaining and maintaining all mobile phone, computer hardware,
                              software and other equipment needed for access to and use of MED and all charges related thereto.</p>
                              
          
          
                              <h5>5.	User Conduct </h5>
                              <ol type="A">
                                  <li>
                                      User shall use MED for lawful and professional purposes only. User shall not post or transmit
                                      through MED any material which violates or infringes in any way upon the rights of others, which
                                      is unlawful, threatening, abusive, defamatory, invasive of privacy or publicity rights, vulgar,
                                      obscene, profane or otherwise objectionable, which encourages conduct that would constitute a
                                      criminal offence, give rise to civil liability or otherwise violate any law, or which, without
                                      MED's express prior approval, contains advertising or any solicitation with respect to products
                                      or services. Any conduct by a User that in MED's discretion restricts or inhibits any other
                                      User from using or enjoying MED will not be permitted. User shall not use MED to advertise or
                                      perform any commercial solicitation, including, but not limited to, the solicitation of
                                      users to become subscribers of other online information services competitive with MED .
                                  </li>
                                  <li>
                                      MED contains copyrighted material, trademarks and other proprietary information, including, but
                                      not limited to, text, software, photos, video, graphics, illustrations and artwork, and the
                                      entire contents of MED are copyrighted as a collective work under international copyright laws.
                                      MED owns a copyright in the selection, coordination, arrangement and enhancement of such content,
                                      as well as in the content original to it. User may not modify, publish, transmit, participate in
                                      the transfer or sale, create derivative works, or in any way exploit, any of the content, in whole
                                      or in part. User may download copyrighted material for User's personal use only. Except as otherwise
                                      expressly permitted under copyright law, no copying, redistribution, retransmission, publication or
                                      commercial exploitation of downloaded material will be permitted without the express permission of
                                      MED. In the event of any permitted copying, redistribution or publication of copyrighted material,
                                      no changes in or deletion of author attribution, trademark legend or copyright notice shall be made.
                                      User acknowledges that it does not acquire any ownership rights by downloading copyrighted material.
                                  </li>
                                  <li>
                                      User shall not upload, post or otherwise make available on MED any material protected by copyright,
                                      trademark or other proprietary right without the express permission of the owner of the copyright,
                                      trademark or other proprietary right and the burden of determining that any material is not protected
                                      by copyright rests with User. User shall be solely liable for any damage resulting from any infringement
                                      of copyrights, proprietary rights, or any other harm resulting from such a submission. By submitting
                                      material to any public or private area of MED, User automatically grants, or warrants that the owner
                                      of such material has expressly granted MED the royalty-free, perpetual, irrevocable, non-exclusive
                                      right and license to use, reproduce, modify, adapt, publish, translate and distribute such material
                                      (in whole or in part) worldwide and/or to incorporate it in other works in any form, media or technology
                                      now known or hereafter developed for the full term of any copyright that may exist in such material.
                                      User also permits any other User to access, view, store or reproduce the material for that User's
                                      personal use. User hereby grants MED the right to edit, copy, publish and distribute any material
                                      made available on MED by User.
                                  </li>
                                  <li>
                                      The foregoing provisions of Section 5 are for the benefit of MED, its subsidiaries, affiliates and
                                      its third party content providers and licensors and each shall have the right to assert and enforce
                                      such provisions directly or on its own behalf.
                                  </li>
                              </ol>
                              
          
                              <h5>6.	Use of services </h5>
                              <p>The Services may contain messaging, email communication, discussions, surveys and doctor
                              directories, designed to enable User to communicate with others (each a "Communication Service"
                              and collectively "Communication Services"). User agrees to use the Communication Services only
                              to post, send and receive messages and material that are proper and, when applicable, related
                              to the particular Communication Service. By way of example, and not as a limitation,
                              User agrees that when using the Communication Services, User will not:</p>
                              
          
                              <ul>
                              <li>Use the Communication Services in connection with surveys not authorised or posted on MED,
                              contests, pyramid schemes, chain letters, junk email, spamming or any duplicative or unsolicited
                              messages (commercial or otherwise).</li>
          
                              <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights
                              (such as rights of privacy and publicity) of others.</li>
          
                              <li>Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, obscene,
                              indecent or unlawful topic, name, material or information.</li>
          
                              <li>Upload, or otherwise make available, files that contain images, photographs, software or other
                              material protected by intellectual property laws, including, by way of example, and not as limitation,
                              copyright or trademark laws (or by rights of privacy or publicity) unless User own or control the
                              rights thereto or have received all necessary consent to do the same.</li>
          
                              <li>Use any material or information, including images or photographs, which are made available through
                              the Services in any manner that infringes any copyright, trademark, patent, trade secret, or other
                              proprietary right of any party.</li>
          
                              <li>Upload files that contain viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files,
                              or any other similar software or programs that may damage the operation of another's computer or
                              property of another.</li>
          
                              <li>Advertise or offer to sell or buy any goods or services for any business purpose, unless such
                              Communication Services specifically allows such messages and has been authorised by MED.</li>
          
                              <li>Download any file posted by another user of a Communication Service that User know, or reasonably
                              should know, cannot be legally reproduced, displayed, performed, and/or distributed in such manner.</li>
          
                              <li>Falsify or delete any copyright management information, such as author attributions, legal or
                              other proper notices or proprietary designations or labels of the origin or source of software
                              or other material contained in a file that is uploaded.</li>
          
                              <li>Restrict or inhibit any other user from using and enjoying the Communication Services.</li>
          
                              <li>Violate any code of conduct or other guidelines which may be applicable for any particular
                              Communication Service.</li>
          
                              <li>Harvest or otherwise collect information about others, including email addresses.</li>
          
                              <li>Violate any applicable laws or regulations.</li>
          
                              <li>Create a false identity for the purpose of misleading others.</li>
          
                              <li>Use, download or otherwise copy, or provide (whether or not for a fee) to a person or entity
                              any directory of users of the Services or other user or usage information or any portion thereof.</li>
          
                              </ul>
          
                              
                              <p>MED has no obligation to monitor the Communication Services. However, MED reserves the
                              right to review materials posted to the Communication Services and to remove any materials in
                              its sole discretion. MED reserves the right to terminate User’s access to any or all of the
                              Communication Services at any time, without notice, for any reason whatsoever. MED reserves
                              the right at all times to disclose any information as it deems necessary to satisfy any applicable
                              law, regulation, legal process or governmental request, or to edit, refuse to post or to remove
                              any information or materials, in whole or in part, in MED's sole discretion.</p>
          
                              
                              <p>Materials uploaded to the Communication Services may be subject to posted limitations on usage,
                              reproduction and/or dissemination; User is responsible for adhering to such limitations if User
                              downloads the materials.</p>
          
                              
                              <p>Always use caution when giving out any personally identifiable information in any Communication
                              Services. MED does not control or endorse the content, messages or information found in any
                              Communication Services and, therefore, MED specifically disclaims any liability with regard to
                              the Communication Services and any actions resulting from User’s participation in any Communication
                              Services. Managers and hosts are not authorised MED spokespersons, and their views do not necessarily
                              reflect those of MED.</p>
                              
          
                              <h5>7.	Member account, password and security</h5>
          
                              <p>If any of the Services requires User to open an account, User must complete the registration
                              process by providing MED with current, complete and accurate information as prompted by the
                              applicable registration form. User also will choose a password and a username. User is entirely
                              responsible for maintaining the confidentiality of User’s password and account. Furthermore,
                              User is entirely responsible for any and all activities that occur under User’s account. User
                              agrees to notify MED immediately of any unauthorised use of User’s account or any other breach
                              of security. MED will not be liable for any loss that User may incur as a result of someone
                              else using User’s password or account, either with or without User’s knowledge. However, User
                              could be held liable for losses incurred by MED or another party due to someone else using
                              User’s account or password. User may not use anyone else's account at any time, without the
                              permission of the account holder.</p>
                              
          
                              <h5>8.	Notice specific to software available on this web site</h5>
          
                              <p>Any software that is made available to download from the Services ("Software") is the
                              copyrighted work of MED and/or its suppliers, and includes apps that are available through
                              Apple or Google app stores. Use of the Software is governed by the terms of the end user
                              license agreement, if any, which accompanies or is included with the Software ("License Agreement").
                              An end user will be unable to install any Software that is accompanied by or includes a
                              License Agreement, unless he or she first agrees to the License Agreement terms.</p>
          
                              <p>The Software is made available for download solely for use by end users according to the License
                              Agreement. Any reproduction or redistribution of the Software not in accordance with the License
                              Agreement is expressly prohibited by law, and may result in severe civil and criminal penalties.
                              Violators will be prosecuted to the maximum extent possible.</p>
                              
          
                              <p>Without limiting the foregoing, copying or reproduction of the software to any other server or
                              location for further reproduction or redistribution is expressly prohibited, unless such reproduction
                              or redistribution is expressly permitted by the license agreement accompanying such software. The
                              software is warranted, if at all, only according to the terms of the license agreement. Except as
                              warranted in the license agreement, MED hereby disclaims all warranties and conditions with regard
                              to the software, including all warranties and conditions of merchantability, whether express,
                              implied or statutory, fitness for a particular purpose, title and non-infringement.</p>
                              
          
                              <p>For your convenience, MED may make available as part of the services or in its software products,
                              tools and utilities for use and/or download. MED does not make any assurances with regard to the
                              accuracy of the results or output that derives from such use of any such tools and utilities.
                              Please respect the intellectual property rights of others when using the tools and utilities made
                              available on the services.</p>
          
          
                              <h5>9.	Notice specific to documents available on this web site</h5>
          
                              <p>Permission to use Documents (such as white papers, press releases, data sheets, clinical trials,
                              research papers and FAQs) from the Services is granted, provided that (1) the below copyright notice
                              appears in all copies and that both the copyright notice and this permission notice appear, (2)
                              use of such Documents from the Services is for informational and non-commercial or personal use
                              only and will not be copied or posted on any network computer or broadcast in any media, and (3)
                              no modifications of any Documents are made. Accredited educational institutions, such as universities,
                              private/public colleges, and state community colleges, may download and reproduce the Documents
                              for distribution in the classroom. Distribution outside the classroom requires express written
                              permission. Use for any other purpose is expressly prohibited by law, and may result in severe
                              civil and criminal penalties. Violators will be prosecuted to the maximum extent possible.</p>
                              
          
                              <p>MED and/or its respective suppliers make no representations about the suitability of the information
                              contained in the documents and related graphics published as part of the services for any purpose.
                              All such documents and related graphics are provided "as is" without warranty of any kind. MED and/or
                              its respective suppliers hereby disclaim all warranties and conditions with regard to this information,
                              including all warranties and conditions of merchantability, whether express, implied or statutory,
                              fitness for a particular purpose, title and non-infringement. In no event shall MED and/or its
                              respective suppliers be liable for any special, indirect or consequential damages or any damages
                              whatsoever resulting from loss of use, data or profits, whether in an action of contract,
                              negligence or other tortious action, arising out of or in connection with the use or performance
                              of information available from the services.</p>
                              
          
                              <p>The documents and related graphics published on the services could include technical inaccuracies
                              or typographical errors. Changes are periodically added to the information herein. MED and/or its
                              respective suppliers may make improvements and/or changes in the product(s) and/or the program(s)
                              described herein at any time.</p>
          
                              <h5>10.	Notices regarding software, documents and services available on this site</h5>
                              <p>In no event shall MED and/or its respective suppliers be liable for any special, indirect
                              or consequential damages or any damages whatsoever resulting from loss of use, data or profits,
                              whether in an action of contract, negligence or other tortious action, arising out of or in
                              connection with the use or performance of software, documents, provision of or failure to provide
                              services, or information available from the services.</p>
          
          
                              <h5>11.	Materials provided to MED or posted at any of its web sites</h5>
                               
                              <p>MED does not claim ownership of the materials User provide to MED (including feedback and suggestions)
                               or post, upload, input or submit to any Services or its associated services for review by the
                               general public, or by the members of any public or private community,
                               (each a "Submission" and collectively "Submissions"). However, by posting, uploading, inputting,
                               providing or submitting ("Posting") User’s Submission User is granting MED, its affiliated
                               companies and/or clients, and necessary sublicensees permission to use User’s Submission in
                               connection with the operation of their Internet businesses (including, without limitation,
                               all MED Services), including, without limitation, the license rights to: copy, distribute,
                               transmit, publicly display, publicly perform, reproduce, edit, translate and reformat User’s
                               Submission for commercial use; to publish User’s name in connection with User’s Submission;
                               and the right to sublicense such rights to any supplier of the Services.</p>
                              
                              
                              <p>No compensation will be paid with respect to the use of User’s Submission, as provided herein.
                              MED is under no obligation to post or use any Submission User may provide and MED may remove any
                              Submission at any time in its sole discretion. By Posting a Submission, User warrants and represents
                              to own or otherwise control all of the rights to User’s Submission as described in these Terms
                              including, without limitation, all the rights necessary for User to provide, post, upload, input
                              or submit the Submissions.</p>
                              
          
                              <p>In addition to the warranty and representation set forth above, by Posting a Submission that
                              contain images, photographs, pictures or that are otherwise graphical in whole or in part
                              ("Images"), User warrant and represent that (a) User is the copyright owner of such Images,
                              or that the copyright owner of such Images has granted User permission to use such Images or
                              any content and/or images contained in such Images consistent with the manner and purpose of
                              User’s use and as otherwise permitted by these Terms of Use and the Services, (b) User have
                              the rights necessary to grant the licenses and sub-licenses described in these Terms of Use,
                              and (c) that each person depicted in such Images, if any, has provided consent to the use of
                              the Images as set forth in these Terms of Use, including, by way of example, and not as a
                              limitation, the distribution, public display and reproduction of such Images. By Posting
                              Images, User is granting (a) to all members of User’s private community (for each such Images
                              available to members of such private community), and/or (b) to the general public (for each
                              such Images available anywhere on the Services, other than a private community), permission
                              to use User’s Images in connection with the use, as permitted by these Terms of Use, of any
                              of the Services, (including, by way of example, and not as a limitation, making prints and
                              gift items which include such Images), and including, without limitation, a non-exclusive,
                              world-wide, royalty-free license to: copy, distribute, transmit, publicly display, publicly
                              perform, reproduce, edit, translate and reformat User’s Images without having User’s name
                              attached to such Images, and the right to sublicense such rights to any supplier of the Services.
                              The licenses granted in the preceding sentences for a Images will terminate at the time User
                              completely remove such Images from the Services, provided that, such termination shall not
                              affect any licenses granted in connection with such Images prior to the time User completely
                              remove such Images. No compensation will be paid with respect to the use of User’s Images.</p>
          
                              <h5>12.	Disclaimer of warranty; limitation of liability</h5>
                              <ol type="A">
                              <li>User expressly agrees that use of MED is at user's sole risk. Neither MED,
                              its affiliates nor any of their respective employees, clients, agents, third
                              party content providers or licensors warrant that MED will be uninterrupted
                              or error free; nor do they make any warranty as to the results that may be
                              obtained from use of MED, or as to the accuracy, reliability or content of any
                              information, service, or merchandise provided through MED.</li>
          
                              <li>MED is provided on an "as is" basis without warranties of any kind, either express or implied,
                              including, but not limited to, warranties of title or implied warranties of merchantability or
                              fitness for a particular purpose, other than those warranties which are implied by and incapable
                              of exclusion, restriction or modification under the laws applicable to this agreement.</li>
          
                              <li>This disclaimer of liability applies to any damages or injury caused by any failure of performance,
                              error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus,
                              communication line failure, theft or destruction or unauthorised access to, alteration of, or use of
                              record, whether for breach of contract, tortious behaviour, negligence, or under any other cause of
                              action. User specifically acknowledges that MED is not liable for the defamatory, offensive or illegal
                              conduct of other users or third-parties and that the risk of injury from the foregoing rests entirely
                              with user.   </li>
          
                              <li>In no event will MED, or any person or entity involved in creating, producing or distributing MED
                              or the MED software, be liable for any damages, including, without limitation, direct, indirect,
                              incidental, special, consequential or punitive damages arising out of the use of or inability to use
                              MED. User hereby acknowledges that the provisions of this section shall apply to all content on the site.
          
                              <li>In addition to the terms set forth above neither, MED, nor its affiliates, information providers or
                              content partners shall be liable regardless of the cause or duration, for any errors, inaccuracies,
                              omissions, or other defects in, or untimeliness or inauthenticity of, the information contained within
                              MED, or for any delay or interruption in the transmission thereof to the user, or for any claims or
                              losses arising therefrom or occasioned thereby. None of the foregoing parties shall be liable for any
                              third-party claims or losses of any nature, including, but not limited to, lost profits, punitive or
                              consequential damages.</li>
          
                              <li>Force Majeure – neither party will be responsible for any failure or delay in performance due to
                              circumstances beyond its reasonable control, including, without limitation, acts of god, war, riot,
                              embargoes, acts of civil or military authorities, fire, floods, accidents, service outages resulting
                              from equipment and/or software failure and/or telecommunications failures, power failures, network
                              failures, failures of third party service providers (including providers of internet services and
                              telecommunications). The party affected by any such event shall notify the other party within a
                              maximum of fifteen (15) days from its occurrence. The performance of this agreement shall then
                              be suspended for as long as any such event shall prevent the affected party from performing its
                              obligations under this agreement.</li>
                              </ol>
          
          
                              <h5>13.	Links to third party websites</h5>
          
                              <p>The links in this area will let you leave MED's site. The linked sites are not under the control
                              of MED and MED is not responsible for the contents of any linked site or any link contained in a
                              linked site, or any changes or updates to such sites. MED is not responsible for webcasting or any
                              other form of transmission received from any linked site. MED is providing these links to you only
                              as a convenience, and the inclusion of any link does not imply endorsement by MED of the site.</p>
                              
          
                              <h5>14.	Unsolicited idea submission policy</h5>
          
          
                              <p>MED or any of its employees do not accept or consider unsolicited ideas, including ideas for new
                              advertising campaigns, new promotions, new products or technologies, processes, materials, marketing
                              plans or new product names. Please do not send any original creative artwork, samples, demos, or other
                              works. The sole purpose of this policy is to avoid potential misunderstandings or disputes when MED's
                              products or marketing strategies might seem similar to ideas submitted to MED. So, please do not send
                              your unsolicited ideas to MED or anyone at MED. If, despite our request that you not send us your
                              ideas and materials, you still send them, please understand that MED makes no assurances that your
                              ideas and materials will be treated as confidential or proprietary.</p>
                              
          
          
          
                              <h5>15.	Monitoring </h5>
                              <p>MED shall have the right, but not the obligation, to monitor the content of MED, including
                              discussion areas, to determine compliance with this Agreement and any operating rules established by
                              MED and to satisfy any law, regulation or authorised government request. MED shall have the right in
                              its sole discretion to edit, refuse to post or remove any material submitted to or posted on MED.
                              Without limiting the foregoing, MED shall have the right to remove any material that MED, in its
                              sole discretion, finds to be in violation of the provisions hereof or otherwise objectionable.</p>
                              
          
          
                              <h5>16.	Indemnification </h5>
                              <p>User agrees to defend, indemnify and hold harmless MED, its affiliates and their respective
                              directors, officers, employees, clients and agents from and against all claims and expenses,
                              including attorneys' fees, arising out of the use of MED by User or User's Account.</p>
                              
          
          
                              <h5>17.	Termination </h5>
                              <p>Either MED or User may terminate this Agreement at any time. Without limiting the foregoing,
                              MED shall have the right to imMEDiately terminate User's Account in the event of any conduct by
                              User which MED, in its sole discretion, considers to be unacceptable, or in the event of any breach
                              by User of this Agreement.</p>
                              
          
          
                              <h5>18.	Miscellaneous</h5>
                               <p>This Agreement and any operating rules for MED established by MED  constitute the entire agreement
                              of the parties with respect to the subject matter hereof, and supersede all previous written or oral
                              agreements between the parties with respect to such subject matter. This Agreement shall be construed
                              in accordance with the laws of the United States, without regard to its conflict of laws rules. No
                              waiver by either party of any breach or default hereunder shall be deeMED to be a waiver of any
                              preceding or subsequent breach or default. The section headings used herein are for convenience
                              only and shall not be given any legal import.</p>
                              
          
          
                              <h5>19.	Copyright notice</h5>
          
                              <p>MED and its logos are trademarks of Middle East Doctor LLC. All rights reserved. All other trademarks
                              appearing on MED are the property of their respective owners.</p>
                              
          
          
                              <h5>20.	Trademarks</h5>
          
                              <p>The names of actual companies and products mentioned herein may be the trademarks of their
                              respective owners. The example companies, organisations, products, domain names, email addresses,
                              logos, people and events depicted herein are fictitious. No association with any real company,
                              organisation, product, domain name, e-mail address, logo, person, or event is intended or should
                              be inferred.</p>
                              
                              <p>Any rights not expressly granted herein are reserved.</p>
          
          
                </div>
        </div>
        </div>
        @include('patient.templates.sidebar-ads')

        </div>
    </div>
</div>
@stop
