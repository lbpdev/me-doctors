<div class="hs_single_profile_detail padding-b-5 clearfix">
<hr class="margin-b-5 margin-t-0">
    <h3 class="no-border blue hs_heading margin-b-0 padding-b-0 margin-t-0 clearfix">
        <span class="page-title"><i class="fa fa-user blue"></i> User Profile</span>
    </h3>
<hr class="margin-tb-5">
<div class="row">
    <div class="col-md-3 col-sm-3  col-xs-3 col-xxs-12 padding-b-0">
        <img src="{{ asset($user->avatar) }}" alt="{{ $user->name }}" width="100%"/>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-9 col-xxs-12 ">
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0margin-tb-0 padding-tb-0">
            <div class="row">
                <h3 class="margin-t-7 margin-tb-0">{{ $user->name }}</h3>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-tb-0 padding-tb-0">
            <div class="row">
              <h4 class="margin-tb-10">
              @foreach($user->specialties as $index=>$specialty)
                  {{ $specialty->name }}{{ ($index+1) < count($user->specialties) ? ', ' : '' }}
              @endforeach
              </h4>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 margin-tb-0 padding-tb-0">
                <div class="row line-height-22">
                    @if ($user->contacts)
                        @if($user->workPhone)
                            <p class="text-capitalize">
                                <span class="small-icon phone"></span>
                                {{ $user->workPhone->number }} ({{ $user->workPhone->type }})
                            </p>
                        @endif
                        @if($user->personalPhone)
                            <p class="text-capitalize">
                                <span class="small-icon phone"></span>
                                {{ $user->personalPhone->number }} ({{ $user->personalPhone->type }})
                            </p>
                        @endif
                    @else
                        <p>No phone number.</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="ool-md-12">
        @if($authUser->username == $user->username)
            <a href="{{ route('patient.edit_personal_profile', $user->username) }}">
                <button class="button t-black">[ <i class="fa fa-edit"></i> Edit Profile ]</button>
            </a>
            <br>
            @if(Session::has('registration.success'))
                <div class="alert alert-success margin-t-20">
                    {{ Session::get('registration.success')  }}
                </div>
            @endif
            @if(!$user->isVerified)
                <div class="alert alert-danger margin-t-20">
                    This account is not yet verified. <br>
                    Please check your email or click <a class="" href="#" data-toggle="modal" data-target="#reVerifyModal">Here</a> to resend verification.
                </div>
            @endif

        @endif
        </div>
    </div>
</div>

@include('patient.templates.modals.reverify')
