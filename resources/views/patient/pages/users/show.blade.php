@extends('patient.master')

@section('style')
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
@stop

@section('content')
    <div class="container profile page-container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 border-right">
                <div class="hs_single_profile">

                @include ('patient.partials._news-ticker')

                    @include ('patient.pages.users.profile.personal')
                    <hr class="margin-tb-5">


                        </div>
                    </div>
                    </div>
                    @include('patient.templates.sidebar-ads')
                </div>

        </div>
    </div>

    <div class="hs_margin_40"></div>
@stop
@section('js')
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/custom/profileFields.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script type="text/javascript">
      $('select').select2();
    </script>


    @if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== true)

        <script type="text/javascript">

                $( ".dataForm" ).submit(function( event ) {

                  var $form = $( this ),
                    data = $form.serialize(),
                    url = $form.attr( "action" );
                    $.ajax({
                      type: "POST",
                      url: url,
                      data: { formData : data },
                      beforeSend: function (xhr) {
                         var token = $('meta[name="csrf_token"]').attr('content');

                         if (token) {
                               return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                         }
                      },
                      success: function(results){
                          var data = JSON.parse(results);
                          if(data.length > 0){
                              if(data[0].location === undefined )
                                updateSingle($(event.target), data);
                              else
                                updateMulti($(event.target), data);
                          } else {
                            $(event.target).closest('.section').find('.dataList').empty();
                            toggleForms($(event.target));
                          }
                      },
                      error: function(xhr, status, error) {
                          $(event.target).closest('.section').find('.alert-danger').show().delay(3000).fadeOut(3000);
                      }
                    });
                  event.preventDefault();
                });
////////////////////////////////////////
                // UPDATE HTML WITH NEW DATA
                //////////////////////////////////////////

                function toggleForms(target){
                    target.closest('.add-form').toggle();
                    target.closest('.section').find('.add-toggle').toggle();
                }

                function updateSingle(target, data){
                  target.closest('.section').find('.alert-success').show().delay(3000).fadeOut(3000);
                    var holder = target.closest('.section').find('.dataList');
                    holder.empty();

                    if(data){
                        for(var i in data) {
                            holder.append('<span>'+data[i]+'</span>');
                        }
                    }
                    toggleForms(target);
                    event.preventDefault();
                }

                ////////////////////////////////////////
                            // UPDATE HTML WITH NEW DATA
                            //////////////////////////////////////////

                function updateMulti(target, data){

                  var days = ['Sun' , 'Mon' , 'Tue' , 'Wed' , 'Thu' , 'Fri' , 'Sat'];
                  target.closest('.section').find('.alert-success').show().delay(3000).fadeOut(3000);
                    var holder = target.closest('.section').find('.dataList');
                    holder.empty();

                    if(data){
                        for(var i in data) {
                            holder.append('<b>'+data[i]['name']+'</b><br>');
                            holder.append('<span>'+data[i]['location']+'</span>');


                            if(data[i]['schedules']){
                                holder.append('<b>Days:</b> ');
                                data[i]['schedules'].forEach(function(entry) {
                                    holder.append(days[entry]+' , ');
                                });
                            }
                            holder.append('<hr>');
                        }
                    }

                    toggleForms(target);

                    $('html,body').animate({scrollTop: target.closest('.section').offset().top - 50});
                    event.preventDefault();
                }

          function initMap(index) {
            var map = new google.maps.Map(document.getElementsByClassName('map')[index], {
                zoom: 12,
                disableDefaultUI: true,
                draggable: false,
                zoomControl: false,
                scrollwheel: false,
                disableDoubleClickZoom: true
            });
            var geocoder = new google.maps.Geocoder();

            geocodeAddress(geocoder, map , index);
          }

          function geocodeAddress(geocoder, resultsMap ,index) {
            var address = $('input.location')[index].value;
            var map = $('.map')[index];
            if(address=="")
                address = "Dubai - United Arab Emirates";

            geocoder.geocode({'address': address}, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                $(map).find('.gm-style').show();
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                  map: resultsMap,
                  position: results[0].geometry.location
                });
              } else {
              $(map).find('.gm-style').hide();
              }
            });
          }

        ////////////////////////////////////////
                    // BIND THE AUTOCOMPLETE EVENT TO ALL FIELDS WITH CLASS ''
                    //////////////////////////////////////////

        function autoCompleteBind(){
            $(".work-name").autocomplete({
                source    : <?php echo json_encode($hospitals); ?>,
                autoFocus : true,
                select: function(event, ui){
                    $(event.target).parent().find('#address input').val();
                    $(event.target).parent().find('.location').val(ui.item.location);
                    $(event.target).parent().find('.street_number').val(ui.item.street_number);
                    $(event.target).parent().find('.route').val(ui.item.street_route);
                    $(event.target).parent().find('.postal_code').val(ui.item.post_code);
                    $(event.target).parent().find('.locality').val(ui.item.city);
                    $(event.target).parent().find('.administrative_area_level_1').val(ui.item.state);
                    $(event.target).parent().find('.country').val(ui.item.country);
                    var target = $(event.target).closest('.fields');

                    initMap($('.fields-group .fields').index(target));
                }
            });

            $(".work-name").on( "focus", function( event ) {
                $(event.target).autocomplete( "search", " " );
            });
        }
        autoCompleteBind();
        </script>
    @endif

@stop
