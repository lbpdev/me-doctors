<div class="panel with-nav-tabs panel-default clearfix latest-tabs">
    <div class="col-md-12 col-sm-12 no-padding">

        @include('doctor.partials._tweets')

        <div class="sidebar-element clearfix">
            @if($authUser)
            @else
                <a href="{{ route('patient.login') }}">
                    <img class=" width-full" src="{{ asset('public/images/ads/register-side.png') }}" alt="register med">
                </a>
            @endif
            
            @include('templates.box-event')
            @include('templates.sidebar-long')
        </div>
    </div> <!---- COL-MD-12 ----->
</div>