@if ($poll)
    <div class="panel panel-primary transparent-bg no-margin-bottom">
        <div class="panel-heading">
            <h3 class="panel-title robotolight blue">{{ $poll->title }}</h3>
        </div>
        <div class="panel-body padding-0 transparent-bg margin-t-10">

            <ul class="list-group transparent-bg margin-b-0 padding-r-20">
                @if ($voted = $poll->items->contains('voted', 1))
                    <li class="list-group-item transparent-bg">
                        <label><p>Thank you for voting. You voted for "{{ $poll->items->where('voted', 1)->first()->body }}"</p></label>
                    </li>
                @else
                    {!! Form::hidden('poll_id', $poll->id ); !!}
                    @foreach ($poll->items as $index=>$item)
                        <li class="list-group-item transparent-bg no-border">
                            <table><tr>
                                <td>
                                <input type="radio" name="poll_item[]" value="{{ $item->id }}" id="radio{{ $index }}">
                                <label for="radio{{ $index }}"><span></span></label>
                                </td>
                                <td><label class="margin-l-5"> {{ $item->body }} </label></td>
                            </tr></table>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>

        @if (!$voted)
            <div class="panel-footer padding-tb-0 text-center transparent-bg relative">
                @if($authUser)
                    {!! Form::submit('Vote', ['class'=>'btn btn-primary btn-block btn-sm'] ) !!}
                @else
                    <a href="{{ route('patient.login') }}">Login to Vote.</a>
                @endif
                {{--<a href="#" class="small">View Result</a>--}}
            </div>
        @endif
     </div>
@endif