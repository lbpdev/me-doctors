<div class="panel with-nav-tabs margin-b-0 panel-default clearfix latest-tabs">
        <h4 class="pull-left no-margin padding-8 width-full blue-bg t-white robotolight text-center text-uppercase">
            Most Popular <br> Discussions
        </h4>
            <div class="col-md-12 no-padding">
                <div class="panel with-nav-tabs panel-default margin-b-7">
                <div class=" sidebar-element panel-body clearfix">
                    <div class="tab-content clearfix">
                        <div class="tab-pane fade in active" id="tab1default">
                            <ul class="discussions-list clearfix">
                                @if ($discussions->count())
                                    @foreach ($discussions as $index=>$discussion)
                                        <li>
                                            <div class="col-md-1 col-sm-1  col-xs-1 no-padding">
                                                <span class="counts">{{ $index+1 }}</span>
                                            </div>
                                            <div class="col-md-11 col-sm-11 col-xs-11 padding-r-10 padding-l-0">
                                                <a href="{{ route('patient.discussions.show', $discussion->slug )}}">
                                                {{ Str::limit($discussion->title,53) }}
                                                </a>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>