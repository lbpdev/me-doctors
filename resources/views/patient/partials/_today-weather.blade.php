<div class="col-md-12 col-sm-12 time-n-where clearfix padding-t-0 padding-b-5 ">
    <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12 no-padding">
            <span>{{ Carbon::now()->format('l') }}</span>
            <span> {{ Carbon::now()->format('j') }}<sup>{{ Carbon::now()->format('S') }}</sup></span>
            <span>{{ Carbon::now()->format('F') }}</span>
            <span>{{ Carbon::now()->format('Y') }}</span>
        </div>
        <div class="col-md-8 col-sm-7 col-xs-12 no-padding text-right location">
            <span class="pull-right">{{ $today['city'] }}</span>
            <div class="pull-right margin-r-10 padding-r-10" style="border-right: 1px dotted #ccc;">
                <span class="weather-icon margin-r-5 moon pull-left"></span>
                <p id="sunset">{{ $today['sunset'] }}</p>
            </div>
            <div class="pull-right margin-r-10 padding-r-10" style="border-right: 1px dotted #ccc;">
                <span class="weather-icon sun pull-left margin-r-5"></span>
                <p id="sunrise">{{ $today['sunrise'] }}</p>
            </div>
       </div>
   </div>
</div>
