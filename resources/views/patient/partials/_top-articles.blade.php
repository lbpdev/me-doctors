<div class="panel with-nav-tabs margin-b-0 panel-default clearfix latest-tabs">

            <div class="col-md-12 no-padding">

                <div class="sidebar-element panel-body clearfix margin-b-0">
                    <h4 class="no-margin padding-b-5 blue-bg t-white robotoregular">Top Five</h4>
                    <div class="col-md-12 blue-bg padding-b-7 padding-rl-7 padding-t-0">
                        <div class="custom-select white-bg">
                            <select class="form-control" onchange="toggleSection()" id="sidebarTherapySelect">
                                    <option value="all" selected>Overall</option>
                                @foreach($articlesTherapy as $therapy)
                                    <option value="{{ $therapy['id'] }}">{{ $therapy['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="sidebar-element panel-body clearfix gray-bg padding-lr-7 margin-b-7">
                    <div class="tab-content clearfix">
                    <ul class="therapy-list clearfix therapy-list" id="therapy-list-all">
                        @foreach ($popularArticles as $index=>$article)
                            <li>
                                <div class="col-md-1 col-sm-1  col-xs-1 no-padding">
                                    <span class="counts">{{ $index+1 }}</span>
                                </div>
                                <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                                    <a href="{{ route('patient.articles.show', $article->slug )}}">{{ Str::limit($article->title, 50) }}</a>
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    @foreach($articlesTherapy as $tIndex=>$therapy)
                        <ul class="therapy-list clearfix therapy-list temp-hide" id="therapy-list-{{ $therapy['id'] }}">
                            @if (isset($therapy['articles']))
                                @foreach ($therapy['articles'] as $index=>$article)
                                    @if($index<5)
                                        <li>
                                            <div class="col-md-1 col-sm-1  col-xs-1 no-padding">
                                                <span class="counts">{{ $index+1 }}</span>
                                            </div>
                                            <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                                                <a href="{{ route('patient.articles.show', $article->slug )}}">{{ Str::limit($article->title, 50) }}</a>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            @else
                                <li>
                                    <div class="col-md-12 no-padding">
                                        No articles in this category.
                                    </div>
                                </li>
                            @endif
                        </ul>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('sidebar-js')
    <script>
        function toggleSection(e){
            var selectedTherapy = document.getElementById("sidebarTherapySelect").value;
            var therapylists = document.getElementsByClassName("therapy-list");
            $(".therapy-list").hide();
            $("#therapy-list-" + selectedTherapy).show();
        }
    </script>
@stop