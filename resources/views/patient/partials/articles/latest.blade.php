<div class="row latest-articles">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <h2 class="section-header">Latest Articles</h2>
        @if ($latest->count())
            <div class="ocarousel">
                <div id="owlcarousel" class="owl-carousel owl-theme">
                    @foreach ($latest as $index=>$article)
                        @include ('patient.partials.articles._article')
                    @endforeach
                </div>
                <div class="customNavigation surveys text-right">
                    <a class="btn_prev white prev"><span class="arrow yellow left"></span></a>
                    <a class="btn_next white next"><span class="arrow yellow right"></span></a>
                </div>
            </div>
        @else
            <p>No Surveys</p>
        @endif
    </div>
</div>