<div class="row pull-left width-full latest-discussions">
    <div class="col-lg-12 col-md-12 col-sm-12 padding-r-0">
        <h2 class="section-header">Latest Discussions</h2>
        @if ($panels->count())
            <div class="ocarousel">
                <div id="owlcarousel" class="owl-carousel owl-theme">
                    @foreach ($panels as $index=>$discussion)
                        @include ('patient.partials.discussions._discussion')
                    @endforeach
                </div>
                <div class="customNavigation text-right">
                    <a class="btn_prev white prev"><span class="arrow pink left"></span></a>
                    <a class="btn_next white next"><span class="arrow pink right"></span></a>
                </div>
            </div>
        @else
            <p>No Discussions</p>
        @endif
    </div>
</div>

@section('js-inner')
    <script type="text/javascript" src="{{ asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('public/js/custom/initializeOwlCarousel.js') }}"></script>
@stop