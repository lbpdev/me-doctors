<div class="sidebar-element clearfix rssfeeds ">
    <h4 class="section-header margin-t-0 newsfeed pull-left">
    NEWS FEED
        <a href="{{ route('patient.news.index') }}">
            <span class="action-title pull-right font-14 " style="line-height: 25px;">
            View All
            </span>
        </a>
    </h4>
    <ul class="media-list rss padding-l-0" id="feed">
    @if(count($news))
        @foreach ($news as $newsFeed)
            <li class="margin-b-7 clearfix">
                <div class="media-body col-md-12 padding-tb-10">
                    <p class="media-heading" style="line-height: 17px;">
                        <a href="{{ route('patient.news.show', $newsFeed['slug']) }}">
                            {{ Str::limit($newsFeed['title'], 63) }}
                        </a>
                    </p>
                </div>
            </li>
        @endforeach
    @else
        <p>There are currently no news feeds.</p>
    @endif
    </ul>
</div>