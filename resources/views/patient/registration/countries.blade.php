<div class="row">
    <div class="form-group col-md-12">
        
        <h4>In which country do you practice medicine?</h4>
    
        {!! Form::select('country', ['' => 'Choose country'] + $countries, null, ['class' => 'margin-b-5 form-control', 'id' => 'country']) !!}

        <div class="alert alert-danger temp-hide" id="country-alert">
            Sorry but membership for this site is only available for doctors in the middle-east.
        </div>
    </div>
</div>