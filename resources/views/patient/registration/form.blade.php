<div class="form-group">
    <div class="form-group col-md-6 col-sm-6 col-xs-12 {{ $errors->first('fname') ? 'has-error' : '' }}">
        {!! Form::input('text' , 'fname' , null, [
            'required',
            'class' => 'form-control',
            'placeholder' => 'First Name...'
        ]) !!}
        {!! $errors->first('fname','<span class="help-block">:message</span>') !!}
        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-l-0 {{ $errors->first('lname') ? 'has-error' : '' }}">
        {!! Form::input('text' , 'lname', null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Last Name...']) !!}
        {!! $errors->first('lname','<span class="help-block">:message</span>') !!}
        <div class="help-block with-errors"></div>
    </div>
</div>

<div class="form-group col-md-12 {{ $errors->first('username') ? 'has-error' : '' }}">
    {!! Form::input('text' , 'username' , null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Username...']) !!}
    {!! $errors->first('username','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group col-md-12 {{ $errors->first('email') ? 'has-error' : '' }}">
    {!! Form::email('email', null, [
        'required',
        'class'             => 'form-control',
        'type'              => 'email',
        'placeholder'       => 'E-mail...',
        'data-native-error' => 'Please enter a valid email address.'
    ]) !!}
    {!! $errors->first('email','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group col-md-12 {{ $errors->first('password') ? 'has-error' : '' }}">
    {!! Form::password('password', [
        'required',
        'placeholder'          => 'Password...',
        'data-minlength'       => '6',
        'data-minlength-error' => 'The password is not long enough – please enter a longer password.',
        'class'                => 'form-control password-peeker',
        'id'                   => 'inputPassword'
    ]) !!}
    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

<div class="form-group col-md-12 {{ $errors->first('designation') ? 'has-error' : '' }}">
    {!! Form::input('text' , 'designation' , null, [ 'required', 'class' => 'form-control' , 'placeholder' => 'Designation...']) !!}
    {!! $errors->first('designation','<span class="help-block">:message</span>') !!}
    <div class="help-block with-errors"></div>
</div>

@if (session()->has('registration.error'))
    <div class="col-md-12">

        <div class="alert alert-danger" role="alert">
            {{ session('registration.error') }}
        </div>

        @include('doctor.registration.email_verification_error')
    </div>
@endif