@extends('doctor.master')

@section('styles')
    @parent
    <link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/css/jquery-ui.min.css') }}" media="screen"/>
@stop

@section('content')
    <div class="container login-container margin-t-20">

        {!! Form::model($user, [
            'route'       => 'doctor.register',
            'data-toggle' => 'validator' ,
            'role'        => 'form' ,
            'class'       => 'clearfix',
            'files'       => true
        ]) !!}

        <div class="row">

            <div class="col-md-12">
                @include('doctor.registration.countries')
            </div>

            @include('doctor.registration.form')
        </div>

        <div class="doctor-verification">
            <p>Upon submitting the form below, along with the required documents, your credentials will be verified by our team and you will receive an email about your application within 48 hours.</p>

            <p>Please upload the following up-to-date documents so that Middle East Doctor can verify your credentials:</p>

            <ul>
                <li>Medical License (issued by health authority)</li>
                <li>Photo Identification (driving license or other government issued ID)</li>
                <li>CV/Resume (optional)</li>
                <li>Academic certificates (optional)</li>
            </ul>
        </div>
        
        <div class="form-group">
            <div class="padding-l-0 col-md-6 col-sm-6 col-xs-12 {{ $errors->first('license_number') ? 'has-error' : '' }}">
                {!! Form::input('text', 'place_of_employment', null, ['class' => 'form-control', 'placeholder' => 'Place of Employment']) !!}
                {!! $errors->first('place_of_employment', '<span class="help-block">:message</span>') !!}
                <div class="help-block with-errors"></div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 padding-l-0 {{ $errors->first('phone_number') ? 'has-error' : '' }}">
                {!! Form::input('text', 'phone_number', null, ['class' => 'form-control', 'placeholder' => 'Phone Number']) !!}
                {!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="padding-l-0 col-md-6 col-sm-6 col-xs-12 {{ $errors->first('license_files') ? 'has-error' : '' }}">
                {!! Form::label('license_files', 'Medical License (Required):') !!}
                {!! Form::input('file', 'license_files[]' , null , [ 'class' => 'form-control', 'multiple' => true ]) !!}
                {!! $errors->first('license_files', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="padding-l-0 col-md-6 col-sm-6 col-xs-12 {{ $errors->first('photo_files') ? 'has-error' : '' }}">
                {!! Form::label('photo_files', 'Photo Identification (Required):') !!}
                {!! Form::input('file', 'photo_files[]', null, [ 'class' => 'form-control', 'multiple' => true ]) !!}
                {!! $errors->first('photo_files', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    
        <div class="clearfix"></div>

        <div class="form-group {{ $errors->first('other_files') ? 'has-error' : '' }}">
            {!! Form::label('other_files', 'Other Attachments (Optional) e.g. CV/Resume, etc. ') !!}
            {!! Form::input('file', 'other_files[]', null, [ 'class' => 'form-control', 'multiple' => true ]) !!}
            {!! $errors->first('other_files', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group text-center">
            {!! Form::submit('Verify my credentials', ['class' => 'submit button']) !!}
        </div>
        
        {!! Form::close() !!}
    </div>
@stop

@section('js')
    @include('doctor.auth.scripts')
@stop