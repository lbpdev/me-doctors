<div class="col-md-3 clearfix border-right-light padding-l-0 padding-r-10">
    <h3 class="no-border yellow hs_heading margin-b-5 padding-b-0 margin-t-0 clearfix">
        <span class="page-icon about"></span>
        <span class="page-title doctor-blue">About</span>
    </h3>
    <div class="col-md-12 gray-bg padding-tb-10">
        <div class="row">
            <ul class="about-list blue">
                 <li class="clearfix height-auto">
                   <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                         <h4 class="media-heading clearfix">
                             <a href="{{ url('patient/about') }}" class="{{ strpos(Request::path(), 'about')  ? 'active' : '' }} pull-left">About MED</a>
                         </h4>
                   </div>
                 </li>
                 {{--<li class="clearfix height-auto">--}}
                   {{--<div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">--}}
                         {{--<h4 class="media-heading clearfix">--}}
                             {{--<a href="{{ url('patient/etiquette') }}" class="{{ strpos(Request::path(), 'etiquette')  ? 'active' : '' }} pull-left">Etiquette</a>--}}
                         {{--</h4>--}}
                   {{--</div>--}}
                 {{--</li>--}}
                 <li class="clearfix height-auto">
                   <div class="height-auto col-md-12 col-sm-12 col-xs-12 padding-0">
                         <h4 class="media-heading clearfix">
                             <a href="{{ URL::to('patient/terms') }}" class="{{ strpos(Request::path(), 'terms')  ? 'active' : '' }} pull-left">Terms of Use</a>
                         </h4>
                   </div>
                 </li>
            </ul>
        </div>
    </div>
</div>