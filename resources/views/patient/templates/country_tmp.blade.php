
<div class="row">
    <div class="form-group col-md-12 {{ $errors->any() ? 'has-error' : '' }}">
        
        <h4>In which country do you practice medicine?</h4>
    
        {!! Form::select('country', ['' => 'Choose country'] + $countries, null, ['class' => 'margin-b-5 form-control', 'id' => 'country']) !!}

        <div class="alert alert-danger temp-hide margin-t-10" id="country-alert">
            Thank you for your interest in Middle East Doctor. This website is only available for healthcare professionals that are practising in the Middle East.
        </div>
    </div>

</div>
