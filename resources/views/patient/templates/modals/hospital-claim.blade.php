<!-- Modal -->
<div class="modal fade" id="claim" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center pink">Claim Hospital Listing</h4>
      </div>

      <div class="modal-body text-center">
           @if(!$authUser)
               <p class="margin-b-7">In order to claim this listing you need to register or log into the site.<br></p>
                <p><a href="{{ route('patient.login') }}">Click here to proceed</a></p>
           @else
               {!! Form::open(['id' => 'claimForm','route' => 'patient.hospitals.claim']) !!}

                @if($hospital->ownedBySomeone)
                    <p class="margin-b-7">This listing has already been claimed. If you would also like to  gain access<br> to this listing, please enter your email address below.</p>
                @else
                    <p class="margin-b-7">In order to claim this listing, we need to verify you via email. <br> Please enter your company address below.</p>
                @endif

               <div class="alert alert-danger temp-hide" id="invalidEmail">
                <p class="margin-b-7">Invalid e-mail. Please use a company e-mail.</p>
               </div>
               {!! Form::hidden('hospital_id', $hospital->id) !!}
               {!! Form::input('email', 'email', null , ['id' => 'email' ,'class' => 'form-control','required']) !!}
               {!! Form::close() !!}
               <div class="width-full text-center">
                {!! Form::button('Submit',['id' => 'submitClaim', 'class' => 'button pink font-20 margin-tb-5 padding-l-0']) !!}
               </div>
           @endif
      </div>
    </div>

  </div>
</div>