<!-- Modal -->
<div class="modal fade" id="ratingHelpModal" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center pink">KEY</h4>
      </div>

      <div class="modal-body text-center">
           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-friendliness"></i> Friendliness</h4>
           <p>Was the doctor friendly and courteous<br>
           during your appointment?</p>

           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-professionalism"></i> Professionalism</h4>
           <p>Did the doctor conduct himself professionally<br>
           throughout the appointment?</p>

           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-bed-manner"></i> Bedside manner</h4>
           <p>Was the doctor’s approach and attitude<br>
           positive throughout the appointment?</p>

           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-medical-knowledge"></i> Medical knowledge</h4>
           <p>Did the doctor exhibit good medical<br>
           knowledge surrounding your condition?</p>

           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-helpfulness"></i> Helpfulness</h4>
           <p>Did the doctor successfully treat your<br>
           symptoms and provide a helpful diagnosis<br>
           and treatment course?</p>

           <h4 class="pink  robotolight"><i class="font-28 pink rate-icon ri-waiting-time"></i> Waiting Time</h4>
           <p>Was the doctor punctual or were you<br>
           kept waiting for a long time?</p>

      </div>
    </div>

  </div>
</div>