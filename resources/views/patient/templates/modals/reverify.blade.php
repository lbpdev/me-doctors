<!-- Modal -->
<div class="modal fade" id="reVerifyModal" role="dialog">
  <div class="modal-dialog" style="width: auto">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title text-center pink font-20">Resend Email Verification</h5>
      </div>

      <div class="modal-body text-center">
           <div class="panel-body">
                <div class="alert alert-success temp-hide float-none" id="successMessage"></div>
                   <form class="form-horizontal text-center" id="resendForm" role="form" method="POST">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="alert alert-danger temp-hide float-none" id="invalidEmail">
                            <p class="margin-b-7">Invalid e-mail. <br>Please use a working email.</p>
                        </div>

                       <label class="col-md-12 text-center no-padding">E-mail Address</label>
                       <div class="col-md-12 no-padding">
                           <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                       </div>
                   </form>

                   <div class="col-md-12 no-padding margin-t-10">
                       <button type="submit" id="emailSubmit" class="btn btn-primary blue-bg">
                           Submit
                       </button>
                   </div>
           </div>
      </div>
    </div>

  </div>
</div>

@section('js')
    <script>
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    $('#email').keydown(function(event){
    console.log(event.keyCode);
       if(event.keyCode === 13) {

        if(re.test($(this).val()))
            $('#invalidEmail').hide();
        else
        {
            event.preventDefault();
            $('#invalidEmail').show().text('Invalid email. Please use a working email.');
            return false;
        }
        $('#emailSubmit').trigger('click');
        return false;
       }
     });

    $('#emailSubmit').on('click',function(){
        var url = '{{ route('patient.resend-verify.execute') }}';
        var user_id = '{{ $authUser->id }}';
        $.ajax({
          url: url,
          type: 'POST',
          data: { email : $('#email').val(), user_id : user_id },
          context: document.body,
          success: function(data){
          console.log(data);
            if(data['result']==1){
                $('#invalidEmail').hide();
                $('#resendForm').hide();
                $('#emailSubmit').hide();
                $('#successMessage').show().html('').append(data['text']);
            }
            else{
                event.preventDefault();
                $('#invalidEmail').show().text(data['text']);
                return false;
            }

          return false;
          }
        }).done(function() {
          $( this ).addClass( "done" );
        });


    });
    </script>
@endsection

