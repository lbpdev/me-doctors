<div class="clearfix col-md-3 col-sm-12 sidebar margin-t-10 padding-r-0">


    <div class="panel with-nav-tabs panel-default clearfix latest-tabs">
            <div class="col-md-12 no-padding">

            <div class="sidebar-element clearfix">
                <h4>Lastest Articles</h4>
                <div class="col-md-12 padding-10 side-poll">
                    <div class="tab-pane fade in active" id="tab1default">
                        <ul class="discussions-list clearfix">
                        @if($popular)
                            @foreach($popular as $discussion)
                                <li>
                                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                    <a href="{{ route('posts_single',$discussion['slug'] )}}">{{ $discussion['title'] }}</a></div>

                                </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
            </div>

            {{--<div class="sidebar-element clearfix">--}}
                {{--<div class="col-md-12 padding-10 side-poll">--}}
                    {{--<h2>To blaborro omnimus mi?</h2>--}}
                    {{--{!! Form::open() !!}--}}
                        {{--<div class="text-left margin-b-20 clearfix">--}}
                            {{--<div class="col-md-12">{!! Form::radio('value' , 1 ) !!} Yes</div>--}}
                            {{--<div class="col-md-12">{!! Form::radio('value' , 1 ) !!} No</div>--}}
                        {{--</div>--}}
                        {{--<div class="text-left">--}}
                            {{--{!! Form::submit('Vote' , ['class' => 'form-control']) !!}--}}
                        {{--</div>--}}
                    {{--{!! Form::close() !!}--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="sidebar-element clearfix">
                <div class="col-md-12 no-padding side-ad">

                @if(Auth::check())
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x455" width="100%"></div>
                @else
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x220" width="100%"></div>
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x220" width="100%"></div>
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x455" width="100%"></div>
                @endif
                </div>
            </div>
        </div> <!---- COL-MD-12 ----->
    </div>
</div>