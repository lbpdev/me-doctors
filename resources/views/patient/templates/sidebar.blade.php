<div class="clearfix pull-left col-md-3 col-sm-4 sidebar margin-t-10 padding-r-0">
    {!! Form::open() !!}
        <div class="search-field">
            {!! Form::input('text' , 'keyword' , null , ['class' => 'form-control']) !!}
            <span class="magnify"><i class="fa fa-search"></i> </span>
        </div>
    {!! Form::close() !!}

    @if(Auth::check())
    <div class="panel with-nav-tabs panel-default clearfix latest-tabs">
        <h4 class="margin-t-0">Most Popular</h4>
            <div class="col-md-12 no-padding">
                <div class="panel with-nav-tabs panel-default">
                    <div class="">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Discussions</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Diagnosis</a></li>
                        </ul>
                    </div>
                <div class=" sidebar-element panel-body clearfix">
                    <div class="tab-content clearfix">
                        <div class="tab-pane fade in active" id="tab1default">
                            <ul class="discussions-list clearfix">
                            @if($data['discussions'] && count($data['discussions']) > 0)
                                @foreach($data['discussions'] as $discussion)
                                    <li>
                                        <div class="col-md-9 col-sm-9 col-xs-9 no-padding">
                                        <a href="{{ URL::to('discussions/'.$discussion['slug'] )}}">{{ $discussion['title'] }}</a></div>
                                        <div class="col-md-3 col-sm-3  col-xs-3 no-padding">
                                        <span class="counts">{{ $discussion['comment_count'] }}</span></div>
                                    </li>
                                @endforeach
                            @endif
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                        <ul class="discussions-list clearfix">
                            @if($data['diagnosis'])
                                @foreach($data['diagnosis'] as $diagnosis)
                                    <li>
                                        <div class="col-md-9 col-sm-9 col-xs-9 no-padding">
                                        <a href="{{ URL::to('discussions/'.$diagnosis['slug']) }}">{{ $diagnosis['title'] }}</a></div>
                                        <div class="col-md-3 col-sm-3  col-xs-3 no-padding">
                                        <span class="counts">{{ $diagnosis['comment_count'] }}</span></div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                        </div>
                    </div>
                </div>
            </div>

            @else
            <div class="sidebar-element clearfix">
                <div class="user-link patient">
                    <img src="{{ asset('public/images/icon/doctor-icon.png') }}" alt="doctor icon">
                    <h2 class="t-white">Patient Looking for a doctor?</h2>
                    <a href="#">Click here</a>
                </div>
            </div>
            @endif

            <div class="sidebar-element clearfix">
                <div class="col-md-12 padding-15 side-poll">
                    {!! Form::open(['route'=>'poll_vote']) !!}
                        @include('pages.home.sidebar')
                    {!! Form::close() !!}
                </div>
            </div>


            <div class="sidebar-element clearfix">
                <div class="col-md-12 no-padding side-ad">
                <div class="col-md-12 col-sm-12 rssfeeds padding-l-0 padding-r-0 row-sm">
                    <h4 class="margin-t-0">Medical News</h4>
                    <ul class="media-list" id="feed">

                   </ul>
                </div>

                @if(Auth::check())
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x455" width="100%"></div>
                @else
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x220" width="100%"></div>
                    <div class="col-md-12 col-sm-12 no-padding col-xs-12"><img src="http://placehold.it/220x220" width="100%"></div>
                @endif
                </div>
            </div>
        </div> <!---- COL-MD-12 ----->
    </div>
</div>