<div class="videos_slider_item padding-5">
    <div class="hs_event_div">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <a class="fancybox" data-type="iframe" href="{{ $video->url }}" title="youtube">
                    <img src="{{ $video->thumbnail }}" class=" width-full">
                    <h5>{{ Str::words($video->title, 12) }}</h5>
                </a>
            </div>
        </div>
    </div>
</div>