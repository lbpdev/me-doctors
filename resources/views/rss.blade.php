{!! '<?xml version="1.0" encoding="utf-8"?>' !!}

<feed xmlns="http://www.w3.org/2005/Atom">

    <title>Middle East Doctor</title>
    <subtitle>Tag line or any other subtitle.</subtitle>
    <link href="http://middleeastdoctor.com/feed/" rel="self" />
    <link href="http://middleeastdoctor.com/" />
    <id>tag:middleeastdoctor.com,{{ date('Y') }}:/feed</id>
    <updated>{{ Carbon::now()->toAtomString() }}</updated>
    
    @foreach ($articles as $article)
        <entry>
            <title>{{ $article->title }}</title>
            <link href="{{ route('doctor.articles.show', $article->slug) }}" />
            <id>{{ article_tag_uri($article) }}</id>
            <summary>{{ Str::words(strip_tags($article->content), 70) }}</summary>
            <content type="xhtml">
                <div xmlns="http://www.w3.org/1999/xhtml">
                    {{ $article->content }}
                </div>
            </content>
        </entry>
    @endforeach
</feed>