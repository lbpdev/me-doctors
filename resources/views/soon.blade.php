
<html>
    <head>
    <title>Middle East Doctor - The Official Website</title>


    <!-- Place this data between the <head> tags of your website -->
    <meta name="description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East." />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Middle East Doctor">
    <meta itemprop="description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East.">
    <meta itemprop="image" content="{{ asset('public/images/MED-Footer.png') }}">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="www.middleeastdoctor.com">
    <meta name="twitter:title" content="Middle East Doctor">
    <meta name="twitter:description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East.">
    <meta name="twitter:creator" content="Middle East Doctor">
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <meta name="twitter:image:src" content="{{ asset('public/images/MED-Footer.png') }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="Middle East Doctor" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="http://middleeastdoctor.com/" />
    <meta property="og:image" content="{{ asset('public/images/MED-Footer.png') }}" />
    <meta property="og:description" content="Middle East Doctor has been created to help connect doctors across the region and to provide them with a community where they can interact with peers across multiple therapy areas and subspecialties. Healthcare professionals (HCPs) can seek expert advice and opinions on treatments, and they can participate in discussions and surveys that can influence how the industry operates in the Middle East." />
    <meta property="og:site_name" content="www.middleeastdoctor.com" />
    <meta property="fb:admins" content="100000370643856" />

    </head>
<body style="padding:0; margin: 0; background-size: auto 100%; background-color:#000; background-image: url({{ asset('public/images/med-coming-soon.png') }}); background-position: center center; background-repeat: no-repeat">
</body>
</html>