<div class="sidebar-element clearfix">
    @if(count($ads['sidebar-sm']))
        <a target="_blank" href="{{ isset($ads['sidebar-sm']->link) ? $ads['sidebar-sm']->link : '#' }}">
        	<img src="{{ $ads['sidebar-sm']->AdImage }}" class=" width-full" alt="MED sidebar ad" title="MED sidebar ad">
    	</a>
    @endif
</div>