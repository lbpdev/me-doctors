<div class="sidebar-element clearfix">
    <?php
    $turn = rand(0,1);
    ?>

    @if($turn==1 && count($ads['sidebar-long']))
        <a target="_blank" href="{{ isset($ads['sidebar-long']->link) ? $ads['sidebar-long']->link : '#' }}"><img class=" width-full" src="{{ $ads['sidebar-long']->AdImage }}" alt="Ad Image" title="Ad Image"></a>
    @else
            <!-- 728x90 MED -->
        <ins class="adsbygoogle margin-b-10"
             style="display:inline-block;width:100%;height: 490px;"
             data-ad-client="ca-pub-0346424792861794"
             data-ad-slot="8006657434"></ins>

        <script type="text/javascript">
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    @endif


</div>