<div class="col-md-9 col-sm-9 col-xs-12 padding-l-0 padding-r-0-xs padding-r-0">

    <?php
        $turn = rand(0,1);
    ?>

    @if($turn==1 && count($ads['top-long']))
        <a target="_blank"  href="{{ isset($ads['top-long']->link) ? $ads['top-long']->link : '#' }}"><img src="{{ $ads['top-long']->AdImage }}" class="long-ad height-full" title="Let patients find you easily. MED Ad" alt="Let patients find you easily. MED Ad"></a>
    @else
        <!-- 728x90 MED -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:728px;height:90px"
             data-ad-client="ca-pub-0346424792861794"
             data-ad-slot="1792000238"></ins>

        <script type="text/javascript">
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    @endif
</div>
