<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
    <?php
        $turn = rand(0,1);
    ?>

    @if($turn==1 && count($ads['top-sm']))
            <a target="_blank" href="{{ isset($ads['top-sm']->link) ? $ads['top-sm']->link : '#' }}"><img src="{{ $ads['top-sm']->AdImage }}" class="short-ad height-full" title="Let patients find you easily. MED Ad" alt="Let patients find you easily. MED Ad"></a>
    @else

    <!-- MED - 245 x 90 - doctors -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:245px;height:90px"
         data-ad-client="ca-pub-0346424792861794"
         data-ad-slot="8895817835"></ins>

    <script type="text/javascript">
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    @endif
</div>