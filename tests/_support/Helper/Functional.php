<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Laracasts\TestDummy\Factory as TestDummy;

class Functional extends \Codeception\Module
{

     /**
     * Prepare a user account, and log in.
     */
    public function logIn()
    {
        $email = 'foo@example.com';
        $username = 'foobar';
        $password = bcrypt('secret');

        $this->haveAnAccount(compact('username', 'email', 'password'));

        $I = $this->getModule('Laravel5');

        $I->amOnPage('/login');
        $I->submitForm('#login-form', [
            'username_email' => $username,
            'password' => 'secret'
        ]);
    }

    /**
     * Visit a dummy discussion.
     *
     * @param  string $title
     * 
     * @return void
     */
    public function visitADiscussion($title)
    {
        $slug = str_slug($title);

        $this->have('panel_discussion', compact('title', 'slug'));

        $I = $this->getModule('Laravel5');

        $I->amOnPage('discussions');
        $I->click($title);
        $I->seeInCurrentUrl($slug);
        $I->amOnPage("/discussions/{$slug}");
    }

    /**
     * Create some dummy therapies.
     *
     * @return void
     */
    public function haveTherapiesLoaded()
    {
        $this->have('MEDoctors\Models\Therapy');
        $this->have('MEDoctors\Models\Therapy');
        $this->have('MEDoctors\Models\Therapy');
    }

    /**
     * Post a discussion
     *
     * @param  string $title
     * @param  string $content
     *
     * @return mixed
     */
    public function postANewDiscussion($title, $content)
    {
        $I = $this->getModule('Laravel5');

        $I->fillField(['name' => 'title'], $title);
        $I->fillField(['name' => 'content'], $content);

        $option = $I->grabTextFrom('select option:first-child');
        $I->selectOption("select", $option);

        $I->click('Submit', 'input[type="submit"]');
    }

    /**
     * Post a comment.
     *
     * @param  string $content
     *      
     * @return void
     */
    public function postAComment($content)
    {
        $I = $this->getModule('Laravel5');

        $I->fillField('content', $content);
        $I->click('Send');
    }

    /**
     * Create a channel for the discussion
     *
     * @param array $overrides
     * @return mixed
     */
    public function haveAChannel($overrides = [])
    {
        return $this->have('MEDoctors\Models\Channel', $overrides);
    }

    /**
     * Create a user account in the database.
     *
     * @param array $overrides
     * @return mixed
     */
    public function haveAnAccount($overrides = [])
    {
        $user = $this->have('MEDoctors\Models\User', $overrides);
        $role = $this->have('MEDoctors\Models\Role');
        $therapy = $this->have('MEDoctors\Models\Therapy');

        $user->roles()->sync([$role->id]);
        $user->specialties()->sync([$therapy->id]);

        return $user;
    }

    /**
     * Insert a dummy record into a database table.
     *
     * @param $model
     * @param array $overrides
     * @return mixed
     */
    public function have($model, $overrides = [])
    {
        return TestDummy::create($model, $overrides);
    }
}
