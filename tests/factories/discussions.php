<?php

$factory('MEDoctors\Models\Channel', [
    'name' => $faker->word,
    'slug' => $faker->slug
]);

$factory('MEDoctors\Models\Channel', 'panel_channel', [
    'name' => 'Panel Discussion',
    'slug' => 'panel'
]);

$factory('MEDoctors\Models\Discussion', 'panel_discussion', [
    'author_id'  => 'factory:MEDoctors\Models\User',
    'channel_id' => 'factory:panel_channel',
    'therapy_id' => 'factory:MEDoctors\Models\Therapy',
    'title'      => $faker->sentence,
    'slug'       => $faker->slug,
    'content'    => $faker->paragraph,
    'status'     => 1
]);