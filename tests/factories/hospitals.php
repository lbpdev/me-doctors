<?php
$company = $faker->company;

$factory('MEDoctors\Models\Hospital', [
    'name' => $company,
    'description' => $faker->paragraph(10),
    'slug' => $faker->slug,
    'verified' => 1
]);