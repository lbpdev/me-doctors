<?php
use MEDoctors\Models\Country;

$valid_countries = [ 'AE', 'SA','OM',  'BH','KW', 'QA', 'JO', 'LB', 'PS' , 'IQ', 'IR', 'TR', 'SY' ];
$countries = Country::whereIn('code', $valid_countries)->orderBy('name')->lists('name');

$factory('MEDoctors\Models\Location', [
    'name' => $faker->address,
    'country' => $faker->randomElement($countries),
    'city' => $faker->randomElement($countries),
    'state' => $faker->state,
    'post_code' => $faker->postcode,
    'street_route' => $faker->streetName
]);