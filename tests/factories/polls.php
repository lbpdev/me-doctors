<?php

$factory('MEDoctors\Models\Poll', [
    'title' => $faker->sentence
]);

$factory('MEDoctors\Models\PollItem', [
    'body' => $faker->sentence
]);