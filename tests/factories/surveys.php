<?php

$factory('MEDoctors\Models\Survey', [
    'title' => $faker->sentence,
    'description' => $faker->paragraph,
    'slug' => $faker->slug
]);

$factory('MEDoctors\Models\SurveyItem', [
    'value' => $faker->sentence,
    // 'survey_id' => 'factory:MEDoctors\Models\Survey',
    'allow_multiple' => $faker->randomElement([0, 1])
]);

$factory('MEDoctors\Models\SurveyItemChoice', [
    'value' => $faker->sentence,
    // 'survey_item_id' => 'factory:MEDoctors\Models\SurveyItem'
]);