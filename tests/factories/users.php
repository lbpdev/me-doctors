<?php

$factory('MEDoctors\Models\Role', [
    'name' => $faker->words(2),
]);

$factory('MEDoctors\Models\Therapy', [
    'name' => $faker->words(2),
]);

$factory('MEDoctors\Models\User', [
    'fname'       => $faker->firstName,
    'lname'       => $faker->lastName,
    'username'    => $faker->unique()->userName,
    'email'       => $faker->unique()->email,
    'password'    => 'secret',
    'status'      => 1,
    'verified'    => 1,
    'designation' => $faker->domainWord
]);

$factory('MEDoctors\Models\User', 'patient', [
    'fname'       => $faker->firstName,
    'lname'       => $faker->lastName,
    'username'    => 'patient',
    'email'       => 'patient@test.com',
    'password'    => 'secret',
    'status'      => 1,
    'verified'    => 1,
    'designation' => 'Patient'
]);

$factory('MEDoctors\Models\User', 'doctor', [
    'fname'       => $faker->firstName,
    'lname'       => $faker->lastName,
    'username'    => 'doctor',
    'email'       => 'doctor@test.com',
    'password'    => 'secret',
    'status'      => 1,
    'verified'    => 1,
    'designation' => 'Doctor'
]);

$factory('MEDoctors\Models\User', 'admin', [
    'fname'       => $faker->firstName,
    'lname'       => $faker->lastName,
    'username'    => 'admin',
    'email'       => 'admin@test.com',
    'password'    => 'secret',
    'status'      => 1,
    'verified'    => 1,
    'designation' => 'Admin'
]);

$factory('MEDoctors\Models\User', 'LBP_Admin', [
    'fname'       => $faker->firstName,
    'lname'       => $faker->lastName,
    'username'    => 'LBP_Admin',
    'email'       => 'mars@leadingbrands.me',
    'password'    => 'LBP@JBC1502',
    'status'      => 1,
    'verified'    => 1,
    'designation' => 'Admin'
]);