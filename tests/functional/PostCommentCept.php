<?php 

$I = new FunctionalTester($scenario);
$I->wantTo('post a comment.');

$I->logIn();

$I->visitADiscussion('Sample discussion');

$I->postAComment('Sample comment.');

$I->see('Sample comment.');