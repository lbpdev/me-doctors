<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('Start a new diagnosis discussion.');

$I->logIn();

$I->haveAChannel(['slug' => 'doctor']);

$I->haveTherapiesLoaded();

$I->amOnPage('diagnosis/create');

$I->postANewDiscussion('A test diagnosis discussion', 'Sample content for the test diagnosis discussion');

$I->see('A test diagnosis discussion');
$I->seeCurrentUrlEquals('/diagnosis');
$I->seeRecord('discussions', ['title' => 'A test diagnosis discussion']);