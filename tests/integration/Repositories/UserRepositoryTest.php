<?php

use Laracasts\TestDummy\Factory as TestDummy;

class UserRepositoryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \IntegrationTester
     */
    protected $tester;

    protected function _before()
    {
        $this->repo = $this->tester->grabService('MEDoctors\Repositories\Contracts\UserRepository');
    }

    /** @test */
    public function it_gets_the_user_by_username()
    {
        // Given I have users.
        $users = TestDummy::times(10)->create('MEDoctors\Models\User');
        $username = $users[0]->username;

        // When I get a user by username.
        $user = $this->repo->getByUsername($username);

        // Then it returns the user model.
        $this->assertInstanceOf('MEDoctors\Models\User', $user);
        $this->assertEquals($username, $user->username);
    }

    /** @test */
    public function it_updates_the_education_of_the_user()
    {
        // Given I have an array of educations.
        $educations = [
            ['school' => 'Dummy school'],
            ['school' => 'Dummy school2']
        ];
        
        // And and existing user.
        $user = TestDummy::create('MEDoctors\Models\User');

        // When I try to persist the educations.
        $result = $this->repo->updateEducation($user, $educations);

        // Then it should update the user educations.
        $this->tester->seeRecord('educations', [
            'school' => $educations[0]['school'],
            'school' => $educations[1]['school']
        ]);

        $this->assertCount(count($educations), $user->educations);
    }

    /** @test */
    public function it_removes_all_user_education_if_no_education_is_provided()
    {
        // Given I have an empty array of educations.
        $educations = [];
        
        // And and existing user.
        $user = TestDummy::create('MEDoctors\Models\User');

        // When I try to persist the educations.
        $result = $this->repo->updateEducation($user, $educations);

        // Then it should remove all user education.
        $this->assertCount(0, $user->educations);
    }

    /** @test */
    public function it_updates_the_achievements_of_the_user()
    {
        $achievements = [
            ['name' => 'Achievement 1'],
            ['name' => 'Achievement 2'],
        ];

        $user = TestDummy::create('MEDoctors\Models\User');

        $result = $this->repo->updateAchievements($user, $achievements);

        $this->tester->seeRecord('achievements', [
            'name' => $achievements[0]['name'],
            'name' => $achievements[1]['name']
        ]);

        $this->assertCount(count($achievements), $user->achievements);
    }

    /** @test */
    public function it_removes_all_user_achievements_if_no_achievement_is_provided()
    {
        $achievements = [];
        
        $user = TestDummy::create('MEDoctors\Models\User');

        $result = $this->repo->updateAchievements($user, $achievements);

        $this->assertCount(0, $user->achievements);
    }

    /** @test */
    public function it_updates_the_publications_of_the_user()
    {
        $publications = [
            ['name' => 'Book 1'],
            ['name' => 'Book 2'],
        ];

        $user = TestDummy::create('MEDoctors\Models\User');

        $result = $this->repo->updatePublications($user, $publications);

        $this->tester->seeRecord('publications', [
            'name' => $publications[0]['name'],
            'name' => $publications[1]['name']
        ]);

        $this->assertCount(count($publications), $user->publications);
    }

    /** @test */
    public function it_removes_all_user_publications_if_no_publication_is_provided()
    {
        $publications = [];
        
        $user = TestDummy::create('MEDoctors\Models\User');

        $result = $this->repo->updatepublications($user, $publications);

        $this->assertCount(0, $user->publications);
    }

}